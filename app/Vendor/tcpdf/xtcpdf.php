<?php
App::import('Vendor','tcpdf/tcpdf');
 
class XTCPDF extends TCPDF{
	//SetTituloCodigo	
	public  $Nro_codigo;
	public  $nc_forma;
	public  $nc_size;
	//SetTituloPlantilla
	public  $titulo_plantilla;
	public  $tp_forma;
	public  $tp_size;
	//SetTituloContenido
	public  $titulo_contenido;
	public  $tc_forma;
	public  $tc_size;
	//SetFechaCreacion
	public $f_creacion;
	//SetFechaRevision
	public $f_revision;
	//SetLogos
	public $logo_Izq;
	//Orientacion de la Plantilla
	public $orientacion;
	
	public function SetTituloCodigo($Nro_codigo='Titulo del Contenido',$nc_forma='',$nc_size=12){   
	    $this->Nro_codigo=$Nro_codigo;
	    $this->nc_forma=$nc_forma;// B=> bold,I=>Italica
	    $this->nc_size=$nc_size;
	}
	public function SetTituloPlantilla($titulo_plantilla='',$tp_forma='',$tp_size=12){	    
	    $this->titulo_plantilla=$titulo_plantilla;
	    $this->tp_forma=$tp_forma;// B=> bold,I=>Italica
	    $this->tp_size=$tp_size;
	}
	
	public function SetTituloContenido($titulo_contenido='CODIGO:XX-XX-01',$tc_forma='',$tc_size=12){   
	    $this->titulo_contenido=$titulo_contenido;
	    $this->tc_forma=$tc_forma;// B=> bold,I=>Italica
	    $this->tc_size=$tc_size;
	}

	public function SetTituloEncabezado($titulo_contenido='',$tc_forma='',$tc_size=12){   
	    $this->titulo_Encabezado=$titulo_contenido;
	    $this->te_forma=$tc_forma;// B=> bold,I=>Italica
	    $this->te_size=$tc_size;
	}
	
	public function SetFechaCreacion($f_creacion='01/01/1960')
	{   
	    $this->f_creacion=$f_creacion;
	    /*$this->tc_forma=$tc_forma;// B=> bold,I=>Italica
	    $this->tc_size=$tc_size;*/
	}
	
	public function SetFechaRevision($f_revision='01/01/1960'){   
	    $this->f_revision=$f_revision;
	    /*$this->tc_forma=$tc_forma;// B=> bold,I=>Italica
	    $this->tc_size=$tc_size;*/
	}
	public function SetLogos($logo_Izq='',$logo_Der=''){
		if ($logo_Izq!=''){
	    $this->logo_Izq='img/logo/'.$logo_Izq;
	    }else{
		$this->logo_Izq='img/logo.png';
		}
	    if ($logo_Der!=''){
	    $this->logo_Der='img/logo/'.$logo_Der;
	    }else{
		$this->logo_Der='img/logo.png';	
		}
	}

	public function SetOrientacion($orientacion='P'){   
	    $this->orientacion=$orientacion;	    
	}

	protected function cabecera_horizontal(){
		$this->Image(WWW_ROOT.DS.$this->logo_Izq,10,9,20,20,$type = '',	$link = '',	$align = 'center',$resize = true);  		
		$this->Image(WWW_ROOT.DS.'img/blanco.jpg',191,90,100);
		//Times Roman italic 10
		$this->SetFont('helvetica',$this->te_forma,$this->te_size);
		$this->Ln(1);
		//$this->Cell(0,0.5,''.$this->titulo_Encabezado ,0,1,'C');
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_Encabezado,ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tp_forma,$this->tp_size);		
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_plantilla,ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tc_forma[1],$this->tc_size[1]);		
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_contenido[1],ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tc_forma[2],$this->tc_size[2]);
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_contenido[2],ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);	
		//Definir marca de agua para el formato
		$this->setPageMark();
	}

	protected function cabecera_horizontal_impuesto(){
		$this->Image(WWW_ROOT.DS.$this->logo_Izq,10,9,20,20,$type = '',	$link = '',	$align = 'center',$resize = true);  		
	//	$this->Image(WWW_ROOT.DS.'img/blanco.jpg',191,90,100);
		//Times Roman italic 10
		$this->SetFont('helvetica',$this->te_forma,$this->te_size);
		$this->Ln(1);
		//$this->Cell(0,0.5,''.$this->titulo_Encabezado ,0,1,'C');
		$this->MultiCell(240,0.5,html_entity_decode($this->titulo_Encabezado,ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tp_forma,$this->tp_size);		
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_plantilla,ENT_COMPAT ,'UTF-8'),0,'L',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tc_forma[1],$this->tc_size[1]);		
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_contenido[1],ENT_COMPAT ,'UTF-8'),0,'L',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tc_forma[2],$this->tc_size[2]);
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_contenido[2],ENT_COMPAT ,'UTF-8'),0,'L',false,1,31,'',true,0, false,true, 0,'M',false);	
		//Definir marca de agua para el formato
		$this->setPageMark();
	}

	protected function cabecera_vertical(){
		//------Rectangulos Header ------------------------
		//---------------------------------------------------		
		$this->Image(WWW_ROOT.DS.$this->logo_Izq,10,9,20,20,$type = '',	$link = '',	$align = 'center',$resize = true);  		
		$this->Image(WWW_ROOT.DS.'img/blanco.jpg',191,90,100);
		//Times Roman italic 10
		$this->SetFont('helvetica',$this->te_forma,$this->te_size);
		$this->Ln(1);
		//$this->Cell(0,0.5,''.$this->titulo_Encabezado ,0,1,'C');
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_Encabezado,ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tp_forma,$this->tp_size);		
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_plantilla,ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tc_forma[1],$this->tc_size[1]);		
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_contenido[1],ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tc_forma[2],$this->tc_size[2]);
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_contenido[2],ENT_COMPAT ,'UTF-8'),0,'C',false,1,31,'',true,0, false,true, 0,'M',false);		
				
		//Definir marca de agua para el formato
		$this->setPageMark();
	}

	protected function cabecera_horizontal_retencion(){
		//------Rectangulos Header ------------------------
		//---------------------------------------------------		
		$this->Image(WWW_ROOT.DS.$this->logo_Izq,12,9,40);  		
		$this->Image(WWW_ROOT.DS.'img/blanco.jpg',191,90,100);
		//Times Roman italic 10
		$this->SetFont('helvetica',$this->te_forma,$this->te_size);
		$this->Ln(2);
		//$this->Cell(0,0.5,''.$this->titulo_Encabezado ,0,1,'C');
		$this->MultiCell(220,0.5,html_entity_decode($this->titulo_Encabezado,ENT_COMPAT ,'UTF-8'),0,'J',false,1,60,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tp_forma,$this->tp_size);
		$this->Cell(0,0.5,$this->titulo_plantilla,0,1,'C');
		$this->SetFont('helvetica',$this->tc_forma[1],$this->tc_size[1]);		
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_contenido[1],ENT_COMPAT ,'UTF-8'),0,'C',false,1,70,'',true,0, false,true, 0,'M',false);
		$this->SetFont('helvetica',$this->tc_forma[2],$this->tc_size[2]);
		$this->MultiCell(160,0.5,html_entity_decode($this->titulo_contenido[2],ENT_COMPAT ,'UTF-8'),0,'C',false,1,70,'',true,0, false,true, 0,'M',false);		
		
		//Definir marca de agua para el formato
		$this->setPageMark();
	}
	
	public function Header(){

		switch ($this->orientacion) {
		case 'P':
			$this->cabecera_vertical();
		break;
		case 'L':
			$this->cabecera_horizontal();
		break;
		case 'LRET':
			$this->cabecera_horizontal_retencion();
		break;
		case 'LIMP':
			$this->cabecera_horizontal_impuesto();
		break;
		}
	}
	
	public function Footer(){		
		$fecha=date("d-m-Y h:m:s a");
		$this->SetX(-30);
		$this->SetFont('helvetica','',7);
		$this->Cell(0,0.5,''.$fecha,0,1,'L');
	}
	
	public function texto($contenido){
		$this->SetFont('helvetica','',12);
		// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true);
		$this->writeHTMLCell(180,180, '', 40, $contenido, 0, 0, 0, true, 'J', true);
	}

	 /**
     * Print chapter
     * @param $num (int) chapter number
     * @param $title (string) chapter title
     * @param $file (string) name of the file containing the chapter body
     * @param $mode (boolean) if true the chapter body is in HTML, otherwise in simple text.
     * @public
     */
    public function PrintChapter($num, $title, $file, $mode=false) {
        // add a new page
        //$this->AddPage();
        // disable existing columns
        $this->resetColumns();
        // print chapter title
        $this->ChapterTitle($num, $title);
        // set columns
        $this->setEqualColumns(2, 95);
        // print chapter body
        $this->ChapterBody($file, $mode);
    }

    /**
     * Set chapter title
     * @param $num (int) chapter number
     * @param $title (string) chapter title
     * @public
     */
    public function ChapterTitle($num, $title) {
        $this->SetFont('helvetica', '', 7);
        $this->SetFillColor(200, 220, 255);
        $this->Cell(180, 6, 'Chapter '.$num.' : '.$title, 0, 1, '', 1);
        $this->Ln(4);
    }

    /**
     * Print chapter body
     * @param $file (string) name of the file containing the chapter body
     * @param $mode (boolean) if true the chapter body is in HTML, otherwise in simple text.
     * @public
     */
    public function ChapterBody($file, $mode=false) {
        $this->selectColumn();
        // get esternal file content
        //$content = file_get_contents($file, false);
        $content = $file;
        // set font
        $this->SetFont('helvetica', '', 9);
        //$this->SetTextColor(50, 50, 50);
        // print content
        if ($mode) {
            // ------ HTML MODE ------
            $this->writeHTML($content, true, false, true, false, 'J');
        } else {
            // ------ TEXT MODE ------
            $this->Write(0, $content, '', 0, 'J', true, 0, false, true, 0);
        }
        $this->Ln();
    }
}
?>
