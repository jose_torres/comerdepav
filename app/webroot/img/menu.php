<?php
// Set the content-type
header('Content-type: image/png');

// Create the image
$ancho = $_GET['ancho'];
$alto = $_GET['alto'];
$im = imagecreatetruecolor($ancho, $alto);

// Create some colors
$white = imagecolorallocate($im, 255, 255, 255);
$grey = imagecolorallocate($im, 128, 128, 128);
$black = imagecolorallocate($im, 0, 0, 0);
//imagefilledrectangle($im, 0, 0, 200, 29, $white);
imagefill($im,0,0,$white); 
ImageRectangle($im,0,0,199,18,$white); 
// The text to draw
//$text = 'Prueba...';
$text = $_GET['texto'];
// Replace path by your own font path
$font = 'fuentes/Zurchlc.ttf';

// Add some shadow to the text
//imagettftext($im, 20, 0, 11, 21, $grey, $font, $text);
//imagettftext($im, 14, 0, 11, 20, $white, $font, $text);
// Add the text
imagettftext($im, 14, 0, 10,14, $black, $font, $text);

// Using imagepng() results in clearer text compared with imagejpeg()
imagepng($im);
//imagejpeg($im);
imagedestroy($im);
?>
