function actualizar(nombrediv,url,id,datos){
	
	$('#'+nombrediv).html('<img src="/comerdepav2/theme/CakeAdminLTE/img/ajax-loader.gif" title="Espere..." align="absmiddle" alt=""/><br><strong>Cargando...</strong>');
	
			
	$.ajax({
		type: 'POST',
		url: url+id,
		data: datos,
		dataType: 'html'
	})

	.done(function( html ) {					
		$( '#'+nombrediv ).html( html );
	})
	.fail( function() {
    alert( 'No se Guardo Correctamente. Por favor Intente mas Tarde' );
    $( '#'+nombrediv ).html( '<div class="alert alert-danger"><strong>No se Guardo Correctamente</strong></div>' );
	});

}

function seleccionar_todo(){
  for (i=0;i<document.frm.elements.length;i++){
  if(document.frm.elements[i].type == "checkbox"){
	 document.frm.elements[i].checked=1;
	// document.frm.elements[i].checked=source.checked;
	 //alert ("Revisado Total:"+document.frm.elements.length);
	 }
  }
  location.reload(false);	 
} 
	
function deseleccionar_todo(){
  for (i=0;i<document.frm.elements.length;i++){
	  if(document.frm.elements[i].type == "checkbox"){
		 document.frm.elements[i].checked=0;
		 //alert ("Desmarcado Total:"+document.frm.elements.length);
		}
  }
  location.reload(false);	
}

function validar(){

	if(!confirm(" Esta seguro de Cerrar el Cuadre Diarios de Fecha "+document.frm.ReporteHasta.value+"? ")){
		return false;
	}else{
		document.getElementById("frm").submit();
	} 
}

// Actualiza el monto total en html
function f_totales(maximo,valor_sumar,valor_act,opcion){
	
	switch(opcion) {
    case 'f_ingles'://Formato Ingles
        var monto_base=0;
		for (i=1;i<=eval(maximo);i++){
			valor=eval(formato_ingles_campo(document.getElementById(""+valor_sumar+i).value));
			monto_base=parseFloat(eval(monto_base) + valor).toFixed(2);
		}
		valor_act.innerHTML = formato_numero(monto_base , 2 , ',' , '.');
        break;

    default:
        var monto_base=0;
		for (i=1;i<=eval(maximo);i++){
				valor=eval(document.getElementById(""+valor_sumar+i).value);
				//alert(" Valor Sumar:"+valor_sumar+i+' valor:'+valor+" Value:"+document.getElementById(""+valor_sumar+i).value);
				monto_base=parseFloat(eval(monto_base) + valor).toFixed(2);
		}
		valor_act.innerHTML = monto_base
	} 
}

	// Actualiza de manera masiva todos los archivos cargados en la tercera pestaña.
function grabaTodoTabla(TABLAID){
	//tenemos 2 variables, la primera será el Array principal donde estarán nuestros datos y la segunda es el objeto tabla
	var DATA 	= [];
	var TABLA 	= $("#"+TABLAID+" tbody > tr");
	var valores = ''; 
	var valor_id = ''; 
	var cant = ''; 
	var cont=0;
	//una vez que tenemos la tabla recorremos esta misma recorriendo cada TR y por cada uno de estos se ejecuta el siguiente codigo
	TABLA.each(function(){
		cont = cont + 1;
		//por cada fila o TR que encuentra rescatamos 3 datos, el ID de cada fila, la Descripción que tiene asociada en el input text, y el valor seleccionado en un select
/*		var ID 		= $(this).find("td[id='td_id']").text(),
			VALOR 	= $(this).find("input[id*='EfectivoValor*']").val(),
			CANTIDAD 	= $(this).find("input[id*='EfectivoCantidad*']").val(),
			VALOR_ID 	= $(this).find("input[id*='EfectivoId*']").val();*/
		if (cont<=document.getElementById("EfectivoTotales").value){	
		var ID 		= $(this).find("td[id='td_id']").text();
		//alert("EfectivoValor"+cont)	;
		var VALOR 	= document.getElementById("EfectivoValor"+cont).value;
		var	CANTIDAD 	= document.getElementById("EfectivoCantidad"+cont).value;
		var	VALOR_ID 	= document.getElementById("EfectivoDenominacioneId"+cont).value;
		
		valores = valores + '&efecvalor'+cont+'='+VALOR;	
		    cant = cant + '&efeccantidad'+cont+'='+CANTIDAD;	
		    valor_id = valor_id + '&efecid'+cont+'='+VALOR_ID;
		//entonces declaramos un array para guardar estos datos, lo declaramos dentro del each para así reemplazarlo y cada vez
		item = {};
		//si miramos el HTML vamos a ver un par de TR vacios y otros con el titulo de la tabla, por lo que le decimos a la función que solo se ejecute y guarde estos datos cuando exista la variable ID, si no la tiene entonces que no anexe esos datos.
		//if(ID !== ''){
		    	
		    //fin = valores.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
			//tipoventa = valores.substr( 0, fin ); // elimino la coma final
	        item ["id"] 	= ID;
	        item ["valor"] 	= VALOR;
	        item ["denominacione_id"] 	= VALOR_ID;
	        //una vez agregados los datos al array "item" declarado anteriormente hacemos un .push() para agregarlos a nuestro array principal "DATA".
	        DATA.push(item);
		//}
		}
	});
	console.log(DATA);
 
	//eventualmente se lo vamos a enviar por PHP por ajax de una forma bastante simple y además convertiremos el array en json para evitar cualquier incidente con compativilidades.
	INFO 	= new FormData();
	aInfo 	= JSON.stringify(DATA);
 
	INFO.append('data', aInfo);
	return [valores,cant,valor_id];
	//return INFO;
 /*
	$.ajax({
		data: INFO,
		type: 'POST',
		url : './funciones_upload.php',
		processData: false, 
		contentType: false,
		success: function(r){
			//Una vez que se haya ejecutado de forma exitosa hacer el código para que muestre esto mismo.
		}
	}); */
}


function limpiar(TABLAID){
	document.getElementById("SucdepositoBanco").value='';
	document.getElementById("SucdepositoCuentasbancariaId").value='';
	document.getElementById('total_monto').innerHTML='0';
	document.getElementById('total_cantidad').innerHTML='0';
	var TABLA 	= $("#"+TABLAID+" tbody > tr");	
	var cont=0;
	//una vez que tenemos la tabla recorremos esta misma recorriendo cada TR y por cada uno de estos se ejecuta el siguiente codigo
	TABLA.each(function(){
		cont = cont + 1;		
		if (cont<=document.getElementById("EfectivoTotales").value){	
			var ID 		= $(this).find("td[id='td_id']").text();
			document.getElementById("EfectivoCantidad"+cont).value=0;			
			document.getElementById("EfectivoMonto"+cont).value=0;			
		}
	});
	console.log(DATA);
	
}

function limpiardeposito(TABLAID,opcion='Deposito'){

	if(opcion=='Deposito'){
		document.getElementById(TABLAID+"Id").value='';	
		document.getElementById(TABLAID+"Tipo").value='';	
		document.getElementById(TABLAID+"Nrodocumento").value='';
		document.getElementById(TABLAID+"Monto").value='';
		document.getElementById("CuentabancariaNumerocuenta").value='';
		document.getElementById("BancoNombre").value='';
	}else if(opcion=='Gastos'){
		document.getElementById(TABLAID+"Id").value='';	
		document.getElementById(TABLAID+"Nro").value='';	
		document.getElementById(TABLAID+"Nombre").value='';
		document.getElementById(TABLAID+"Motivo").value='';
		document.getElementById(TABLAID+"Monto").value='';
	}else if(opcion=='Retencion'){
		document.getElementById(TABLAID+"Id").value='';	
		document.getElementById(TABLAID+"Numero").value='';	
		document.getElementById(TABLAID+"Documento").value='';
		document.getElementById(TABLAID+"Cliente").value='';
		document.getElementById(TABLAID+"Montobruto").value='';
		document.getElementById(TABLAID+"Montoiva").value='';
		document.getElementById(TABLAID+"Montoretenido").value='';
	}
	
}


function guardar(nombrediv,url,id,datos){
	var INFO = new Array();
	//var INFO2 = new FormData();
	INFO = grabaTodoTabla('efectivo');		
	valores=INFO[0]; cant=INFO[1];valor_id=INFO[2];		//INFO2=INFO[3];
	if(document.getElementById("SucdepositoBanco").value!='' && document.getElementById("SucdepositoCuentasbancariaId").value!='' && eval(document.getElementById('total_monto').innerHTML)>0){
		actualizar(nombrediv,url,id,datos+valores+cant+valor_id);
		limpiar('efectivo');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}

function guardarDeposito(nombrediv,url,id,datos){
	if(document.getElementById("MovbancarioNrodocumento").value!=''){
		actualizar(nombrediv,url,id,datos);
		limpiardeposito('Movbancario');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}

function guardarGasto(nombrediv,url,id,datos){
	if(document.getElementById("GastoNro").value!=''){
		actualizar(nombrediv,url,id,datos);
		limpiardeposito('Gasto','Gastos');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}

function guardarRetencion(nombrediv,url,id,datos){
	if(document.getElementById("ConretencionventadetNumero").value!='' && document.getElementById("ConretencionventadetNumero").value!='____-__-__________'){
		actualizar(nombrediv,url,id,datos);
		limpiardeposito('Conretencionventadet','Retencion');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}
