function actualizar(nombrediv,url,id,datos){
	
	$('#'+nombrediv).html('<img src="/comerdepav2/theme/CakeAdminLTE/img/ajax-loader.gif" title="Espere..." align="absmiddle" alt=""/><br><strong>Cargando...</strong>');
	
	var selected = '&clientes=0-';var cont=1;
	$('#ReporteClienteId option:checked').each(function(){
	selected += $(this).val() + '-'; 
	});
	fin = selected.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
	selected = selected.substr( 0, fin ); // elimino la coma final
	
	var vendedores = '&vendedores=0-';var cont=1;
	$('#ReporteVendedoreId option:checked').each(function(){
	vendedores += $(this).val() + '-'; 
	});
	fin = vendedores.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
	vendedores = vendedores.substr( 0, fin ); // elimino la coma final
	
	var tipoventa = '&tipoventa=0-';var cont=1;
	$('#ReporteTipoventa option:checked').each(function(){
	tipoventa += $(this).val() + '-'; 
	});
	fin = tipoventa.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
	tipoventa = tipoventa.substr( 0, fin ); // elimino la coma final
			
	$.ajax({
		type: 'POST',
		url: url+id,
		data: datos+''+selected+''+vendedores+''+tipoventa,
		dataType: 'html'
	})

	.done(function( html ) {					
		$( '#'+nombrediv ).html( html );
	});

}

$(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
