$(function () {
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   
    
  });
    function prueba(){
      let desde = $('#desde').val();
      let hasta = $('#hasta').val();
      let estatus = $('#cmbOrdenarPor').val();
      const nombrediv = 'buscar';
      let datos = {
        fechainicio:desde!=''?desde:null, 
        fechafin:hasta!=''?hasta:null,
        estatus: estatus
        }
      $.ajax({
        type: 'POST',
        url: 'buscarajustes',
        contentType: 'application/json',
        data: JSON.stringify(datos),
        dataType: 'html'
      })
      .done(function( html ) {					
        $( '#'+nombrediv ).html( html );
        $('#btnLimpiar').show();
      })
      .fail( function() {
        alert( 'Por favor Intente mas Tarde' );
        $( '#'+nombrediv ).html( '<div class="alert alert-danger"><strong>No hay Conexion con la Sucursal. Revise las conexiones de Internet.</strong></div>' );
        $('#btnLimpiar').show();
     
      });
    }