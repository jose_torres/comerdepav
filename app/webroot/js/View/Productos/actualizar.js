function actualizar(nombrediv,url,id,datos){
	$('#'+nombrediv).html('<img src="/comerdepav2/theme/CakeAdminLTE/img/ajax-loader.gif" title="Espere..." align="absmiddle" alt=""/><br><strong>Enviando Productos. Esto puedo tardar algunos minutos ...</strong>');
			
	$.ajax({
		type: 'POST',
		url: url+id,
		data: datos,
		dataType: 'html'
	})

	.done(function( html ) {					
		$( '#'+nombrediv ).html( html );
	})
        .fail( function() {
    alert( 'No hay Conexion con la Sucursal. Por favor Intente mas Tarde' );
    $( '#'+nombrediv ).html( '<div class="alert alert-danger"><strong>No hay Conexion con la Sucursal. Revise las conexiones de Internet.</strong></div>' );
	})
    ;

}
