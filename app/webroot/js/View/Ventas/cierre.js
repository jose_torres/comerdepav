function actualizar(nombrediv,url,id,datos){
	
	$('#'+nombrediv).html('<img src="/comerdepav2/theme/CakeAdminLTE/img/ajax-loader.gif" title="Espere..." align="absmiddle" alt=""/><br><strong>Cargando...</strong>');
	
	var selected = '&clientes=0-';var cont=1;
	$('#ReporteClienteId option:checked').each(function(){
	selected += $(this).val() + '-'; 
	});
	fin = selected.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
	selected = selected.substr( 0, fin ); // elimino la coma final
	
	var vendedores = '&vendedores=0-';var cont=1;
	$('#ReporteVendedoreId option:checked').each(function(){
	vendedores += $(this).val() + '-'; 
	});
	fin = vendedores.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
	vendedores = vendedores.substr( 0, fin ); // elimino la coma final
	
	var tipoventa = '&tipoventa=0-';var cont=1;
	$('#ReporteTipoventa option:checked').each(function(){
	tipoventa += $(this).val() + '-'; 
	});
	fin = tipoventa.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
	tipoventa = tipoventa.substr( 0, fin ); // elimino la coma final
			
	$.ajax({
		type: 'POST',
		url: url+id,
		data: datos+''+selected+''+vendedores+''+tipoventa,
		dataType: 'html'
	})

	.done(function( html ) {					
		$( '#'+nombrediv ).html( html );
	})
	.fail( function() {
    alert( 'No hay Conexion con la Sucursal. Por favor Intente mas Tarde' );
    $( '#'+nombrediv ).html( '<div class="alert alert-danger"><strong>No hay Conexion con la Sucursal</strong></div>' );
	});

}

function seleccionar_todo(){
  for (i=0;i<document.frm.elements.length;i++){
  if(document.frm.elements[i].type == "checkbox"){
	 document.frm.elements[i].checked=1;
	 }
  }	 
} 
	
function deseleccionar_todo(){
  for (i=0;i<document.frm.elements.length;i++){
	  if(document.frm.elements[i].type == "checkbox"){
		 document.frm.elements[i].checked=0;
		}
  }	
}

function validar(){

	if(!confirm(" Esta seguro de Cerrar el Cuadre Diarios de Fecha "+document.frm.ReporteHasta.value+"? ")){
		return false;
	}else{
		document.getElementById("frm").submit();
	} 
}

// Actualiza el monto total en html
function f_totales(maximo,valor_sumar,valor_act,opcion){
	
	switch(opcion) {
    case 'f_ingles'://Formato Ingles
        var monto_base=0;
		for (i=1;i<=eval(maximo);i++){
			valor=eval(formato_ingles_campo(document.getElementById(""+valor_sumar+i).value));
			monto_base=parseFloat(eval(monto_base) + valor).toFixed(2);
		}
		valor_act.innerHTML = formato_numero(monto_base , 2 , ',' , '.');
        break;

    default:
        var monto_base=0;
		for (i=1;i<=eval(maximo);i++){
				valor=eval(document.getElementById(""+valor_sumar+i).value);
				//alert(" Valor Sumar:"+valor_sumar+i+' valor:'+valor+" Value:"+document.getElementById(""+valor_sumar+i).value);
				monto_base=parseFloat(eval(monto_base) + valor).toFixed(2);
		}
		valor_act.innerHTML = monto_base
	} 
}

function calcularCheques(campo,campoRecibir,campoDar){
	//alert("Campo:"+campo+" Valor"+document.getElementById("Seleccion1Chequeo").checked);
	if (document.getElementById(""+campo).checked==true){
		document.getElementById(""+campoRecibir).value = document.getElementById(""+campoDar).value;
	//	alert("Campo:"+document.getElementById(""+campoRecibir).value);
	}else{
		document.getElementById(""+campoRecibir).value = 0;
	}
	f_totales(document.getElementById("ChequeTotales").value,"ChequeMontosel",document.getElementById("total_monto_che_dep"));
	document.getElementById("total_monto_che_dep").innerHTML=formato_numero(eval( document.getElementById("total_monto_che_dep").innerHTML ), 2 , ',' , '.');
}

function calcular(valor,pos){

	var cantidad = document.getElementById("EfectivoCantidad"+pos);
	var monto = document.getElementById("EfectivoMonto"+pos);
	var totales = document.getElementById("EfectivoTotales");

	var total_registro = document.getElementById("EfectivoTotales");
	var total_cantidad = document.getElementById("total_cantidad");
	var total_monto = document.getElementById("total_monto");

	total=parseFloat(cantidad.value*valor).toFixed(2);
	monto.value=total;

	// Se actualiza los totales
	f_totales(total_registro.value,"EfectivoCantidad",total_cantidad);
	f_totales(total_registro.value,"EfectivoMonto",total_monto);

}

function calcularMontoDebito(valorActual,valorMax,campoDevolver){
	//alert ("valorActual:"+valorActual+" valorMax:"+valorMax);
	if(eval(valorActual)>eval(valorMax) ){
		alert ("El monto a Depositar debe ser Menor o Igual a Bs."+formato_numero(valorMax, 2 , ',' , '.')+" ");
		document.getElementById(""+campoDevolver).value = formato_numero(valorMax, 2 , ',' , '.');
		valorActual = valorMax;
	}
	f_totales(document.getElementById("DebitoTotales").value,"DebitoMonto",document.getElementById("total_monto_deb_dep"),'f_ingles');

}

	// Actualiza de manera masiva todos los archivos cargados en la tercera pestaña.
function grabaTodoTabla(TABLAID){
	//tenemos 2 variables, la primera será el Array principal donde estarán nuestros datos y la segunda es el objeto tabla
	var DATA 	= [];
	var TABLA 	= $("#"+TABLAID+" tbody > tr");
	var valores = ''; 
	var valor_id = ''; 
	var cant = ''; 
	var cont=0;
	//una vez que tenemos la tabla recorremos esta misma recorriendo cada TR y por cada uno de estos se ejecuta el siguiente codigo
	TABLA.each(function(){
		cont = cont + 1;
		//por cada fila o TR que encuentra rescatamos 3 datos, el ID de cada fila, la Descripción que tiene asociada en el input text, y el valor seleccionado en un select
/*		var ID 		= $(this).find("td[id='td_id']").text(),
			VALOR 	= $(this).find("input[id*='EfectivoValor*']").val(),
			CANTIDAD 	= $(this).find("input[id*='EfectivoCantidad*']").val(),
			VALOR_ID 	= $(this).find("input[id*='EfectivoId*']").val();*/
		if (cont<=document.getElementById("EfectivoTotales").value){	
		var ID 		= $(this).find("td[id='td_id']").text();
		//alert("EfectivoValor"+cont)	;
		var VALOR 	= document.getElementById("EfectivoValor"+cont).value;
		var	CANTIDAD 	= document.getElementById("EfectivoCantidad"+cont).value;
		var	VALOR_ID 	= document.getElementById("EfectivoDenominacioneId"+cont).value;
		
		valores = valores + '&efecvalor'+cont+'='+VALOR;	
		    cant = cant + '&efeccantidad'+cont+'='+CANTIDAD;	
		    valor_id = valor_id + '&efecid'+cont+'='+VALOR_ID;
		//entonces declaramos un array para guardar estos datos, lo declaramos dentro del each para así reemplazarlo y cada vez
		item = {};
		//si miramos el HTML vamos a ver un par de TR vacios y otros con el titulo de la tabla, por lo que le decimos a la función que solo se ejecute y guarde estos datos cuando exista la variable ID, si no la tiene entonces que no anexe esos datos.
		//if(ID !== ''){
		    	
		    //fin = valores.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
			//tipoventa = valores.substr( 0, fin ); // elimino la coma final
	        item ["id"] 	= ID;
	        item ["valor"] 	= VALOR;
	        item ["denominacione_id"] 	= VALOR_ID;
	        //una vez agregados los datos al array "item" declarado anteriormente hacemos un .push() para agregarlos a nuestro array principal "DATA".
	        DATA.push(item);
		//}
		}
	});
	console.log(DATA);
 
	//eventualmente se lo vamos a enviar por PHP por ajax de una forma bastante simple y además convertiremos el array en json para evitar cualquier incidente con compativilidades.
	INFO 	= new FormData();
	aInfo 	= JSON.stringify(DATA);
 
	INFO.append('data', aInfo);
	return [valores,cant,valor_id];
	//return INFO;
 /*
	$.ajax({
		data: INFO,
		type: 'POST',
		url : './funciones_upload.php',
		processData: false, 
		contentType: false,
		success: function(r){
			//Una vez que se haya ejecutado de forma exitosa hacer el código para que muestre esto mismo.
		}
	}); */
}

	// Actualiza de manera masiva todos los archivos cargados en la tercera pestaña.
function grabaTodoTablaCheque(TABLAID){
	//tenemos 2 variables, la primera será el Array principal donde estarán nuestros datos y la segunda es el objeto tabla
	var DATA 	= [];
	var TABLA 	= $("#"+TABLAID+" tbody > tr");
	var valores = ''; 
	var valor_id = ''; 
	var cant = ''; 
	var cont=0;
	//una vez que tenemos la tabla recorremos esta misma recorriendo cada TR y por cada uno de estos se ejecuta el siguiente codigo
	TABLA.each(function(){
		cont = cont + 1;
		//por cada fila o TR que encuentra rescatamos 3 datos, el ID de cada fila, la Descripción que tiene asociada en el input text, y el valor seleccionado en un select
/*		var ID 		= $(this).find("td[id='td_id']").text(),
			VALOR 	= $(this).find("input[id*='EfectivoValor*']").val(),
			CANTIDAD 	= $(this).find("input[id*='EfectivoCantidad*']").val(),
			VALOR_ID 	= $(this).find("input[id*='EfectivoId*']").val();*/
		if (cont<=document.getElementById("ChequeTotales").value){	
		var ID 		= $(this).find("td[id='td_id']").text();
		//alert("EfectivoValor"+cont)	;
		//var VALOR 	= document.getElementById("EfectivoValor"+cont).value;
		var	CANTIDAD 	= document.getElementById('Seleccion'+cont+'Chequear').checked;
		var	VALOR_ID 	= document.getElementById("ChequeId"+cont).value;
		 if(CANTIDAD==true){   
		   valor_id = valor_id + '&cheque_id'+cont+'='+VALOR_ID;
		 } 
	
		}
	});
	
	return [valor_id];
}

function grabaTodoTablaDebito(TABLAID){
	//tenemos 2 variables, la primera será el Array principal donde estarán nuestros datos y la segunda es el objeto tabla
	var DATA 	= [];
	var TABLA 	= $("#"+TABLAID+" tbody > tr");
	var valores = ''; 	 
	var valor_id = ''; 
	var cuentabanco_id = ''; 
	var banco = ''; 
	var cant = ''; 
	var cont=0;

	TABLA.each(function(){
		cont = cont + 1;

		if (cont<=document.getElementById("DebitoTotales").value){	
		var ID 		= $(this).find("td[id='td_id']").text();
		//alert("EfectivoValor"+cont)	;
		var MONTO 	= formato_ingles_campo(document.getElementById("DebitoMonto"+cont).value);
		var CUENTA 	= document.getElementById("DebitoCodcuenta"+cont).value;
		var	VALOR_ID 	= document.getElementById("DebitoCodpuntoventa"+cont).value;
		var	BANCO 	= document.getElementById("DebitoCodbanco"+cont).value;
		 
		valor_id = valor_id + '&codpuntoventa'+cont+'='+VALOR_ID;
		valores = valores + '&montodeb'+cont+'='+MONTO;
		cuentabanco_id = cuentabanco_id + '&cuentasbancaria_id'+cont+'='+CUENTA;
		banco = banco + '&banco_id'+cont+'='+BANCO;
		  
		}
	});
	return [valores,cuentabanco_id,valor_id,banco];
}

function limpiar(TABLAID){
	document.getElementById("SucdepositoBanco").value='';
	document.getElementById("SucdepositoCuentasbancariaId").value='';
	document.getElementById('total_monto').innerHTML='0';
	document.getElementById('total_cantidad').innerHTML='0';
	var TABLA 	= $("#"+TABLAID+" tbody > tr");	
	var cont=0;
	//una vez que tenemos la tabla recorremos esta misma recorriendo cada TR y por cada uno de estos se ejecuta el siguiente codigo
	TABLA.each(function(){
		cont = cont + 1;		
		if (cont<=document.getElementById("EfectivoTotales").value){	
			var ID 		= $(this).find("td[id='td_id']").text();
			document.getElementById("EfectivoCantidad"+cont).value=0;			
			document.getElementById("EfectivoMonto"+cont).value=0;			
		}
	});
	console.log(DATA);
	
}

function limpiardeb(TABLAID){
	
	//document.getElementById(TABLAID+"Banco").value='';
	document.getElementById(TABLAID+"Maquinas").value='';
	document.getElementById(TABLAID+"Nrodocumento").value='';
	//document.getElementById(TABLAID+"Monto").value=0;
	
}

function limpiartrans(TABLAID){
	
	document.getElementById(TABLAID+"Banco").value='';
	document.getElementById(TABLAID+"Nrodocumento").value='';
	document.getElementById(TABLAID+"Monto").value=0;
	
}

function limpiarcheque(TABLAID){
	
	document.getElementById(TABLAID+"Banco").value='';
	document.getElementById(TABLAID+"CuentasbancariaId").value='';
	document.getElementById(TABLAID+"Nrodocumento").value='';
	document.getElementById("total_monto_che_dep").innerHTML=0;
	
}

function limpiargasto(TABLAID){
	
	document.getElementById(TABLAID+"Nro").value='';
	document.getElementById(TABLAID+"Motivo").value='';
	document.getElementById(TABLAID+"Nombre").value='';
	document.getElementById(TABLAID+"Monto").value=0;
	
}

function guardar(nombrediv,url,id,datos){
	var INFO = new Array();
	//var INFO2 = new FormData();
	INFO = grabaTodoTabla('efectivo');		
	valores=INFO[0]; cant=INFO[1];valor_id=INFO[2];		//INFO2=INFO[3];
	if(document.getElementById("SucdepositoBanco").value!='' && document.getElementById("SucdepositoCuentasbancariaId").value!='' && eval(document.getElementById('total_monto').innerHTML)>0){
		actualizar(nombrediv,url,id,datos+valores+cant+valor_id);
		limpiar('efectivo');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}

function guardardeb(nombrediv,url,id,datos){
	var INFO = new Array();
	INFO = grabaTodoTablaDebito('puntos');
	valores=INFO[0]; cuentabanco_id=INFO[1];valor_id=INFO[2];banco_id=INFO[3];			
	if(document.getElementById("DebitoMaquinas").value!='' && document.getElementById("DebitoNrodocumento").value!='' && eval(formato_ingles_campo(document.getElementById('total_monto_deb_dep').innerHTML))>0){
		actualizar(nombrediv,url,id,datos+valores+cuentabanco_id+valor_id+banco_id);
		limpiardeb('Debito');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}

function guardartrans(nombrediv,url,id,datos){
			
	if(document.getElementById("TransferBanco").value!='' && document.getElementById("TransferCuentasbancariaId").value!='' && eval(formato_ingles_campo(document.getElementById('TransferMonto').value))>0){
		actualizar(nombrediv,url,id,datos);
		limpiartrans('Transfer');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}

function guardarcheque(nombrediv,url,id,datos){
	var INFO = new Array();
	INFO = grabaTodoTablaCheque('cheque');		
	valor_id=INFO[0];		
	if(document.getElementById("ChequeBanco").value!='' && document.getElementById("ChequeCuentasbancariaId").value!='' && eval(formato_ingles_campo(document.getElementById("total_monto_che_dep").innerHTML))>0){
		actualizar(nombrediv,url,id,datos+valor_id);
		limpiarcheque('Cheque');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}

function guardargasto(nombrediv,url,id,datos){
			
	if(document.getElementById("GastoNombre").value!='' && document.getElementById("GastoMotivo").value!='' && eval(document.getElementById('GastoMonto').value)>0){
		actualizar(nombrediv,url,id,datos);
		limpiargasto('Gasto');
		alert("Guardo Registro");	
	}else{
		alert("Falta Campos por llenar");	
	}
	
}

function calcularCuadre(){
	document.getElementById('total_cuadre').innerHTML=formato_numero(eval(formato_ingles_campo(document.getElementById('tot_res_depositos').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_gastos').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_retencion').innerHTML)) - eval(formato_ingles_campo(document.getElementById('total_ingreso_cuadre').innerHTML)), 2 , ',' , '.' );
	if(eval(formato_ingles_campo(document.getElementById('total_cuadre').innerHTML))>0){
		document.getElementById('mensaje_cuadre').innerHTML="Sobrante";
	}else if(eval(formato_ingles_campo(document.getElementById('total_cuadre').innerHTML))<0){
		document.getElementById('mensaje_cuadre').innerHTML="Faltante";
	}

}

function calcularDepositos(){
	document.getElementById('tot_res_depositos').innerHTML=formato_numero(eval(formato_ingles_campo(document.getElementById('tot_res_efectivo').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_debito').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_cheque').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_trans').innerHTML)) , 2 , ',' , '.');
}

