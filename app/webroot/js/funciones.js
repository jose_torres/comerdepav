function trim(cadena)
	{
		for(i=0; i<cadena.length; )
		{
			if(cadena.charAt(i)==" ")
				cadena=cadena.substring(i+1, cadena.length);
			else
				break;
		}

		for(i=cadena.length-1; i>=0; i=cadena.length-1)
		{
			if(cadena.charAt(i)==" ")
				cadena=cadena.substring(0,i);
			else
				break;
		}
		
		return cadena;
	}
/*
 * Da formato a un número para su visualización
 *
 * numero (Number o String) - Número que se mostrará
 * decimales (Number, opcional) - Nº de decimales (por defecto, auto)
 * separador_decimal (String, opcional) - Separador decimal (por defecto, coma)
 * separador_miles (String, opcional) - Separador de miles (por defecto, ninguno)
 */
function formato_numero(numero, decimales, separador_decimal, separador_miles){ // v2007-08-06
    numero=parseFloat(numero);
    if(isNaN(numero)){
        return "";
    }

    if(decimales!==undefined){
        // Redondeamos
        numero=numero.toFixed(decimales);
    }

    // Convertimos el punto en separador_decimal
    numero=numero.toString().replace(".", separador_decimal!==undefined ? separador_decimal : ",");

    if(separador_miles){
        // Añadimos los separadores de miles
        var miles=new RegExp("(-?[0-9]+)([0-9]{3})");
        while(miles.test(numero)) {
            numero=numero.replace(miles, "$1" + separador_miles + "$2");
        }
    }

    return numero;
}
/*
 * Da formato a un número para su visualización en el textbox
 *
 * Campo (Number o String) - Nombre del Campo donde actualizara en formato número 
 */
function numero(Campo){
		// Obtener última tecla pulsada 11/Abr/2012 V.L.
		enny=document.getElementById(""+Campo).value;
		cad1=enny.substring(0,enny.length-1);
		cad2=enny.substr(enny.length-1,1);
		if(cad2 == '.' || cad2 == ',' ){
			var num=cad1+',';
		}else{
			if(isNaN(cad2)){
				var num=cad1;
			}else{
				var num=cad1+cad2;
			}
		}
		obj=document.getElementById(""+Campo).value;
		simbolo='';
		punto = "";
		posicion = num.indexOf(',');
		ubicacion = num.indexOf(',');
		cents = "";
		if(posicion != -1){
			punto = ",";
			cents = num.substring(posicion);
			if(cents.length >= 4) cents = cents.substring(0, 3);
			posicion += posicion / 3;
			num = num.split(',');
			num = num.toString();
			num = num.substring(0,ubicacion);
		}
		num = num.replace(/\$|\./g,'');
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		num = Math.floor(num/100).toString();
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+'.'+ num.substring(num.length-(4*i+3));
		if(simbolo) simbolo = '$'
		else simbolo = ''
		document.getElementById(""+Campo).value= (((sign)?'':'-') + simbolo + num + ((cents != "00" && cents.length > 0) ? cents : ''));
	}
/*
 * Da formato a un número para su visualización en formato ingles Ej. 1234.56
 *
 * Campo (Number o String) - Nombre del Campo donde actualizara en formato número 
 */

 function formato_ingles(Campo){
	var texto = document.getElementById(""+Campo).value;
	var val1=texto.replace(/\$|\./g,'');
	var val2=val1.replace(/\$|\,/g,'.');
	return val2;
 }
 
 /*
 * Da formato a un número para su visualización en formato ingles Ej. 1234.56
 *
 * Campo (Number o String) - Nombre del Campo donde actualizara en formato número 
 */

 function formato_ingles_c(Campo){
	var texto = Campo.toString();
	var val1=texto.replace(/\$|\./g,'');
	var val2=val1.replace(/\$|\,/g,'.');
	return parseFloat(val2,2);
 }

 /*
 * Da formato a un número para su visualización en formato ingles Ej. 1234.56
 *
 * Campo (Number o String) - Nombre del Campo donde actualizara en formato número 
 */

 function formato_ingles_campo(Campo){
	var texto = Campo;
	var val1=texto.replace(/\$|\./g,'');
	var val2=val1.replace(/\$|\,/g,'.');
	return val2;
 }
