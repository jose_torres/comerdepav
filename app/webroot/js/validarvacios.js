
function validarvacios(cajatexto,mensaje)
{
	var p='V';	
	if (cajatexto.value=='' || cajatexto.value=='...escriba aqui'){
		    alert(""+mensaje);
		    cajatexto.focus();	
		    p='F';
	}
	return p;
}

function validarconvalor(cajatexto,valorcomparar,mensaje)
{
	var p='V';	
	if (cajatexto.value==valorcomparar){
		    alert(""+mensaje);
		    cajatexto.focus();	
		    p='F';
	}
	return p;
}

function validarnoiguales(cajatexto,valorcomparar,mensaje)
{
	var p='V';	
	if (cajatexto.value!=valorcomparar){
		    alert(""+mensaje);
		    cajatexto.focus();	
		    p='F';
	}
	return p;
}

function validarnegativo(cajatexto,mensaje)
{
	var p='V';	
	if (cajatexto.value>0){
		    alert(""+mensaje);
		    cajatexto.focus();	
		    p='F';
	}
	return p;
}