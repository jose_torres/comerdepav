<?php
class ConretencionventadetsController extends AppController
{
    public $name = 'Conretencionventadets';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Conretencionventadet','Conretencionventa','Retgenventa','Oficina','Grupo','Configuracion','Funcione','Perfile','Concliente','Vendedore','Proveedore','Compra','Cpabono','Cpabonopago','Empresa','Sucursal','Concuadrediario','Conventa');

//-----------------------Seguridad--------------------------------------	
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------	
    public function add(){
            $this->checkSession();
            $title_for_layout='Registrar una Retenci&oacute;n';
            $proveedore=$this->Proveedore->llenar_combo();
            $this->set(compact('proveedore','title_for_layout'));		          
    }

    public function edit($id = null) {
            $this->checkSession();
            if (!$id && empty($this->data)) {
                    $this->Session->setFlash(__('Retencion Invalida', true));
                    $this->redirect(array('action' => 'index'));
            }
            if (!empty($this->data)) {
                    $datos=$this->data;
                    $datos['Conretencionventa']['numero']=$datos['Conretencionventadet']['numero'];
                    if ($this->Conretencionventadet->save($datos)) {
                            $abonos = $this->Conretencionventa->save($datos);				
                            $this->flash('La Retencion ha sido Guardado.','/conretencionventadets/index');
                    } else {
                            $this->flash('La Retencion no se puede Guardar.','/conretencionventadets/edit/'.$datos['Conretencionventadet']['edit']);
                    }
            }		
            if (empty($this->data)) {
                    $this->data = $this->Conretencionventadet->read(null, $id);
            }
            $title_for_layout='RETENCIONES DE VENTAS';
            $this->set(compact('title_for_layout'));
    }

    public function buscarfacturas($id=NULL) {
            $datos=$_POST;
            $data=$this->Retencioncompra->reporte($datos,'',2);
            $this->set(compact('data','datos'));
            $this->render('buscarfacturas', 'ajax');
    }

    public function calcularretencion($id=NULL) {
            //print_r($_POST);		
            $registros=$_POST;
            $registros['codmovimientos']=explode('-',$_POST['codmovimientos']);
            $registros['tipomovimientos']=explode('-',$_POST['tipomovimientos']);
            $registros['nrodocumentos']=explode('-',$_POST['nrodocumentos']);
            $data=$this->Compra->calcular_retencion_compras($registros);
            $facturas=$this->Retencioncompra->reporte($registros,'',3);	
            $this->set(compact('data','registros','facturas'));
            $this->render('calcularretencion', 'ajax');
    }	

    public function incluir($id=NULL) {
        $this->checkSession();
        if (!empty($this->data)) {
                $datos=$this->data;
                $datos=$this->Retencioncompra->ajustarDatos('add',$datos);
                $valor['Retencioncompra']=$datos['Retencioncompras'];

                if ($this->Retencioncompra->saveAll($valor['Retencioncompra'])) {
                        $this->Retencioncompra->guardarAbono($datos);
                        $this->flash('La Retencion ha sido Guardado.','/retenciones/add');
                } else {
                        $this->flash('La Retencion no se puede Guardar.','/retenciones/add');
                }
        }
    }

    public function index() {
            $this->checkSession();
            $title_for_layout='LISTA DE RETENCIONES DE VENTAS REGISTRADOS ';
            $proveedore=$this->Concliente->llenar_combo();
            $this->Conretencionventadet->recursive = 0;
            $criteria = " Conretencionventa.estatus = 'A' ";
            //$criteria = " ";
            $order="Conretencionventa.fechaemision DESC, Conretencionventadet.numero DESC";
    $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order/*,'group'=>$grupo,'fields'=>$campos*/);
            $this->set('unidades', $this->paginate());
            $this->set(compact('title_for_layout','proveedore'));
    }

    public function conciliados() {
            $this->checkSession();
            $title_for_layout='LISTA DE RETENCIONES DE VENTAS REGISTRADOS ';
            Configure::write('Model.globalSource', $this->CONEXION);
            $sucursal = $this->Sucursal->read(null,1);
            Configure::write('Model.globalSource', 'default');
    /*	$this->Conretencionventadet->incluirFaltante($sucursal);
            $this->Conretencionventa->incluirFaltante($sucursal);
            $this->Conventa->incluirFaltante($sucursal);
            $this->Concuadrediario->transferirclientes($sucursal,'');*/
            $primeraFecha = $this->Conretencionventadet->getFecha();

            $this->Conretencionventadet->recursive = 0;
            $criteria = " Conretencionventa.estatus = 'C' ";
            $order="Conretencionventa.fechaemision DESC, Conretencionventadet.numero DESC";
    $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order/*,'group'=>$grupo,'fields'=>$campos*/);
            $this->set('unidades', $this->paginate());
            $this->set(compact('title_for_layout','primeraFecha'));
    }

    public function cerrados() {
            $this->checkSession();
            $title_for_layout='LISTA DE RETENCIONES DE VENTAS REGISTRADOS ';
            //$proveedore=$this->Concliente->llenar_combo();
            $this->Retgenventa->recursive = 0;
            $criteria = "  ";
            $order="Retgenventa.id DESC";
    $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order/*,'group'=>$grupo,'fields'=>$campos*/);
            $this->set('unidades', $this->paginate('Retgenventa'));
            $this->set(compact('title_for_layout','proveedore'));
    }

    public function transito() {
            $this->checkSession();
            $title_for_layout='LISTA DE RETENCIONES EN TRANSITO REGISTRADOS ';
            $this->Retencioncompra->recursive = 0;
            $criteria = "Retencioncompra.tiporetencion='Transito' ";
            $order="Retencioncompra.codretencion DESC";        
    $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
            $this->set('unidades', $this->paginate());
            $this->set(compact('title_for_layout'));
    }

    public function viewpdf($id){
            $this->checkSession();
            $this->layout = 'pdf';						
            $empresa = $this->Empresa->find('first',array('conditions'=>''));	
            $encabezado=$this->Retencioncompra->getCabecera();

            //$data = $this->Retencioncompra->read(null, $id);
            $data = $this->Retencioncompra->find('all',array('conditions'=>" Retencioncompra.numero='".$id."'"));	
            $datos_nc = $this->Nccompra->busquedas('Datos',$data);
            $datos['numero']=$data[0]['Retencioncompra']['numero'];
            $datos_nd = $this->Cpnotadebito->reporte($datos);

            $quincena = $this->Retencioncompra->getQuincena(date('d',strtotime($data[0]['Retencioncompra']['fechaemision'])));		
            $this->set(compact('data','texto','empresa','encabezado','quincena','datos_nc','datos_nd'));
    }

    public function buscarretenciones($id=1){
            $datos=$_POST;
            $provee=explode('-',$_POST['proveedores']);
            $datos['provee']=$provee;
            $data = $this->Retencioncompra->findRetencion($datos);
   // print_r ($data);
   // die();
    $this->set(compact('data'));
    $this->render('buscarretenciones', 'ajax');
    }

    public function declarar(){
            $this->checkSession();
            $title_for_layout='REGISTRO DE IMPUESTO RETENIDO ';
            //Buscar Declaracion
            $declaracion = $this->Retgenventa->getDeclaracionActiva();
            $primeraFecha = $this->Conretencionventadet->getFecha();
            $datos['retgenventa_id'] = $declaracion['Retgenventa']['id'];
            $primeraFechaAsociado = $this->Conretencionventadet->getFecha('primera_asociada',$datos);
            $asociado = $noasociado = array();
            $this->set(compact('title_for_layout','declaracion','asociado','noasociado','primeraFecha','primeraFechaAsociado'));
    }

    public function buscarlistado($opcion='no_asociado'){
            $datos=$_POST;
            $noasociado = $this->Conretencionventadet->listado($datos['opcion'],$datos);
            $variables = $this->Conretencionventadet->getVariables($datos);
            $this->set(compact('noasociado','variables'));
    $this->render('buscarlistado', 'ajax');
    }

    public function asociar(){
            $this->checkSession();
            //print_r($this->data);
            $title_for_layout='REGISTRO DE IMPUESTO RETENIDO ';
            $mensaje = array();
            if(isset($this->data['Seleccion'])){
                    $mensaje[0] = $this->Conretencionventadet->guardar_seleccionados($this->data['Seleccion'],$this->data);
            }		
            $this->set(compact('title_for_layout','mensaje'));
    }

    public function desasociar(){
            $this->checkSession();
            //print_r($this->data);
            $title_for_layout='REGISTRO DE IMPUESTO RETENIDO ';
            $mensaje = array();
            if(isset($this->data['Seleccion'])){
                    $mensaje[0] = $this->Conretencionventadet->guardar_deseleccionados($this->data['Seleccion'],$this->data);
            }		
            $this->set(compact('title_for_layout','mensaje'));
    }

    function editarmovimientos($id=null){
            $this->checkSession();
            $data=$_POST;	
            $datos['Conretencionventadet'] = $_POST; $datos['Conretencionventa'] = $_POST;
            $this->Conretencionventadet->save($datos);
            $datos['Conretencionventa']['id']=$datos['Conretencionventa']['id_ret'];
            $this->Conretencionventa->save($datos);	
            $noasociado = $this->Conretencionventadet->listado($data['opcion'],$data);
            $variables = $this->Conretencionventadet->getVariables($data);
            $this->set(compact('noasociado','variables'));
    $this->render('buscarlistado', 'ajax');

    }

    function cerrar($id = null) {
            $this->checkSession();
            $mensaje = $this->Retgenventa->closeDeclaracionActiva($id);
            $this->set(compact('mensaje'));
    }

    function imprimir($id = null) {
            $this->checkSession();
            $this->layout = 'pdf';
            $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id=1'));
            //list($codcuadre,$codsucursal) = explode('-',$id);
            $retgenventa =$this->Retgenventa->read(null, $id);
            $encabezado['line2'] = 'IMPUESTO RETENIDO IVA 75%';
            $encabezado['line3']=$this->nombre_mes($retgenventa['Retgenventa']['mes']).'-'.$retgenventa['Retgenventa']['anio'];
            $encabezado['titulo_reporte']="";
            $data['retgenventa_id'] = $retgenventa['Retgenventa']['id'];
            $asociado = $this->Conretencionventadet->listado('asociado',$data);

            $this->set(compact('retgenventa','asociado','empresa','encabezado'));
            $this->render('imprimir');
    }
	
}
?>
