<?php
class PanelesController extends AppController {

	public $name = 'Paneles';
	public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Usuario','Funcione','Grupo','Configuracion','Empleado','Perfile','Venta','Conretencionventadet','Concuadrediario','Dia');
  /*  public $paginate = array('limit' => 20,
						'order' => array('Modulo.nombre' => 'asc')
    );*/

//-----------------------Seguridad--------------------------------------
	public function beforeFilter(){
		$this->checkSession();
		$this->datosEmpleado=$this->Session->read('Empleado.nombre');
		$this->set('datos_empleado',$this->datosEmpleado);
		$this->set('datos_paneles',$this->Session->read('Perfil'));
		$this->set('datos_menu',$this->menuPrincipal($this->Session->read('Usuario.perfil_id')));
		$this->lista_permiso($this->params['controller'],$this->params['action']);
	}	

	public function lista_permiso($controlador,$accion){
		$datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));			
		$encontrado=false;		
		foreach ($datos_lista as $registro){
			$registro['Funcione']['direccion']=$registro['Funcione']['direccion'].'/';
			list($contr,$acc)=explode('/',$registro['Funcione']['direccion']);
			if ($acc=='') $acc='index';
			if ( $contr.'/'.$acc==$controlador.'/'.$accion){
				$encontrado=true;		
			}
		}
		if ($encontrado==false){
			$this->Session->setFlash(' Usted no tiene permiso para ver esta funcionalidad.');
      			$this->redirect('/usuarios/home');
		}
	}
	
	public function menuPrincipal($id){
		return $this->Configuracion->menu($id);
	}
//----------------------------------------------------------------------
	public function administrador() {
		$title_for_layout='PANEL PRINCIPAL';
		$NroUsuario = $this->Usuario->obtenerNroUsuario();
		$ultimaFechaPago = $this->Venta->ultimaFechaPago();
		$ultimaNroPago = $this->Venta->obtenerNroPago();
		$NroRetSinAsociar = $this->Conretencionventadet->obtenerNroRetenciones();
		$NroCuadreConMovPend = $this->Concuadrediario->obtenerNroCuadreMovPendientes();
		$registrosVentas = $this->Concuadrediario->obtenerVentasConsolidadas();
		$dias = $this->Dia->find('all',array('conditions'=>'Ayo=2018'));
		$this->set(compact('title_for_layout','NroUsuario','ultimaFechaPago','ultimaNroPago','NroRetSinAsociar','NroCuadreConMovPend','registrosVentas','dias'));
	}

}
?>
