<?php
class TipodocumentosController extends AppController
{
    public $name = 'Tipodocumentos';
	public $helpers = array('Pagination'/*,'Ajax','fpdf','Javascript'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Tipodocumento','Usuario','Configuracion','Perfile','Documento');
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
	public function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Oficina Invalida', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('unidade', $this->Tipodocumento->read(null, $id));
	}

	public function add() {
	
		if (!empty($this->data)) {
			//$this->Unidade->create();
			if ($this->Tipodocumento->save($this->data)) {
				$this->flash('El Tipo de Documento ha sido Guardado.','/tipodocumentos');
			} else {
				$this->Session->setFlash(__('El Tipo de Documento no se puede guardar.', true));
			}
		}
	}

	public function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Tipodocumento Invalida', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Tipodocumento->save($this->data)) {
				$this->flash('El Tipo de Documento ha sido Guardado.','/tipodocumentos');
			} else {
				$this->Session->setFlash(__('El Tipo de Documento no se puede guardar.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Tipodocumento->read(null, $id);
		}
	}

	public function buscarhijos($id){
		$this->set('data_perfiles', $this->Oficina->find('all',array('conditions'=>' Oficina.familia='.$id)));
		$this->render('buscarhijos', 'ajax');
	}
		
	public function delete($id){
		 $nro_registro=$this->Documento->find('count',array('conditions'=>' Documento.tipodocumento_id='.$id));
		 $mensaje = $this->Tipodocumento->eliminar($nro_registro,$id);
		 $this->flash($mensaje,'/tipodocumentos');			
	}
	
		
	public function buscar($id=NULL) {

//		$criteria=" Perfile.descripcion ~* '^".$id."'";//postgres
		$criteria=" Perfile.descripcion like '".$id."%'";
		if ($id==""){

		$this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/perfiles');
		 $data=array();
		}else{
        $data = $this->Perfile->find('all',array('conditions'=>$criteria)); // Extra parameters added                

	        if(count($data)<=0){
	   			$this->flash('No se ha conseguido Registro. Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/perfiles');
	        }
		}
        $this->set('data',$data);
		$this->render('buscar', 'ajax');	
	}
//-------------------Pruebas con Arboles ---------------------------	
// Función para organizar las categorías

}

?>
