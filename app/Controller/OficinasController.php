<?php
class OficinasController extends AppController
{
    public $name = 'Oficinas';
	public $helpers = array('Pagination'/*,'Ajax','fpdf','Javascript'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Oficina','Usuario','Configuracion','Perfile','Empresa');
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function index() {
            $this->Oficina->recursive = 0;
            //$arbol_oficina = $this->Oficina->generatetreelist(null, null, '{n}.Oficina.descripcion', '&nbsp;&nbsp;&nbsp;');
            //$this->set('arbol_oficina', $arbol_oficina);
            $params = array(
    'recursive' => -1,
    'fields' => 'Oficina.id, Oficina.siglas, Oficina.descripcion, Oficina.modified, Oficina.parent_id',
            );
            $datas  = $this->Oficina->find('threaded',$params);
            //$datas  = $this->Oficina->generatetreelist(null, null, '{n}.Oficina.descripcion', '&nbsp;&nbsp;&nbsp;');
            //debug($categories);
            $this->set(compact('datas',$this->paginate('Oficina'))); 
    }

    public function view($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Oficina Invalida', true));
                    $this->redirect(array('action' => 'index'));
            }
            $this->set('unidade', $this->Oficina->read(null, $id));
    }

    public function add() {

            $this->set('empresa', $this->Empresa->llenar_combo('',0));
            $this->set('data_perfil', $this->Oficina->find('all',array('conditions'=>' Oficina.parent_id=0')));
            $this->set('data_oficina', $this->Oficina->find('all',array('conditions'=>' Oficina.familia=0','recursive'=>-1)));
            if (!empty($this->data)) {
                    //$this->Unidade->create();
                    if ($this->Oficina->save($this->data)) {
                            $this->flash('La Oficina ha sido Guardado.','/oficinas');
                    } else {
                            $this->Session->setFlash(__('La Oficina no se puede guardar.', true));
                    }
            }
    }

    public function edit($id = null) {
            if (!$id && empty($this->data)) {
                    $this->Session->setFlash(__('Oficina Invalida', true));
                    $this->redirect(array('action' => 'index'));
            }
            if (!empty($this->data)) {
                    if ($this->Oficina->save($this->data)) {
                            $this->flash('La Oficina ha sido Guardado.','/oficinas');
                    } else {
                            $this->Session->setFlash(__('La Oficina no se puede guardar.', true));
                    }
            }
            if (empty($this->data)) {
                    $this->set('data_perfil', $this->Oficina->find('all',array('conditions'=>' Oficina.parent_id=0')));
                    $this->set('data_oficina', $this->Oficina->find('all',array('recursive'=>-1)));
                    $this->set('empresa', $this->Empresa->llenar_combo('',0));
                    //$this->set('data_oficina', $this->Oficina->generatetreelist(null, '{n}.Oficina.id', '{n}.Oficina.descripcion', '-->'));
                    //$parents = $this->Oficina->getpath(1);
                    $parents = array();
                    //$this->set('parents',$parents); 
                    $this->data = $this->Oficina->read(null, $id);
            }
    }

    public function buscarhijos($id){
            $this->set('data_perfiles', $this->Oficina->find('all',array('conditions'=>' Oficina.familia='.$id)));
            $this->render('buscarhijos', 'ajax');
    }	

    public function delete($id){		
            if ($this->Oficina->delete($id)){
                    /*$this->Session->setFlash(' La Oficina con el id: '.$id.' ha sido Eliminado.');
                    $this->redirect('/oficinas');*/				
                    $this->flash('La Oficina con el id: '.$id.' ha sido Eliminado.', '/oficinas');
            }						
    }	

    public function buscar($id=NULL) {
//		$criteria=" Perfile.descripcion ~* '^".$id."'";//postgres
            $criteria=" Perfile.descripcion like '".$id."%'";
            if ($id==""){

            $this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/perfiles');
             $data=array();
            }else{
    $data = $this->Perfile->find('all',array('conditions'=>$criteria)); // Extra parameters added                
            if(count($data)<=0){
                            $this->flash('No se ha conseguido Registro. Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/perfiles');
            }
            }
    $this->set('data',$data);
            $this->render('buscar', 'ajax');	
    }
}
?>
