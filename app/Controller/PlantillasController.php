<?php
class PlantillasController extends AppController
{
    public $name = 'Plantillas';
	public $helpers = array('Pagination','tcpdf','fpdf');
	public $components = array ('Pagination'); 
    public $uses = array ('Plantilla','Grupo','Configuracion','Funcione','Perfile','Elemento','Seccione','Marco','Esquema','Contenido','Documento','Empresa');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------	
	public function index(){
		$this->checkSession();
        $this->Plantilla->recursive = 0;
		$this->set('data', $this->paginate());          
	}
	
	public function view($id){
		$this->checkSession();
		//$this->Plantilla->read(NULL,$id)
		$criteria = 'Plantilla.id='.$id;
		$data = $this->Plantilla->find('first',array('conditions'=>$criteria,'recursive'=>2));		
		$this->set('data', $data);
	}
	
	public function add(){
		$this->checkSession();
		if(!empty($this->data)){
			if ($this->Plantilla->save($this->data)){
				$this->flash('La Plantilla ha sido Guardado.','/plantillas');
			}else{
				$this->set('data', $this->data);
				$this->render('add');
			}
		}		
		$this->set('nombreProyecto', $this->nombreProyecto);
	}
	
	public function edit($id=null){
		$this->checkSession();
		$cabecera = $this->Elemento->llenar_combo('','Encabezado');
		$seccion = $this->Seccione->llenar_combo('',0);
		$pie = $this->Elemento->llenar_combo('','Pie_Pagina');
		
		$this->set(compact('cabecera','pie','seccion'));
		if (empty($this->data)){
			$this->data =  $this->Plantilla->read(NULL,$id);
			$this->Session->write('Enc_'.$id, $this->Marco->cargarMarco($id,'Encabezado') );
			$this->Session->write('Pie_'.$id, $this->Marco->cargarMarco($id,'Pie_Pagina') );
			$this->Session->write('Sec_'.$id, $this->Esquema->cargarEsquema($id) );
		}else{
			$datos=$this->data;$valorControl=$datos['Plantilla']['id'];
			if ($this->Plantilla->save($datos)){
				$data_enc=$this->Session->read('Enc_'.$valorControl);
				$data_pie=$this->Session->read('Pie_'.$valorControl);
				$data_sec=$this->Session->read('Sec_'.$valorControl);
				if ($this->Marco->guardarMarco($valorControl,$data_enc,'Encabezado')){ 
					$this->Session->write('Enc_'.$valorControl, array());
				}
				if ($this->Marco->guardarMarco($valorControl,$data_pie,'Pie_Pagina')){ 
					$this->Session->write('Pie_'.$valorControl, array());
				}
				if ($this->Esquema->guardarEsquema($valorControl,$data_sec)){ 
					$this->Session->write('Sec_'.$valorControl, array());
				}
				$this->flash('La Plantilla ha sido Actualizado.','/plantillas');
			}else{
				$this->set('data', $this->data);
			}
		}
		$this->set('nombreProyecto', $this->nombreProyecto);
	}
	
	public function delete($id){
		$this->checkSession();
		$nro = $this->Documento->find('count',array('conditions'=>' Documento.plantilla_id='.$id));
		if ($nro>0){
      		$this->flash('La Plantilla con el id: '.$id.' no puede ser Eliminado porque tiene movimientos.','/plantillas');
		}else{
			if ($this->Plantilla->delete($id)){
      			$this->flash('La Plantilla con el id: '.$id.' ha sido Eliminado.','/plantillas');				
			}		
		}
	}

	public function buscar($id=NULL) {
		$criteria=" upper(Plantilla.nombre) like upper('".$id."%')";
		if ($id==""){
			$this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/plantillas');
			 $data=array();
		}else{
			$data = $this->Plantilla->find('all',array('conditions'=>$criteria));  
		}
        $this->set('data',$data);
		$this->render('buscar', 'ajax');	
	}
	
	public function eliminardetalle($codigo=null){
		list($opcion,$id)=explode('-',$codigo);
		switch ($opcion) {
			case 'Encabezado'://Guarda los Detalles de Encabezados
				$data_enc=$this->Session->read('Enc_'.$_POST['valorControl']);
				list($data_enc,$mensaje)=$this->Elemento->eliminartemp($id,$data_enc);
				$this->Session->write('Enc_'.$_POST['valorControl'], $data_enc);			
				$valorControl=$_POST['valorControl'];
				$this->set(compact('data_enc','mensaje','valorControl'));
				$this->render('guardardetallec', 'ajax');
			break;
			case 'Seccion'://Guarda los Detalles de las Secciones
				$data_enc=$this->Session->read('Sec_'.$_POST['valorControl']);
				list($data_enc,$mensaje)=$this->Seccione->eliminartemp($id,$data_enc);		
				$this->Session->write('Sec_'.$_POST['valorControl'], $data_enc);	
				$valorControl=$_POST['valorControl'];
				$this->set(compact('data_enc','mensaje','valorControl'));
				$this->render('guardardetalles', 'ajax');			
			break;
			case 'Pie_Pagina'://Guarda los Detalles del Pie de Pagina
				$data_enc=$this->Session->read('Pie_'.$_POST['valorControl']);				
				list($data_enc,$mensaje)=$this->Elemento->eliminartemp($id,$data_enc);
				$this->Session->write('Pie_'.$_POST['valorControl'], $data_enc);
				$valorControl=$_POST['valorControl'];				
				$this->set(compact('data_enc','mensaje','valorControl'));
				$this->render('guardardetallep', 'ajax');
			break;
		}
	
	}
	
	public function guardardetalle($opcion=''){
		
		switch ($opcion) {
			case 'Encabezado'://Guarda los Detalles de Encabezados
				$data_enc=$this->Session->read('Enc_'.$_POST['valorControl']);
				list($data_enc,$mensaje)=$this->Elemento->guardartemp($_POST,$data_enc);
				$this->Session->write('Enc_'.$_POST['valorControl'], $data_enc);
				$valorControl=$_POST['valorControl'];
				$this->set(compact('data_enc','mensaje','valorControl'));
				$this->render('guardardetallec', 'ajax');
			break;
			case 'Seccion'://Guarda los Detalles de las Secciones
				$data_enc=$this->Session->read('Sec_'.$_POST['valorControl']);
				list($data_enc,$mensaje)=$this->Seccione->guardartemp($_POST,$data_enc);
				$this->Session->write('Sec_'.$_POST['valorControl'], $data_enc);
				$valorControl=$_POST['valorControl'];
				$this->set(compact('data_enc','mensaje','valorControl'));
				$this->render('guardardetalles', 'ajax');
			
			break;
			case 'Pie_Pagina'://Guarda los Detalles del Pie de Pagina
				$data_enc=$this->Session->read('Pie_'.$_POST['valorControl']);
				list($data_enc,$mensaje)=$this->Elemento->guardartemp($_POST,$data_enc);
				$this->Session->write('Pie_'.$_POST['valorControl'], $data_enc);
				$valorControl=$_POST['valorControl'];
				$this->set(compact('data_enc','mensaje','valorControl'));
				$this->render('guardardetallep', 'ajax');
			break;
		}
	
	}
	
	public function viewpdf($id){
		$this->checkSession();
		$this->layout = 'pdf';		
		$empresa = $this->Empresa->find('first',array('conditions'=>''));
		$data = $this->Plantilla->getPlantilla('Plantilla.id='.$id);

		list($secciones,$texto)=$this->Contenido->cargarContenido($data['Esquema']);		
		//cargarContenido
		$this->set(compact('data','texto','empresa'));
	}
	
	public function viewpdf1($id){
		$this->checkSession();
		$this->layout = 'pdf';
		//--------------------------------------------------------------
		$this->set('titulo', '');
		$this->set('logoIzq', 'logo.png');
		$this->set('logoDer', 'logo.png');
		$this->set('line1','SISTEMA DE GESTION');
		$this->set('line2','');
		$this->set('line3','');
		$this->set('line4','');
		//--------------------------------------------------------------
			
		$criteria = 'Plantilla.id='.$id;
		$data = $this->Plantilla->find('first',array('conditions'=>$criteria,'recursive'=>2));
		list($secciones,$texto)=$this->Contenido->cargarContenido($data['Esquema']);
		$fecha = array('posHoja'=>'P');		
		//cargarContenido
		$this->set(compact('data','texto','fecha'));
	}	

}
?>
