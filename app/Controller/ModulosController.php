<?php
class ModulosController extends AppController {

    public $name = 'Modulos';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Modulo','Funcione','Grupo','Configuracion','Empleado','Perfile');
    public $paginate = array('limit' => 20,
                            'order' => array('Modulo.nombre' => 'asc')
    );

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function index() {
            $this->Modulo->recursive = 0;
            $this->set('unidades', $this->paginate());
    }

    public function view($id = null){
            if (!$id) {
                    $this->Session->setFlash(__('Modulo Invalido', true));
                    $this->redirect(array('action' => 'index'));
            }
            $this->set('unidade', $this->Modulo->read(null, $id));
    }

    public function add() {
            if (!empty($this->data)) {
                    if ($this->Modulo->save($this->data)) {
                            $this->flash('El Modulo ha sido Guardado.','/modulos');
                    } else {
                            $this->Session->setFlash(__('El Modulo no se puede guardar.', true));
                    }
            }
            $this->set('nombreProyecto', $this->nombreProyecto);		
    }

    public function edit($id = null) {
            if (!$id && empty($this->data)) {
                    $this->Session->setFlash(__('Modulo Invalido', true));
                    $this->redirect(array('action' => 'index'));
            }
            if (!empty($this->data)) {
                    if ($this->Modulo->save($this->data)) {
                            $this->flash('El Modulo ha sido Guardado.','/modulos');
                    } else {
                            $this->Session->setFlash(__('El Modulo no se puede guardar.', true));
                    }
            }
            if (empty($this->data)) {
                    $this->data = $this->Modulo->read(null, $id);
            }
            $this->set('nombreProyecto', $this->nombreProyecto);		
    }

    public function delete($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Invalid id for unidade', true));
                    $this->redirect(array('action'=>'index'));
            }
            if ($this->Modulo->delete($id)) {
                    $this->flash('El Modulo ha sido Eliminado.','/modulos');
            }
            $this->flash('El Modulo ha no sido Eliminado.','/modulos');
    }
}
?>
