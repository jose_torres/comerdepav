<?php 
//vendor('phpcaptcha'.DS.'php-captcha.inc');
App::import('Vendor','PhpCaptcha' ,array('file'=>'phpcaptcha/php-captcha.inc.php')); 

class CaptchaComponent extends Component
{
    var $controller;
 
    function startup(Controller $controller ) {
        $this->controller = &$controller;
    }

    function image(){
        
//        $imagesPath = realpath(VENDORS . 'phpcaptcha').'/fonts/';
 	$imagesPath = APP . 'vendors' . DS . 'phpcaptcha'.'/fonts/';        

        $aFonts = array(
            $imagesPath.'VeraBd.ttf',
            $imagesPath.'VeraIt.ttf',
            $imagesPath.'Vera.ttf'
        );
        
        $oVisualCaptcha = new PhpCaptcha($aFonts, 200, 50);
        $oVisualCaptcha->UseColour(false);
//        $oVisualCaptcha->SetOwnerText('Fuente: lev');
//	$oVisualCaptcha->DisplayShadow(true);
//	$oVisualCaptcha->SetBackgroundImages(IMAGES.'captcha2.jpg');
        $oVisualCaptcha->SetNumChars(6);
        $oVisualCaptcha->Create();
    }
    
    function audio(){
        $oAudioCaptcha = new AudioPhpCaptcha('/usr/bin/flite', '/tmp/');
        $oAudioCaptcha->Create();
    }
    
    function check($userCode, $caseInsensitive = true){
        if ($caseInsensitive) {
            $userCode = strtoupper($userCode);
        }

        if (!empty($_SESSION[CAPTCHA_SESSION_ID]) && $userCode == $_SESSION[CAPTCHA_SESSION_ID]) {
            // clear to prevent re-use
            unset($_SESSION[CAPTCHA_SESSION_ID]);
            
            return true;
        }
        else return false;
        
    }
}
?>
