<?php
class ProductosController extends AppController
{
    public $name = 'Productos';
    public $helpers = array('Pagination');
    public $components = array ('Pagination'); 
    public $uses = array ('Producto','Funcione','Grupo','Configuracion','Perfile','Empresa','Sucursal','Departamento','Deposito','Archivo');
    
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function index(){
        $this->checkSession();
        $mensaje[0] = '';
        $title_for_layout ='Productos';	
        $this->set(compact('title_for_layout','mensaje'));
        $this->render('index');
    }
    
    public function activar(){
        $this->checkSession();
       
        $mensaje[0] = '';
        $title_for_layout ='Activaci&oacute;n de Productos';
        $sucursal=$this->Empresa->llenar_combo('',5);
        $this->set(compact('title_for_layout','mensaje','sucursal'));
        $this->render('activar');
    }
    
    public function preciossaint(){
        $this->checkSession();
        //--------------------      
        $this->Archivo->actualizardbf($this->filesProyecto);
        $this->Archivo->eliminarProductos();
        $this->Archivo->inicializar();
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCK21.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCK21');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC1.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC1');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC2.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC2');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC3.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC3');
        //------------------
        $result = $this->Producto->actualizarSaintSiscom();
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Listado de Precios SAINT';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function auditoriasaint(){
        $this->checkSession();
        //--------------------
        $this->Archivo->actualizardbf($this->filesProyecto);
        $this->Archivo->eliminarProductos();
        $this->Archivo->inicializar();
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCK21.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCK21');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC1.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC1');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC2.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC2');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC3.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC3');
        //-----------------
        $result = $this->Producto->actualizarSaintSiscom();
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Comparaci&oacute;n de Precios SAINT';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function costosaint(){
        $this->checkSession();
        //--------------------
        $this->Archivo->actualizardbf($this->filesProyecto);
        $this->Archivo->eliminarProductos();
        $this->Archivo->inicializar();
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCK21.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCK21');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC1.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC1');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC2.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC2');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC3.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC3');
        //-----------------
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Comparaci&oacute;n de Costos SAINT';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function descripcionsaint(){
        $this->checkSession();
        //--------------------
        $this->Archivo->actualizardbf($this->filesProyecto);
        $this->Archivo->eliminarProductos();
        $this->Archivo->inicializar();
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCK21.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCK21');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC1.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC1');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC2.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC2');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC3.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC3');
        //-----------------
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Comparaci&oacute;n de Descripci&oacute;n de Productos SAINT';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function consolidadosaint(){
        $this->checkSession();
        //--------------------
        $this->Archivo->actualizardbf($this->filesProyecto);
        $this->Archivo->eliminarProductos();
        $this->Archivo->inicializar();
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCK21.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCK21');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC1.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC1');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC2.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC2');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC3.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC3');
        //-----------------
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Consolidado de Inventario SAINT';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function negativosaint(){
        $this->checkSession();
        //--------------------
        $this->Archivo->actualizardbf($this->filesProyecto);
        $this->Archivo->eliminarProductos();
        $this->Archivo->inicializar();
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCK21.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCK21');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC1.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC1');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC2.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC2');
        $fichero_dbf = $this->filesProyecto.'/dbf/STOCKC3.DBF';
        $this->Archivo->incluirProductos($fichero_dbf,'STOCKC3');
        //-----------------
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Inventario en Negativos de SAINT';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
     public function buscarinventario($id=NULL) {
            //print_r($_POST);
        $datos=$_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $dep=explode('-',$_POST['depositos']);$vercosto=explode('-',$_POST['vercosto']);
        $precio=explode('-',$_POST['precio']);
        
        $titulo='Existencia de Art&iacute;culos';
        $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
        $datos['precio']=$precio;			
        $data=$this->Archivo->listadoprecio($datos);
        $departamento=$this->Departamento->llenar_combo('',2);            
        $this->set(compact('data','titulo','data_dev','datos','departamento'));
        $this->render('buscarinvprecio', 'ajax');
        
    }
    
    public function buscarconsolidado($id=NULL) {
            //print_r($_POST);
        $datos=$_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $dep=explode('-',$_POST['depositos']);$vercosto=explode('-',$_POST['vercosto']);
        $precio=explode('-',$_POST['precio']);
        
        $titulo='Consolidado de Inventario';
        $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
        $datos['precio']=$precio;			
        $data=$this->Archivo->listadoprecio($datos);
        $departamento=$this->Departamento->llenar_combo('',2);            
        $this->set(compact('data','titulo','data_dev','datos','departamento'));
        
        $this->render('buscarconsolidado', 'ajax');
        
    }
    
    public function buscarnegativo($id=NULL) {
            //print_r($_POST);
        $datos=$_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $dep=explode('-',$_POST['depositos']);$vercosto=explode('-',$_POST['vercosto']);
        $precio=explode('-',$_POST['precio']);
        
        $titulo='Consolidado de Inventario';
        $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
        $datos['precio']=$precio;			
        $data=$this->Archivo->listadonegativo($datos);
        $departamento=$this->Departamento->llenar_combo('',2);            
        $this->set(compact('data','titulo','data_dev','datos','departamento'));
        
        $this->render('buscarnegativo', 'ajax');
        
    }
    
    public function comparar($id=NULL) {
            //print_r($_POST);
        $datos=$_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $dep=explode('-',$_POST['depositos']);$vercosto=explode('-',$_POST['vercosto']);
        $precio=explode('-',$_POST['precio']);
        
        $titulo='Comparaci&oacute;n de Producto';
        $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
        $datos['precio']=$precio;			
        $data=$this->Archivo->compararprecio($datos);
        $departamento=$this->Departamento->llenar_combo('',2);            
        $this->set(compact('data','titulo','data_dev','datos','departamento'));
        $this->render('comparar', 'ajax');
        
    }
    
    public function compararcosto($id=NULL) {
            //print_r($_POST);
        $datos=$_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $dep=explode('-',$_POST['depositos']);$vercosto=explode('-',$_POST['vercosto']);
        $precio=explode('-',$_POST['precio']);
        
        $titulo='Comparaci&oacute;n de Producto';
        $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
        $datos['precio']=$precio;			
        $data=$this->Archivo->compararcostoactual($datos);
        $departamento=$this->Departamento->llenar_combo('',2);            
        $this->set(compact('data','titulo','data_dev','datos','departamento'));
        $this->render('compararcosto', 'ajax');
        
    }
    
     public function comparardescripcion($id=NULL) {
            //print_r($_POST);
        $datos=$_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $dep=explode('-',$_POST['depositos']);$vercosto=explode('-',$_POST['vercosto']);
        $precio=explode('-',$_POST['precio']);
        
        $titulo='Comparaci&oacute;n de Producto';
        $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
        $datos['precio']=$precio;			
        $data=$this->Archivo->comparardescripcion($datos);
        $departamento=$this->Departamento->llenar_combo('',2);            
        $this->set(compact('data','titulo','data_dev','datos','departamento'));
        $this->render('comparardescripcion', 'ajax');
        
    }
    
    public function viewpdfinv($id){
        $this->checkSession();
        $this->layout = 'pdf';        			
        $datos = $this->data['Reporte'];
        
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        //Configure::write('Model.globalSource', $conexion);
        $encabezado['titulo_reporte']='Listado de Precio de Productos';
        $datos = $this->Producto->ajustarDatos('pdf',$datos);

        $data=$this->Archivo->listadoprecio($datos);
        $departamento=$this->Departamento->llenar_combo('',2); 
        $this->set(compact('data','empresa','encabezado','departamento','datos')); 
        if($datos['tiporep']=='Listado'){      
        $this->render('viewpdfinvprecio1');
		}else{
			$this->render('viewpdfinvprecio2');
		}
        				
    }
    
    public function viewpdfcosto($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        			
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        //Configure::write('Model.globalSource', $conexion);
        $encabezado['titulo_reporte']='Listado de Precio de Productos';
        $datos = $this->Producto->ajustarDatos('pdf',$datos);

        $data=$this->Archivo->compararcostoactual($datos);
        $departamento=$this->Departamento->llenar_combo('',2); 
        $this->set(compact('data','empresa','encabezado','departamento','datos'));        
        $this->render('viewpdfcosto');
        				
    }
    
    public function viewpdfcomparacion($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        			
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        $encabezado['titulo_reporte']='Listado de Precio con Diferencia de Precio';
        $datos = $this->Producto->ajustarDatos('pdf',$datos);

        $data=$this->Archivo->compararprecio($datos);
        $departamento=$this->Departamento->llenar_combo('',2); 
        $this->set(compact('data','empresa','encabezado','departamento','datos'));        
        $this->render('viewpdfcomparacion');
        				
    }
    
    public function viewpdfconsolidado($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        			
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        $encabezado['titulo_reporte']='Consolidado de Inventario';
        $datos = $this->Producto->ajustarDatos('pdf',$datos);

        $data=$this->Archivo->listadoprecio($datos);
        $departamento=$this->Departamento->llenar_combo('',2); 
        $this->set(compact('data','empresa','encabezado','departamento','datos'));        
        $this->render('viewpdfconsolidado');
        				
    }
    
    public function viewpdfnegativo($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        			
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        $encabezado['titulo_reporte']='Inventario en Negativo';
        $datos = $this->Producto->ajustarDatos('pdf',$datos);

        $data=$this->Archivo->listadonegativo($datos);
        $departamento=$this->Departamento->llenar_combo('',2); 
        $this->set(compact('data','empresa','encabezado','departamento','datos'));        
        $this->render('viewpdfnegativo');
        				
    }
    
    public function viewpdfdescripcion($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        			
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        //Configure::write('Model.globalSource', $conexion);
        $encabezado['titulo_reporte']='Listado de Productos con Diferencia de Descripcion';
        $datos = $this->Producto->ajustarDatos('pdf',$datos);

        $data=$this->Archivo->comparardescripcion($datos);
        $departamento=$this->Departamento->llenar_combo('',2); 
        $this->set(compact('data','empresa','encabezado','departamento','datos'));        
        $this->render('viewpdfdescripcion');
        				
    }

    public function activacion(){
        $datos = $_POST;
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
	Configure::write('Model.globalSource', $conexion);
        $mensaje[0] = $this->Producto->activar();        
        $this->set(compact('mensaje','datos'));
        $this->render('activacion', 'ajax');
    }
    
    public function actualizar(){
        $this->checkSession();
        $mensaje[0] = '';
        $title_for_layout ='Actualizar Precios de Productos entre Principal a la Sucursal';
        //$sucursal=$this->Empresa->llenar_combo('',5);
        $sucursal=$this->Sucursal->llenar_combo('',0);
        $this->set(compact('title_for_layout','mensaje','sucursal'));
        $this->render('actualizar');
    }
    
    public function preciosenlote(){
        $this->checkSession();
        $mensaje[0] = '';
        $title_for_layout ='Actualizar Precios de Productos en Lotes';
        //$sucursal=$this->Empresa->llenar_combo('',5);
        $sucursal=$this->Sucursal->llenar_combo('',0);
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',1);
        $this->set(compact('title_for_layout','mensaje','sucursal','producto','departamento'));
        $this->render('preciosenlote');
    }
    
    public function actualizarprecios(){
        $datos = $_POST;
        $sucursal = $this->Sucursal->read(null,$datos['sucursal']);
        list($nro,$mensaje[0]) = $this->Producto->nroproductosnvos($sucursal);
        $mensaje[1] = $this->Producto->transferirproductos($sucursal);
        list($nro,$mensaje[2]) = $this->Producto->nroproductoscambiados($sucursal);
        $mensaje[3] = $this->Producto->actualizarproductos($sucursal);
        $this->set(compact('mensaje','datos'));
        $this->render('actualizarprecios', 'ajax');
    }
    
    public function listadoproducto(){
        $datos = $_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $datos['prod']=$prod;$datos['depto']=$depto;       
        $data=$this->Producto->reporte($datos,'',3);
        $this->set(compact('data','datos'));
        $this->render('listadoproducto', 'ajax');
    }

    public function guardarmarcados($id=0){
        $this->checkSession();
        $mensaje[0] = '';
        if(isset($this->data['Seleccionj'])){
            $mensaje[0] = $this->Dia->guardar_seleccionados($this->data['Seleccionj']);
        }		
        $title_for_layout ='Dias Feriados';	
        $this->set(compact('title_for_layout','mensaje'));
        $this->render('index');
    }

}
?>
