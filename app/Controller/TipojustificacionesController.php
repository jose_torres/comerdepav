<?php
class TipojustificacionesController extends AppController
{
    public $name = 'Tipojustificaciones';
	public $helpers = array('Pagination'/*,'Ajax','fpdf','Javascript'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Tipojustificacione','Usuario','Configuracion','Perfile','Documento');
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------

	public function index() {
		$this->Tipojustificacione->recursive = 0;
		$datas  = $this->paginate('Tipojustificacione');
		$this->set(compact('datas')); 		
	}

	public function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Oficina Invalida', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('unidade', $this->Tipojustificacione->read(null, $id));
	}

	public function add() {	
		if (!empty($this->data)) {
			list($mensaje,$url,$verificacion)=$this->Tipojustificacione->actualizar($this->data,'add');
			$this->flash($mensaje,$url);
		}
		$acciones=$this->Tipojustificacione->obtener_acciones();
		$this->set(compact('acciones'));
	}

	public function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Tipodocumento Invalida', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			list($mensaje,$url,$verificacion)=$this->Tipojustificacione->actualizar($this->data,'edit');
			$this->flash($mensaje,$url);
		}
		if (empty($this->data)) {
			$this->data = $this->Tipojustificacione->read(null, $id);
			$acciones=$this->Tipojustificacione->obtener_acciones();
			$this->set(compact('acciones'));
		}
	}

	public function buscarhijos($id){
		$this->set('data_perfiles', $this->Oficina->find('all',array('conditions'=>' Oficina.familia='.$id)));
		$this->render('buscarhijos', 'ajax');
	}
		
	public function delete($id){
		 $nro_registro=$this->Documento->find('count',array('conditions'=>' Documento.tipojustificacione_id='.$id));
		 $mensaje = $this->Tipojustificacione->eliminar($nro_registro,$id);
		 $this->flash($mensaje,'/tipojustificaciones');			
	}
	
		
	public function buscar($id=NULL) {

//		$criteria=" Perfile.descripcion ~* '^".$id."'";//postgres
		$criteria=" Perfile.descripcion like '".$id."%'";
		if ($id==""){

		$this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/perfiles');
		 $data=array();
		}else{
        $data = $this->Perfile->find('all',array('conditions'=>$criteria)); // Extra parameters added                

	        if(count($data)<=0){
	   			$this->flash('No se ha conseguido Registro. Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/perfiles');
	        }
		}
        $this->set('data',$data);
		$this->render('buscar', 'ajax');	
	}
//-------------------Pruebas con Arboles ---------------------------	
// Función para organizar las categorías

}

?>
