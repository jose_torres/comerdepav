<?php
class EmpleadosController extends AppController
{
    public $name = 'Empleados';
	public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Empleado','Funcione','Grupo','Configuracion','Empleado','Perfile');
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function index(){
        $this->checkSession();
        $this->Empleado->recursive = 0;
        $this->set('data', $this->paginate()); 
    }

    public function view($id){
        $this->set('data',$this->Empleado->read(null,$id));
    }

    public function add(){
        $oficinas = $this->Empleado->Oficina->find('list',array('fields'=>'descripcion'));
        $this->set(compact('oficinas'));
        if (!empty($this->data)){
            if ($this->Empleado->save($this->data)){
                $this->flash('La Datos del Empleados ha sido Guardada.','/empleados');
            }else{
                $this->set('data', $this->data);
                $this->render('add');
            }
        }
    }

    public function edit($id=null){
        if (empty($this->data)){
            $oficinas = $this->Empleado->Oficina->find('list',array('fields'=>'descripcion'));
            $this->set(compact('oficinas'));
            $this->data = $this->Empleado->read(null,$id);
        }else{
            if ( $this->Empleado->save($this->data)){
                $this->flash('Los Datos del Empleado ha sido Actualizada.','/empleados');
            }else{
                $this->set('data', $this->data);				
            }
        }
    }

    public function delete($id){
            if ($this->Empleado->delete($id)){
                    $this->flash('El Empleado con el id: '.$id.' ha sido eliminado.', '/empleados');
            }
    }

    public function buscar($id=NULL) {

            $criteria=" cast(Empleado.cedula as text) like '".$id."%' ";
            if ($id==""){
                    $this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/empleados');
                     $data=array();
            }else{
            $data = $this->Empleado->find('all',array('conditions'=>$criteria)); // Extra parameters added                  
            if(count($data)<=0){
                            $this->flash('No se ha conseguido Registro. Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/empleados');
            }
            }
    $this->set('data',$data);
            $this->render('buscar', 'ajax');	
    }

}
?>
