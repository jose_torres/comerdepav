<?php
class UsuariosController extends AppController
{
    public $name = 'Usuarios';
    public $components = array ('Session','Pagination','Captcha');   
    public $helpers = array('Js','Pagination');
    public $uses = array('Usuario','Perfile','Configuracion','Empleado');

/*-------------Funciones Captcha------------------------------------*/	
	public function captcha_image()
	{
		$this->Captcha->image();
	}

	public function validateCaptcha($fieldName, $params,$modelo){
		$caseInsensitive = true;
//		$val = $this->data[$this->name][$fieldName];		
		$val = $this->data[$modelo][$fieldName];	
//		echo $val;
		if ($caseInsensitive) {
		    $val = strtoupper($val);
		}
		//php-captcha.inc.php
		if(!defined('CAPTCHA_SESSION_ID'))
		    define('CAPTCHA_SESSION_ID', 'php_captcha');
 
		 if (!empty($_SESSION[CAPTCHA_SESSION_ID]) && $val == $_SESSION[CAPTCHA_SESSION_ID]) {
		    // clear to prevent re-use
	
		    unset($_SESSION[CAPTCHA_SESSION_ID]);
		    
		    return true;
		 }
		 
		 return false;
	    } 		    
	public function captcha_audio()
	{
		$this->Captcha->audio();
	} 
/*------------------------------------------------------------------*/
	public function menuPrincipal()
	{
		$this->datosEmpleado=$this->Session->read('Empleado.nombre');
		$this->set('datos_empleado',$this->datosEmpleado);
		$this->datosProyecto=$this->Session->read('Proyecto');
		$this->set('datos_proyecto',$this->datosProyecto);
		return $this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
	}

	public function lista_permiso($controlador,$accion){
		$datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));		
		$encontrado=false;		
		foreach ($datos_lista as $registro){
			$registro['Funcione']['direccion']=$registro['Funcione']['direccion'].'/';
			list($contr,$acc)=explode('/',$registro['Funcione']['direccion']);
			//echo $contr.'->'.$acc.'<br>';
			if ($acc=='') $acc='index';
			if ( $contr.'/'.$acc==$controlador.'/'.$accion){
				$encontrado=true;		
			}
		}
		if ($encontrado==false){
			$this->Session->setFlash(' Usted no tiene permiso para ver esta funcionalidad.');
      			$this->redirect('/usuarios/home');
		}

	}

	public function listadoFuncion($id){
		$config=$this->Configuracion->find('all',array('conditions'=>'Configuracion.perfil_id='.$id));
		foreach($config as $reg){
			$func[$reg['Configuracion']['id']]=$reg['Funcione']['direccion'];
		}
		$this->Session->write('fun',$func);		
	}
	
	public function panelControl(){
		return $this->Configuracion->panel($this->Session->read('Usuario.perfil_id'));
	}

	public function listado(){
		$this->checkSession();
		$this->set('datos_menu',$this->menuPrincipal());	
		$perfil=$this->Usuario->find(' Usuario.id='.$this->Session->read('Usuario.id'));
		$criterio=$this->construircriterio($perfil['Proyecto'],' Proyecto.id ','id','and','or');

		$criteria=$this->construircriterio($perfil['Proyecto'],' Proyectousuario.proyecto_id ','id','','or');
        list($order,$limit,$page) = $this->Pagination->init($criteria,NULL,array('modelClass'=>'Proyectousuario','sortBy'=>'usuario_id')); // Added

        $data2 = $this->Proyectousuario->findAll($criteria, NULL, $order, $limit, $page); // Extra parameters added        
        $this->set('data',$data2);			

	}
	public function index()
	{

		$this->checkSession();
		$this->lista_permiso($this->request->params['controller'],$this->request->params['action']);
		$this->set('datos_menu',$this->menuPrincipal());	
//------- Validacion de Existe Usuario -------------------		
		$this->listadoFuncion($this->Session->read('Usuario.perfil_id'));
		$clave = array_search('usuarios', $this->Session->read('fun'));
		if ($clave==false) $this->redirect('usuarios/listado');	
//-----------------------------------------------------------	
		$this->Usuario->recursive = 0;
		$this->set('data', $this->paginate());
	}
	
	public function view($id)
	{
		$this->checkSession();
		$this->lista_permiso($this->params['controller'],$this->params['action']);		
		$this->set('datos_menu',$this->menuPrincipal());
		$this->set('data', $this->Usuario->read(null,$id));
	}

	public function buscarhijos($id){
		$perfil=$this->Usuario->find(' Usuario.id='.$this->Session->read('Usuario.id'));		
		$datos=$this->Perfile->find(' Perfile.id='.$id);
		$this->render('buscarhijos', 'ajax');
	}
		
	public function add(){
		$this->checkSession();
		$this->lista_permiso($this->request->params['controller'],$this->request->params['action']);
		
		$this->set('datos_menu',$this->menuPrincipal());
		$this->set('data_perfiles', $this->Perfile->find('all',array('recursive'=>-1)));
		$this->set('data_empleado', $this->Empleado->find('all',array('order' =>'Empleado.cedula ','recursive'=>-1) ));
		$this->set('data_perfil', $this->Perfile->find('all',array('recursive'=>-1)));
		if (empty($this->request->data)){
			$this->render();
		}else{
//			$this->params['data']['Listado']=$this->params['data']['Usuario'];
			if ($this->Usuario->save($this->request->data)){
				$this->Session->setFlash('El Usuario ha sido Guardado.');
      			$this->redirect('/usuarios');
			}else{
				$this->set('data', $this->request->data);
				$this->render('edit');
			}
		}
	}
	
	public function edit($id=null)
	{
		$this->checkSession();
		$this->lista_permiso($this->params['controller'],$this->params['action']);		
		$this->set('datos_menu',$this->menuPrincipal());

//------- Validacion de Existe Usuario -------------------		
		$this->listadoFuncion($this->Session->read('Usuario.perfil_id'));
		$clave = array_search('usuarios', $this->Session->read('fun'));
		if ($clave==false){
			$clave = array_search('usuarios/listado', $this->Session->read('fun'));
			if ($clave==false){
				$this->Session->setFlash(' No tiene Permiso para Actualizar este Registro.');
				$this->redirect('usuarios/home');
				}
		}else{

		} 	
//-----------------------------------------------------------			
		$this->set('data_perfiles', $this->Perfile->find('all',array('recursive'=>-1)));
		$this->set('data_empleado', $this->Empleado->find('all',array('order' =>'Empleado.cedula ','recursive'=>-1) ));
		$this->set('data_perfil', $this->Perfile->find('all',array('recursive'=>-1)));
		if (empty($this->request->data)){
			$this->data = $this->Usuario->read(null,$id);		
			$this->render();
		}else{
			
			if ( $this->Usuario->save($this->request->data)){
				$this->Session->setFlash('El Usuario ha sido Actualizado.');
      			$this->redirect('/usuarios');
			}else{
				$this->set('data', $this->request->data);
				$this->render();
			}
		}
	}
	
	public function cambiocontrasena($id=NULL){
		$this->lista_permiso($this->params['controller'],$this->params['action']);
		$this->checkSession();
		$this->set('datos_menu',$this->menuPrincipal());
		$this->set('datos_proyecto',$this->Perfile->find(' Perfile.id='.$this->Session->read('Usuario.perfil_id')));
		$id=$this->Session->read('Usuario.id');
		$perfil=$this->Perfile->find(' Perfile.id='.$this->Session->read('Usuario.perfil_id'));
		$criterio=$this->construircriterio($perfil['Proyecto'],' Proyecto.id ','id','and','or');
//		echo $criterio;
		$this->set('data_perfiles', $this->Proyecto->perfilesproyecto($criterio));		
		$this->set('data_empleado', $this->Empleado->findAll());
		$this->set('data_perfil', $this->Perfile->findAll());
		if (empty($this->params['data']))
		{
			$this->Usuario->setId($id);
			$this->params['data'] = $this->Usuario->read();
			$perfil=$this->Usuario->find(' Usuario.id='.$this->Session->read('Usuario.id'));
			// app_controller
			$criterio=$this->construircriterio($perfil['Proyecto'],' Proyecto.id ','id','and','or');
			$datos=$this->Perfile->find(' Perfile.id='.$this->params['data']['Usuario']['perfil_id']);
	//		print_r($datos);
			$this->set('data_perfiles2', $this->Proyecto->usuariosproyecto($datos['Perfile']['id_padre'],$criterio));				
			
			$this->render();
		}
		else
		{
			if ( $this->Usuario->save($this->params['data']))
			{
				$this->flash('El Usuario ha sido Actualizado.','/usuarios');
			}
			else
			{
				$this->set('data', $this->params['data']);
					$this->render();
				}
			}		
		}
		
	public function delete($id){
		$this->checkSession();
		$this->lista_permiso($this->params['controller'],$this->params['action']);			
//------- Validacion de Existe Usuario -------------------		
		
//-----------------------------------------------------------				
		if ($this->Usuario->delete($id)){
			$this->Session->setFlash('El Usuario con el id: '.$id.' ha sido Eliminado.');
      		$this->redirect('/usuarios');
		}else{
			$this->Session->setFlash('El Usuario con el id: '.$id.' NO ha sido Eliminado.');
      		$this->redirect('/usuarios');
		}
	}

	public function home(){
		$this->checkSession();
		$data_menu=$this->menuPrincipal();
		//$datos_menu['grupo']=$data_menu
		$this->set('datos_menu',$this->menuPrincipal());		
		$this->set('datos_panel',$this->panelControl());
		$this->datosEmpleado=$this->Session->read('Empleado.nombre');
		$this->set('datos_empleado',$this->datosEmpleado);
		$this->datosPerfil=$this->Session->read('Perfil.descripcion');
		$this->set('datos_perfil',$this->datosPerfil);
	}

	public function cambiopassword($id=null){
		$this->checkSession();
		$this->lista_permiso($this->params['controller'],$this->params['action']);
		$this->set('datos_lista',$this->params);
		
		$this->set('datos_menu',$this->menuPrincipal());
		$id=$this->Session->read('Usuario.id');
		$this->set('data_perfiles', $this->Perfile->find('all',array('conditions'=>' Perfile.id='.$this->Session->read('Usuario.perfil_id')) ));		
		$this->set('data_empleado', $this->Empleado->find('all',array('conditions'=>' Empleado.id='.$this->Session->read('Usuario.empleado_id')) ));
		$this->set('data_perfil', $this->Perfile->find('all'));
		if (empty($this->request->data)){
			$empleados = $this->Usuario->Empleado->find('list',array('fields'=>'nombre'));
			$perfiles = $this->Usuario->Perfile->find('list',array('fields'=>'descripcion'));
			$this->set(compact('empleados','perfiles'));
			//$this->params['data'] = $this->Usuario->read(null,$id);
			$this->request->data = $this->Usuario->read(null,$id);			
			$this->render();
		}else{
			$this->request->data['Usuario']['clave']=$this->request->data['Usuario']['clavenueva'];
			if ( $this->Usuario->save($this->request->data)){
				$this->flash('El Usuario ha sido Actualizado.','/usuarios/cambiopassword');
			}else{
				$this->set('data', $this->request->data);
				$this->render();
			}
		}
	}

	public function cambioclave($id=null)
	{
		$this->set('datos_lista',$this->params);
		$this->checkSession();
		$this->set('datos_menu',$this->menuPrincipal());
		$id=$this->Session->read('Usuario.id');
		$perfil=$this->Perfile->find(' Perfile.id='.$this->Session->read('Usuario.perfil_id'));
		$this->set('data_perfiles', $this->Perfile->find('all',array('conditions'=>' Perfile.id='.$this->Session->read('Usuario.perfil_id')) ));		
		$this->set('data_empleado', $this->Empleado->find('all',array('conditions'=>' Empleado.id='.$this->Session->read('Usuario.empleado_id')) ));
		$this->set('data_perfil', $this->Perfile->find('all'));

		if (empty($this->params['data'])){
//			$this->Usuario->setId($id);
			$this->params['data'] = $this->Usuario->read(null,$id);
			$this->data = $this->Usuario->read(null,$id);
//			$this->params['data'] = $this->Usuario->read();
			$perfil=$this->Usuario->find(' Usuario.id='.$this->Session->read('Usuario.id'));

			$datos=$this->Perfile->find(' Perfile.id='.$this->params['data']['Usuario']['perfil_id']);			
			$this->render();
		}
		else{
			$this->params['data']['Usuario']['clave']=$this->params['data']['Usuario']['clavenueva'];
			if ( $this->Usuario->save($this->params['data'])){
				$this->flash('El Usuario se cambio la clave sido Actualizado. Haga Click aqui para continuar <-','/usuarios/logout');
			}else{
				$this->set('data', $this->params['data']);
				$this->render();
			}
		}
	}

//Funcion para logearse en el sistema
	public function login()
	{
		if (!empty($this->data)){
			
			$someone=$this->Usuario->find('first',array('conditions'=>" usuario='".$this->request->data['Usuario']['usuario']."' "));
			//$lista=$this->Listado->find(" usuario='".$this->params['data']['Usuario']['usuario']."' ",NULL,NULL,1);
//			if ($this->validateCaptcha('captcha', $this->data,'Usuario')){
//			echo $someone['Usuario']['clave'];
//			if ($this->Captcha->check($this->params['data']['Usuario']['captcha'])){			
				if ($someone['Usuario']['clave']==$this->request->data['Usuario']['clave']){

					$this->Session->write('Usuario',$someone['Usuario']);
					$this->Session->write('Empleado',$someone['Empleado']);
					$this->Session->write('Perfil',$someone['Perfile']);
	//				$this->Session->write('Proyecto',$someone['Proyecto']);
					$this->set('datos_menu',$this->menuPrincipal());
					$this->set('datos_panel',$this->panelControl());
					$this->datosEmpleado=$this->Session->read('Empleado.nombre');
					$this->set('datos_empleado',$this->datosEmpleado);
					$this->datosPerfil=$this->Session->read('Perfil.descripcion');
					$this->set('datos_perfil',$this->datosPerfil);
			//		if ($someone['Usuario']['clave']==$lista['Listado']['clave']){
			//      			$this->redirect('/usuarios/cambioclave');		
			//		}else{
					//$this->render('home');			
					$this->redirect('/documentos/publicados');						
			//		}
				}else{
					$this->Session->setFlash(' Acceso Denegado. El usuario o la clave tipeada es incorrecta.');
		      			$this->redirect('/usuarios/login');		
				}

//			}else{
//				$this->flash('Acceso denegado','/usuarios/login');
//				$this->Session->setFlash(' La letras escritas no son iguales a la que aparecen en la imagen.');
//	      			$this->redirect('/usuarios/login');		

//			}
		}else{
			$this->set('error','true');
		}
		$this->set('error','false');
	}
//Funcion para salir del sistema
	public function logout(){
		$this->Session->destroy();
		//unset($_SESSION[CAPTCHA_SESSION_ID]);
		session_destroy();		
		$this->redirect('/usuarios/login');			
	}

	public function buscar($id=NULL) {

		$this->checkSession();
		$this->lista_permiso($this->params['controller'],$this->params['action']);
		$criteria=" Usuario.usuario like '".$id."%'";
		if ($id==""){

		$this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/usuarios');
		 $data=array();
		}else{
//        $data = $this->Usuario->findAll($criteria); // Extra parameters added
        $data = $this->Usuario->find('all',array('conditions'=>$criteria)); // Extra parameters added                
	        if(count($data)<=0){
	   			$this->flash('No se ha conseguido Registro. Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/usuarios');
	        }
		}
        $this->set('data',$data);
		$this->render('buscar', 'ajax');	
	}

}
?>
