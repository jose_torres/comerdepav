<?php
class VentasController extends AppController
{
    public $name = 'Ventas';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Venta','Oficina','Grupo','Configuracion','Funcione','Perfile','Cliente','Vendedore','Proveedore','Compra','Cpabono','Cpabonopago','Empresa','Nccompra','Cpnotadebito','Devolucionventa','Maquina','Cuadrediario','Dinerodenominacione','Banco','Cuentasbancaria','Documentotipo','Movbancario','Sucdeposito','Sucgasto','Succheque','Ventaspago','Puntodeventa','Suctarjeta','Suctran','Retencionventa','Cargo','Descargo');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function asociar($id=NULL) {
        $this->checkSession();
        if (!empty($this->data)) {
            $datos=$this->data;
            list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
            list($id_empresa,$conexion)=explode('-',$datos['Reporte']['sucursal']);
            Configure::write('Model.globalSource', $conexion);
            $maquinas = $this->Maquina->find('first',array('conditions'=>' Maquina.codmaquina='.$maquina));

            $fechaant=$this->anomesdia($datos['Reporte']['desde']);
            $dias= 1; // los días a restar
            $fechaant=date("Y-m-d", strtotime("$fechaant -$dias day"));	
            $criteria=" Venta.fecha <= '".$fechaant." 23:59:59' ";
            $criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Abierto' ";
            $nro_reg= $this->Venta->find('count',array('conditions'=>$criteria));
            if($nro_reg<=0){ 
            //----------------------------------------- 
            if(isset($maquinas['Maquina']['correlativo'])){
                $datos['Reporte']['correlativo']=$maquinas['Maquina']['correlativo'];
                $datos=$this->Venta->ajustarDatos('asociar',$datos);
                $valor['Venta']=$datos['Ventas'];
                if ($this->Venta->saveAll($valor['Venta'])){
                    $this->Maquina->save($datos);				
                    $this->flash('La Ventas ha sido Actualizado.','/ventas/ventasz');
                } else {
                    $this->flash('Las Ventas NO se puede Guardar.','/ventas/ventasz');
                }
            }else{
                $this->flash('Las Maquina NO se ha seleccionado o Debe crearla.','/ventas/ventasz');
            }

            }else{
                $this->flash('Las Ventas Fiscales no se pueden asociar porque existen Ventas en Fechas anteriores que no se han asociado.','/ventas/ventasz');
            }

        }
    }

    public function cerrar($id=NULL) {
        $this->checkSession();
        if (!empty($this->data)) {
            $datos=$this->data;
            list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
            list($id_empresa,$conexion)=explode('-',$datos['Reporte']['sucursal']);
            Configure::write('Model.globalSource', $conexion);
            $maquinas = $this->Maquina->find('first',array('conditions'=>' Maquina.codmaquina='.$maquina));

            $fechaant=$this->anomesdia($datos['Reporte']['desde']);
            $dias= 1; // los días a restar
            $fechaant=date("Y-m-d", strtotime("$fechaant -$dias day"));	
            $criteria=" Venta.fecha <= '".$fechaant." 23:59:59' ";
            $criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Asociado' ";
             $nro_reg= $this->Venta->find('count',array('conditions'=>$criteria));
            if($nro_reg<=0){ 

            if(isset($maquinas['Maquina']['correlativo'])){
                $datos['Reporte']['correlativo']=$maquinas['Maquina']['correlativo'];
                $datos=$this->Venta->ajustarDatos('cerrar',$datos);
                $valor['Venta']=$datos['Ventas'];

                if ($this->Venta->saveAll($valor['Venta'])) {				
                    $this->flash('La Ventas Z han sido Cerradas.','/ventas/ventaszcerrar');
                } else {
                    $this->flash('Las Ventas NO se puede Guardar.','/ventas/ventaszcerrar');
                }
            }else{
                $this->flash('Las Maquina NO se ha seleccionado o Debe crearla.','/ventas/ventaszcerrar');
            }

            }else{
                $this->flash('Las Ventas Fiscales NO se pueden CERRAR porque existen Ventas en Fechas anteriores que no se han CERRADO.','/ventas/ventaszcerrar');
            }

        }
    }

    public function devolver($id=NULL) {
        $this->checkSession();
        if (!empty($this->data)) {
            $datos=$this->data;
            list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
            list($id_empresa,$conexion)=explode('-',$datos['Reporte']['sucursal']);
            Configure::write('Model.globalSource', $conexion);
            $maquinas = $this->Maquina->find('first',array('conditions'=>' Maquina.codmaquina='.$maquina));

            $fechaant=$this->anomesdia($datos['Reporte']['desde']);
            $dias= 1; // los días a restar
            $fechaant=date("Y-m-d", strtotime("$fechaant + $dias day"));	
            $criteria=" Venta.fecha >= '".$fechaant." 00:00:00' ";
            $criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Asociado' ";
            $nro_reg= $this->Venta->find('count',array('conditions'=>$criteria));
            if($nro_reg<=0){ 

            if(isset($maquinas['Maquina']['correlativo'])){
                $datos['Reporte']['correlativo']=$maquinas['Maquina']['correlativo'];
                $datos=$this->Venta->ajustarDatos('devolver',$datos);
                $valor['Venta']=$datos['Ventas'];

                if ($this->Venta->saveAll($valor['Venta'])) {
                        $this->Maquina->save($datos);				
                        $this->flash('La Ventas Z han sido Abiertas.','/ventas/ventaszdevolver');
                } else {
                        $this->flash('Las Ventas NO se puede Guardar.','/ventas/ventaszdevolver');
                }
            }else{
                $this->flash('Las Maquina NO se ha seleccionado o Debe crearla.','/ventas/ventaszdevolver');
            }

            }else{
                $this->flash('Las Ventas Fiscales NO se pueden DESASOCIAR porque existen Ventas en Fechas anteriores que estan ASOCIADO.','/ventas/ventaszdevolver');
            }

        }
    }

    public function abrir($id=NULL) {
        $this->checkSession();
        if (!empty($this->data)) {
            $datos=$this->data;
            list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
            list($id_empresa,$conexion)=explode('-',$datos['Reporte']['sucursal']);
            Configure::write('Model.globalSource', $conexion);
            $maquinas = $this->Maquina->find('first',array('conditions'=>' Maquina.codmaquina='.$maquina));
             $nro_reg= 0;
            if($nro_reg<=0){ 
            //----------------------------------------- 
            if(isset($maquinas['Maquina']['correlativo'])){
                $datos['Reporte']['correlativo']=$maquinas['Maquina']['correlativo'];
                $datos=$this->Venta->ajustarDatos('abrir',$datos);
                $valor['Venta']=$datos['Ventas'];
                if ($this->Venta->saveAll($valor['Venta'])) {
                        //$this->Maquina->save($datos);				
                        $this->flash('La Ventas ha sido Actualizado.','/ventas/ventasznf');
                } else {
                        $this->flash('Las Ventas NO se puede Guardar.','/ventas/ventasznf');
                }
            }else{
                $this->flash('Las Maquina NO se ha seleccionado o Debe crearla.','/ventas/ventasznf');
            }

            }else{
                $this->flash('Las Ventas Fiscales no se pueden asociar a la Maquina porque existen Ventas en Fechas anteriores asociado.','/ventas/ventasznf');
            }

        }
    }

    public function ventasz(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $maquinas=$this->Maquina->llenar_combo('',0);
        $this->set(compact('cliente','vendedores','sucursal','maquinas'));         
    }

    public function ventaszreportes(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $maquinas=$this->Maquina->llenar_combo('',0);
        $this->set(compact('cliente','vendedores','sucursal','maquinas'));         
    }

    public function ventaszcerrar(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $maquinas=$this->Maquina->llenar_combo('',0);
        $this->set(compact('cliente','vendedores','sucursal','maquinas'));         
    }

    public function ventaszdevolver(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $maquinas=$this->Maquina->llenar_combo('',0);
        $this->set(compact('cliente','vendedores','sucursal','maquinas'));         
    }

    public function ventasznf(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $maquinas=$this->Maquina->llenar_combo('',0);
        $this->set(compact('cliente','vendedores','sucursal','maquinas'));         
    }

    public function buscarventas($id=NULL) {        
        //useDbConfig = 'comerdepa_3'
        $datos=$_POST;$client=explode('-',$_POST['clientes']);		
        $vend=explode('-',$_POST['vendedores']);$tipoven=explode('-',$_POST['tipoventa']);
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $SUCURSAL = $datos['sucursal'];
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Ventas_Diarias':
            $titulo='Ventas Fiscales a Asociar';
            $tipoven=explode('-','0');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Venta->reporte($datos,'',9);
            $data_dev=$this->Devolucionventa->reporte($datos,'',0);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasdiarias', 'ajax');
        break;
        case 'Ventas_Diarias_Asociadas':
            $titulo='Ventas Fiscales a Cerrar';
            $tipoven=explode('-','0');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;	
            $data=$this->Venta->reporte($datos,'',10);			
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasdiariascerrar', 'ajax');
        break;
        case 'Ventas_Diarias_Cerradas':
            $titulo='Ventas Fiscales a Cerradas';
            $tipoven=explode('-','0');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Venta->reporte($datos,'',11);			
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasdiariascerradas', 'ajax');
        break;
        case 'Ventas_Diarias_Devolver':
            $titulo='Ventas Fiscales a Abrir';
            $tipoven=explode('-','0');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Venta->reporte($datos,'',10);			
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasdiariasdevolver', 'ajax');
        break;
        case 'Ventas_Diarias_No_Fiscal':
            $titulo='Ventas Fiscales No Fiscal';
            $tipoven=explode('-','0');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $fechaant=$this->Venta->ultimaFechaAsociada();
            $dias= 1; // los días a restar
            $datos['fechadesde'] =date("Y-m-d", strtotime("$fechaant + $dias day"));
            $data=$this->Venta->reporte($datos,'',12);			
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasdiariasnf', 'ajax');
        break;
        case 'Ventas_Diarias_Cierre':
            $titulo='Ventas del Dia'; $tipoven=explode('-','0');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Venta->reporte($datos,'',14);//Buscar Ventas realizadas 	
            $datapago = $this->Venta->pagosRealizados($datos);//Buscar los Pagos realizado
            $data_dev=$this->Devolucionventa->reporte($datos,'',3);//Buscar Devoluciones de Ventas realizadas
            // Buscar Datos Adicionales
            $denominacion = $this->Dinerodenominacione->find('all',array('order'=>'Dinerodenominacione.fin desc,Dinerodenominacione.valor asc','recursive' => -1,'conditions'=>" Dinerodenominacione.fin='DINERO' or Dinerodenominacione.fin='MONEDA'"));			
            $banco = $this->Banco->llenar_combo('',1);
            $documentotipo['dp'] = $this->Documentotipo->llenar_combo(2);
            $documentotipo['nc'] = $this->Documentotipo->llenar_combo(3);

            $cuadre = $this->Cuadrediario->buscar($datos);
            $this->Cuadrediario->actualizarMontosCuadre($cuadre['Cuadrediario']['id']);
            $puntos = $this->Puntodeventa->llenar_combo('');
            $transfer = $this->Suctran->llenar_combo(0," and VT.fecha='".$cuadre['Cuadrediario']['fecha']."' ");
            //--------Buscar Cheques de Ventas
            $cheques = $this->Ventaspago->find('all',array('conditions'=>" Venta.fecha='".$cuadre['Cuadrediario']['fecha']."' and Ventaspago.tipopago='CHEQUE'",'order'=>'Ventaspago.banco'));
            if($cuadre['Cuadrediario']['estatus']==1){				
                $cheques = $this->Succheque->buscar($cheques,$cuadre);// Buscar Suc Cheques en Cuadre
            }
            //------------------------
            $depositos = $this->Movbancario->buscarDepositos($cuadre);			
            $gastos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$cuadre['Cuadrediario']['fecha']."' "));
            $retenciones['Total'] = $this->Retencionventa->buscarResumenDia($cuadre);
            $retenciones['Registros'] = $this->Retencionventa->buscarRegistrosDia($cuadre);
            $depositos=array_merge($depositos,$gastos);
//------------------- Cargos y Descargos--------------------------------------
            $datos['fechahasta'] = $datos['fechadesde'] = date("d-m-Y",strtotime($cuadre['Cuadrediario']['fecha']));
            $cargos=$this->Cargo->listadoCargoResumen($datos);
            $descargos=$this->Descargo->listadoDescargoResumen($datos);
//----------------------------------------------------------------------------            
            $this->set(compact('data','titulo','data_dev','datapago','denominacion','banco','cuadre','documentotipo','depositos','cheques','puntos','transfer','retenciones','SUCURSAL','cargos','descargos'));
            if($cuadre['Cuadrediario']['estatus']==1){
                $this->render('buscarventasdiariascaja', 'ajax');
            }else{
                $mensaje = ' El Cuadre de Fecha '.date("d-m-Y",strtotime($cuadre['Cuadrediario']['fecha'])).' esta Cerrado';
                $this->set(compact('mensaje'));
                $this->render('buscarventasdiariascajacerrada', 'ajax');
            }
        break;
        }
    }

    public function buscarchequesxdep($id=NULL){
        $datos=$_POST;
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);

        $cuadre['Cuadrediario']['fecha'] = $datos['cuadre_fecha'];
        $cuadre['Cuadrediario']['id'] = $datos['cuadre_id'];
        // Buscar Suc Cheques en Cuadre
        $cheques = $this->Succheque->find('all',array('conditions'=> "Succheque.fecha='".$cuadre['Cuadrediario']['fecha']."' and Succheque.movbancario_id=0 "));
        $this->set(compact('cheques'));
        $this->render('buscarchequesxdep', 'ajax');
    }

    public function buscartransferencia($id=NULL){
        $datos=$_POST; $data[0][0]['monto']=0;$data[0][0]['numerotransferencia']='';
        if($datos['opcion']!=''){
        list($codventa,$codsucursal,$numerotransferencia,$cod_transf,$id_empresa,$conexion)=explode('-',$id);
        Configure::write('Model.globalSource', $conexion);		
        //--------Buscar Transferencia de Ventas
        $condicion = " and VT.codventa=".$codventa." and VT.codsucursal=".$codsucursal." and VTP.numerotransferencia='".$numerotransferencia."' and VTP.id=".$cod_transf." " ;
        $data = $this->Suctran->buscarpagos($condicion);
        }	
        $this->set(compact('data'));
        $this->render('buscartransferencia', 'ajax');
    }

    public function buscartransferxdep($id=null){
        $datos=$_POST;
        list($id_empresa,$conexion)=explode('-',$id);
        Configure::write('Model.globalSource', $conexion);	
        $transfer = $this->Suctran->llenar_combo(0," and VT.fecha='".$datos['cuadre_fecha']."' ");
        $this->set(compact('transfer'));
        $this->render('buscartransferxdep', 'ajax');
    }

    public function buscarmaquinas($id='1-1'){
        $this->checkSession();		
        list($id_empresa,$conexion)=explode('-',$id);
        Configure::write('Model.globalSource', $conexion);		
        $maquinas=$this->Maquina->llenar_combo('',0);
        $this->set(compact('maquinas'));		          
        $this->render('buscarmaquinas', 'ajax');
    }

    public function buscarctabanco($id = null) {
        $datos = $_POST;
        list($codbanco,$id_empresa,$conexion)=explode('-',$id);
        Configure::write('Model.globalSource', $conexion);
        $criterio="Cuentasbancaria.codbanco=".$codbanco;
        $ctabancos = $this->Cuentasbancaria->llenar_combo('',0,$criterio);
        $modelo = 'Sucdeposito';
        if(isset($datos['opcion'])){
                $modelo = $datos['opcion'];
        }
        $this->set(compact('ctabancos','modelo'));

        $this->render('buscarctabanco', 'ajax');		
    }

    public function depositar($id=NULL) {
        $datos=$_POST;
        //$client=explode('-',$_POST['clientes']);		
        $vend=explode('-',$_POST['vendedores']);$tipoven=explode('-',$_POST['tipoventa']);
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['opcion']) {
        case 'Efectivo':
        // 1.- Buscar Sucursal
            $datos['codsucursal'] = $this->Venta->obtenerSucursal();
            if ($datos['codsucursal'] > 0){
            // 2.- Guardar Movimientos Bancario
                $datos = $this->Movbancario->ajustarDatos('add',$datos);
                if ($this->Movbancario->save($datos)) {
                // 3.- Obtener ID Movimientos Bancario
                $datos['movbancario_id'] = $this->Movbancario->getLastInsertID();
                // 4.- Guardar Deposito
                $datos = $this->Sucdeposito->ajustarDatos('add',$datos);
                if ($this->Sucdeposito->saveAll($datos['Sucdeposito'])) {}
                }			
            }
            // 5.- Mostrar Depositos en Efectivo
            $depositos['Efectivo'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='EFECTIVO'"));
            $this->set(compact('datos','depositos'));
            $this->render('depefectivos', 'ajax');
        break;
        case 'Debito':
        // 1.- Buscar Sucursal
            $datos['codsucursal'] = $this->Venta->obtenerSucursal();
            if ($datos['codsucursal'] > 0){
            // 2.- Guardar Movimientos Bancario
                $datos['totalDeposito'] = $this->formato_ingles($datos['totalDeposito']);
                $datos = $this->Movbancario->ajustarDatos('addDeb',$datos);
                if ($this->Movbancario->save($datos)) {
                // 3.- Obtener ID Movimientos Bancario
                $datos['movbancario_id'] = $this->Movbancario->getLastInsertID();
                // 4.- Guardar Deposito
                $datos = $this->Suctarjeta->ajustarDatos('add',$datos);
                if ($this->Suctarjeta->saveAll($datos['Suctarjeta'])) {}		
                }			
            }
            // 5.- Mostrar Depositos en Efectivo
            $depositos['Debito'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='DEBITO'"));
            $this->set(compact('datos','depositos'));
            $this->render('depdebito', 'ajax');
        break;
        case 'Cheque':
        // 1.- Buscar Sucursal
            $datos['codsucursal'] = $this->Venta->obtenerSucursal();
            $cuadre['cuadre_fecha'] = $datos['cuadre_fecha'];
            if ($datos['codsucursal'] > 0){
            // 2.- Guardar Movimientos Bancario
                $datos['totalDeposito'] = $this->formato_ingles($datos['totalDeposito']);
                $datos = $this->Movbancario->ajustarDatos('add',$datos);		
                if ($this->Movbancario->save($datos)) {
                // 3.- Obtener ID Movimientos Bancario
                $movban['movbancario_id'] = $this->Movbancario->getLastInsertID();
                $datos=array_merge($datos,$movban);
                // 4.- Guardar Cheques
                $datos = $this->Succheque->ajustarDatos('cuadre',$datos);
                if ($this->Succheque->saveAll($datos['Succheque'])) {                     
                }
                }			
            }
            // 5.- Mostrar Depositos en Efectivo
            $depositos['Cheque'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$cuadre['cuadre_fecha']."' and Movbancario.tipodeposito='CHEQUE'"));
            $this->set(compact('datos','depositos'));
            $this->render('depcheque', 'ajax');
        break;
        case 'Transfer':
        // 1.- Buscar Sucursal
            $datos['codsucursal'] = $this->Venta->obtenerSucursal();
            if ($datos['codsucursal'] > 0){
            // 2.- Guardar Movimientos Bancario
                $datos['totalDeposito'] = $this->formato_ingles($datos['totalDeposito']);
                $datos = $this->Movbancario->ajustarDatos('add',$datos);
                if ($this->Movbancario->save($datos)) {
                // 3.- Obtener ID Movimientos Bancario
                $datos['movbancario_id'] = $this->Movbancario->getLastInsertID();
                // 4.- Guardar Deposito
                $datos = $this->Suctran->ajustarDatos('add',$datos);
                if ($this->Suctran->saveAll($datos['Suctran'])) {}				
                }			
            }
            // 5.- Mostrar Depositos en Efectivo
            $depositos['Transfer'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='TRANSFER'"));
            $this->set(compact('datos','depositos'));
            $this->render('deptransfer', 'ajax');
        break;
        case 'Gasto':
        // 1.- Buscar Sucursal
            $datos['codsucursal'] = $this->Venta->obtenerSucursal();
            if ($datos['codsucursal'] > 0){
            // 2.- Guardar Movimientos Bancario
                $datos['totalDeposito'] = $this->formato_ingles($datos['totalDeposito']);
                $datos = $this->Sucgasto->ajustarDatos('add',$datos);
                if ($this->Sucgasto->save($datos)){}			
            }
            // 5.- Mostrar Gastos
            $depositos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$datos['cuadre_fecha']."' "));
            $this->set(compact('datos','depositos'));
            $this->render('gastos', 'ajax');
        break;
        }
    }

    public function eliminardeposito($id=NULL) {
        $datos=$_POST;
        //$client=explode('-',$_POST['clientes']);		
        $vend=explode('-',$_POST['vendedores']);$tipoven=explode('-',$_POST['tipoventa']);
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['opcion']) {
        case 'Efectivo':
        // 1.- Preguntar si el Cuadre esta abierto			
            $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
            if ($cuadre['Cuadrediario']['estatus'] == 1){
            // 2.- Eliminar Movimientos Bancario				
                if ($this->Movbancario->delete($datos['movbancario_id'])){		
                // 3.- Eliminar Deposito				
                if ($this->Sucdeposito->deleteAll(array('Sucdeposito.movbancario_id' => $datos['movbancario_id']))){}
                }			
            }
            // 4.- Mostrar Depositos en Efectivo
            $depositos['Efectivo'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='EFECTIVO'"));
            $this->set(compact('datos','depositos'));
            $this->render('depefectivos', 'ajax');
        break;
        case 'Debito':
        // 1.- Preguntar si el Cuadre esta abierto			
            $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
            if ($cuadre['Cuadrediario']['estatus'] == 1){
            // 2.- Eliminar Movimientos Bancario				
                if ($this->Movbancario->delete($datos['movbancario_id'])){
            // 3.- Eliminar Deposito				
                if ($this->Suctarjeta->deleteAll(array('Suctarjeta.movbancario_id' => $datos['movbancario_id']))){}				
                }			
            }
            // 4.- Mostrar Depositos en Efectivo
            $depositos['Debito'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='DEBITO'"));
            $this->set(compact('datos','depositos'));
            $this->render('depdebito', 'ajax');
        break;
        case 'Cheque':
        // 1.- Preguntar si el Cuadre esta abierto			
            $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
            if ($cuadre['Cuadrediario']['estatus'] == 1){
            // 2.- Eliminar Movimientos Bancario				
                if ($this->Movbancario->delete($datos['movbancario_id'])){
                // 3.- Eliminar Deposito				
                if ($this->Succheque->updateAll(array('Succheque.movbancario_id' => 0),array('Succheque.movbancario_id' => $datos['movbancario_id']))){}
                }			
            }
            // 4.- Mostrar Depositos en Efectivo
            $depositos['Cheque'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='CHEQUE'"));
            $this->set(compact('datos','depositos'));
            $this->render('depcheque', 'ajax');
        break;
        case 'Transfer':
        // 1.- Preguntar si el Cuadre esta abierto			
            $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
            if ($cuadre['Cuadrediario']['estatus'] == 1){
            // 2.- Eliminar Movimientos Bancario				
                if ($this->Movbancario->delete($datos['movbancario_id'])){
            // 3.- Eliminar Deposito				
                if ($this->Suctran->deleteAll(array('Suctran.movbancario_id' => $datos['movbancario_id']))){}			
                }			
            }
            // 3.- Mostrar Depositos en Efectivo
            $depositos['Transfer'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='TRANSFER'"));
            $this->set(compact('datos','depositos'));
            $this->render('deptransfer', 'ajax');
        break;
        case 'Gasto':
        // 1.- Preguntar si el Cuadre esta abierto			
            $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
            if ($cuadre['Cuadrediario']['estatus'] == 1){
            // 2.- Eliminar Movimientos Bancario				
                if ($this->Sucgasto->delete($datos['sucgasto_id'])){}		
            }
            // 3.- Mostrar Depositos en Efectivo
            $depositos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$datos['cuadre_fecha']."' "));
            $this->set(compact('datos','depositos'));
            $this->render('gastos', 'ajax');
        break;
        case 'Retencion':
        // 1.- Preguntar si el Cuadre esta abierto			
            $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
            if ($cuadre['Cuadrediario']['estatus'] == 1){
            // 2.- Eliminar Retenciones				
                $this->Retencionventa->eliminarDetalle($datos['codretencion'],$id_empresa);
                $this->Retencionventa->eliminarCabecera($datos['codretencion']);
            }
            // 3.- Mostrar Depositos en Efectivo
            $retenciones['Total'] = $this->Retencionventa->buscarResumenDia($cuadre);
            $retenciones['Registros'] = $this->Retencionventa->buscarRegistrosDia($cuadre);
            $this->set(compact('datos','retenciones'));
            $this->render('retenciones', 'ajax');
        break;
        }
    }
//----------------------------------------------------------------------
    public function cierre(){
        $this->checkSession();
        if (!empty($this->data)) {
            $datos = $this->data;
            list($id_empresa,$conexion)=explode('-',$datos['Reporte']['sucursal']);
            Configure::write('Model.globalSource', $conexion);
            $cuadre['Cuadrediario']['id'] = $datos['Cuadre']['id'];
            $cuadre['Cuadrediario']['estatus'] = 2;
            $this->Cuadrediario->save($cuadre);
            $this->flash('Se ha Realizado el Cierre de la Fecha:'.date("d-m-Y",strtotime($datos['Cuadre']['fecha'])),'/ventas/cierre');
        }
        Configure::write('Model.globalSource', 'default');
        $sucursal=$this->Empresa->llenar_combo('',2);
        //$sucursal=$this->Empresa->llenar_combo('',1);
        Configure::write('Model.globalSource', 'comerdepa');
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $maquinas=$this->Maquina->llenar_combo('',0);

        $this->set(compact('cliente','vendedores','sucursal','maquinas'));         
    }

//----------------------------------------------------------------------	
    public function add(){
        $this->checkSession();
        $title_for_layout='Registrar una Retenci&oacute;n';
        $proveedore=$this->Proveedore->llenar_combo();
        $this->set(compact('proveedore','title_for_layout'));		          
    }

    public function edit($id = null) {
        $this->checkSession();
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Retencion Invalida', true));
            $this->redirect(array('action' => '/'));
        }
        if (!empty($this->data)) {
            $datos=$this->data;
            if($datos['Retencioncompra']['tiporetencion']=='Normal'){
            $datos=$this->Retencioncompra->ajustarDatos('edit',$datos);
            if ($this->Retencioncompra->save($datos)) {
                $abonos = $this->Cpabonopago->busquedas('Uno',$datos);
                $abonos = array_merge($datos, $abonos);
                $this->Cpabono->actualizar($abonos);
                $this->Cpabonopago->actualizar($abonos);				
                $this->flash('La Retencion ha sido Guardado.','/retenciones/add');
            } else {
                $this->flash('La Retencion no se puede Guardar.','/retenciones/add');
            }
            }else{
                $this->flash('La Retencion no se puede Guardar porque no se ha colocado en Normal.','/retenciones/edit/'.$datos['Retencioncompra']['codretencion']);
            }
        }

        if (empty($this->data)) {
            $this->data = $this->Retencioncompra->read(null, $id);
        }
        		
    }

    public function buscarfacturas($id=NULL) {
        $datos=$_POST;
        $data=$this->Retencioncompra->reporte($datos,'',2);
        $this->set(compact('data','datos'));
        $this->render('buscarfacturas', 'ajax');
    }

    public function calcularretencion($id=NULL) {        		
        $registros=$_POST;
        $registros['codmovimientos']=explode('-',$_POST['codmovimientos']);
        $registros['tipomovimientos']=explode('-',$_POST['tipomovimientos']);
        $registros['nrodocumentos']=explode('-',$_POST['nrodocumentos']);
        $data=$this->Compra->calcular_retencion_compras($registros);
        $facturas=$this->Retencioncompra->reporte($registros,'',3);	
        $this->set(compact('data','registros','facturas'));
        $this->render('calcularretencion', 'ajax');
    }	

    public function incluir($id=NULL) {
        $this->checkSession();
        if (!empty($this->data)) {
            $datos=$this->data;
            $datos=$this->Retencioncompra->ajustarDatos('add',$datos);
            $valor['Retencioncompra']=$datos['Retencioncompras'];

            if ($this->Retencioncompra->saveAll($valor['Retencioncompra'])) {
                $this->Retencioncompra->guardarAbono($datos);
                $this->flash('La Retencion ha sido Guardado.','/retenciones/add');
            } else {
                $this->flash('La Retencion no se puede Guardar.','/retenciones/add');
            }
        }
    }

    public function index() {
        $this->checkSession();
        $title_for_layout='LISTA DE RETENCIONES REGISTRADOS ';
        $proveedore=$this->Proveedore->llenar_combo();
        $this->Retencioncompra->recursive = 0;
        $criteria = "Retencioncompra.tiporetencion='Normal' ";
        $order="Retencioncompra.fechaemision DESC,Retencioncompra.numero DESC";        
        $campos= "Retencioncompra.numero,Proveedore.descripcion,Retencioncompra.fechaemision,sum(Retencioncompra.montoiva) as montoiva, sum(Retencioncompra.montoretenido) as montoretenido";
        $grupo= " Retencioncompra.numero,Proveedore.descripcion,Retencioncompra.fechaemision ";
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order,'group'=>$grupo,'fields'=>$campos);
        $this->set('unidades', $this->paginate());
        $this->set(compact('title_for_layout','proveedore'));
    }

    public function transito() {
        $this->checkSession();
        $title_for_layout='LISTA DE RETENCIONES EN TRANSITO REGISTRADOS ';
        $this->Retencioncompra->recursive = 0;
        $criteria = "Retencioncompra.tiporetencion='Transito' ";
        $order="Retencioncompra.codretencion DESC";        
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
        $this->set('unidades', $this->paginate());
        $this->set(compact('title_for_layout'));
    }

    public function viewpdf($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        $empresa = $this->Empresa->find('first',array('conditions'=>''));	
        $encabezado=$this->Retencioncompra->getCabecera();
        //$data = $this->Retencioncompra->read(null, $id);
        $data = $this->Retencioncompra->find('all',array('conditions'=>" Retencioncompra.numero='".$id."'"));	
        $datos_nc = $this->Nccompra->busquedas('Datos',$data);
        $datos['numero']=$data[0]['Retencioncompra']['numero'];
        $datos_nd = $this->Cpnotadebito->reporte($datos);

        $quincena = $this->Retencioncompra->getQuincena(date('d',strtotime($data[0]['Retencioncompra']['fechaemision'])));		
        $this->set(compact('data','texto','empresa','encabezado','quincena','datos_nc','datos_nd'));
    }

    public function buscarretenciones($id=1){
        $datos=$_POST;
        $provee=explode('-',$_POST['proveedores']);
        $datos['provee']=$provee;
        $data = $this->Retencioncompra->findRetencion($datos);   
        $this->set(compact('data'));
        $this->render('buscarretenciones', 'ajax');
    }

    public function buscarpdvxdep($id=null){
        $datos = $_POST; $pdv = array();

        list($codmaquina,$id_empresa,$conexion)=explode('-',$id);
        if($codmaquina!=''){
            Configure::write('Model.globalSource', $conexion);
            $criterio="and PUNTO.codpuntoventa=".$codmaquina." and PUNTO.fecha='".$datos['fecha']."' ";
            $pdv = $this->Puntodeventa->buscarpagos($criterio);
        }
        $this->set(compact('pdv'));
        $this->render('buscarpdvxdep', 'ajax');
    }

}
?>
