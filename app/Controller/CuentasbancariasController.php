<?php
class CuentasbancariasController extends AppController
{
    public $name = 'Cuentasbancarias';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Cuentasbancaria','Funcione','Grupo','Configuracion','Banco','Perfile','Movbancario','Conmovbancario');
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function index(){
        $this->checkSession();
        $this->Cuentasbancaria->recursive = 0;
        $this->set('data', $this->paginate());
        $bancos = $this->Banco->llenar_combo('',1);
        $this->set('bancos', $bancos);
        $this->render('index');
    }

    public function view($id){
            $this->set('data',$this->Cuentasbancaria->read(null,$id));
    }

    public function add(){
            $tipcuentas = $this->Cuentasbancaria->tipo_cuenta();
            $bancos = $this->Banco->llenar_combo();
            $dirigido = $this->Cuentasbancaria->tipo_dirigida();

            if (!empty($this->data)){

                    if ($this->Cuentasbancaria->save($this->data)){
                            Configure::write('Model.globalSource', $this->CONEXION);
                            $this->Cuentasbancaria->save($this->data);
                    $this->flash('Los Datos de la Cuenta Bancaria han sido Guardada.','/cuentasbancarias/index');
                    }else{
                            $this->set('data', $this->data);
                            $this->render('add');
                    }			
                    }
            $title_for_layout='Agregar una Nueva Cuenta Bancaria';	
            $this->set(compact('title_for_layout','bancos','tipcuentas','dirigido'));

    }

    public function edit($id=null){
            if (empty($this->data)){
                    $tipcuentas = $this->Cuentasbancaria->tipo_cuenta();
                    $bancos = $this->Banco->llenar_combo();
                    $dirigido = $this->Cuentasbancaria->tipo_dirigida();
                    $this->set(compact('bancos','tipcuentas','dirigido'));
                    $this->data = $this->Cuentasbancaria->read(null,$id);
            }else{
                    if ( $this->Cuentasbancaria->save($this->data)){
                            Configure::write('Model.globalSource', $this->CONEXION);
                            $this->Cuentasbancaria->save($this->data);
                    $this->flash('Los Datos de la Cuenta Bancaria han sido Actualizado.','/cuentasbancarias/index');
                    }else{
                            $this->set('data', $this->data);				
                    }
            }

            $title_for_layout='Editar una Cuenta Bancaria';	
            $this->set(compact('title_for_layout'));
    }

    public function delete($id){
            $cont[0] = $this->Movbancario->find('count',array('conditions'=>'Movbancario.cuentasbancaria_id='.$id));
            $cont[1] = $this->Conmovbancario->find('count',array('conditions'=>'Conmovbancario.cuentasbancaria_id='.$id));
            if ($cont[0]<=0 && $cont[0]<=0){
                    if ($this->Cuentasbancaria->delete($id)){
                            $this->flash('El Numero de Cuenta Bancaria con el id: '.$id.' ha sido eliminado.', '/cuentasbancarias/index');
                    }
            }else{
                    $this->flash('El Numero de Cuenta Bancaria con el id: '.$id.' no puede ser eliminado, porque posee Movimientos Asociados.', '/cuentasbancarias/index');
            }
    }

    public function buscar($id=NULL) {
            list($cuenta,$codbanco)=explode('-',$id);
            $criteria=" cast(Cuentasbancaria.numerocuenta as text) like '".$cuenta."%' ";
            if($codbanco!=''){
                    $criteria= $criteria." and Cuentasbancaria.codbanco=".$codbanco."";
            }
            if ($id==""){
                    $this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/cuentasbancarias/index');
                     $data=array();
            }else{
            $data = $this->Cuentasbancaria->find('all',array('conditions'=>$criteria)); // Extra parameters added                  
            if(count($data)<=0){
                            $this->flash('No se ha conseguido Registro. Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/cuentasbancarias/index');
            }
            }
        $this->set('data',$data);
        $this->render('buscar', 'ajax');	
    }

}
?>
