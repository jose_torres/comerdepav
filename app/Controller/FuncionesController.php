<?php
class FuncionesController extends AppController
{
    public $name = 'Funciones';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Funcione','Grupo','Configuracion','Empleado','Perfile');

//-----------------------Seguridad--------------------------------------	
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }	

//----------------------------------------------------------------------		
    public function index(){
        $this->checkSession();
        $this->Funcione->recursive = 0;
        $this->set('data', $this->paginate());
    }

    public function view($id){
        $this->set('data', $this->Funcione->read(null,$id));
    }

    public function add(){
        $grupos = $this->Grupo->llenar_combo('',0);
        $modulos = $this->Funcione->Modulo->find('list',array('fields'=>'nombre'));
        $funciones = $this->Funcione->find('all',array('conditions'=>' ','recursive'=>-1));
        $this->set(compact('modulos','grupos','funciones'));
        if (!empty($this->data)){
            if ($this->Funcione->save($this->data)){					
                $this->flash('La Funcion ha sido guardada.','/funciones');
            }else{
                $this->set('data', $this->data);
                $this->render('add');
            }
        }
    }

    public function edit($id=null){		
        if (empty($this->data)){
            $this->data = $this->Funcione->read(null,$id);
            $grupos = $this->Grupo->llenar_combo('',0);
            $modulos = $this->Funcione->Modulo->find('list',array('fields'=>'nombre'));
            $funciones = $this->Funcione->find('all',array('conditions'=>' ','recursive'=>-1));
            $this->set(compact('modulos','grupos','funciones'));
        }else{
            if ( $this->Funcione->save($this->data)){
                $this->flash('La Funcion ha sido Actualizada.','/funciones');
            }else{
                $this->set('data', $this->data);
            }
        }
    }

    public function delete($id){
        $total = $this->Configuracion->find('count',array('conditions'=>" Configuracion.funcion_id=".$id." and Configuracion.status='1' "));
        //OJO ES NECESARIO CONFIRMAR CON EL MOVIMIENTO
        if ($total>0){
                $this->flash('La Funcion con el id: '.$id.' no puede ser Eliminado porque tiene movimientos asociadas a el.','/funciones');
        }else{	
                if ($this->Funcione->delete($id)){
                        $this->flash('El tipo de egreso con el id: '.$id.' ha sido Eliminado.','/funciones');
                }		
        }
    }

    public function buscar($id=NULL) {
        $criteria=" upper(Funcione.nombre) like upper('".$id."%')";
        if ($id==""){
                $this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/funciones');
                $data=array();
        }else{
                $data = $this->Funcione->find('all',array('conditions'=>$criteria));
        }
$this->set('data',$data);
        $this->render('buscar', 'ajax');	
    }

}
?>
