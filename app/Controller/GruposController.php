<?php
class GruposController extends AppController
{
    public $name = 'Grupos';
	public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Grupo','Configuracion','Funcione','Perfile');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------	
    public function index(){
        $this->checkSession();
        $this->Grupo->recursive = 0;
        $this->set('data', $this->paginate());          
    }

    public function view($id){
        $this->checkSession();
        $this->set('data', $this->Grupo->read(NULL,$id));
    }

    public function add(){
        $this->checkSession();
        if(!empty($this->data)){
                if ($this->Grupo->save($this->data)){
                        $this->flash('El Grupo ha sido Guardado.','/grupos');
                }else{
                        $this->set('data', $this->data);
                        $this->render('add');
                }
        }		
        $this->set('nombreProyecto', $this->nombreProyecto);
    }

    public function edit($id=null){
        $this->checkSession();
        if (empty($this->data)){
                $this->data =  $this->Grupo->read(NULL,$id);
        }else{
                if ($this->Grupo->save($this->data)){
                        $this->flash('El Grupo ha sido Actualizado.','/grupos');
                }else{
                        $this->set('data', $this->data);
                }
        }
        $this->set('nombreProyecto', $this->nombreProyecto);
    }

    public function delete($id){
        $this->checkSession();
        $nro = $this->Funcione->find('count',array('conditions'=>' Funcione.grupo_id='.$id));
        if ($nro>0){
            $this->flash('El Grupo con el id: '.$id.' no puede ser Eliminado porque tiene movimientos.','/grupos');
        }else{
            if ($this->Grupo->delete($id)){
                $this->flash('El Grupo con el id: '.$id.' ha sido Eliminado.','/grupos');				
            }		
        }
    }

    public function buscar($id=NULL) {
        $criteria=" upper(Grupo.nombre) like upper('".$id."%')";
        if ($id==""){
            $this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/grupos');
            $data=array();
        }else{
            $data = $this->Grupo->find('all',array('conditions'=>$criteria));  
        }
        $this->set('data',$data);
        $this->render('buscar', 'ajax');	
    }

}
?>
