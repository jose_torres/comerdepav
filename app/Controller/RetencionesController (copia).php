<?php
class RetencionesController extends AppController
{
    public $name = 'Retenciones';
	public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Retencioncompra','Oficina','Grupo','Configuracion','Funcione','Perfile','Cliente','Vendedore','Proveedore','Compra','Cpabono','Cpabonopago','Empresa','Nccompra');

//-----------------------Seguridad--------------------------------------	
	public function beforeFilter(){
		if($this->Session->check('Empleado')){
			$this->datosEmpleado=$this->Session->read('Empleado.nombre');
			$this->set('datos_empleado',$this->datosEmpleado);
			$this->set('datos_menu',$this->menuPrincipal($this->Session->read('Usuario.perfil_id')));
			$this->lista_permiso($this->params['controller'],$this->params['action']);
		}		
	}	

	public function lista_permiso($controlador,$accion){
		$datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));			
		$encontrado=false;		
		foreach ($datos_lista as $registro){
			$registro['Funcione']['direccion']=$registro['Funcione']['direccion'].'/';
			list($contr,$acc)=explode('/',$registro['Funcione']['direccion']);
			//echo $contr.'->'.$acc.'<br>';
			if ($acc=='') $acc='index';
			if ( $contr.'/'.$acc==$controlador.'/'.$accion){
				$encontrado=true;		
			}
		}
		if ($encontrado==false){
			$this->Session->setFlash('Usted no tiene permiso para ver esta funcionalidad.');
      			$this->redirect('/usuarios/home');
		}
	}

	public function menuPrincipal($id){
		return $this->Configuracion->menu($id);
	}
//----------------------------------------------------------------------	
	public function add(){
		$this->checkSession();
		$title_for_layout='Registrar una Retenci&oacute;n';
		$proveedore=$this->Proveedore->llenar_combo();
		$this->set(compact('proveedore','title_for_layout'));		          
	}

	public function edit($id = null) {
		$this->checkSession();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Retencion Invalida', true));
			$this->redirect(array('action' => '/'));
		}
		if (!empty($this->data)) {
			$datos=$this->data;
			if($datos['Retencioncompra']['tiporetencion']=='Normal'){
			$datos=$this->Retencioncompra->ajustarDatos('edit',$datos);
			if ($this->Retencioncompra->save($datos)) {
				$abonos = $this->Cpabonopago->busquedas('Uno',$datos);
				$abonos = array_merge($datos, $abonos);
				$this->Cpabono->actualizar($abonos);
				$this->Cpabonopago->actualizar($abonos);				
				$this->flash('La Retencion ha sido Guardado.','/retenciones/add');
			} else {
				$this->flash('La Retencion no se puede Guardar.','/retenciones/add');
			}
			}else{
				$this->flash('La Retencion no se puede Guardar porque no se ha colocado en Normal.','/retenciones/edit/'.$datos['Retencioncompra']['codretencion']);
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Retencioncompra->read(null, $id);
		}
		
		//$this->set('nombreProyecto', $this->nombreProyecto);		
	}

	public function buscarfacturas($id=NULL) {
		$datos=$_POST;
		$data=$this->Compra->reporte($datos,'',2);
		$this->set(compact('data','datos'));
		$this->render('buscarfacturas', 'ajax');
	}

	public function calcularretencion($id=NULL) {
		$registros=$_POST;
		$data=$this->Compra->calcular_retencion_compras($registros);	
		$this->set(compact('data','registros'));
		$this->render('calcularretencion', 'ajax');
	}	

	public function incluir($id=NULL) {
		$this->checkSession();
		if (!empty($this->data)) {
			$datos=$this->data;
			$datos=$this->Retencioncompra->ajustarDatos('add',$datos);
			if ($this->Retencioncompra->save($datos)) {
				$this->Retencioncompra->guardarAbono($datos);
				$this->flash('La Retencion ha sido Guardado.','/retenciones/add');
			} else {
				$this->flash('La Retencion no se puede Guardar.','/retenciones/add');
			}
		}
	}

	public function index() {
		$this->checkSession();
		$title_for_layout='LISTA DE RETENCIONES REGISTRADOS ';
		$this->Retencioncompra->recursive = 0;
		$criteria = "Retencioncompra.tiporetencion='Normal' ";
		$order="Retencioncompra.fechaemision DESC,Retencioncompra.numero DESC";        
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
		$this->set('unidades', $this->paginate());
		$this->set(compact('title_for_layout'));
	}

	public function transito() {
		$this->checkSession();
		$title_for_layout='LISTA DE RETENCIONES EN TRANSITO REGISTRADOS ';
		$this->Retencioncompra->recursive = 0;
		$criteria = "Retencioncompra.tiporetencion='Transito' ";
		$order="Retencioncompra.codretencion DESC";        
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
		$this->set('unidades', $this->paginate());
		$this->set(compact('title_for_layout'));
	}
	
	public function viewpdf($id){
		$this->checkSession();
		$this->layout = 'pdf';						
		$empresa = $this->Empresa->find('first',array('conditions'=>''));	
		$encabezado=$this->Retencioncompra->getCabecera();
		$data = $this->Retencioncompra->read(null, $id);
		$data['Retencioncompra']['tipo_trans'] = '1 reg';
		//
		$datos_nc = $this->Nccompra->busquedas('Datos',$data);
		/*print_r ($datos_nc);
		die();*/
		$quincena = $this->Retencioncompra->getQuincena(date('d',strtotime($data['Retencioncompra']['fechaemision'])));		
		$this->set(compact('data','texto','empresa','encabezado','quincena','datos_nc'));
	}
	
}
?>
