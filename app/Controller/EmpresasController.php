<?php
class EmpresasController extends AppController
{
    public $name = 'Empresas';
	public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Empresa','Oficina','Grupo','Configuracion','Funcione','Perfile','Sucursal');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------	
	public function index(){
		$this->checkSession();
        $this->Empresa->recursive = 0;
		$this->set('data', $this->paginate());          
	}
	
	public function view($id){
		$this->checkSession();
		$this->set('data', $this->Empresa->read(NULL,$id));
	}
	
	public function add(){
		$this->checkSession();
		if(!empty($this->data)){
			$datos=$this->data;
			//----------------------------------------------------------
			if($datos['Empresa']['logoizq']['name']!=""){
			if (substr_count($datos['Empresa']['logoizq']['name'],'.')<>1){
				$this->Session->setFlash(__('El nombre del Archivo posee puntos (.). Por Favor Cambiar el nombre.', true));
				$this->set('data', $datos);
				$this->render();
			}
			$parte_nom=$this->generar_letras_aleatorias(5).'_';
			$datos['Empresa']['logo_izquierdo']=$this->Empresa->generar_nombre_archivo($parte_nom,$datos['Empresa']['logoizq']['name']);
			if($this->adjuntar_archivo($datos['Empresa']['logoizq']['tmp_name'],$this->filesLogo.$datos['Empresa']['logo_izquierdo'])){}
			}
			if($datos['Empresa']['logoder']['name']!=""){
			if (substr_count($datos['Empresa']['logoder']['name'],'.')<>1){
				$this->Session->setFlash(__('El nombre del Archivo posee puntos (.). Por Favor Cambiar el nombre.', true));
				$this->set('data', $datos);
				$this->render();
			}
			$parte_nom=$this->generar_letras_aleatorias(5).'_';
			$datos['Empresa']['logo_derecho']=$this->Empresa->generar_nombre_archivo($parte_nom,$datos['Empresa']['logoizq']['name'],'_ld');
			if($this->adjuntar_archivo($datos['Empresa']['logoder']['tmp_name'],$this->filesLogo.$datos['Empresa']['logo_derecho'])){}
			}
			//----------------------------------------------------------
			if ($this->Empresa->save($datos)){
				$this->flash('La Empresa ha sido Guardado.','/empresas');
			}else{
				$this->set('data', $this->data);
				$this->render('add');
			}
		}
		
		$this->set('sucursal', $this->Empresa->cargarBD());
		$this->set('nombreProyecto', $this->nombreProyecto);
	}
	
	public function edit($id=null){
		$this->checkSession();
		if (empty($this->data)){
			$this->data =  $this->Empresa->read(NULL,$id);
		}else{
			$datos=$this->data;
			//----------------------------------------------------------
			if($datos['Empresa']['logoizq']['name']!=""){
			if (substr_count($datos['Empresa']['logoizq']['name'],'.')<>1){
				$this->Session->setFlash(__('El nombre del Archivo posee puntos (.). Por Favor Cambiar el nombre.', true));
				$this->set('data', $datos);
				$this->render();
			}
			$parte_nom=$this->generar_letras_aleatorias(5).'_';
			$datos['Empresa']['logo_izquierdo']=$this->Empresa->generar_nombre_archivo($parte_nom,$datos['Empresa']['logoizq']['name']);
			if($this->adjuntar_archivo($datos['Empresa']['logoizq']['tmp_name'],$this->filesLogo.$datos['Empresa']['logo_izquierdo'])){}
			}
			if($datos['Empresa']['logoder']['name']!=""){
			if (substr_count($datos['Empresa']['logoder']['name'],'.')<>1){
				$this->Session->setFlash(__('El nombre del Archivo posee puntos (.). Por Favor Cambiar el nombre.', true));
				$this->set('data', $datos);
				$this->render();
			}
			$parte_nom=$this->generar_letras_aleatorias(5).'_';
			$datos['Empresa']['logo_derecho']=$this->Empresa->generar_nombre_archivo($parte_nom,$datos['Empresa']['logoizq']['name'],'_ld');
			if($this->adjuntar_archivo($datos['Empresa']['logoder']['tmp_name'],$this->filesLogo.$datos['Empresa']['logo_derecho'])){}
			}
			//----------------------------------------------------------
			if ($this->Empresa->save($datos)){
				$this->flash('La Empresa ha sido Actualizado.','/empresas');
			}else{
				$this->set('data', $this->data);
			}
		}
		$this->set('sucursal', $this->Empresa->cargarBD());
		$this->set('nombreProyecto', $this->nombreProyecto);
	}
	
	public function delete($id){
		$this->checkSession();
		$nro = $this->Oficina->find('count',array('conditions'=>' Oficina.empresa_id='.$id));
		if ($nro>0){
      		$this->flash('La Empresa con el id: '.$id.' no puede ser Eliminado porque tiene movimientos.','/empresas');
		}else{
			if ($this->Empresa->delete($id)){
      			$this->flash('La Empresa con el id: '.$id.' ha sido Eliminado.','/empresas');				
			}		
		}
	}

	public function buscar($id=NULL) {
		$criteria=" upper(Grupo.nombre) like upper('".$id."%')";
		if ($id==""){
			$this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/empresas');
			 $data=array();
		}else{
			$data = $this->Grupo->find('all',array('conditions'=>$criteria));  
		}
        $this->set('data',$data);
		$this->render('buscar', 'ajax');	
	}
	

	public function conexion(){
		$this->checkSession();
		$title_for_layout ='Actualizar Conexión de BD con las Sucursales';
        //$sucursal=$this->Empresa->llenar_combo('',5);
        $sucursal=$this->Sucursal->llenar_combo('',0);
        $this->set(compact('title_for_layout','sucursal'));
        $this->render('conexion');
	}
	
	function realizarajuste($id=null){
		$mensaje = array();
        $sucursal = $this->Sucursal->read(null,$id);
        
        $mensaje[0] = '<div class="alert alert-info">Ip P&uacute;blica enviada:'.$_POST['sucursal'].'</div>';
        $carpeta = 'CI';
        if ($sucursal['Sucursal']['ip']=='comerdepa2.ddns.net'){
			$carpeta = 'CII';
		}
        $mensaje[1] = $this->Empresa->traer_pg($sucursal['Sucursal']['ip'],$this->filesProyecto,$carpeta);
        $mensaje[2] = $this->Empresa->modificar_archivo($this->filesProyecto,$carpeta,$_POST['sucursal']);
        $mensaje[3] = $this->Empresa->enviar_pg($sucursal['Sucursal']['ip'],$this->filesProyecto,$carpeta);
        $mensaje[4] = $this->Empresa->reiniciar_pg($sucursal['Sucursal']['ip']);
        /*
         * 1.- Obtener Ip publica Actual +
         * 2.- Conectar y Traer el archivo pg_hba.conf del servidor remoto  +
         * 3.- Abrir Archivo buscar ultima linea y eliminar contenido y agregar nueva linea
         * 4.- Enviar Archivo al servidor remoto
         * 5.- Reiniciar el postgres del Servidor Remoto
         * */
         
        $this->set(compact('ip','sucursal','mensaje'));
        $this->render('realizarajuste', 'ajax');
        
    }    

}
?>
