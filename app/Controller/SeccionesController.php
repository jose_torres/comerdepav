<?php
class SeccionesController extends AppController
{
    public $name = 'Secciones';
	//public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
	public $helpers = array('Pagination','Html' => array(
        'className' => 'Bootstrap3.BootstrapHtml'
    ),'Form' => array(
        'className' => 'Bootstrap3.BootstrapForm'
    ),'Modal' => array(
        'className' => 'Bootstrap3.BootstrapModal'
    ));
	
	public $components = array ('Pagination'); 
    public $uses = array ('Seccione','Grupo','Configuracion','Funcione','Perfile');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------	
	public function index(){
		$this->checkSession();
        $this->Seccione->recursive = 0;
		$this->set('data', $this->paginate());          
	}
	
	public function view($id){
		$this->checkSession();
		$this->set('data', $this->Seccione->read(NULL,$id));
	}
	
	public function add(){
		$this->checkSession();
		if(!empty($this->data)){
			if ($this->Seccione->save($this->data)){
				$this->flash('La Seccion ha sido Guardado.','/secciones');
			}else{
				$this->set('data', $this->data);
				$this->render('add');
			}
		}		
		$this->set('nombreProyecto', $this->nombreProyecto);
	}
	
	public function edit($id=null){
		$this->checkSession();
		if (empty($this->data)){
			$this->data =  $this->Seccione->read(NULL,$id);
		}else{
			if ($this->Seccione->save($this->data)){
				$this->flash('La Seccion ha sido Actualizado.','/secciones');
			}else{
				$this->set('data', $this->data);
			}
		}
		$this->set('nombreProyecto', $this->nombreProyecto);
	}
	
	public function delete($id){
		$this->checkSession();
		$nro = $this->Funcione->find('count',array('conditions'=>' Funcione.grupo_id='.$id));
		if ($nro>0){
      		$this->flash('La Seccion con el id: '.$id.' no puede ser Eliminado porque tiene movimientos.','/secciones');
		}else{
			if ($this->Seccione->delete($id)){
      			$this->flash('La Seccion con el id: '.$id.' ha sido Eliminado.','/secciones');				
			}		
		}
	}

	public function buscar($id=NULL) {
		$criteria=" upper(Seccione.nombre) like upper('".$id."%')";
		if ($id==""){
			$this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/secciones');
			 $data=array();
		}else{
			$data = $this->Seccione->find('all',array('conditions'=>$criteria));  
		}
        $this->set('data',$data);
		$this->render('buscar', 'ajax');	
	}

}
?>
