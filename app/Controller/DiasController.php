<?php
class DiasController extends AppController
{
    public $name = 'Dias';
    public $helpers = array('Pagination');
    public $components = array ('Pagination'); 
    public $uses = array ('Dia','Funcione','Grupo','Configuracion','Perfile');
    
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function index(){
        $this->checkSession();
        $mensaje[0] = '';
        $title_for_layout ='Dias Feriados';	
        $this->set(compact('title_for_layout','mensaje'));
        $this->render('index');
    }

    public function buscardiasmes(){
        $datos = $_POST;
        $fecha['mes'] = $datos['mes']; $fecha['anio'] = $datos['anio'];
        $fecha['inicio'] = $this->first_month_day($datos['mes'],$datos['anio']);
        $fecha['fin'] = $this->last_month_day($datos['mes'],$datos['anio']);
        $fecha['dia_fin'] = substr($fecha['fin'],8,2);
        $dias_trans = 0;
        $nro_dias_mes = $this->getNroDayMonthAll($fecha);
        $data = $this->Dia->buscarDiasMes($datos);
        $fecha['feriados'] = $this->Dia->buscarDiasFeriados($datos);
        
        $this->set(compact('data','datos','fecha','nro_dias_mes','dias_trans'));
        $this->render('buscardiasmes', 'ajax');
    }

    public function guardarmarcados($id=0){
        $this->checkSession();
        $mensaje[0] = '';
        if(isset($this->data['Seleccionj'])){
            $mensaje[0] = $this->Dia->guardar_seleccionados($this->data['Seleccionj']);
        }		
        $title_for_layout ='Dias Feriados';	
        $this->set(compact('title_for_layout','mensaje'));
        $this->render('index');
    }

}
?>
