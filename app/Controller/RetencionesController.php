<?php
class RetencionesController extends AppController
{
    public $name = 'Retenciones';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Retencioncompra','Oficina','Grupo','Configuracion','Funcione','Perfile','Cliente','Vendedore','Proveedore','Compra','Cpabono','Cpabonopago','Empresa','Nccompra','Cpnotadebito');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------	
	public function add(){
		$this->checkSession();
		$title_for_layout='Registrar una Retenci&oacute;n';
		$proveedore=$this->Proveedore->llenar_combo();
		$this->set(compact('proveedore','title_for_layout'));		          
	}

	public function edit($id = null) {
		$this->checkSession();
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Retencion Invalida', true));
			$this->redirect(array('action' => '/'));
		}
		if (!empty($this->data)) {
			$datos=$this->data;
			if($datos['Retencioncompra']['tiporetencion']=='Normal'){
			$datos=$this->Retencioncompra->ajustarDatos('edit',$datos);
			if ($this->Retencioncompra->save($datos)) {
				$abonos = $this->Cpabonopago->busquedas('Uno',$datos);
				$abonos = array_merge($datos, $abonos);
				$this->Cpabono->actualizar($abonos);
				$this->Cpabonopago->actualizar($abonos);				
				$this->flash('La Retencion ha sido Guardado.','/retenciones/add');
			} else {
				$this->flash('La Retencion no se puede Guardar.','/retenciones/add');
			}
			}else{
				$this->flash('La Retencion no se puede Guardar porque no se ha colocado en Normal.','/retenciones/edit/'.$datos['Retencioncompra']['codretencion']);
			}
		}
		
		if (empty($this->data)) {
			$this->data = $this->Retencioncompra->read(null, $id);
		}
		
		//$this->set('nombreProyecto', $this->nombreProyecto);		
	}

	public function buscarfacturas($id=NULL) {
		$datos=$_POST;
		$data=$this->Retencioncompra->reporte($datos,'',2);
		$this->set(compact('data','datos'));
		$this->render('buscarfacturas', 'ajax');
	}

	public function calcularretencion($id=NULL) {
		//print_r($_POST);		
		$registros=$_POST;
		$registros['codmovimientos']=explode('-',$_POST['codmovimientos']);
		$registros['tipomovimientos']=explode('-',$_POST['tipomovimientos']);
		$registros['nrodocumentos']=explode('-',$_POST['nrodocumentos']);
		$data=$this->Compra->calcular_retencion_compras($registros);
		$facturas=$this->Retencioncompra->reporte($registros,'',3);	
		$this->set(compact('data','registros','facturas'));
		$this->render('calcularretencion', 'ajax');
	}	

	public function incluir($id=NULL) {
		$this->checkSession();
		if (!empty($this->data)) {
			$datos=$this->data;
			$datos=$this->Retencioncompra->ajustarDatos('add',$datos);
			$valor['Retencioncompra']=$datos['Retencioncompras'];
			/*echo '<pre>';			
			print_r($datos);
			echo '</pre>';
			die();*/
			if ($this->Retencioncompra->saveAll($valor['Retencioncompra'])) {
				$this->Retencioncompra->guardarAbono($datos);
				$this->flash('La Retencion ha sido Guardado.','/retenciones/add');
			} else {
				$this->flash('La Retencion no se puede Guardar.','/retenciones/add');
			}
		}
	}

	public function index() {
		$this->checkSession();
		$title_for_layout='LISTA DE RETENCIONES REGISTRADOS ';
		$proveedore=$this->Proveedore->llenar_combo();
		$this->Retencioncompra->recursive = 0;
		$criteria = "Retencioncompra.tiporetencion='Normal' ";
		$order="Retencioncompra.fechaemision DESC,Retencioncompra.numero DESC";        
		$campos= "Retencioncompra.numero,Proveedore.descripcion,Retencioncompra.fechaemision,sum(Retencioncompra.montoiva) as montoiva, sum(Retencioncompra.montoretenido) as montoretenido";
		$grupo= " Retencioncompra.numero,Proveedore.descripcion,Retencioncompra.fechaemision ";
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order,'group'=>$grupo,'fields'=>$campos);
		$this->set('unidades', $this->paginate());
		$this->set(compact('title_for_layout','proveedore'));
	}

	public function transito() {
		$this->checkSession();
		$title_for_layout='LISTA DE RETENCIONES EN TRANSITO REGISTRADOS ';
		$this->Retencioncompra->recursive = 0;
		$criteria = "Retencioncompra.tiporetencion='Transito' ";
		$order="Retencioncompra.codretencion DESC";        
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
		$this->set('unidades', $this->paginate());
		$this->set(compact('title_for_layout'));
	}
	
	public function viewpdf($id){
		$this->checkSession();
		$this->layout = 'pdf';						
		$empresa = $this->Empresa->find('first',array('conditions'=>''));	
		$encabezado=$this->Retencioncompra->getCabecera();
		
		//$data = $this->Retencioncompra->read(null, $id);
		$data = $this->Retencioncompra->find('all',array('conditions'=>" Retencioncompra.numero='".$id."'"));	
		$datos_nc = $this->Nccompra->busquedas('Datos',$data);
		$datos['numero']=$data[0]['Retencioncompra']['numero'];
		$datos_nd = $this->Cpnotadebito->reporte($datos);
		
		$quincena = $this->Retencioncompra->getQuincena(date('d',strtotime($data[0]['Retencioncompra']['fechaemision'])));		
		$this->set(compact('data','texto','empresa','encabezado','quincena','datos_nc','datos_nd'));
	}

	public function buscarretenciones($id=1){
		$datos=$_POST;
		$provee=explode('-',$_POST['proveedores']);
		$datos['provee']=$provee;
		$data = $this->Retencioncompra->findRetencion($datos);
       // print_r ($data);
       // die();
        $this->set(compact('data'));
        $this->render('buscarretenciones', 'ajax');
	}
	
}
?>
