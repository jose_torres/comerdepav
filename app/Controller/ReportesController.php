<?php
class ReportesController extends AppController
{
    public $name = 'Reportes';
    public $helpers = array('Pagination','fpdf'/*,'Ajax'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Venta','Oficina','Grupo','Configuracion','Funcione','Perfile','Cliente','Vendedore','Proveedore','Compra','Deposito','Empresa','Ventaproducto','Devolucionventa','Devolucioncompra','Producto','Departamento','Sucursal','Cargo','Descargo');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------	
    public function ventas(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $this->set(compact('cliente','vendedores','sucursal'));         
    }

    public function clientes(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $this->set(compact('cliente','vendedores','sucursal'));         
    }

    public function buscarvendedoresxsuc($id='1-1'){
        $this->checkSession();		
        list($id_empresa,$conexion)=explode('-',$id);
        Configure::write('Model.globalSource', $conexion);		
        $vendedores=$this->Vendedore->llenar_combo();
        $this->set(compact('vendedores'));		          
        $this->render('buscarvendedoresxsuc', 'ajax');
    }

    public function buscarclientesxsuc($id='1-1'){
        $this->checkSession();		
        list($id_empresa,$conexion)=explode('-',$id);
        Configure::write('Model.globalSource', $conexion);		
        $cliente=$this->Cliente->llenar_combo();
        $this->set(compact('cliente'));		          
        $this->render('buscarclientesxsuc', 'ajax');
    }	      
          
    public function buscarventas($id=NULL) {
        //print_r($_POST);
        //useDbConfig = 'comerdepa_3'
        $datos=$_POST;$client=explode('-',$_POST['clientes']);		
        $vend=explode('-',$_POST['vendedores']);$tipoven=explode('-',$_POST['tipoventa']);
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Ventas_Diarias':
            $titulo='Reporte de Ventas Realizadas';
            $tipoven=explode('-','0');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;

            $data=$this->Venta->reporte($datos,'',3);
            $data_dev=$this->Devolucionventa->reporte($datos,'',0);
    //	print_r ($datos);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasdiarias', 'ajax');
        break;
        case 'Ventas_Diarias_Detallada':
            $titulo='Reporte de Ventas Realizadas Detallada';			
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;

            $data=$this->Venta->reporte($datos,'',2);
            $data_dev=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventascreditodet', 'ajax');
        break;
        case 'Ventas_Contados':
            $titulo='Reporte de Ventas Realizadas de Contado';
            $tipoven=explode('-','0-P');	
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Venta->reporte($datos);
            $data_dev=$this->Devolucionventa->reporte($datos,'',0);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventascredito', 'ajax');
        break;
        case 'Ventas_Contados_Detallada':
            $titulo='Reporte de Ventas Realizadas de Contado Detallada';
            $tipoven=explode('-','0-P');	
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Venta->reporte($datos,'',2);
            $data_dev=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventascreditodet', 'ajax');
        break;
        case 'Ventas_Creditos':
            $titulo='Reporte de Ventas Realizadas de Cr&eacute;dito';
            $tipoven=explode('-','0-A');	
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Venta->reporte($datos,'',0);
            $data_dev=$this->Devolucionventa->reporte($datos,'',0);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventascredito', 'ajax');
        break;
        case 'Ventas_Creditos_Detallada':
            $titulo='Reporte de Ventas Realizadas de Cr&eacute;dito Detallada';
            $tipoven=explode('-','0-A');	
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            //$this->Venta->useDbConfig=$datos['sucursal'];
            //$this->Devolucionventa->useDbConfig=$datos['sucursal'];
            $data=$this->Venta->reporte($datos,'',2);
            $data_dev=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventascreditodet', 'ajax');
        break;
        case 'Ventas_Devueltos':
            $titulo='Reporte de Devoluciones en Ventas';
            $tipoven=explode('-','0-E');	
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            //$this->Venta->useDbConfig=$datos['sucursal'];
            //$data=$this->Venta->reporte($datos);
            $data=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','titulo'));
            $this->render('buscarventas', 'ajax');
        break;
        case 'Ventas_Resumen_Cobros':
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            //$this->Venta->useDbConfig=$datos['sucursal'];			
            if ($id_empresa == 5){
                $data=$this->Venta->reporte($datos,'',41);
            }else{
                $data=$this->Venta->reporte($datos,'',4);
            }
            $this->set(compact('data'));
            $this->render('buscarrelventascobros', 'ajax');
        break;
        case 'Ventas_Resumen_Cxc':
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            //$this->Venta->useDbConfig=$datos['sucursal'];	
            $data=$this->Venta->reporte($datos,'',5);
            $this->set(compact('data'));
            $this->render('buscarventasresumencxc', 'ajax');
        break;
        case 'Ventas_Cobros':
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            //$this->Venta->useDbConfig=$datos['sucursal'];	
            $data=$this->Venta->reporte($datos,'',1);
            $this->set(compact('data'));
            $this->render('buscarventascobros', 'ajax');
        break;
        case 'Ventas_Cobros_Contado':
            $tipoven=explode('-','0-P');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            //$this->Venta->useDbConfig=$datos['sucursal'];	
            $data=$this->Venta->reporte($datos,'',1);
            $this->set(compact('data'));
            $this->render('buscarventascobros', 'ajax');
        break;
        case 'Ventas_Cobros_Credito':
            $tipoven=explode('-','0-A');
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            //$this->Venta->useDbConfig=$datos['sucursal'];	
            $data=$this->Venta->reporte($datos,'',1);
            $this->set(compact('data'));
            $this->render('buscarventascobros', 'ajax');
        break;
        case 'Ventas_Relacion_Cobros':
            $titulo='Relaci&oacute;n de Cobros desde '.$datos['fechadesde'].' hasta '.$datos['fechahasta'];
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            if ($id_empresa == 5){
                $data=$this->Venta->reporte($datos,'',61);
            }else{
                $data=$this->Venta->reporte($datos,'',6);
            }

            $this->set(compact('data','titulo'));
            $this->render('buscarcobrosventas', 'ajax');
        break;
        case 'Ventas_Transacciones':
            $titulo='REPORTE DE TRANSACCIONES PROCESADAS';
            //$tipoven=explode('-','0-A');	
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;		
            $data=$this->Venta->reporte($datos,'',7);
            $data_dev=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventastransaccion', 'ajax');
        break;
        case 'Ventas_Vendedor':
            $titulo='Reporte de Ventas por Vendedor';			
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;			
            $data=$this->Venta->reporte($datos,'',8);
            $data_dev=$this->Devolucionventa->reporte($datos,'',2);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasvendedor', 'ajax');
        break;
        case 'Listado_Cliente':
            $titulo='Listado de Clientes';			
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;			
            $data=$this->Cliente->reporte($datos,'',0);
            $this->set(compact('data','titulo'));
            $this->render('buscarlistadocliente', 'ajax');
        break;

        }

    }

    public function viewpdfventas($id){
        $this->checkSession();
        $this->layout = 'pdf';				
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Ventas_Diarias':
            $encabezado['titulo_reporte']='REPORTE DE VENTAS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $tipoven=explode('-','0');
    
            $datos = $this->Venta->ajustarDatos('pdf',$datos);
            $data=$this->Venta->reporte($datos,'',3);
            $data_dev=$this->Devolucionventa->reporte($datos,'',0);
            $deposito=$this->Deposito->llenar_combo();
    
            $this->set(compact('data','titulo','data_dev','empresa','encabezado','deposito'));
            $this->render('viewpdfventasdiarias');
        break;	
        case 'Ventas_Diarias_Detallada':
            $encabezado['titulo_reporte']='REPORTE DE VENTAS DETALLADA DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];		
            $datos = $this->Venta->ajustarDatos($opcion='pdf',$datos);			
            $data=$this->Venta->reporte($datos,'',2);
            $deposito=$this->Deposito->llenar_combo();
            $data_dev=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','data_dev','empresa','encabezado','deposito'));
            $this->render('viewpdfventascreditodet');
        break;
        case 'Ventas_Creditos':			
            $encabezado['titulo_reporte']='REPORTE DE VENTAS A CR&Eacute;DITO DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $tipoven=explode('-','0-A');$datos['tipoventa']=$tipoven;			
            $datos = $this->Venta->ajustarDatos($opcion='pdf',$datos);
            $data=$this->Venta->reporte($datos,'',0);
            $data_dev=$this->Devolucionventa->reporte($datos,'',0);			
            $deposito=$this->Deposito->llenar_combo();
            $this->set(compact('data','data_dev','empresa','encabezado','deposito'));
            $this->render('viewpdfventascredito');
        break;
        case 'Ventas_Creditos_Detallada':
            $encabezado['titulo_reporte']='REPORTE DE VENTAS A CR&Eacute;DITO DETALLADA DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $tipoven=explode('-','0-A');$datos['tipoventa']=$tipoven;			
            $datos = $this->Venta->ajustarDatos($opcion='pdf',$datos);
            $data=$this->Venta->reporte($datos,'',2);						
            $data_dev=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','data_dev','empresa','encabezado','deposito'));
            $this->render('viewpdfventascreditodet');
        break;
        case 'Ventas_Devueltos':
            $encabezado['titulo_reporte']='REPORTE DE DEVOLUCION EN VENTAS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $tipoven=explode('-','0-E');	
            $datos['tipoven']=$tipoven;
            $datos = $this->Venta->ajustarDatos($opcion='pdf',$datos);
            $data_dev = $this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data_dev','empresa','encabezado','deposito'));
            $this->render('viewpdfdevolucionventa');
        break;
        case 'Ventas_Contados':			
            $encabezado['titulo_reporte']='REPORTE DE VENTAS AL CONTADO DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $tipoven=explode('-','0-P');$datos['tipoventa']=$tipoven;			
            $datos = $this->Venta->ajustarDatos($opcion='pdf',$datos);
            $data=$this->Venta->reporte($datos,'',0);
            $data_dev=$this->Devolucionventa->reporte($datos,'',0);
            $deposito=$this->Deposito->llenar_combo();
            $this->set(compact('data','data_dev','empresa','encabezado','deposito'));
            $this->render('viewpdfventascontado');
        break;
        case 'Ventas_Contados_Detallada':
            $encabezado['titulo_reporte']='REPORTE DE VENTAS AL CONTADO DETALLADA DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $tipoven=explode('-','0-P');$datos['tipoventa']=$tipoven;
            $datos = $this->Venta->ajustarDatos($opcion='pdf',$datos);
            $data=$this->Venta->reporte($datos,'',2);						
            $data_dev=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','data_dev','empresa','encabezado','deposito'));
            $this->render('viewpdfventascontadodet');
        break;
        case 'Ventas_Resumen_Cobros':
            $encabezado['titulo_reporte']='REPORTE DE RELACION DE VENTAS Y COBROS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            //$tipoven=explode('-','0-P');$datos['tipoventa']=$tipoven;
            $datos = $this->Venta->ajustarDatos('pdf',$datos);
            //$this->Venta->useDbConfig=$datos['sucursal'];
            if ($id_empresa == 5){
                $data=$this->Venta->reporte($datos,'',41);
            }else{
                $data=$this->Venta->reporte($datos,'',4);
            }						
            $this->set(compact('data','empresa','encabezado'));
            $this->render('viewpdfventasrelcobros');
        break;
        case 'Ventas_Resumen_Cxc':
            $encabezado['titulo_reporte']='RESUMEN DE CUENTAS POR COBRAR DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Venta->ajustarDatos('pdf',$datos);
            //$this->Venta->useDbConfig=$datos['sucursal'];			
            $data=$this->Venta->reporte($datos,'',5);
            $this->set(compact('data','empresa','encabezado'));
            $this->render('viewpdfventasresumencxc');
        break;
        case 'Ventas_Relacion_Cobros':
            $encabezado['titulo_reporte']='REPORTE DE RELACION DE  COBROS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Venta->ajustarDatos('pdf',$datos);		
            if ($id_empresa == 5){
                $data=$this->Venta->reporte($datos,'',61);
            }else{
                $data=$this->Venta->reporte($datos,'',6);
            }
            $this->set(compact('data','empresa','encabezado'));
            $this->render('viewpdfcobrosventas');
        break;
        case 'Ventas_Transacciones':
            $encabezado['titulo_reporte']='REPORTE DE TRANSACCIONES PROCESADAS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Venta->ajustarDatos('pdf',$datos);				
            $data=$this->Venta->reporte($datos,'',7);
            $data_dev=$this->Devolucionventa->reporte($datos,'',1);
            $this->set(compact('data','data_dev','empresa','encabezado'));			
            $this->render('viewpdfventastransaccion');
        break;
        case 'Ventas_Vendedor':			
            $encabezado['titulo_reporte']='REPORTE DE VENTAS POR VENDEDOR DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Venta->ajustarDatos('pdf',$datos);			
            $data=$this->Venta->reporte($datos,'',8);
            $data_dev=$this->Devolucionventa->reporte($datos,'',0);			
            $this->set(compact('data','data_dev','empresa','encabezado'));			
            $this->render('viewpdfventasvendedor');
        break;
        case 'Listado_Cliente':
            $encabezado['titulo_reporte']='LISTADO DE CLIENTES';
            $datos = $this->Venta->ajustarDatos('pdf',$datos);			
            $data=$this->Cliente->reporte($datos,'',0);
            $this->set(compact('data','empresa','encabezado'));
            $this->render('viewpdflistadocliente', 'ajax');
        break;
         default:
            $this->redirect(array('action' => 'ventas'));
        }				
    }

    public function compras(){
        $this->checkSession();
        $this->Compra->recursive = 0;
        $proveedore=$this->Proveedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $this->set(compact('proveedore','sucursal'));         
    }

    public function buscarcompras($id=NULL) {
        //print_r($_POST);
        $datos=$_POST;
        $provee=explode('-',$_POST['proveedores']);
        $tipoven=explode('-',$_POST['tipoventa']);
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Compras_General':
            $titulo='Reporte de Compras Realizadas';
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Compra->reporte($datos);
            $data_dev=$this->Devolucioncompra->reporte($datos,'',0);
            $deposito=$this->Deposito->llenar_combo();
            $this->set(compact('data','titulo','deposito','data_dev'));
            $this->render('buscarcomprasdiarias', 'ajax');
        break;
        case 'Compras_Creditos':
            $tipoven=explode('-','0-A');
            $titulo='Reporte de Compras Realizadas a Cr&eacute;dito';
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Compra->reporte($datos);
            $data_dev=$this->Devolucioncompra->reporte($datos,'',0);	
            $deposito=$this->Deposito->llenar_combo();
            $this->set(compact('data','titulo','deposito','data_dev'));
            $this->render('buscarcompras', 'ajax');
        break;
        case 'Compras_Devueltos':
            $tipoven=explode('-','0-E');
            $titulo='Reporte de Compras Realizadas a Devueltas';
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Compra->reporte($datos);
            $deposito=$this->Deposito->llenar_combo();
            $this->set(compact('data','titulo','deposito'));
            $this->render('buscarcompras', 'ajax');
        break;
        case 'Compras_Cobros':
            //echo $datos['tiporeporte'];
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Compra->reporte($datos,'',1);
            $this->set(compact('data'));
            $this->render('buscarcomprascobros', 'ajax');
        break;
        case 'Compras_Pagos':
            //echo $datos['tiporeporte'];
            $titulo='Reporte de Compras Realizadas a Devueltas';
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Compra->reporte($datos,'',3);
            $this->set(compact('data','titulo'));
            $this->render('buscarcompraspagos', 'ajax');
        break;
        case 'Compras_Transacciones':
            $titulo='REPORTE DE COMPRAS TRANSACCIONES PROCESADAS';			
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Compra->reporte($datos,'',4);			
            $this->set(compact('data','titulo'));
            $this->render('buscarcomprastransaccion', 'ajax');
        break;
        }
    }

    public function viewpdf($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        //$empresa = $this->Empresa->find('first',array('conditions'=>' id=1'));	
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        Configure::write('Model.globalSource', $conexion);

        switch ($datos['tiporeporte']) {
        case 'Compras_General':			
            $encabezado['titulo_reporte']='REPORTE COMPRAS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];			
            $datos = $this->Compra->ajustarDatos('pdf',$datos);
            $data = $this->Compra->reporte($datos);
            $data_dev=$this->Devolucioncompra->reporte($datos,'',0);			
            $deposito=$this->Deposito->llenar_combo();			
            $this->set(compact('data','empresa','encabezado','quincena','deposito','data_dev'));
            $this->render('viewpdfcomprasdiarias');
        break;
        case 'Compras_Creditos':			
            $encabezado['titulo_reporte']='REPORTE COMPRAS A CR&Eacute;DITOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $tipoven=explode('-','0-A');$datos['tipoventa']=$tipoven;			
            $datos = $this->Compra->ajustarDatos('pdf',$datos);
            $data = $this->Compra->reporte($datos);
            $data_dev=$this->Devolucioncompra->reporte($datos,'',0);			
            $deposito=$this->Deposito->llenar_combo();			
            $this->set(compact('data','empresa','encabezado','quincena','deposito','data_dev'));
            $this->render('viewpdfcomprascreditos');
        break;
        case 'Compras_Pagos':
            $encabezado['titulo_reporte']='REPORTE PAGOS DE COMPRAS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];			
            $datos = $this->Compra->ajustarDatos('pdf',$datos);
            $data=$this->Compra->reporte($datos,'',3);
            $this->set(compact('data','empresa','encabezado','deposito'));
            $this->render('viewpdfcompraspagos');
        break;
        case 'Compras_Transacciones':			
            $encabezado['titulo_reporte']='REPORTE DE COMPRAS TRANSACCIONES PROCESADAS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];			
            $datos = $this->Compra->ajustarDatos('pdf',$datos);
            $data=$this->Compra->reporte($datos,'',4);						
            $this->set(compact('data','empresa','encabezado'));
            $this->render('viewpdfcomprastransaccion', 'ajax');
        break;
         default:
            $this->redirect(array('action' => 'compras'));
        }				
    }
// Relacionado a Inventarios
    public function inventario(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',1);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Consolidado de Inventario';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function precios(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',1);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Listado de Precios';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function costoutilidad(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',1);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $this->set(compact('producto','departamento','sucursal','deposito'));         
    }
       
    public function buscarinventario($id=NULL) {
            //print_r($_POST);
        $datos=$_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $dep=explode('-',$_POST['depositos']);$vercosto=explode('-',$_POST['vercosto']);
        $precio=explode('-',$_POST['precio']);
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Inv_Existencia':
            $titulo='Existencia de Art&iacute;culos';
            $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
            //print_r($datos);
            $data=$this->Producto->reporte($datos,'',4);
            $deposito=$this->Deposito->llenar_combo();
            $this->set(compact('data','titulo','deposito','data_dev','precio','datos','vercosto'));
            $this->render('buscarinvexistencia', 'ajax');
        break;
        case 'Inv_Costo':
            $titulo='Costos vs Utilidad';
            $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
            $datos['precio']=$precio;            
            $data=$this->Producto->reporte($datos,'',1);
            $deposito=$this->Deposito->llenar_combo();
            $this->set(compact('data','titulo','deposito','data_dev','datos'));
            $this->render('buscarinvcostoutil', 'ajax');
        break;
        case 'Inv_Precio':
            
            $titulo='Existencia de Art&iacute;culos';
            $datos['prod']=$prod;$datos['depto']=$depto;$datos['dep']=$dep;
            $datos['precio']=$precio;			
            $data=$this->Producto->reporte($datos,'',4);
            $deposito=$this->Deposito->llenar_combo();

            $this->set(compact('data','titulo','deposito','data_dev','datos'));
            $this->render('buscarinvprecio', 'ajax');
        break;
        //buscarinvcostoutil

        }
    }

    public function viewpdfinv($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        //$empresa = $this->Empresa->find('first',array('conditions'=>' id=1'));			
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        Configure::write('Model.globalSource', $conexion);

        switch ($datos['tiporeporte']) {
        case 'Inv_Existencia':
            $encabezado['titulo_reporte']='Existencia de Art&iacute;culos';
            $datos = $this->Producto->ajustarDatos('pdf',$datos);              
            $data=$this->Producto->reporte($datos,'',4);			
            $deposito=$this->Deposito->llenar_combo();                
            $this->set(compact('data','empresa','encabezado','deposito','datos'));    
            $this->render('viewpdfinvconsolidado');
        break;
        case 'Inv_Costo':
            $encabezado['titulo_reporte']='Costo vs Utilidad';
            $datos = $this->Producto->ajustarDatos('pdf',$datos);	
            $data=$this->Producto->reporte($datos,'',1);            
            $this->set(compact('data','empresa','encabezado'/*,'deposito'*/,'datos'));
            $this->render('viewpdfinvcostoutil1');
        break;
        case 'Inv_Precio':
            $encabezado['titulo_reporte']='Listado de Precio de Productos';
            $datos = $this->Producto->ajustarDatos('pdf',$datos);
            $data=$this->Producto->reporte($datos,'',4);
            $deposito=$this->Deposito->llenar_combo();
            $this->set(compact('data','empresa','encabezado','deposito','datos'));
            //$this->render('viewpdfinvprecio');
            $this->render('viewpdfinvprecio1');
        break;
         default:
            $this->redirect(array('action' => 'inventario'));
        }				
    }
// Relacionado a Proveedores
    public function proveedores(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $proveedore=$this->Proveedore->llenar_combo();
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',1);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $this->set(compact('producto','departamento','sucursal','deposito','proveedore'));         
    }

    public function buscarproveedores($id=NULL) {
        //print_r($_POST);
        $datos=$_POST;
        $provee=explode('-',$_POST['proveedores']);
        $tipoven=explode('-',$_POST['tipoventa']);
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Proveedore_Listado':
            $titulo='Listado de Proveedores';
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Proveedore->reporte($datos);			
            $this->set(compact('data','titulo'));
            $this->render('buscarlistadoproveedores', 'ajax');
        break;
        case 'Proveedore_Cxp':
            $titulo='Cuentas por Pagar al d&iacute;a '.$datos['fechahasta'];
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Proveedore->reporte($datos,'',1);			
            $this->set(compact('data','titulo'));
            $this->render('buscarcuentasporpagar', 'ajax');
        break;
        case 'Compras_Pagos':
            $titulo='Relaci&oacute;n de Pagos desde el d&iacute;a '.$datos['fechadesde'].' haste el d&iacute;a '.$datos['fechahasta'];
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Proveedore->reporte($datos,'',2);
            $this->set(compact('data','titulo'));
            $this->render('buscarcompraspagos', 'ajax');
        break;
        case 'Proveedore_Estado':
            $titulo='Estado de Cuentas desde el d&iacute;a '.$datos['fechadesde'].' haste el d&iacute;a '.$datos['fechahasta'];
            $datos['provee']=$provee;$datos['tipoven']=$tipoven;
            $data=$this->Proveedore->reporte($datos,'',3);
            $saldo=$this->Proveedore->calcularSaldo($datos['fechadesde']);
            $this->set(compact('data','titulo','saldo'));
            $this->render('buscarprovestadocuenta', 'ajax');
        break;

        }
    }
// Vendedores
    public function vendedores(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $this->set(compact('cliente','vendedores','sucursal'));         
    }	
	
    public function buscarvendedores($id=NULL) {
        //print_r($_POST);
        //useDbConfig = 'comerdepa_3'
        $datos=$_POST;$client=explode('-',$_POST['clientes']);		
        $vend=explode('-',$_POST['vendedores']);$tipoven=explode('-',$_POST['tipoventa']);
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Ventas_Vendedor':
            $titulo='Reporte de Ventas por Vendedor';			
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Venta->reporte($datos,'',8);
            $data_dev=$this->Devolucionventa->reporte($datos,'',2);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasvendedor', 'ajax');
        break;
        case 'Ventas_Vendedor_Det':
            $titulo='Reporte de Ventas por Vendedor';
            $tipoven=explode('-','0-A-P');				
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Vendedore->reporte($datos,'',1);
            //$data_dev=$this->Devolucionventa->reporte($datos,'',2);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasvendedordet', 'ajax');
        break;
        case 'Ventas_Cobros':
            $titulo='Reporte de Ventas por Vendedor';
            $tipoven=explode('-','0-A-P');				
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;	$data=$this->Vendedore->reporte($datos,'',2);
            //$data_dev=$this->Devolucionventa->reporte($datos,'',2);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasvendcobros', 'ajax');
        break;
        case 'Ventas_Cobros_Det':
            $titulo='Reporte de Ventas por Vendedor';
            $tipoven=explode('-','0-A-P');				
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Vendedore->reporte($datos,'',2);
            $data_dev=$this->Devolucionventa->reporte($datos,'',2);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasvendcobrosdet', 'ajax');
        break;
        case 'Ventas_Vendedor_Pro':
            $titulo='Ventas de Vendedores por Productos';
            $tipoven=explode('-','0-A-P');				
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;	
            $data=$this->Vendedore->reporte($datos,'',3);
            //$data_dev=$this->Devolucionventa->reporte($datos,'',2);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasvendprod', 'ajax');
        break;
        case 'Ventas_Vendedor_Dep':
            $titulo='Ventas de Vendedores por Departamentos';
            $tipoven=explode('-','0-A-P');				
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;	
            $data=$this->Vendedore->reporte($datos,'',4);
            //$data_dev=$this->Devolucionventa->reporte($datos,'',2);
            $this->set(compact('data','titulo','data_dev'));
            $this->render('buscarventasvenddep', 'ajax');
        break;
        case 'Listado_Vendedor':
            $titulo='Listado de Vendedores';			
            $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
            $data=$this->Vendedore->reporte($datos,'',0);
            $this->set(compact('data','titulo'));
            $this->render('buscarlistadovendedores', 'ajax');
        break;
        }
    }

    public function impuestos(){
        $this->checkSession();
        $this->Venta->recursive = 0;
        $cliente=$this->Cliente->llenar_combo();
        $vendedores=$this->Vendedore->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $this->set(compact('cliente','vendedores','sucursal'));         
    }

    public function buscarimpuestos($id=NULL) {
        //print_r($_POST);
        $datos=$_POST;        
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Iva_Dia':
            $titulo='Resumen de Iva por D&iacute;a';                                
            $data=$this->Venta->resumenIvaVentas($datos,'');                
            $this->set(compact('data','titulo'));
            $this->render('buscarivapordia', 'ajax');
        break;    
        case 'Libro_Ventas':
            $titulo='Libro de Ventas';
            $tipoven=explode('-','0');
            //$datos['client']=$client;$datos['vend']=$vend;
            //$datos['tipocontribuyente']=$tipocontribuyente;
            $data=$this->Venta->reporteImpuestos($datos,'',1);
            $data_iva=$this->Ventaproducto->buscariva();
            //$data_dev=$this->Devolucionventa->reporte($datos,'',0);
            $this->set(compact('data','titulo','data_dev','data_iva'));
            if($datos['tipocontribuyente']=='ordinario'){ 
                $this->render('buscarlibroventas', 'ajax');
            }else{
                $this->render('buscarlibroventasesp', 'ajax');
            }
        break;
        case 'Libro_Compras':
            $titulo='Libro de Compras';
            $tipoven=explode('-','0');
            //$datos['client']=$client;$datos['vend']=$vend;
            //$datos['tipocontribuyente']=$tipocontribuyente;
            $data=$this->Compra->reporteImpuestos($datos,'',1);
            //$data_dev=$this->Devolucionventa->reporte($datos,'',0);
            $this->set(compact('data','titulo','data_dev'));
            if($datos['tipocontribuyente']=='ordinario'){ 
                $this->render('buscarlibrocompras', 'ajax');
            }else{
                $this->render('buscarlibrocomprasesp', 'ajax');
            }
        break;
        }
    }

    public function viewpdfimpuestos($id){
        $this->checkSession();
        $this->layout = 'pdf';						
        //$empresa = $this->Empresa->find('first',array('conditions'=>' id=1'));
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        Configure::write('Model.globalSource', $conexion);

        switch ($datos['tiporeporte']){
        case 'Iva_Dia':            
            $encabezado['titulo_reporte']='Resumen de Iva por D&iacute;a';
            $datos = $this->Venta->ajustarDatos('impuesto',$datos);
            $data=$this->Venta->resumenIvaVentas($datos,'');                   
            $this->set(compact('data','empresa','encabezado','deposito','datos'));
            $this->render('viewpdfivapordia');
        break;      
        case 'Libro_Ventas':
            $encabezado['titulo_reporte']='Libro de Ventas';
            $datos = $this->Venta->ajustarDatos('impuesto',$datos);
            $data=$this->Venta->reporteImpuestos($datos,'',1);
            $datos['MES']=$this->nombre_mes($datos['mes']);
            $this->set(compact('data','empresa','encabezado','deposito','datos'));

            if($datos['tipocontribuyente']=='ordinario'){ 
                    $this->render('viewpdflibroventas');
            }else{
                    $this->render('viewpdflibroventasesp');
            }
        break;
        case 'Libro_Compras':
            $encabezado['titulo_reporte']='Libro de Compras';
            $datos = $this->Venta->ajustarDatos('impuesto',$datos);
            $data=$this->Compra->reporteImpuestos($datos,'',1);
            $datos['MES']=$this->nombre_mes($datos['mes']);
            $this->set(compact('data','empresa','encabezado','deposito','datos'));

            if($datos['tipocontribuyente']=='ordinario'){ 
                    $this->render('viewpdflibrocompras');
            }else{
                    $this->render('viewpdflibrocomprasesp');
            }
        break;
        default:
            $this->redirect(array('action' => 'impuestos'));
        }				
    }
        
    public function ventasxdpto(){
        $this->checkSession();
        
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Ventas por Departamentos';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    } 
    
    public function buscarventasxdpto() {
		
        $datos=$_POST;
        list($id_empre,$conexion)=explode('-',$datos['sucursal']);
        $datos['dpto']=explode('-',$_POST['departamentos']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {
        case 'Res_Dpto':
            $titulo='Resumido por Departamento';              
            $data=$this->Venta->resumenVentasPorDpto($datos,'');                        
            $this->set(compact('data','titulo'));            
            $this->render('buscarventasxdpto', 'ajax');            
        break;
        case 'Det_Dpto':
            $titulo='Ventas por Departamento Detallado por Productos';              
            $data=$this->Venta->detalladoVentasPorDpto($datos,'');                        
            $this->set(compact('data','titulo'));            
            $this->render('buscarventasxdptodet', 'ajax');            
        break;
        
        }
    }
    
    public function viewpdfventasxdpto($id){
        $this->checkSession();
        $this->layout = 'pdf';        			
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        Configure::write('Model.globalSource', $conexion);

        switch ($datos['tiporeporte']){
        case 'Res_Dpto':
            $encabezado['titulo_reporte']='RESUMEN DE VENTAS POR DEPARTAMENTOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Venta->ajustarDatos('pdf',$datos);			
            $data=$this->Venta->resumenVentasPorDpto($datos,'');  			
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfventasxdpto');
        break;
       case 'Det_Dpto':
            $encabezado['titulo_reporte']='RESUMEN DE VENTAS POR PRODUCTOS Y DEPARTAMENTOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Venta->ajustarDatos('pdf',$datos);			
            $data=$this->Venta->detalladoVentasPorDpto($datos,'');  			
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfventasxdptodet');
        break;       
        }				
    }
    
    public function cargos(){
        $this->checkSession();        
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Cargos Realizados';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function buscarcargos() {
		
        $datos=$_POST;
        list($id_empre,$conexion)=explode('-',$datos['sucursal']);
        $datos['dpto']=explode('-',$_POST['departamentos']);
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {        
        case 'List_Cargo':
            $titulo='Listado de Cargos Realizados';              
            $data=$this->Cargo->listadoCargo($datos,'');                        
            $this->set(compact('data','titulo'));            
            $this->render('buscarcargos', 'ajax');            
        break;        
        case 'List_Cargo_Res':
            $titulo='Listado de Cargos Realizados';              
            $data=$this->Cargo->listadoCargoResumen($datos);                        
            $this->set(compact('data','titulo'));            
            $this->render('buscarcargosres', 'ajax');            
        break;
        case 'List_Descargo':
            $titulo='Listado de Descargos Realizados';              
            $data=$this->Descargo->listadoDescargo($datos,'');                        
            $this->set(compact('data','titulo'));            
            $this->render('buscardescargos', 'ajax');            
        break;
        case 'List_Descargo_UI':
            $titulo='Listado de Descargos de Uso Internos Realizados';              
            $data=$this->Descargo->listadoDescargo($datos," and V.usointerno='true' ");
            $this->set(compact('data','titulo'));            
            $this->render('buscardescargos', 'ajax');            
        break;
        case 'List_Descargo_Res':
            $titulo='Listado de Descargos Realizados';              
            $data=$this->Descargo->listadoDescargoResumen($datos);
            $this->set(compact('data','titulo'));            
            $this->render('buscardescargosres', 'ajax');            
        break;
        case 'List_Descargo_Res_UI':
            $titulo='Listado de Descargos de Uso Internos Realizados';              
            $data=$this->Descargo->listadoDescargoResumen($datos," and V.usointerno='true' ");
            $this->set(compact('data','titulo'));            
            $this->render('buscardescargosres', 'ajax');            
        break;
        
        }
    }
    
    public function viewpdfcargos($id){
        $this->checkSession();
        $this->layout = 'pdf';        			
        $datos = $this->data['Reporte'];
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        Configure::write('Model.globalSource', $conexion);

        switch ($datos['tiporeporte']){
        case 'List_Cargo':
            $encabezado['titulo_reporte']='LISTADO DE CARGOS REALIZADOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Cargo->ajustarDatos('pdf',$datos);			
            $data=$this->Cargo->listadoCargo($datos,'');  			
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfcargos');
        break;
        case 'List_Cargo_Res':
            $encabezado['titulo_reporte']='LISTADO DE CARGOS REALIZADOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Cargo->ajustarDatos('pdf',$datos);           	
            $data=$this->Cargo->listadoCargoResumen($datos);
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfcargosres');
        break;
        case 'List_Descargo':
            $encabezado['titulo_reporte']='LISTADO DE DESCARGOS REALIZADOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Descargo->ajustarDatos('pdf',$datos);			
            $data=$this->Descargo->listadoDescargo($datos,''); 			
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfdescargos');
        break;
        case 'List_Descargo_UI':
            $encabezado['titulo_reporte']='LISTADO DE DESCARGOS DE USO INTERNO REALIZADOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Descargo->ajustarDatos('pdf',$datos);			
            $data = $this->Descargo->listadoDescargo($datos," and V.usointerno='true' ");
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfdescargos');
        break; 
        case 'List_Descargo_Res':
            $encabezado['titulo_reporte']='LISTADO DE DESCARGOS REALIZADOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Descargo->ajustarDatos('pdf',$datos);			
            $data = $this->Descargo->listadoDescargoResumen($datos); 			
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfdescargosres');
        break; 
        case 'List_Descargo_Res_UI':
            $encabezado['titulo_reporte']='LISTADO DE DESCARGOS DE USO INTERNO REALIZADOS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Descargo->ajustarDatos('pdf',$datos);			
            $data=$this->Descargo->listadoDescargoResumen($datos," and V.usointerno='true' "); 			
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfdescargosres');
        break; 
        }				
    }
    
    public function historial(){
        $this->checkSession();        
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',2);
        $deposito=$this->Deposito->llenar_combo();
        $sucursal=$this->Empresa->llenar_combo('',1);
        $title_for_layout='Operaciones de Inventarios';
        $this->set(compact('producto','departamento','sucursal','deposito','title_for_layout'));         
    }
    
    public function buscarhistorial() {
		
        $datos=$_POST;
        list($id_empre,$conexion)=explode('-',$datos['sucursal']);
        $datos['dpto']=explode('-',$_POST['departamentos']);
        $datos['prod']=explode('-',$_POST['productos']);        
        Configure::write('Model.globalSource', $conexion);
        switch ($datos['tiporeporte']) {        
        case 'Mov_x_Productos':
            $titulo='Operaciones Realizados';              
            $data=$this->Producto->historialProducto($datos);                        
            $this->set(compact('data','titulo'));            
            $this->render('buscarhistorial', 'ajax');            
        break;
        case 'Mov_x_Fechas':
            $titulo='Operaciones Realizados';               
            $data=$this->Producto->historialProducto($datos);            
            $this->set(compact('data','titulo'));            
            $this->render('buscarhistorial', 'ajax');            
        break;
        
        }
    }
    
    public function viewpdfhistorial($id){
        $this->checkSession();
        $this->layout = 'pdf';        			
        $datos = $this->data['Reporte'];        
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$id_empresa));
        Configure::write('Model.globalSource', $conexion);

        switch ($datos['tiporeporte']){
        case 'Mov_x_Productos':
            $encabezado['titulo_reporte']='LISTADO DE OPERACIONES REALIZADAS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Producto->ajustarDatos('pdf',$datos);			
            $data=$this->Producto->historialProducto($datos);  			
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfhistorial');
        break;
       case 'Mov_x_Fechas':
            $encabezado['titulo_reporte']='LISTADO DE OPERACIONES REALIZADAS DESDE '.$this->data['Reporte']['desde'].' HASTA '.$this->data['Reporte']['hasta'];
            $datos = $this->Producto->ajustarDatos('pdf',$datos);			
            $data=$this->Producto->historialProducto($datos); 			
            $this->set(compact('data','empresa','encabezado'));			
            $this->render('viewpdfhistorial');
        break;       
        }				
    }
    
}
?>
