<?php
class ProductosController extends AppController
{
    public $name = 'Productos';
    public $helpers = array('Pagination');
    public $components = array ('Pagination'); 
    public $uses = array ('Producto','Funcione','Grupo','Configuracion','Perfile','Empresa','Sucursal','Departamento');
    
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function index(){
        $this->checkSession();
        $mensaje[0] = '';
        $title_for_layout ='Productos';	
        $this->set(compact('title_for_layout','mensaje'));
        $this->render('index');
    }
    
    public function activar(){
        $this->checkSession();
        $mensaje[0] = '';
        $title_for_layout ='Activaci&oacute;n de Productos';
        $sucursal=$this->Empresa->llenar_combo('',5);
        $this->set(compact('title_for_layout','mensaje','sucursal'));
        $this->render('activar');
    }

    public function activacion(){
        $datos = $_POST;
        list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
	Configure::write('Model.globalSource', $conexion);
        $mensaje[0] = $this->Producto->activar();        
        $this->set(compact('mensaje','datos'));
        $this->render('activacion', 'ajax');
    }
    
    public function actualizar(){
        $this->checkSession();
        $mensaje[0] = '';
        $title_for_layout ='Actualizar Precios de Productos entre Principal a la Sucursal';
        //$sucursal=$this->Empresa->llenar_combo('',5);
        $sucursal=$this->Sucursal->llenar_combo('',0);
        $this->set(compact('title_for_layout','mensaje','sucursal'));
        $this->render('actualizar');
    }
    
    public function preciosenlote(){
        $this->checkSession();
        $mensaje[0] = '';
        $title_for_layout ='Actualizar Precios de Productos en Lotes';
        //$sucursal=$this->Empresa->llenar_combo('',5);
        $sucursal=$this->Sucursal->llenar_combo('',0);
        $producto=$this->Producto->llenar_combo('',1);
        $departamento=$this->Departamento->llenar_combo('',1);
        $this->set(compact('title_for_layout','mensaje','sucursal','producto','departamento'));
        $this->render('preciosenlote');
    }
    
    public function actualizarprecios(){
        $datos = $_POST;
        $sucursal = $this->Sucursal->read(null,$datos['sucursal']);
        list($nro,$mensaje[0]) = $this->Producto->nroproductosnvos($sucursal);
        $mensaje[1] = $this->Producto->transferirproductos($sucursal);
        list($nro,$mensaje[2]) = $this->Producto->nroproductoscambiados($sucursal);
        $mensaje[3] = $this->Producto->actualizarproductos($sucursal);
        $this->set(compact('mensaje','datos'));
        $this->render('actualizarprecios', 'ajax');
    }
    
    public function listadoproducto(){
        $datos = $_POST;
        $prod=explode('-',$_POST['productos']);$depto=explode('-',$_POST['departamentos']);
        $datos['prod']=$prod;$datos['depto']=$depto;       
        $data=$this->Producto->reporte($datos,'',3);
        $this->set(compact('data','datos'));
        $this->render('listadoproducto', 'ajax');
    }

    public function guardarmarcados($id=0){
        $this->checkSession();
        $mensaje[0] = '';
        if(isset($this->data['Seleccionj'])){
            $mensaje[0] = $this->Dia->guardar_seleccionados($this->data['Seleccionj']);
        }		
        $title_for_layout ='Dias Feriados';	
        $this->set(compact('title_for_layout','mensaje'));
        $this->render('index');
    }

}
?>
