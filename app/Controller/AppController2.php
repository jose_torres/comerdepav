<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	//	var $helpers = array('Session','numerosaletras');
	public $nombreProyecto = "comerdepav2";
//	public $filesProyecto = "/var/www/comerdepav2/app/webroot/files/archivos/";
//	public $filesLogo = "/var/www/comerdepav2/app/webroot/img/logo/";
	public $filesProyecto = "/var/www/html/comerdepav2/app/webroot/files/archivos/";
	public $filesLogo = "/var/www/html/comerdepav2/app/webroot/img/logo/";
	public $theme = "CakeAdminLTE";
	public $CONEXION = "comerdepa_3";
	public $ID_EMPRESA = 4;
	public $CONEXION_MAYOR = "comerdepa";
	public $ID_EMPRESA_MAYOR = 1;
	
	function checkSession(){
		if(!$this->Session->check('Usuario')){
			$this->redirect('/usuarios/login');
		}
	}

	function adjuntar_archivo($dirtmp,$dirfinal)
	{
		if(!copy($dirtmp,$dirfinal))		
		{
		//	echo "error al copiar el archivo";
			return false;
		}
		else
		{
		//	echo "archivo subido con exito";
			return true;
		}
	}
/*
 * 
 * @return string Ej. '$operador1 ( condicion1 $operador2 condicion2 $operador2...condicionN )'
 * @param $datos Object //trae el arreglo de los datos a convertir en un criterio 
 * @param $campoCondicion Object //Se le pasa el campo con el modelo a mostrar Ej. Modelo.campo
 * @param $campo Object // nombre del campo del arreglo de donde se extrae el valor Ej. $datos["id"]
 * @param $operador1 Object  // Se coloca un operador and o or para la primera condicion
 * @param $operador2 Object	// Se coloca un operador and o or para las condiciones siguiente
 */	
	function construircriterio($datos,$campoCondicion,$campo,$operador1,$operador2){
		$criterio='';$i=1;
		foreach($datos as $reg){
			if($i==1){
				$criterio=' '.$operador1.' ('.$campoCondicion.'='.$reg[$campo].' ';		
			}else{
				$criterio=$criterio.' '.$operador2.' '.$campoCondicion.'='.$reg[$campo].' ';
			}
			$i=$i+1;
		}
		if ($criterio!='')	$criterio=$criterio.') ';
		return $criterio;
	}
/*
 * 
 * @return string Ej. 2008-12-12
 * @param $fecha Time Ej. '12-12-2008'
 */
	function anomesdia($fecha){
		//list($d,$m,$a)=split('[-/]',$fecha);
		list($d,$m,$a)=explode('-',$fecha);
		return trim($a).'-'.$m.'-'.$d;
	}	
/*
 Convierte las letras con acento y caracteres especiales a formato html

*/
	function convertLatin1ToHtml($str) {
	    $html_entities = array (
		"&" =>  "&amp;",     #ampersand  
		"á" =>  "&aacute;", 
		"é" =>  "&eacute;", 
		"í" =>  "&iacute;", 
		"ó" =>  "&oacute;", 
		"ú" =>  "&uacute;", 
		"Á" =>  "&Aacute;", 
		"É" =>  "&Eacute;", 
		"Í" =>  "&Iacute;", 
		"Ó" =>  "&Oacute;", 
		"Ú" =>  "&Uacute;",
		"ñ" =>  "&ntilde;",
		"Ñ" =>  "&Ntilde;"
		
	);

	    foreach ($html_entities as $key => $value) {
		$str = str_replace($key, $value, $str);
	    }
	    return $str;
	}

/**/
 /*************************************
 Devuelve una cadena con la fecha que se 
 le manda como parámetro en formato largo.
 *************************************/
	function FechaFormateada($FechaStamp){
		
		$ano = date('Y',strtotime($FechaStamp)); //<-- Año
		$mes = date('m',strtotime($FechaStamp)); //<-- número de mes (01-31)
		$dia = date('d',strtotime($FechaStamp)); //<-- Día del mes (1-31)
	//	$dialetra = date('w',$FechaStamp);  //Día de la semana(0-7)
	/*	switch($dialetra){
			case 0: $dialetra="Domingo"; break;
			case 1: $dialetra="Lunes"; break;
			case 2: $dialetra="Martes"; break;
			case 3: $dialetra="Miércoles"; break;
			case 4: $dialetra="Jueves"; break;
			case 5: $dialetra="Viernes"; break;
			case 6: $dialetra="Sábado"; break;
		}*/
		switch($mes) {
			case '01': $mesletra="Enero"; break;
			case '02': $mesletra="Febrero"; break;
			case '03': $mesletra="Marzo"; break;
			case '04': $mesletra="Abril"; break;
			case '05': $mesletra="Mayo"; break;
			case '06': $mesletra="Junio"; break;
			case '07': $mesletra="Julio"; break;
			case '08': $mesletra="Agosto"; break;
			case '09': $mesletra="Septiembre"; break;
			case '10': $mesletra="Octubre"; break;
			case '11': $mesletra="Noviembre"; break;
			case '12': $mesletra="Diciembre"; break;
		}    
		return " $dia de $mesletra del a&ntilde;o $ano";
	}
/*
 * 
 * @return string Ej. 2
 * @param $fechaMayor Time Ej. '2008-12-14'
* @param $fechaMenor Time Ej. '2008-12-12'
   Devuelve en numero los dias de diferencias entre dos fechas
*/
	function restaFechas($fechaMayor,$fechaMenor){

		list($ano1,$mes1,$dia1)=split('-',$fechaMayor);
		list($ano2,$mes2,$dia2)=split('-',$fechaMenor);

		//calculo timestam de las dos fechas
		$timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1);
		$timestamp2 = mktime(0,0,0,$mes2,$dia2,$ano2);

		//resto a una fecha la otra
		$segundos_diferencia = $timestamp1 - $timestamp2;
		//echo $segundos_diferencia;

		//convierto segundos en días
		$dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

		//obtengo el valor absoulto de los días (quito el posible signo negativo)
		$dias_diferencia = abs($dias_diferencia);

		//quito los decimales a los días de diferencia
		$dias_diferencia = floor($dias_diferencia);

		return $dias_diferencia;


	}
/*
 * 
 * @return string Ej. '001'
 * @param $cadena int Ej. '1' Texto a la cual se le van a agregar los ceros
 * @param $cantidad int Ej. '3' Longitud de la cadena 

   Devuelve en numero con ceros a la izquierda
*/
	function completarceros($cadena,$cantidad){
		$hasta=$cantidad-strlen($cadena);
		for ($i = 1; $i <= $hasta; $i++) {
		    $cadena='0'.$cadena;
		}
		return $cadena;
	}

/*
 * 
 * @return string Ej. '001'
 * @param $cadena int Ej. '1' Texto a la cual se le van a agregar los ceros
 * @param $cantidad int Ej. '3' Longitud de la cadena 

   Devuelve los nombres de los numeros en mayusculas pero no lo conectores
*/
    function mayusculas_oraciones($title) {
        $smallwordsarray = array(
            'y','con',
        );

        $words = explode(' ', $title);
        foreach ($words as $key => $word)
        {
            if ($key == 0 or !in_array($word, $smallwordsarray))
            $words[$key] = ucwords(strtolower($word));
        }

        $newtitle = implode(' ', $words);
        return $newtitle;
    } 
/**
 * Shows a message to the user for $pause seconds, then redirects to $url.
 * Uses flash.ctp as the default layout for the message.
 * Does not work if the current debug level is higher than 0.
 *
 * @param string $message Message to display to the user
 * @param mixed $url Relative string or array-based URL to redirect to after the time expires
 * @param integer $pause Time to show the message
 * @param string $layout Layout you want to use, defaults to 'flash'
 * @return void Renders flash layout
 * @access public
 * @link http://book.cakephp.org/view/983/flash
 */
	function confirm($message,$urlSi, $urlNo, $pause = 1, $layout = 'confirm',$seguir='N') {
		$this->autoRender = false;
		$this->set('urlNo', Router::url($urlNo));
		$this->set('urlSi', Router::url($urlSi));
		$this->set('message', $message);
		$this->set('seguir', $seguir);
		$this->set('pause', $pause);
		$this->set('page_title', $message);
		$this->render(false, $layout);
	}
/*
 * @param integer $cantDatos Número de registros de los datos encontrado
 * @param string $mensneg Muestra el mensaje cuando no consigue datos a mostrar
 */
	function c_mensaje($cantDatos=0,$mensNeg='No se ha conseguido Registro. Por favor selecciones otros datos.'){
		$mensaje=$mensNeg;
		$s='s';
		if($cantDatos==1){ $s='';}
		if($cantDatos>=1){
			$mensaje = ' Se han conseguido '.$cantDatos.' registro'.$s.'';
		}		
		return $mensaje;
	}
/*
 * @param integer $mes Número del mes a convertir
 * */
	function nombre_mes($mes=01){
		switch($mes) {
			case '01': $mesletra="Enero"; break;
			case '02': $mesletra="Febrero"; break;
			case '03': $mesletra="Marzo"; break;
			case '04': $mesletra="Abril"; break;
			case '05': $mesletra="Mayo"; break;
			case '06': $mesletra="Junio"; break;
			case '07': $mesletra="Julio"; break;
			case '08': $mesletra="Agosto"; break;
			case '09': $mesletra="Septiembre"; break;
			case '10': $mesletra="Octubre"; break;
			case '11': $mesletra="Noviembre"; break;
			case '12': $mesletra="Diciembre"; break;
		}
		return $mesletra;
	}
/*
 *
 * */
	function construir_caption($opciones=array(),$titulo_constante=''){
		$caption='';
		if($opciones['selperiodo']=='true' or $opciones['selperiodo']==1){
				$caption=$titulo_constante." desde ".$opciones['desde']." hasta el ".$opciones['hasta'];
			}else{
				$caption=$titulo_constante." del mes ".$opciones['mes']." del a&ntilde;o ".$opciones['ano'];
			}
		return	$caption;
	}
/*
 *	@param string $campo Número en formato español Ej. 2.300,24
 * */
	function formato_ingles($campo=''){
		$par1=str_replace(".","", $campo);
		$par2=str_replace(",",".", $par1);
		$campo=$par2;
		return $campo;
	}
/*
 *
 * */
	function generar_letras_aleatorias($numerodeletras=10){
		$caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"; //posibles caracteres a usar
		//$numerodeletras=10; //numero de letras para generar el texto
		$cadena = ""; //variable para almacenar la cadena generada
		for($i=0;$i<$numerodeletras;$i++)
		{
			$cadena .= substr($caracteres,rand(0,strlen($caracteres)),1); /*Extraemos 1 caracter de los caracteres 
		entre el rango 0 a Numero de letras que tiene la cadena */
		}
		return $cadena;

	}
/*
 * @param string $filename Nombre del Archivo a Crear
 * @param array $files Arreglos de Archivos con sus direcciones a Comprimir
 * */
	function compress_file($filename='test.zip',$files=array()){
		$zip = new ZipArchive();
		$ejecutado = false;
		if($zip->open($filename,ZipArchive::CREATE)===true) {
			foreach ($files as $reg) {
				//echo 'File:'.$reg['file'].' Local:'.$reg['localname'].'<br>';
				$zip->addFile(''.$reg['file'],''.$reg['localname']);
				//$zip->addFile(''.$reg['file']);
			}
			//$zip->setArchiveComment('Generado por SISCOM');				
			$zip->close();
			$ejecutado = true;
			//print_r($files);
			$mensaje = '<div class="alert alert-success">Creado el archivo '.$filename.'</div>';
		}else{
			$mensaje = '<div class="alert alert-danger">Error creando el archivo '.$filename.'</div>';
		}
		return array($ejecutado,$mensaje);
	}
/*
 *	@param string $dirfile Direccion del Archivo Comprimido
 *	@param string $dirextract Direccion de la Carpeta donde el archivo sera descomprimido 
 * */
	function decompress_file($dirfile='',$dirextract=''){
		$zip = new ZipArchive();
		$ejecutado = false;
		if($zip->open($dirfile)===true) {
			$zip->extractTo($dirextract);
			$zip->close();
			$ejecutado = true;
			$mensaje = '<div class="alert alert-success">El archivo ha sido descomprimido correctamente</div>';
		} else {
			$mensaje = '<div class="alert alert-danger">Error al descomprimir</div>';
		}
		return array($ejecutado,$mensaje);
	}
	/** Actual month last day **/
	function last_month_day($mes='1',$anio='2018') { 
	  $month = date($mes);
	  $year = date($anio);
	  $day = date("d", mktime(0,0,0, $month+1, 0, $year));

	  return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
	}
 
	/** Actual month first day **/
	function first_month_day($mes='1',$anio='2018') {
	  $month = date($mes);
	  $year = date($anio);
	  return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
	}
	
	/** Obtener Nro de dia de la Semana en el Mes **/
	function getNroDayMonth($fecha = array(),$dia_semana=1){
		$cont = 0;
		for ($i = 1; $i <= $fecha['dia_fin']; $i++) {
			$fecha_actual = $fecha['anio'].'-'.$fecha['mes'].'-'.str_pad($i, 2, "0", STR_PAD_LEFT);
			if (date('N', strtotime($fecha_actual))==$dia_semana){
				$cont = $cont + 1;
			}
		}	
		return $cont;
	}
	
	/** Obtener Nro por dia en el Mes **/
	function getNroDayMonthAll($fecha = array()){
		$dia_actual = $cont[1] = $cont[2] = $cont[3] = $cont[4] = $cont[5] = $cont[6] = $cont[7] = 0;
		for ($i = 1; $i <= 7; $i++) {
			$cont[$i] = $this->getNroDayMonth($fecha,$i); 
		}			
		return $cont;
	}
}
