<?php
class McuadrediariosController extends AppController {

    public $name = 'Mcuadrediarios';
    public $helpers = array('Html', 'Form'/*,'Pagination','Ajax','fpdf'*/);
    public $components = array ('Pagination');
    public $uses = array ('Mcuadrediario','Funcione','Grupo','Configuracion','Empleado','Perfile','Empresa','Movbancario','Sucgasto','Banco','Venta','Ventaproducto','Retencionventa','Retencionventacab','Ventaspago','Devolucionventa','Devolucionventaproducto','Devolucionventapago','Devolucionventacreditopago','Cliente','Vendedore','Maquina','Dinerodenominacione','Documentotipo','Puntodeventa','Suctran','Succheque','Cuentasbancaria','Sucdeposito','Suctarjeta','Cargo','Cargoproducto','Descargo','Descargoproducto');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------

    function index() {
            $criteria='';  
            $this->Mcuadrediario->recursive = 0;
            $sucursal=$this->Empresa->llenar_combo('',3);
            $this->set(compact('sucursal'));
            $order=' Mcuadrediario.fecha desc';
            $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
            $title_for_layout='COBROS DE FACTURAS A CR&Eacute;DITOS';
            $this->set(compact('title_for_layout'));
            $this->set('data', $this->paginate());
    }

    function transferir() {
            $criteria='';     	

            $this->Mcuadrediario->recursive = 0;
            $sucursal=$this->Empresa->llenar_combo('',2);
            $this->set(compact('sucursal'));
            $order=' Mcuadrediario.fecha desc';
            $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
            $this->set('data', $this->paginate());
    }

    function exportar($id = null){
            Configure::write('Model.globalSource', $this->CONEXION_MAYOR ) ;
            $cuadre=$this->Mcuadrediario->read(null, $id);
            $cuadre['Mcuadrediario']['codsucursal'] = $this->ID_EMPRESA_MAYOR;
            $linea = '';// $filesProyecto
            $archivo = "Cuadre_Mayor_".date('d-m-Y',strtotime($cuadre['Mcuadrediario']['fecha']))."_suc_".$cuadre['Mcuadrediario']['codsucursal'].".txt";
            $fp = fopen($this->filesProyecto."cuadres/".$archivo, "w+");
//		fputs($fp, "--Generado por SISCOM --\n");
            //$fec = $cuadre['Mcuadrediario']['fecha'];
            $fec = $this->Mcuadrediario->encriptar($cuadre['Mcuadrediario']['fecha']);
            fputs($fp, $fec."\n");
            //$suc = $cuadre['Mcuadrediario']['codsucursal'];
            $suc = $this->Mcuadrediario->encriptar($cuadre['Mcuadrediario']['codsucursal']);
            fputs($fp, $suc."\n");
            $linea = $this->Mcuadrediario->generar_linea_txt($cuadre);
            $linea = $linea.$this->Movbancario->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Sucgasto->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Venta->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Ventaproducto->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Ventaspago->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Retencionventacab->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Retencionventa->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Devolucionventa->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Devolucionventaproducto->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Devolucionventapago->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Devolucionventacreditopago->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Cargo->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Cargoproducto->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $linea.$this->Descargo->generar_linea_txt($cuadre,'Mcuadrediario');		
            $linea = $linea.$this->Descargoproducto->generar_linea_txt($cuadre,'Mcuadrediario');		
            $linea = $linea.$this->Cliente->generar_linea_txt($cuadre,'Mcuadrediario');
            $linea = $this->Mcuadrediario->encriptar($linea);
            fputs($fp, $linea);
            fclose($fp);
            $filename = "Cuadre_Mayor_".date('d-m-Y',strtotime($cuadre['Mcuadrediario']['fecha']))."_suc_".$cuadre['Mcuadrediario']['codsucursal'].".zip";
            $files[]=array('file'=>$this->filesProyecto."cuadres/".$archivo,'localname'=>$archivo); // Archivos a Comprimir
            list($ej,$mensaje) = $this->compress_file($this->filesProyecto."cuadres/".$filename,$files);
            $title_for_layout='COBROS DE FACTURAS A CR&Eacute;DITOS';
            $this->set(compact('archivo','filename','mensaje','title_for_layout'));
    }

    function view($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Invalid Cuadrediario.', true));
                    $this->redirect(array('action'=>'index'));
            }
            Configure::write('Model.globalSource', $this->CONEXION_MAYOR );
            $this->Mcuadrediario->actualizarMontosCuadre($id);
            $cuadre=$this->Mcuadrediario->read(null, $id);
            $cuadre2['Cuadrediario'] = $cuadre['Mcuadrediario'];
            $depositos = $this->Movbancario->buscarDepositos($cuadre2);			
            $gastos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$cuadre['Mcuadrediario']['fecha']."' "));
            $retenciones['Total'] = $this->Retencionventa->buscarResumenDia($cuadre2);
            $retenciones['Registros'] = $this->Retencionventa->buscarRegistrosDia($cuadre2);
            $ventadeldiac = $this->Venta->ventasContado(" and VT.fecha='".$cuadre['Mcuadrediario']['fecha']."'");
            if(!isset($ventadeldiac[0][0]['total'])){
                    $ventadeldiac[0][0]['total']=0;
            }
            $ventadeldia = $this->Venta->ventasCredito(" and VT.fecha='".$cuadre['Mcuadrediario']['fecha']."'");
            if(!isset($ventadeldia[0][0]['total'])){
                    $ventadeldia[0][0]['total']=0 + $ventadeldiac[0][0]['total'];
            }else{
                    $ventadeldia[0][0]['total']=$ventadeldia[0][0]['total'] + $ventadeldiac[0][0]['total'];
            }
            $title_for_layout='COBROS DE FACTURAS A CR&Eacute;DITOS';
            $this->set(compact('cuadre','depositos','gastos','ventadeldia','retenciones','title_for_layout'));

    }

    function viewpdf($id = null) {

            $this->layout = 'pdf';
            $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$this->ID_EMPRESA_MAYOR));
            Configure::write('Model.globalSource', $this->CONEXION_MAYOR);
            $cuadre=$this->Mcuadrediario->read(null, $id);
            $cuadre2['Cuadrediario'] = $cuadre['Mcuadrediario'];
            $encabezado['titulo_reporte']='Resumen de Cuadre Diario de Fecha '.date('d-m-Y',strtotime($cuadre['Mcuadrediario']['fecha']));
            $depositos = $this->Movbancario->buscarDepositos($cuadre2);		
            $gastos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$cuadre['Mcuadrediario']['fecha']."' "));
            $retenciones['Registros'] = $this->Retencionventa->buscarRegistrosDia($cuadre2);
            $ventadeldia = $this->Venta->ventasCredito(" and VT.fecha='".$cuadre['Mcuadrediario']['fecha']."'");
            if(!isset($ventadeldia[0][0]['total'])){
                    $ventadeldia[0][0]['total']=0;
            } 
            $this->set(compact('cuadre','depositos','gastos','ventadeldia','retenciones','empresa','encabezado'));
            $this->render('viewpdf');
    }

    function viewdet($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Invalid Cuadrediario.', true));
                    $this->redirect(array('action'=>'index'));
            }
            $this->set('cuadrediario', $this->Mcuadrediario->read(null, $id));
            $cuadre=$this->Mcuadrediario->read(null, $id);
            $efectivos=$this->Efectivo->total_efectivo($id);
            $cheques=$this->Cheque->total_cheque($id,1);

            $movbancarios = $this->Movbancario->find('all',array('conditions'=>" (Movbancario.documentotipo_id=4 ) and Movbancario.cuadrediario_id='".$id."' and Movbancario.monto>0 ",'order'=>'Banco.descripcion')); 
            $movnotadebito = $this->Movbancario->find('all',array('conditions'=>" ( Movbancario.documentotipo_id=5) and Movbancario.cuadrediario_id='".$id."' ",'order'=>'Banco.descripcion')); 
            $tarjetas=$this->Tarjeta->total_tarjeta($id);
            $cupones=$this->Cupone->total_cupone($id);
            $cupones_elect=$this->Tarjeta->total_tarjeta($id,'CUPON');
            $fondos=$this->Fondo->find('all',array('conditions'=>' Fondo.cuadrediario_id='.$id.' ','order'=> ' Fondo.caja_id '));
            $prestamos=$this->Prestamo->total_prestamo($id,1);
            //$compragastos=$this->Compragasto->total_compragasto($id," and tipocompragastos.credito='NO' ");
            $compragastos=$this->Compragasto->total_compragasto($id," and (tipocompragastos.descorta like '%CAJ%') and tipocompragastos.credito like 'NO%' and tipocompragastos.descorta not like '%CH%'"," and Movbancario.fecha='".$cuadre['Mcuadrediario']['fecha']."' ",1);
            $compragastos_cre=$this->Compragasto->total_compragasto($id," and tipocompragastos.credito='SI' ",'',1);
            $zreportes=$this->Zreporte->total_zreporte($id);

            $this->set(compact('efectivos','cheques','tarjetas','cupones','cupones_elect','fondos','prestamos','compragastos','compragastos_cre', 'zreportes','movbancarios','movnotadebito'));

    }

    function viewdetpdf($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Cuadrediario Invalido.', true));
                    $this->redirect(array('action'=>'index'));
            }
            $this->layout = 'pdf';
            $this->set('cuadrediario', $this->Mcuadrediario->read(null, $id));
            $cuadre=$this->Mcuadrediario->read(null, $id);
            $efectivos=$this->Efectivo->total_efectivo($id);
            $cheques=$this->Cheque->total_cheque($id,1);

            $movbancarios = $this->Movbancario->find('all',array('conditions'=>" (Movbancario.documentotipo_id=4 ) and Movbancario.cuadrediario_id='".$id."' and Movbancario.monto>0 ",'order'=>'Banco.descripcion')); 
            $movnotadebito = $this->Movbancario->find('all',array('conditions'=>" ( Movbancario.documentotipo_id=5) and Movbancario.cuadrediario_id='".$id."' ",'order'=>'Banco.descripcion')); 
            $tarjetas=$this->Tarjeta->total_tarjeta($id);
            $cupones=$this->Cupone->total_cupone($id);
            $cupones_elect=$this->Tarjeta->total_tarjeta($id,'CUPON');
            $fondos=$this->Fondo->find('all',array('conditions'=>' Fondo.cuadrediario_id='.$id.' ','order'=> ' Fondo.caja_id '));
            $prestamos=$this->Prestamo->total_prestamo($id);
    //	$compragastos=$this->Compragasto->total_compragasto($id," and tipocompragastos.credito='NO' ");
            $compragastos=$this->Compragasto->total_compragasto($id," and (tipocompragastos.descorta like '%CAJ%') and tipocompragastos.credito like 'NO%' and tipocompragastos.descorta not like '%CH%'"," and Movbancario.fecha='".$cuadre['Mcuadrediario']['fecha']."' ",1);
            $compragastos_cre=$this->Compragasto->total_compragasto($id," and tipocompragastos.credito='SI' ",'',1);
            $zreportes=$this->Zreporte->total_zreporte($id);

            $this->set(compact('efectivos','cheques','tarjetas','cupones','cupones_elect','fondos','prestamos','compragastos','compragastos_cre','zreportes','movbancarios','movnotadebito'));

            //--------------------- Inicializacion del pdf ------------------------------
            $this->set('titulo', '');
            $this->set('logoIzq', 'LOGOCARNICERIA.jpg');
            $this->set('logoDer', 'LOGOCARNICERIA.jpg');
            $this->set('line1','CARNICERIA Y CHARCUTERIA ALTAGRACIA C.A.');
            $this->set('line2','RIF.:J-08517249-1');
            //$this->set('line3','Desde: '.$par2.'  Hasta: '.$par3);
            $this->set('line3',' Reporte de Cuadre Diario');
            $this->set('line4',' Fecha: '.date('d-m-Y',strtotime($cuadre['Mcuadrediario']['fecha'])));

    }

    function viewdetalles($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Invalid Cuadrediario.', true));
                    $this->redirect(array('action'=>'index'));
            }

            $this->set('cuadrediario', $this->Mcuadrediario->read(null, $id));
            $cuadre=$this->Mcuadrediario->read(null, $id);
            //$efectivos=$this->Efectivo->total_efectivo($id);
            $datos=array('desde'=>$cuadre['Mcuadrediario']['fecha'],'hasta'=>$cuadre['Mcuadrediario']['fecha'],'lote'=>'','caja'=>'','banco'=>'');
            $efectivos = $this->Efectivo->reporte($datos,array(),2);
            $cheques = $this->Cheque->reporte($datos,array(),2);
            $legal = $this->Cccobranza->reporte($datos," and V.tipocc='legal'",4);
            $personal = $this->Cccobranza->reporte($datos," and V.tipocc='personal'",4);	

            $this->set(compact('efectivos','cheques','legal','personal'));

    }

    function viewdetallespdf($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Invalid Cuadrediario.', true));
                    $this->redirect(array('action'=>'index'));
            }
            $this->layout = 'pdf';
            $this->set('cuadrediario', $this->Mcuadrediario->read(null, $id));
            $cuadre=$this->Mcuadrediario->read(null, $id);
            //$efectivos=$this->Efectivo->total_efectivo($id);
            $datos=array('desde'=>$cuadre['Mcuadrediario']['fecha'],'hasta'=>$cuadre['Mcuadrediario']['fecha'],'lote'=>'','caja'=>'','banco'=>'');
            $efectivos = $this->Efectivo->reporte($datos,array(),2);
            $cheques = $this->Cheque->reporte($datos,array(),2);
            $legal = $this->Cccobranza->reporte($datos," and V.tipocc='legal'",4);
            $personal = $this->Cccobranza->reporte($datos," and V.tipocc='personal'",4);	

            $this->set(compact('efectivos','cheques','legal','personal','cuadre'));
            //--------------------- Inicializacion del pdf ------------------------------
            $this->set('titulo', '');
            $this->set('logoIzq', '');
            $this->set('logoDer', 'LOGOCARNICERIA_P.jpg');
            $this->set('line1','CARNICERIA Y CHARCUTERIA ALTAGRACIA C.A. RIF.:J-08517249-1');
            $this->set('line3','');
            //$this->set('line3','Desde: '.$par2.'  Hasta: '.$par3);
            $this->set('line2',' Reporte Resumen de Cuadre Diario de Fecha: '.date('d-m-Y',strtotime($cuadre['Mcuadrediario']['fecha'])));
            $this->set('line4','');

    }

    function add($id = null) {

            $nro=$this->Mcuadrediario->find('all',array('conditions'=>' Mcuadrediario.estatus=0 ','fields'=>'Mcuadrediario.nro','order'=> 'id desc'));

            if(count($nro)>0){
                    $this->Session->setFlash(__('Posee un Cuadre diarios abierto', true));
                    $this->redirect(array('action'=>'index'));
            }else{
                    $ult_nro=$this->Mcuadrediario->find('first',array('conditions'=>' Mcuadrediario.estatus=1 ','fields'=>'Mcuadrediario.nro','order'=> 'nro desc'));
            }
            $this->set(compact('ult_nro'));
            if (!empty($this->data)) {
                    //$this->Mcuadrediario->create();
                    if ($this->Mcuadrediario->save($this->data)) {
                            $this->Session->setFlash(__('El Cuadre diario ha sido creado', true));
                            $this->redirect(array('action'=>'index'));
                    } else {
                            $this->Session->setFlash(__('The Cuadrediario could not be saved. Please, try again.', true));
                    }
            }
    }

    function edit($id = null) {
            if (!$id && empty($this->data)) {
                    $this->Session->setFlash(__('Invalid Cuadrediario', true));
                    $this->redirect(array('action'=>'index'));
            }
            if (!empty($this->data)) {
                    if ($this->Mcuadrediario->save($this->data)) {
                            $this->flash('El Cuadre diario ha sido Actualizada.','/mcuadrediarios/index');
                    } else {
                            $this->Session->setFlash(__('El Cuadre diario no se guardo. Por favor, intente otra vez.', true));
                    }
            }
            if (empty($this->data)) {
                    $this->data = $this->Mcuadrediario->read(null, $id);
            }
            $title_for_layout='COBROS DE FACTURAS A CR&Eacute;DITOS';
            $this->set(compact('title_for_layout'));
    }


    function cerrar($id = null) {
            if (!$id && empty($this->data)) {
                    $this->Session->setFlash(__('Codigo invalido', true));
                    $this->redirect(array('action'=>'index'));
            }
            $this->data = $this->Mcuadrediario->read(null, $id);
            $this->data['Mcuadrediario']['estatus']=1;
            if (!empty($this->data)) {
                    if ($this->Mcuadrediario->save($this->data)) {
                            $this->Session->setFlash(__('El Cuadre diario ha sido Cerrado', true));
                            $this->redirect(array('action'=>'index'));
                    } else {
                            $this->Session->setFlash(__('El Cuadre diario no pudo ser salvado. Por favor, intentelo.', true));
                    }
            }
    }

    function delete($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Invalid id for Cuadrediario', true));
                    $this->redirect(array('action'=>'index'));
            }
            if ($this->Mcuadrediario->del($id)) {
                    $this->Session->setFlash(__('Cuadrediario deleted', true));
                    $this->redirect(array('action'=>'index'));
            }
    }

    function reporteexcel(){
            $this->layout = 'excel';
            $archivo='ventas';
            if (isset($this->data['Mcuadrediario']['nombre'])){
                    $archivo=$this->data['Mcuadrediario']['nombre'];
            }
            //print_r($_POST);
            $excel=$this->data['Mcuadrediario']['excel'];
            $this->set(compact('excel','archivo'));
    }

    public function cierre(){
            $this->checkSession();
            if (!empty($this->data)) {
                    $datos = $this->data;
                    list($id_empresa,$conexion)=explode('-',$datos['Reporte']['sucursal']);
                    Configure::write('Model.globalSource', $conexion);
                    $cuadre['Mcuadrediario']['id'] = $datos['Cuadre']['id'];
                    $cuadre['Mcuadrediario']['estatus'] = 2;
                    $this->Mcuadrediario->save($cuadre);
                    $this->flash('Se ha Realizado el Cierre de la Fecha:'.date("d-m-Y",strtotime($datos['Cuadre']['fecha'])),'/mcuadrediarios/cierre');
            }
            Configure::write('Model.globalSource', 'default');
            $sucursal=$this->Empresa->llenar_combo('',3);
            //$sucursal=$this->Empresa->llenar_combo('',1);
            Configure::write('Model.globalSource', 'comerdepa');
    $this->Venta->recursive = 0;
            $cliente=$this->Cliente->llenar_combo();
            $vendedores=$this->Vendedore->llenar_combo();
            $maquinas=$this->Maquina->llenar_combo('',0);
            $title_for_layout='COBROS DE FACTURAS A CR&Eacute;DITOS';
            $this->set(compact('cliente','vendedores','sucursal','maquinas','title_for_layout'));         
    }

    function buscarctabanco($id = null) {
            $datos = $_POST;
            list($codbanco,$id_empresa,$conexion)=explode('-',$id);
            Configure::write('Model.globalSource', $conexion);
            $criterio="Cuentasbancaria.codbanco=".$codbanco;
            $ctabancos = $this->Cuentasbancaria->llenar_combo('',0,$criterio);
            $modelo = 'Sucdeposito';
            if(isset($datos['opcion'])){
                    $modelo = $datos['opcion'];
            }
            $this->set(compact('ctabancos','modelo'));

            $this->render('buscarctabanco', 'ajax');		
    }

    public function buscarpdvxdep($id=null){
            $datos = $_POST; $pdv = array();

            list($codmaquina,$id_empresa,$conexion)=explode('-',$id);
            if($codmaquina!=''){
            Configure::write('Model.globalSource', $conexion);
            $criterio="and PUNTO.codpuntoventa=".$codmaquina." and PUNTO.fecha='".$datos['fecha']."' ";
            $pdv = $this->Puntodeventa->buscarpagos($criterio);
            }
    $this->set(compact('pdv'));
    $this->render('buscarpdvxdep', 'ajax');
    }

    public function buscarchequesxdep($id=NULL){
            $datos=$_POST;
            list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
            Configure::write('Model.globalSource', $conexion);

            $cuadre['Cuadrediario']['fecha'] = $datos['cuadre_fecha'];
            $cuadre['Cuadrediario']['id'] = $datos['cuadre_id'];
            // Buscar Suc Cheques en Cuadre
            $cheques = $this->Succheque->find('all',array('conditions'=> "Succheque.fecha='".$cuadre['Cuadrediario']['fecha']."' and Succheque.movbancario_id=0 "));
            $this->set(compact('cheques'));
            $this->render('buscarchequesxdep', 'ajax');
    }

    public function buscartransferencia($id=NULL){
            $datos=$_POST; $data[0][0]['monto']=0;$data[0][0]['numerotransferencia']='';
            if($datos['opcion']!=''){
            list($codventa,$codsucursal,$numerotransferencia,$cod_transf,$id_empresa,$conexion)=explode('-',$id);
            Configure::write('Model.globalSource', $conexion);		
            //--------Buscar Transferencia de Ventas
            $condicion = " and VT.codventa=".$codventa." and VT.codsucursal=".$codsucursal." and VTP.numerotransferencia='".$numerotransferencia."' and VTP.id=".$cod_transf." " ;
            $data = $this->Suctran->buscarpagos($condicion);
            }	
            $this->set(compact('data'));
            $this->render('buscartransferencia', 'ajax');
    }

    public function buscartransferxdep($id=null){
            $datos=$_POST;
            list($id_empresa,$conexion)=explode('-',$id);
            Configure::write('Model.globalSource', $conexion);	
            $transfer = $this->Suctran->llenar_combo(0," and VT.fecha='".$datos['cuadre_fecha']."' ");
            $this->set(compact('transfer'));
            $this->render('buscartransferxdep', 'ajax');
    }

    public function depositar($id=NULL) {
            $datos=$_POST;
            //$client=explode('-',$_POST['clientes']);		
            $vend=explode('-',$_POST['vendedores']);$tipoven=explode('-',$_POST['tipoventa']);
            list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
            Configure::write('Model.globalSource', $conexion);
            switch ($datos['opcion']) {
            case 'Efectivo':
            // 1.- Buscar Sucursal
                    $datos['codsucursal'] = $this->Venta->obtenerSucursal();
                    if ($datos['codsucursal'] > 0){
                    // 2.- Guardar Movimientos Bancario
                            $datos = $this->Movbancario->ajustarDatos('add',$datos);
                            if ($this->Movbancario->save($datos)) {
                            // 3.- Obtener ID Movimientos Bancario
                            $datos['movbancario_id'] = $this->Movbancario->getLastInsertID();
                            // 4.- Guardar Deposito
                            $datos = $this->Sucdeposito->ajustarDatos('add',$datos);
                            if ($this->Sucdeposito->saveAll($datos['Sucdeposito'])) {}
                            }			
                    }
                    // 5.- Mostrar Depositos en Efectivo
                    $depositos['Efectivo'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='EFECTIVO'"));
                    $this->set(compact('datos','depositos'));
                    $this->render('depefectivos', 'ajax');
            break;
            case 'Debito':
            // 1.- Buscar Sucursal
                    $datos['codsucursal'] = $this->Venta->obtenerSucursal();
                    if ($datos['codsucursal'] > 0){
                    // 2.- Guardar Movimientos Bancario
                            $datos['totalDeposito'] = $this->formato_ingles($datos['totalDeposito']);
                            $datos = $this->Movbancario->ajustarDatos('addDeb',$datos);
                            if ($this->Movbancario->save($datos)) {
                            // 3.- Obtener ID Movimientos Bancario
                            $datos['movbancario_id'] = $this->Movbancario->getLastInsertID();
                            // 4.- Guardar Deposito
                            $datos = $this->Suctarjeta->ajustarDatos('add',$datos);
                            if ($this->Suctarjeta->saveAll($datos['Suctarjeta'])) {}			
                            }			
                    }
                    // 5.- Mostrar Depositos en Efectivo
                    $depositos['Debito'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='DEBITO'"));
                    $this->set(compact('datos','depositos'));
                    $this->render('depdebito', 'ajax');
            break;
            case 'Cheque':
            // 1.- Buscar Sucursal
                    $datos['codsucursal'] = $this->Venta->obtenerSucursal();
                    $cuadre['cuadre_fecha'] = $datos['cuadre_fecha'];
                    if ($datos['codsucursal'] > 0){
                    // 2.- Guardar Movimientos Bancario
                            $datos['totalDeposito'] = $this->formato_ingles($datos['totalDeposito']);
                            $datos = $this->Movbancario->ajustarDatos('add',$datos);		
                            if ($this->Movbancario->save($datos)) {
                            // 3.- Obtener ID Movimientos Bancario
                            $movban['movbancario_id'] = $this->Movbancario->getLastInsertID();
                            $datos=array_merge($datos,$movban);
                            // 4.- Guardar Cheques
                            $datos = $this->Succheque->ajustarDatos('cuadre',$datos);
                            if ($this->Succheque->saveAll($datos['Succheque'])) {
                                    //echo 'Se guardo<br>';
                            }
                            }			
                    }
                    // 5.- Mostrar Depositos en Efectivo
                    $depositos['Cheque'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$cuadre['cuadre_fecha']."' and Movbancario.tipodeposito='CHEQUE'"));
                    $this->set(compact('datos','depositos'));
                    $this->render('depcheque', 'ajax');
            break;
            case 'Transfer':
            // 1.- Buscar Sucursal
                    $datos['codsucursal'] = $this->Venta->obtenerSucursal();
                    if ($datos['codsucursal'] > 0){
                    // 2.- Guardar Movimientos Bancario
                            $datos['totalDeposito'] = $this->formato_ingles($datos['totalDeposito']);
                            $datos = $this->Movbancario->ajustarDatos('add',$datos);
                            if ($this->Movbancario->save($datos)) {
                            // 3.- Obtener ID Movimientos Bancario
                            $datos['movbancario_id'] = $this->Movbancario->getLastInsertID();
                            // 4.- Guardar Deposito
                            $datos = $this->Suctran->ajustarDatos('add',$datos);
                            if ($this->Suctran->saveAll($datos['Suctran'])) {}				
                            }			
                    }
                    // 5.- Mostrar Depositos en Efectivo
                    $depositos['Transfer'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='TRANSFER'"));
                    $this->set(compact('datos','depositos'));
                    $this->render('deptransfer', 'ajax');
            break;
            case 'Gasto':
            // 1.- Buscar Sucursal
                    $datos['codsucursal'] = $this->Venta->obtenerSucursal();
                    if ($datos['codsucursal'] > 0){
                    // 2.- Guardar Movimientos Bancario
                            $datos['totalDeposito'] = $this->formato_ingles($datos['totalDeposito']);
                            $datos = $this->Sucgasto->ajustarDatos('add',$datos);
                            if ($this->Sucgasto->save($datos)) {				
                            }			
                    }
                    // 5.- Mostrar Gastos
                    $depositos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$datos['cuadre_fecha']."' "));
                    $this->set(compact('datos','depositos'));
                    $this->render('gastos', 'ajax');
            break;
            }
    }

    public function eliminardeposito($id=NULL) {
            $datos=$_POST;
            //$client=explode('-',$_POST['clientes']);		
            $vend=explode('-',$_POST['vendedores']);$tipoven=explode('-',$_POST['tipoventa']);
            list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
            Configure::write('Model.globalSource', $conexion);
            switch ($datos['opcion']) {
            case 'Efectivo':
            // 1.- Preguntar si el Cuadre esta abierto			
                    $cuadre = $this->Mcuadrediario->find('first',array('conditions'=>" Mcuadrediario.fecha='".$datos['cuadre_fecha']."' "));
                    if ($cuadre['Mcuadrediario']['estatus'] == 1){
                    // 2.- Eliminar Movimientos Bancario				
                            if ($this->Movbancario->delete($datos['movbancario_id'])){				
                            // 3.- Eliminar Deposito				
                            if ($this->Sucdeposito->deleteAll(array('Sucdeposito.movbancario_id' => $datos['movbancario_id']))){}
                            }			
                    }
                    // 4.- Mostrar Depositos en Efectivo
                    $depositos['Efectivo'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='EFECTIVO'"));
                    $this->set(compact('datos','depositos'));
                    $this->render('depefectivos', 'ajax');
            break;
            case 'Debito':
            // 1.- Preguntar si el Cuadre esta abierto			
                    $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
                    if ($cuadre['Cuadrediario']['estatus'] == 1){
                    // 2.- Eliminar Movimientos Bancario				
                            if ($this->Movbancario->delete($datos['movbancario_id'])){
                                    // 3.- Eliminar Deposito				
                                    if ($this->Suctarjeta->deleteAll(array('Suctarjeta.movbancario_id' => $datos['movbancario_id']))){}				
                            }			
                    }
                    // 4.- Mostrar Depositos en Efectivo
                    $depositos['Debito'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='DEBITO'"));
                    $this->set(compact('datos','depositos'));
                    $this->render('depdebito', 'ajax');
            break;
            case 'Cheque':
            // 1.- Preguntar si el Cuadre esta abierto			
                    $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
                    if ($cuadre['Cuadrediario']['estatus'] == 1){
                    // 2.- Eliminar Movimientos Bancario				
                            if ($this->Movbancario->delete($datos['movbancario_id'])){				
                            // 3.- Eliminar Deposito				
                            if ($this->Succheque->updateAll(array('Succheque.movbancario_id' => 0),array('Succheque.movbancario_id' => $datos['movbancario_id']))){}
                            }			
                    }
                    // 4.- Mostrar Depositos en Efectivo
                    $depositos['Cheque'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='CHEQUE'"));
                    $this->set(compact('datos','depositos'));
                    $this->render('depcheque', 'ajax');
            break;
            case 'Transfer':
            // 1.- Preguntar si el Cuadre esta abierto			
                    $cuadre = $this->Cuadrediario->find('first',array('conditions'=>" Cuadrediario.fecha='".$datos['cuadre_fecha']."' "));
                    if ($cuadre['Cuadrediario']['estatus'] == 1){
                    // 2.- Eliminar Movimientos Bancario				
                            if ($this->Movbancario->delete($datos['movbancario_id'])){
                            // 3.- Eliminar Deposito				
                            if ($this->Suctran->deleteAll(array('Suctran.movbancario_id' => $datos['movbancario_id']))){}			
                            }			
                    }
                    // 3.- Mostrar Depositos en Efectivo
                    $depositos['Transfer'] = $this->Movbancario->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$datos['cuadre_fecha']."' and Movbancario.tipodeposito='TRANSFER'"));
                    $this->set(compact('datos','depositos'));
                    $this->render('deptransfer', 'ajax');
            break;
            case 'Gasto':
            // 1.- Preguntar si el Cuadre esta abierto			
                    $cuadre = $this->Mcuadrediario->find('first',array('conditions'=>" Mcuadrediario.fecha='".$datos['cuadre_fecha']."' "));
                    if ($cuadre['Mcuadrediario']['estatus'] == 1){
                    // 2.- Eliminar Movimientos Bancario				
                            if ($this->Sucgasto->delete($datos['sucgasto_id'])){				
                            }			
                    }
                    // 3.- Mostrar Depositos en Efectivo
                    $depositos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$datos['cuadre_fecha']."' "));
                    $this->set(compact('datos','depositos'));
                    $this->render('gastos', 'ajax');
            break;
            case 'Retencion':
            // 1.- Preguntar si el Cuadre esta abierto			
                    $cuadre = $this->Mcuadrediario->find('first',array('conditions'=>" Mcuadrediario.fecha='".$datos['cuadre_fecha']."' "));
                    $cuadre2['Cuadrediario'] = $cuadre['Mcuadrediario'];
                    if ($cuadre['Mcuadrediario']['estatus'] == 1){
                    // 2.- Eliminar Retenciones				
                            $this->Retencionventa->eliminarDetalle($datos['codretencion'],$id_empresa);
                            $this->Retencionventa->eliminarCabecera($datos['codretencion']);
                    }
                    // 3.- Mostrar Depositos en Efectivo
                    $retenciones['Total'] = $this->Retencionventa->buscarResumenDia($cuadre2);
                    $retenciones['Registros'] = $this->Retencionventa->buscarRegistrosDia($cuadre2);
                    $this->set(compact('datos','retenciones'));
                    $this->render('retenciones', 'ajax');
            break;
            }
    }

    public function buscarventas($id=NULL) {
            //useDbConfig = 'comerdepa_3'
            $datos=$_POST;$client=explode('-',$_POST['clientes']);		
            $vend=explode('-',$_POST['vendedores']);$tipoven=explode('-',$_POST['tipoventa']);
            list($id_empresa,$conexion)=explode('-',$datos['sucursal']);
            $SUCURSAL = $datos['sucursal'];
            Configure::write('Model.globalSource', $conexion);
            switch ($datos['tiporeporte']) {
            case 'Ventas_Diarias_Cierre':
                    $titulo='Ventas del Dia'; $tipoven=explode('-','0');
                    $datos['client']=$client;$datos['vend']=$vend;$datos['tipoven']=$tipoven;
                    $data=$this->Venta->reporte($datos,'',14);//Buscar Ventas realizadas 	
                    $datapago = $this->Venta->pagosRealizados($datos,'credito');//Buscar los Pagos realizados			
                    $data_dev=$this->Devolucionventa->reporte($datos,'',3);//Buscar Devoluciones de Ventas realizadas
                    // Buscar Datos Adicionales
                    $denominacion = $this->Dinerodenominacione->find('all',array('order'=>'Dinerodenominacione.fin desc,Dinerodenominacione.valor asc','recursive' => -1,'conditions'=>" Dinerodenominacione.fin='DINERO' or Dinerodenominacione.fin='MONEDA'"));			
                    $banco = $this->Banco->llenar_combo('',1);
                    $documentotipo['dp'] = $this->Documentotipo->llenar_combo(2);
                    $documentotipo['nc'] = $this->Documentotipo->llenar_combo(3);

                    $cuadre = $this->Mcuadrediario->buscar($datos);
                    $datos_movbancario['efectivo']=$this->Movbancario->ajustarRegistrosCuadre('Movbancario_Efectivo',$cuadre['Mcuadrediario']['id'],$datapago['efectivo'],$id_empresa);
                    $datos_movbancario['debito']=$this->Movbancario->ajustarRegistrosCuadre('Movbancario_Debito',$cuadre['Mcuadrediario']['id'],$datapago['debito'],$id_empresa);
                    $datos_movbancario['cheque']=$this->Movbancario->ajustarRegistrosCuadre('Movbancario_Cheque',$cuadre['Mcuadrediario']['id'],$datapago['cheque'],$id_empresa);
                    $datos_movbancario['transfer']=$this->Movbancario->ajustarRegistrosCuadre('Movbancario_Transfer',$cuadre['Mcuadrediario']['id'],$datapago['transfer'],$id_empresa);
                    $this->Mcuadrediario->actualizarMontosCuadre($cuadre['Mcuadrediario']['id']);
                    $puntos = $this->Puntodeventa->llenar_combo('');
                    $transfer = $this->Suctran->llenar_combo(0," and VT.fecha='".$cuadre['Mcuadrediario']['fecha']."' ");
                    //--------Buscar Cheques de Ventas
                    $cheques = $this->Ventaspago->find('all',array('conditions'=>" Venta.fecha='".$cuadre['Mcuadrediario']['fecha']."' and Ventaspago.tipopago='CHEQUE'",'order'=>'Ventaspago.banco'));
                    $cuadre2['Cuadrediario'] = $cuadre['Mcuadrediario'];
                    if($cuadre['Mcuadrediario']['estatus']==1){				
                            $cheques = $this->Succheque->buscar($cheques,$cuadre2);// Buscar Suc Cheques en Cuadre
                    }
                    //------------------------
                    $depositos = $this->Movbancario->buscarDepositos($cuadre2);			
                    $gastos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$cuadre['Mcuadrediario']['fecha']."' "));
                    $retenciones['Total'] = $this->Retencionventa->buscarResumenDia($cuadre2);
                    $retenciones['Registros'] = $this->Retencionventa->buscarRegistrosDia($cuadre2);
                    $depositos=array_merge($depositos,$gastos);
                    $cuadre = $cuadre2;
                    $this->set(compact('data','titulo','data_dev','datapago','denominacion','banco','cuadre','documentotipo','depositos','cheques','puntos','transfer','retenciones','SUCURSAL'));
                    if($cuadre['Cuadrediario']['estatus']==1){
                            $this->render('buscarventasdiariascaja', 'ajax');
                    }else{
                            $mensaje = ' El Cuadre de Fecha '.date("d-m-Y",strtotime($cuadre['Cuadrediario']['fecha'])).' esta Cerrado';
                            $this->set(compact('mensaje'));
                            $this->render('buscarventasdiariascajacerrada', 'ajax');
                    }
            break;
            }
    }

}
?>
