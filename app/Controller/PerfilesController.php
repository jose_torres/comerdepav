<?php
class PerfilesController extends AppController
{
    public $name = 'Perfiles';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Perfile','Usuario','Configuracion');
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function index(){
        $this->Perfile->recursive = 0;
        $this->set('data', $this->paginate()); 
    }

    public function view($id){
        $this->set('data', $this->Perfile->read(NULL,$id));
    }

    public function buscarhijos($id){
        $this->set('data_perfiles', $this->Perfile->findAll(' Perfile.familia='.$id));
        $this->render('buscarhijos', 'ajax');
    }

    public function add(){
           	
        if(!empty($this->data)){
                if ($this->Perfile->save($this->data)){				
                        $this->data = $this->Perfile->read(NULL,$this->Perfile->getLastInsertId());
                        $datos=$this->data;
                        if ($datos['Perfile']['id_padre']==0)
                        $datos['Perfile']['familia']=$this->Perfile->getLastInsertId();
                        $this->Perfile->save($datos);
                        $this->flash('El Perfil ha sido Guardado.','/perfiles');
                }else{
                        $this->set('data', $this->data);
                        $this->render('add');
                }
        }
        $this->set('perfiles', $this->Perfile->llenar_combo('',0));
    }

    public function edit($id=null){
        $this->checkSession();
        $this->set('perfiles', $this->Perfile->llenar_combo('',0));	
        if (empty($this->data)){
                $this->data = $this->Perfile->read(NULL,$id);
        }else{
                if ($this->Perfile->save($this->data)){
                        $this->flash('Tu Perfil ha sido Actualizado.','/perfiles');
                }else{
                        $this->set('data', $this->data);
                }
        }		
    }

    public function delete($id){
        $this->checkSession();
        $nro = $this->Usuario->find('count',array('conditions'=>' Usuario.perfil_id='.$id));	
        if ($nro>0){	
                $this->flash('El Perfil con el id: '.$id.' no puede ser Eliminado porque tiene movimientos.', '/perfiles');
        }else{
                if ($this->Perfile->delete($id)){
                        /*$this->Session->setFlash(' El Perfil con el id: '.$id.' ha sido Eliminado.');	$this->redirect('/perfiles');*/				
                        $this->flash('El Perfil con el id: '.$id.' ha sido Eliminado.', '/perfiles');
                }		
        }			
    }

    public function buscar($id=NULL) {
        $criteria=" upper(Perfile.descripcion) like upper('".$id."%')";
        if ($id==""){
                $this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/perfiles');
                $data=array();
        }else{
                $data = $this->Perfile->find('all',array('conditions'=>$criteria));
        }
$this->set('data',$data);
        $this->render('buscar', 'ajax');	
    }

}
?>
