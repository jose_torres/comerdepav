<?php
class ConfiguracionsController extends AppController
{
    public $name = 'Configuracions';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array('Configuracion','Perfile','Funcione');
//-----------------------Seguridad--------------------------------------	
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------	
    public function index(){
        $this->checkSession();
        $this->Perfile->recursive = 0;
        $this->set('data', $this->paginate('Perfile'));
    }

    public function view($id){
        $this->set('data', $this->Configuracion->read());
    }

    public function delete($id){
        if ($this->Configuracion->del($id)){
            $this->flash('The Configuracion with id: '.$id.' has been deleted.', '/configuracions');
        }
    }

    public function eliminar($id){
        list($funcionid,$perfilid)=split('-',$id);
        $configuracion= $this->Configuracion->find('all',array('conditions'=>' perfil_id='.$perfilid.' and funcion_id='.$funcionid));
        if (count($configuracion)>0){
            if ($this->Configuracion->delete($configuracion[0]['Configuracion']['id'])){
                    $this->redirect('/configuracions/mostrar/'.$perfilid);
            }
        }
    }

    public function cambiar($id){
        list($cod,$tipo,$valor)=split('-',$id);
        $this->set('id',$id);
        $this->set('tipo',$tipo);
        $this->set('cod',$cod);

        $privilegio = $this->Configuracion->findAll(' Configuracion.id='.$cod);
        switch ($tipo) {
                case "I":
                                $privilegio[0]['Configuracion']['incluir']=$valor;
                                if($this->Configuracion->save($privilegio[0])){
                                        //echo 'Grabo';
                                        $this->set('valor',$valor);
                                }
                                break;
                case "M":
                                $privilegio[0]['Configuracion']['modificar']=$valor;
                                if($this->Configuracion->save($privilegio[0])){
                                        //echo 'Grabo';
                                        $this->set('valor',$valor);
                                }
                                break;
                case "C":
                                $privilegio[0]['Configuracion']['consultar']=$valor;
                                if($this->Configuracion->save($privilegio[0])){
                                        //echo 'Grabo';
                                        $this->set('valor',$valor);
                                }
                                break;
                case "E":
                                $privilegio[0]['Configuracion']['eliminar']=$valor;
                                if($this->Configuracion->save($privilegio[0])){
                                        //echo 'Grabo';
                                        $this->set('valor',$valor);
                                }
                                break;															
        }
        $this->render('cambiar', 'ajax');
    }

    public function grabar(){
        $this->data['Configuracion']['perfil_id']=$_REQUEST['idperfil'];
            $this->data['Configuracion']['funcion_id']=$_REQUEST['idfuncion'];
            $this->data['Configuracion']['incluir']='SI'/*$_REQUEST['incluir']*/;
            $this->data['Configuracion']['modificar']='SI'/*$_REQUEST['modificar']*/;
            $this->data['Configuracion']['consultar']='SI'/*$_REQUEST['consultar']*/;
            $this->data['Configuracion']['eliminar']='SI'/*$_REQUEST['eliminar']*/;

            if ( $this->Configuracion->save($this->data))
            {
                    //$this->flash('Your Configuracion has been updated.','/configuracions');
            }
    $data= $this->Configuracion->find('all',array('conditions'=>' perfil_id='.$_REQUEST['idperfil'], 'order'=>' Funcione.direccion '));
    $data_arbol= $this->Configuracion->arbol($_REQUEST['idperfil']);
    //$this->set('data',$data_configuracion);
    $this->set(compact('data','data_arbol'));
    $this->render('grabar', 'ajax');
    }

    public function actualizar($id=null){
            $valores['Configuracion']=array();
            $x=0;
            for ($i = 0; $i < $this->Funcione->find('count'); $i++) {
                    $conf= $this->Configuracion->find('first',array('conditions'=>' perfil_id='.$this->data['Configuracion']['perfil_id'].' and funcion_id='.$this->data['Configuracion']['codigo'.$i].''));
                    if (isset($conf['Configuracion']['id']))
                    $valores['Configuracion'][$i]['id'] = $conf['Configuracion']['id'];

                    $valores['Configuracion'][$i]['perfil_id'] = $this->data['Configuracion']['perfil_id'];
                    $valores['Configuracion'][$i]['funcion_id'] = $this->data['Configuracion']['codigo'.$i];
                    if($this->data['Configuracion']['funcion_id'.$i]==0){
                            $valores['Configuracion'][$i]['status']='0';
                    }else{
                            $valores['Configuracion'][$i]['status']='1';
                    }
            }
            if ($this->Configuracion->saveAll($valores['Configuracion'])) {
                    $this->redirect('/configuracions/mostrar/'.$this->data['Configuracion']['perfil_id']);
            }else{
                    $this->redirect('/configuracions/mostrar/'.$this->data['Configuracion']['perfil_id']);
            }
    }

    public function mostrar($id){
            //$data= $this->Configuracion->find('all',array('conditions'=>' perfil_id='.$id, 'order'=>' Funcione.direccion ','recursive'=>1));
            $data_perfil= $this->Perfile->find('all',array('conditions'=>' id='.$id,'recursive'=>-1));
            //$data_funcion= $this->Funcione->find('all',array('order'=>' Funcione.direccion ','recursive'=>-1));
            $data_arbol= $this->Configuracion->arbol($id);
            $this->set(compact(/*'data',*/'data_perfil'/*,'data_funcion'*/,'data_arbol'));
            //$this->render('mostrar');
    //	$this->set('datos_menu',$this->menuPrincipal());
    } 

    public function buscar($id=NULL) {
            $criteria=" Perfile.descripcion like '".$id."%'";
            if ($id==""){
            $this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/configuracions');
             $data=array();
            }else{
    $data = $this->Perfile->find('all',array('conditions'=>$criteria)); // Extra parameters added                
            if(count($data)<=0){
                            $this->flash('No se ha conseguido Registro. Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/configuracions');
            }
            }
    $this->set('data',$data);
            $this->render('buscar', 'ajax');	
    }

}
?>
