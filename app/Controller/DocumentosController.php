<?php
class DocumentosController extends AppController
{
    public $name = 'Documentos';
	public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
	public $components = array ('Pagination'); 
    public $uses = array ('Documento','Grupo','Configuracion','Funcione','Perfile','Usuario','Oficina','Documentopermiso','Tipojustificacione','Revisione','Plantilla','Esquema','Contenido','Empresa');
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
	public function publicados(){
		$this->checkSession();
		$criteria="  Documento.estatus='PUBLICADO' and Documentopermiso.accion='PUBLICAR' and Documentopermiso.oficina_id=0";        
        $order="Documentopermiso.id DESC";        
        $this->paginate = array('conditions' => $criteria,'limit' => 10,'order' => $order);
        $data  = $this->paginate('Documentopermiso');
        $this->set('data',$data);
        $datos_resumen['id']=1;
        $this->set('datos_resumen',$datos_resumen); 
	}

	public function publicadosoficinas(){
		$this->checkSession();
		$user=$this->Usuario->find('first',array('conditions'=>'Usuario.id='.$this->Session->read('Usuario.id')));
		$criteria="  Documento.estatus='PUBLICADO' and Documentopermiso.accion='PUBLICAR' and Documentopermiso.oficina_id=".$user['Empleado']['oficina_id'];
        //list($order,$limit,$page) = $this->Pagination->init($criteria,NULL,array('modelClass'=>'Documentopermiso','sortBy'=>'id DESC'));
        $order="Documentopermiso.id DESC";
        //$data = $this->Documentopermiso->find('all',array('conditions'=>$criteria,'order'=> $order, 'limit'=>$limit,'page'=>$page)); // Extra parameters added           
        $this->paginate = array('conditions' => $criteria,'limit' => 10,'order' => $order);
        $data  = $this->paginate('Documentopermiso');
        $this->set('data',$data);
        $datos_resumen['id']=2;
        $this->set('datos_resumen',$datos_resumen); 
	}
	
	public function index(){
		$this->checkSession();
		$criteria=" Documento.usuario_id=".$this->Session->read('Usuario.id')." and Documento.estatus='SIN_ENVIAR'";        
        $order="Documento.codigo desc";                     
        $this->paginate = array('conditions' => $criteria,'limit' => 10,'order' => $order);
        $data  = $this->paginate('Documento');
        $this->set('data',$data);
        
        $datos_resumen= $this->Documento->registro_totales($this->Session->read('Usuario.id'));
        $datos_resumen['recibido']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=0);
        $datos_resumen['recibido_aprob']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=1);
        $datos_resumen['id']=1;
        $this->set('datos_resumen',$datos_resumen); 
	}

	public function enviados(){
		$this->checkSession();
		$criteria=" Documento.usuario_id=".$this->Session->read('Usuario.id')." and Documento.estatus='ENVIADO'";        
        $order="Documento.id DESC";       
        $this->paginate = array('conditions' => $criteria,'limit' => 10,'order' => $order);
        $data  = $this->paginate('Documento');               
        $this->set('data',$data);

        $datos_resumen= $this->Documento->registro_totales($this->Session->read('Usuario.id'));
        $datos_resumen['recibido']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=0);
        $datos_resumen['recibido_aprob']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=1);
        $datos_resumen['id']=2;
        $this->set('datos_resumen',$datos_resumen); 
	}

	public function poraprobar(){
		$this->checkSession();
		$criteria=" Documento.usuario_id=".$this->Session->read('Usuario.id')." and Documento.estatus='A_APROBACION'";
        //list($order,$limit,$page) = $this->Pagination->init($criteria); // Added
        $order="Documento.id DESC";
        //$data = $this->Documento->find('all',array('conditions'=>$criteria,'order'=> $order, 'limit'=>$limit,'page'=>$page)); // Extra parameters added    
        $this->paginate = array('conditions' => $criteria,'limit' => 10,'order' => $order);
        $data  = $this->paginate('Documento');            
        $this->set('data',$data);

        $datos_resumen= $this->Documento->registro_totales($this->Session->read('Usuario.id'));
        $datos_resumen['recibido']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=0);
        $datos_resumen['recibido_aprob']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=1);
        $datos_resumen['id']=3;
        $this->set('datos_resumen',$datos_resumen); 
	}

	public function recibidos(){
		$this->checkSession();
		$criteria=" Documentopermiso.usuario_id=".$this->Session->read('Usuario.id')." and Documento.estatus='ENVIADO' and Documentopermiso.accion='REVISAR'";        
        $order="Documentopermiso.id DESC";        
		$this->paginate = array('conditions' => $criteria,'limit' => 10,'order' => $order);
        $data  = $this->paginate('Documentopermiso');
        
        $datos_resumen= $this->Documento->registro_totales($this->Session->read('Usuario.id'));
        $datos_resumen['recibido']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=0);
        $datos_resumen['recibido_aprob']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=1);
        $datos_resumen['id']=5;
        $usuarios = $this->Usuario->llenar_combo('',0);
        $this->set(compact('data','datos_resumen','usuarios'));   
	}

	public function recibidosaprobar(){
		$this->checkSession();
		$criteria=" Documentopermiso.usuario_id=".$this->Session->read('Usuario.id')." and Documento.estatus='A_APROBACION' and Documentopermiso.accion='APROBAR'";        
        $order="Documentopermiso.id DESC";        
		$this->paginate = array('conditions' => $criteria,'limit' => 10,'order' => $order);
        $data  = $this->paginate('Documentopermiso');
        $datos_resumen= $this->Documento->registro_totales($this->Session->read('Usuario.id'));
        $datos_resumen['recibido']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=0);
        $datos_resumen['recibido_aprob']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=1);
        $datos_resumen['id']=6;
        $usuarios = $this->Usuario->llenar_combo('',0);
        $this->set(compact('data','datos_resumen','usuarios'));   
	}

	public function porpublicar(){
		$this->checkSession();
		$criteria=" Documento.usuario_id=".$this->Session->read('Usuario.id')." and Documento.estatus='APROBADO'";
        //list($order,$limit,$page) = $this->Pagination->init($criteria); // Added
        $order="Documento.id DESC";
        //$data = $this->Documento->find('all',array('conditions'=>$criteria,'order'=> $order, 'limit'=>$limit,'page'=>$page)); // Extra parameters added          
        $this->paginate = array('conditions' => $criteria,'limit' => 10,'order' => $order);
        $data  = $this->paginate('Documento');      
        $this->set('data',$data);         

        $datos_resumen= $this->Documento->registro_totales($this->Session->read('Usuario.id'));
        $datos_resumen['recibido']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=0);
        $datos_resumen['recibido_aprob']=$this->Documentopermiso->contar_registros($this->Session->read('Usuario.id'),$opcion=1);
        $datos_resumen['id']=4;
        $usuarios = $this->Usuario->llenar_combo('',0);
        $this->set(compact('data','datos_resumen','usuarios'));   
	}
	
	public function view($id){
		$this->checkSession();
		list($cod,$url) = explode("-", $id);
		if($url!='index'){$url='/'.$url;}else{ $url='';}
		$data = $this->Documento->read(NULL,$cod);
		$usuarios = $this->Usuario->llenar_combo('',0);
		$this->set(compact('data','url','usuarios'));
	}
	
	public function add(){
		$this->checkSession();
		$tipodocumentos = $this->Documento->Tipodocumento->find('list',array('fields'=>'nombre'));
		$oficinas = $this->Documento->Oficina->find('list',array('fields'=>'siglas'));
		$tipojustificaciones = $this->Tipojustificacione->llenar_combo('',0," accion='Crear'");
		$plantilla = $this->Plantilla->llenar_combo();
		$this->set(compact('tipodocumentos','oficinas','tipojustificaciones','plantilla'));
		//$this->set('nrodoc', String::uuid(8));
		if (empty($this->data)){
			$this->render();
		}else{	
			$datos=$this->data;
			if($datos['Documento']['archivo']['name']!=""){
			if (substr_count($datos['Documento']['archivo']['name'],'.')<>1){
				$this->Session->setFlash(__('El nombre del Archivo posee puntos (.). Por Favor Cambiar el nombre.', true));
				$this->set('data', $datos);
				$this->render();
			}
			$parte_nom=$this->generar_letras_aleatorias(5).'_';
			$datos['Documento']['archivo_nombre']=$this->Documento->generar_nombre_archivo($parte_nom,$datos);
			if($this->adjuntar_archivo($datos['Documento']['archivo']['tmp_name'],$this->filesProyecto.$datos['Documento']['archivo_nombre'])){}
			}
			if ($datos['Documento']['codigo']==''){
				$oficina = $this->Oficina->find('first',array('conditions'=>' Oficina.id='.$datos['Documento']['oficina_id'],'recursive'=> -1));
				$datos['Documento']['codigo']=$this->Documento->generar_codigo($datos,$oficina);
			}			
			$datos['Documento']['usuario_id']=$this->Session->read('Usuario.id');

			if ($this->Documento->save($datos)){	
				$id_doc=$this->Documento->getInsertID();
				$tot_reg=$datos['Documento']['totales'];
				if($datos['Documento']['plantilla_id']>0){
					if ($this->Contenido->guardar($tot_reg,$id_doc,$datos)){}
				}	
				$this->flash('El Documento ha sido Guardado.','/documentos');
			}else{				
				$this->set('data', $datos);
				$this->render('add');
			}
		}
	}
	
	public function edit($id=null)
	{
		$this->checkSession();
		$tipodocumentos = $this->Documento->Tipodocumento->find('list',array('fields'=>'nombre'));
		$oficinas = $this->Documento->Oficina->find('list',array('fields'=>'siglas'));
		$tipojustificaciones = $this->Tipojustificacione->llenar_combo('',0," accion='Crear'");
		$plantilla = $this->Plantilla->llenar_combo();		
		$this->set(compact('tipodocumentos','oficinas','tipojustificaciones','plantilla'));
		if (empty($this->data)){
			$this->data =  $this->Documento->read(NULL,$id);
			$contenidos = $this->Contenido->find('all',array('conditions'=>' Contenido.documento_id='.$this->data['Documento']['id'],'recursive'=>2));
			if($this->data['Documento']['plantilla_id']==0){
				$plantilla = array(''=>'Sin Plantilla');
				$this->set(compact('plantilla'));
			}
			$this->set(compact('contenidos'));
			$this->render();
		}else{
			$datos=$this->data;
			if($datos['Documento']['archivo']['name']!=""){
				if (substr_count($datos['Documento']['archivo']['name'],'.')<>1){
				$this->Session->setFlash(__('El nombre del Archivo posee puntos (.). Por Favor Cambiar el nombre.', true));
				$this->set('data', $datos);
				$this->render();
				}
				$parte_nom=$this->generar_letras_aleatorias(5).'_';
				$datos['Documento']['archivo_nombre']=$this->Documento->generar_nombre_archivo($parte_nom,$datos);
				if($this->adjuntar_archivo($datos['Documento']['archivo']['tmp_name'],$this->filesProyecto.$datos['Documento']['archivo_nombre'])){}
			}
			$datos['Documento']['usuario_id']=$this->Session->read('Usuario.id');
			if ($datos['Documento']['codigo']==''){
				$oficina = $this->Oficina->find('first',array('conditions'=>' Oficina.id='.$datos['Documento']['oficina_id'],'recursive'=> -1));
				$datos['Documento']['codigo']=$this->Documento->generar_codigo($datos,$oficina);
			}
			//print_r($this->data);
			if ( $this->Documento->save($datos)){
				$id_doc=$datos['Documento']['id'];
				$tot_reg=$datos['Documento']['totales'];
				if($datos['Documento']['plantilla_id']>0){
					if ($this->Contenido->actualizar($tot_reg,$id_doc,$datos)){}
				}
				$this->flash('El Documento ha sido Actualizado.','/documentos');
			}else{
				$this->set('data', $datos);
				$this->render();
			}
		}
	}

	public function revisar($id=null){
		$this->checkSession();
		if (empty($this->data)){
			$data = $this->Documento->read(NULL,$id);
			$tipojustificaciones = $this->Tipojustificacione->llenar_combo('',0," accion='Aprobar' or accion='Revisar' or accion='Rechazar'");
			$this->set(compact('data','tipojustificaciones'));		
		}else{
			$datos=$this->data;			
			$datos['Revisione']['usuario_id']=$this->Session->read('Usuario.id');			
			if ($this->Revisione->save($datos)){
				$nro_usuarios_env=$this->Documentopermiso->contar_registros($datos['Documento']['id'],2);
				$nro_usuarios_apr=$this->Revisione->contar_registros($datos,0);
				//echo ' Us_Env:'.$nro_usuarios_env.' Us_Apr:'.$nro_usuarios_apr;
				if($nro_usuarios_env==$nro_usuarios_apr){
					//------------------------------------------------------
					$datos['Documento']['estatus']='A_APROBACION';		
					if ($this->Documento->save($datos)){}
					$usuarios = $this->Usuario->llenar_combo('',2);
					foreach ($usuarios as $clave => $valor){
						$this->Documentopermiso->guardar_aprobar($datos['Documento']['id'],$clave);
					}
					$this->flash('La Revision ha sido Incluido. Se ha enviado a APROBACIÓN porque todos los usuarios aprobaron.','/documentos/recibidos');
					//-----------------------------------------------------
				}else{
					$this->flash('La Revision ha sido Incluido.','/documentos/recibidos');
				}
			}else{
				$this->flash('La Revision no ha sido Incluido.','/documentos/revisar/'.$id);
			}
		}
	}

	public function aprobar($id=null){
		$this->checkSession();
		if (empty($this->data)){
			$data = $this->Documento->read(NULL,$id);
			$tipojustificaciones = $this->Tipojustificacione->llenar_combo('',0," accion='Aprobar' or accion='Revisar' or accion='Rechazar'");
			$this->set(compact('data','tipojustificaciones'));
			$this->render();
		}else{
			$datos=$this->data;
			$datos['Revisione']['usuario_id']=$this->Session->read('Usuario.id');
			if ( $this->Revisione->save($datos)){
				$nro_usuarios_env=$this->Documentopermiso->contar_registros($datos['Documento']['id'],3);
				$nro_usuarios_apr=$this->Revisione->contar_registros($datos,1);
				$nro_usuarios_rec=$this->Revisione->contar_registros($datos,3);
				//echo ' Us_Env:'.$nro_usuarios_env.' Us_Apr:'.$nro_usuarios_apr;
				if($nro_usuarios_env==$nro_usuarios_rec){
					//------------------------------------------------------
					$datos['Documento']['estatus']='ENVIADO';		
					if ($this->Documento->save($datos)){}
					$this->Documentopermiso->deleteAll(array('Documentopermiso.documento_id' => $datos['Documento']['id'],'Documentopermiso.accion'=>'APROBAR'), false);
					$usuarios = $this->Usuario->llenar_combo('',2);
					foreach ($usuarios as $clave => $valor){
						$this->Documentopermiso->guardar_aprobar($datos['Documento']['id'],$clave,0,'ENVIADO');
					}
					$this->flash('La Revision ha sido Incluido. Se ha enviado a REVISION porque todos los usuarios Rechazaron.','/documentos/recibidosaprobar');
					//-----------------------------------------------------				
				}elseif($nro_usuarios_env==$nro_usuarios_apr){
					//------------------------------------------------------
					$datos['Documento']['estatus']='APROBADO';		
					if ($this->Documento->save($datos)){}
					$usuarios = $this->Usuario->llenar_combo('',3);
					foreach ($usuarios as $clave => $valor){
						$this->Documentopermiso->guardar_aprobar($datos['Documento']['id'],$clave,0,'POR_PUBLICAR');
					}
					$this->flash('La Revision ha sido Incluido. Se ha enviado a PUBLICACIÓN porque todos los usuarios aprobaron.','/documentos/recibidosaprobar');
					//-----------------------------------------------------
				}else{
					$this->flash('La Revision ha sido Incluido.','/documentos/recibidosaprobar');
				}
				
			}else{
				$this->flash('La Revision no ha sido Incluido.','/documentos/aprobar/'.$id);
			}
		}
	}	
	
	public function delete($id){
		$this->checkSession();
		$nro_registro=0;
		 $mensaje = $this->Documento->eliminar($nro_registro,$id);
		 $this->flash($mensaje,'/documentos');	
	}

	public function enviar($id=null){
		$this->checkSession();
		if (empty($this->data)){
			$this->data =  $this->Documento->read(NULL,$id);
			$this->render();
		}else{
			$datos=$this->data;
			$datos['Documento']['usuario_id']=$this->Session->read('Usuario.id');		
			$datos['Documento']['estatus']='ENVIADO';		
			if ( $this->Documento->save($datos)){
				$det=$this->Session->read('Datos_'.$datos['Documento']['id']);
				$this->Documentopermiso->guardar_permisos($datos['Documento']['id'],$det);
				$this->Session->write('Datos_'.$datos['Documento']['id'],array());
				$this->flash('El Documento ha sido Enviado.','/documentos/enviados');
			}else{
				$this->set('data', $this->data);
				$this->render();
			}
		}
	}

	public function publicar($id=null){
		$this->checkSession();
		if (empty($this->data)){
			$this->data =  $this->Documento->read(NULL,$id);
			$this->render();
		}else{
			$datos=$this->data;
			$datos['Documento']['usuario_id']=$this->Session->read('Usuario.id');		
			$datos['Documento']['estatus']='PUBLICADO';
			if ( $this->Documento->save($datos)){
				if($this->data['Envio']['dirigido']!='Todos'){
				$data=$this->Session->read('Datos_'.$datos['Documento']['id']);
				$this->Documentopermiso->guardar_permisos($datos['Documento']['id'],$data,'PUBLICAR');
				$this->Session->write('Datos_'.$datos['Documento']['id'],array());
				}else{
					$this->Documentopermiso->guardar_publico($datos['Documento']['id']);
				}
				$this->flash('El Documento ha sido Publicado.','/documentos');
			}else{
				$this->set('data', $datos);
				$this->render();
			}
		}
	}

	public function enviaraprobar($id=null){
		$this->checkSession();
		if (empty($this->data)){
			$this->data =  $this->Documento->read(NULL,$id);
			$this->render();
		}
		else{
			$datos=$this->data;	
			if($datos['Documento']['archivo']['name']!=""){
				if (substr_count($datos['Documento']['archivo']['name'],'.')<>1){
				$this->Session->setFlash(__('El nombre del Archivo posee puntos (.). Por Favor Cambiar el nombre.', true));
				$this->set('data', $datos);
				$this->render();
				}
				$parte_nom=$this->generar_letras_aleatorias(5).'_';
				$datos['Documento']['archivo_nombre']=$this->Documento->generar_nombre_archivo($parte_nom,$datos);
				if($this->adjuntar_archivo($datos['Documento']['archivo']['tmp_name'],$this->filesProyecto.$datos['Documento']['archivo_nombre'])){}
			}
			$datos['Documento']['usuario_id']=$this->Session->read('Usuario.id');		
			$datos['Documento']['estatus']='A_APROBACION';		
			if ( $this->Documento->save($datos)){
				$det=$this->Session->read('Datos_'.$datos['Documento']['id']);
				$this->Documentopermiso->guardar_permisos($datos['Documento']['id'],$det,'APROBAR');
				$this->Session->write('Datos_'.$datos['Documento']['id'],array());
				$this->flash('El Documento ha sido Enviado a Aprobacion.','/documentos/poraprobar');
			}else{
				$this->set('data', $datos);
				$this->render();
			}
		}
	}
// Funciones de Ajax
	public function buscar($id=NULL) {

		$criteria=" Documento.titulo like '".$id."%'";
		if ($id==""){

		$this->flash('Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/bancos');
		 $data=array();
		}else{

        $data = $this->Documento->find('all',array('conditions'=>$criteria)); // Extra parameters added                
	        if(count($data)<=0){
	   			$this->flash('No se ha conseguido Registro. Escriba en el campo buscar o Haga Click aqui para Actualizar la pagina.', '/documentos');
	        }
		}
        $this->set('data',$data);
		$this->render('buscar', 'ajax');	
	}

	public function buscarremitente($id='Usuario') {

		$data=array();
		if ($id=="Usuario"){
			if(isset($_POST['aprobar'])){
			$data = $this->Usuario->llenar_combo('',2);
			}else{
			$data = $this->Usuario->llenar_combo('',1);
			}
		}else{                      
			$data = $this->Oficina->llenar_combo('',0);    
		}
        $this->set('remitente',$data);
        $this->set('label',$id);
		$this->render('buscarremitente', 'ajax');	
	}

	public function eliminardetalle($id=null){
		
		$data=$this->Session->read('Datos_'.$_POST['valorControl']);
		unset($data['Documentopermiso'][$id]);
		$this->Session->write('Datos_'.$_POST['valorControl'], $data);
		$mensaje='Se elimino el detalle';
		$valorControl=$_POST['valorControl'];
		$this->set(compact('data','mensaje','valorControl'));

		$this->render('guardardetalle', 'ajax');
	}
	public function guardardetalle($id=null){

		$data=$this->Session->read('Datos_'.$_POST['valorControl']);
		
		if($_POST['permiso']!=''){
		if ($id=='Usuario'){
			$criterio="Usuario.id=".$_POST['remitente'];
			$datos_usuario = $this->Usuario->find('first',array('conditions'=>$criterio));
			$data['Documentopermiso'][$_POST['remitente']]=array('usuario_id'=>$_POST['remitente'],'usuario'=>$datos_usuario['Usuario']['usuario'],'permiso'=>$_POST['permiso'],'oficina_id'=>0);
			$mensaje='Se Guardo el detalle';
		}elseif ($id=='Oficina'){
			$criterio=" Empleado.oficina_id=".$_POST['remitente']." and Empleado.revisar='SI' ";
			$datos_usuario = $this->Usuario->find('all',array('conditions'=>$criterio));
			foreach ($datos_usuario as $row){
				$data['Documentopermiso'][$row['Usuario']['id']]=array('usuario_id'=>$row['Usuario']['id'],'usuario'=>$row['Usuario']['usuario'],'permiso'=>$_POST['permiso'],'oficina_id'=>0);
			}
			$mensaje='Se Guardo el detalle';
		}elseif ($id=='OficinaPub'){
			$criterio="Oficina.id=".$_POST['remitente'];
			$datos_usuario = $this->Oficina->find('first',array('conditions'=>$criterio));
			$data['Documentopermiso'][$_POST['remitente']]=array('usuario_id'=>0,'usuario'=>$datos_usuario['Oficina']['siglas'],'permiso'=>$_POST['permiso'],'oficina_id'=>$_POST['remitente']);
			$mensaje='Se Guardo el detalle';
		}elseif ($id=='Todos'){
			$criterio=" ";
			$datos_usuario = $this->Usuario->find('all',array('conditions'=>$criterio));
			foreach ($datos_usuario as $row){
				$data['Documentopermiso'][$row['Usuario']['id']]=array('usuario_id'=>$row['Usuario']['id'],'usuario'=>$row['Usuario']['usuario'],'permiso'=>$_POST['permiso']);
			}
			$mensaje='Se Guardo el detalle';
		}
		}else{
			$mensaje='Debe seleccionar el Permiso';
		}

		$this->Session->write('Datos_'.$_POST['valorControl'], $data);
		$valorControl=$_POST['valorControl'];
		$this->set(compact('data','mensaje','valorControl'));
		$this->render('guardardetalle', 'ajax');
	}
	
	function viewsecciones($id){
		$data = $this->Esquema->find('all',array('conditions'=>' Esquema.plantilla_id='.$id." and Esquema.status='A'",'order'=>'Esquema.id'));
		$this->set(compact('data'));
		$this->render('viewsecciones', 'ajax');
	}
	
	public function viewpdf($id){
		$this->checkSession();
		$this->layout = 'pdf';				
		$criteria = 'Contenido.documento_id='.$id;
		$data = $this->Contenido->find('all',array('conditions'=>$criteria,'recursive'=>2));		
		list($secciones,$texto)=$this->Contenido->cargarContenido($data,'Contenido');
		$doc = $this->Documento->find('first',array('conditions'=>' Documento.id='.$id,'recursive'=>1));
		$empresa = $this->Empresa->find('first',array('conditions'=>''));
		$criteria = 'Plantilla.id='.$doc['Documento']['plantilla_id'];		
		$plantilla = $this->Plantilla->find('first',array('conditions'=>$criteria,'recursive'=>-1));			
		//cargarContenido
		$this->set(compact('data','texto','plantilla','doc','empresa'));
	}

	public function viewpdfsp($id){
		$this->checkSession();
		$this->layout = 'pdf';				
		$criteria = 'Contenido.documento_id='.$id;
		$data = $this->Contenido->find('all',array('conditions'=>$criteria,'recursive'=>2));		
		list($secciones,$texto)=$this->Contenido->cargarContenido($data,'Contenido');
		$doc = $this->Documento->find('first',array('conditions'=>' Documento.id='.$id,'recursive'=>1));
		$criteria = 'Plantilla.id='.$doc['Documento']['plantilla_id'];		
		$plantilla = $this->Plantilla->find('first',array('conditions'=>$criteria,'recursive'=>-1));			
		//cargarContenido
		$this->set(compact('data','texto','plantilla','doc'));
	}

}

?>
