<?php
class ConcuadrediariosController extends AppController {

    public $name = 'Concuadrediarios';
    public $helpers = array('Html', 'Form'/*,'Pagination','Ajax','fpdf'*/);
    public $components = array ('Pagination');
    public $uses = array ('Concuadrediario','Funcione','Grupo','Configuracion','Empleado','Perfile','Empresa','Movbancario','Sucgasto','Banco','Venta','Sucursal','Conmovbancario','Congasto','Conventa','Conretencionventadet','Conretencionventa','Cuadrediario','Condevolucionventa','Condevolucionventaproducto','Conventaproducto','Dia','Concargo','Concargoproducto','Condescargo','Condescargoproducto');
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    function index() {
        $title_for_layout='Cuadres Consolidados';	
        $sucursal=$this->Empresa->llenar_combo('',4);
        $primeraFecha = $this->Concuadrediario->primeraFecha('Por_Conciliar');
        $this->set(compact('title_for_layout','sucursal','primeraFecha'));

        $criteria=' Concuadrediario.nro_movpendientes>0 ';     			
        $this->Concuadrediario->recursive = 1;		
        $order=' Concuadrediario.fecha desc,Concuadrediario.codsucursal ';
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
        $this->set('data', $this->paginate());
    }

    function buscar(){
        $datos = $_POST;
        list($datos['fechadesde'],$datos['fechahasta']) = explode('al',trim($datos['fecha']));
        list($datos['sucursal_id'],$datos['conexion']) = explode('-',$datos['sucursal']);
        $data = $this->Concuadrediario->reporte($datos);
        $this->set(compact('data','datos'));
        $this->render('buscar', 'ajax');
    }
	
    function indexedit() {
        $title_for_layout='Cuadres Consolidados';	
        $this->set(compact('title_for_layout'));
        $criteria='';     			
        $this->Concuadrediario->recursive = 0;
        $sucursal=$this->Empresa->llenar_combo('',2);
        $this->set(compact('sucursal'));
        $order=' Concuadrediario.fecha desc';
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
        $this->set('data', $this->paginate());
    }

    function transferir() {
        $title_for_layout='Cuadres Consolidados';	
        $this->set(compact('title_for_layout'));
        $criteria='';     			
        $this->Concuadrediario->recursive = 0;
        $sucursal=$this->Sucursal->llenar_combo('',0);
        //$sucursal=$this->Empresa->llenar_combo('',2);
        $this->set(compact('sucursal'));
        $order=' Concuadrediario.fecha desc';
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
        $this->set('data', $this->paginate());
    }

    function buscarcierre($id=null){

        $sucursal = $this->Sucursal->read(null,$id);
        $datos = $_POST;
        $datos['fecha'] = $this->anomesdia($datos['fecha']);
        // En la Sucursal		
        // 2. Buscar Cantidad Mov. Depositos
        $reg = $this->Concuadrediario->buscarregdepositos($sucursal,$datos['fecha']);
        $totales_suc['Efectivo']['reg'] = $totales_suc['Efectivo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Efectivo']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Efectivo']['monto'] = $reg[0][0]['monto'];
        }
        // 3. Buscar Cantidad Mov. Punto de Ventas
        $reg = $this->Concuadrediario->buscarregdepositos($sucursal,$datos['fecha'],'DEBITO');
        $totales_suc['Debito']['reg'] = $totales_suc['Debito']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Debito']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Debito']['monto'] = $reg[0][0]['monto'];
        }
        // 4. Buscar Cantidad Mov. Cheques
        $reg = $this->Concuadrediario->buscarregdepositos($sucursal,$datos['fecha'],'CHEQUE');
        $totales_suc['Cheque']['reg'] = $totales_suc['Cheque']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Cheque']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Cheque']['monto'] = $reg[0][0]['monto'];
        }
        // 5. Buscar Cantidad Mov. Transferencia
        $reg = $this->Concuadrediario->buscarregdepositos($sucursal,$datos['fecha'],'TRANSFER');
        $totales_suc['Transfer']['reg'] = $totales_suc['Transfer']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Transfer']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Transfer']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Mov. Gastos
        $reg = $this->Concuadrediario->buscarregdepositosgastos($sucursal,$datos['fecha']);
        $totales_suc['Gasto']['reg'] = $totales_suc['Gasto']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Gasto']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Gasto']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Ventas
        $reg = $this->Concuadrediario->buscarregventas($sucursal,$datos['fecha']);
        $totales_suc['Ventas']['reg'] = $totales_suc['Ventas']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Ventas']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Ventas']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Devolucion
        $reg = $this->Concuadrediario->buscarregdevolucion($sucursal,$datos['fecha']);
        $totales_suc['Devolucion']['reg'] = $totales_suc['Devolucion']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Devolucion']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Devolucion']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Cargo
        $reg = $this->Concuadrediario->buscarregcargo($sucursal,$datos['fecha']);
        $totales_suc['Cargo']['reg'] = $totales_suc['Cargo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Cargo']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Cargo']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Descargo
        $reg = $this->Concuadrediario->buscarregdescargo($sucursal,$datos['fecha']);
        $totales_suc['Descargo']['reg'] = $totales_suc['Descargo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Descargo']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Descargo']['monto'] = $reg[0][0]['monto'];
        }

        // En la Principal (Comerdepa)
        // 7. Buscar Cantidad Mov. Depositos
        $reg = $this->Concuadrediario->buscarregprincipal($sucursal,$datos['fecha']);
        $totales_pri['Efectivo']['reg'] = $totales_pri['Efectivo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Efectivo']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Efectivo']['monto'] = $reg[0][0]['monto'];
        }
        // 8. Buscar Cantidad Mov. Punto de Ventas
        $reg = $this->Concuadrediario->buscarregprincipal($sucursal,$datos['fecha'],'DEBITO');
        $totales_pri['Debito']['reg'] = $totales_pri['Debito']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Debito']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Debito']['monto'] = $reg[0][0]['monto'];
        }
        // 9. Buscar Cantidad Mov. Cheques
        $reg = $this->Concuadrediario->buscarregprincipal($sucursal,$datos['fecha'],'CHEQUE');
        $totales_pri['Cheque']['reg'] = $totales_pri['Cheque']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Cheque']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Cheque']['monto'] = $reg[0][0]['monto'];
        }
        // 10. Buscar Cantidad Mov. Transferencia
        $reg = $this->Concuadrediario->buscarregprincipal($sucursal,$datos['fecha'],'TRANSFER');
        $totales_pri['Transfer']['reg'] = $totales_pri['Transfer']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Transfer']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Transfer']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Mov. Gastos
        $reg = $this->Concuadrediario->buscarregprincipalgastos($sucursal,$datos['fecha']);
        $totales_pri['Gasto']['reg'] = $totales_pri['Gasto']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Gasto']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Gasto']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Ventas
        $reg = $this->Concuadrediario->buscarregprincipalventas($sucursal,$datos['fecha']);
        $totales_pri['Ventas']['reg'] = $totales_pri['Ventas']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Ventas']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Ventas']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Devolucion
        $reg = $this->Concuadrediario->buscarregprincipaldevolucion($sucursal,$datos['fecha']);
        $totales_pri['Devolucion']['reg'] = $totales_pri['Devolucion']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Devolucion']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Devolucion']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Cargo
        $reg = $this->Concuadrediario->buscarregprincipalcargo($sucursal,$datos['fecha']);
        $totales_pri['Cargo']['reg'] = $totales_pri['Cargo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Cargo']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Cargo']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Descargo
        $reg = $this->Concuadrediario->buscarregprincipaldescargo($sucursal,$datos['fecha']);
        $totales_pri['Descargo']['reg'] = $totales_pri['Descargo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Descargo']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Descargo']['monto'] = $reg[0][0]['monto'];
        }

        $cuadre = $this->Concuadrediario->buscarregcuadre($sucursal,$datos['fecha']);
        $mensaje='';
        if(isset($cuadre[0][0]['estatus'])){
                if($cuadre[0][0]['estatus']==1){
                        $mensaje='<div class="alert alert-warning"><strong>Cuadre en la Sucursal NO se ha Cerrado. Por favor Cierre para Transferir los Datos</strong></div>';
                }
        }else{
                $mensaje='<div class="alert alert-danger">Cuadre no se encuentra registrado en la Sucursal</div>';
        }

        $this->set(compact('sucursal','datos','totales_suc','totales_pri','mensaje'));
        $this->render('buscarcierre', 'ajax');
    }

    function transferirprincipal($id=null){

        $sucursal = $this->Sucursal->read(null,$id);	
        $datos = $_POST;
        $datos['fecha'] = $this->anomesdia($datos['fecha']);
        // Transferir Datos solo si no esta conciliado el Cuadre
        //----Cuadres-----
        $reg = $this->Concuadrediario->transferirmovbancarios($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferirgastos($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferircuadre($sucursal,$datos['fecha']);
        //----Ventas-----
        $reg = $this->Concuadrediario->transferirventasproductos($sucursal,$datos['fecha']);		
        $reg = $this->Concuadrediario->transferirventaspagos($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferirventas($sucursal,$datos['fecha']);
        //----Retenciones de Ventas------
        $reg = $this->Concuadrediario->transferirretencionesdetalle($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferirretenciones($sucursal,$datos['fecha']);
        //----Devoluciones en Ventas -----		
        $reg = $this->Concuadrediario->transferirdevolucionventaproductos($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferirdevolucionventapagos($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferirdevolucionventacreditopagos($sucursal,$datos['fecha']);
        // 2.- devolucionventa
        $reg = $this->Concuadrediario->transferirdevolucionventa($sucursal,$datos['fecha']);
        // ---- Transferir Cargos / Descargos -----
        $reg = $this->Concuadrediario->transferirproductoscargos($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferircargos($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferirproductosdescargos($sucursal,$datos['fecha']);
        $reg = $this->Concuadrediario->transferirdescargos($sucursal,$datos['fecha']);
        // ---- Buscar Ventas si estan devueltas en otro dia para cambiar estatus --
        $reg = $this->Concuadrediario->actualizarVentasDevolucion($sucursal);		
        // Buscar Clientes Nvos
        $reg = $this->Concuadrediario->transferirclientes($sucursal,$datos['fecha']);	
        // En la Sucursal
        // 2. Buscar Cantidad Mov. Depositos
        $reg = $this->Concuadrediario->buscarregdepositos($sucursal,$datos['fecha']);
        $totales_suc['Efectivo']['reg'] = $totales_suc['Efectivo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Efectivo']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Efectivo']['monto'] = $reg[0][0]['monto'];
        }
        // 3. Buscar Cantidad Mov. Punto de Ventas
        $reg = $this->Concuadrediario->buscarregdepositos($sucursal,$datos['fecha'],'DEBITO');
        $totales_suc['Debito']['reg'] = $totales_suc['Debito']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Debito']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Debito']['monto'] = $reg[0][0]['monto'];
        }
        // 4. Buscar Cantidad Mov. Cheques
        $reg = $this->Concuadrediario->buscarregdepositos($sucursal,$datos['fecha'],'CHEQUE');
        $totales_suc['Cheque']['reg'] = $totales_suc['Cheque']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Cheque']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Cheque']['monto'] = $reg[0][0]['monto'];
        }
        // 5. Buscar Cantidad Mov. Transferencia
        $reg = $this->Concuadrediario->buscarregdepositos($sucursal,$datos['fecha'],'TRANSFER');
        $totales_suc['Transfer']['reg'] = $totales_suc['Transfer']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Transfer']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Transfer']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Mov. Gastos
        $reg = $this->Concuadrediario->buscarregdepositosgastos($sucursal,$datos['fecha']);
        $totales_suc['Gasto']['reg'] = $totales_suc['Gasto']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Gasto']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Gasto']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Ventas
        $reg = $this->Concuadrediario->buscarregventas($sucursal,$datos['fecha']);
        $totales_suc['Ventas']['reg'] = $totales_suc['Ventas']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Ventas']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Ventas']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Devolucion
        $reg = $this->Concuadrediario->buscarregdevolucion($sucursal,$datos['fecha']);
        $totales_suc['Devolucion']['reg'] = $totales_suc['Devolucion']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Devolucion']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Devolucion']['monto'] = $reg[0][0]['monto'];
        }

        // 6. Buscar Cantidad Cargo
        $reg = $this->Concuadrediario->buscarregcargo($sucursal,$datos['fecha']);
        $totales_suc['Cargo']['reg'] = $totales_suc['Cargo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Cargo']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Cargo']['monto'] = $reg[0][0]['monto'];
        }
        // 6. Buscar Cantidad Descargo
        $reg = $this->Concuadrediario->buscarregdescargo($sucursal,$datos['fecha']);
        $totales_suc['Descargo']['reg'] = $totales_suc['Descargo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_suc['Descargo']['reg'] = $reg[0][0]['reg'];
                $totales_suc['Descargo']['monto'] = $reg[0][0]['monto'];
        }
        // En la Principal (Comerdepa)
        // 7. Buscar Cantidad Mov. Depositos
        $reg = $this->Concuadrediario->buscarregprincipal($sucursal,$datos['fecha']);
        $totales_pri['Efectivo']['reg'] = $totales_pri['Efectivo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Efectivo']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Efectivo']['monto'] = $reg[0][0]['monto'];
        }
        // 8. Buscar Cantidad Mov. Punto de Ventas
        $reg = $this->Concuadrediario->buscarregprincipal($sucursal,$datos['fecha'],'DEBITO');
        $totales_pri['Debito']['reg'] = $totales_pri['Debito']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Debito']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Debito']['monto'] = $reg[0][0]['monto'];
        }
        // 9. Buscar Cantidad Mov. Cheques
        $reg = $this->Concuadrediario->buscarregprincipal($sucursal,$datos['fecha'],'CHEQUE');
        $totales_pri['Cheque']['reg'] = $totales_pri['Cheque']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Cheque']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Cheque']['monto'] = $reg[0][0]['monto'];
        }
        // 10. Buscar Cantidad Mov. Transferencia
        $reg = $this->Concuadrediario->buscarregprincipal($sucursal,$datos['fecha'],'TRANSFER');
        $totales_pri['Transfer']['reg'] = $totales_pri['Transfer']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Transfer']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Transfer']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Mov. Gastos
        $reg = $this->Concuadrediario->buscarregprincipalgastos($sucursal,$datos['fecha']);
        $totales_pri['Gasto']['reg'] = $totales_pri['Gasto']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Gasto']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Gasto']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Ventas
        $reg = $this->Concuadrediario->buscarregprincipalventas($sucursal,$datos['fecha']);
        $totales_pri['Ventas']['reg'] = $totales_pri['Ventas']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Ventas']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Ventas']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Devolucion
        $reg = $this->Concuadrediario->buscarregprincipaldevolucion($sucursal,$datos['fecha']);
        $totales_pri['Devolucion']['reg'] = $totales_pri['Devolucion']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Devolucion']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Devolucion']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Cargo
        $reg = $this->Concuadrediario->buscarregprincipalcargo($sucursal,$datos['fecha']);
        $totales_pri['Cargo']['reg'] = $totales_pri['Cargo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Cargo']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Cargo']['monto'] = $reg[0][0]['monto'];
        }
        // 11. Buscar Cantidad Descargo
        $reg = $this->Concuadrediario->buscarregprincipaldescargo($sucursal,$datos['fecha']);
        $totales_pri['Descargo']['reg'] = $totales_pri['Descargo']['monto'] = 0;
        if(isset($reg[0][0]['reg'])){
                $totales_pri['Descargo']['reg'] = $reg[0][0]['reg'];
                $totales_pri['Descargo']['monto'] = $reg[0][0]['monto'];
        }

        $cuadre = $this->Concuadrediario->find('first',array('conditions'=>" Concuadrediario.fecha = '".$datos['fecha']."' and Concuadrediario.codsucursal=".$sucursal['Sucursal']['codsucursal']."" ));
        $this->Concuadrediario->actualizarMontosCuadre($cuadre['Concuadrediario']['id']);
        $mensaje='<div class="alert alert-success"><strong>El Cuadre ha sido transferido</strong></div>';
        $this->set(compact('sucursal','datos','totales_suc','totales_pri','mensaje'));
        $this->render('buscarcierre', 'ajax');
    }

    function view($id = null) {
        if (!$id) {
                $this->Session->setFlash(__('Invalid Cuadrediario.', true));
                $this->redirect(array('action'=>'index'));
        }
        list($codcuadre,$codsucursal) = explode('-',$id);
        $this->Concuadrediario->actualizarMontosCuadre($codcuadre);
        $cuadre=$this->Concuadrediario->read(null, $codcuadre);		
        $depositos = $this->Conmovbancario->buscarDepositos($cuadre);			
        $gastos['Gastos'] = $this->Congasto->find('all',array('order'=>'Congasto.id desc','conditions'=>" Congasto.fecha='".$cuadre['Concuadrediario']['fecha']."' and Congasto.codsucursal=".$codsucursal." "));
        $retenciones['Total'] = $this->Conretencionventadet->buscarResumenDia($cuadre);
        $retenciones['Registros'] = $this->Conretencionventadet->buscarRegistrosDia($cuadre);
        $ventadeldia = $this->Conventa->ventasContado(" and VT.fecha='".$cuadre['Concuadrediario']['fecha']."' and VT.codsucursal=".$codsucursal." ");
        if(!isset($ventadeldia[0][0]['total'])){
                $ventadeldia[0][0]['total']=0;
        }
        $devoluciones = $this->Condevolucionventa->reporte($datos=array('fechahasta'=>$cuadre['Concuadrediario']['fecha'],'fechadesde'=>$cuadre['Concuadrediario']['fecha'],'client'=>array(0),'tipoven'=>array(0)),'',1); 
        $this->set(compact('cuadre','depositos','gastos','ventadeldia','retenciones','devoluciones'));

    }

    function conciliar($id = null) {
        if (!$id) {
                $this->Session->setFlash(__('Invalid Cuadrediario.', true));
                $this->redirect(array('action'=>'index'));
        }

        list($codcuadre,$codsucursal) = explode('-',$id);
        $this->Concuadrediario->actualizarMontosCuadre($codcuadre);
        $cuadre=$this->Concuadrediario->read(null, $codcuadre);
        $depositos = $this->Conmovbancario->buscarDepositos($cuadre,1);			
        $gastos['Gastos'] = $this->Congasto->find('all',array('order'=>'Congasto.id desc','conditions'=>" Congasto.fecha='".$cuadre['Concuadrediario']['fecha']."' and Congasto.codsucursal=".$codsucursal." "));
        $retenciones['Total'] = $this->Conretencionventadet->buscarResumenDia($cuadre);
        $retenciones['Registros'] = $this->Conretencionventadet->buscarRegistrosDia($cuadre);
        $ventadeldia = $this->Conventa->ventasContado(" and VT.fecha='".$cuadre['Concuadrediario']['fecha']."' and VT.codsucursal=".$codsucursal." ");
        if(!isset($ventadeldia[0][0]['total'])){
                $ventadeldia[0][0]['total']=0;
        }
        $devoluciones = $this->Condevolucionventa->reporte($datos=array('fechahasta'=>$cuadre['Concuadrediario']['fecha'],'fechadesde'=>$cuadre['Concuadrediario']['fecha'],'client'=>array(0),'tipoven'=>array(0),'codsucursal'=>$codsucursal),'',1);
        $ventas = $this->Conventa->reporte($datos=array('fechahasta'=>$cuadre['Concuadrediario']['fecha'],'fechadesde'=>$cuadre['Concuadrediario']['fecha'],'client'=>array(0),'tipoven'=>array(0),'codsucursal'=>$codsucursal),'',2);
        $concargos = $this->Concargo->reporte($datos=array('fechahasta'=>$cuadre['Concuadrediario']['fecha'],'fechadesde'=>$cuadre['Concuadrediario']['fecha'],'codsucursal'=>$codsucursal),'','buscar');
        $condescargos = $this->Condescargo->reporte($datos=array('fechahasta'=>$cuadre['Concuadrediario']['fecha'],'fechadesde'=>$cuadre['Concuadrediario']['fecha'],'codsucursal'=>$codsucursal),'','buscar');
        $this->set(compact('cuadre','depositos','gastos','ventadeldia','retenciones','devoluciones','ventas','concargos','condescargos','codsucursal'));

    }

    function editar($id = null) {
        if (!$id) {
                $this->Session->setFlash(__('Invalid Cuadrediario.', true));
                $this->redirect(array('action'=>'index'));
        }

        list($codcuadre,$codsucursal) = explode('-',$id);
        $cuadre=$this->Concuadrediario->read(null, $codcuadre);
        $depositos = $this->Conmovbancario->buscarDepositos($cuadre,1);			
        $gastos['Gastos'] = $this->Congasto->find('all',array('order'=>'Congasto.id desc','conditions'=>" Congasto.fecha='".$cuadre['Concuadrediario']['fecha']."' and Congasto.codsucursal=".$codsucursal." "));
        $retenciones['Total'] = $this->Conretencionventadet->buscarResumenDia($cuadre);
        $retenciones['Registros'] = $this->Conretencionventadet->buscarRegistrosDia($cuadre);
        $ventadeldia = $this->Conventa->ventasContado(" and VT.fecha='".$cuadre['Concuadrediario']['fecha']."' and VT.codsucursal=".$codsucursal." ");
        if(!isset($ventadeldia[0][0]['total'])){
                $ventadeldia[0][0]['total']=0;
        } 
        $this->set(compact('cuadre','depositos','gastos','ventadeldia','retenciones'));

    }

    function guardarmarcados($id){
        if(isset($this->data['Seleccionj'])){
            $mensaje[0] = $this->Conmovbancario->guardar_seleccionados($this->data['Seleccionj']);
        }
        if(isset($this->data['Seleccionp'])){
            $mensaje[1] = $this->Conmovbancario->guardar_seleccionados($this->data['Seleccionp']);
        }
        if(isset($this->data['Selecciong'])){
            $mensaje[2] = $this->Congasto->guardar_seleccionados($this->data['Selecciong']);
        }
        if(isset($this->data['Seleccionr'])){
            $mensaje[3] = $this->Conretencionventa->guardar_seleccionados($this->data['Seleccionr']);
        }
        if(isset($this->data['Selecciondev'])){
            $mensaje[4] = $this->Condevolucionventa->guardar_seleccionados($this->data['Selecciondev']);
        }
        list($codcuadre,$codsucursal) = explode('-',$id);
        $this->Concuadrediario->actualizarMontosCuadre($codcuadre);
        $this->set(compact('id','mensaje'));
    }

    function viewpdf($id = null) {
        $this->checkSession();
        $this->layout = 'pdf';
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$this->ID_EMPRESA));
        list($codcuadre,$codsucursal) = explode('-',$id);
        $cuadre=$this->Concuadrediario->read(null, $codcuadre);        
        $encabezado['titulo_reporte']='REPORTE DE CIERRE DE CAJA DE LA '.strtoupper($cuadre['Sucursal']['descripcion']).' DE FECHA '.date('d-m-Y',strtotime($cuadre['Concuadrediario']['fecha']));
        $depositos = $this->Conmovbancario->buscarDepositos($cuadre,1);         
//------------------Congasto------------------------------------------ 
        $gastos['Gastos'] = $this->Congasto->find('all',array('order'=>'Congasto.id desc','conditions'=>" Congasto.fecha='".$cuadre['Concuadrediario']['fecha']."' and Congasto.codsucursal=".$codsucursal." "));
//------------------Conretencionventadet------------------------------------------        
        $retenciones['Total'] = $this->Conretencionventadet->buscarResumenDia($cuadre);
        $retenciones['Registros'] = $this->Conretencionventadet->buscarRegistrosDia($cuadre);
//------------------Conventa------------------------------------------------------ 
        $dataResumen = $this->Conventa->obtenerResumenDia($cuadre);
//------------------Condevolucionventa---------------------------------------------        
        $dataResumenDev = $this->Condevolucionventa->obtenerResumenDia($cuadre);
        
        $this->set(compact('cuadre','depositos','gastos','ventadeldia','empresa','encabezado','retenciones','dataResumenDev','dataResumen'));
        $this->render("viewpdf1");
    }
	

    function add($id = null) {

        $nro=$this->Cuadrediario->find('all',array('conditions'=>' Cuadrediario.estatus=0 ','fields'=>'Cuadrediario.nro','order'=> 'id desc'));

        if(count($nro)>0){
            $this->Session->setFlash(__('Posee un Cuadre diarios abierto', true));
            $this->redirect(array('action'=>'index'));
        }else{
            $ult_nro=$this->Cuadrediario->find('first',array('conditions'=>' Cuadrediario.estatus=1 ','fields'=>'Cuadrediario.nro','order'=> 'nro desc'));
        }
        $this->set(compact('ult_nro'));
        if (!empty($this->data)) {
            //$this->Cuadrediario->create();
            if ($this->Cuadrediario->save($this->data)) {
                $this->Session->setFlash(__('El Cuadre diario ha sido creado', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('The Cuadrediario could not be saved. Please, try again.', true));
            }
        }
    }

    function edit($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Invalid Cuadrediario', true));
            $this->redirect(array('action'=>'index'));
        }
        if (!empty($this->data)) {
            
            if ($this->Cuadrediario->save($this->data)) {
                $this->flash('El Cuadre diario ha sido Actualizada.','/cuadrediarios');
            } else {
                $this->Session->setFlash(__('El Cuadre diario no se guardo. Por favor, intente otra vez.', true));
            }
        }
        if (empty($this->data)) {
            $this->data = $this->Cuadrediario->read(null, $id);
        }
    }

    function editarmovimientos($id=null){
      $opcion = $_POST['opcion'];
      switch ($opcion) {
        case "Deposito_Juridico":
            $datos['Conmovbancario'] = $_POST; $TIPO = 'JURIDICA';$SELEC = 'Seleccionj';
            $this->Conmovbancario->save($datos);
            $cuadre=$this->Concuadrediario->read(null, $id);
            $depositos = $this->Conmovbancario->buscarDepositos($cuadre,1);
            $this->set(compact('cuadre','depositos','TIPO','SELEC'));
            $this->render('editarmovimientos', 'ajax');
        break;
        case "Deposito_Personal":
            $datos['Conmovbancario'] = $_POST; $TIPO = 'PERSONAL';$SELEC = 'Seleccionp';
            $this->Conmovbancario->save($datos);
            $cuadre=$this->Concuadrediario->read(null, $id);
            $depositos = $this->Conmovbancario->buscarDepositos($cuadre,1);
            $this->set(compact('cuadre','depositos','TIPO','SELEC'));
            $this->render('editarmovimientos', 'ajax');
        break;
        case "Gastos":
            $datos['Congasto'] = $_POST; $TIPO = 'GASTO';
            $this->Congasto->save($datos);
            $cuadre=$this->Concuadrediario->read(null, $id);
            $gastos['Gastos'] = $this->Congasto->find('all',array('order'=>'Congasto.id desc','conditions'=>" Congasto.fecha='".$cuadre['Concuadrediario']['fecha']."' and Congasto.codsucursal=".$cuadre['Concuadrediario']['codsucursal']." "));
            $this->set(compact('cuadre','gastos','TIPO'));
            $this->render('editargastos', 'ajax');
        break;
        case "Retencion":
            $datos['Conretencionventadet'] = $_POST; $datos['Conretencionventa'] = $_POST;	
            $datos['Conretencionventa']['id'] = $_POST['id_ret'];
            $this->Conretencionventadet->save($datos);
            $this->Conretencionventa->save($datos);
            $cuadre=$this->Concuadrediario->read(null, $id);
            $retenciones['Total'] = $this->Conretencionventadet->buscarResumenDia($cuadre);
            $retenciones['Registros'] = $this->Conretencionventadet->buscarRegistrosDia($cuadre);
            $this->set(compact('cuadre','retenciones','TIPO'));
            $this->render('editarretencion', 'ajax');
        break;					
    }//Cierre de switch

    }

    function cerrar($id = null) {
        if (!$id && empty($this->data)) {
            $this->Session->setFlash(__('Codigo invalido', true));
            $this->redirect(array('action'=>'index'));
        }
        $this->data = $this->Cuadrediario->read(null, $id);
        $this->data['Cuadrediario']['estatus']=1;
        if (!empty($this->data)) {
            if ($this->Cuadrediario->save($this->data)) {
                $this->Session->setFlash(__('El Cuadre diario ha sido Cerrado', true));
                $this->redirect(array('action'=>'index'));
            } else {
                $this->Session->setFlash(__('El Cuadre diario no pudo ser salvado. Por favor, intentelo.', true));
            }
        }
    }

    function delete($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid id for Cuadrediario', true));
            $this->redirect(array('action'=>'index'));
        }
        if ($this->Cuadrediario->del($id)) {
            $this->Session->setFlash(__('Cuadrediario deleted', true));
            $this->redirect(array('action'=>'index'));
        }
    }

    function import(){
        if(!empty($this->data)){
            $datos=$this->data;
                //1.- Subir Archivo al Servidor
            if($this->adjuntar_archivo($datos['Archivo']['datos']['tmp_name'],$this->filesProyecto.'consolidados/'.$datos['Archivo']['datos']['name'])){
                $mensaje[0] =  '<div class="alert alert-success">El archivo <strong>'.$datos['Archivo']['datos']['name'].'</strong> se adjunto Correctamente.</div>';
                //2.- Descomprimir Archivo
                list($ej,$mensaje[1]) = $this->decompress_file($this->filesProyecto.'consolidados/'.$datos['Archivo']['datos']['name'],$this->filesProyecto.'consolidados/');
                if($ej){
                //3.- Leer Archivo	
                list($name,$ext) = explode ('.',$datos['Archivo']['datos']['name']);
                $archivo = "".$name.".txt"; $texto = '';
                //echo $this->filesProyecto."consolidados/".$archivo;
                if (file_exists($this->filesProyecto."consolidados/".$archivo)){
                $fp = fopen($this->filesProyecto."consolidados/".$archivo, "r"); $cont = 0;
                //4.- Decodificar Archivo
                while(!feof($fp)) {
                    $linea = fgets($fp);
                    //if($cont>0){
                    $linea = $this->Cuadrediario->desencriptar($linea);
                    //}
                    $texto = $texto.$linea;
                    $cont = $cont + 1;
                }
                fclose($fp);
                $fecha = substr($texto, 0, 10); $sucursal = substr($texto, 10, 1);
                $sentencia = substr($texto, 11);
                $sentencias = explode(";", $sentencia);
                //5.- Buscar y eliminar si hay Registros ya transferidos correspondiente
                $this->Concuadrediario->eliminarregistros($sucursal,$fecha);
                //6.- Ejecutar el contenido del Archivo
                //print_r($sentencias);
                //die();
                foreach ($sentencias as $sql) {
                    if($sql!=''){
                            $this->Concuadrediario->ejecutar($sql);
                    }
                }
                $cuadre = $this->Concuadrediario->find('first',array('conditions'=>" Concuadrediario.fecha = '".$fecha."' and Concuadrediario.codsucursal=".$sucursal."" ));
                $this->Concuadrediario->actualizarMontosCuadre($cuadre['Concuadrediario']['id']);
                }else{
                    $mensaje[0] = '<div class="alert alert-danger">El Archivo No se Encontro. Por favor revise los permisos y la direcci&oacute;n de la carpeta del servidor</div>';
                }
                }

            }else{
                $mensaje[0] = '<div class="alert alert-danger">El Archivo No se pudo Adjuntar. Por favor revise los permisos y la direcci&oacute;n de la carpeta del servidor</div>';
            }
        }else{
            $mensaje = array();$texto = ''; $sql = '';
        }
        $this->set(compact('mensaje','texto'));		
    }

    function ventasdiarias() {
        $title_for_layout='Cuadres Consolidados';	
        $sucursal=$this->Empresa->llenar_combo('',5);
        $primeraFecha = $this->Concuadrediario->primeraFecha('Por_Conciliar');
        $this->set(compact('title_for_layout','sucursal','primeraFecha'));
    }

    function buscardevolucion(){
        $datos = $_POST;
        $data = $this->Condevolucionventaproducto->find('all',array('conditions'=>' Condevolucionventa.id='.$datos['id']));
        $data_vent = $this->Conventaproducto->getProductoVenta($datos);
        $this->set(compact('data','data_vent','datos'));
        $this->render('buscardevolucion', 'ajax');
    }
    
    function buscarvtas(){
        $datos = $_POST;
        $data_vent = $this->Conventaproducto->getProductoVentaTodo($datos);
        $this->set(compact('data_vent','datos'));
        $this->render('buscarvtas', 'ajax');
    }
    
    function buscarcargos(){
        $datos = $_POST;
        $data_vent = $this->Concargoproducto->getProductoCargo($datos);
        $this->set(compact('data_vent','datos'));
        $this->render('buscarcargos', 'ajax');
    }
    
    function buscardescargos(){
        $datos = $_POST;
        $data_vent = $this->Condescargoproducto->getProductoDescargo($datos);
        $this->set(compact('data_vent','datos'));
        $this->render('buscardescargos', 'ajax');
    }

    function buscarventas(){
        $datos = $_POST;
        $fecha['mes'] = $datos['mes']; $fecha['anio'] = $datos['anio'];
        $fecha['inicio'] = $this->first_month_day($datos['mes'],$datos['anio']);
        $fecha['fin'] = $this->last_month_day($datos['mes'],$datos['anio']);
        $fecha['dia_fin'] = substr($fecha['fin'],8,2);
        $data_sd = $this->Concuadrediario->resumenVentas($datos,'','resumen_ventas');     
        $data_sd = $this->Concuadrediario->mesVentas($data_sd,$datos,$fecha);
        $nro_dias_mes = $this->getNroDayMonthAll($fecha);
        $fecha['feriados'] = $this->Dia->buscarDiasFeriados($datos);
        $dias_trans = $this->Concuadrediario->getDiasTranscurridos($fecha,$nro_dias_mes);
        $data = $this->Dia->buscarDiasMes($datos);
        
        if($datos['sdp']=='false'){
            $this->set(compact('data','data_sd','datos','fecha','nro_dias_mes','dias_trans'));
            $this->render('buscarventas', 'ajax');
        }else{
            Configure::write('Model.globalSource', $this->CONEXION_SDP);
            $data_sdp = $this->Concuadrediario->resumenVentas($datos,'','resumen_ventas_sdp');
            $data_sdp = $this->Concuadrediario->mesVentas($data_sdp,$datos,$fecha);
            $data_sdp_pag = $this->Concuadrediario->resumenVentas($datos,'','resumen_pagos_sdp');             
            $data_sdp_pag = $this->Concuadrediario->mesVentas($data_sdp_pag,$datos,$fecha);
            $this->set(compact('data','data_sd','data_sdp','data_sdp_pag','datos','fecha','nro_dias_mes','dias_trans'));
            $this->render('buscarventassdp', 'ajax');
        }        
        
    }
    
    function indexcargo() {
        $title_for_layout='Cuadres Consolidados';	
        $sucursal=$this->Empresa->llenar_combo('',4);
        $primeraFecha = $this->Concargo->primeraFecha('Por_Conciliar');        
        $data = $this->Concargo->listadoCargosDescargos($datos=array('fechadesde'=>date('d-m-Y',strtotime($primeraFecha)),'fechahasta'=>date('d-m-Y')));  
        $sucursales = $this->Sucursal->llenar_combo();
        $this->set(compact('title_for_layout','sucursal','primeraFecha','data','sucursales'));        
    }
    
    function conciliarcargo($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid Cuadrediario.', true));
            $this->redirect(array('action'=>'indexcargo'));
        }

        list($codsucursal,$anio,$mes,$dia) = explode('-',$id);
        $fecha = $dia.'-'.$mes.'-'.$anio;        
        $sucursal=$this->Sucursal->llenar_combo();
        $concargos = $this->Concargo->reporte($datos=array('fechahasta'=>$fecha,'fechadesde'=>$fecha,'codsucursal'=>$codsucursal),'','buscar');
        $condescargos = $this->Condescargo->reporte($datos=array('fechahasta'=>$fecha,'fechadesde'=>$fecha,'codsucursal'=>$codsucursal),'','buscar');
        $ultimonro['cargo'] = $this->Concargo->getNroUltimoFechaAnt($fecha,$codsucursal);
        $ultimonro['descargo'] = $this->Condescargo->getNroUltimoFechaAnt($fecha,$codsucursal);
        $this->set(compact('concargos','condescargos','codsucursal','fecha','sucursal','ultimonro'));
    }
    
    function guardarmarcadoscargo($id){
        $mensaje = array();
        if(isset($this->data['Seleccioncar'])){
            $mensaje[0] = $this->Concargo->guardar_seleccionados($this->data['Seleccioncar']);
        }
        if(isset($this->data['Selecciondescar'])){
            $mensaje[1] = $this->Condescargo->guardar_seleccionados($this->data['Selecciondescar']);
        }              
        $this->set(compact('id','mensaje'));
    }
    
    function viewpdfcargos($id = null) {
        $this->checkSession();
        $this->layout = 'pdf';
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$this->ID_EMPRESA));
        list($codsucursal,$anio,$mes,$dia) = explode('-',$id);
        $fecha = $anio.'-'.$mes.'-'.$dia;       
        $sucursal=$this->Sucursal->llenar_combo();
        $encabezado['titulo_reporte']='LISTADO DE CARGOS Y DESCARGOS DE LA '.strtoupper($sucursal[$codsucursal]).' DE FECHA '.date('d-m-Y',strtotime($fecha));        
        $concargos = $this->Concargo->reporte($datos=array('fechahasta'=>$fecha,'fechadesde'=>$fecha,'codsucursal'=>$codsucursal),'','buscar');
        $condescargos = $this->Condescargo->reporte($datos=array('fechahasta'=>$fecha,'fechadesde'=>$fecha,'codsucursal'=>$codsucursal),'','buscar');     
        $this->set(compact('empresa','encabezado','concargos','condescargos','codsucursal','fecha','sucursal'));
        $this->render('viewpdfcargos');
    }
    
    function buscarlistadoscargos(){
        $datos = $_POST;
        list($datos['fechadesde'],$datos['fechahasta']) = explode('al',trim($datos['fecha']));
        list($datos['sucursal_id'],$datos['conexion']) = explode('-',$datos['sucursal']);
        if ($datos['sucursal_id']>0){
            $datos['codsucursal'] = $datos['sucursal_id'];			
        }
        $data = $this->Concargo->listadoCargosDescargos($datos);
        $sucursales = $this->Sucursal->llenar_combo();
        $this->set(compact('data','datos','sucursales'));
        $this->render('buscarlistadoscargos', 'ajax');
    }

}
?>
