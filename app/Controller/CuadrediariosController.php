<?php
class CuadrediariosController extends AppController {

    public $name = 'Cuadrediarios';
    public $helpers = array('Html', 'Form'/*,'Pagination','Ajax','fpdf'*/);
    public $components = array ('Pagination');
    public $uses = array ('Cuadrediario','Funcione','Grupo','Configuracion','Empleado','Perfile','Empresa','Movbancario','Sucgasto','Banco','Venta','Ventaproducto','Retencionventa','Retencionventacab','Ventaspago','Devolucionventa','Devolucionventaproducto','Devolucionventapago','Devolucionventacreditopago','Cliente','Cargo','Cargoproducto','Descargo','Descargoproducto');

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------

    function index() {
        $criteria=' Cuadrediario.estatus=1 '; 
        $this->Cuadrediario->recursive = 0;
        $sucursal=$this->Empresa->llenar_combo('',2);
        $this->set(compact('sucursal'));
        $order=' Cuadrediario.fecha desc';
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
        $this->set('data', $this->paginate());
    }

    function listadoxabrir() {
        $title_for_layout='Listado de Cuadres para Edici&oacute;n';
        $criteria=' Cuadrediario.estatus=2 ';     	

        $this->Cuadrediario->recursive = 0;
        $sucursal=$this->Empresa->llenar_combo('',2);
        $this->set(compact('sucursal','title_for_layout'));
        $order=' Cuadrediario.fecha desc';
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
        $this->set('data', $this->paginate());
    }

    function buscar(){
        $datos = $_POST;
        $data = $this->Cuadrediario->reporte($datos);
        $this->set(compact('data','datos'));
        $this->render('buscar', 'ajax');
    }

    function buscarxabrir(){
        $datos = $_POST;
        $data = $this->Cuadrediario->reporte($datos);
        $this->set(compact('data','datos'));
        $this->render('buscar', 'ajax');
    }

    function transferir() {
        $criteria='';     	

        $this->Cuadrediario->recursive = 0;
        $sucursal=$this->Empresa->llenar_combo('',2);
        $this->set(compact('sucursal'));
        $order=' Cuadrediario.fecha desc';
        $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
        $this->set('data', $this->paginate());
    }

    function exportar($id = null){
        Configure::write('Model.globalSource', $this->CONEXION);
        $cuadre=$this->Cuadrediario->read(null, $id);
        $cuadre['Cuadrediario']['codsucursal'] = $this->ID_EMPRESA;
        $linea = '';// $filesProyecto
        $archivo = "Cuadre_".date('d-m-Y',strtotime($cuadre['Cuadrediario']['fecha']))."_suc_".$cuadre['Cuadrediario']['codsucursal'].".txt";
        $fp = fopen($this->filesProyecto."cuadres/".$archivo, "w+");
//		fputs($fp, "--Generado por SISCOM --\n");
        //$fec = $cuadre['Cuadrediario']['fecha'];
        $fec = $this->Cuadrediario->encriptar($cuadre['Cuadrediario']['fecha']);
        fputs($fp, $fec."\n");
        //$suc = $cuadre['Cuadrediario']['codsucursal'];
        $suc = $this->Cuadrediario->encriptar($cuadre['Cuadrediario']['codsucursal']);
        fputs($fp, $suc."\n");
        $linea = $this->Cuadrediario->generar_linea_txt($cuadre);
        $linea = $linea.$this->Movbancario->generar_linea_txt($cuadre);
        $linea = $linea.$this->Sucgasto->generar_linea_txt($cuadre);
        $linea = $linea.$this->Venta->generar_linea_txt($cuadre);
        $linea = $linea.$this->Ventaproducto->generar_linea_txt($cuadre);
        $linea = $linea.$this->Ventaspago->generar_linea_txt($cuadre);
        $linea = $linea.$this->Retencionventacab->generar_linea_txt($cuadre);
        $linea = $linea.$this->Retencionventa->generar_linea_txt($cuadre);
        $linea = $linea.$this->Devolucionventa->generar_linea_txt($cuadre);
        $linea = $linea.$this->Devolucionventaproducto->generar_linea_txt($cuadre);
        $linea = $linea.$this->Devolucionventapago->generar_linea_txt($cuadre);
        $linea = $linea.$this->Devolucionventacreditopago->generar_linea_txt($cuadre);
        $linea = $linea.$this->Cargo->generar_linea_txt($cuadre);		
        $linea = $linea.$this->Cargoproducto->generar_linea_txt($cuadre);		
        $linea = $linea.$this->Descargo->generar_linea_txt($cuadre);		
        $linea = $linea.$this->Descargoproducto->generar_linea_txt($cuadre);		
        $linea = $linea.$this->Cliente->generar_linea_txt($cuadre);
        $linea = $this->Cuadrediario->encriptar($linea);
        fputs($fp, $linea);
        fclose($fp);
        $filename = "Cuadre_".date('d-m-Y',strtotime($cuadre['Cuadrediario']['fecha']))."_suc_".$cuadre['Cuadrediario']['codsucursal'].".zip";
        $files[]=array('file'=>$this->filesProyecto."cuadres/".$archivo,'localname'=>$archivo); // Archivos a Comprimir
        list($ej,$mensaje) = $this->compress_file($this->filesProyecto."cuadres/".$filename,$files);

        $this->set(compact('archivo','filename','mensaje'));
    }

    function view($id = null) {
            if (!$id) {
                    $this->Session->setFlash(__('Invalid Cuadrediario.', true));
                    $this->redirect(array('action'=>'index'));
            }
            Configure::write('Model.globalSource', $this->CONEXION);
            $this->Cuadrediario->actualizarMontosCuadre($id);
            $cuadre=$this->Cuadrediario->read(null, $id);
            $depositos = $this->Movbancario->buscarDepositos($cuadre);			
            $gastos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$cuadre['Cuadrediario']['fecha']."' "));
            $retenciones['Total'] = $this->Retencionventa->buscarResumenDia($cuadre);
            $retenciones['Registros'] = $this->Retencionventa->buscarRegistrosDia($cuadre);
            $ventadeldia = $this->Venta->ventasContado(" and VT.fecha='".$cuadre['Cuadrediario']['fecha']."'");
            if(!isset($ventadeldia[0][0]['total'])){
                    $ventadeldia[0][0]['total']=0;
            } 
            $this->set(compact('cuadre','depositos','gastos','ventadeldia','retenciones'));

    }

    function viewpdf($id = null) {

        $this->layout = 'pdf';
        $empresa = $this->Empresa->find('first',array('conditions'=>' Empresa.id='.$this->ID_EMPRESA));
        Configure::write('Model.globalSource', $this->CONEXION);
        $cuadre=$this->Cuadrediario->read(null, $id);
        $encabezado['titulo_reporte']='REPORTE DE CIERRE DE CAJA DE FECHA '.date('d-m-Y',strtotime($cuadre['Cuadrediario']['fecha']));
        $depositos = $this->Movbancario->buscarDepositos($cuadre,1);        
//------------------Sucgasto------------------------------------------             
        $gastos['Gastos'] = $this->Sucgasto->find('all',array('order'=>'Sucgasto.id desc','conditions'=>" Sucgasto.fecha='".$cuadre['Cuadrediario']['fecha']."' "));
//------------------Retencionventa------------------------------------------       
        $retenciones['Registros'] = $this->Retencionventa->buscarRegistrosDia($cuadre);
//------------------Venta------------------------------------------------------ 
        $dataResumen = $this->Venta->obtenerResumenDia($cuadre);
//------------------Devolucionventa---------------------------------------------        
        $dataResumenDev = $this->Devolucionventa->obtenerResumenDia($cuadre);
        $this->set(compact('cuadre','depositos','gastos','ventadeldia','empresa','encabezado','retenciones','dataResumenDev','dataResumen'));         
        $this->render('viewpdf1');
    }

    function add($id = null) {

            $nro=$this->Cuadrediario->find('all',array('conditions'=>' Cuadrediario.estatus=0 ','fields'=>'Cuadrediario.nro','order'=> 'id desc'));

            if(count($nro)>0){
                    $this->Session->setFlash(__('Posee un Cuadre diarios abierto', true));
                    $this->redirect(array('action'=>'index'));
            }else{
                    $ult_nro=$this->Cuadrediario->find('first',array('conditions'=>' Cuadrediario.estatus=1 ','fields'=>'Cuadrediario.nro','order'=> 'nro desc'));
            }
            $this->set(compact('ult_nro'));
            if (!empty($this->data)) {
                    //$this->Cuadrediario->create();
                    if ($this->Cuadrediario->save($this->data)) {
                            $this->Session->setFlash(__('El Cuadre diario ha sido creado', true));
                            $this->redirect(array('action'=>'index'));
                    } else {
                            $this->Session->setFlash(__('The Cuadrediario could not be saved. Please, try again.', true));
                    }
            }
    }

    function edit($id = null) {
            if (!$id && empty($this->data)) {
                    $this->Session->setFlash(__('Invalid Cuadrediario', true));
                    $this->redirect(array('action'=>'index'));
            }
            if (!empty($this->data)) {
                    if ($this->Cuadrediario->save($this->data)) {
                            $this->flash('El Cuadre diario ha sido Actualizada.','/cuadrediarios/index');
                    } else {
                            $this->Session->setFlash(__('El Cuadre diario no se guardo. Por favor, intente otra vez.', true));
                    }
            }
            if (empty($this->data)) {
                    $this->data = $this->Cuadrediario->read(null, $id);
            }
    }


    function cerrar($id = null) {
            if (!$id && empty($this->data)) {
                    $this->Session->setFlash(__('Codigo invalido', true));
                    $this->redirect(array('action'=>'index'));
            }
            $this->data = $this->Cuadrediario->read(null, $id);
            $this->data['Cuadrediario']['estatus']=1;
            if (!empty($this->data)) {
                    if ($this->Cuadrediario->save($this->data)) {
                            $this->Session->setFlash(__('El Cuadre diario ha sido Cerrado', true));
                            $this->redirect(array('action'=>'index'));
                    } else {
                            $this->Session->setFlash(__('El Cuadre diario no pudo ser salvado. Por favor, intentelo.', true));
                    }
            }
    }

    function delete($id = null) {
        if (!$id) {
                $this->Session->setFlash(__('Invalid id for Cuadrediario', true));
                $this->redirect(array('action'=>'index'));
        }
        if ($this->Cuadrediario->del($id)) {
                $this->Session->setFlash(__('Cuadrediario deleted', true));
                $this->redirect(array('action'=>'index'));
        }
    }

    function reporteexcel(){
            $this->layout = 'excel';
            $archivo='ventas';
            if (isset($this->data['Cuadrediario']['nombre'])){
                    $archivo=$this->data['Cuadrediario']['nombre'];
            }
            //print_r($_POST);
            $excel=$this->data['Cuadrediario']['excel'];
            $this->set(compact('excel','archivo'));
    }

    function listarventas(){
            $this->checkSession();
            Configure::write('Model.globalSource', $this->CONEXION);
            $title_for_layout='LISTA DE VENTAS EN ESPERA ';
            $criteria = "Venta.estatus='A' and Venta.numerofactura like 'E%' ";
            $order="Venta.fecha ";       
    $this->paginate = array('conditions' => $criteria,'limit' => 20,'order' => $order);
            $this->set('data', $this->paginate('Venta'));
            $this->set(compact('title_for_layout','proveedore'));
    }

    function deleteVentaEspera($id=null){
        $this->checkSession();
        Configure::write('Model.globalSource', $this->CONEXION);
        if ($this->Ventaproducto->deleteAll(array('Ventaproducto.codventa' => $id), false))
        {	
            if ($this->Venta->deleteAll(array('Venta.codventa' => $id), false)) {
                $this->flash('La Venta ha sido Eliminada.','/cuadrediarios/listarventas');
            }else{
                $this->flash('La Venta NO pudo ser Eliminada.','/cuadrediarios/listarventas');
            }
        }else{
            $this->flash('La Venta NO pudo ser Eliminada.','/cuadrediarios/listarventas');
        }
    }
}
?>
