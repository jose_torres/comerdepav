<?php
class AjusteController extends AppController
{
    public $name = 'Ajuste';
    public $helpers = array('Pagination');
    public $components = array ('Pagination'); 
    public $uses = array ('Hajuste', 'HajusteHproducto', 'Hproducto','Producto','Funcione','Grupo','Configuracion','Perfile','Empresa','Sucursal','Departamento','Deposito','Archivo');
    
//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------




}
?>