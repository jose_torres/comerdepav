<?php
class PanelesController extends AppController {

    public $name = 'Paneles';
    public $helpers = array('Pagination'/*,'Ajax','fpdf'*/);
    public $components = array ('Pagination'); 
    public $uses = array ('Usuario','Funcione','Grupo','Configuracion','Empleado','Perfile','Venta','Conretencionventadet','Concuadrediario');
  /*  public $paginate = array('limit' => 20,
						'order' => array('Modulo.nombre' => 'asc')
    );*/

//-----------------------Seguridad--------------------------------------
    public function beforeFilter(){
        $this->checkSession();
        $datos_empleado = $this->datosEmpleado=$this->Session->read('Empleado.nombre');
        $datos_paneles=$this->Session->read('Perfil');
        $datos_menu['funciones']=$this->Configuracion->menu($this->Session->read('Usuario.perfil_id'));
        $datos_menu['hijos']=$this->Funcione->getArbol();
        $this->set(compact('datos_empleado','datos_paneles','datos_menu'));
        $datos_lista=$this->Configuracion->find('all',array('conditions'=>' Configuracion.perfil_id='.$this->Session->read('Usuario.perfil_id')));
        $this->listado_permiso($datos_lista,$this->params['controller'],$this->params['action']);
    }
//----------------------------------------------------------------------
    public function administrador() {
        $title_for_layout='PANEL PRINCIPAL';
        $NroUsuario = $this->Usuario->obtenerNroUsuario();
        $ultimaFechaPago = $this->Venta->ultimaFechaPago();
        $ultimaNroPago = $this->Venta->obtenerNroPago();
        $NroRetSinAsociar = $this->Conretencionventadet->obtenerNroRetenciones();
        $NroCuadreConMovPend = $this->Concuadrediario->obtenerNroCuadreMovPendientes();
        $registrosVentas = $this->Concuadrediario->obtenerVentasConsolidadas();
        //$dias = $this->Dia->find('all',array('conditions'=>'Ayo=2018'));
        $this->set(compact('title_for_layout','NroUsuario','ultimaFechaPago','ultimaNroPago','NroRetSinAsociar','NroCuadreConMovPend','registrosVentas'));
    }

}
?>
