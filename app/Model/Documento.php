<?php
class Documento extends AppModel
{
    var $name = 'Documento';
    var $useTable = 'documentos';

    var $belongsTo = array('Usuario' => array('className' => 'Usuario',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'usuario_id'
					  ),'Tipodocumento' => array('className' => 'Tipodocumento',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'tipodocumento_id'
					  ),'Oficina' => array('className' => 'Oficina',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'oficina_id'
					  ),'Tipojustificacione' => array('className' => 'Tipojustificacione',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'tipojustificacione_id'
					  )
			);

	var $hasMany = array('Documentopermiso' =>
                         array('className'   => 'Documentopermiso',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'documento_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),'Revisione' =>
                         array('className'   => 'Revisione',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'documento_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),

                  );		   	  

  /* Funciones para la vista */ 	
	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$order="Modulo.nombre ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Modulo'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		case 1://Muestra Todos los Bancos registrados con Cuentas Asociadas
		$order="Modulo.nombre ASC";
		$datos = $this->find('all',array('order'=> $order));
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Modulo'];
			if(count($row['Funcione'])>0){	
			$var[$relleno.$item['id']] = $item['nombre'];
			}
		}
		break;				
		}//Cierre de switch						
		return $var;
	}

	function registro_totales($usuario_id=1){
		$datos=array();
		$criteria= " Documento.usuario_id=".$usuario_id." and Documento.estatus='SIN_ENVIAR'";
		$datos['sin_enviar']=$this->find('count', array('conditions' =>$criteria));

		$criteria= " Documento.usuario_id=".$usuario_id." and Documento.estatus='ENVIADO'";
		$datos['enviado']=$this->find('count', array('conditions' =>$criteria));

		$criteria= " Documento.usuario_id=".$usuario_id." and Documento.estatus='APROBADO'";
		$datos['aprobado']=$this->find('count', array('conditions' =>$criteria));
		
		$criteria= " Documento.usuario_id=".$usuario_id." and Documento.estatus='ANULADO'";
		$datos['anulado']=$this->find('count', array('conditions' =>$criteria));

		$criteria= " Documento.usuario_id=".$usuario_id." and Documento.estatus='A_APROBACION'";
		$datos['poraprobar']=$this->find('count', array('conditions' =>$criteria));

		$criteria= " Documento.usuario_id=".$usuario_id." and Documento.estatus='APROBADO'";
		$datos['porpublicar']=$this->find('count', array('conditions' =>$criteria));

		return $datos;

	}

	function eliminar($nro_registro=0,$id=0){
		$mensaje='';
		if($nro_registro<=0){
			$datos =  $this->read(NULL,$id);
			if ($this->delete($id)){
				$mensaje='El Documento ha sido Eliminado.';
				if(!unlink('/var/www/argos/app/webroot/files/archivos/'.$datos['Documento']['archivo_nombre'])){
					$mensaje=$mensaje.' No se borro el Documento. Eliminelo manualmente.';
				}
			}		
		 }else{
			$mensaje='El Documento no ha sido Eliminado, porque posee registro asociados.'; 
		 }
		return $mensaje; 
	}
  /* Fin de Funciones para la vista */  

	function generar_codigo($datos=array(),$ofice=array()) {
		$siglas = $ofice['Oficina']['siglas'];
		$anyo = date('Y');
		$correlativo = $siglas.'-'.$anyo.'-';
		$nro_registo = $this->find('count', array('conditions' =>" Documento.codigo like '".$correlativo."%'"));
		if($nro_registo>0){
			$ultimo_consecutivo = $this->find('first', array('conditions' =>" Documento.codigo like '".$correlativo."%'",'order'=>' Documento.codigo desc '));
			list($Usiglas,$Uanyo,$cons) = explode('-',$ultimo_consecutivo['Documento']['codigo']);
			$cons = $cons + 1;
			$correlativo = $correlativo.$cons;
		}else{
			$correlativo = $correlativo.'1';
		}
		return $correlativo;		
	}
	
	function generar_nombre_archivo($parte_nom='',$datos=array()){
		list($nombre_archivo,$ext)= explode('.',$datos['Documento']['archivo']['name']);
		$nombre='';
		$nombre = $parte_nom.$nombre_archivo.'_rev_'.$datos['Documento']['nrorevision'].'_1.'.$ext;
		return $nombre;
	}	
				  
}
?>
