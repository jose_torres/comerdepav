<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	public $USUARIO = 'postgres';
	public $CLAVE = '123456';
	public $PUERTO = '5432';

	public $IV = 'ComerdepaSiscom0';
	public $OPCION = 'OPENSSL_RAW_DATA';
	//public $OPCION = 'OPENSSL_RAW_DATA';
	public $encryptionMethod = "AES-128-CBC";  // AES is used by the U.S. gov't to encrypt top secret documents.
	public $secretHash = "S3SC4Mc4m2rd2p1";
 
    public function encrypt($input) {
		$output = openssl_encrypt($input, $this->encryptionMethod, $this->secretHash,$this->OPCION,$this->IV);
        return $output;
    }
 
    public function decrypt ($input) {
        $output  = openssl_decrypt($input, $this->encryptionMethod, $this->secretHash,$this->OPCION,$this->IV);
        return $output;
    }
    
	function get_sima($sim='SIMA001'){
		$this->sima=$sim;
		//echo 'Sim:'.$this->sima;
	}
/*
 * 
 * @return string Ej. 2008-12-12
 * @param $fecha Time Ej. '12-12-2008'
 */
    function anomesdia($fecha){
            //list($d,$m,$a)=split('[-/]',$fecha);
            list($d,$m,$a)=explode('-',$fecha);
            return trim($a).'-'.trim($m).'-'.trim($d);
    }
/*
 * 
 * @return string Ej. 12-12-2008
 * @param $fecha Time Ej. '2008-12-12'
 */
    function diamesano($fecha){
        //list($d,$m,$a)=split('[-/]',$fecha);
        list($a,$m,$d)=explode('-',$fecha);
        return trim($d).'-'.$m.'-'.$a;
    }        
/*
 * */
	function construir_or($datos=array(),$campo='',$tipo='nro',$compara='>'){

		switch ($tipo) {
			case 'nro':
			$c_vend='';$cont=0;
			foreach($datos as $row){
				if($cont!=0){
					if($cont==1){
						$c_vend=$c_vend."and (";
					}else{
						$c_vend=$c_vend." or ";
					}
					$c_vend=$c_vend." ".$campo."=".$row;
				}
				$cont=$cont+1;
			}
			if($cont>1){ $c_vend=$c_vend." )";}
			break;
			case 'letra':
			$c_vend='';$cont=0;
			foreach($datos as $row){
				if($cont!=0){
					if($cont==1){
						$c_vend=$c_vend."and (";
					}else{
						$c_vend=$c_vend." or ";
					}
					$c_vend=$c_vend." ".$campo."='".$row."'";
				}
				$cont=$cont+1;
			}
			if($cont>1){ $c_vend=$c_vend." )";}
			break;
			case 'directa':
			$c_vend='';$cont=0;
			foreach($datos as $row){
				if($cont!=0){
					if($cont==1){
						$c_vend=$c_vend."and (";
					}else{
						$c_vend=$c_vend." or ";
					}
					$c_vend=$c_vend." ".$campo."".$compara."".$row;
				}
				$cont=$cont+1;
			}
			if($cont>1){ $c_vend=$c_vend." )";}
			break;
			case 'noesta_nro':
			$c_vend='';$cont=0;
			foreach($datos as $row){
				
				if($cont==0){
					$c_vend=$c_vend." ".$campo." not in (";
				}
				if($cont!=0){
					if($cont==1){
						$c_vend=$c_vend."".$row;
					}else{
						$c_vend=$c_vend.",".$row;
					}					
				}
				$cont=$cont+1;
			}
			if($cont>1){ $c_vend=$c_vend." )";}
			break;	
			case 'noesta_letra':
			$c_vend='';$cont=0;
			foreach($datos as $row){
				
				if($cont==0){
					$c_vend=$c_vend." ".$campo." not in (";
				}
				if($cont!=0){
					if($cont==1){
						$c_vend=$c_vend."'".$row."'";
					}else{
						$c_vend=$c_vend.",'".$row."'";
					}					
				}
				$cont=$cont+1;
			}
			if($cont>1){ $c_vend=$c_vend." )";}
			break;			
		}
		return $c_vend;
	}
	
	public function getDataSource(){
		
        $source = Configure::read('Model.globalSource');
        if($source !== null && $source !== $this->useDbConfig)
        {
            $this->setDataSource($source);
        }

        return parent::getDataSource();
    }
    
    public function ConversionCondicional($opcion){
		$sel='';
		switch ($opcion) {
			case 'Menor_Igual':
				$sel='<=';
			break;
			case 'Mayor_Igual':
				$sel='>=';
			break;
			case 'Mayor':
				$sel='>';
			break;
			case 'Menor':
				$sel='<';
			break;
			case 'Igual':
				$sel='=';
			break;
		}
		return $sel;
	}

	function construir_campo($datos=array(),$ante='',$tipo='nro'){

		switch ($tipo) {
			case 'nro':
			$c_vend='';$cont=0;
			foreach($datos as $row){
				if($cont!=0){
					//$c_vend=$c_vend.",";
					$c_vend=$c_vend.",".$ante.".".$row;
				}
				$cont=$cont+1;
			}
			if($cont>1){ $c_vend=$c_vend."";}
			break;			
		}
		return $c_vend;
	}
/*
 *	@param string $campo Número en formato español Ej. 2.300,24
 * */
	function formato_ingles($campo=''){
		$par1=str_replace(".","", $campo);
		$par2=str_replace(",",".", $par1);
		$campo=$par2;
		return $campo;
	}
/*
 *
 * */
	function validar($campo='',$opcion='vacio_nulo'){
		$valor = '';
		switch ($opcion) {
			case 'vacio_nulo':
			if($campo=='' || $campo==null){
				$valor=0;
			}
			break;			
			case 'boolean_false':
			if($campo=='' || $campo==null){
				$valor='false';
			}
			break;			
			case 'boolean_true':
			if($campo=='' || $campo==null){
				$valor='true';
			}
			break;			
		}
		return $valor;

	}
    /*
 * 
 * @return string Ej. 2
 * @param $fechaMayor Time Ej. '2008-12-14'
* @param $fechaMenor Time Ej. '2008-12-12'
   Devuelve en numero los dias de diferencias entre dos fechas
*/
    function restaFechas($fechaMayor,$fechaMenor){

        list($ano1,$mes1,$dia1) = explode('-',$fechaMayor);
        list($ano2,$mes2,$dia2) = explode('-',$fechaMenor);

        //calculo timestam de las dos fechas
        $timestamp1 = mktime(0,0,0,$mes1,$dia1,$ano1);
        $timestamp2 = mktime(0,0,0,$mes2,$dia2,$ano2);

        //resto a una fecha la otra
        $segundos_diferencia = $timestamp1 - $timestamp2;
        //echo $segundos_diferencia;

        //convierto segundos en días
        $dias_diferencia = $segundos_diferencia / (60 * 60 * 24);

        //obtengo el valor absoulto de los días (quito el posible signo negativo)
        $dias_diferencia = abs($dias_diferencia);

        //quito los decimales a los días de diferencia
        $dias_diferencia = floor($dias_diferencia);

        return $dias_diferencia;

    }   
 /** Actual month last day **/
    function last_month_day($mes='1',$anio='2018') { 
      $month = date($mes);
      $year = date($anio);
      $day = date("d", mktime(0,0,0, $month+1, 0, $year));

      return date('Y-m-d', mktime(0,0,0, $month, $day, $year));
    }

/** Actual month first day **/
    function first_month_day($mes='1',$anio='2018') {
      $month = date($mes);
      $year = date($anio);
      return date('Y-m-d', mktime(0,0,0, $month, 1, $year));
    }

/** Obtener Nro de dia de la Semana en el Mes **/
    function getNroDayMonth($fecha = array(),$dia_semana=1){
        $cont = 0;
        for ($i = 1; $i <= $fecha['dia_fin']; $i++) {
            $fecha_actual = $fecha['anio'].'-'.$fecha['mes'].'-'.str_pad($i, 2, "0", STR_PAD_LEFT);
            if (date('N', strtotime($fecha_actual))==$dia_semana){
                $cont = $cont + 1;
            }
        }	
        return $cont;
    }

    /** Obtener Nro por dia en el Mes **/
    function getNroDayMonthAll($fecha = array()){
        $dia_actual = $cont[1] = $cont[2] = $cont[3] = $cont[4] = $cont[5] = $cont[6] = $cont[7] = 0;
        for ($i = 1; $i <= 7; $i++) {
            $cont[$i] = $this->getNroDayMonth($fecha,$i); 
        }			
        return $cont;
    }
    
    /* Verificar si existe sino devolver un arreglo */
    function validarArreglo($campo='',$datos=array()){
        if(!isset($datos[$campo])){
            $datos[$campo]=array();
        } else {
            if($datos[$campo]==''){
                $datos[$campo]=array();
            }
        }
        return $datos[$campo];
    }
    
}
