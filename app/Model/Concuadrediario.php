<?php
class Concuadrediario extends AppModel {

    public $name = 'Concuadrediario';
    //public $useDbConfig = 'comerdepa_3';
    public $belongsTo = array('Sucursal' => array('className' => 'Sucursal',
                                            'conditions' => '',
                                            'order' => '',
                                            'foreignKey' => 'codsucursal'
                                            )
                                );


    function llenar_combo($relleno=''){
        $order="Cuadrediario.fecha desc";
        $datos = $this->find('all',array('order'=> $order,'recursive'=>0));
        $i=1;
        $var = array();
        $var[0]='Sin Relacion';
        foreach ($datos as $row) {
                $item = $row['Cuadrediario'];	
                $var[$relleno.$item['id']] = date('d-m-Y',strtotime($item['fecha']));
                $i=$i+1;
        }		
        return $var;
    }
	
    function crear($fecha=''){
        $codigo= 1;
        $nro = $this->find('first',array('conditions'=> "Cuadrediario.fecha='".$fecha."'",'recursive'=>0,'order'=>'Cuadrediario.nro desc'));
        if(isset($nro['Cuadrediario']['nro'])){
                $codigo= $nro['Cuadrediario']['nro'] + 1;
        }
        $datos['Cuadrediario']['nro']=$codigo;
        $datos['Cuadrediario']['fecha']=$fecha;
        $datos['Cuadrediario']['estatus']=1;
        $this->save($datos);
    }
	
    function buscar($datos=array()){
        $valores = array();
        $fecha=$this->anomesdia($datos['fechahasta']);	
        $nro_reg = $this->find('count',array('conditions'=> "Cuadrediario.fecha='".$fecha."'",'recursive'=>0));
        if($nro_reg<=0){
                $this->crear($fecha);
        }	
        $valores = $this->find('first',array('conditions'=> "Cuadrediario.fecha='".$fecha."'",'recursive'=>0));

        return $valores;
    }

    function buscarregcuadre($sucursal=array(),$fecha,$tipodeposito ='EFECTIVO'){
        $sql="select * from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select id,nro,fecha,estatus from cuadrediarios') as t1(id bigint,nro bigint,fecha date,estatus bigint) where fecha='".$fecha."' ";
        //echo $sql;
        $data=$this->query($sql);
        return $data;
    }
	
    function buscarregdepositos($sucursal=array(),$fecha,$tipodeposito ='EFECTIVO'){
        $sql="select * from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select movbancarios.fecha, tipodeposito,count(movbancarios.id) as reg, sum(movbancarios.monto) as monto from movbancarios group by movbancarios.fecha,tipodeposito order by movbancarios.fecha') as t1(fecha date,tipodeposito text,reg int,monto numeric) where fecha='".$fecha."' and tipodeposito='".$tipodeposito."'";
        //echo $sql;
        $data=$this->query($sql);
        return $data;
    }

    function buscarregdepositosgastos($sucursal=array(),$fecha){
        $sql="select * from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select fecha, count(sucgastos.id) as reg,sum(sucgastos.monto) as monto from sucgastos group by fecha order by sucgastos.fecha') as t1(fecha date,reg int,monto numeric) where fecha='".$fecha."' ";
        $data=$this->query($sql);
        return $data;
    }
	
    function buscarregventas($sucursal=array(),$fecha){
        $sql="select * from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select fecha, count(ventas.codventa) as reg,sum(ventas.montobruto) as monto from ventas group by fecha order by fecha') as t1(fecha date,reg int,monto numeric) where fecha='".$fecha."' ";
        $data=$this->query($sql);
        return $data;
    }
	
    function buscarregdevolucion($sucursal=array(),$fecha){
        $sql="select * from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select fecha::date, count(ventas.codventa) as reg,sum(ventas.montobruto) as monto from devolucionventa ventas group by fecha::date order by fecha') as t1(fecha date,reg int,monto numeric) where fecha='".$fecha."' ";
        $data=$this->query($sql);
        return $data;
    }
	
    function buscarregcargo($sucursal=array(),$fecha){
        $sql="select * from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select fecha::date, count(distinct CG.codcargo) as reg,sum(PCG.cantidad*coalesce(costounitario::numeric,0)) as monto from cargos CG inner join productoscargos PCG on (CG.codcargo = PCG.codcargo and CG.codsucursal = PCG.codsucursal) group by fecha::date order by fecha') as t1(fecha date,reg int,monto numeric) where fecha='".$fecha."' ";
        $data=$this->query($sql);
        return $data;
    }

    function buscarregdescargo($sucursal=array(),$fecha){
        $sql="select * from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select fecha::date, count(distinct CG.coddescargo) as reg,sum(PCG.cantidad*coalesce(costounitario::numeric,0)) as monto from descargos CG inner join productosdescargos PCG on (CG.coddescargo = PCG.coddescargo and CG.codsucursal = PCG.codsucursal) group by fecha::date order by fecha') as t1(fecha date,reg int,monto numeric) where fecha='".$fecha."' ";
        $data=$this->query($sql);
        return $data;
    }

    function buscarregprincipal($sucursal=array(),$fecha,$tipodeposito ='EFECTIVO'){
            $sql=" select movbancarios.fecha, tipodeposito,codsucursal, count(movbancarios.id) as reg, sum(movbancarios.monto) as monto from conmovbancarios movbancarios where fecha='".$fecha."' and tipodeposito='".$tipodeposito."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." group by movbancarios.fecha,tipodeposito,codsucursal order by movbancarios.fecha";
            $data=$this->query($sql);
            return $data;
    }

    function buscarregprincipalgastos($sucursal=array(),$fecha){
            $sql="select fecha, codsucursal, count(sucgastos.id) as reg,sum(sucgastos.monto) as monto from  congastos sucgastos where fecha='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." group by fecha, codsucursal order by sucgastos.fecha ";
            $data=$this->query($sql);
            return $data;
    }
	
    function buscarregprincipalventas($sucursal=array(),$fecha){
            $sql="select fecha, count(ventas.codventa) as reg,sum(ventas.montobruto) as monto from  conventas ventas  where fecha='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." group by fecha, codsucursal order by fecha ";
            $data=$this->query($sql);
            return $data;
    }

    function buscarregprincipaldevolucion($sucursal=array(),$fecha){
            $sql="select fecha::date, count(ventas.coddevolucion) as reg,sum(ventas.montobruto) as monto from  condevolucionventa ventas  where fecha::date='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." group by fecha::date, codsucursal order by fecha::date ";
            $data=$this->query($sql);
            return $data;
    }
	
    function buscarregprincipalcargo($sucursal=array(),$fecha){
            $sql="select fecha::date, count(distinct CG.codcargo) as reg,sum(PCG.cantidad*coalesce(costounitario::numeric,0)) as monto from concargos CG inner join conproductoscargos PCG on (CG.codcargo = PCG.codcargo and CG.codsucursal = PCG.codsucursal)  where CG.fecha::date='".$fecha."' and CG.codsucursal=".$sucursal['Sucursal']['codsucursal']." group by fecha::date, CG.codsucursal order by fecha::date ";
            $data=$this->query($sql);
            return $data;
    }

    function buscarregprincipaldescargo($sucursal=array(),$fecha){
            $sql="select fecha::date, count(distinct CG.coddescargo) as reg,sum(PCG.cantidad*coalesce(costounitario::numeric,0)) as monto from condescargos CG inner join conproductosdescargos PCG on (CG.coddescargo = PCG.coddescargo and CG.codsucursal = PCG.codsucursal) where CG.fecha::date='".$fecha."' and CG.codsucursal=".$sucursal['Sucursal']['codsucursal']." group by CG.fecha::date, CG.codsucursal order by CG.fecha::date ";
            $data=$this->query($sql);
            return $data;
    }

    function transferirmovbancarios($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql="delete from conmovbancarios where fecha='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO conmovbancarios (id_sucursal, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito)
            select id_sucursal, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select id, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito
            from movbancarios 
            order by movbancarios.fecha,movbancarios.id') as t1(id_sucursal bigint, banco_id int, cuentasbancaria_id int, codsucursal bigint, nrodocumento text, nrocomprobante text, fecha date, cedbeneficiario text, nombeneficiario text, monto numeric, saldo numeric, observacion text, created timestamp, modified timestamp, conciliado character, documentotipo_id int, chequeo boolean, fconciliacion date, impreanombrede text, leyenda1 text, leyenda2 text, docasociado text, montoref numeric, cuadrediario_id int, tipodeposito text) where fecha='".$fecha."' ";
            $data=$this->query($sql);
            return $data;
    }

    function transferirgastos($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql="delete from congastos where fecha='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO congastos (id_sucursal, codsucursal, monto, fecha, cuadrediario_id, nro, nombre, motivo, created, modified)
            select id_sucursal, codsucursal, monto, fecha, cuadrediario_id, nro, nombre, motivo, created, modified from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select id, codsucursal, monto, fecha, cuadrediario_id, nro, nombre, motivo, created, modified
            from sucgastos 
            order by sucgastos.fecha,sucgastos.id') as t1(id_sucursal bigint, codsucursal bigint, monto numeric, fecha date, cuadrediario_id bigint, nro text, nombre text, motivo text, created timestamp, modified timestamp) where fecha='".$fecha."' ";
            $data=$this->query($sql);
            return $data;
    }
	
    function transferircuadre($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql="delete from concuadrediarios where fecha='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO concuadrediarios (id_sucursal, codsucursal ,nro, fecha, estatus, created, modified)
            select id_sucursal, codsucursal, nro, fecha, estatus, created, modified from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select id,".$sucursal['Sucursal']['codsucursal']." as codsucursal,nro, fecha, estatus, created, modified
            from cuadrediarios 
            order by cuadrediarios.fecha,cuadrediarios.id') as t1(id_sucursal bigint, codsucursal bigint, nro int, fecha date, estatus int, created timestamp, modified timestamp) where fecha='".$fecha."' ";
            $data=$this->query($sql);
            return $data;
    }

    function transferirventasproductos($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql = "DELETE FROM conventasproductos WHERE (codventa,codsucursal) IN
                    (
                      SELECT B.codventa, B.codsucursal
                      FROM   conventasproductos  B
                            INNER JOIN conventas C 
                            ON   (B.codventa = C.codventa and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha = '".$fecha."' and C.codsucursal = ".$sucursal['Sucursal']['codsucursal']."
                    )";
            $data=$this->query($sql);

            $sql="INSERT INTO conventasproductos ( codventa, codsucursal, codproducto, cantidad, precio, unidad, iva, factorconversion, costopromedio, indice, unidad2, factorconversion2)
            select codventa, codsucursal, codproducto, cantidad, precio, unidad, iva, factorconversion, costopromedio, indice, unidad2, factorconversion2 from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select vtp.codventa, vtp.codsucursal, vtp.codproducto, vtp.cantidad, vtp.precio, vtp.unidad, vtp.iva, vtp.factorconversion, vtp.costopromedio, vtp.indice, vtp.unidad2, vtp.factorconversion2, vt.fecha from ventasproductos vtp inner join ventas vt on (vt.codventa = vtp.codventa and vt.codsucursal = vtp.codsucursal) order by vt.fecha,vt.hora') as t1(codventa bigint, codsucursal bigint, codproducto bigint, cantidad numeric, precio numeric, unidad text, iva numeric, factorconversion integer, costopromedio numeric, indice integer, unidad2 text, factorconversion2 numeric, fecha date) where fecha='".$fecha."' ";

            $data=$this->query($sql);
            return $data;
    }

    function transferirventas($sucursal=array(),$fecha){

            $sql="delete from conventas where fecha='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO conventas (codventa, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal, hora, coddeposito, codusuarioconectado, recibido, estatus, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3,  retencion, nrocomprobantefiscal, vf, codmaquina, memoria, estatusz, usuariow, correlativo)
            select codventa, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal, hora, coddeposito, codusuarioconectado, recibido, estatus, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, nrocomprobantefiscal, vf, codmaquina, memoria, estatusz, usuariow, correlativo from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select codventa, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal, hora, coddeposito, codusuarioconectado, recibido, estatus, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, nrocomprobantefiscal, vf, codmaquina, memoria, estatusz, usuariow, correlativo
            from ventas 
            order by ventas.fecha') as t1(codventa bigint, codsucursal bigint, numerofactura text, fecha date, codcliente bigint, codvendedor bigint, codclientesucursal bigint, hora time, coddeposito bigint, codusuarioconectado bigint, recibido numeric, estatus text, montobruto numeric, montoexento numeric, baseimp1 numeric, baseimp2 numeric, baseimp3 numeric, ivaimp1 numeric, ivaimp2 numeric, ivaimp3 numeric, porimp1 numeric, porimp2 numeric, porimp3 numeric,  retencion numeric, nrocomprobantefiscal integer, vf text, codmaquina bigint, memoria bigint, estatusz text, usuariow text, correlativo bigint) where fecha='".$fecha."' ";
            $data=$this->query($sql);
            return $data;
    }

    function transferirventaspagos($sucursal=array(),$fecha){

            //$sql="delete from conventaspagos where fecha='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." ";
            $sql = "DELETE FROM conventaspagos WHERE (codventa,codsucursal) IN
                    (
                      SELECT B.codventa, B.codsucursal
                      FROM   conventaspagos  B
                            INNER JOIN conventas C 
                            ON   (B.codventa = C.codventa and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha = '".$fecha."' and C.codsucursal = ".$sucursal['Sucursal']['codsucursal']."
                    )";
            $data=$this->query($sql);

            $sql="INSERT INTO conventaspagos (codventa, codsucursal, tipopago, monto, banco, nrocuenta, numerotransferencia, cuenta, numerocheque, montorecibido, codpuntoventa,ventaspago_id)
            select codventa, codsucursal, tipopago, monto, banco, nrocuenta, numerotransferencia, cuenta, numerocheque, montorecibido, codpuntoventa, id from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select vtp.codventa, vtp.codsucursal, vtp.tipopago, vtp.monto, vtp.banco, vtp.nrocuenta, vtp.numerotransferencia, vtp.cuenta, vtp.numerocheque, vtp.montorecibido, vtp.codpuntoventa, vtp.id, vt.fecha
            from ventaspagos vtp inner join ventas vt on (vt.codventa = vtp.codventa and vt.codsucursal = vtp.codsucursal) order by vt.fecha,vt.hora') as t1(codventa bigint, codsucursal bigint, tipopago text, monto numeric, banco text, nrocuenta text, numerotransferencia text, cuenta text, numerocheque text, montorecibido numeric, codpuntoventa bigint, id bigint, fecha date) where fecha='".$fecha."' ";
            $data=$this->query($sql);
            return $data;
    }

    function transferirretencionesdetalle($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql = "DELETE FROM conretenciones_ventas_detalles WHERE (codretencion,codsucursal) IN (
              SELECT B.codretencion, B.codsucursal
              FROM   conretenciones_ventas_detalles  B
              INNER JOIN conretenciones_ventas C 
              ON   (B.codretencion = C.codretencion and B.codsucursal = C.codsucursal)
                      WHERE  C.fechaemision = '".$fecha."' and C.codsucursal = ".$sucursal['Sucursal']['codsucursal']."
                    )";
            $data=$this->query($sql);

            $sql="INSERT INTO conretenciones_ventas_detalles( codretencion, numero, codcliente, codsucursalcliente, codmovimiento, codsucursal, documento, tipodocumento, montoretenido, mesafecta, anioafecta)
            select codretencion, numero, codcliente, codsucursalcliente, codmovimiento, codsucursal, documento, tipodocumento, montoretenido, mesafecta, anioafecta from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select vtp.codretencion, vtp.numero, vtp.codcliente, vtp.codsucursalcliente, vtp.codmovimiento, vtp.codsucursal, vtp.documento, vtp.tipodocumento, vtp.montoretenido, vtp.mesafecta, vtp.anioafecta, vt.fechaemision from retenciones_ventas_detalles vtp inner join retenciones_ventas vt on (vt.codretencion = vtp.codretencion) order by vt.fechaemision') as t1(codretencion bigint, numero text, codcliente bigint, codsucursalcliente bigint, codmovimiento bigint, codsucursal bigint, documento text, tipodocumento text, montoretenido numeric, mesafecta text, anioafecta text, fechaemision date) where fechaemision='".$fecha."' ";

            $data=$this->query($sql);
            return $data;
    }

    function transferirretenciones($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql = "DELETE FROM conretenciones_ventas C WHERE  C.fechaemision = '".$fecha."' and C.codsucursal = ".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO conretenciones_ventas( codretencion, codsucursal, numero, fechaemision, horaemision, montototal, estatus)
            select codretencion, codsucursal, numero, fechaemision, horaemision, montototal, 
        estatus from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select codretencion, ".$sucursal['Sucursal']['codsucursal']." as codsucursal, numero, fechaemision, horaemision, montototal, estatus from retenciones_ventas vt  order by vt.fechaemision') as t1(codretencion bigint, codsucursal bigint, numero text, fechaemision date, horaemision time, montototal numeric, estatus text) where fechaemision='".$fecha."' ";

            $data=$this->query($sql);
            return $data;
    }

    function transferirdevolucionventa($sucursal=array(),$fecha){

            $sql="delete from condevolucionventa where fecha::date='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO condevolucionventa ( coddevolucion, codsucursal, codventa, codsucursalventa, fecha, codusuario, codusuariosucursal, coddeposito, codccnotacredito, codccnotacreditosucursal, devoluciontotal, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, observacion)
            select coddevolucion, codsucursal, codventa, codsucursalventa, fecha, codusuario, codusuariosucursal, coddeposito, codccnotacredito, codccnotacreditosucursal, devoluciontotal, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, observacion from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select coddevolucion, codsucursal, codventa, codsucursalventa, fecha, codusuario, codusuariosucursal, coddeposito, codccnotacredito, codccnotacreditosucursal, devoluciontotal, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, observacion from devolucionventa 
            order by devolucionventa.fecha') as t1(coddevolucion bigint, codsucursal bigint, codventa bigint, codsucursalventa bigint, fecha timestamp, codusuario bigint, codusuariosucursal bigint, coddeposito bigint, codccnotacredito bigint, codccnotacreditosucursal bigint, devoluciontotal boolean, montobruto numeric, montoexento numeric, baseimp1 numeric, baseimp2 numeric, baseimp3 numeric, ivaimp1 numeric, ivaimp2 numeric, ivaimp3 numeric, porimp1 numeric, porimp2 numeric, porimp3 numeric, retencion numeric, observacion text) where fecha>='".$fecha." 00:00:00' and fecha<='".$fecha." 23:59:59' ";
            //echo $sql;
            $data=$this->query($sql);
            return $data;
    }

    function transferirdevolucionventaproductos($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql = "DELETE FROM condevolucionventaproductos WHERE (coddevolucion,codsucursal) IN
                    (
                      SELECT B.coddevolucion, B.codsucursal
                      FROM   condevolucionventaproductos  B
                            INNER JOIN condevolucionventa C 
                            ON   (B.coddevolucion = C.coddevolucion and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha::date = '".$fecha."' and C.codsucursal = ".$sucursal['Sucursal']['codsucursal']."
                    )";
            $data=$this->query($sql);

            $sql="INSERT INTO condevolucionventaproductos ( coddevolucion, codsucursal, codproducto, cantidad, indice, unidad)
            select coddevolucion, codsucursal, codproducto, cantidad, indice, unidad from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select vtp.coddevolucion, vtp.codsucursal, vtp.codproducto, vtp.cantidad, vtp.indice, vtp.unidad, vt.fecha from devolucionventaproductos vtp inner join devolucionventa vt on (vt.coddevolucion = vtp.coddevolucion and vt.codsucursal = vtp.codsucursal)') as t1(coddevolucion bigint, codsucursal bigint, codproducto bigint, cantidad numeric, indice int, unidad text, fecha timestamp) where fecha>='".$fecha." 00:00:00' and fecha<='".$fecha." 23:59:59' ";

            $data=$this->query($sql);
            return $data;
    }

    function transferircargos($sucursal=array(),$fecha){

            $sql="delete from concargos where fecha::date='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO concargos ( codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus)
            select codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus from cargos 
            order by fecha') as t1(codcargo bigint, codsucursal bigint, descripcion text, fecha timestamp, autorizado text, responsable text, hora time, estatus text) where fecha>='".$fecha." 00:00:00' and fecha<='".$fecha." 23:59:59' ";
            //echo $sql;
            $data=$this->query($sql);
            return $data;
    }

    function transferirproductoscargos($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql = "DELETE FROM conproductoscargos WHERE (codcargo,codsucursal) IN
                    (
                      SELECT B.codcargo, B.codsucursal
                      FROM   conproductoscargos  B
                            INNER JOIN concargos C 
                            ON   (B.codcargo = C.codcargo and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha::date = '".$fecha."' and C.codsucursal = ".$sucursal['Sucursal']['codsucursal']."
                    )";
            $data=$this->query($sql);

            $sql="INSERT INTO conproductoscargos (codcargo, codsucursal, coddeposito, codproducto, totalcosto, cantidad, unidad, factorconversion, costounitario)
            select codcargo, codsucursal, coddeposito, codproducto, totalcosto, cantidad, unidad, factorconversion, costounitario from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'SELECT B.codcargo, B.codsucursal, B.coddeposito, B.codproducto, B.totalcosto, B.cantidad, B.unidad, B.factorconversion, B.costounitario, C.fecha FROM   productoscargos B INNER JOIN cargos C ON (B.codcargo = C.codcargo and B.codsucursal = C.codsucursal)') as t1(codcargo bigint, codsucursal bigint,coddeposito bigint, codproducto bigint, totalcosto numeric, cantidad numeric, unidad text, factorconversion int, costounitario numeric, fecha timestamp) where fecha>='".$fecha." 00:00:00' and fecha<='".$fecha." 23:59:59' ";

            $data=$this->query($sql);
            return $data;
    }

    function transferirdescargos($sucursal=array(),$fecha){

            $sql="delete from condescargos where fecha::date='".$fecha."' and codsucursal=".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO condescargos ( coddescargo, codsucursal, descripcion, fecha, autorizado, responsable, usointerno, hora, estatus)
            select coddescargo, codsucursal, descripcion, fecha, autorizado, responsable, usointerno, hora, estatus from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select coddescargo, codsucursal, descripcion, fecha, autorizado, responsable, usointerno, hora, estatus from descargos 
            order by fecha') as t1(coddescargo bigint, codsucursal bigint, descripcion text, fecha timestamp, autorizado text, responsable text, usointerno boolean, hora time, estatus text) where fecha>='".$fecha." 00:00:00' and fecha<='".$fecha." 23:59:59' ";
            //echo $sql;
            $data=$this->query($sql);
            return $data;
    }

    function transferirproductosdescargos($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql = "DELETE FROM conproductosdescargos WHERE (coddescargo,codsucursal) IN
                    (
                      SELECT B.coddescargo, B.codsucursal
                      FROM   conproductosdescargos  B
                            INNER JOIN condescargos C 
                            ON   (B.coddescargo = C.coddescargo and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha::date = '".$fecha."' and C.codsucursal = ".$sucursal['Sucursal']['codsucursal']."
                    )";
            $data=$this->query($sql);

            $sql="INSERT INTO conproductosdescargos (coddescargo, codsucursal, coddeposito, codproducto, totalcosto, cantidad, unidad, factorconversion, costounitario)
            select coddescargo, codsucursal, coddeposito, codproducto, totalcosto, cantidad, unidad, factorconversion, costounitario from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'SELECT B.coddescargo, B.codsucursal, B.coddeposito, B.codproducto, B.totalcosto, B.cantidad, B.unidad, B.factorconversion, B.costounitario, C.fecha FROM   productosdescargos B INNER JOIN descargos C ON (B.coddescargo = C.coddescargo and B.codsucursal = C.codsucursal)') as t1(coddescargo bigint, codsucursal bigint,coddeposito bigint, codproducto bigint, totalcosto numeric, cantidad numeric, unidad text, factorconversion int, costounitario numeric, fecha timestamp) where fecha>='".$fecha." 00:00:00' and fecha<='".$fecha." 23:59:59' ";

            $data=$this->query($sql);
            return $data;
    }

    function transferirdevolucionventapagos($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql = "DELETE FROM condevolucionventapago WHERE (coddevolucion,codsucursal) IN
                    (
                      SELECT B.coddevolucion, B.codsucursal
                      FROM   condevolucionventapago  B
                            INNER JOIN condevolucionventa C 
                            ON   (B.coddevolucion = C.coddevolucion and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha::date = '".$fecha."' and C.codsucursal = ".$sucursal['Sucursal']['codsucursal']."
                    )";
            $data=$this->query($sql);

            $sql="INSERT INTO condevolucionventapago ( coddevolucion, codsucursal, tipopago, monto)
            select coddevolucion, codsucursal, tipopago, monto from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select vtp.coddevolucion, vtp.codsucursal, vtp.tipopago, vtp.monto, vt.fecha from devolucionventapago vtp inner join devolucionventa vt on (vt.coddevolucion = vtp.coddevolucion and vt.codsucursal = vtp.codsucursal)') as t1(coddevolucion bigint, codsucursal bigint, tipopago text, monto numeric, fecha timestamp) where fecha>='".$fecha." 00:00:00' and fecha<='".$fecha." 23:59:59' ";

            $data=$this->query($sql);
            return $data;
    }

    function transferirdevolucionventacreditopagos($sucursal=array(),$fecha){
            // Eliminar Datos Asociados
            $sql = "DELETE FROM condevolucionventacreditopagos WHERE fecha::date = '".$fecha."' and codsucursal = ".$sucursal['Sucursal']['codsucursal']." ";
            $data=$this->query($sql);

            $sql="INSERT INTO condevolucionventacreditopagos ( coddevolucionventa, codsucursal, tipopago, monto, fecha)
            select coddevolucionventa, codsucursal, tipopago, monto, fecha from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select coddevolucionventa, codsucursal, tipopago, monto, fecha from devolucionventacreditopagos ') as t1(coddevolucionventa bigint, codsucursal bigint, tipopago text, monto numeric, fecha timestamp) where fecha>='".$fecha." 00:00:00' and fecha<='".$fecha." 23:59:59' ";

            $data=$this->query($sql);
            return $data;
    }

function nroclientesnvos($sucursal=array(),$fecha){

            $sql="select count(codcliente) as nro_reg from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select clientes.codcliente, clientes.codsucursal from clientes order by clientes.codcliente') as t1(codcliente bigint,codsucursal bigint) where (codcliente,codsucursal) not in ( select codcliente,codsucursal from conclientes)";

            $data=$this->query($sql);
            return $data;
    }

    function transferirclientes($sucursal=array(),$fecha){
            $data = array(); $reg = 0;
            $reg = $this->nroclientesnvos($sucursal,$fecha);
            if( $reg > 0){
            $sql="INSERT INTO conclientes(
        codcliente, descripcion, rif, clase, representante, direccion, 
        direccion2, telefonos, email, numerodefax, zona, codvendedor, 
        tipodeprecio, limitecredito, diasdecredito, diastolerancia, fechadeinicio, 
        observaciones, tipodecliente, interesdemora, convenioprecio, 
        estatus, porcdescuento, credito, codsucursal, contribuyentepatente, 
        porcentajepatente, modificado, validado, diacobro, diaventa, 
        fechaultimaventa, nuevo, actualizado1, actualizado2)
            select codcliente, descripcion, rif, clase, representante, direccion, 
        direccion2, telefonos, email, numerodefax, zona, codvendedor, 
        tipodeprecio, limitecredito, diasdecredito, diastolerancia, fechadeinicio, 
        observaciones, tipodecliente, interesdemora, convenioprecio, 
        estatus, porcdescuento, credito, codsucursal, contribuyentepatente, 
        porcentajepatente, modificado, validado, diacobro, diaventa, 
        fechaultimaventa, nuevo, actualizado1, actualizado2 from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select codcliente, descripcion, rif, clase, representante, direccion, direccion2, telefonos, email, numerodefax, zona, codvendedor, tipodeprecio, limitecredito, diasdecredito, diastolerancia, fechadeinicio, observaciones, tipodecliente, interesdemora, convenioprecio, estatus, porcdescuento, credito, codsucursal, contribuyentepatente, porcentajepatente, modificado, validado, diacobro, diaventa, fechaultimaventa, nuevo, actualizado1, actualizado2 from clientes order by clientes.codcliente') as t1(codcliente bigint,descripcion text, rif text, clase text, representante text, direccion text, direccion2 text, telefonos text, email text, numerodefax text, zona text, codvendedor bigint, tipodeprecio text, limitecredito numeric, diasdecredito int, diastolerancia int,fechadeinicio date, observaciones text, tipodecliente text, interesdemora text, convenioprecio text, estatus boolean,  porcdescuento numeric, credito boolean, codsucursal bigint, contribuyentepatente boolean, porcentajepatente numeric, modificado int, validado boolean, diacobro text, diaventa text, fechaultimaventa date, nuevo boolean, actualizado1 text, actualizado2 text) where (codcliente,codsucursal) not in ( select codcliente,codsucursal from conclientes) ";

            $data=$this->query($sql);
            }
            return $data;
    }

    function actualizarVentasDevolucion($sucursal=''){
            $sql = " update conventas set estatus='E' where id in (select CVT.id from conventas CVT inner join condevolucionventa CDV  on ( CVT.codsucursal = CDV.codsucursal and CVT.codventa=CDV.codventa) where  CDV.codsucursal=".$sucursal['Sucursal']['codsucursal']." and CVT.estatus<>'E' order by CDV.fecha::date )";
            $data=$this->query($sql);
    }

    function eliminarregistros($sucursal='',$fecha=''){

            $sql="delete from concuadrediarios where fecha='".$fecha."' and codsucursal=".$sucursal." ";
            $data=$this->query($sql);

            $sql = "DELETE FROM conventaspagos WHERE (codventa,codsucursal) IN
                    (
                      SELECT B.codventa, B.codsucursal
                      FROM   conventaspagos  B
                            INNER JOIN conventas C 
                            ON   (B.codventa = C.codventa and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha = '".$fecha."' and C.codsucursal = ".$sucursal."
                    )";
            $data=$this->query($sql);
            $sql = "DELETE FROM conventasproductos WHERE (codventa,codsucursal) IN
                    (
                      SELECT B.codventa, B.codsucursal
                      FROM   conventasproductos  B
                            INNER JOIN conventas C 
                            ON   (B.codventa = C.codventa and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha = '".$fecha."' and C.codsucursal = ".$sucursal."
                    )";
            $data=$this->query($sql);

            $sql="delete from conventas where fecha='".$fecha."' and codsucursal=".$sucursal." ";
            $data=$this->query($sql);

            $sql = "DELETE FROM conretenciones_ventas_detalles WHERE (codretencion,codsucursal) IN (
              SELECT B.codretencion, B.codsucursal
              FROM   conretenciones_ventas_detalles  B
              INNER JOIN conretenciones_ventas C 
              ON   (B.codretencion = C.codretencion and B.codsucursal = C.codsucursal)
                      WHERE  C.fechaemision = '".$fecha."' and C.codsucursal = ".$sucursal."
                    )";
            $data=$this->query($sql);
            $sql = "DELETE FROM conretenciones_ventas C WHERE  C.fechaemision = '".$fecha."' and C.codsucursal = ".$sucursal." ";
            $data=$this->query($sql);		

            $sql="delete from conmovbancarios where fecha='".$fecha."' and codsucursal=".$sucursal." ";
            $data=$this->query($sql);

            $sql="delete from congastos where fecha='".$fecha."' and codsucursal=".$sucursal." ";
            $data=$this->query($sql);

            $sql = "DELETE FROM condevolucionventacreditopagos WHERE fecha = '".$fecha."' and codsucursal = ".$sucursal." ";
            $data=$this->query($sql);

            $sql = "DELETE FROM condevolucionventapago WHERE (coddevolucion,codsucursal) IN
                    (
                      SELECT B.coddevolucion, B.codsucursal
                      FROM   condevolucionventapago  B
                            INNER JOIN condevolucionventa C 
                            ON   (B.coddevolucion = C.coddevolucion and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha::date = '".$fecha."' and C.codsucursal = ".$sucursal."
                    )";
            $data=$this->query($sql);

            $sql = "DELETE FROM condevolucionventaproductos WHERE (coddevolucion,codsucursal) IN
                    (
                      SELECT B.coddevolucion, B.codsucursal
                      FROM   condevolucionventaproductos  B
                            INNER JOIN condevolucionventa C 
                            ON   (B.coddevolucion = C.coddevolucion and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha::date = '".$fecha."' and C.codsucursal = ".$sucursal."
                    )";
            $data=$this->query($sql);

            $sql="delete from condevolucionventa where fecha::date ='".$fecha."' and codsucursal=".$sucursal." ";
            $data=$this->query($sql);

            $sql="delete from conclientes where fechadeinicio='".$fecha."' and codsucursal=".$sucursal." ";
            $data=$this->query($sql);

            $sql = "DELETE FROM conproductoscargos WHERE (codcargo,codsucursal) IN
                    (
                      SELECT B.codcargo, B.codsucursal
                      FROM   conproductoscargos  B
                            INNER JOIN concargos C 
                            ON   (B.codcargo = C.codcargo and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha::date = '".$fecha."' and C.codsucursal = ".$sucursal."
                    )";
            $data=$this->query($sql);

            $sql="delete from concargos where fecha::date='".$fecha."' and codsucursal=".$sucursal." ";
            $data=$this->query($sql);

            $sql = "DELETE FROM conproductosdescargos WHERE (coddescargo,codsucursal) IN
                    (
                      SELECT B.coddescargo, B.codsucursal
                      FROM   conproductosdescargos  B
                            INNER JOIN condescargos C 
                            ON   (B.coddescargo = C.coddescargo and B.codsucursal = C.codsucursal)
                      WHERE  C.fecha::date = '".$fecha."' and C.codsucursal = ".$sucursal."
                    )";
            $data=$this->query($sql);

            $sql="delete from condescargos where fecha::date='".$fecha."' and codsucursal=".$sucursal." ";

            $data=$this->query($sql);

            return $data;
    }

    function ejecutar($sql=''){

            $data = $this->query($sql,false);
            $mensaje =  '<div class="alert alert-success">Se transfirio el Cuadre</div>';
            return $mensaje;
    }

    function obtenerNroVentas($datos=array()){
            $nro = 0;
            $sql = "select count(codventa) as total from conventas VT where VT.estatus <> 'E' and VT.Fecha = '".$datos['Concuadrediario']['fecha']."' and VT.codsucursal = ".$datos['Concuadrediario']['codsucursal']."
            group by VT.fecha";
            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $nro =$data[0][0]['total'];
            }
            return $nro;

    }

    function obtenerNroDevoluciones($datos=array()){
            $nro = 0;
            $sql = "select count(id) as total from condevolucionventa VT where VT.Fecha >= '".$datos['Concuadrediario']['fecha']." 00:00:00' and VT.Fecha <= '".$datos['Concuadrediario']['fecha']." 23:59:59' and VT.codsucursal = ".$datos['Concuadrediario']['codsucursal']." ";

            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $nro = $data[0][0]['total'];
            }
            return $nro;
    }

    function obtenerNroMovPendientes($cuadre=array(),$opcion='Todos'){
            $nro = 0;
            switch ($opcion) {
            case 'Todos':
            $sql = " select count(id) as total from conmovbancarios where (conciliado = '' or conciliado = 'N' or conciliado is null) and fecha = '".$cuadre['Concuadrediario']['fecha']."' and codsucursal = ".$cuadre['Concuadrediario']['codsucursal']." ";
            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $nro = $nro + $data[0][0]['total'];
            }
            $sql = " select count(id) as total from conretenciones_ventas where estatus = 'A' and fechaemision = '".$cuadre['Concuadrediario']['fecha']."' and codsucursal = ".$cuadre['Concuadrediario']['codsucursal']." ";
            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $nro = $nro + $data[0][0]['total'];
            }
            $sql = " select count(id) as total from congastos where estatus = 'N' and fecha = '".$cuadre['Concuadrediario']['fecha']."' and codsucursal = ".$cuadre['Concuadrediario']['codsucursal']." ";
            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $nro = $nro + $data[0][0]['total'];
            }
            $sql = "select count(id) as total from condevolucionventa VT where VT.Fecha >= '".$cuadre['Concuadrediario']['fecha']." 00:00:00' and VT.Fecha <= '".$cuadre['Concuadrediario']['fecha']." 23:59:59' and VT.codsucursal = ".$cuadre['Concuadrediario']['codsucursal']." and VT.conciliado='NO' ";

            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $nro = $nro + $data[0][0]['total'];
            }
            break;	
            }
            return $nro;
    }

    function obtenerMontoVentas($datos=array()){
            $monto = 0;
            $sql = "select sum(montobruto) as total from conventas VT where VT.estatus <> 'E' and VT.Fecha = '".$datos['Concuadrediario']['fecha']."' and VT.codsucursal = ".$datos['Concuadrediario']['codsucursal']."
            group by VT.fecha";
            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $monto =$data[0][0]['total'];
            }
            return $monto;
    }

    function obtenerMontoDebitos($cuadre=array()){
            $monto = 0;
            $sql = "select sum(monto) as total from congastos Congasto where Congasto.fecha='".$cuadre['Concuadrediario']['fecha']."' and Congasto.codsucursal=".$cuadre['Concuadrediario']['codsucursal']." ;";
            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $monto =$data[0][0]['total'];
            }
            return $monto;
    }

    function obtenerMontoDeposito($cuadre=array()){
            $monto = 0;
            $sql = "select sum(monto) as total from conmovbancarios Conmovbancario where Conmovbancario.fecha='".$cuadre['Concuadrediario']['fecha']."' and Conmovbancario.codsucursal=".$cuadre['Concuadrediario']['codsucursal']." and (Conmovbancario.tipodeposito='CHEQUE' or Conmovbancario.tipodeposito='EFECTIVO' or Conmovbancario.tipodeposito='DEBITO' or Conmovbancario.tipodeposito='TRANSFER');";
            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $monto =$data[0][0]['total'];
            }
            return $monto;
    }

    function obtenerMontoRetencion($datos=array()){
            $monto = 0;
            $hasta=$this->anomesdia($datos['Concuadrediario']['fecha']);
            $desde=$this->anomesdia($datos['Concuadrediario']['fecha']);
            $criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
            $criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";
            $criteria=$criteria." and RV.codsucursal = ".$datos['Concuadrediario']['codsucursal']." ";	
            $sql =  " select RV.fechaemision,sum(RVD.montoretenido) as Total from conretenciones_ventas RV inner join conretenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion)
            where ".$criteria." group by RV.fechaemision";
            $data = $this->query($sql);
            if(isset($data[0][0]['total'])){
                    $monto =$data[0][0]['total'];
            }
            return $monto;
    }

    function actualizarMontosCuadre($id=''){
            $datos_cuadre = $this->read(null, $id);
            $cuadre['Concuadrediario']['id']=$id;		
            $cuadre['Concuadrediario']['nro_venta']=$this->obtenerNroVentas($datos_cuadre);
            $cuadre['Concuadrediario']['nro_devolucion']=$this->obtenerNroDevoluciones($datos_cuadre);
            $cuadre['Concuadrediario']['nro_movpendientes']=$this->obtenerNroMovPendientes($datos_cuadre);
            $cuadre['Concuadrediario']['monto_venta']=$this->obtenerMontoVentas($datos_cuadre);
            $cuadre['Concuadrediario']['monto_debito']=$this->obtenerMontoDebitos($datos_cuadre);
            $cuadre['Concuadrediario']['monto_deposito']=$this->obtenerMontoDeposito($datos_cuadre);
            $cuadre['Concuadrediario']['monto_retencion']=$this->obtenerMontoRetencion($datos_cuadre);
            $this->save($cuadre);
    }

    function ultimaFechaPago(){
            $fecha = date('Y-m-d');
            $sql = "select max(fecha) as ultimafecha from concuadrediarios;";
            $data = $this->query($sql);
            if (isset($data[0][0]['ultimafecha'])){
                    $fecha = $data[0][0]['ultimafecha'];
            }
            return $fecha;
    }

    function primeraFecha($opcion='General'){
            $fecha = date('Y-m-d');
            switch ($opcion) {
            case 'General'://Usado para los reportes
            $sql = "select min(fecha) as ultimafecha from concuadrediarios";
            $data = $this->query($sql);
            if (isset($data[0][0]['ultimafecha'])){
                    $fecha = $data[0][0]['ultimafecha'];
            }
            break;	
            case 'Por_Conciliar'://Usado para los reportes
            $sql = "select min(fecha) as ultimafecha from concuadrediarios where nro_movpendientes>0 ";
            $data = $this->query($sql);
            if (isset($data[0][0]['ultimafecha'])){
                    $fecha = $data[0][0]['ultimafecha'];
            }
            break;	
            }
            return $fecha;
    }

    function obtenerNroCuadreMovPendientes($fecha=''){
            $nro = 0;
            if($fecha==''){
                    $fecha = $this->ultimaFechaPago();
            }
            //$sql = "select count(codccabono) as nro from ccabono where fecha='".$fecha."';";
            $nro = $this->find('count',array('conditions'=>" nro_movpendientes>0 "));
            return $nro;
    }

    function obtenerVentasConsolidadas(){
            $data = array();
            $sql = "select resumen.fecha, sum(resumen.princ) as princ, sum(resumen.detal_1) as detal_1, sum(resumen.detal_2) as detal_2, sum(resumen.detal_3) as detal_3 from (
                    select 
                    CCD.fecha,
                    CASE WHEN (CCD.codsucursal=1) THEN CCD.monto_venta  ELSE 0 END as princ,
                    CASE WHEN (CCD.codsucursal=4) THEN CCD.monto_venta  ELSE 0 END as detal_1,
                    CASE WHEN (CCD.codsucursal=2) THEN CCD.monto_venta  ELSE 0 END as detal_2,
                    CASE WHEN (CCD.codsucursal=3) THEN CCD.monto_venta  ELSE 0 END as detal_3
                    from concuadrediarios CCD
                    order by CCD.fecha desc
                    LIMIT 120
            ) resumen
            group by resumen.fecha
            order by resumen.fecha ";
            $data = $this->query($sql);
            return $data;
    }

    function reporte($datos=array(),$criteria='',$opcion='buscar'){
        if(isset($datos['fechahasta']))
        $hasta=$this->anomesdia($datos['fechahasta']);
        if(isset($datos['fechadesde']))
        $desde=$this->anomesdia($datos['fechadesde']);
        switch ($opcion) {
            case 'buscar'://Usado para los reportes
            $order = " Concuadrediario.fecha desc,Concuadrediario.codsucursal";
            $criteria=" Concuadrediario.fecha >= '".$desde."' ";
            $criteria=$criteria." and Concuadrediario.fecha <= '".$hasta."' ";
            if(isset($datos['sucursal_id'])){
                    if($datos['sucursal_id']<>0){
                            $criteria = $criteria." and Concuadrediario.codsucursal=".$datos['sucursal_id'];
                    }
            }
            if(isset($datos['solo'])){
                    if($datos['solo']=='true'){
                            $criteria = $criteria." and Concuadrediario.nro_movpendientes>0 ";
                    }
            }

            $data=$this->find('all',array('conditions'=>$criteria,'order'=>$order));
            break;
            case 'resumen_ventas'://Usado para los reportes
                //$criteria=" Resumen.fecha >= '".$desde."' ";
                //$criteria=$criteria." and Resumen.fecha <= '".$hasta."' ";
                $criteria=" EXTRACT('month' from Resumen.fecha) = '".$datos['mes']."' ";
                $criteria=$criteria." and EXTRACT('year' from Resumen.fecha) = '".$datos['anio']."' ";
                $sql = "select Resumen.fecha,sum(Resumen.Venta_Mayor) as Credito,
                sum(Resumen.Sucursal_1) as suc1, sum(Resumen.Sucursal_2) as suc2,
                sum(Resumen.Detal) as Detal  
                from (
                select Conventa.fecha, 
                sum(CASE WHEN Conventa.codsucursal=1 THEN montobruto  ELSE 0 END) as Venta_Mayor,
                sum(CASE WHEN Conventa.codsucursal=2 THEN montobruto  ELSE 0 END) as Sucursal_1,
                sum(CASE WHEN Conventa.codsucursal=3 THEN montobruto  ELSE 0 END) as Sucursal_2,
                sum(CASE WHEN Conventa.codsucursal=4 THEN montobruto  ELSE 0 END) as Detal
                from conventas Conventa
                group by Conventa.fecha
                union
                select Conventa.fecha::date, 
                sum(CASE WHEN Conventa.codsucursal=1 THEN montobruto  ELSE 0 END)*-1 as Venta_Mayor,
                sum(CASE WHEN Conventa.codsucursal=2 THEN montobruto  ELSE 0 END)*-1 as Sucursal_1,
                sum(CASE WHEN Conventa.codsucursal=3 THEN montobruto  ELSE 0 END)*-1 as Sucursal_2,
                sum(CASE WHEN Conventa.codsucursal=4 THEN montobruto  ELSE 0 END)*-1 as Detal
                from condevolucionventa Conventa
                group by Conventa.fecha::date
                order by fecha
                ) Resumen
                where ".$criteria."
                group by Resumen.fecha";
                $data = $this->query($sql);
            break;
        case 'resumen_ventas_sdp'://Usado para los reportes
                //$criteria=" Resumen.fecha >= '".$desde."' ";
                //$criteria=$criteria." and Resumen.fecha <= '".$hasta."' ";
                $criteria=" EXTRACT('month' from Resumen.fecha) = '".$datos['mes']."' ";
                $criteria=$criteria." and EXTRACT('year' from Resumen.fecha) = '".$datos['anio']."' ";
                $sql = "select Resumen.fecha,sum(Resumen.Venta_Mayor) as Credito,
                sum(Resumen.Sucursal_1) as suc1, sum(Resumen.Sucursal_2) as suc2,
                sum(Resumen.Detal) as Detal  
                from (
                select Conventa.fecha, 
                sum(CASE WHEN Conventa.codsucursal=1 THEN montobruto  ELSE 0 END) as Venta_Mayor,
                sum(CASE WHEN Conventa.codsucursal=2 THEN montobruto  ELSE 0 END) as Sucursal_1,
                sum(CASE WHEN Conventa.codsucursal=3 THEN montobruto  ELSE 0 END) as Sucursal_2,
                sum(CASE WHEN Conventa.codsucursal=4 THEN montobruto  ELSE 0 END) as Detal
                from ventas as Conventa
                group by Conventa.fecha
                union
                select Conventa.fecha::date, 
                sum(CASE WHEN Conventa.codsucursal=1 THEN montobruto  ELSE 0 END)*-1 as Venta_Mayor,
                sum(CASE WHEN Conventa.codsucursal=2 THEN montobruto  ELSE 0 END)*-1 as Sucursal_1,
                sum(CASE WHEN Conventa.codsucursal=3 THEN montobruto  ELSE 0 END)*-1 as Sucursal_2,
                sum(CASE WHEN Conventa.codsucursal=4 THEN montobruto  ELSE 0 END)*-1 as Detal
                from devolucionventa as Conventa
                group by Conventa.fecha::date
                order by fecha
                ) Resumen
                where ".$criteria."
                group by Resumen.fecha";
                $data = $this->query($sql);
            break;
        }
        return $data;
    }

    function resumenVentas($datos=array(),$criteria='',$opcion='buscar'){
        $var = array();
        $data = $this->reporte($datos,'',$opcion);	
        foreach ($data as $row){
            $var[$row[0]['fecha']]=array('fecha'=>$row[0]['fecha'],'detal'=>$row[0]['detal'],'suc1'=>$row[0]['suc1'],'suc2'=>$row[0]['suc2'],'credito'=>$row[0]['credito'],'cobro'=>0);
        }

        return $var;
    }

    function mesVentas($data_venta=array(),$datos=array(),$fecha=array()){
        $data =array();
        for ($i = 1; $i <= $fecha['dia_fin']; $i++) {
        $fecha_actual = $datos['anio'].'-'.$datos['mes'].'-'.str_pad($i, 2, "0", STR_PAD_LEFT);
        if(isset($data_venta[$fecha_actual])){
            $data[$fecha_actual] = $data_venta[$fecha_actual];
        }else{
            $data[$fecha_actual] = array('fecha'=>$fecha_actual,'detal'=>0,'suc1'=>0,'suc2'=>0,'credito'=>0,'cobro'=>0);
        }

        }
        return $data;
    }
    
    public function getDiasTranscurridos($datos=array(),$nro_dias_mes=array()){
        $dias_trans = 0;
        //print_r($datos);
        $fecha_actual = date("Y-m-d");
       // echo $fecha_actual;
        $ultima_fecha_mes = $datos['anio'].'-'.$datos['mes'].'-'.$datos['dia_fin'];
        if ($fecha_actual > $ultima_fecha_mes){
            $dias_trans = $nro_dias_mes[1]+$nro_dias_mes[2]+$nro_dias_mes[3]+$nro_dias_mes[4]+$nro_dias_mes[5] - $datos['feriados'];
        }else{
            $dias_trans = $this->restaFechas($fecha_actual,$datos['inicio']);
        }
        return $dias_trans;        
    }
	
}
?>
