<?php
class Conretencionventadet extends AppModel
{
    public $name = 'Conretencionventadet';
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'conretenciones_ventas_detalles';
/*
 *
 * */
	public $belongsTo = array('Conretencionventa' => array('className' => 'Conretencionventa',
  						'conditions' => ' Conretencionventa.codretencion = Conretencionventadet.codretencion and Conretencionventa.codsucursal = Conretencionventadet.codsucursal and Conretencionventa.numero = Conretencionventadet.numero',
						'order' => '',
						'foreignKey' => '',
						'fields'=> ''
					  ),'Concliente'=> array('className' => 'Concliente',
  						'conditions' => ' Concliente.codcliente = Conretencionventadet.codcliente and Concliente.codsucursal = Conretencionventadet.codsucursal and Conretencionventa.codsucursal = Concliente.codsucursal  ',
						'order' => '',
						'foreignKey' => '',
						'fields'=> ''
					  ),'Conventa'=> array('className' => 'Conventa',
  						'conditions' => ' trim(Conventa.numerofactura) = trim(Conretencionventadet.documento) and Conventa.codcliente = Conretencionventadet.codcliente and Conventa.codsucursal = Conretencionventadet.codsucursal ',
						'order' => '',
						'foreignKey' => '',
						'fields'=> ''
					  ),
					  	  					  
			); 
			
	public $ART_RETENCION="Art. 11: &quot;La Administraci&oacute;n Tributaria podra designar como Responsable del Pago de Impuesto, en calidad de Agentes de Retenci&oacute;n a quienes por sus funciones p&uacute;blicas o por raz&oacute;n de sus Actividades privadas intervengan en operaciones gravadas con el Impuesto establecido en ese Decreto con rango, Valor y Fuerza de Ley&quot;";

	public $TITULO_COMPROBANTE = "COMPROBANTE DE RETENCI&Oacute;N DE IVA";
	public $PROVIDENCIA="Providencia Administrativa Nro SNAT/2015/0049 del 10/08/2015";

	function reporte($datos=array(),$criteria='',$opcion=0){
		if (isset($datos['fechahasta']))
		$hasta=$this->anomesdia($datos['fechahasta']);
		if (isset($datos['fechadesde']))
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$order='Compra.estatus, Compra.codcompra';
			$criteria=" Compra.fregistro >= '".$desde."' ";
			$criteria=$criteria." and Compra.fregistro <= '".$hasta."' ";			
			$c_cliente=$this->construir_or($datos['provee'],'Compra.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Compra.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>0));
		break;		
		case 1:			
			$order=' PAGOS_VENTAS.estatus,PAGOS_VENTAS.descripcion,PAGOS_VENTAS.femision';
			$criteria=" PAGOS_COMPRAS.femision >= '".$desde."' ";
			$criteria=$criteria." and PAGOS_COMPRAS.femision <= '".$hasta."' ";			
			$c_cliente=$this->construir_or($datos['provee'],'PAGOS_COMPRAS.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
			$c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_COMPRAS.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select PAGOS_COMPRAS.codcompra,PAGOS_COMPRAS.nrodocumento, PAGOS_COMPRAS.codproveedor, PAGOS_COMPRAS.rif,PAGOS_COMPRAS.descripcion, PAGOS_COMPRAS.fecha, PAGOS_COMPRAS.baseimp2,PAGOS_COMPRAS.ivaimp2, PAGOS_COMPRAS.retencion,  PAGOS_COMPRAS.femision, PAGOS_COMPRAS.tipomovimiento, PAGOS_COMPRAS.MontoPago, PAGOS_COMPRAS.estatus from (
			select VT.codcompra,VT.nrodocumento, VT.codproveedor,CT.rif,CT.descripcion, VT.femision as Fecha, VT.baseimp2,VT.ivaimp2, VT.retencion,VT.femision,'1COMPRAS'::text AS tipomovimiento, 0 as MontoPago, VT.estatus from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			union all
			select VT.codcompra,CAP.numero as nrodocumento, VT.codproveedor,CT.rif,CAP.banco as descripcion, 
			CASE WHEN upper(CAP.tipopago)='RETENCION' THEN CAP.fecharetencion 
			WHEN upper(CAP.tipopago)='CAJA' THEN VT.femision  ELSE CAP.fechacobro 
			END as Fecha,			 VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'2PAGOS-'||upper(CAP.tipopago) AS tipomovimiento, 
			 CAP.Monto as MontoPago, VT.estatus
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cppagadas  CCP on (CCP.codmovimientocp=VT.codcompra)
			inner join cpabonopago CAP on (CAP.codcpabono=CCP.codmovimientopago)
			union all
			select VT.codcompra, cast(DC.coddevolucioncompra as text) as nrodocumento, VT.codproveedor,CT.rif,
			'DEVOLUCION EN COMPRA' as descripcion, 
			DC.fdevolucion as Fecha,
			 VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'3'||'DEVOLUCION'::text AS tipomovimiento,
			  DC.montobruto as MontoPago, VT.estatus
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join devolucioncompra DC on (DC.codcompra=VT.codcompra)
			union all
			select VT.codcompra, cast(NC.codcpnotacredito as text) as nrodocumento, VT.codproveedor,CT.rif,
			NC.concepto as descripcion, 
			NC.fecha as Fecha,
			 VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'4'||'NOTA DE CREDITO'::text AS tipomovimiento,
			  NC.monto*-1 as MontoPago, VT.estatus
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cpnotacredito NC on (NC.nrodocumento=VT.nrodocumento and NC.codproveedor=VT.codproveedor)
			) PAGOS_COMPRAS
			where 1=1 and ".$criteria."
ORDER BY PAGOS_COMPRAS.estatus,PAGOS_COMPRAS.codcompra,PAGOS_COMPRAS.tipomovimiento;";
			$data = $this->query($sql);
		break;
		case 2:			
			$order=' PAGOS_VENTAS.estatus,PAGOS_VENTAS.descripcion,PAGOS_VENTAS.femision';
			$criteria=" 1=1 ";
			//$criteria=$criteria." and PAGOS_COMPRAS.femision <= '".$hasta."' ";			
			//echo $criteria; .$datos['codproveedor'].
		/*	$sql="SELECT A.codmovimiento,A.tipomovimiento,A.monto - COALESCE(B.monto,0) AS vmonto,NroDocumento,concepto,fvencimiento,femision,A.codsucursal,C.total FROM (SELECT A.codproveedor,codcompra AS codmovimiento,'COMPRAS' AS tipomovimiento,nrodocumento,femision,A.codsucursal, A.montobruto - A.descuento as Total FROM compras A WHERE A.codproveedor = '".$datos['codproveedor']."' Union SELECT A.codproveedor,codcpfactura AS codmovimiento, 'FA' AS tipomovimiento, nrodocumento,fecha AS femision,A.codsucursal, A.monto as Total FROM cpfactura A WHERE A.codproveedor = '".$datos['codproveedor']."' Union SELECT A.codproveedor,codcpnotadebito AS codmovimiento,'ND' AS tipomovimiento,nrodocumento,fecha AS femision,A.codsucursal, A.monto as Total FROM cpnotadebito A WHERE A.codproveedor = '".$datos['codproveedor']."') AS C Inner Join cuentasporpagar a LEFT JOIN (SELECT sum(monto) AS monto,codmovimientocp,tipomovimientocp FROM cppagadas Group By codmovimientocp,tipomovimientocp) AS B ON A.codmovimiento = B.codmovimientocp AND A.tipomovimiento = B.tipomovimientocp ON C.codmovimiento = A.codmovimiento AND C.tipomovimiento = A.tipomovimiento
			where A.codmovimiento not in (select RC.codmovimiento  from retenciones_compras RC where RC.codsucursal=A.codsucursal)
			";*/
			$sql="SELECT A.codmovimiento,A.tipomovimiento,A.monto - COALESCE(B.monto,0) AS vmonto,NroDocumento, concepto,fvencimiento,femision,A.codsucursal,coalesce(C.total,0) as total,coalesce(C.iva,0) as iva,C.exento FROM 
			(SELECT A.codproveedor,codcompra AS codmovimiento,'COMPRAS' AS tipomovimiento,nrodocumento,
			femision,A.codsucursal, 
			sum(round(cast(MCP.baseiva+MCP.baseiva*MCP.poriva/100 as numeric),2)) - A.descuento as Total,
			sum(round(cast(MCP.baseiva*MCP.poriva/100 as numeric),2)) as Iva,
			sum(round(cast(
			CASE WHEN MCP.poriva=0 THEN MCP.baseiva ELSE 0 END  as numeric
			),2)) as Exento			 
			FROM compras A 
			left join movimientoivacp MCP on (MCP.codmovimiento=A.codcompra and 
			MCP.tipomovimiento='COMPRAS') 
			GROUP BY A.codproveedor,codcompra,nrodocumento,femision,A.codsucursal,MCP.poriva,A.descuento
			Union 
			SELECT A.codproveedor,codcpfactura AS codmovimiento, 'FA' AS tipomovimiento, nrodocumento,
			fecha AS femision,A.codsucursal, sum(round(cast(MCP.baseiva+MCP.baseiva*MCP.poriva/100 as numeric),2)) as Total,
			sum(round(cast(MCP.baseiva*MCP.poriva/100 as numeric),2)) as Iva,
			sum(round(cast(
			CASE WHEN MCP.poriva=0 THEN MCP.baseiva ELSE 0 	END  as numeric
			),2)) as Exento  
			FROM cpfactura A 
			left join movimientoivacp MCP on (MCP.codmovimiento=A.codcpfactura and 
			MCP.tipomovimiento='FA')
			group by A.codproveedor,codcpfactura, nrodocumento,fecha,A.codsucursal
			Union 
			SELECT A.codproveedor,codcpnotadebito AS codmovimiento,'ND' AS tipomovimiento,nrodocumento,
			fecha AS femision,A.codsucursal, 
			sum(round(cast(MCP.baseiva+MCP.baseiva*MCP.poriva/100 as numeric),2)) as Total,
			sum(round(cast(MCP.baseiva*MCP.poriva/100 as numeric),2)) as Iva,
			sum(round(cast(
			CASE WHEN MCP.poriva=0 THEN MCP.baseiva ELSE 0 END  as numeric
			),2)) as Exento 
			FROM cpnotadebito A 
			left join movimientoivacp MCP on (MCP.codmovimiento=A.codcpnotadebito and 
			MCP.tipomovimiento='ND')
			GROUP BY A.codproveedor,codcpnotadebito,nrodocumento,fecha,A.codsucursal,MCP.poriva
			) AS C 
			Inner Join cuentasporpagar a 
			LEFT JOIN (SELECT sum(monto) AS monto,codmovimientocp,tipomovimientocp 
			FROM cppagadas Group By codmovimientocp,tipomovimientocp
			) AS B ON A.codmovimiento = B.codmovimientocp AND A.tipomovimiento = B.tipomovimientocp 
			ON C.codmovimiento = A.codmovimiento AND C.tipomovimiento = A.tipomovimiento 
			where A.codmovimiento not in (select RC.codmovimiento from retenciones_compras RC 
			where RC.codsucursal=A.codsucursal) 
			and C.codproveedor = '".$datos['codproveedor']."'";
			//echo $sql;
			$data = $this->query($sql);
		break;
		case 3:			
		    $criteria=" 1>1 ";
			if(count($datos['codmovimientos'])>1 and count($datos['tipomovimientos'])>1){
			$criteria=" 1=1 ";
			$c_vend='';$cont=0;
			foreach($datos['codmovimientos'] as $row){
				if($cont!=0){
					if($cont==1){
						$c_vend=$c_vend."and (";
					}else{
						$c_vend=$c_vend." or ";
					}
					$c_vend=$c_vend." (A.codmovimiento=".$row." and A.tipomovimiento='".$datos['tipomovimientos'][$cont]."')";
				}
				$cont=$cont+1;
			}
			if($cont>1){ $c_vend=$c_vend." )";}
			$c_codmovimiento="and C.codproveedor = '".$datos['codproveedor']."'";
			$criteria=$criteria."  ".$c_codmovimiento;	
			$criteria=$criteria."  ".$c_vend;
			}
			$sql="SELECT A.codmovimiento,A.tipomovimiento,A.monto - COALESCE(B.monto,0) AS vmonto,NroDocumento, concepto, fvencimiento, femision, A.codsucursal, coalesce(C.total,0) as total,coalesce(C.iva,0) as iva,C.exento FROM 
			(SELECT A.codproveedor,codcompra AS codmovimiento,'COMPRAS' AS tipomovimiento,nrodocumento,femision,A.codsucursal, 
			sum(round(cast(MCP.baseiva+MCP.baseiva*MCP.poriva/100 as numeric),2)) - A.descuento as Total,
			sum(round(cast(MCP.baseiva*MCP.poriva/100 as numeric),2)) as Iva,
			sum(round(cast(
			CASE WHEN MCP.poriva=0 THEN MCP.baseiva ELSE 0 END  as numeric
			),2)) as Exento			 
			FROM compras A 
			left join movimientoivacp MCP on (MCP.codmovimiento=A.codcompra and 
			MCP.tipomovimiento='COMPRAS') 
			GROUP BY A.codproveedor,codcompra,nrodocumento,
			femision,A.codsucursal,MCP.poriva
			Union 
			SELECT A.codproveedor,codcpfactura AS codmovimiento, 'FA' AS tipomovimiento, nrodocumento, fecha AS femision,A.codsucursal, sum(round(cast(MCP.baseiva+MCP.baseiva*MCP.poriva/100 as numeric),2)) as Total,
			sum(round(cast(MCP.baseiva*MCP.poriva/100 as numeric),2)) as Iva,
			sum(round(cast(
			CASE WHEN MCP.poriva=0 THEN MCP.baseiva ELSE 0 	END  as numeric
			),2)) as Exento  
			FROM cpfactura A 
			left join movimientoivacp MCP on (MCP.codmovimiento=A.codcpfactura and 
			MCP.tipomovimiento='FA')
			group by A.codproveedor,codcpfactura, nrodocumento,fecha,A.codsucursal
			Union 
			SELECT A.codproveedor,codcpnotadebito AS codmovimiento,'ND' AS tipomovimiento,nrodocumento, fecha AS femision,A.codsucursal, 
			sum(round(cast(MCP.baseiva+MCP.baseiva*MCP.poriva/100 as numeric),2)) as Total,
			sum(round(cast(MCP.baseiva*MCP.poriva/100 as numeric),2)) as Iva,
			sum(round(cast(
			CASE WHEN MCP.poriva=0 THEN MCP.baseiva ELSE 0 END  as numeric
			),2)) as Exento 
			FROM cpnotadebito A 
			left join movimientoivacp MCP on (MCP.codmovimiento=A.codcpnotadebito and 
			MCP.tipomovimiento='ND')
			GROUP BY A.codproveedor, codcpnotadebito, nrodocumento, fecha, A.codsucursal, MCP.poriva
			) AS C 
			Inner Join cuentasporpagar a 
			LEFT JOIN (SELECT sum(monto) AS monto,codmovimientocp,tipomovimientocp 
			FROM cppagadas Group By codmovimientocp,tipomovimientocp
			) AS B ON A.codmovimiento = B.codmovimientocp AND A.tipomovimiento = B.tipomovimientocp 
			ON C.codmovimiento = A.codmovimiento AND C.tipomovimiento = A.tipomovimiento 
			where A.codmovimiento not in (select RC.codmovimiento from retenciones_compras RC 
			where RC.codsucursal=A.codsucursal) 
			and ".$criteria." ";
			//echo $sql;
			$data = $this->query($sql);
		break;
		}
		return $data;
	}

	function get_numero_retencion($opcion='Normal'){
		switch ($opcion) {
		case 'Normal':
			//$sql="select last_value from retnormal_codretnormal_seq";
			$sql="select nextval('retnormal_codretnormal_seq'::regclass) as last_value";
			$data = $this->query($sql);
		break;
		case 'Transito':
			$sql="select nextval('rettransito_codrettransito_seq'::regclass) as last_value ";
			$data = $this->query($sql);
		break;
		}
		return $data[0][0]['last_value'];
	}

	function get_numero($datos=array()){
		$codigo='';
		switch ($datos['Retencioncompra']['tiporetencion']) {
		case 'Normal':
			$codigo=str_pad($this->get_numero_retencion('Normal'), 10, "0", STR_PAD_LEFT);
			$codigo=$datos['Retencioncompra']['anioafecta'].$datos['Retencioncompra']['mesafecta'].$codigo;
		break;
		case 'Transito':
			$codigo=str_pad($this->get_numero_retencion('Transito'), 10, "0", STR_PAD_LEFT);
		break;
		}
		return $codigo;
	}

	function ajustarDatos($opcion='add',$datos){
		switch ($opcion) {
		case 'add':
			$datos['Retencioncompra']['mesafecta']=$datos['Retencioncompra']['mes']['month'];
			$datos['Retencioncompra']['anioafecta']=$datos['Retencioncompra']['anio']['year'];
			$datos['Retencioncompra']['horaemision']=date('h:i:s');
			$datos['Retencioncompra']['fechaemision']=$this->anomesdia($datos['Retencioncompra']['fecha']);
			$datos['Retencioncompra']['fecharegistro']=$this->anomesdia($datos['Retencioncompra']['fecha']);
			$datos['Retencioncompra']['numero']=$this->get_numero($datos);
			$cont=0;
			for ($i = 0; $i < $datos['Retencione']['totales']; $i++) {
				$datos['Retencioncompras'][$i]['mesafecta']=$datos['Retencioncompra']['mes']['month'];
				$datos['Retencioncompras'][$i]['anioafecta']=$datos['Retencioncompra']['anio']['year'];
				$datos['Retencioncompras'][$i]['fechaemision']=$this->anomesdia($datos['Retencioncompra']['fecha']);
				$datos['Retencioncompras'][$i]['fecharegistro']=$this->anomesdia($datos['Retencioncompra']['fecha']);
				$datos['Retencioncompras'][$i]['tiporetencion']=$datos['Retencioncompra']['tiporetencion'];
				$datos['Retencioncompras'][$i]['horaemision']=date('h:i:s');
				$datos['Retencioncompras'][$i]['numero']=$datos['Retencioncompra']['numero'];
			}
			
		break;
		case 'edit':
			$datos['Retencioncompra']['mesafecta']=$datos['Retencioncompra']['mes']['month'];
			$datos['Retencioncompra']['anioafecta']=$datos['Retencioncompra']['anio']['year'];
			$datos['Retencioncompra']['horaemision']=date('h:i:s');
			$datos['Retencioncompra']['fechaemision']=$this->anomesdia($datos['Retencioncompra']['fecha']);
			$datos['Retencioncompra']['numeroant']=$datos['Retencioncompra']['numero'];
			$datos['Retencioncompra']['numero']=$this->get_numero($datos);
		break;
		}
		return $datos;
	}
	
	function nro_documentos($sucursal=1){
		$sql = "SELECT incrementarnumerodocumento('Pago o Abono Compra', ".$sucursal.") as numerodoc";
		$data = $this->query($sql);
		return $data[0][0]['numerodoc'];
	}

	function longitud_documentos($sucursal=1){
		$sql = "select * from numerodocumento where tipodocumento = 'Pago o Abono Compra' and codsucursal = ".$sucursal."";
		$data = $this->query($sql);
		if (isset($data[0][0]['longitud'])){
			return $data[0][0]['longitud'];
		}else{
			return 8;
		}
	}

	function guardarAbono($datos=array()){
		
		$nrodoc= $this->nro_documentos($datos['Retencioncompra']['codsucursal']);
		$longitud= $this->longitud_documentos($datos['Retencioncompra']['codsucursal']);
		$nrodoc=str_pad($nrodoc, $longitud, "0", STR_PAD_LEFT);

		$sql = "select crearcpabono(nextval('cpabono_codcpabono_seq'::regclass),'".$nrodoc."','PAGO DE RETENCION NRO ".$datos['Retencioncompra']['numero']."','".$datos['Retencioncompra']['fechaemision']."') as pagocreado;";
		$data = $this->query($sql);
		$codcpabono = $data[0][0]['pagocreado'];

		$sql = " insert into cpabonopago (codcpabono,tipopago,monto,numero,fecharetencion,mesretencion,anioretencion) values (".$codcpabono.",'retencion',".$datos['Retencioncompra']['montoretenido'].",'".$datos['Retencioncompra']['numero']."','".$datos['Retencioncompra']['fechaemision']."','".$datos['Retencioncompra']['mesafecta']."','".$datos['Retencioncompra']['anioafecta']."');";
		$data = $this->query($sql);
		$pag='PAG';
		if($datos['Retencioncompra']['tiporetencion']=='Transito'){
			$pag='PAG';
		}
		// 1. - Realizar un for para Guardar los movimientos 
		// 2. -
		$sql =  " INSERT INTO cppagadas(codmovimientocp,tipomovimientocp, codmovimientopago,tipomovimientopago,monto,codsucursalcp,fecha,codsucursalpago) Values ('".$datos['Retencioncompra']['codmovimiento']."','".$datos['Retencioncompra']['tipodocumento']."','".$codcpabono."','".$pag."','".$datos['Retencioncompra']['montoretenido']."','".$datos['Retencioncompra']['codsucursal']."','".$datos['Retencioncompra']['fechaemision']."','".$datos['Retencioncompra']['codsucursal']."');";
		
		foreach ($datos['Retencioncompras'] as $row){	
			$sql =  " INSERT INTO cppagadas(codmovimientocp,tipomovimientocp, codmovimientopago,tipomovimientopago,monto,codsucursalcp,fecha,codsucursalpago) Values ('".$row['codmovimiento']."','".$row['tipodocumento']."','".$codcpabono."','".$pag."','".$row['montoretenido']."','".$row['codsucursal']."','".$row['fechaemision']."','".$row['codsucursal']."');";
			$data = $this->query($sql);
		}		
		
	}
	
	function getCabecera(){
		$encabezado['encabezado']=$this->ART_RETENCION;			
		$encabezado['providencia']=$this->PROVIDENCIA;			
		$encabezado['titulo']=$this->TITULO_COMPROBANTE;	
		return $encabezado;
	}
	
	function getQuincena($dia){
		$quincena = '1era Quincena';		
		if($dia>15){
			$quincena = '2da Quincena';
		}
		return $quincena;
	}

	function buscarResumenDia($datos=array()){
		$hasta=$this->anomesdia($datos['Concuadrediario']['fecha']);
		$desde=$this->anomesdia($datos['Concuadrediario']['fecha']);
		$criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";
		$criteria=$criteria." and RV.codsucursal = ".$datos['Concuadrediario']['codsucursal']." ";	
		$sql =  " select RV.fechaemision,sum(RVD.montoretenido) as Total from conretenciones_ventas RV inner join conretenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion)
		where ".$criteria." group by RV.fechaemision";
		$data = $this->query($sql);
		if(!isset($data[0][0]['total'])){
			$data[0][0]['total']=0;
		}
        return $data[0][0];
	}

	function buscarRegistrosDia($datos=array()){
		$hasta=$this->anomesdia($datos['Concuadrediario']['fecha']);
		$desde=$this->anomesdia($datos['Concuadrediario']['fecha']);
		$criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";	
		$criteria=$criteria." and RV.codsucursal = ".$datos['Concuadrediario']['codsucursal']." ";	
		$sql =  " select RV.id as id_cab, RV.estatus, RV.fechaemision, CL.rif,CL.descripcion,RVD.*,CV.montobruto, (CV.ivaimp1+CV.ivaimp2+CV.ivaimp3) as montoiva from conretenciones_ventas RV inner join conretenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion) inner join conclientes CL on (CL.codcliente  = RVD.codcliente and CL.codsucursal = RVD.codsucursalcliente)
		inner join conventas CV on (CV.codventa  = RVD.codmovimiento and CV.codsucursal = RVD.codsucursal)
		where ".$criteria." order by RVD.codretencion";
		
		$data = $this->query($sql);
		
        return $data;
	}

	function eliminarDetalle($codretencion='',$codsucursal=''){
		$sql =  " DELETE FROM retenciones_ventas_detalles where codretencion=".$codretencion." and codsucursal=".$codsucursal." ";
		$data = $this->query($sql);
	}

	function eliminarCabecera($codretencion=''){
		$sql =  " DELETE FROM retenciones_ventas where codretencion=".$codretencion." ";
		$data = $this->query($sql);
	}

	function findRetencion($datos=array()){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		$criteria = "Retencioncompra.tiporetencion='Normal' ";
		$criteria=$criteria." and Retencioncompra.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and Retencioncompra.fechaemision <= '".$hasta." 23:59:59' ";	
		$c_cliente=$this->construir_or($datos['provee'],'Retencioncompra.codproveedor');
		$criteria=$criteria."  ".$c_cliente;
		$order="Retencioncompra.fechaemision DESC,Retencioncompra.numero DESC";        
		$campos= "Retencioncompra.numero,Proveedore.descripcion,Retencioncompra.fechaemision,sum(Retencioncompra.montoiva) as montoiva, sum(Retencioncompra.montoretenido) as montoretenido";
		$grupo= " Retencioncompra.numero,Proveedore.descripcion,Retencioncompra.fechaemision ";
        $data = $this->find('all',array('conditions' => $criteria,'order' => $order,'group'=>$grupo,'fields'=>$campos));
        return $data;

	}

	function nroRetencionNuevos($sucursal=array()){
		$data = array();
		$sql="select COALESCE(count(codretencion),0) as nro_reg from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select RVD.codretencion, RVD.codsucursal from retenciones_ventas_detalles RVD order by RVD.codretencion') as t1(codretencion bigint,codsucursal bigint) where (codretencion,codsucursal) not in (select codretencion,codsucursal from conretenciones_ventas_detalles)";
		//echo $sql;
		$data=$this->query($sql);
		$cont = 0;
		//print_r($data);
		if(isset($data[0][0]['nro_reg'])){
			$cont = $data[0][0]['nro_reg'];
		}
		return $cont;
	}

	function guardarRetencion($sucursal=array()){
		
		$sql="INSERT INTO conretenciones_ventas_detalles( codretencion, numero, codcliente, codsucursalcliente, codmovimiento, codsucursal, documento, tipodocumento, montoretenido, mesafecta, anioafecta)
		select codretencion, numero, codcliente, codsucursalcliente, codmovimiento, codsucursal, documento, tipodocumento, montoretenido, mesafecta, anioafecta from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select vtp.codretencion, vtp.numero, vtp.codcliente, vtp.codsucursalcliente, vtp.codmovimiento, vtp.codsucursal, vtp.documento, vtp.tipodocumento, vtp.montoretenido, vtp.mesafecta, vtp.anioafecta, vt.fechaemision from retenciones_ventas_detalles vtp inner join retenciones_ventas vt on (vt.codretencion = vtp.codretencion) order by vt.fechaemision') as t1(codretencion bigint, numero text, codcliente bigint, codsucursalcliente bigint, codmovimiento bigint, codsucursal bigint, documento text, tipodocumento text, montoretenido numeric, mesafecta text, anioafecta text, fechaemision date) where (codretencion,codsucursal) not in (select codretencion,codsucursal from conretenciones_ventas_detalles) ";
		//echo $sql;
		$data=$this->query($sql);
		return $data;
	}
	
	function incluirFaltante($sucursal=''){
		// Buscar Registros que no estan en el consolidado
		$cantidad = $this->nroRetencionNuevos($sucursal);
		// Incluir si el registro es mayor a cero
		if($cantidad>0){
			$this->guardarRetencion($sucursal);
		}
	}

	function listado($opcion='asociado',$datos=array()){

		switch (trim($opcion)) {
			case 'asociado':
			$criteria ="Conretencionventadet.retgenventa_id <> 0 and Conretencionventa.estatus = 'C' ";
			if(isset($datos['fechadesde'])){
			$desde=$this->anomesdia($datos['fechadesde']);
			$criteria=$criteria." and Conretencionventa.fechaemision >= '".$desde." 00:00:00' ";
			}
			if(isset($datos['fechahasta'])){
			$hasta=$this->anomesdia($datos['fechahasta']);
			$criteria=$criteria." and Conretencionventa.fechaemision <= '".$hasta." 23:59:59' ";
			}			
			if(isset($datos['consulta']) ){
				$criteria=$criteria." and (Conretencionventadet.numero like '%".$datos['consulta']."%' or Conretencionventadet.documento like '%".$datos['consulta']."%' or upper(Concliente.descripcion) like upper('%".$datos['consulta']."%') ) ";
			}
			if(isset($datos['retgenventa_id']) ){
				$criteria=$criteria." and  Conretencionventadet.retgenventa_id = ".$datos['retgenventa_id']." ";
			}
			//echo $criteria;
			$campos = " distinct(Conretencionventadet.id) as id, Conretencionventa.id, Conretencionventadet.codretencion, Conretencionventadet.numero, Conretencionventadet.codcliente, Conretencionventadet.codsucursalcliente, Conretencionventadet.codmovimiento, Conretencionventadet.codsucursal, Conretencionventadet.documento, Conretencionventadet.tipodocumento, Conretencionventadet.montoretenido, Conretencionventadet.mesafecta, Conretencionventadet.anioafecta, Conretencionventadet.retgenventa_id, Concliente.rif, Concliente.descripcion, Conretencionventa.fechaemision, Conventa.ivaimp1, Conventa.ivaimp2, Conventa.ivaimp3, Conventa.fecha";
			$datos = $this->find('all',array('conditions' => $criteria,'order' => " Conretencionventa.fechaemision ",'fields'=>$campos));
			break;
			case 'no_asociado':
			$hasta=$this->anomesdia($datos['fechahasta']);
			$desde=$this->anomesdia($datos['fechadesde']);
			$criteria ="Conretencionventadet.retgenventa_id = 0 and Conretencionventa.estatus = 'C' ";
			$criteria=$criteria." and Conretencionventa.fechaemision >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Conretencionventa.fechaemision <= '".$hasta." 23:59:59' ";			
			if(isset($datos['consulta']) ){
				$criteria=$criteria." and (Conretencionventadet.numero like '%".$datos['consulta']."%' or Conretencionventadet.documento like '%".$datos['consulta']."%' or upper(Concliente.descripcion) like upper('%".$datos['consulta']."%') ) ";
			}
			//echo $criteria;
			$campos = " distinct(Conretencionventadet.id) as id, Conretencionventa.id, Conretencionventadet.codretencion, Conretencionventadet.numero, Conretencionventadet.codcliente, Conretencionventadet.codsucursalcliente, Conretencionventadet.codmovimiento, Conretencionventadet.codsucursal, Conretencionventadet.documento, Conretencionventadet.tipodocumento, Conretencionventadet.montoretenido, Conretencionventadet.mesafecta, Conretencionventadet.anioafecta, Conretencionventadet.retgenventa_id, Concliente.descripcion, Conretencionventa.fechaemision, Conventa.ivaimp1, Conventa.ivaimp2, Conventa.ivaimp3";
			$datos = $this->find('all',array('conditions' => $criteria,'order' => " Conretencionventa.fechaemision ",'fields'=>$campos));
			break;
		}
		return $datos;
	}

	function guardar_seleccionados($datos,$valores=array()){

		$data = array();$i = 0;
		foreach ($datos as $registros){
			$v_chequeo = $registros['conciliar'];
			$data['Conretencionventadet'][$i]['id'] = $registros['id'];
			$data['Conretencionventadet'][$i]['mesafecta'] = $valores['Retgenventa']['mes'];
			$data['Conretencionventadet'][$i]['anioafecta'] = $valores['Retgenventa']['anio'];
			$data['Conretencionventadet'][$i]['retgenventa_id'] = 0;
			if ($v_chequeo>=1) {
				$data['Conretencionventadet'][$i]['retgenventa_id'] = $valores['Retgenventa']['id'];
			}
			$i = $i + 1;
		}
		$mensaje = '<div class="alert alert-danger">Datos de Movimientos de las Retenciones de Ventas NO se Asociaron.</div>';	
		if($this->saveAll($data['Conretencionventadet'])){
			$mensaje =  '<div class="alert alert-success">Datos de Movimientos de las Retenciones de Ventas se Asociaron Correctamente.</div>';
		}
		return $mensaje;
	}

	function guardar_deseleccionados($datos,$valores=array()){

		$data = array();$i = 0;
		foreach ($datos as $registros){
			$v_chequeo = $registros['conciliar'];
			$data['Conretencionventadet'][$i]['id'] = $registros['id'];
			$data['Conretencionventadet'][$i]['mesafecta'] = $valores['Retgenventa']['mes'];
			$data['Conretencionventadet'][$i]['anioafecta'] = $valores['Retgenventa']['anio'];
			if ($v_chequeo>=1) {
				$data['Conretencionventadet'][$i]['retgenventa_id'] = 0;
			}
			$i = $i + 1;
		}
		$mensaje = '<div class="alert alert-danger">Datos de Movimientos de las Retenciones de Ventas NO se Guardaron.</div>';	
		if($this->saveAll($data['Conretencionventadet'])){
			$mensaje =  '<div class="alert alert-success">Datos de Movimientos de las Retenciones de Ventas se Guardaron Correctamente.</div>';
		}
		return $mensaje;
	}

	function getFecha($opcion='primera',$variables=array()){

		switch ($opcion) {
			case 'primera':
				$criteria ="Conretencionventadet.retgenventa_id = 0 and Conretencionventa.estatus = 'C' ";
				$campos = " Conretencionventa.fechaemision "; 
				$nro = $this->find('count',array('conditions' => $criteria,'order' => " Conretencionventa.fechaemision ",'fields'=>$campos));
				if($nro>0){
					$datos = $this->find('first',array('conditions' => $criteria,'order' => " Conretencionventa.fechaemision ",'fields'=>$campos));
				}else{
					$datos['Conretencionventadet']['fechaemision'] = date("Y-m-d");
				}
				
			break;
			case 'primera_asociada':
				$criteria ="Conretencionventa.estatus = 'C'    ";
				if(isset($variables['retgenventa_id'])){
				$criteria = $criteria." and Conretencionventadet.retgenventa_id = ".$variables['retgenventa_id']." ";
				}
				$campos = " Conretencionventa.fechaemision "; 
				$nro = $this->find('count',array('conditions' => $criteria,'order' => " Conretencionventa.fechaemision ",'fields'=>$campos));
				if($nro>0){
					$datos = $this->find('first',array('conditions' => $criteria,'order' => " Conretencionventa.fechaemision ",'fields'=>$campos));
				}else{
					$datos['Conretencionventadet']['fechaemision'] = date("Y-m-d");
				}
				
			break;
		}
		return $datos;
	}

	function getVariables($datos=array()){
		$variables['nombretabla'] = 'retencion'; $variables['nombrediv'] = 'buscar';
		$variables['nombreformulario'] = 'frm';
		if(isset($datos['nombretabla'])){
			$variables['nombretabla'] = $datos['nombretabla'];
			$variables['nombrediv'] = 'buscar2';
		}
		if(isset($datos['nombreformulario'])){
			$variables['nombreformulario'] = $datos['nombreformulario'];
		}
		if(isset($datos['opcion'])){
			$variables['opcion'] = $datos['opcion'];
		}
		return $variables;
	}

	function obtenerNroRetenciones($opcion = 'sin_asociar'){
		$nro = 0;
		switch ($opcion) {
			case 'sin_asociar':
				$nro = $this->find('count',array('conditions'=>' Conretencionventadet.retgenventa_id = 0 ','recursive'=>-1));
			break;
			case 'asociada':
				$nro = $this->find('count',array('conditions'=>' Conretencionventadet.retgenventa_id <> 0 ','recursive'=>-1));
			break;
		}
		return $nro;
	}
			  
}
?>
