<?php
class Retgenventa extends AppModel {

	public $name = 'Retgenventa';

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $hasMany = array(
		'Conretencionventadet' => array(
			'className' => 'Conretencionventadet',
			'foreignKey' => 'retgenventa_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

	function getNro(){
		$cant = $this->find('count',array('order' => " Retgenventa.id desc"));
		if($cant<=0){
			$nro = 1;
		}else{
			$data = $this->find('first',array('order' => " Retgenventa.id desc"));
			$nro = $data['Retgenventa']['nro'] + 1;
		}
		return $nro;	
	}

	function getMesAnio(){
		$cant = $this->find('count',array('conditions' => " Retgenventa.estatus=0 ",'order' => " Retgenventa.id desc"));
		if($cant<=0){
			$data['Retgenventa']['mes'] = date("m");
			$data['Retgenventa']['anio'] = date("Y");
		}else{
			$datos = $this->find('first',array('conditions' => " Retgenventa.estatus=0 ",'order' => " Retgenventa.id desc"));
			if($datos['Retgenventa']['mes']+1<=12){
				$data['Retgenventa']['mes'] = str_pad($datos['Retgenventa']['mes'] + 1,  2, "0");
				$data['Retgenventa']['anio'] = $datos['Retgenventa']['anio'];
			}else{
				$data['Retgenventa']['mes'] = "01";
				$data['Retgenventa']['anio'] = $datos['Retgenventa']['anio'] + 1;
			}
		}
		return $data;
	}
	
	function createDeclaracion($criteria=" Retgenventa.estatus=0 ",$order=" Retgenventa.id desc"){
	// Buscar la ultima declaracion que este creada y activa si no existe crearla
		$nro = $this->find('count',array('conditions' => $criteria,'order' => $order));
		if($nro<=0){
			$data = $this->getMesAnio();
			$datos['Retgenventa']['nro']=$this->getNro();
			$datos['Retgenventa']['mes']=$data['Retgenventa']['mes'];
			$datos['Retgenventa']['anio']=$data['Retgenventa']['anio'];
			$datos['Retgenventa']['estatus']=0;
			$this->save($datos);
		}
	}
	
	function getDeclaracionActiva($criteria=" Retgenventa.estatus=0 ",$order=" Retgenventa.id desc"){
	// Buscar mes y Año Activo
		$this->createDeclaracion();
		$data = $this->find('first',array('conditions' => $criteria,'order' => $order));
        return $data;
	}

	function closeDeclaracionActiva($id=0){
	// Cambiar estatus a 1=> Cerrada
	// Crear la proxima declaracion
		$mensaje = '<div class="alert alert-danger">El Registro de Retenciones de Ventas NO se CERRO.</div>';
		$datos = $this->read(null, $id);
		$datos['Retgenventa']['estatus'] = 1;			
		if($this->save($datos)){
			$mensaje =  '<div class="alert alert-success">El Registro de Retenciones de Ventas se CERRO Correctamente.</div>';
			$this->createDeclaracion();
		}
		return $mensaje;
	}
	
}
?>
