<?php
class Congasto extends AppModel {

	public $name = 'Congasto';
	//public $useDbConfig = 'comerdepa';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		/*'Efectivo' => array(
			'className' => 'Efectivo',
			'foreignKey' => 'efectivo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),*/
		/*'Movbancario' => array(
			'className' => 'Movbancario',
			'foreignKey' => 'movbancario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)*/
	);

	function ajustarDatos($opcion='add',$datos){
		switch ($opcion) {
		case 'add':			
			$datos['Sucgasto']['codsucursal']=$datos['codsucursal'];
			$datos['Sucgasto']['nombre']=$datos['nombre'];
			$datos['Sucgasto']['fecha']=$datos['cuadre_fecha'];
			$datos['Sucgasto']['cuadrediario_id']=$datos['cuadre_id'];			
			$datos['Sucgasto']['monto']=$datos['totalDeposito'];			
			$datos['Sucgasto']['motivo']=$datos['motivo'];			
		break;
		case 'edit':
			$datos['Sucgasto']['codsucursal']=$datos['codsucursal'];
			$datos['Sucgasto']['nombre']=$datos['nombre'];
			$datos['Sucgasto']['fecha']=$datos['cuadre_fecha'];
			$datos['Sucgasto']['cuadrediario_id']=$datos['cuadre_id'];			
			$datos['Sucgasto']['monto']=$datos['totalDeposito'];			
			$datos['Sucgasto']['motivo']=$datos['motivo'];
		break;
		}
		return $datos;
	}

	function obtenerDepositos($fecha=''){
		$sucursal = 0;
		$sql = "select distinct codsucursal from sucdepositos where fecha='".$fecha."'";
		$data = $this->query($sql);
		if (isset($data[0][0]['codsucursal'])){
			$sucursal = $data[0][0]['codsucursal'];
		}
		return $sucursal;
	}

	function guardar_seleccionados($datos){

		$data = array();$i = 0;
		foreach ($datos as $registros){
			$v_chequeo = $registros['conciliar'];
			$data['Congasto'][$i]['id'] = $registros['id'];
			$data['Congasto'][$i]['estatus'] = 'N';
			if ($v_chequeo>=1) {
				$data['Congasto'][$i]['estatus'] = 'C';
			}
			$i = $i + 1;
		}
		$mensaje = '<div class="alert alert-danger">Datos de Movimientos de los Gastos No se Conciliaron.</div>';	
		if($this->saveAll($data['Congasto'])){
			$mensaje =  '<div class="alert alert-success">Datos de Movimientos de los Gastos se Guardaron Correctamente.</div>';
		}
		return $mensaje;
	}

}
?>
