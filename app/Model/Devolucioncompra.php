<?php
class Devolucioncompra extends AppModel
{
    public $name = 'Devolucioncompra';
    public $primaryKey = 'coddevolucioncompra';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'devolucioncompra';
/*
 *
 * */
	public $belongsTo = array('Compra' => array('className' => 'Compra',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codcompra',
						'fields'=> ''
					  )					  					  
			);
			
	function ajustarDatos($opcion='pdf',$datos){
		switch ($opcion) {
		case 'pdf':
			if($datos['tipoventa']==''){
				$datos['tipoventa']=array();
			}
			if($datos['cliente_id']==''){
				$datos['cliente_id']=array();
			}
			if($datos['vendedore_id']==''){
				$datos['vendedore_id']=array();
			}
			$datos['client']=$datos['cliente_id'];$datos['tipoven']=$datos['tipoventa'];
			$datos['vend']=$datos['vendedore_id'];
			$datos['fechadesde']=$datos['desde'];$datos['fechahasta']=$datos['hasta'];
		break;		
		}
		return $datos;
	}

	function reporte($datos=array(),$criteria='',$opcion=0){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$order='Compra.estatus, Compra.codcompra';
			$criteria=" Devolucioncompra.fdevolucion >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Devolucioncompra.fdevolucion <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['provee'],'Compra.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Compra.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>2));
		break;		
		case 1:			
			$order='Venta.estatus desc,Venta.codventa';
			$criteria=" Devolucionventa.fecha >= '".$desde."' ";
			$criteria=$criteria." and Devolucionventa.fecha <= '".$hasta."' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select Devolucionventa.coddevolucion,Venta.codventa ,Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion as Cliente__descripcion, 
Devolucionventa.baseimp2, Devolucionventa.ivaimp2,Devolucionventa.montoexento, Venta.estatus,Devolucionventa.fecha, Vendedore.descripcion as Vendedore__descripcion,
sum(Devolucionventaproducto.cantidad*Ventaproducto.costopromedio) as Costo from devolucionventa as Devolucionventa
inner join ventas Venta on (Devolucionventa.codventa=Venta.codventa)
inner join devolucionventaproductos Devolucionventaproducto on (Devolucionventa.coddevolucion=Devolucionventaproducto.coddevolucion)
inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente)
inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Devolucionventa.codventa and Devolucionventaproducto.codproducto=Ventaproducto.codproducto)
where ".$criteria."
group by Devolucionventa.coddevolucion,Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, 
Devolucionventa.baseimp2, Devolucionventa.ivaimp2,Devolucionventa.montoexento, Venta.estatus,Devolucionventa.fecha, Vendedore.descripcion";
			$data = $this->query($sql);
		break;
		}
		return $data;
	}
				  
}
?>
