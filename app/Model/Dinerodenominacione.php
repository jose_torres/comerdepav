<?php
class Dinerodenominacione extends AppModel
{
    public $name = 'Dinerodenominacione';
    public $useTable = 'dinerodenominaciones';
    public $useDbConfig = 'comerdepa';
/*   	var $validate = array(
      'nombre' => array(
	  	'rule' => 'empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
    var $hasMany = array('Funcione' =>
                         array('className'   => 'Funcione',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'grupo_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )
                  );			  

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$order="Dinerodenominacione.valor ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Dinerodenominacione'];	
			$var[$relleno.$item['id']] = $item['valor'];
		}
		break;			
		}//Cierre de switch						
		return $var;
	}
	
	function denominacion_cupone($id,$nro_caj=1)
	{
	/*	$sql="select Dinerodenominacione.id,Dinerodenominacione.denominacion,Dinerodenominacione.valor as valor 
from dinerodenominaciones as Dinerodenominacione
where fin='CUPONES' 
union
select 0,to_char(Cupone.monto, '999D99') as denominacion,Cupone.monto as valor 
from cupones as Cupone
where Cupone.monto not in (select valor from dinerodenominaciones where fin='CUPONES')
and Cupone.cuadrediario_id=".$id."
order by valor";*/
		$sql="select Dinerodenominacione.id,Dinerodenominacione.denominacion,Dinerodenominacione.valor as valor 
from dinerodenominaciones as Dinerodenominacione
where fin='CUPONES' and Dinerodenominacione.valor in (select monto from cupones where cuadrediario_id=".$id." and cantidad>0 and empresacupon_id=".$nro_caj.")
union
select 0,to_char(Cupone.monto, '999D99') as denominacion,Cupone.monto as valor 
from cupones as Cupone
where Cupone.monto not in (select valor from dinerodenominaciones where fin='CUPONES')
and Cupone.cuadrediario_id=".$id." and Cupone.cantidad>0
order by valor";
//		echo $sql;
		$datos=$this->query($sql);
	    return $datos;
	
	}	
//  Agrega las denominaciones que no estan	
	function agregar_cupones($datos=array()){
		//print_r($datos);
		$datos['Dinerodenominacione']['valor']=$datos['Cupone']['monton'];
		$datos['Dinerodenominacione']['denominacion']=$datos['Cupone']['monton'];
		$datos['Dinerodenominacione']['fin']='CUPONES';
		$reg= $this->find('count',array('conditions'=>" Dinerodenominacione.valor=".$datos['Dinerodenominacione']['valor']." and Dinerodenominacione.fin='CUPONES'"));
			if ($reg<=0){
				if ($this->save($datos))
				{
					//echo 'Guardo';
					
				}
				else
				{
					//echo 'No Guardo';
					
				}
				return 0;
			}else{
				return 1;
			}	
	}		  
}

?>
