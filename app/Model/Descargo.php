<?php
class Descargo extends AppModel
{
    public $name = 'Descargo';
    public $primaryKey = 'coddescargo';
    /**
 * Use database config
 *
 * @var string
 */
    public $useDbConfig = 'comerdepa';
/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'descargos';
/*
 *
 * */
    public $hasMany = array('Descargoproducto' =>
                     array('className'   => 'Descargoproducto',
                           'conditions'  => '',
                           'order'       => '',
                           'limit'       => '',
                           'foreignKey'  => 'coddescargo',
                     ),
              );			

    function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
        switch ($opcion) {
            case 'Cuadrediario':
            $texto = '';
            $condicion = "  Descargo.fecha>='".$datos['Cuadrediario']['fecha']." 00:00:00' and Descargo.fecha<='".$datos['Cuadrediario']['fecha']." 23:59:59' ";
            $valores = $this->find('all',array('order'=>'Descargo.coddescargo','conditions'=>$condicion,'recursive'=>-1));

            foreach ($valores as $reg) {
                $fila = $reg['Descargo'];
                if ($fila['usointerno']==1){
                        $fila['usointerno']='true';
                }else{	
                        $fila['usointerno']='false';
                }
                //echo 'UsoInterno:'.$fila['usointerno'];
                //die();	
                $texto = $texto."INSERT INTO condescargos ( coddescargo, codsucursal, descripcion, fecha, autorizado, responsable, usointerno, hora, estatus) values (";
                $texto = $texto."".$fila['coddescargo'].",".$fila['codsucursal'].",'".$fila['descripcion']."','".$fila['fecha']."','".$fila['autorizado']."','".$fila['responsable']."',".$fila['usointerno'].",'".$fila['hora']."','".$fila['estatus']."');";
                //$texto = $texto.'\n';
            }
            break;				
            case 'Mcuadrediario':
            $texto = '';
            $condicion = "  Descargo.fecha>='".$datos['Mcuadrediario']['fecha']."' and Descargo.fecha<='".$datos['Mcuadrediario']['fecha']."' ";
            $valores = $this->find('all',array('order'=>'Descargo.coddescargo','conditions'=>$condicion,'recursive'=>-1));

            foreach ($valores as $reg) {
                $fila = $reg['Descargo'];

                if ($fila['usointerno']==1){
                        $fila['usointerno']='true';
                }else{	
                        $fila['usointerno']='false';
                }
                //echo 'UsoInterno:'.$fila['usointerno'];
                //die();	
                $texto = $texto."INSERT INTO condescargos ( coddescargo, codsucursal, descripcion, fecha, autorizado, responsable, usointerno, hora, estatus) values (";
                $texto = $texto."".$fila['coddescargo'].",".$fila['codsucursal'].",'".$fila['descripcion']."','".$fila['fecha']."','".$fila['autorizado']."','".$fila['responsable']."',".$fila['usointerno'].",'".$fila['hora']."','".$fila['estatus']."');";
                //$texto = $texto.'\n';
            }	
            break;				
        }	
        return $texto;
    }

    function ajustarDatos($opcion='pdf',$datos){
        switch ($opcion) {
            case 'pdf':
                $datos['fechadesde']=$datos['desde'];
                $datos['fechahasta']=$datos['hasta'];
            break;
        }
        return $datos;
     } 
	
    function listadoDescargo($datos=array(),$criterio=''){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);        
        $criteria=" V.fecha >= '".$desde."' ";
        $criteria=$criteria." and V.fecha <= '".$hasta."' ";			
        $criteria= $criteria.$criterio;
        $sql="select  V.coddescargo,V.codsucursal,V.descripcion, V.fecha,V.hora, V.responsable, V.autorizado, V.estatus, V.usointerno, V.coddepartamento, V.dptonombre, V.dptocod, V.codproducto, V.codigo, V.nombre, V.dptoid, V.unidad, V.cantidad,V.costounitario as costo from
(
select CG.coddescargo,CG.codsucursal,CG.descripcion, CG.fecha,CG.hora, CG.responsable, CG.autorizado, CG.estatus, CG.usointerno,
DP.coddepartamento, DP.descripcion as dptonombre, DP.codigo as dptocod,
PR.codproducto, PR.codigo, PR.nombre, DP.coddepartamento as dptoid, PC.unidad, 
PC.cantidad,PC.costounitario 
from descargos CG
inner join productosdescargos PC on (PC.coddescargo = CG.coddescargo and  PC.codsucursal = CG.codsucursal)
inner join producto PR on (PR.codproducto= PC.codproducto)
inner join departamentos DP on (DP.coddepartamento= PR.coddepartamento)
order by PC.coddescargo, PC.codsucursal
) as V
        where ".$criteria."
        order by V.coddescargo,V.codsucursal,V.codigo";
        $data = $this->query($sql);
        return $data;
    }    
    
    function listadoDescargoResumen($datos=array(),$criterio=''){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);        
        $criteria=" V.fecha >= '".$desde."' ";
        $criteria=$criteria." and V.fecha <= '".$hasta."' ";			
        $criteria= $criteria.$criterio;    
        $sql="select  V.coddescargo,V.codsucursal,V.descripcion, V.fecha::date, V.responsable, V.autorizado, V.estatus, V.usointerno, sum(V.cantidad) as cantidad, sum(V.cantidad * V.costounitario) as total from
(
select CG.coddescargo,CG.codsucursal,CG.descripcion, CG.fecha,CG.hora, CG.responsable, CG.autorizado, CG.estatus, CG.usointerno,
DP.coddepartamento, DP.descripcion as dptonombre, DP.codigo as dptocod,
PR.codproducto, PR.codigo, PR.nombre, DP.coddepartamento as dptoid, PC.unidad, 
PC.cantidad,PC.costounitario 
from descargos CG
inner join productosdescargos PC on (PC.coddescargo = CG.coddescargo and  PC.codsucursal = CG.codsucursal)
inner join producto PR on (PR.codproducto= PC.codproducto)
inner join departamentos DP on (DP.coddepartamento= PR.coddepartamento)
order by PC.coddescargo, PC.codsucursal
) as V
        where ".$criteria."
            group by V.coddescargo,V.codsucursal,V.descripcion, V.fecha::date, V.responsable, V.autorizado, V.estatus, V.usointerno
        order by V.coddescargo,V.codsucursal";
        $data = $this->query($sql);
        return $data;
    }
				  
}
?>
