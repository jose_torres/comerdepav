<?php
class Grupo extends AppModel
{
    var $name = 'Grupo';
    var $useTable = 'grupo';
/*   	var $validate = array(
      'nombre' => array(
	  	'rule' => 'empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
    var $hasMany = array('Funcione' =>
                         array('className'   => 'Funcione',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'grupo_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )
                  );

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Perfiles Padres
		$order="Grupo.nombre ASC ";
		$condiciones = " ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Grupo'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		}		
		return $var;
	}	
				  
}

?>
