<?php
class Ubicacion extends AppModel
{
    public $name = 'Ubicacion';
    //public $useTable = 'grupo';
/*   	var $validate = array(
      'nombre' => array(
	  	'rule' => 'empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
    public $hasMany = array('Elemento' =>
                         array('className'   => 'Elemento',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'ubicacion_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )
                  );

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Perfiles Padres
		$order="Ubicacion.nombre ASC ";
		$condiciones = " ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Ubicacion'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		}		
		return $var;
	}	
				  
}

?>
