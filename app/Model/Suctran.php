<?php
class Suctran extends AppModel {

	public $name = 'Suctran';
	public $useDbConfig = 'comerdepa';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		/*'Efectivo' => array(
			'className' => 'Efectivo',
			'foreignKey' => 'efectivo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),*/
		'Movbancario' => array(
			'className' => 'Movbancario',
			'foreignKey' => 'movbancario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	function ajustarDatos($opcion='add',$datos){
		switch ($opcion) {
		case 'add':			
			list($codventa,$codsucursal,$numerotransferencia,$id_trans)=explode('-',$datos['datospagos']);
			$i = 0;
			$datos['Suctran'][$i]['movbancario_id']=$datos['movbancario_id'];
			$datos['Suctran'][$i]['codsucursal']=$datos['codsucursal'];
			$datos['Suctran'][$i]['fecha']=$datos['cuadre_fecha'];
			$datos['Suctran'][$i]['cuadrediario_id']=$datos['cuadre_id'];
			$datos['Suctran'][$i]['nro']=$datos['nrodocumento'];;
			$datos['Suctran'][$i]['numerotransferencia']=$numerotransferencia;
			$datos['Suctran'][$i]['codventa']=$codventa;
			$datos['Suctran'][$i]['codsucursal']=$codsucursal;			
			$datos['Suctran'][$i]['banco_id']=$datos['banco_id'];
			$datos['Suctran'][$i]['monto']=$datos['totalDeposito'];		
			$datos['Suctran'][$i]['ventaspago_id']=$id_trans;		
			
		break;
		case 'edit':
			
		break;
		}
		return $datos;
	}

	function obtenerDepositos($fecha=''){
		$sucursal = 0;
		$sql = "select distinct codsucursal from sucdepositos where fecha='".$fecha."'";
		$data = $this->query($sql);
		if (isset($data[0][0]['codsucursal'])){
			$sucursal = $data[0][0]['codsucursal'];
		}
		return $sucursal;
	}

	function buscarpagos($condicion=''){
		$sql="select VT.codventa, VT.numerofactura, VT.codsucursal,VT.fecha,VTP.numerotransferencia, VTP.banco,VTP.nrocuenta, BC.nombre, VTP.tipopago, 'PAG' as tipomovimientopago, VTP.monto, VTP.id, 0 as monto_depositado
		from ventas VT 
		inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
		inner join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
		where VTP.tipopago='TRANSFER' and VT.estatus<>'E' and (VT.codventa, VTP.numerotransferencia, VT.codsucursal, VTP.id) not in ( select SCT.codventa, SCT.numerotransferencia, SCT.codsucursal, SCT.ventaspago_id from suctrans as SCT) ".$condicion." ";
		//echo $sql;
		$data = $this->query($sql);
		return $data;
	}

	function llenar_combo($opcion=0,$condicion=''){

		switch ($opcion) {
		case 0://Muestra Todos los Pagos Realizados por Transferencia
		$datos = $this->buscarpagos($condicion);		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['0'];	
			$var[$item['codventa'].'-'.$item['codsucursal'].'-'.$item['numerotransferencia'].'-'.$item['id']] = 'Factura Nro.'.$item['numerofactura'].' Pago:'.$item['numerotransferencia'].' '.$item['nombre'].' '.$item['nrocuenta'].' Monto:'.number_format(sprintf("%01.2f", $item['monto']), 2, ',', '.');
		}
		break;				
		}//Cierre de switch						
		return $var;
	}

}
?>
