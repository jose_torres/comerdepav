<?php
class Dia extends AppModel
{
    public $name = 'Dia';
    public $useTable = 'dias';

  /* Funciones para la vista */ 	
    function llenar_combo($relleno='',$opcion=0){
        switch ($opcion) {
        case 0://Muestra Todos los Bancos registrados
        $order="Banco.nombre ASC";
        $datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
        $var = array();
        foreach ($datos as $row) {
                $item = $row['Banco'];	
                $var[$relleno.$item['codbanco']] = $item['nombre'];
        }
        break;
        case 1://Muestra Todos los Bancos registrados con Cuentas Asociadas
        $order="Banco.nombre ASC";
        $datos = $this->find('all',array('order'=> $order));
        $var = array();
        foreach ($datos as $row) {
                $item = $row['Banco'];
                if(count($row['Cuentasbancaria'])>0){	
                $var[$relleno.$item['codbanco']] = $item['nombre'];
                }
        }
        break;				
        }//Cierre de switch						
        return $var;
    }
/* Fin de Funciones para la vista */  

    public function buscarDiasMes($datos=array()){
        $order="Dia.fecha ASC";
        $criteria=" EXTRACT('month' from Dia.fecha) = '".$datos['mes']."' ";
        $criteria=$criteria." and EXTRACT('year' from Dia.fecha) = '".$datos['anio']."' ";
        $datos = $this->find('all',array('order'=> $order,'conditions'=>$criteria));
        $var = array();
        foreach ($datos as $row){
            $item = $row['Dia'];            
            $var[$item['fecha']] = array('tipo'=>$item['status'],'id'=>$item['id']);
            
        }
        return $var;
    }
    
    public function buscarDiasFeriados($datos=array()){
        $order="Dia.fecha ASC";
        $criteria=" EXTRACT('month' from Dia.fecha) = '".$datos['mes']."' ";
        $criteria=$criteria." and EXTRACT('year' from Dia.fecha) = '".$datos['anio']."' ";
        $criteria=$criteria." and status = 'FERIADO' ";
        $nro = $this->find('count',array('order'=> $order,'conditions'=>$criteria));
        
        return $nro;
    }

    public function guardar_seleccionados($datos=array()){
        //print_r($datos);
        $data = array();$i = 0;
        foreach ($datos as $registros){
            if($registros['id']>0){
                $data['Dia'][$i]['id'] = $registros['id'];
            }
            $data['Dia'][$i]['fecha'] = $registros['fecha'];
            $data['Dia'][$i]['status'] = $registros['tipo'];
            $i = $i + 1;
        }

        $mensaje = '<div class="alert alert-danger">Los Dias No se Guardaron.</div>';	
        if($this->saveAll($data['Dia'])){
            $mensaje =  '<div class="alert alert-success">Los Dias se Guardaron Correctamente.</div>';
        }
        return $mensaje;
    }				  
}
?>
