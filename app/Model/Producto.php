<?php
class Producto extends AppModel
{
    public $name = 'Producto';
    /**
 * Use database config
 *
 * @var string
 */
    public $useDbConfig = 'comerdepa';
    public $primaryKey = 'codproducto';

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'producto';
/*
 * */
    public $hasMany = array('Ventaproducto' =>
                    array('className'   => 'Ventaproducto',
                          'conditions'  => '',
                          'order'       => '',
                          'limit'       => '',
                          'foreignKey'  => 'codproducto',
                          'dependent'   => true,
                          'exclusive'   => false,
                          'finderSql'   => ''
                    ),
             );

    function llenar_combo($relleno='',$opcion=0){
            switch ($opcion) {
            case 0:
            $order="Producto.nombre ASC";
            $datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
            $var = array();
            foreach ($datos as $row) {
                    $item = $row['Producto'];	
                    $var[$relleno.$item['codproducto']] = $item['nombre'];
            }
            break;
            case 1://Muestra Todos los Bancos registrados
            $order="Producto.nombre ASC";
            $datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
            $var = array();
            foreach ($datos as $row) {
                    $item = $row['Producto'];	
                    $var[$relleno.$item['codproducto']] = $item['codigo'].' '.$item['nombre'];
            }
            break;			
            }				
            return $var;
    }

    function ajustarDatos($opcion='pdf',$datos){
        switch ($opcion) {
        case 'pdf':
            
            $datos['producto_id'] = $this->validarArreglo('producto_id',$datos);
            $datos['departamento_id'] = $this->validarArreglo('departamento_id',$datos);
            $datos['deposito_id'] = $this->validarArreglo('deposito_id',$datos);
            $datos['precio'] = $this->validarArreglo('precio',$datos);
            $datos['nodpto_id'] = $this->validarArreglo('nodpto_id',$datos);
            			
           /* if($datos['deposito_id']==''){
                $datos['deposito_id']=array();
            }
            if($datos['precio']==''){
                $datos['precio']=array();
            }
            if(!isset($datos['nodpto_id'])){
                $datos['nodpto_id']=array();
            }else{
                if($datos['nodpto_id']==''){
                    $datos['nodpto_id']=array();
                }
            }*/
            
            $datos['condicion']=$datos['condicion_id'];
            $inicio=array(0);
            $datos['prod'] = array_merge($inicio, $datos['producto_id']);			
            //$datos['prod']=$datos['producto_id'];
            $datos['depto']=array_merge($inicio, $datos['departamento_id']);
            $datos['dep']=array_merge($inicio, $datos['deposito_id']);
            $datos['nodepto']=array_merge($inicio, $datos['nodpto_id']);         
            
            $datos['precio']=array_merge($inicio, $datos['precio']);	
            if(isset($datos['desde'])){
                $datos['fechadesde']=$datos['desde'];
            }
            
            if(isset($datos['hasta'])){
                $datos['fechahasta']=$datos['hasta'];
            }
            ;
        break;		
        }
        return $datos;
    }

    function reporte($datos=array(),$criteria='',$opcion=0){
        //$hasta=$this->anomesdia($datos['fechahasta']);
        //$desde=$this->anomesdia($datos['fechadesde']);
        $data=array();
        switch ($opcion) {
            case 0:			
            $order=' Venta.codventa';
    //	$criteria=" PAGOS_VENTAS.fecha >= '".$desde." 00:00:00' ";
    //	$criteria=$criteria." and PAGOS_VENTAS.fecha <= '".$hasta." 23:59:59' ";	
            $c_prod=$this->construir_or($datos['prod'],'INV.codproducto');
            $criteria=$criteria."  ".$c_prod;
            $c_depto=$this->construir_or($datos['depto'],'INV.coddepartamento');
            $criteria=$criteria."  ".$c_depto;
            //$c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_VENTAS.estatus','letra');
            $c_depto=$this->construir_or($datos['dep'],'INV.coddeposito');	

            //$this->ConversionCondicional();
            $criteria=$criteria."  ".$c_depto;	
            if($datos['condicion']=='' || $datos['condicion']=='Todos'){

            }else{
                $criteria=$criteria." and cantidad".$this->ConversionCondicional($datos['condicion']).$datos['cantidad'];
            }		
            //echo 'Criterio='.$criteria.'<br>';
            $sql="select INV.codproducto,INV.codigo,INV.nombre,INV.preciominimo,INV.preciomayor,INV.preciodetal, INV.costoactual,INV.costopromedio,INV.coddepartamento,INV.DP__Codigo as DP__Codigo, INV.descripcion,INV.coddeposito,
            INV.DEP__descripcion as DEP__descripcion,INV.unidad,
            INV.Utilidad_min_act as Utilidad__minimoact,INV.Utilidad_min_act as Utilidad__minimopro,
            INV.Utilidad_may_act as Utilidad__mayoract,INV.Utilidad_may_act as Utilidad__mayorpro,
            INV.Utilidad_det_act as Utilidad__detalact,INV.Utilidad_det_act as Utilidad__detalpro,
            round( CAST(INV.cantidad as numeric), 2) as cantidad
            from (
            select PR.codproducto,PR.codigo,PR.nombre,DP.coddepartamento,DP.codigo as DP__Codigo, DP.descripcion,DEP.descripcion as DEP__descripcion,DEP.coddeposito,
            PR.preciominimo,PR.preciomayor,PR.preciodetal,PR.costoactual,PR.costopromedio,
            PR.unidadprincipal as unidad, 
            CASE WHEN PR.preciominimo>0 THEN ((PR.preciominimo-PR.costoactual)/PR.preciominimo*100) ELSE 0 END as Utilidad_min_act,
            CASE WHEN PR.preciominimo>0 THEN ((PR.preciominimo-PR.costopromedio)/PR.preciominimo*100) ELSE 0 END as Utilidad_min_prom,
            CASE WHEN PR.preciomayor>0 THEN ((PR.preciomayor-PR.costoactual)/PR.preciomayor*100) ELSE 0 END as Utilidad_may_act,
            CASE WHEN PR.preciomayor>0 THEN ((PR.preciomayor-PR.costopromedio)/PR.preciomayor*100) ELSE 0 END as Utilidad_may_prom,
            CASE WHEN PR.preciodetal>0 THEN ((PR.preciodetal-PR.costoactual)/PR.preciodetal*100) ELSE 0 END as Utilidad_det_act,
            CASE WHEN PR.preciodetal>0 THEN ((PR.preciodetal-PR.costopromedio)/PR.preciodetal*100) ELSE 0 END as Utilidad_det_prom,
            sum(CASE WHEN HP.unidad=PR.unidadprincipal THEN round( CAST(HP.cantidad as numeric), 2) ELSE round( CAST(HP.cantidad as numeric), 2)/PR.factorconversion End) as cantidad 
            from v_historiadelproducto HP 
            inner join producto PR on (PR.codproducto=HP.codproducto) inner join departamentos DP on (PR.coddepartamento=DP.coddepartamento) 
            inner join depositos DEP on (HP.deposito=DEP.coddeposito) 
            group by PR.codproducto,PR.codigo,PR.nombre,DP.coddepartamento,DP.codigo,DP.descripcion,DEP.coddeposito,PR.preciominimo,PR.preciomayor,PR.preciodetal,PR.costoactual,PR.costopromedio, DEP.descripcion,PR.unidadprincipal 
            order by PR.codigo
            ) INV
            where 1=1 ".$criteria."
            ";
            //echo $sql;
            $data = $this->query($sql);
        break;
        case 1:			
            $order=' Venta.codventa';
            $c_prod=$this->construir_or($datos['prod'],'INV.codproducto');
            $criteria=$criteria."  ".$c_prod;
            $c_depto=$this->construir_or($datos['depto'],'INV.coddepartamento');
            $criteria=$criteria."  ".$c_depto;

            if($datos['condicion']=='' || $datos['condicion']=='Todos'){				
            }else{
                    $criteria=$criteria." and cantidad".$this->ConversionCondicional($datos['condicion']).$datos['cantidad'];
            }
            if(count($datos['precio'])<=1){				
            }else{
                    $cont=0;
                    foreach($datos['precio'] as $reg){
                    if($cont>=1){	
                    if($reg=='preciominimo'){
                            $camp="Utilidad_min_act";
                            if($datos['costo'] == 'costopromedio') $camp="Utilidad_min_prom";
                            $criteria=$criteria." and ".$camp.$this->ConversionCondicional($datos['utilidad_id']).$datos['utilcantidad'];
                    }elseif($reg=='preciomayor'){
                            $camp="Utilidad_may_act";
                            if($datos['costo'] == 'costopromedio') $camp="Utilidad_may_prom";
                            $criteria=$criteria." and ".$camp.$this->ConversionCondicional($datos['utilidad_id']).$datos['utilcantidad'];
                    }elseif($reg=='preciodetal'){
                            $camp="Utilidad_det_act";
                            if($datos['costo'] == 'costopromedio') $camp="Utilidad_det_prom";
                            $criteria=$criteria." and ".$camp.$this->ConversionCondicional($datos['utilidad_id']).$datos['utilcantidad'];
                    }
                    }
                    $cont=$cont + 1;
            }

            }
            //$campos=$this->construir_campo($datos['precio'],'PR');
            //echo 'Criterio='.$criteria.'<br>';
            $sql="
            select INV.codproducto,INV.codigo,INV.nombre,INV.coddepartamento,INV.DP__Codigo as DP__Codigo, INV.descripcion, INV.preciominimo, INV.preciomayor, INV.preciodetal, INV.costoactual, INV.costopromedio, INV.unidad,INV.Utilidad_min_act, INV.Utilidad_min_prom, INV.Utilidad_may_act,INV.Utilidad_may_prom, INV.Utilidad_det_act, INV.Utilidad_det_prom
            from(
            select PR.codproducto,PR.codigo,PR.nombre,DP.coddepartamento,DP.codigo as DP__Codigo, DP.descripcion,
            PR.preciominimo,PR.preciomayor,PR.preciodetal,PR.costoactual,PR.costopromedio,
            PR.unidadprincipal as unidad, 
            CASE WHEN PR.preciominimo>0 THEN ((PR.preciominimo-PR.costoactual)/PR.preciominimo*100) ELSE 0 END as Utilidad_min_act,
            CASE WHEN PR.preciominimo>0 THEN ((PR.preciominimo-PR.costopromedio)/PR.preciominimo*100) ELSE 0 END as Utilidad_min_prom,
            CASE WHEN PR.preciomayor>0 THEN ((PR.preciomayor-PR.costoactual)/PR.preciomayor*100) ELSE 0 END as Utilidad_may_act,
            CASE WHEN PR.preciomayor>0 THEN ((PR.preciomayor-PR.costopromedio)/PR.preciomayor*100) ELSE 0 END as Utilidad_may_prom,
            CASE WHEN PR.preciodetal>0 THEN ((PR.preciodetal-PR.costoactual)/PR.preciodetal*100) ELSE 0 END as Utilidad_det_act,
            CASE WHEN PR.preciodetal>0 THEN ((PR.preciodetal-PR.costopromedio)/PR.preciodetal*100) ELSE 0 END as Utilidad_det_prom,
            sum(CASE WHEN HP.unidad=PR.unidadprincipal THEN HP.cantidad ELSE HP.cantidad/PR.factorconversion End ) as cantidad 
            from v_historiadelproducto HP 
            inner join producto PR on (PR.codproducto=HP.codproducto) inner join departamentos DP on (PR.coddepartamento=DP.coddepartamento)
            group by PR.codproducto,PR.codigo,PR.nombre,DP.coddepartamento,DP.codigo,DP.descripcion,PR.preciominimo,PR.preciomayor,PR.preciodetal,PR.costoactual,PR.costopromedio,PR.unidadprincipal
            order by PR.codigo
            ) INV
            where 1=1 ".$criteria."
            order by INV.DP__Codigo ";
            //echo $sql;
            $data = $this->query($sql);
        break;
        case 2:			
            $order=' Venta.codventa';
            $c_prod=$this->construir_or($datos['prod'],'PR.codproducto');
            $criteria=$criteria."  ".$c_prod;
            $c_depto=$this->construir_or($datos['depto'],'DP.coddepartamento');
            $criteria=$criteria."  ".$c_depto;

            $sql="
            select PR.codproducto,PR.codigo,PR.nombre,DP.coddepartamento,DP.codigo as Codigodp, DP.descripcion,
            PR.preciominimo,PR.preciomayor,PR.preciodetal,PR.costoactual,PR.costopromedio,
            PR.unidadprincipal as unidad 
            from producto PR 
            inner join departamentos DP on (PR.coddepartamento=DP.coddepartamento)
            where 1=1 ".$criteria."
            group by PR.codproducto,PR.codigo,PR.nombre,DP.coddepartamento,DP.codigo,DP.descripcion,PR.preciominimo,PR.preciomayor,PR.preciodetal,PR.costoactual,PR.costopromedio,PR.unidadprincipal
            order by PR.codigo
                    ";
            //echo $sql;
            $data = $this->query($sql);
        break;
        case 3:			
            $order=' Producto.codigo';
            $criteria=' 1=1 ';
            $c_prod=$this->construir_or($datos['prod'],'Producto.codproducto');
            $criteria=$criteria."  ".$c_prod;
            $c_depto=$this->construir_or($datos['depto'],'Producto.coddepartamento');
            $criteria=$criteria."  ".$c_depto;

            $data = $this->find('all',array('conditions'=> $criteria,'order'=> $order));
        break;
    
        case 4:
            $c_prod=$this->construir_or($datos['prod'],'U.codproducto');
            $criteria=$criteria."  ".$c_prod;
            $c_depto=$this->construir_or($datos['depto'],'U.coddepartamento');
            $criteria=$criteria."  ".$c_depto;
            if($datos['condicion']=='' || $datos['condicion']=='Todos'){				
            }else{
                $criteria=$criteria." and cantidad".$this->ConversionCondicional($datos['condicion']).$datos['cantidad'];
            }
            
            $sql=" select codproducto,codigo,nombre,coddepartamento,unidad, costoactual,costopromedio,preciominimo,preciomayor,preciodetal,dp__codigo,descripcion, cant_principal, cant_suc1,cant_suc2,cant_suc3,cantidad from ( 
                select codproducto,V.codigo,nombre,V.coddepartamento,unidad, costoactual,costopromedio,preciominimo,preciomayor,preciodetal,DP.codigo as dp__codigo, DP.descripcion,
sum(CASE WHEN V.Suc='Pr' THEN V.cantidad ELSE 0 END) as cant_principal,
sum(CASE WHEN V.Suc='Suc1' THEN V.cantidad ELSE 0 END) as cant_suc1, 
sum(CASE WHEN V.Suc='Suc2' THEN V.cantidad ELSE 0 END) as cant_suc2, 
sum(CASE WHEN V.Suc='Suc3' THEN V.cantidad ELSE 0 END) as cant_suc3, 
sum(cantidad) as Cantidad from
(
select 'Pr' as Suc, codproducto,codigo,nombre,coddepartamento,unidad,
costoactual,costopromedio,
preciominimo,preciomayor,preciodetal,cantidad 
from inventario_actual
union all
select 'Suc1' as Suc,codproducto,codigo,nombre,coddepartamento,unidad,costoactual,costopromedio,preciominimo,preciomayor,preciodetal,cantidad 
from dblink('dbname=BDcdpVF2_I host=192.168.1.8 user=postgres password=123456 port=5432',
'select codproducto,codigo,nombre,coddepartamento,unidad,costoactual,costopromedio,preciominimo,preciomayor,preciodetal,cantidad 
from inventario_actual') as Suc1(codproducto int,codigo text,nombre text,coddepartamento int,
unidad text,costoactual numeric,costopromedio numeric,preciominimo numeric,preciomayor numeric,
preciodetal numeric,cantidad numeric)
union all
select 'Suc2' as Suc,codproducto,codigo,nombre,coddepartamento,unidad,costoactual,costopromedio,preciominimo,preciomayor,preciodetal,cantidad 
from dblink('dbname=BDcdpVF2_II host=192.168.1.8 user=postgres password=123456 port=5432',
'select codproducto,codigo,nombre,coddepartamento,unidad,costoactual,costopromedio,preciominimo,preciomayor,preciodetal,cantidad 
from inventario_actual') as Suc1(codproducto int,codigo text,nombre text,coddepartamento int,
unidad text,costoactual numeric,costopromedio numeric,preciominimo numeric,preciomayor numeric,
preciodetal numeric,cantidad numeric)
union all
select 'Suc3' as Suc,codproducto,codigo,nombre,coddepartamento,unidad,costoactual,costopromedio,preciominimo,preciomayor,preciodetal,cantidad 
from dblink('dbname=BDcdpVF2_III host=192.168.1.8 user=postgres password=123456 port=5432',
'select codproducto,codigo,nombre,coddepartamento,unidad,costoactual,costopromedio,preciominimo,preciomayor,preciodetal,cantidad 
from inventario_actual') as Suc1(codproducto int,codigo text,nombre text,coddepartamento int,
unidad text,costoactual numeric,costopromedio numeric,preciominimo numeric,preciomayor numeric,
preciodetal numeric,cantidad numeric)
) V
    inner join departamentos DP on (DP.coddepartamento = V.coddepartamento)
            group by codproducto,V.codigo,nombre,V.coddepartamento,unidad,
costoactual,costopromedio,
preciominimo,preciomayor,preciodetal,DP.codigo,DP.descripcion
order by DP.codigo,codproducto
) U
         where 1=1 ".$criteria."
                    ";
            //echo $sql;
            $data = $this->query($sql);
        break;
        }
        return $data;
    }
    
    public function activar(){
        $mensaje = '<div class="alert alert-danger">No se pudo Activar los Productos.</div>';
         if($this->updateAll( array('estado' => "'ACTIVO'"),array('estado' => 'INACTIVO')))
         {
             $mensaje = '<div class="alert alert-success">Se Activaron todos los Productos.</div>';
         }        
        return $mensaje;
    }
    
    function nroproductosnvos($sucursal=array()){

            $sql=" select count(codproducto) as nro_reg from producto where (codproducto,codigo) not in  (select producto.codproducto, producto.codigo from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select producto.codproducto, producto.codigo from producto order by producto.codproducto') as t1(codproducto bigint,codigo text)  )";
            //echo $sql;
            $data=$this->query($sql);
            $nro = $data[0][0]['nro_reg'];
            $mensaje = '<div class="alert alert-success">Se consiguieron '.$nro.' Productos Nuevos</div>';
            return array($nro, $mensaje);
    }
    
    function nroproductoscambiados($sucursal=array()){

            $sql=" select count(PR.codproducto) as nro_reg from producto as PR
inner join (select t1.codproducto, t1.codigo,t1.nombre, t1.coddepartamento, t1.costoactual, t1.costopromedio, t1.costoanterior, t1.preciominimo, t1.preciomayor, t1.preciodetal from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select producto.codproducto, producto.codigo, producto.nombre,producto.coddepartamento,costoactual, costopromedio, costoanterior, preciominimo, preciomayor, preciodetal from producto order by producto.codproducto') 
as t1(codproducto bigint,codigo text,nombre text,coddepartamento bigint,costoactual real, costopromedio real, 
costoanterior real, preciominimo real, preciomayor real, preciodetal real) ) as sucpro
on (sucpro.codproducto=PR.codproducto and (PR.nombre<>sucpro.nombre or sucpro.preciominimo::numeric<>PR.preciominimo::numeric
or sucpro.preciomayor::numeric<>PR.preciomayor::numeric or sucpro.preciodetal::numeric<>PR.preciodetal::numeric 
or sucpro.costoactual::numeric<>PR.costoactual::numeric
or sucpro.costopromedio::numeric<>PR.costopromedio::numeric or sucpro.coddepartamento<>PR.coddepartamento
)
)";
            //echo $sql;
            $data=$this->query($sql);
            $nro = $data[0][0]['nro_reg'];
            $mensaje = '<div class="alert alert-success">Se consiguieron '.$nro.' Productos con Cambios</div>';
            return array($nro, $mensaje);
    }
    
    function productosnvos($sucursal=array()){

            $sql=" select codproducto, codigo, nombre, coddepartamento, codmarca, modelo, 
       referencia, codbarra, codiva, estado, stockminimo, stockmaximo, 
       foto, unidadprincipal, unidadalterna, factorconversion, costoactual, 
       costopromedio, costoanterior, preciominimo, preciomayor, preciodetal, 
       numero, fecha, preciominimo1, preciomayor1, 
       preciodetal1, minimoventap, minimoventaa from producto where (codproducto,codigo) not in  (select producto.codproducto, producto.codigo from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select producto.codproducto, producto.codigo from producto order by producto.codproducto') as t1(codproducto bigint,codigo text)  )";
            echo $sql;
            $data=$this->query($sql);
            return $data;
    }
    
    function productoscambiados($sucursal=array()){

            $sql=" select PR.codproducto, PR.codigo, PR.nombre, PR.coddepartamento, codmarca, modelo, referencia, codbarra, codiva, estado, 
stockminimo, stockmaximo, foto, unidadprincipal, unidadalterna, factorconversion, PR.costoactual, PR.costopromedio, 
PR.costoanterior, PR.preciominimo, PR.preciomayor, PR.preciodetal, numero, fecha, preciominimo1, preciomayor1, preciodetal1, 
minimoventap, minimoventaa from producto as PR
inner join 
(select t1.codproducto, t1.codigo,t1.nombre, t1.coddepartamento, t1.costoactual, t1.costopromedio, 
t1.costoanterior, t1.preciominimo, t1.preciomayor, t1.preciodetal from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select producto.codproducto, producto.codigo, producto.nombre,producto.coddepartamento,costoactual, costopromedio, 
costoanterior, preciominimo, preciomayor, preciodetal from producto order by producto.codproducto') 
as t1(codproducto bigint,codigo text,nombre text,coddepartamento bigint,costoactual real, costopromedio real, 
costoanterior real, preciominimo real, preciomayor real, preciodetal real) ) as sucpro
on (sucpro.codproducto=PR.codproducto and (PR.nombre<>sucpro.nombre or sucpro.preciominimo::numeric<>PR.preciominimo::numeric or sucpro.preciomayor::numeric<>PR.preciomayor::numeric or sucpro.preciodetal::numeric<>PR.preciodetal::numeric or sucpro.costoactual::numeric<>PR.costoactual::numeric or sucpro.costopromedio::numeric<>PR.costopromedio::numeric or sucpro.coddepartamento<>PR.coddepartamento
)
)";
            //echo $sql;
            $data=$this->query($sql);
            return $data;
    }
    
    function transferirproductos($sucursal=array()){
        $data = array(); $reg = 0;
        list($reg,$mensaje) = $this->nroproductosnvos($sucursal);
        $mensaje = '<div class="alert alert-info">No Se Registraron Productos Nuevos</div>';
        if( $reg > 0){
            $datos = $this->productosnvos($sucursal);
            $sql = "select  dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', '";
            foreach ($datos as $row) {
                $sql=$sql."INSERT INTO producto(codproducto, codigo, nombre, coddepartamento, codmarca, modelo, 
       referencia, codbarra, codiva, estado, stockminimo, stockmaximo, 
       foto, unidadprincipal, unidadalterna, factorconversion, costoactual, 
       costopromedio, costoanterior, preciominimo, preciomayor, preciodetal,  preciominimo1, preciomayor1, preciodetal1, minimoventap, minimoventaa) VALUES (".$row[0]['codproducto'].", ''".$row[0]['codigo']."'',''".$row[0]['nombre']."'',".$row[0]['coddepartamento'].",".$row[0]['codmarca']." , ''".$row[0]['modelo']."'',''".$row[0]['referencia']."'' 
       , ''".$row[0]['codbarra']."'',".$row[0]['codiva'].", ''".$row[0]['estado']."'',".$row[0]['stockminimo']." , ".$row[0]['stockmaximo'].", ''".$row[0]['foto']."'' 
       , ''".$row[0]['unidadprincipal']."'' ,''".$row[0]['unidadalterna']."'' , ".$row[0]['factorconversion']." , ".$row[0]['costoactual'].",".$row[0]['costopromedio']." 
       ,".$row[0]['costoanterior']." ,".$row[0]['preciominimo']." ,".$row[0]['preciomayor']." , ".$row[0]['preciodetal'].",".$row[0]['preciominimo1']."  ,".$row[0]['preciomayor1']." , ".$row[0]['preciodetal1'].",".$row[0]['minimoventap']." ,".$row[0]['minimoventaa']." );";
            }
           $sql = $sql."')";
         $mensaje = '<div class="alert alert-success"> Se Registraron '.$reg.' Productos Nuevos</div>';   
        $data=$this->query($sql); 
        }
        return $mensaje;
    }
    
    function actualizarproductos($sucursal=array()){
        $data = array(); $reg = 0;
        list($reg,$mensaje) = $this->nroproductoscambiados($sucursal);
        $mensaje = '<div class="alert alert-info">No Se Registraron Cambios en Productos</div>';
        if( $reg > 0){
        //$datos = $this->find('all',array('recursive'=>-1)); 
        $datos = $this->productoscambiados($sucursal);
       
        $sql = "select  dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."','";
        $cant = 0;
        foreach ($datos as $row) {
        $sql=$sql." UPDATE  producto set codigo=''".$row[0]['codigo']."'',nombre=''".$row[0]['nombre']."'',coddepartamento=".$row[0]['coddepartamento'].",codmarca=".$row[0]['codmarca']." , modelo=''".$row[0]['modelo']."'',referencia=''".$row[0]['referencia']."'' , codbarra=''".$row[0]['codbarra']."'',codiva=".$row[0]['codiva'].", estado=''".$row[0]['estado']."'',stockminimo=".$row[0]['stockminimo']." , stockmaximo=".$row[0]['stockmaximo'].", foto=''".$row[0]['foto']."'', unidadprincipal=''".$row[0]['unidadprincipal']."'' ,unidadalterna=''".$row[0]['unidadalterna']."'' , factorconversion=".$row[0]['factorconversion']." ,costoactual=".$row[0]['costoactual'].",costopromedio=".$row[0]['costopromedio']." ,costoanterior=".$row[0]['costoanterior']." ,preciominimo=".$row[0]['preciominimo']." ,preciomayor=".$row[0]['preciomayor']." , preciodetal=".$row[0]['preciodetal'].",preciominimo1=".$row[0]['preciominimo1']."  ,preciomayor1=".$row[0]['preciomayor1']." , preciodetal1=".$row[0]['preciodetal1'].",minimoventap=".$row[0]['minimoventap']." ,minimoventaa=".$row[0]['minimoventaa']." where codproducto=".$row[0]['codproducto']."; ";
            $cant = $cant + 1;
        }
        $sql = $sql."')";
        $data=$this->query($sql);
        $mensaje = '<div class="alert alert-success">Se Actualizaron los datos de '.$cant.'  Productos</div>';
        }
        return $mensaje;
    }
    
    function actualizarSaintSiscom(){
	
		$sql = " UPDATE producto
		   SET  nombre=archivos.descripcio, coddepartamento=archivos.departamen::integer, 
			   costoactual=archivos.costoactu, costopromedio=archivos.costoprom, 
			   preciominimo=(archivos.precio1/1.16), preciomayor=(archivos.precio2/1.16), preciodetal=(archivos.precio3/1.16),  
				preciominimo1=archivos.precio1, preciomayor1=archivos.precio2, preciodetal1=archivos.precio3,
        unidadprincipal = CASE
		  WHEN (archivos.unidad='PAQ') THEN 'Paquete'
		  WHEN (archivos.unidad='KGS') THEN 'Kilo'
		  WHEN (archivos.unidad='MTS') THEN 'Metro'
		  WHEN (archivos.unidad='ROL' or archivos.unidad='BOB') THEN 'Rollo'
		  WHEN (archivos.unidad='GAL') THEN 'Galón'
		  WHEN (archivos.unidad='BID') THEN 'Bidón' 
		  WHEN (archivos.unidad='BTO') THEN 'Bulto'  
		  WHEN (archivos.unidad='TBR') THEN 'Tambor'  
		  ELSE 'Unidad'
		 END
		FROM 
		  archivos  
		WHERE 
		  trim(producto.codigo) = trim(archivos.codigo)
		  and archivos.archivo = 'STOCK21'";
		  $data=$this->query($sql);
		  return $data;
	}
	
	function incluirProductosSiscom(){
		
		$sql = "INSERT INTO producto(
            codigo, nombre, coddepartamento, codiva, codmarca, factorconversion, estado, 
            unidadprincipal, unidadalterna, costoactual, stockminimo, stockmaximo,
            costopromedio, costoanterior, preciominimo, preciomayor, preciodetal, 
             preciominimo1, preciomayor1, preciodetal1)
select codigo,descripcio,departamen::integer, 1 as codiva, 0 as codmarca, 1 as factorconversion, 'ACTIVO' as estado,CASE
  WHEN (archivos.unidad='PAQ') THEN 'Paquete'
  WHEN (archivos.unidad='KGS') THEN 'Kilo'
  WHEN (archivos.unidad='MTS') THEN 'Metro'
  WHEN (archivos.unidad='ROL' or archivos.unidad='BOB') THEN 'Rollo'
  WHEN (archivos.unidad='GAL') THEN 'Galón'
  WHEN (archivos.unidad='BID') THEN 'Bidón' 
  WHEN (archivos.unidad='BTO') THEN 'Bulto'  
  WHEN (archivos.unidad='TBR') THEN 'Tambor'  
  ELSE 'Unidad'
 END as unidad, CASE
  WHEN (archivos.unidad='PAQ') THEN 'Paquete'
  WHEN (archivos.unidad='KGS') THEN 'Kilo'
  WHEN (archivos.unidad='MTS') THEN 'Metro'
  WHEN (archivos.unidad='ROL' or archivos.unidad='BOB') THEN 'Rollo'
  WHEN (archivos.unidad='GAL') THEN 'Galón'
  WHEN (archivos.unidad='BID') THEN 'Bidón' 
  WHEN (archivos.unidad='BTO') THEN 'Bulto'  
  WHEN (archivos.unidad='TBR') THEN 'Tambor'  
  ELSE 'Unidad'
 END  as alterna, 0 as stockminimo, 0 as stockminimo, costoactu, costoprom,
0 as costoanterior, precio1/1.16,precio2/1.16,precio3/1.16, precio1, precio2, precio3 from archivos where
archivo = 'STOCK21' and codigo not in (select codigo from producto)";
		
		$data=$this->query($sql);
		return $data;
	
	}
        
    function historialProducto($datos=array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" HPR.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and HPR.fecha <= '".$hasta." 23:59:59' ";
        
        if(isset($datos['prod'])){
            $c_prod=$this->construir_or($datos['prod'],'HPR.codproducto');        
            $criteria=$criteria."  ".$c_prod;
        }
        
        $sql = "select PR.codproducto, PR.codigo, PR.nombre, DP.codigo as dep_codigo, DP.descripcion as dep_nombre, HPR.tipo, HPR.numero, HPR.fecha, HPR.descripcion,
 HPR.cantidad, HPR.unidad, HPR.costo * HPR.cantidad as costo_total 
from v_historiadelproducto HPR
inner join producto PR on ( HPR.codproducto = PR.codproducto )
inner join departamentos DP on ( DP.coddepartamento = PR.coddepartamento )
where ".$criteria."
order by HPR.fecha::date,HPR.tipo, HPR.numero";
        
        $data=$this->query($sql);
        return $data;
    }    

}
?>
