<?php
class Deposito extends AppModel
{
    public $name = 'Deposito';
    public $primaryKey = 'coddeposito';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'depositos';
	

	function actualizar($datos=array()){

		$condicion = " Cpabono.codcpabono = '".$datos['Cpabonopago']['codcpabono']."' ";
		$sql="update Cpabono set concepto='PAGO DE RETENCION NRO ".$datos['Retencioncompra']['numero']."' where ".$condicion;
		$data = $this->query($sql);
	}	
	
	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Depositos registrados
		$order="Deposito.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Deposito'];	
			$var[$relleno.$item['coddeposito']] = $item['descripcion'];
		}
		break;			
		}//Cierre de switch						
		return $var;
	}						  
}
?>
