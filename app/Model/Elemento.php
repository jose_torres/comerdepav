<?php
class Elemento extends AppModel
{
    public $name = 'Elemento';    

    public $belongsTo = array('Ubicacion' => array('className' => 'Ubicacion',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'ubicacion_id'
					  )
			);
	
	public function llenar_combo($relleno='',$opcion='Todos'){
		switch ($opcion) {
		case 'Todos'://Muestra Todos los Perfiles Padres
		$order="Elemento.nombre ASC ";
		$condiciones = " ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Elemento'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		case 'Encabezado':
		$order="Elemento.nombre ASC ";
		$condiciones = " Elemento.ubicacion_id=1 ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Elemento'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		case 'Pie_Pagina'://Muestra Todos los Perfiles Padres
		$order="Elemento.nombre ASC ";
		$condiciones = " Elemento.ubicacion_id=2 ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Elemento'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		}		
		return $var;
	}	
	
	public function guardartemp($post=array(),$data_enc=array()){
		$mensaje='Seleccione un Elemento de Cabecera';
		if ($_POST['elemento_id']){
			$criterio="Elemento.id=".$_POST['elemento_id'];
			$datos_elemento = $this->find('first',array('conditions'=>$criterio));
			$data_enc['Marco'][$_POST['elemento_id']]=array('elemento_id'=>$_POST['elemento_id'],'plantilla_id'=>$_POST['valorControl'],'elemento_nombre'=>$datos_elemento['Elemento']['nombre']);				
			$mensaje='Se Guardo el detalle';
		}
		if (!isset($data_enc['Marco']) ){ $data_enc['Marco']=array(); }
		return array($data_enc,$mensaje);
	}
	
	public function eliminartemp($post='0',$data_enc=array()){
		$mensaje='Se elimino el detalle';		
		unset($data_enc['Marco'][$post]);
		if (!isset($data_enc['Marco']) ){ $data_enc['Marco']=array(); }		
		return array($data_enc,$mensaje);
	}	
			
							  
}

?>
