<?php
class Retencionventacab extends AppModel
{
    public $name = 'Retencionventacab';
    public $primaryKey = 'codretencion';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'retenciones_ventas';
/*
 *
 * */
	public $hasMany = array('Retencionventa' =>
                         array('className'   => 'Retencionventa',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'codretencion',
                         ),                       
                  );


	function buscarResumenDia($datos=array()){
		$hasta=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$desde=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";	
		$sql =  " select RV.fechaemision,sum(RVD.montoretenido) as Total from retenciones_ventas RV inner join retenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion)
		where ".$criteria." group by RV.fechaemision";
		$data = $this->query($sql);
		if(!isset($data[0][0]['total'])){
			$data[0][0]['total']=0;
		}
        return $data[0][0];
	}

	function buscarRegistrosDia($datos=array()){
		$hasta=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$desde=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";	
		$sql =  " select RV.fechaemision,CL.rif,CL.descripcion,RVD.* from retenciones_ventas RV inner join retenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion) inner join clientes CL on (CL.codcliente  = RVD.codcliente and CL.codsucursal = RVD.codsucursalcliente)
		where ".$criteria." order by RVD.codretencion";
		$data = $this->query($sql);
		
        return $data;
	}

	function eliminarDetalle($codretencion='',$codsucursal=''){
		$sql =  " DELETE FROM retenciones_ventas_detalles where codretencion=".$codretencion." and codsucursal=".$codsucursal." ";
		$data = $this->query($sql);
	}

	function eliminarCabecera($codretencion=''){
		$sql =  " DELETE FROM retenciones_ventas where codretencion=".$codretencion." ";
		$data = $this->query($sql);
	}

	function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
		switch ($opcion) {
			case 'Cuadrediario':
			$texto = '';
			$condicion = "  Retencionventacab.fechaemision='".$datos['Cuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Retencionventacab.codretencion','conditions'=>$condicion,'recursive'=>-1));
			
			foreach ($valores as $reg) {
				$fila = $reg['Retencionventacab'];
				$texto = $texto."INSERT INTO conretenciones_ventas( codretencion, codsucursal, numero, fechaemision, horaemision, montototal, estatus) values (";
				$texto = $texto."".$fila['codretencion'].",".$datos['Cuadrediario']['codsucursal'].",'".$fila['numero']."','".$fila['fechaemision']."','".$fila['horaemision']."',".$fila['montototal'].",'".$fila['estatus']."');";
				//$texto = $texto.'\n';
			}	
			break;				
			case 'Mcuadrediario':
			$texto = '';
			$condicion = "  Retencionventacab.fechaemision='".$datos['Mcuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Retencionventacab.codretencion','conditions'=>$condicion,'recursive'=>-1));
			
			foreach ($valores as $reg) {
				$fila = $reg['Retencionventacab'];
				$texto = $texto."INSERT INTO conretenciones_ventas( codretencion, codsucursal, numero, fechaemision, horaemision, montototal, estatus) values (";
				$texto = $texto."".$fila['codretencion'].",".$datos['Mcuadrediario']['codsucursal'].",'".$fila['numero']."','".$fila['fechaemision']."','".$fila['horaemision']."',".$fila['montototal'].",'".$fila['estatus']."');";
				//$texto = $texto.'\n';
			}	
			break;				
		}	
		return $texto;
	}
				  
}
?>
