<?php
class Condescargo extends AppModel
{
    public $name = 'Condescargo';
    public $primaryKey = 'id';
    /**
 * Use database config
 *
 * @var string
 */
    //public $useDbConfig = 'comerdepa';
/**
* Use table
*
* @var mixed False or table name
*/
    public $useTable = 'condescargos';
/*
*
* */
    public $hasMany = array('Condescargoproducto' =>
                     array('className'   => 'Condescargoproducto',
                           'conditions'  => '',
                           'order'       => '',
                           'limit'       => '',
                           'foreignKey'  => 'coddescargo',
                     ),
              );			

    function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
        switch ($opcion) {
                case 'Cuadrediario':
                $texto = '';
                $condicion = "  Cargo.fecha>='".$datos['Cuadrediario']['fecha']." 00:00:00' and Cargo.fecha<='".$datos['Cuadrediario']['fecha']." 23:59:59' ";
                $valores = $this->find('all',array('order'=>'Cargo.codcargo','conditions'=>$condicion,'recursive'=>-1));

                foreach ($valores as $reg) {
                        $fila = $reg['Cargo'];

                        $texto = $texto."INSERT INTO concargos ( codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus) values (";
                        $texto = $texto."".$fila['codcargo'].",".$fila['codsucursal'].",'".$fila['descripcion']."','".$fila['fecha']."','".$fila['autorizado']."','".$fila['responsable']."','".$fila['hora']."','".$fila['estatus']."');";
                        //$texto = $texto.'\n';
                }	
                break;				
                case 'Mcuadrediario':
                $texto = '';
                $condicion = "  Cargo.fecha>='".$datos['Mcuadrediario']['fecha']."' and Cargo.fecha<='".$datos['Mcuadrediario']['fecha']."' ";
                $valores = $this->find('all',array('order'=>'Cargo.codcargo','conditions'=>$condicion,'recursive'=>-1));

                foreach ($valores as $reg) {
                        $fila = $reg['Cargo'];

                        $texto = $texto."INSERT INTO concargos ( codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus) values (";
                        $texto = $texto."".$fila['codcargo'].",".$fila['codsucursal'].",'".$fila['descripcion']."','".$fila['fecha']."','".$fila['autorizado']."','".$fila['responsable']."','".$fila['hora']."','".$fila['estatus']."');";
                        //$texto = $texto.'\n';
                }	
                break;				
        }	
        return $texto;
    }
    
    function reporte($datos=array(),$criteria='',$opcion='buscar'){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        switch ($opcion) {
        case 'buscar'://Usado para los reportes
            $order = " CC.fecha desc";
            $criteria=" CC.fecha >= '".$desde."' ";
            $criteria=$criteria." and CC.fecha <= '".$hasta."' ";
            if(isset($datos['codsucursal'])){
                if($datos['codsucursal']<>0){
                    $criteria = $criteria." and CC.codsucursal=".$datos['codsucursal'];
                }
            }

            $sql = 'select CC.id, CC.coddescargo, CC.codsucursal, CC.descripcion, CC.fecha, CC.autorizado, CC.responsable, CC.hora,CC.conciliar, CC.usointerno, sum(CP.totalcosto) as Total from condescargos CC inner join conproductosdescargos CP on ( CC.coddescargo = CP.coddescargo and CC.codsucursal= CP.codsucursal  ) where '.$criteria.'
    group by CC.id,CC.coddescargo, CC.codsucursal, CC.descripcion, CC.fecha, CC.autorizado, CC.responsable, CC.hora, CC.conciliar,CC.usointerno order by CC.coddescargo';
            $data = $this->query($sql);
            break;
        }
        return $data;
    }
    
    function guardar_seleccionados($datos){

        $data = array();$i = 0;
        foreach ($datos as $registros){
            $v_chequeo = $registros['conciliar'];
            $data['Condescargo'][$i]['id'] = $registros['id'];
            $data['Condescargo'][$i]['conciliar'] = 'N';
            if ($v_chequeo>=1) {
                    $data['Condescargo'][$i]['conciliar'] = 'C';
            }
            $i = $i + 1;
        }
        
        $mensaje = '<div class="alert alert-danger">Los Datos de los Descargos No se Conciliaron.</div>';	
        if($this->saveAll($data['Condescargo'])){
                $mensaje =  '<div class="alert alert-success">Los Datos de los Descargos se Guardaron Correctamente.</div>';
        }
        return $mensaje;
    }
    
    function getNroUltimoFechaAnt($fecha='',$codsucursal=0){
        
        $fecha=$this->anomesdia($fecha);
        $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        $sql = "select max(coddescargo) as ultimonro from condescargos where fecha<='".$nuevafecha."' and codsucursal=".$codsucursal;
        $data = $this->query($sql);
        $ultimonro = 0;        
        if (isset($data[0][0]['ultimonro'])){
            $ultimonro = $data[0][0]['ultimonro'];
        }
        return $ultimonro;    
    }
				  
}
?>
