<?php
class Descargoproducto extends AppModel
{
    public $name = 'Descargoproducto';
    public $primaryKey = 'coddescargo';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'productosdescargos';
/*
 *
 * */
	public $belongsTo = array('Descargo' => array('className' => 'Descargo',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'coddescargo',
						'fields'=> ''
					  )					  					  
			);

	function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
		switch ($opcion) {
			case 'Cuadrediario':
			$texto = '';
			$condicion = "  Descargo.fecha>='".$datos['Cuadrediario']['fecha']." 00:00:00' and Descargo.fecha<='".$datos['Cuadrediario']['fecha']." 23:59:59' ";
			$valores = $this->find('all',array('order'=>'Descargoproducto.coddescargo','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Descargoproducto'];
				
				$texto = $texto."INSERT INTO conproductosdescargos (coddescargo, codsucursal, coddeposito, codproducto, totalcosto, cantidad, unidad, factorconversion, costounitario) values (";
				$texto = $texto."".$fila['coddescargo'].",".$fila['codsucursal'].",".$fila['coddeposito'].",".$fila['codproducto'].",".$fila['totalcosto'].",".$fila['cantidad'].",'".$fila['unidad']."','".$fila['factorconversion']."',".$fila['costounitario'].");";
				//$texto = $texto.'\n';
			}	
			break;				
			case 'Mcuadrediario':
			$texto = '';
			$condicion = "  Descargo.fecha>='".$datos['Mcuadrediario']['fecha']."' and Descargo.fecha<='".$datos['Mcuadrediario']['fecha']."' ";
			$valores = $this->find('all',array('order'=>'Descargo.coddescargo','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Descargoproducto'];
				if($fila['costounitario']==''){
					$fila['costounitario']=0;
				}
				
				$texto = $texto."INSERT INTO conproductosdescargos (coddescargo, codsucursal, coddeposito, codproducto, totalcosto, cantidad, unidad, factorconversion, costounitario) values (";
				$texto = $texto."".$fila['coddescargo'].",".$fila['codsucursal'].",".$fila['coddeposito'].",".$fila['codproducto'].",".$fila['totalcosto'].",".$fila['cantidad'].",'".$fila['unidad']."','".$fila['factorconversion']."',".$fila['costounitario'].");";
				//$texto = $texto.'\n';
			}		
			break;				
		}	
		return $texto;
	}
				  
}
?>
