<?php
class Cpabonopago extends AppModel
{
    public $name = 'Cpabonopago';
    public $primaryKey = 'codcpabono';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cpabonopago';
	

	function busquedas($opcion='Datos',$datos=array()){
		switch ($opcion) {
		case 'Datos':
			$numero=$datos['Retencioncompra']['numero'];
			if(isset($datos['Retencioncompra']['numeroant'])){ $numero=$datos['Retencioncompra']['numeroant']; }
			$condicion = " Cpabonopago.numero = '".$numero."' and Cpabonopago.tipopago='retencion' "; 
			$data = $this->find('all',array('conditions'=>$condicion));
		break;
		case 'Uno':
			$numero=$datos['Retencioncompra']['numeroant'];
			if(isset($datos['Retencioncompra']['numeroant'])){ $numero=$datos['Retencioncompra']['numeroant']; }
			$condicion = " Cpabonopago.numero = '".$numero."' and Cpabonopago.tipopago='retencion' "; 
			$data = $this->find('first',array('conditions'=>$condicion));
		break;
		
		}
		return $data;
	}

	function actualizar($datos=array()){

		$condicion = " Cpabonopago.numero = '".$datos['Retencioncompra']['numeroant']."' and Cpabonopago.tipopago='retencion' ";
		$sql="update cpabonopago set numero='".$datos['Retencioncompra']['numero']."',fecharetencion='".$datos['Retencioncompra']['fechaemision']."',mesretencion='".$datos['Retencioncompra']['mesafecta']."', anioretencion='".$datos['Retencioncompra']['anioafecta']."' where ".$condicion;
		$data = $this->query($sql);
	}

	function guardarAbono($datos=array()){
		
		$nrodoc= $this->nro_documentos($datos['Retencioncompra']['codsucursal']);
		$longitud= $this->longitud_documentos($datos['Retencioncompra']['codsucursal']);
		$nrodoc=str_pad($nrodoc, $longitud, "0", STR_PAD_LEFT);

		$sql = "select crearcpabono(nextval('cpabono_codcpabono_seq'::regclass),'".$nrodoc."','PAGO DE RETENCION NRO ".$datos['Retencioncompra']['numero']."','".$datos['Retencioncompra']['fechaemision']."') as pagocreado;";
		$data = $this->query($sql);
		$codcpabono = $data[0][0]['pagocreado'];

		$sql = " insert into cpabonopago (codcpabono,tipopago,monto,numero,fecharetencion,mesretencion,anioretencion) values (".$codcpabono.",'retencion',".$datos['Retencioncompra']['montoretenido'].",'".$datos['Retencioncompra']['numero']."','".$datos['Retencioncompra']['fechaemision']."','".$datos['Retencioncompra']['mesafecta']."','".$datos['Retencioncompra']['anioafecta']."');";
		$data = $this->query($sql);
		$pag='PAG';
		if($datos['Retencioncompra']['tiporetencion']=='Transito'){
			$pag='PAG';
		}

		$sql =  " INSERT INTO cppagadas(codmovimientocp,tipomovimientocp, codmovimientopago,tipomovimientopago,monto,codsucursalcp,fecha,codsucursalpago) Values ('".$datos['Retencioncompra']['codmovimiento']."','".$datos['Retencioncompra']['tipodocumento']."','".$codcpabono."','".$pag."','".$datos['Retencioncompra']['montoretenido']."','".$datos['Retencioncompra']['codsucursal']."','".$datos['Retencioncompra']['fechaemision']."','".$datos['Retencioncompra']['codsucursal']."');";

		$data = $this->query($sql);
		
	}
	
				  
}
?>
