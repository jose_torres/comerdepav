<?php
class Plantilla extends AppModel
{
    public $name = 'Plantilla';
   /* public $belongsTo = array('Seccione' => array('className' => 'Seccione',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'seccione_id'
					  )
			);*/
	public $hasMany = array('Esquema' =>array('className' => 'Esquema',
					 'conditions' => '',
					 'order' => '',
					 'limit' => '',
					 'foreignKey' => 'plantilla_id',
					 'dependent' => true,
					 'exclusive' => false,
					 'finderQuery' => '',
					 'fields' => '',
					 'offset' => '',
					 'counterQuery' => ''
					 ),
					 'Marco' =>array('className' => 'Marco',
					 'conditions' => '',
					 'order' => '',
					 'limit' => '',
					 'foreignKey' => 'plantilla_id',
					 'dependent' => true,
					 'exclusive' => false,
					 'finderQuery' => '',
					 'fields' => '',
					 'offset' => '',
					 'counterQuery' => ''
					 )
				 );	
				 
	public function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Perfiles Padres
		$order="Plantilla.nombre ASC ";
		$condiciones = " ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));	
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Plantilla'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		}		
		return $var;
	}

	public function getPlantilla($criteria=''){

		$data = $this->find('first',array('conditions'=>$criteria,'recursive'=>2));
		if ($data['Plantilla']['orientacion']=='V'){
			$data['Plantilla']['orientation']='P';
		}else{
			$data['Plantilla']['orientation']='L';
		}
		return $data;
	}			
							  
}
?>
