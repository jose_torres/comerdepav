<?php
class Hajuste extends AppModel
{
    public $name = 'Hajuste';
    public $useTable = 'h_ajuste';
    public $primaryKey = 'codajuste';
    public $useDbConfig = 'comerdepa';
/*   	var $validate = array(
      'nombre' => array(
	  	'rule' => 'empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
    public $belongsTo = array('Usuario' =>
                         array('className'   => 'Usuario_alt',
                               'foreignKey'  => 'codusuario'
                         )
                  );

                  public $hasMany = array(
                    'HajusteHproducto' => array(
                        'className' => 'HajusteHproducto',
                        'foreignKey' => 'codajuste'
                    )
                );

                function resumen($id=null){  
                    $model = new Hajuste();      
                    $sql = "select distinct(hahp.codproducto), hp.codigo, hp.nombre, hp.costoanterior, hp.costonuevo, hp.preciominimoanterior, hp.preciominimonuevo, hp.preciomayoranterior, hp.preciomayornuevo, hp.preciodetalanterior, hp.preciodetalnuevo, (select estatus from h_ajuste_hproducto aux where codajuste =".$id." and codsucursal =2 and codproducto = hahp.codproducto ) as sucursal2, (select estatus from h_ajuste_hproducto aux where codajuste = ".$id." and codsucursal =3 and codproducto = hahp.codproducto ) as sucursal3"; 
                    $sql .= " from h_ajuste_hproducto hahp , h_producto hp";
                    $sql .= " where hahp.codajuste = ".$id." and hahp.codproducto = hp.codproducto and hp.codajuste = hahp.codajuste order by hp.codigo";
                    // $resumen = $model->query($sql);
                     $db = ConnectionManager::getDataSource('comerdepa');
                  $resumen =  $db->rawQuery($sql);
                     return $resumen;
                
            }

            function productos_modificados($id=null){
            $Hproducto = ClassRegistry::init('Hproducto');
            $productos_ids = $Hproducto->find('all', array(
                                                    'conditions'=> array('Hproducto.codajuste' => $id),
                                                    'fields' => array('Hproducto.codigo')                  
            ));
            $ids = array();
            foreach($productos_ids as $row): $registro = $row['Hproducto'];
                    array_push($ids, str_pad($registro['codigo'], 6, '0', STR_PAD_LEFT));          
        endforeach;
                $Producto = ClassRegistry::init('Producto');
                $productos = $Producto->find('all', array('conditions'=> array('Producto.codigo'=> $ids)));
                return $productos;
            }

function crear_tabla_temporal($nombre){
                $sql = <<<PRUEBA
CREATE TABLE if not exists public.$nombre
(
    codproducto integer NOT NULL,
    codigo character varying(8) COLLATE pg_catalog."default" NOT NULL,
    nombre text COLLATE pg_catalog."default" NOT NULL,
    coddepartamento integer NOT NULL,
    codmarca integer NOT NULL,
    modelo character varying(20) COLLATE pg_catalog."default",
    referencia character varying(15) COLLATE pg_catalog."default",
    codbarra character varying(14) COLLATE pg_catalog."default",
    codiva integer NOT NULL,
    estado character varying(10) COLLATE pg_catalog."default" NOT NULL DEFAULT 'ACTIVO'::character varying,
    stockminimo integer,
    stockmaximo integer,
    foto text COLLATE pg_catalog."default",
    unidadprincipal character varying(20) COLLATE pg_catalog."default" NOT NULL,
    unidadalterna character varying(20) COLLATE pg_catalog."default" NOT NULL,
    factorconversion integer NOT NULL,
    costoactual real NOT NULL,
    costopromedio real NOT NULL,
    costoanterior real NOT NULL,
    preciominimo real NOT NULL,
    preciomayor real NOT NULL,
    preciodetal real NOT NULL,
    numero integer,
    fecha date,
    "varchar" character varying(20) COLLATE pg_catalog."default",
    texto text COLLATE pg_catalog."default",
    "float" real,
    preciominimo1 real,
    preciomayor1 real,
    preciodetal1 real,
    minimoventap real NOT NULL DEFAULT 0.01,
    minimoventaa real NOT NULL DEFAULT 0.01,
    CONSTRAINT "pkey_$nombre" PRIMARY KEY (codproducto)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.$nombre
    OWNER to postgres;
PRUEBA;
return $sql;

}

function vaciar_tabla($nombre){
    $sql = "TRUNCATE TABLE ".$nombre." ;";
    return $sql;
}

function upsert($producto=Producto){
    $numero = ($producto['numero']=='')?'0':$producto['numero'];
$texto = <<<CONFLICTO
WITH upsert AS(
UPDATE public.producto SET 
CONFLICTO;
$texto.= "nombre = '".$producto['nombre']."',";
$texto.= "coddepartamento = ".$producto['coddepartamento'].", ";
$texto.= "codmarca = ".$producto['codmarca'].", ";
$texto.= "modelo = '".$producto['modelo']."', ";
$texto.= "referencia = '".$producto['referencia']."', ";
$texto.= "codbarra = '".$producto['codbarra']."', ";
$texto.= "codiva = ".$producto['codiva'].", ";
$texto.= "estado = '".$producto['estado']."', ";
$texto.= "stockminimo = ".$producto['stockminimo'].", ";
$texto.= "stockmaximo = ".$producto['stockmaximo'].", ";
$texto.= "foto = '".$producto['foto']."', ";
$texto.= "unidadprincipal = '".$producto['unidadprincipal']."', ";
$texto.= "unidadalterna = '".$producto['unidadalterna']."', ";
$texto.= "factorconversion = ".$producto['factorconversion'].", ";
$texto.= "costoactual = ".sprintf("%01.2f", $producto['costoactual']).", ";
$texto.= "costopromedio = ".sprintf("%01.2f",$producto['costopromedio']).", ";
$texto.= "costoanterior = ".sprintf("%01.2f",$producto['costoanterior']).", ";
$texto.= "preciominimo = ".sprintf("%01.2f",$producto['preciominimo']).", ";
$texto.= "preciomayor = ".sprintf("%01.2f",$producto['preciomayor']).", ";
$texto.= "preciodetal = ".sprintf("%01.2f",$producto['preciodetal']).", ";
$texto.= "numero = ".$numero.", ";
$texto.= "fecha = '".$producto['fecha']."', ";
$texto.= "preciominimo1 = ".sprintf("%01.2f",$producto['preciominimo1']).", ";
$texto.= "preciomayor1 = ".sprintf("%01.2f",$producto['preciomayor1']).", ";
$texto.= "preciodetal1 = ".sprintf("%01.2f",$producto['preciodetal1']).", ";
$texto.= "minimoventap = ".sprintf("%01.2f",$producto['minimoventap']).", ";
$texto.= "minimoventaa = ".sprintf("%01.2f",$producto['minimoventaa'])." ";
$texto.= " WHERE codigo='".$producto['codigo']."' RETURNING *)";    
$texto.= <<<INSERTAR
 INSERT INTO public.producto(
        codproducto, codigo, nombre, coddepartamento, codmarca, modelo, referencia, codbarra, codiva, estado, stockminimo, stockmaximo, foto, unidadprincipal, unidadalterna, factorconversion, costoactual, costopromedio, costoanterior, preciominimo, preciomayor, preciodetal, numero, fecha, preciominimo1, preciomayor1, preciodetal1, minimoventap, minimoventaa)
        SELECT 
INSERTAR;
    $texto.= $producto['codproducto'].", ";
    $texto.= "'".$producto['codigo']."', ";
    $texto.= "'".$producto['nombre']."', ";
    $texto.= $producto['coddepartamento'].", ";
    $texto.= $producto['codmarca'].", ";
    $texto.= "'".$producto['modelo']."', ";
    $texto.= "'".$producto['referencia']."', ";
    $texto.= "'".$producto['codbarra']."', ";
    $texto.= $producto['codiva'].", ";
    $texto.= "'".$producto['estado']."', ";
    $texto.= $producto['stockminimo'].", ";
    $texto.= $producto['stockmaximo'].", ";
    $texto.= "'".$producto['foto']."', ";
    $texto.= "'".$producto['unidadprincipal']."', ";
    $texto.= "'".$producto['unidadalterna']."', ";
    $texto.= $producto['factorconversion'].", ";
    $texto.= sprintf("%01.2f",$producto['costoactual']).", ";
    $texto.= sprintf("%01.2f",$producto['costopromedio']).", ";
    $texto.= sprintf("%01.2f",$producto['costoanterior']).", ";
    $texto.= sprintf("%01.2f",$producto['preciominimo']).", ";
    $texto.= sprintf("%01.2f",$producto['preciomayor']).", ";
    $texto.= sprintf("%01.2f",$producto['preciodetal']).", ";
    $texto.= $numero.", ";
    $texto.= "'".$producto['fecha']."', ";
    $texto.= sprintf("%01.2f",$producto['preciominimo1']).", ";
    $texto.= sprintf("%01.2f",$producto['preciomayor1']).", ";
    $texto.= sprintf("%01.2f",$producto['preciodetal1']).", ";
    $texto.= sprintf("%01.2f",$producto['minimoventap']).", ";
    $texto.= sprintf("%01.2f",$producto['minimoventaa'])." WHERE NOT EXISTS (SELECT 1 FROM upsert);";
return $texto;
}

function generar_linea_txt($producto=Producto){
    $texto = <<<INSERTAR
INSERT INTO public.producto(
	codproducto, codigo, nombre, coddepartamento, codmarca, modelo, referencia, codbarra, codiva, estado, stockminimo, stockmaximo, foto, unidadprincipal, unidadalterna, factorconversion, costoactual, costopromedio, costoanterior, preciominimo, preciomayor, preciodetal, numero, fecha, preciominimo1, preciomayor1, preciodetal1, minimoventap, minimoventaa)
    VALUES 
INSERTAR;
$texto.= "(.".$producto['Producto']['codproducto'].", ";
$texto.= $producto['Producto']['codigo'].", ";
$texto.= $producto['Producto']['nombre'].", ";
$texto.= $producto['Producto']['coddepartamento'].", ";
$texto.= $producto['Producto']['codmarca'].", ";
$texto.= $producto['Producto']['modelo'].", ";
$texto.= $producto['Producto']['referencia'].", ";
$texto.= $producto['Producto']['codbarra'].", ";
$texto.= $producto['Producto']['codiva'].", ";
$texto.= $producto['Producto']['estado'].", ";
$texto.= $producto['Producto']['stockminimo'].", ";
$texto.= $producto['Producto']['stockmaximo'].", ";
$texto.= $producto['Producto']['foto'].", ";
$texto.= $producto['Producto']['unidadprincipal'].", ";
$texto.= $producto['Producto']['unidadalterna'].", ";
$texto.= $producto['Producto']['factorconversion'].", ";
$texto.= $producto['Producto']['costoactual'].", ";
$texto.= $producto['Producto']['costopromedio'].", ";
$texto.= $producto['Producto']['costoanterior'].", ";
$texto.= $producto['Producto']['preciominimo'].", ";
$texto.= $producto['Producto']['preciomayor'].", ";
$texto.= $producto['Producto']['preciodetal'].", ";
$texto.= $producto['Producto']['numero'].", ";
$texto.= $producto['Producto']['fecha'].", ";
$texto.= $producto['Producto']['preciominimo1'].", ";
$texto.= $producto['Producto']['preciomayor1'].", ";
$texto.= $producto['Producto']['preciodetal1'].", ";
$texto.= $producto['Producto']['minimoventap'].", ";
$texto.= $producto['Producto']['minimoventaa']."); ";

return $texto;
}


function ejecutar($sql=''){
    $data = $this->query($sql,false);
    if($data){
        $mensaje =  '<div class="alert alert-success">Exito!</div>';
    }else{
        $mensaje =  '<div class="alert alert-danger">Error al intentar actualizar</div>';        
    }
    return $mensaje;
}


function enviarSucursal($fileroot='', $archivo='',$sucursal=null,$username='',$password='',$id=null){
   //Buscar la sucursal
   $remoto= "/var/www/html/comerdepav2/app/webroot/files/archivos/precios/";
   $Sucursal = ClassRegistry::init('Sucursal');
   $suc = $Sucursal->find('first', array('conditions'=>array('Sucursal.codsucursal'=>$sucursal), 'recursive'=>0));
    //Conectarse al servidor remoto sucursal
    $connection = ssh2_connect($suc['Sucursal']['ip'], 22);
    ssh2_auth_password($connection, $username, $password);
    //Enviar el archivo
    if($connection){
        $mensaje[0] = '<div class="alert alert-success">Conectado a '.$suc['Sucursal']['descripcion'].'</div>';
        $send = ssh2_scp_send($connection, $fileroot.$archivo, $remoto.$archivo, 0777);
        if($send){
            $mensaje[1] = '<div class="alert alert-success">Archivos copiados con exito</div>';
            //Ejecutar el archivo
            $execute = ssh2_exec($connection, 'psql '.$suc['Sucursal']['bd'].' -h localhost -U postgres < '.$remoto.$archivo);
            if($execute != false){
                //Actualizar el registro de Hajuste con estatus 'F'(finalizado) correspondiente a la sucursal
                $Hahp = ClassRegistry::init('HajusteHproducto');
                $Hahp->updateAll(array('HajusteHproducto.estatus' => "'F'"), array('HajusteHproducto.codajuste'=>$id, 'HajusteHproducto.codsucursal'=> $sucursal));
                $mensaje[2] = '<div class="alert alert-success">Inventario actualizado</div>';
           }else{
               //error al ejecutar el script sql
            $mensaje[2] = '<div class="alert alert-danger">Error al actualizar inventario. Compruebe la conexion en la sucursal y vuelva a intentar.</div>';
            }
        }else{
            //error al copiar el archivo
            $mensaje[1] = '<div class="alert alert-danger">Error al copiar los archivos</div>';
        }
    }else{
        //error de conexion
        $mensaje[0] = '<div class="alert alert-danger">No se ha podido establecer conexion con la '.$suc['Sucursal']['descripcion'].'</div>';
    }
    return $mensaje;
}

function cerrarAjuste($id=null){
    $sql = "UPDATE public.h_ajuste ha SET estatus = 'F' WHERE ha.codajuste=".$id." AND (SELECT COUNT(*) FROM public.h_ajuste_hproducto hahp WHERE  hahp.estatus = 'P' and hahp.codajuste = ha.codajuste) = 0";
    $mensaje = $this->ejecutar($sql);
    return $mensaje;
}
} 
                ?>  