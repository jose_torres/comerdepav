<?php
class Conventa extends AppModel
{
    public $name = 'Conventa';
    public $primaryKey = 'id';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'conventas';
/*
 *
 * */
	public $belongsTo = array('Concliente' => array('className' => 'Concliente',
  						'conditions' => 'Concliente.codsucursal=Conventa.codclientesucursal and Concliente.codcliente=Conventa.codcliente',
						'order' => '',
						'foreignKey' => 'codcliente',
						'fields'=> 'Concliente.codcliente,Concliente.rif,Concliente.descripcion,Concliente.direccion,Concliente.diasdecredito'
					  ),
					 				  					  
			); 

/*	public $hasMany = array('Conventaproducto' =>
                         array('className'   => 'Ventaproducto',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'codventa',
                         ),
                         'Ventaspago' =>
                         array('className'   => 'Ventaspago',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'codventa',

                         )
                       
                  );			
*/
    function ajustarDatos($opcion='pdf',$datos){
        switch ($opcion) {
        case 'pdf':
                if($datos['tipoventa']==''){
                        $datos['tipoventa']=array();
                }
                if($datos['cliente_id']==''){
                        $datos['cliente_id']=array();
                }
                if($datos['vendedore_id']==''){
                        $datos['vendedore_id']=array();
                }
                $datos['client']=$datos['cliente_id'];$datos['tipoven']=$datos['tipoventa'];
                $datos['vend']=$datos['vendedore_id'];
                $datos['fechadesde']=$datos['desde'];$datos['fechahasta']=$datos['hasta'];
        break;
        case 'impuesto':
                $datos['mes']=$datos['mes']['month'];
                $datos['year']=$datos['year']['year'];

        break;
        case 'asociar':
                $cont=0;
                list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
                $correlativo = $datos['Reporte']['correlativo'];
                for ($i = 1; $i <= $datos['Venta']['totales']; $i++) {
                        if($datos['Seleccion'][$i]['chequeo']>0){
                                $correlativo = $correlativo + 1;
                                $datos['Ventas'][$i]['codventa']=$datos['Valor'][$i]['codventa'];
                                $datos['Ventas'][$i]['codsucursal']=$datos['Valor'][$i]['codsucursal'];
                                $datos['Ventas'][$i]['codmaquina']=$maquina;
                                $datos['Ventas'][$i]['memoria']=$memoria;
                                $datos['Ventas'][$i]['correlativo']=$correlativo;
                                $datos['Ventas'][$i]['estatusz']='Asociado';
                                $datos['Ventas'][$i]['usuariow']='lrivero';
                        }

                }
                $datos['Maquina']['codmaquina']=$maquina;	
                $datos['Maquina']['correlativo']=$correlativo;	

        break;
        case 'cerrar':
                $cont=0;
                list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
                //$correlativo = $datos['Reporte']['correlativo'];
                for ($i = 1; $i <= $datos['Venta']['totales']; $i++) {
                        if($datos['Seleccion'][$i]['chequeo']>0){
                //		$correlativo = $correlativo + 1;
                                $datos['Ventas'][$i]['codventa']=$datos['Valor'][$i]['codventa'];
                                $datos['Ventas'][$i]['codsucursal']=$datos['Valor'][$i]['codsucursal'];
                                $datos['Ventas'][$i]['codmaquina']=$maquina;
                                $datos['Ventas'][$i]['memoria']=$memoria;
                //		$datos['Ventas'][$i]['correlativo']=$correlativo;
                                $datos['Ventas'][$i]['estatusz']='Cerrado';
                                $datos['Ventas'][$i]['usuariow']='lrivero';
                        }

                }			
        break;
        case 'devolver':
                $cont=0;
                list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
                $correlativo = $datos['Reporte']['correlativo'];
                for ($i = 1; $i <= $datos['Venta']['totales']; $i++) {
                        if($datos['Seleccion'][$i]['chequeo']>0){
                        //	$correlativo = $correlativo + 1;
                                $datos['Ventas'][$i]['codventa']=$datos['Valor'][$i]['codventa'];
                                $datos['Ventas'][$i]['codsucursal']=$datos['Valor'][$i]['codsucursal'];
                                $datos['Ventas'][$i]['codmaquina']=0;
                                $datos['Ventas'][$i]['memoria']=0;
                                $datos['Ventas'][$i]['correlativo']=0;
                                $datos['Ventas'][$i]['estatusz']='Abierto';
                                $datos['Ventas'][$i]['usuariow']='lrivero';
                        }

                }
                $datos['Maquina']['codmaquina']=$maquina;	
                $datos['Maquina']['correlativo']=$correlativo - $datos['Venta']['totales'];	

        break;
        case 'abrir':
                $cont=0;
                list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
                //$correlativo = $datos['Reporte']['correlativo'];
                for ($i = 1; $i <= $datos['Venta']['totales']; $i++) {
                        if($datos['Seleccion'][$i]['chequeo']>0){
                                $datos['Ventas'][$i]['codventa']=$datos['Valor'][$i]['codventa'];
                                $datos['Ventas'][$i]['codsucursal']=$datos['Valor'][$i]['codsucursal'];
                                $datos['Ventas'][$i]['vf']='SI';					
                        }								
                }			
        break;
        

        }
        return $datos;
    }

	function reporte($datos=array(),$criteria='',$opcion=0){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
                    $order='Conventa.estatus desc,Conventa.codventa';
                    $criteria=" Conventa.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Conventa.fecha <= '".$hasta." 23:59:59' ";
                    if(isset($datos['codsucursal'])){
                        $criteria=$criteria." and Conventa.codsucursal=".$datos['codsucursal']." ";
                    }
                    $c_cliente=$this->construir_or($datos['client'],'Conventa.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    /*$c_vend=$this->construir_or($datos['vend'],'Conventa.codvendedor');
                    $criteria=$criteria."  ".$c_vend;*/
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Conventa.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    $fields = "Conventa.codventa, Conventa.numerofactura, Concliente.rif, Concliente.descripcion, sum(Conventa.baseimp1 +Conventa.baseimp2+Conventa.baseimp3) as baseimp2, Conventa.poriva2, Conventa.estatus, sum(Conventa.ivaimp2 +Conventa.ivaimp2+Conventa.ivaimp3) as ivaimp2";
                    $group= "Conventa.codventa, Conventa.numerofactura, Concliente.rif, Concliente.descripcion, Conventa.baseimp2, Conventa.poriva2, Conventa.estatus";
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;		
		case 1:			
			$order=' Venta.codventa';
			$criteria=" PAGOS_VENTAS.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and PAGOS_VENTAS.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'PAGOS_VENTAS.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'PAGOS_VENTAS.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_VENTAS.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select PAGOS_VENTAS.codventa,PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.fecha, PAGOS_VENTAS.baseimp2, PAGOS_VENTAS.ivaimp2, PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago,PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Monto_Pagado, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco, PAGOS_VENTAs.coddevolucion, PAGOS_VENTAS.observacion from (
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' THEN VTP.numerotransferencia WHEN VTP.tipopago='CHEQUE' THEN VTP.numerocheque  ELSE 'S/N' END as Nropago, CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, DVT.coddevolucion,DVT.observacion from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			left join ventaspagos VTP on (VTP.codventa=VT.codventa)
			left join devolucionventa DVT on (DVT.codventa=VT.codventa)
			left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
			where VTP.tipopago<>'CREDITO'
			union all
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,VT.estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, CAP.numero as Nropago,CAP.banco, DVT.coddevolucion,DVT.observacion from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			left join ventaspagos VTP on (VTP.codventa=VT.codventa)
			left join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
			left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
			left join devolucionventa DVT on (DVT.codventa=VT.codventa)
			where VTP.tipopago='CREDITO'
			) PAGOS_VENTAS
			where 1=1 and ".$criteria."
			order by PAGOS_VENTAS.estatus, PAGOS_VENTAS.fecha, PAGOS_VENTAS.descripcion, PAGOS_VENTAS.numerofactura;";
			
			$data = $this->query($sql);
		break;
		case 2:			
			$order='Conventa.estatus desc,Venta.codventa';
			$criteria=" Conventa.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Conventa.fecha <= '".$hasta." 23:59:59' ";                       
			if(isset($datos['codsucursal'])){
			$criteria=$criteria." and Conventa.codsucursal=".$datos['codsucursal']." ";
			}
			$c_cliente=$this->construir_or($datos['client'],'Conventa.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			/*$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;*/
			$c_tipoven=$this->construir_or($datos['tipoven'],'Conventa.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;				
			//echo $criteria;
			$sql="select Conventa.id,Conventa.codventa, Conventa.codsucursal, Conventa.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion  as Cliente__descripcion,  Conventa.montoexento, Conventa.estatus,Conventa.fecha, sum(Conventa.ivaimp2+Conventa.ivaimp1+Conventa.ivaimp3) as ivaimp2, sum(Conventa.baseimp2+Conventa.baseimp1+Conventa.baseimp3) as baseimp2 from conventas as Conventa
			inner join conclientes Cliente on (Cliente.codcliente=Conventa.codcliente and Cliente.codsucursal=Conventa.codclientesucursal)                    
			where ".$criteria."
			group by Conventa.id,Conventa.codventa, Conventa.codsucursal, Conventa.numerofactura, Cliente.rif, Cliente.descripcion, Conventa.montoexento, Conventa.estatus,Conventa.fecha
			order by Conventa.codventa";
			//echo $sql;
			$data = $this->query($sql);
		break;
		case 3:
			$order='Venta.numerofactura,Venta.fecha';
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			echo $criteria;
			
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 4:			
			$order='Venta.estatus desc,Venta.codventa';
			$criteria=" fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and fecha <= '".$hasta." 23:59:59' ";			
			/*$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;*/				
			//echo $criteria;
			$sql="select fecha,sum(Contado) as contado, sum(MontoNetoCredito) as MontoNetoCredito,sum(MontoIvaCredito) as MontoIvaCredito, sum(MontoTotalCredito) as MontoTotalCredito, sum(Costo) as costo, sum(Cobros) as cobros, sum(CobrosDirectos) as CobrosDirectos,sum(NotaCredito) as notacredito	from
			(
			select Venta.fecha,
			sum(CASE WHEN Venta.estatus::text = 'P'::text or substring(Venta.numerofactura from 1 for 1)='B' THEN Venta.montobruto ELSE 0::real END) as Contado, 
			sum(CASE WHEN Venta.estatus::text = 'A'::text or substring(Venta.numerofactura from 1 for 1)='A' THEN Venta.baseimp2 + Venta.montoexento ELSE 0::real END) as MontoNetoCredito, 
			sum(CASE WHEN Venta.estatus::text = 'A'::text or substring(Venta.numerofactura from 1 for 1)='A' THEN Venta.ivaimp2 ELSE 0::real END) as MontoIvaCredito, 
			sum(CASE WHEN Venta.estatus::text = 'A'::text or substring(Venta.numerofactura from 1 for 1)='A' THEN Venta.baseimp2 + Venta.montoexento + Venta.ivaimp2 ELSE 0::real END) as MontoTotalCredito, 
			sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo, 
			0 as Cobros, 
			COALESCE(sum(Ventapago.monto),0) as CobrosDirectos, 
			0 as NotaCredito from ventas as Venta 
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
			left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
			left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa and Ventapago.tipopago<>'CREDITO') 
			left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
			left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
			Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc)  
			group by Venta.fecha 
			union all
			select Ccpag.fecha,	0::Real as Contado, 0::Real as MontoNetoCredito, 0::Real as MontoIvaCredito, 0::Real as MontoTotalCredito, 0::Real as Costo, COALESCE(sum(Ccpag.monto),0) as Cobros, 0 as CobrosDirectos, 
			0 as NotaCredito from ventas as Venta 
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
			left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
			left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa and Ventapago.tipopago<>'CREDITO') 
			left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
			left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
			Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc ) 
			group by Ccpag.fecha
			union all
			select NCventas.fecha,0::Real as Contado,0::Real as MontoNetoCredito, 0::Real as MontoIvaCredito, 0::Real as MontoTotalCredito, 0::Real as Costo, 0::Real as Cobros, 0::Real as CobrosDirectos, 
			COALESCE(sum(NCventas.montobruto),0) as NotaCredito from ventas as Venta 
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
			left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
			left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa and Ventapago.tipopago<>'CREDITO') 
			left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
			left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
			Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc ) 
			group by NCventas.fecha
			) RESUMEN_VENTA
			where ".$criteria."
			group by RESUMEN_VENTA.fecha 
			order by RESUMEN_VENTA.fecha";
			$data = $this->query($sql);
		break;
		case 5:			
			$order='';
			$criteria=" Cxc.fvencimiento >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Cxc.fvencimiento <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;				
			//echo $criteria;
			$sql="select Venta.fecha, Venta.numerofactura, Cliente.rif as Cliente__rif,
			Cliente.descripcion Cliente__descripcion,
			Cxc.fvencimiento,
			sum(Venta.montobruto) as Monto,
			sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,
			COALESCE(sum(Ccpag.monto),0) as Cobros, 
			COALESCE(sum(NCventas.montobruto),0) as NotaCredito
			from ventas as Venta
			inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
			inner join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa)
			left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc
			and Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc)
			left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura)
			where ".$criteria."
			group by Venta.fecha,Venta.numerofactura,Cliente.rif, Cliente.descripcion,Cxc.fvencimiento
			order by Cliente.descripcion,Cxc.fvencimiento";
			$data = $this->query($sql);
		break;
		case 6:			
			$order=' Venta.codventa';
			$criteria=" PAGOS_VENTAS.fechapago >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and PAGOS_VENTAS.fechapago <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'PAGOS_VENTAS.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'PAGOS_VENTAS.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_VENTAS.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select /*PAGOS_VENTAS.codventa,*/ PAGOS_VENTAS.numerofactura,PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, /*PAGOS_VENTAS.fecha, PAGOS_VENTAS.baseimp2, PAGOS_VENTAS.ivaimp2,*/ PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago, PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Monto_Pagado, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco,  PAGOS_VENTAS.concepto
			from (
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,
			VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, 
			CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' THEN VTP.numerotransferencia WHEN VTP.tipopago='CHEQUE' THEN VTP.numerocheque  ELSE 'S/N' END as Nropago, 
			CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, 'PAGO DE FACTURA AL CONTADO' as concepto 
			from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
			left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
			where VTP.tipopago<>'CREDITO'
			union all
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,
			VT.estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, 
			CAP.numero as Nropago,CAP.banco,CCA.concepto from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
			inner join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
			left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
			left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
			where VTP.tipopago='CREDITO'
			union all
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,
			VT.estatus, upper('DEVOLUCION') as TipoPago, 'NOTA DE CREDITO' AS tipomovimientopago, (NCV.montobruto *-1) as Monto_Pagado,CCP.fecha as FechaVencimiento,NCV.fecha as FechaPago, 
			NCV.codnotacredito::varchar as Nropago,'' as banco,'NOTA DE CREDITO A LA FACTURA' AS concepto from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			inner join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura and NCV.codcliente=VT.codcliente)
			--inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
			left join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
			--left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
			--left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
			--left join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura)
			union all
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,
			VT.estatus, DTP.tipopago,'PAG' as tipomovimientopago ,(DTP.monto*-1) as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, 
			DVT.coddevolucion::varchar as Nropago, 
			'' as banco, 'DEVOLUCION DE PAGO DE FACTURA' as concepto 
			from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			inner join devolucionventa DVT on (DVT.codventa=VT.codventa)
			inner join devolucionventapago DTP on (DTP.coddevolucion=DVT.coddevolucion)
			where DTP.tipopago<>'CREDITO'
			) PAGOS_VENTAS
			where 1=1 and  ".$criteria."
			group by PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago, PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Monto_Pagado, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco, PAGOS_VENTAS.concepto
			order by PAGOS_VENTAS.fechapago, PAGOS_VENTAS.descripcion  ;";
			//echo $sql;
			$data = $this->query($sql);
		break;
		case 7:			
			$order='Venta.estatus desc,Venta.codventa';
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;				
			//echo $criteria;
			$sql="select Venta.codventa, Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion  as Cliente__descripcion, 
			Ventaproducto.precio, Ventaproducto.iva*Ventaproducto.precio/100 as ivaimp, Venta.estatus,Venta.fecha, Vendedore.descripcion as Vendedore__descripcion, 
			Producto.codigo,Producto.nombre,Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad,Ventaproducto.indice,
			(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,(Ventaproducto.cantidad*Ventaproducto.precio) as Total 
			from ventas as Venta
			inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
			inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
			inner join producto Producto on (Ventaproducto.codproducto=Producto.codproducto)
			where ".$criteria."
			group by Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, 
			Ventaproducto.precio, Ventaproducto.iva, Venta.estatus,Venta.fecha, Vendedore.descripcion, Producto.codigo, Producto.nombre, Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad, Ventaproducto.indice
			order by Venta.codventa, Ventaproducto.indice";
			$data = $this->query($sql);
		break;		
		case 8:
			$order='Venta.codvendedor,Venta.fecha';
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 9:
			$order='Venta.numerofactura,Venta.fecha';
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";
			$criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Abierto' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 10:
			$order='Venta.numerofactura,Venta.fecha';
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";
			$criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Asociado' ";	
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 11:
			$order='Venta.numerofactura,Venta.fecha';
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";
			$criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Cerrado' ";	
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 12:
			$order='Venta.numerofactura,Venta.fecha';
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";
			$criteria=$criteria." and Venta.vf = 'NO' ";	
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 14:			
			$order='Venta.estatus desc,Venta.codventa';
			$criteria=" VT.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and VT.fecha <= '".$hasta." 23:59:59' ";			
		/*	$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;*/				
			//echo $criteria;
			$sql="select VT.fecha,VT.hora,VT.codventa,VT.numerofactura,VT.estatus,
			VT.codcliente,VT.codsucursal,VT.rif,VT.descripcion, VT.poriva,
			retencion,VT.contado,VT.credito,VT.impuestos,
			sum(pagoefectivo) as pagoefectivo,sum(pagodebito) as pagodebito,sum(pagocheque) as pagocheque,
			sum(pagotransfer) as pagotransfer
			from (
			select Venta.fecha,Venta.hora,Venta.codventa,Venta.numerofactura,Venta.estatus,
			CL.codcliente,CL.codsucursal,CL.rif,cl.descripcion, MCC.poriva,
			CASE WHEN Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E' THEN Venta.retencion ELSE 0 END as retencion,
			CASE WHEN Venta.estatus='P' or  MCC.poriva::text is null  THEN (Venta.baseimp1+Venta.baseimp2+Venta.baseimp3) ELSE 0 END as contado,
			CASE WHEN Venta.estatus='A' or MCC.poriva::text is not null THEN (Venta.baseimp1+Venta.baseimp2+Venta.baseimp3) ELSE 0 END as credito, 
			CASE WHEN Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E' THEN (Venta.ivaimp1+Venta.ivaimp2+Venta.ivaimp3) ELSE 0 END as impuestos,
			CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and VTP.tipopago='EFECTIVO' THEN VTP.monto  ELSE 0 END as pagoefectivo, 
			CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and VTP.tipopago='DEBITO' THEN VTP.monto  ELSE 0 END as pagodebito, 
			CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and VTP.tipopago='CHEQUE' THEN VTP.monto  ELSE 0 END as pagocheque,
			CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and VTP.tipopago='TRANSFER' THEN VTP.monto  ELSE 0 END as pagotransfer 
			from ventas Venta
			inner join clientes CL on (CL.codcliente=Venta.codcliente and CL.codsucursal=Venta.codclientesucursal)
			inner join ventaspagos VTP on (VTP.codventa=Venta.codventa)
			left join movimientoivacc MCC on (MCC.codmovimiento=Venta.codventa)
			) VT
			where ".$criteria."
			group by VT.fecha,VT.hora,VT.codventa,VT.numerofactura,VT.estatus,
			VT.codcliente,VT.codsucursal,VT.rif,VT.descripcion, VT.poriva,
			retencion,retencion,VT.contado,VT.credito,VT.impuestos
			order by VT.fecha,VT.hora";
			$data = $this->query($sql);
		break;
		case 15:			
			$order=' Venta.codventa';
			$criteria=" PAGOS_VENTAS.fechapago >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and PAGOS_VENTAS.fechapago <= '".$hasta." 23:59:59' ";			
			
			$criteria=$criteria." and PAGOS_VENTAS.tipopago like '%".$datos['tipopago']."%'";			
			//echo $criteria;
			$sql="select PAGOS_VENTAS.numerofactura,PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, /*PAGOS_VENTAS.fecha, PAGOS_VENTAS.baseimp2, PAGOS_VENTAS.ivaimp2,*/ PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago, PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Monto_Pagado, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco,  PAGOS_VENTAS.concepto
			from (
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,
			VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, 
			CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' or VTP.tipopago='DEBITO' THEN VTP.numerotransferencia WHEN VTP.tipopago='CHEQUE' THEN VTP.numerocheque  ELSE 'S/N' END as Nropago, 
			CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, 'PAGO DE FACTURA AL CONTADO' as concepto 
			from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
			left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
			where VTP.tipopago<>'CREDITO'
			union all
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,
			VT.estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, 
			CAP.numero as Nropago,CAP.banco,CCA.concepto from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
			inner join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
			left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
			left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
			where VTP.tipopago='CREDITO'
			union all
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,
			VT.estatus, upper('DEVOLUCION') as TipoPago, 'NOTA DE CREDITO' AS tipomovimientopago, (NCV.montobruto *-1) as Monto_Pagado,CCP.fecha as FechaVencimiento,NCV.fecha as FechaPago, 
			NCV.codnotacredito::varchar as Nropago,'' as banco,'NOTA DE CREDITO A LA FACTURA' AS concepto from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			inner join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura and NCV.codcliente=VT.codcliente)
			--inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
			left join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
			--left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
			--left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
			--left join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura)
			union all
			select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,
			VT.estatus, DTP.tipopago,'PAG' as tipomovimientopago ,(DTP.monto*-1) as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, 
			DVT.coddevolucion::varchar as Nropago, 
			'' as banco, 'DEVOLUCION DE PAGO DE FACTURA' as concepto 
			from ventas VT 
			inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
			inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
			inner join devolucionventa DVT on (DVT.codventa=VT.codventa)
			inner join devolucionventapago DTP on (DTP.coddevolucion=DVT.coddevolucion)
			where DTP.tipopago<>'CREDITO'
			) PAGOS_VENTAS
			where 1=1 and  ".$criteria."
			group by PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago, PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Monto_Pagado, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco, PAGOS_VENTAS.concepto
			order by PAGOS_VENTAS.fechapago, PAGOS_VENTAS.descripcion  ;";
			//echo $sql;
			$data = $this->query($sql);
		break;
		}
		return $data;
	}

	function reporteImpuestos($datos=array(),$criteria='',$opcion=0){
		//$hasta=$this->anomesdia($datos['fechahasta']);
		//$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$ult_dia = date("d",(mktime(0,0,0,$datos['mes']+1,1,$datos['year'])-1));
			$order='Venta.numerofactura,Venta.fecha';
			$criteria=" Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ";			
		
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 1:			
			$order='Venta.estatus desc,Venta.codventa';
			$ult_dia = date("d",(mktime(0,0,0,$datos['mes']+1,1,$datos['year'])-1));
			$order='Venta.numerofactura,Venta.fecha';
			$criteria=" Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ";
		/*	if ($datos['tipofecha']=='FF'){
				$criteria=" Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
				$criteria=$criteria." and Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ";
			}else{
				$criteria="CASE WHEN Venta.estatus='A' THEN HCXC.fechacobro >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ELSE Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' END";
				$criteria=$criteria." and CASE WHEN Venta.estatus='A' THEN HCXC.fechacobro <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ELSE Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' END ";
			}*/
			/*$criteria=$criteria." and Cliente.tipodecliente = '".$datos['tipocontribuyente']."' ";*/
				
			//echo $criteria;
			$sql="select Venta.codventa, Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion  as Cliente__descripcion,  Venta.montoexento, Venta.estatus,Venta.fecha, Vendedore.descripcion as Vendedore__descripcion, Ventaproducto.iva as porimp2, Venta.retencion::numeric,
			sum(Ventaproducto.cantidad::numeric * Ventaproducto.precio::numeric * Ventaproducto.iva::numeric/100::numeric) as ivaimp2, sum( Ventaproducto.cantidad::numeric*Ventaproducto.precio::numeric) as baseimp2, sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo from ventas as Venta
			inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
			inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
			left join h_cuentasporcobrar HCXC on (HCXC.codmovimiento=Venta.codventa and trim(HCXC.tipomovimiento)='VENTAS')
			where ".$criteria."
			group by Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, 
			 Ventaproducto.iva,Venta.montoexento, Venta.retencion, Venta.estatus,Venta.fecha, vendedore.descripcion
			order by Venta.codventa";
			//echo $sql;
			$data = $this->query($sql);
		break;
		}
		return $data;	
	}

	function ultimaFechaAsociada(){
		$fecha = date('Y-m-d');
		$sql = "select max(fecha) as ultimafecha from ventas where vf='SI' and (estatusz='Asociado' or estatusz='Cerrado');";
		$data = $this->query($sql);
		if (isset($data[0][0]['ultimafecha'])){
			$fecha = $data[0][0]['ultimafecha'];
		}
		return $fecha;
	}

	function obtenerSucursal(){
		$sucursal = 0;
		$sql = "select distinct codsucursal from ventas";
		$data = $this->query($sql);
		if (isset($data[0][0]['codsucursal'])){
			$sucursal = $data[0][0]['codsucursal'];
		}
		return $sucursal;
	}

	function pagosRealizados($datos=array()){
		$datos['tipopago']='EFECTIVO';
		$datapago['efectivo']=$this->reporte($datos,'',15);
		$datos['tipopago']='DEBITO';
		$datapago['debito']=$this->reporte($datos,'',15);
		$datos['tipopago']='CHEQUE';
		$datapago['cheque']=$this->reporte($datos,'',15);
		$datos['tipopago']='TRANSFER';
		$datapago['transfer']=$this->reporte($datos,'',15);
		return $datapago;
	}

	function ventasContado($condicion=''){
		$sql = "select VT.fecha, sum(montobruto) as total from conventas VT where VT.estatus <> 'E' ".$condicion."
		group by VT.fecha";
		$data = $this->query($sql);
		return $data;
	}

	function nroRetencionNuevos($sucursal=array()){
		$data = array();
		$sql="select COALESCE(count(codventa),0) as nro_reg from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select RVD.codventa, RVD.codsucursal from ventas RVD order by RVD.codventa') as t1(codventa bigint,codsucursal bigint) where (codventa,codsucursal) not in (select codventa,codsucursal from conventas)";
		//echo $sql;
		$data=$this->query($sql);
		$cont = 0;
		//print_r($data);
		if(isset($data[0][0]['nro_reg'])){
			$cont = $data[0][0]['nro_reg'];
		}
		return $cont;
	}

	function transferirventas($sucursal=array()){

		$sql="INSERT INTO conventas (codventa, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal, hora, coddeposito, codusuarioconectado, recibido, estatus, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3,  retencion, nrocomprobantefiscal, vf, codmaquina, memoria, estatusz, usuariow, correlativo)
		select codventa, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal, hora, coddeposito, codusuarioconectado, recibido, estatus, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, nrocomprobantefiscal, vf, codmaquina, memoria, estatusz, usuariow, correlativo from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select codventa, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal, hora, coddeposito, codusuarioconectado, recibido, estatus, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, nrocomprobantefiscal, vf, codmaquina, memoria, estatusz, usuariow, correlativo
		from ventas 
		order by ventas.fecha') as t1(codventa bigint, codsucursal bigint, numerofactura text, fecha date, codcliente bigint, codvendedor bigint, codclientesucursal bigint, hora time, coddeposito bigint, codusuarioconectado bigint, recibido numeric, estatus text, montobruto numeric, montoexento numeric, baseimp1 numeric, baseimp2 numeric, baseimp3 numeric, ivaimp1 numeric, ivaimp2 numeric, ivaimp3 numeric, porimp1 numeric, porimp2 numeric, porimp3 numeric,  retencion numeric, nrocomprobantefiscal integer, vf text, codmaquina bigint, memoria bigint, estatusz text, usuariow text, correlativo bigint) where (codventa,codsucursal) not in (select codventa,codsucursal from conventas) ";
		$data=$this->query($sql);
		return $data;
	}

    function incluirFaltante($sucursal=''){
            // Buscar Registros que no estan en el consolidado
            $cantidad = $this->nroRetencionNuevos($sucursal);
            // Incluir si el registro es mayor a cero
            if($cantidad>0){
                    $this->transferirventas($sucursal);
            }
    }
	
    function resumenVentas($datos= array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" V.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and V.fecha <= '".$hasta." 23:59:59' ";
        if(isset($datos['codsucursal'])){
            if ($datos['codsucursal']==''){
                $datos['codsucursal']= 0;
            }			
            $criteria=$criteria." and V.codsucursal=".$datos['codsucursal']." ";
        }		

        $sql = "select V.fecha, V.codsucursal,  sum(V.ivaimp) as iva, sum(costo) as costo, sum(total) as neto,sum(total+ivaimp) as total   
        from (
        select  Venta.numerofactura,Venta.fecha::date,Venta.codsucursal,Ventaproducto.precio, Ventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio/100 as ivaimp, Venta.estatus, 
        Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad, 'V' as Tipo,
         (Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,
        (Ventaproducto.cantidad*Ventaproducto.precio) as Total 
        from conventas as Venta
        inner join conventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa and Ventaproducto.codsucursal=Venta.codsucursal)
        group by  Venta.numerofactura,Ventaproducto.precio,Venta.codsucursal, Ventaproducto.iva, Venta.estatus,Venta.fecha::date,  Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad
        union
        select 
        Venta.numerofactura,Devolucionventa.fecha::date,Devolucionventa.codsucursal,Ventaproducto.precio, (Devolucionventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio*-1)/100 as ivaimp, Venta.estatus, 
        Devolucionventaproducto.cantidad *-1, Ventaproducto.costopromedio,Ventaproducto.unidad, 'D' as Tipo,
        Devolucionventaproducto.cantidad*Ventaproducto.costopromedio*-1 as Costo,
        (Devolucionventaproducto.cantidad*Ventaproducto.precio*-1) as Total 
         from condevolucionventa as Devolucionventa
        inner join conventas Venta on (Devolucionventa.codventa=Venta.codventa  and Devolucionventa.codsucursal=Venta.codsucursal)
        inner join (select coddevolucion,codsucursal,codproducto,cantidad from condevolucionventaproductos group by coddevolucion,codsucursal,codproducto,cantidad) 
        Devolucionventaproducto on (Devolucionventa.coddevolucion=Devolucionventaproducto.coddevolucion and Devolucionventa.codsucursal=Devolucionventaproducto.codsucursal)
        inner join ( select codproducto,precio,cantidad,costopromedio,codventa,iva,unidad from conventasproductos 
        group by codproducto,precio,cantidad,costopromedio,codventa,iva,unidad ) Ventaproducto on (Ventaproducto.codventa=Devolucionventa.codventa 
        and Devolucionventaproducto.codproducto=Ventaproducto.codproducto)

        group by 
        Venta.numerofactura,Ventaproducto.precio,Devolucionventa.codsucursal, Ventaproducto.iva, Venta.estatus,Devolucionventa.fecha::date,  Devolucionventaproducto.cantidad, 
        Ventaproducto.costopromedio,Ventaproducto.unidad
        ) as V
        where ".$criteria."
        group by V.fecha,V.codsucursal
        order by V.fecha,V.codsucursal";
        $data = $this->query($sql);
        return $data;
    }
    
    function resumenPagosVentas($datos= array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" V.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and V.fecha <= '".$hasta." 23:59:59' ";
        if(isset($datos['codsucursal'])){
            if ($datos['codsucursal']==''){
                $datos['codsucursal']= 0;
            }			
            $criteria=$criteria." and V.codsucursal=".$datos['codsucursal']." ";
        }		

        $sql = "select V.fecha,V.codsucursal, V.tipopago, sum(Monto_Pagado) as monto
from (
select  Venta.numerofactura,Venta.fecha::date,Venta.codsucursal, Venta.estatus, 
VTP.tipopago,  'V' as Tipo, sum(VTP.monto) as Monto_Pagado
from conventas as Venta
inner join conventaspagos VTP on (VTP.codventa=Venta.codventa and VTP.codsucursal=Venta.codsucursal)
group by  Venta.numerofactura,Venta.codsucursal,  Venta.estatus,
Venta.fecha::date, VTP.tipopago
union
select 
Venta.numerofactura,Devolucionventa.fecha::date,Devolucionventa.codsucursal, Venta.estatus, 
VTP.tipopago, 'D' as Tipo, sum(VTP.monto*-1) as Monto_Pagado
 from condevolucionventa as Devolucionventa
inner join conventas Venta on (Devolucionventa.codventa=Venta.codventa  and Devolucionventa.codsucursal=Venta.codsucursal)
inner join condevolucionventapago VTP on (VTP.coddevolucion=Devolucionventa.coddevolucion and VTP.codsucursal=Devolucionventa.codsucursal)
group by 
Venta.numerofactura,Devolucionventa.codsucursal,  
Venta.estatus,Devolucionventa.fecha::date, VTP.tipopago
) V
where ".$criteria."
group by V.fecha,V.codsucursal, V.tipopago
order by V.fecha,V.codsucursal, V.tipopago
";
        $data = $this->query($sql);
        return $data;
    }
    
    function nroVentasRealizadas($datos= array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" V.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and V.fecha <= '".$hasta." 23:59:59' ";
        if(isset($datos['codsucursal'])){
            if ($datos['codsucursal']==''){
                $datos['codsucursal']= 0;
            }			
            $criteria=$criteria." and V.codsucursal=".$datos['codsucursal']." ";
        }		

        $sql = "select fecha::date, count(codventa) as nro from conventas as V where ".$criteria." group by fecha::date order by V.fecha::date";
        $data = $this->query($sql);
        //echo $sql;
        //die();
        if(($desde==$hasta) && count($data)==0 ){
            $data = array(0=>array('fecha'=>$desde,'nro'=>0));
        }
        return $data;
    } 
//---------------Funciones de Interfaces-----------------------------------
    function obtenerResumenDia($cuadre=array()){
        
        $dataResumen['VentaDia'] = $this->ventasContado(" and VT.fecha='".$cuadre['Concuadrediario']['fecha']."' and VT.codsucursal=".$cuadre['Concuadrediario']['codsucursal']." ");
        if(!isset($dataResumen['VentaDia'][0][0]['total'])){
                $dataResumen['VentaDia'][0][0]['total']=0;
        }
        $datos=array('fechahasta'=>$this->diamesano($cuadre['Concuadrediario']['fecha']),'fechadesde'=>$this->diamesano($cuadre['Concuadrediario']['fecha']),'client'=>array(0),'tipoven'=>array(0),'codsucursal'=>$cuadre['Concuadrediario']['codsucursal']);

        $dataResumen['Venta'] =  $this->resumenVentas($datos);
        $dataResumen['Pago'] =  $this->resumenPagosVentas($datos);
        $dataResumen['Nro'] =  $this->nroVentasRealizadas($datos);
        return $dataResumen;
    }
}
?>
