<?php
class Contenido extends AppModel
{
    public $name = 'Contenido';

	public $belongsTo = array('Esquema' => array('className' => 'Esquema',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'esquema_id'
					  ),'Documento' => array('className' => 'Documento',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'documento_id'
						)
			);
	
	public function cargarContenido($datos=array(),$opcion='Ejemplo'){
		
		switch ($opcion) {
		case 'Ejemplo'://LLena de Ejemplo	
		$var = array();$texto='<ol>';
		foreach ($datos as $row) {
			$texto=$texto.'<li><strong>'.$row['Seccione']['nombre'].'</strong></li>';
			$texto=$texto.'<ol  type="A">
<li> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
<li>Aenean eget libero et erat faucibus condimentum quis sit amet justo.</li>
<li>Morbi tristique leo a elementum iaculis.</li>
<li>Donec interdum ex et convallis mollis.</li>
<li>Phasellus vitae ante efficitur, ultrices dui a, congue tortor.</li>
<li>Aliquam ut elit luctus, aliquam lectus ut, hendrerit ipsum.</li>
<li>Quisque ut ex eu lorem volutpat varius.</li>
<li>Curabitur ac eros tincidunt, vestibulum nisi ac, pharetra elit.</li>
<li>Donec sit amet lectus ac ipsum vehicula porttitor.</li>
<li>Aliquam quis ex fermentum velit interdum convallis at non nisi.</li>
<li>Aliquam sit amet arcu eu sem gravida vulputate.</li>
<li>Nunc vitae nunc vel massa egestas cursus ac eu leo.</li>
<li>Donec in magna eu lacus accumsan ultricies a nec massa.</li>
<li>Duis placerat neque et ante pellentesque interdum.</li>
<li>Duis a nisl molestie, rhoncus est in, ultrices massa.</li>
<li>Mauris interdum metus eu nisi pretium cursus.</li>
<li>Proin scelerisque odio id nunc bibendum bibendum.</li>
<li>Mauris rhoncus tortor id ex varius, quis ornare risus eleifend.</li>
<li>Etiam et eros et ligula mattis semper.</li>
</ol><br>';
			$var['Contenido'][$row['seccione_id']]=array('seccione_id'=>$row['seccione_id'],'elemento_nombre'=>$row['Seccione']['nombre'],'texto'=>$texto);
		}
		$texto=$texto.'</ol> ';
		break;
		case 'Contenido'://LLena de Ejemplo	
		$var = array();$texto='<ol>';
		foreach ($datos as $row) {
			$texto=$texto.'<li><strong>'.$row['Esquema']['Seccione']['nombre'].'</strong></li>';
			$texto=$texto.$row['Contenido']['texto'].'';
			$var['Contenido'][$row['Esquema']['seccione_id']]=array('seccione_id'=>$row['Esquema']['seccione_id'],'elemento_nombre'=>$row['Esquema']['Seccione']['nombre'],'texto'=>$texto);
		}
		$texto=$texto.'</ol> ';
		break;
		}		
		return array($var,$texto);
	}
	
	function guardar($tot_reg=0,$id_doc=0,$datos=array()){
		$valores['Contenido']=array();
		for ($i = 1; $i <= $tot_reg; $i++) {
			$valores['Contenido'][$i-1]['documento_id']=$id_doc;
			$valores['Contenido'][$i-1]['esquema_id']=$datos['Documento'][$i]['esquema'];
			$valores['Contenido'][$i-1]['texto']=$datos['Documento'][$i]['seccion'];
		}
		
		if ($this->saveAll($valores['Contenido'])){
			return true;
		}else{
			return false;
		}	
	}
	
	function actualizar($tot_reg=0,$id_doc=0,$datos=array()){
		$valores['Contenido']=array();
		for ($i = 1; $i <= $tot_reg; $i++) {
			$valores['Contenido'][$i-1]['id']=$datos['Documento'][$i]['contenido'];
			$valores['Contenido'][$i-1]['documento_id']=$id_doc;
			$valores['Contenido'][$i-1]['esquema_id']=$datos['Documento'][$i]['esquema'];
			$valores['Contenido'][$i-1]['texto']=$datos['Documento'][$i]['seccion'];
		}
		
		if ($this->saveAll($valores['Contenido'])){
			return true;
		}else{
			return false;
		}	
	}
				  
}
?>
