<?php
class Tipojustificacione extends AppModel
{
    var $name = 'Tipojustificacione';
    var $useTable = 'tipojustificaciones';
/*   	var $validate = array(
      'nombre' => array(
	  	'rule' => 'empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
    var $hasMany = array('Documento' =>
                         array('className'   => 'Documento',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'tipojustificacione_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),'Revisione' =>
                         array('className'   => 'Revisione',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'tipojustificacione_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )
                  );

	function actualizar($datos=array(),$funcion='add'){
		$id='';
		if (isset($datos['Tipojustificacione']['id'])){
			$id='/'.$datos['Tipojustificacione']['id'];
		}
		$mensaje='El Tipo de Justificación no se puede guardar.';
		$url='/tipojustificaciones/'.$funcion.$id;
		$verificacion=false;
		if ($this->save($datos)){
			$mensaje='El Tipo de Justificación ha sido Guardado.';
			$url='/tipojustificaciones';
			$verificacion=true;
		}
		return array($mensaje,$url,$verificacion); 
	}

	function eliminar($nro_registro=0,$id=0){
		$mensaje='';
		if($nro_registro<=0){
			if ($this->delete($id)){
				$mensaje='El Tipo de Justificación ha sido Eliminado.';
			}		
		 }else{
			$mensaje='El Tipo de Justificación no ha sido Eliminado, porque posee registro asociados.'; 
		 }
		return $mensaje; 
	}

	function obtener_acciones(){
		$datos['Crear']='Crear';
		$datos['Revisar']='Revisar';
		$datos['Aprobar']='Aprobar';
		$datos['Rechazar']='Rechazar';		
		$datos['Anular']='Anular';		
		return $datos; 
	}

	function llenar_combo($relleno='',$opcion=0,$conditions=''){
		switch ($opcion) {
		case 0://Muestra Todos los registros
		$order="Tipojustificacione.nombre ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1,'conditions'=>$conditions));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Tipojustificacione'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;			
		}//Cierre de switch						
		return $var;
	}            			  
				  
}
?>
