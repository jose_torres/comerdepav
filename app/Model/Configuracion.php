<?php
/* SVN FILE: $Id$ */

/**
 * Enter description here ....
 *
 * @filesource
 * @copyright    Copyright (c) 2006, .
 * @link
 * @package
 * @subpackage
 * @since
 * @version      $Revision$
 * @modifiedby   $LastChangedBy$
 * @lastmodified $Date$
 * @license      http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Configuracion
 *
 * Enter description here...
 *
 * @package
 * @subpackage
 * @since
 */
class Configuracion extends AppModel
{
    public $name = 'Configuracion';
    public $useTable = 'configuracion';
/*   	var $validate = array(
      'funcion_id' => array(
	  	'rule' => 'not_empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
    public $belongsTo  = array('Perfile' =>
                           array('className'  => 'Perfile',
                                 'conditions' => '',
                                 'order'      => '',
                                 'foreignKey' => 'perfil_id'
                           ),
			    'Funcione' =>
                           array('className'  => 'Funcione',
                                 'conditions' => '',
                                 'order'      => '',
                                 'foreignKey' => 'funcion_id'
                           )
    );
//Esta funcion trae los menus por perfil    
    function menu($id)
    {
        $datos=$this->query("SELECT grupo.id,grupo.nombre as gnombre,funcion.id as fid,funcion.nombre as fnombre,funcion.direccion,funcion.nombreimagen,grupo.imagen, configuracion.perfil_id
 FROM funcion,perfil,grupo,configuracion 
 WHERE funcion.grupo_id=grupo.id and configuracion.perfil_id=perfil.id and configuracion.funcion_id=funcion.id and funcion.visible='SI' and funcion.parent_id=0 and configuracion.status='1' and 
 configuracion.perfil_id=".$id." order by grupo.usuario_id,fnombre; ");
		
        return $datos;
	
    }

    function panel($id)
    {
            $datos=$this->query("SELECT titulopanel,imagenpanel,direccion FROM funcion,perfil,grupo,configuracion 
WHERE funcion.grupo_id=grupo.id and configuracion.perfil_id=perfil.id and configuracion.funcion_id=funcion.id and 
configuracion.perfil_id=".$id." and panel='1' order by funcion.id; ");
        return $datos;

    }	

    function arbol($id)
    {
            $datos=$this->query("select Funcione.id,Funcione.nombre,Funcione.direccion, 
CASE WHEN Funcione.id in (select funcion_id from configuracion where perfil_id=".$id." and status='1') THEN 1
        ELSE 0
   END
as seleccion       
from funcion as Funcione
order by Funcione.direccion; ");
        return $datos;

    }
    
    
}

?>
