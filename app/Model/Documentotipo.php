<?php
class Documentotipo extends AppModel
{
    public $name = 'Documentotipo';
    public $useTable = 'documentotipo';
    public $useDbConfig = 'comerdepa';
    /* Funciones para la vista */ 	
	public function llenar_combo($opcion=0){
		switch ($opcion) {
		case 0:
			$datos = $this->find('all');
			$i=1;
			$var = array();
			foreach ($datos as $row) {
				$item = $row['Documentotipo'];	
				$var[$item['id']] = $item['descorta'];
				$i=$i+1;
			}
		break;
		case 1:
			$datos = $this->find('all',array('conditions'=>"Documentotipo.accion='AUMENTA'",'recursive'=>0));
			$i=1;
			$var = array();
			foreach ($datos as $row) {
				$item = $row['Documentotipo'];	
				$var[$item['id']] = $item['descripcion'];
				$i=$i+1;
			}
		break;			
		case 2:
			$datos = $this->find('all',array('conditions'=>"Documentotipo.accion='AUMENTA' and Documentotipo.descorta='DP'",'recursive'=>0));
			$i=1;
			$var = array();
			foreach ($datos as $row) {
				$item = $row['Documentotipo'];	
				$var[$item['id']] = $item['descripcion'];
				$i=$i+1;
			}
		break;	
		case 3:
			$datos = $this->find('all',array('conditions'=>"Documentotipo.accion='AUMENTA' and Documentotipo.descorta='NC'",'recursive'=>0));
			$i=1;
			$var = array();
			foreach ($datos as $row) {
				$item = $row['Documentotipo'];	
				$var[$item['id']] = $item['descripcion'];
				$i=$i+1;
			}	
		break;			
		}			
		return $var;
	}
  /* Fin de Funciones para la vista */ 
     	function mayuscula_documentotipos() {
			$datos=$this->query("update documentotipo set descripcion = upper(descripcion), descorta=upper(descorta)");
		}					  
}

?>
