<?php
class Puntodeventa extends AppModel
{
    public $name = 'Puntodeventa';
    public $primaryKey = 'codpuntoventa';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'puntodeventa';
	
	/*public $belongsTo = array('Venta' => array('className' => 'Venta',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codventa',
						'fields'=> ''
					  )					  					  
			);*/
	
  /* Funciones para la vista */ 	
	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$order="Puntodeventa.codpuntoventa ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Puntodeventa'];	
			$var[$relleno.$item['codpuntoventa']] = $item['modelo'].' '.$item['marca'].' '.$item['serial'];
		}
		break;
		case 1://Muestra Todos los Bancos registrados con Cuentas Asociadas
		$order="Puntodeventa.codpuntoventa ASC";
		$datos = $this->find('all',array('order'=> $order));
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Banco'];
			if(count($row['Cuentasbancaria'])>0){	
			$var[$relleno.$item['codbanco']] = $item['nombre'];
			}
		}
		break;				
		}//Cierre de switch						
		return $var;
	}

	function buscariva(){
		$sql="select iva from ventasproductos group by iva ";
		$data = $this->query($sql);
	}

	function buscarpagos($condicion=''){
		
		$sql = "select PUNTO.fecha, PUNTO.codpuntoventa, PUNTO.modelo, PUNTO.marca, PUNTO.serial, 
PUNTO.codbanco, PUNTO.nrodecuenta, PUNTO.nombre, PUNTO.tipopago,PUNTO.tipomovimientopago, 
PUNTO.codcuenta, sum(PUNTO.Monto_Pagado) as Monto_Pagado, 
sum(PUNTO.monto_depositado) as monto_depositado, sum(PUNTO.Monto_Pagado) - sum(PUNTO.monto_depositado) as monto_x_depositar
from
(
select VT.fecha, VTP.codpuntoventa, PDV.modelo, PDV.marca, PDV.serial, PDV.codbanco, 
PDV.nrodecuenta, BC.nombre, VTP.tipopago,'PAG' as tipomovimientopago, 
CTB.codcuenta, sum(VTP.monto) as Monto_Pagado, 
0 as monto_depositado
from ventas VT 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
inner join puntodeventa PDV on (PDV.codpuntoventa=VTP.codpuntoventa)
inner join bancos BC on (PDV.codbanco=BC.codbanco)
inner join cuentasbancarias CTB on (CTB.numerocuenta=PDV.nrodecuenta)
where VTP.tipopago='TDEBITO' or VTP.tipopago='DEBITO' and VT.estatus<>'E'
group by VT.fecha, VTP.codpuntoventa, PDV.modelo,PDV.marca, PDV.serial, PDV.codbanco, 
PDV.nrodecuenta, BC.nombre, VT.estatus, VTP.tipopago, CTB.codcuenta
union all 
select ST.fecha,ST.maquina_id,PDV.modelo, PDV.marca, PDV.serial, PDV.codbanco, 
PDV.nrodecuenta, BC.nombre,'DEBITO' as tipopago,'PAG' as tipomovimientopago, 
CTB.codcuenta,
0 as Monto_Pagado, sum(monto) as monto_depositado
from suctarjetas as ST
inner join puntodeventa PDV on (PDV.codpuntoventa=ST.maquina_id)
inner join bancos BC on (PDV.codbanco=BC.codbanco)
inner join cuentasbancarias CTB on (CTB.numerocuenta=PDV.nrodecuenta)
group by ST.fecha,ST.maquina_id,PDV.modelo, PDV.marca, PDV.serial, PDV.codbanco, 
PDV.nrodecuenta, BC.nombre, CTB.codcuenta
) PUNTO
where 1=1 ".$condicion."
group by PUNTO.fecha, PUNTO.codpuntoventa, PUNTO.modelo, PUNTO.marca, PUNTO.serial, 
PUNTO.codbanco, PUNTO.nrodecuenta, PUNTO.nombre, PUNTO.tipopago,PUNTO.tipomovimientopago, 
PUNTO.codcuenta
order by PUNTO.fecha";
		$data = $this->query($sql);
		return $data;
	}
				  
}
?>
