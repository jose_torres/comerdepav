<?php
class Departamento extends AppModel
{
    public $name = 'Departamento';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';
	public $primaryKey = 'coddepartamento';
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'departamentos';
/*
 * */
	public $hasMany = array('Producto' =>
                         array('className'   => 'Producto',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'coddepartamento',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),
                  );

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0:
		$order="Departamento.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Departamento'];	
			$var[$relleno.$item['coddepartamento']] = $item['nombre'];
		}
		break;
		case 1:
		$order="Departamento.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Departamento'];	
			$var[$relleno.$item['coddepartamento']] = $item['codigo'].' '.$item['descripcion'];
		}
		break;
		case 2:
		$order="Departamento.codigo ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Departamento'];	
			$var[$relleno.$item['codigo']] = $item['codigo'].' '.$item['descripcion'];
		}
		break;			
		}				
		return $var;
	}

}
?>
