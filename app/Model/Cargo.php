<?php
class Cargo extends AppModel
{
    public $name = 'Cargo';
    public $primaryKey = 'codcargo';
/**
 * Use database config
 *
 * @var string
 */
    public $useDbConfig = 'comerdepa';
/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'cargos';
/*
 *
 * */
    public $hasMany = array('Cargoproducto' =>
                         array('className'   => 'Cargoproducto',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'codcargo',
                         ),
                  );			

    function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
            switch ($opcion) {
                    case 'Cuadrediario':
                    $texto = '';
                    $condicion = "  Cargo.fecha>='".$datos['Cuadrediario']['fecha']." 00:00:00' and Cargo.fecha<='".$datos['Cuadrediario']['fecha']." 23:59:59' ";
                    $valores = $this->find('all',array('order'=>'Cargo.codcargo','conditions'=>$condicion,'recursive'=>-1));

                    foreach ($valores as $reg) {
                            $fila = $reg['Cargo'];

                            $texto = $texto."INSERT INTO concargos ( codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus) values (";
                            $texto = $texto."".$fila['codcargo'].",".$fila['codsucursal'].",'".$fila['descripcion']."','".$fila['fecha']."','".$fila['autorizado']."','".$fila['responsable']."','".$fila['hora']."','".$fila['estatus']."');";
                            //$texto = $texto.'\n';
                    }	
                    break;				
                    case 'Mcuadrediario':
                    $texto = '';
                    $condicion = "  Cargo.fecha>='".$datos['Mcuadrediario']['fecha']."' and Cargo.fecha<='".$datos['Mcuadrediario']['fecha']."' ";
                    $valores = $this->find('all',array('order'=>'Cargo.codcargo','conditions'=>$condicion,'recursive'=>-1));

                    foreach ($valores as $reg) {
                            $fila = $reg['Cargo'];

                            $texto = $texto."INSERT INTO concargos ( codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus) values (";
                            $texto = $texto."".$fila['codcargo'].",".$fila['codsucursal'].",'".$fila['descripcion']."','".$fila['fecha']."','".$fila['autorizado']."','".$fila['responsable']."','".$fila['hora']."','".$fila['estatus']."');";
                            //$texto = $texto.'\n';
                    }	
                    break;				
            }	
            return $texto;
    }

    function ajustarDatos($opcion='pdf',$datos=array()){
        switch ($opcion) {
        case 'pdf':
            $datos['fechadesde']=$datos['desde'];
            $datos['fechahasta']=$datos['hasta'];
        break;
        }
        return $datos;
     }   
	
    function listadoCargo($datos=array(),$criteria=''){
        // print_r($datos); die();
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);        
        $criteria=" V.fecha >= '".$desde."' ";
        $criteria=$criteria." and V.fecha <= '".$hasta."' ";			
        /*$c_dpto=$this->construir_or($datos['dpto'],'Departamento.coddepartamento');
        $criteria=$criteria."  ".$c_dpto;*/			
        //echo $criteria;
        $sql="select  V.codcargo,V.codsucursal,V.descripcion, V.fecha,V.hora, V.responsable, V.autorizado, V.estatus, V.coddepartamento, V.dptonombre, V.dptocod, V.codproducto, V.codigo, V.nombre, V.dptoid, V.unidad, V.cantidad,V.costounitario as costo from
		(
		select CG.codcargo,CG.codsucursal,CG.descripcion, CG.fecha,CG.hora, CG.responsable,CG.autorizado, CG.estatus,
		DP.coddepartamento, DP.descripcion as dptonombre, DP.codigo as dptocod,
		PR.codproducto, PR.codigo, PR.nombre, DP.coddepartamento as dptoid, PC.unidad, 
		PC.cantidad,PC.costounitario 
		from cargos CG
		inner join productoscargos PC on (PC.codcargo = CG.codcargo and  PC.codsucursal = CG.codsucursal)
		inner join producto PR on (PR.codproducto= PC.codproducto)
		inner join departamentos DP on (DP.coddepartamento= PR.coddepartamento)
		order by PC.codcargo, PC.codsucursal
		) as V
        where ".$criteria."
        order by V.codcargo,V.codsucursal,V.codigo";
        $data = $this->query($sql);
        return $data;
    }
    
    function listadoCargoResumen($datos=array()){
        // print_r($datos); die();
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);        
        $criteria=" V.fecha >= '".$desde."' ";
        $criteria=$criteria." and V.fecha <= '".$hasta."' ";			
        
        $sql="select  V.codcargo,V.codsucursal,V.descripcion, V.fecha::date as fecha, V.responsable, V.autorizado, V.estatus, sum(V.cantidad) as cantidad, sum(V.cantidad * V.costounitario) as total	from
		(
		select CG.codcargo,CG.codsucursal,CG.descripcion, CG.fecha,CG.hora, CG.responsable,CG.autorizado, CG.estatus, DP.coddepartamento, DP.descripcion as dptonombre, DP.codigo as dptocod, PR.codproducto, PR.codigo, PR.nombre, DP.coddepartamento as dptoid, PC.unidad,  PC.cantidad,PC.costounitario 
		from cargos CG
		inner join productoscargos PC on (PC.codcargo = CG.codcargo and  PC.codsucursal = CG.codsucursal)
		inner join producto PR on (PR.codproducto= PC.codproducto)
		inner join departamentos DP on (DP.coddepartamento= PR.coddepartamento)
		order by PC.codcargo, PC.codsucursal
		) as V
        where ".$criteria."
        group by V.codcargo,V.codsucursal,V.descripcion, V.fecha::date, V.responsable, V.autorizado, V.estatus
        order by V.codcargo,V.codsucursal";
        $data = $this->query($sql);
        return $data;
    }
				  
}
?>
