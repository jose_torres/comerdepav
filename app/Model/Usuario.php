<?php
class Usuario extends AppModel
{
    public $name = 'Usuario';
    public $useTable = 'usuario';

    public $belongsTo  = array('Empleado' =>
                           array('className'  => 'Empleado',
                                 'conditions' => '',
                                 'order'      => '',
                                 'foreignKey' => 'empleado_id'
                           ),
						   'Perfile' =>
                           array('className'  => 'Perfile',
                                 'conditions' => '',
                                 'order'      => '',
                                 'foreignKey' => 'perfil_id'
                           )
						   );

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Usuarios registrados
		$order="Usuario.usuario ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Usuario'];	
			$var[$relleno.$item['id']] = $item['usuario'].'=>'. $row['Empleado']['nombre'];
		}
		break;
		case 1://Muestra Todos los Usuarios Permiso de Revisar
		$order="Usuario.usuario ASC";
		$criteria=" Empleado.revisar='SI' ";
		$datos = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Usuario'];	
			$var[$relleno.$item['id']] = $item['usuario'].'=>'. $row['Empleado']['nombre'];
		}
		break;
		case 2://Muestra Todos los Usuarios Permiso de Aprobar
		$order="Usuario.usuario ASC";
		$criteria=" Empleado.aprobar='SI' ";
		$datos = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Usuario'];	
			$var[$relleno.$item['id']] = $item['usuario'].'=>'. $row['Empleado']['nombre'];
		}
		break;	
		case 3://Muestra Todos los Usuarios Permiso de Publicar
		$order="Usuario.usuario ASC";
		$criteria=" Empleado.publicar='SI' ";
		$datos = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Usuario'];	
			$var[$relleno.$item['id']] = $item['usuario'].'=>'. $row['Empleado']['nombre'];
		}
		break;					
		}//Cierre de switch						
		return $var;
	}

	function obtenerDireccion($datos=array()){
		$direccion = '/usuarios/home';
		if($datos['Perfile']['redireccion']!=''){
			$direccion = $datos['Perfile']['redireccion'];
		}
		return $direccion;
	}

	function obtenerNroUsuario(){
		$datos = $this->find('count',array('recursive'=>1));
		return $datos;
	}

}
?>
