<?php
class Movbancario extends AppModel {

    public $name = 'Movbancario';
    public $useDbConfig = 'comerdepa';
    //The Associations below have been created with all possible keys, those that are not needed can be removed
    public $belongsTo = array(
            'Banco' => array(
                    'className' => 'Banco',
                    'foreignKey' => 'banco_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
            ),
            'Documentotipo' => array(
                    'className' => 'Documentotipo',
                    'foreignKey' => 'documentotipo_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
            ),
            'Cuentasbancaria' => array(
                    'className' => 'Cuentasbancaria',
                    'foreignKey' => 'cuentasbancaria_id',
                    'conditions' => '',
                    'fields' => '',
                    'order' => ''
            )
    );

    function ajustarDatos($opcion='add',$datos){
            switch ($opcion) {
            case 'add':
                    $datos['Movbancario']['banco_id']=$datos['banco_id'];
                    $datos['Movbancario']['cuentasbancaria_id']=$datos['cuentabancaria_id'];
                    $datos['Movbancario']['codsucursal']=$datos['codsucursal'];
                    $datos['Movbancario']['nrodocumento']=$datos['nrodocumento'];
                    $datos['Movbancario']['fecha']=$datos['cuadre_fecha'];
                    $datos['Movbancario']['cuadrediario_id']=$datos['cuadre_id'];
                    $datos['Movbancario']['documentotipo_id']=$datos['documentotipo_id'];
                    $datos['Movbancario']['monto']=$datos['totalDeposito'];
                    $datos['Movbancario']['tipodeposito']=strtoupper($datos['opcion']);
                    $datos['Movbancario']['observacion']=' Deposito de la Sucursal nro:'.$datos['codsucursal'].' a la Fecha:'.$datos['cuadre_fecha'];					
            break;
            case 'addDeb':
                    $datos['Movbancario']['banco_id']=$datos['banco_id1'];
                    $datos['Movbancario']['cuentasbancaria_id']=$datos['cuentasbancaria_id1'];
                    $datos['Movbancario']['codsucursal']=$datos['codsucursal'];
                    $datos['Movbancario']['nrodocumento']=$datos['nrodocumento'];
                    $datos['Movbancario']['fecha']=$datos['cuadre_fecha'];
                    $datos['Movbancario']['cuadrediario_id']=$datos['cuadre_id'];
                    $datos['Movbancario']['documentotipo_id']=$datos['documentotipo_id'];
                    $datos['Movbancario']['monto']=$datos['totalDeposito'];
                    $datos['Movbancario']['tipodeposito']=strtoupper($datos['opcion']);
                    $datos['Movbancario']['observacion']=' Deposito de la Sucursal nro:'.$datos['codsucursal'].' a la Fecha:'.$datos['cuadre_fecha'];

            break;
            case 'edit':
                    $datos['Movbancario']['id']=$datos['id'];
                    $datos['Movbancario']['banco_id']=$datos['banco_id'];
                    $datos['Movbancario']['cuentasbancaria_id']=$datos['cuentabancaria_id'];
                    $datos['Movbancario']['codsucursal']=$datos['codsucursal'];
                    $datos['Movbancario']['nrodocumento']=$datos['nrodocumento'];
                    $datos['Movbancario']['fecha']=$datos['cuadre_fecha'];
                    $datos['Movbancario']['cuadrediario_id']=$datos['cuadre_id'];
                    $datos['Movbancario']['documentotipo_id']=$datos['documentotipo_id'];
                    $datos['Movbancario']['monto']=$datos['totalDeposito'];
                    $datos['Movbancario']['observacion']=' Deposito de la Sucursal nro:'.$datos['codsucursal'].' a la Fecha:'.$datos['cuadre_fecha'];
            break;

            }
            return $datos;
    }
	
    function arreglar_saldos($id,$resultado,$cuenta,$par){
            $datos=$this->query("select * from movbancarios where ctabanco_id=".$cuenta." order by fecha,id asc");
            $sal=$resultado;
            foreach ($datos as $registros){
                    if ($par==1) {
                            $sal=$resultado;
                            $datos=$this->query("update movbancarios set saldo = ".$sal." where id=".$id);
                            $par=0;
                    }else{
                            $v_id = $registros[0]['id'];
                            $tipodoc = $registros[0]['documentotipo_id'];
                            $documentotipo=$this->query("select * from documentotipo where id=".$tipodoc);
                            //print_r($documentotipo);
                            $accion = $documentotipo[0][0]['accion'];
                            $mon=$registros[0]['monto'];
                            if ($accion=="DISMINUYE") {
                                    $resultado = $sal - $mon;
                            }else{
                                    $resultado = $sal + $mon;
                            }
                            $sal=$resultado;
                            $datos=$this->query("update movbancarios set saldo = ".$sal." where id=".$v_id);
                    }
            }
    }
	
    function guardar_seleccionados($fecha,$datos){
    //print_r($fecha);
    //print_r($datos);
            foreach ($datos as $registros){
                    $v_id = $registros['id'];
                    $v_chequeo = $registros['chequeo'];
                    if ($v_chequeo==1) {
                            $datos=$this->query("update movbancarios set conciliado = 'S', fconciliacion = '".$fecha."' where id=".$v_id);
                    }
            }
    }

    function guardarmontousuario($monto,$ctabanco_id) {
            $datos=$this->query("update ctabancos set usuariomonto =".$monto." where id=".$ctabanco_id);
    }

    function estatus_cheque($cheque) {
            $datos=$this->query("update ctrolchequeras set estatus = 'G' where correlativo='".$cheque."'");
    }

    function detalle_mov_ban($concepto,$tipogasto,$detalle,$monto,$documento,$banco,$cuenta,$tipodocumento){
            $datos=$this->query("insert into detallepagos(tipocompragasto_id,monto,concepto,detalle,nrodocumento,banco_id,ctabanco_id,documentotipo_id) values (".$tipogasto.",".$monto.",'".$concepto."', '".$detalle."','".$documento."',".$banco.",".$cuenta.",".$tipodocumento.")");
    }

    function eliminardetalle_mov_ban($id){
            $datos=$this->query("Delete From detallepagos where id=".$id);
    }

    function reporte($datos=array(),$criteria=array(),$opcion=0){

            switch ($opcion) {
            case 0:
            $datos['desde_c']=$this->anomesdia($datos['desde']);
            $datos['hasta_c']=$this->anomesdia($datos['hasta']);
            $criteria=" Movbancario.fecha >= '".$datos['desde_c']."' ";
            $criteria=$criteria." and Movbancario.fecha <= '".$datos['hasta_c']."' ";
            if(isset($datos['selperiodo'])){
            if ($datos['selperiodo']=='true' or $datos['selperiodo']==1){
                    $criteria=" Movbancario.fecha >= '".$datos['desde_c']."' ";
                    $criteria=$criteria." and Movbancario.fecha <= '".$datos['hasta_c']."' ";
            }else{
                    $criteria=" EXTRACT('month' from Movbancario.fecha) = '".$datos['mesconsulta']."' ";
                    $criteria=$criteria." and EXTRACT('year' from Movbancario.fecha) = '".$datos['anoconsulta']."' ";
            }
            }
            if (isset($datos['descorta'])){
                    $criteria=$criteria." and Documentotipo.descorta like '".$datos['descorta']."%'";
            }
            if ($datos['banco']!='' ){
                    $criteria=$criteria." and Movbancario.banco_id=".$datos['banco'];
            }

            if ($datos['lote']!='' ){
                    $data = $this->find('all',array('fields'=>' Movbancario.nrodocumento,Movbancario.fecha, Movbancario.banco_id,
     Banco.descripcion, Movbancario.nombeneficiario,Movbancario.monto','conditions'=>$criteria,'order'=>' Banco.descripcion,Movbancario.fecha'));

            }else{
                    $sql="select Movbancario.banco_id,Banco.descripcion,count(Movbancario.banco_id) as cant,sum(Movbancario.monto) as Total from movbancarios as Movbancario, bancos as Banco,documentotipo as Documentotipo where Banco.id=Movbancario.banco_id and Movbancario.documentotipo_id=Documentotipo.id and ".$criteria." GROUP BY Movbancario.banco_id,Banco.descripcion order by Banco.descripcion";
                    $data=$this->query($sql);
            //	echo $sql;
            }
            break;
            case 1://Usado para la busqueda
                    $datos['desde_c']=$this->anomesdia($datos['desde']);
                    $datos['hasta_c']=$this->anomesdia($datos['hasta']);
                    $criteria=" Movbancario.fecha >= '".$datos['desde_c']."' ";
                    $criteria=$criteria." and Movbancario.fecha <= '".$datos['hasta_c']."' ";
                    if(isset($datos['selperiodo'])){
                    if ($datos['selperiodo']=='true' or $datos['selperiodo']==1){
                            $criteria=" Movbancario.fecha >= '".$datos['desde_c']."' ";
                            $criteria=$criteria." and Movbancario.fecha <= '".$datos['hasta_c']."' ";
                    }else{
                            $criteria=" EXTRACT('month' from Movbancario.fecha) = '".$datos['mesconsulta']."' ";
                            $criteria=$criteria." and EXTRACT('year' from Movbancario.fecha) = '".$datos['anoconsulta']."' ";
                    }
                    }
                    if ($datos['cuenta']!='' ){
                    $criteria=$criteria." and Movbancario.ctabanco_id=".$datos['cuenta'];
                    }
                    $orden="Movbancario.fecha,Movbancario.id";
                    $campo="Movbancario.id,Movbancario.fecha,Documentotipo.descorta,Movbancario.nombeneficiario,Movbancario.saldo,Movbancario.monto,Movbancario.nrodocumento,Documentotipo.accion,Movbancario.conciliado";
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $orden,'fields'=>$campo));
            break;				
            }//Cierre de switch

            return $data;
    }

    function notadebitoporcomision($banco,$cuenta,$documento,$comprobante,$fecha,$beneficiario,$nombre,$v_monto,$saldo,$observacion,$conciliado,$tipodocumento,$docasociado){
            $datos=$this->query("insert into movbancarios(banco_id,ctabanco_id,nrodocumento,nrocomprobante,fecha,cedbeneficiario,nombeneficiario,monto,saldo,observacion,conciliado,documentotipo_id,docasociado) values (".$banco.",".$cuenta.",'".$documento."','".$comprobante."','".$fecha."','".$beneficiario."','".$nombre."',".$v_monto.",".$saldo.",'".$observacion."','".$conciliado."',".$tipodocumento.",'".$docasociado."')");
    }

    function mayuscula_beneficiarios() {
            $datos=$this->query("update movbancarios set nrodocumento = upper(nrodocumento), cedbeneficiario = upper(cedbeneficiario), nombeneficiario = upper(nombeneficiario), observacion = upper(observacion), impreanombrede = upper(impreanombrede), leyenda2 = upper(leyenda2), leyenda1 = upper(leyenda1)");
    }
	
    function salvar_docasociado($iddocasociado,$estatusconciliado,$monto,$observacion,$fecha,$nombeneficiario){
            $datos=$this->query("update movbancarios set conciliado = '".$estatusconciliado."', monto =".$monto.", fecha='".$fecha."', nombeneficiario='".$nombeneficiario."', observacion='".$observacion."' where id=".$iddocasociado);
    }

    function actualizartarjetas($id){
            $datos=$this->query("update tarjetas set movbancario_id = 0 where movbancario_id=".$id);
    }

    function actualizarboletas($id){
            $datos=$this->query("update boletas set movbancario_id = 0 where movbancario_id=".$id);
    }

    function actualizardepositos($id){
            $datos=$this->query("delete from depositos where movbancario_id=".$id);
    }

    function actualizarcheques($id){
            $datos=$this->query("update cheques set movbancario_id = 0 where movbancario_id=".$id);
    }

    function actualizartarjetas_cxc($id){
            $datos=$this->query("update cctarjetas set movbancario_id = 0 where movbancario_id=".$id);
    }

    function actualizardepositos_cxc($id){
            $datos=$this->query("delete from ccdepositos where movbancario_id=".$id);
    }

    function actualizarcheques_cxc($id){
            $datos=$this->query("update cccheques set movbancario_id = 0 where movbancario_id=".$id);
    }

    function actualizartransferencia_cxc($id){
            $datos=$this->query("update cctrans set movbancario_id = 0 where movbancario_id=".$id);
    }

    function eliminar_asociado($id,$mov_padre){

            $criterio=" Movbancario.banco_id=".$mov_padre['Movbancario']['banco_id'];
            $criterio=$criterio." and Movbancario.ctabanco_id=".$mov_padre['Movbancario']['ctabanco_id'];		
            $criterio=$criterio." and Movbancario.documentotipo_id=5";
            $criterio=$criterio." and trim(Movbancario.docasociado)=trim('".$mov_padre['Movbancario']['nrodocumento']."')";

            $orden="Movbancario.fecha,Movbancario.id ASC";
            $reg=$this->find('count',array('conditions'=>$criterio));
            if($reg>0){
                    $mov_hijo = $this->find('first',array('conditions'=>$criterio,'order'=> $orden));
                    $this->delete($mov_hijo['Movbancario']['id']);
            }
    }

    function arreglar_saldos_2($ctabanco_id=0) {
            $datos=$this->query("select * from saldos_1(".$ctabanco_id.") AS ( id bigint,
fecha date,descorta text,accion text,entradas double precision,salidas double precision,saldof numeric)");
            return $datos;
    }

    function buscarDepositos($cuadre='',$opcion = 0){
        switch ($opcion) {
            case 0:
            $depositos['Efectivo'] = $this->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$cuadre['Cuadrediario']['fecha']."' and Movbancario.tipodeposito='EFECTIVO'"));
            $depositos['Debito'] = $this->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$cuadre['Cuadrediario']['fecha']."' and Movbancario.tipodeposito='DEBITO'"));
            $depositos['Cheque'] = $this->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$cuadre['Cuadrediario']['fecha']."' and Movbancario.tipodeposito='CHEQUE'"));
            $depositos['Transfer'] = $this->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$cuadre['Cuadrediario']['fecha']."' and Movbancario.tipodeposito='TRANSFER'"));
            break;
            case 1:
		$depositos['Efectivo'] = $this->find('all',array('order'=>'Movbancario.id desc','conditions'=>" Movbancario.fecha='".$cuadre['Cuadrediario']['fecha']."' "));		
            break;
        }//Cierre de switch
        return $depositos;

    }

    function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){

            switch ($opcion) {
                    case 'Cuadrediario':
                    $texto = '';
                    $condicion = "  Movbancario.fecha='".$datos['Cuadrediario']['fecha']."'";
                    $valores = $this->find('all',array('order'=>'Movbancario.id','conditions'=>$condicion,'recursive'=>-1));

                    foreach ($valores as $reg) {
                            $fila = $reg['Movbancario'];
                            if($fila['saldo']=='' || $fila['saldo']==null){
                                    $fila['saldo']=0;
                            }
                            if($fila['chequeo']=='' || $fila['chequeo']==null){
                                    $fila['chequeo']='false';
                            }
                            $texto = $texto."INSERT INTO conmovbancarios (id_sucursal, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito) values (";
                            $texto = $texto."".$fila['id'].",".$fila['banco_id'].",".$fila['cuentasbancaria_id'].",".$fila['codsucursal'].",'".$fila['nrodocumento']."','".$fila['nrocomprobante']."','".$fila['fecha']."','".$fila['cedbeneficiario']."','".$fila['nombeneficiario']."',".$fila['monto'].",".$fila['saldo'].",'".$fila['observacion']."','".$fila['created']."','".$fila['modified']."','".$fila['conciliado']."',".$fila['documentotipo_id'].",".$fila['chequeo'].",'".$fila['created']."','".$fila['impreanombrede']."','".$fila['leyenda1']."','".$fila['leyenda2']."','".$fila['docasociado']."',".$fila['montoref'].",".$fila['cuadrediario_id'].",'".$fila['tipodeposito']."');";
                            //$texto = $texto.'\n';
                    }
            break;
            case 'Mcuadrediario':
            $texto = '';
            $condicion = "  Movbancario.fecha='".$datos['Mcuadrediario']['fecha']."'";
            $valores = $this->find('all',array('order'=>'Movbancario.id','conditions'=>$condicion,'recursive'=>-1));

            foreach ($valores as $reg) {
                    $fila = $reg['Movbancario'];
                    if($fila['saldo']=='' || $fila['saldo']==null){
                            $fila['saldo']=0;
                    }
                    if($fila['chequeo']=='' || $fila['chequeo']==null){
                            $fila['chequeo']='false';
                    }
                    $texto = $texto."INSERT INTO conmovbancarios (id_sucursal, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito) values (";
                    $texto = $texto."".$fila['id'].",".$fila['banco_id'].",".$fila['cuentasbancaria_id'].",".$fila['codsucursal'].",'".$fila['nrodocumento']."','".$fila['nrocomprobante']."','".$fila['fecha']."','".$fila['cedbeneficiario']."','".$fila['nombeneficiario']."',".$fila['monto'].",".$fila['saldo'].",'".$fila['observacion']."','".$fila['created']."','".$fila['modified']."','".$fila['conciliado']."',".$fila['documentotipo_id'].",".$fila['chequeo'].",'".$fila['created']."','".$fila['impreanombrede']."','".$fila['leyenda1']."','".$fila['leyenda2']."','".$fila['docasociado']."',".$fila['montoref'].",".$fila['cuadrediario_id'].",'".$fila['tipodeposito']."');";
                    //$texto = $texto.'\n';
            }
            break;				
            }	

            return $texto;
    }

    function ajustarRegistrosCuadre($opcion='Movbancario_Efectivo',$cuadre_id='',$registros=array(),$codsucursal=1){
            $datos['Movbancario'] = array();
            switch ($opcion) {
            case 'Movbancario_Efectivo':
                    $datos_cuadre = $this->read(null, $cuadre_id); $i = 1;
                    foreach ($registros as $row){
                            $nro_reg = $this->find('count',array('conditions'=>" trim(Movbancario.nrocomprobante)::integer = ".$row[0]['coddepbanco']." " ));
                            if($nro_reg<=0){
                            $datos['Movbancario'][$i]['cuadrediario_id']=$cuadre_id;
                            $datos['Movbancario'][$i]['cuentasbancaria_id'] = $row[0]['codcuenta'];
                            $datos['Movbancario'][$i]['banco_id'] = $row[0]['codbanco'];
                            $datos['Movbancario'][$i]['nrodocumento'] = $row[0]['nropago'];
                            $datos['Movbancario'][$i]['nrocomprobante'] = $row[0]['coddepbanco'];
                            $datos['Movbancario'][$i]['fecha'] = $row[0]['fecha'];
                            $datos['Movbancario'][$i]['monto'] = $row[0]['monto_pagado'];
                            $datos['Movbancario'][$i]['tipodeposito'] = 'EFECTIVO';
                            $datos['Movbancario'][$i]['conciliado']='N';
                            $datos['Movbancario'][$i]['documentotipo_id']=1;				
                            $datos['Movbancario'][$i]['codsucursal']=$codsucursal;				
                            $i= $i + 1;
                            }

                    }
                    if(count($datos['Movbancario'])>0){
                            $this->saveAll($datos['Movbancario']);
                    }
            break;
            case 'Movbancario_Debito':
                    $datos_cuadre = $this->read(null, $cuadre_id); $i = 1;
                    foreach ($registros as $row){
                            $nro_reg = $this->find('count',array('conditions'=>" trim(Movbancario.nrocomprobante)::integer = ".$row[0]['coddepbanco']." " ));
                            if($nro_reg<=0){
                            $datos['Movbancario'][$i]['cuadrediario_id']=$cuadre_id;
                            $datos['Movbancario'][$i]['cuentasbancaria_id'] = $row[0]['codcuenta'];
                            $datos['Movbancario'][$i]['banco_id'] = $row[0]['codbanco'];
                            $datos['Movbancario'][$i]['nrodocumento'] = $row[0]['nropago'];
                            $datos['Movbancario'][$i]['nrocomprobante'] = $row[0]['coddepbanco'];
                            $datos['Movbancario'][$i]['fecha'] = $row[0]['fecha'];
                            $datos['Movbancario'][$i]['monto'] = $row[0]['monto_pagado'];
                            $datos['Movbancario'][$i]['tipodeposito'] = 'DEBITO';
                            $datos['Movbancario'][$i]['conciliado']='N';
                            $datos['Movbancario'][$i]['documentotipo_id']=2;
                            $datos['Movbancario'][$i]['codsucursal']=$codsucursal;								
                            $i= $i + 1;
                            }

                    }
                    if(count($datos['Movbancario'])>0){
                            $this->saveAll($datos['Movbancario']);
                    }
            break;	
            case 'Movbancario_Transfer':
                    $datos_cuadre = $this->read(null, $cuadre_id); $i = 1;
                    foreach ($registros as $row){
                            $nro_reg = $this->find('count',array('conditions'=>" trim(Movbancario.nrocomprobante)::integer = ".$row[0]['coddepbanco']." " ));
                            if($nro_reg<=0){
                            $datos['Movbancario'][$i]['cuadrediario_id']=$cuadre_id;
                            $datos['Movbancario'][$i]['cuentasbancaria_id'] = $row[0]['codcuenta'];
                            $datos['Movbancario'][$i]['banco_id'] = $row[0]['codbanco'];
                            $datos['Movbancario'][$i]['nrodocumento'] = $row[0]['nropago'];
                            $datos['Movbancario'][$i]['nrocomprobante'] = $row[0]['coddepbanco'];
                            $datos['Movbancario'][$i]['fecha'] = $row[0]['fecha'];
                            $datos['Movbancario'][$i]['monto'] = $row[0]['monto_pagado'];
                            $datos['Movbancario'][$i]['tipodeposito'] = 'TRANSFER';
                            $datos['Movbancario'][$i]['conciliado']='N';
                            $datos['Movbancario'][$i]['documentotipo_id']=2;
                            $datos['Movbancario'][$i]['codsucursal']=$codsucursal;								
                            $i= $i + 1;
                            }

                    }
                    if(count($datos['Movbancario'])>0){
                            $this->saveAll($datos['Movbancario']);
                    }
            break;	
            case 'Movbancario_Cheque':
                    $datos_cuadre = $this->read(null, $cuadre_id); $i = 1;
                    foreach ($registros as $row){
                            $nro_reg = $this->find('count',array('conditions'=>" trim(Movbancario.nrocomprobante)::integer = ".$row[0]['coddepbanco']." " ));
                            if($nro_reg<=0){
                            $datos['Movbancario'][$i]['cuadrediario_id']=$cuadre_id;
                            $datos['Movbancario'][$i]['cuentasbancaria_id'] = $row[0]['codcuenta'];
                            $datos['Movbancario'][$i]['banco_id'] = $row[0]['codbanco'];
                            $datos['Movbancario'][$i]['nrodocumento'] = $row[0]['nropago'];
                            $datos['Movbancario'][$i]['nrocomprobante'] = $row[0]['coddepbanco'];
                            $datos['Movbancario'][$i]['fecha'] = $row[0]['fecha'];
                            $datos['Movbancario'][$i]['monto'] = $row[0]['monto_pagado'];
                            $datos['Movbancario'][$i]['tipodeposito'] = 'CHEQUE';
                            $datos['Movbancario'][$i]['conciliado']='N';
                            $datos['Movbancario'][$i]['documentotipo_id']=1;
                            $datos['Movbancario'][$i]['codsucursal']=$codsucursal;								
                            $i= $i + 1;
                            }

                    }
                    if(count($datos['Movbancario'])>0){
                            $this->saveAll($datos['Movbancario']);
                    }
            break;	
            }
            return $datos;
    }
}
?>
