<?php
class Sucdeposito extends AppModel {

	public $name = 'Sucdeposito';
	public $useDbConfig = 'comerdepa';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		/*'Efectivo' => array(
			'className' => 'Efectivo',
			'foreignKey' => 'efectivo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),*/
		'Movbancario' => array(
			'className' => 'Movbancario',
			'foreignKey' => 'movbancario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	function ajustarDatos($opcion='add',$datos){
		switch ($opcion) {
		case 'add':
			
			$cont=0;
			for ($i = 1; $i <= $datos['total_reg']; $i++) {
				$datos['Sucdeposito'][$i]['movbancario_id']=$datos['movbancario_id'];
				$datos['Sucdeposito'][$i]['codsucursal']=$datos['codsucursal'];
				$datos['Sucdeposito'][$i]['fecha']=$datos['cuadre_fecha'];
				$datos['Sucdeposito'][$i]['cuadrediario_id']=$datos['cuadre_id'];
				$datos['Sucdeposito'][$i]['caja_id']=1;
				$datos['Sucdeposito'][$i]['valor']=$datos['efecvalor'.$i];
				$datos['Sucdeposito'][$i]['cantidad']=$datos['efeccantidad'.$i];
				$datos['Sucdeposito'][$i]['monto']=$datos['efeccantidad'.$i]*$datos['efecvalor'.$i];				
			}
			
		break;
		case 'edit':
			$datos['Retencioncompra']['mesafecta']=$datos['Retencioncompra']['mes']['month'];
			$datos['Retencioncompra']['anioafecta']=$datos['Retencioncompra']['anio']['year'];
			$datos['Retencioncompra']['horaemision']=date('h:i:s');
			$datos['Retencioncompra']['fechaemision']=$this->anomesdia($datos['Retencioncompra']['fecha']);
			$datos['Retencioncompra']['numeroant']=$datos['Retencioncompra']['numero'];
			$datos['Retencioncompra']['numero']=$this->get_numero($datos);
		break;
		}
		return $datos;
	}

	function obtenerDepositos($fecha=''){
		$sucursal = 0;
		$sql = "select distinct codsucursal from sucdepositos where fecha='".$fecha."'";
		$data = $this->query($sql);
		if (isset($data[0][0]['codsucursal'])){
			$sucursal = $data[0][0]['codsucursal'];
		}
		return $sucursal;
	}

}
?>
