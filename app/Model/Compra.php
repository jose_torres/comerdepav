<?php
class Compra extends AppModel
{
    public $name = 'Compra';
    public $primaryKey = 'codcompra';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'compras';
/*
 *
 * */
	public $belongsTo = array('Proveedore' => array('className' => 'Proveedore',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codproveedor',
						'fields'=> 'Proveedore.codproveedor,Proveedore.rif,Proveedore.descripcion,Proveedore.direccion,Proveedore.diasdecredito'
					  ),
					  'Retencioncompra' => array('className' => 'Retencioncompra',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codcompra',
						'fields'=> ''
					  )				  					  
			);
	

	function reporte($datos=array(),$criteria='',$opcion=0){
		if (isset($datos['fechahasta']))
		$hasta=$this->anomesdia($datos['fechahasta']);
		if (isset($datos['fechadesde']))
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$order='Compra.estatus, Compra.codcompra';
			$criteria=" Compra.femision >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Compra.femision <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['provee'],'Compra.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Compra.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>0));
		break;		
		case 1:			
			$order=' PAGOS_VENTAS.estatus,PAGOS_VENTAS.descripcion,PAGOS_VENTAS.femision';
			$criteria=" PAGOS_COMPRAS.femision >= '".$desde." ' ";
			$criteria=$criteria." and PAGOS_COMPRAS.femision <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['provee'],'PAGOS_COMPRAS.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
			$c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_COMPRAS.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select PAGOS_COMPRAS.codcompra,PAGOS_COMPRAS.nrodocumento, PAGOS_COMPRAS.codproveedor, PAGOS_COMPRAS.rif,PAGOS_COMPRAS.descripcion, PAGOS_COMPRAS.fecha, PAGOS_COMPRAS.baseimp2,PAGOS_COMPRAS.ivaimp2, PAGOS_COMPRAS.retencion,  PAGOS_COMPRAS.femision, PAGOS_COMPRAS.tipomovimiento, PAGOS_COMPRAS.MontoPago, PAGOS_COMPRAS.estatus from (
			select VT.codcompra,VT.nrodocumento, VT.codproveedor,CT.rif,CT.descripcion, VT.femision as Fecha, (VT.baseimp1+VT.baseimp2+VT.baseimp3) as baseimp2,(VT.ivaimp1+VT.ivaimp2+VT.ivaimp3) as ivaimp2, VT.retencion,VT.femision,'1COMPRAS'::text AS tipomovimiento, 0 as MontoPago, VT.estatus from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			union all
			select VT.codcompra,CAP.numero as nrodocumento, VT.codproveedor,CT.rif,CAP.banco as descripcion, 
			CASE WHEN upper(CAP.tipopago)='RETENCION' THEN CAP.fecharetencion 
			WHEN upper(CAP.tipopago)='CAJA' THEN VT.femision  ELSE CAP.fechacobro 
			END as Fecha, (VT.baseimp1+VT.baseimp2+VT.baseimp3) as baseimp2, (VT.ivaimp1+VT.ivaimp2+VT.ivaimp3) as ivaimp2,VT.retencion,VT.femision,'2PAGOS-'||upper(CAP.tipopago) AS tipomovimiento, 
			 CAP.Monto as MontoPago, VT.estatus
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cppagadas  CCP on (CCP.codmovimientocp=VT.codcompra)
			inner join cpabonopago CAP on (CAP.codcpabono=CCP.codmovimientopago)
			union all
			select VT.codcompra, cast(DC.coddevolucioncompra as text) as nrodocumento, VT.codproveedor,CT.rif,
			'DEVOLUCION EN COMPRA' as descripcion, 
			DC.fdevolucion as Fecha,
			(VT.baseimp1+VT.baseimp2+VT.baseimp3) as baseimp2, (VT.ivaimp1+VT.ivaimp2+VT.ivaimp3) as ivaimp2, VT.retencion,VT.femision,'3'||'DEVOLUCION'::text AS tipomovimiento,
			  DC.montobruto as MontoPago, VT.estatus
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join devolucioncompra DC on (DC.codcompra=VT.codcompra)
			union all
			select VT.codcompra, cast(NC.codcpnotacredito as text) as nrodocumento, VT.codproveedor,CT.rif,
			NC.concepto as descripcion, 
			NC.fecha as Fecha, (VT.baseimp1+VT.baseimp2+VT.baseimp3) as baseimp2, (VT.ivaimp1+VT.ivaimp2+VT.ivaimp3) as ivaimp2,VT.retencion,VT.femision,'4'||'NOTA DE CREDITO'::text AS tipomovimiento,
			  NC.monto*-1 as MontoPago, VT.estatus
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cpnotacredito NC on (NC.nrodocumento=VT.nrodocumento and NC.codproveedor=VT.codproveedor)
			) PAGOS_COMPRAS
			where 1=1 and ".$criteria."
ORDER BY PAGOS_COMPRAS.estatus,PAGOS_COMPRAS.codcompra,PAGOS_COMPRAS.tipomovimiento;";
			$data = $this->query($sql);
		break;
		case 2:			
			$order=' PAGOS_VENTAS.estatus,PAGOS_VENTAS.descripcion,PAGOS_VENTAS.femision';
			$criteria=" 1=1 ";
			//$criteria=$criteria." and PAGOS_COMPRAS.femision <= '".$hasta."' ";			
			//echo $criteria; .$datos['codproveedor'].
			$sql="SELECT A.codmovimiento,A.tipomovimiento,A.monto - COALESCE(B.monto,0) AS vmonto,NroDocumento,concepto,fvencimiento,femision,A.codsucursal FROM (SELECT A.codproveedor,codcompra AS codmovimiento,'COMPRAS' AS tipomovimiento,nrodocumento,femision,A.codsucursal FROM compras A WHERE A.codproveedor = '".$datos['codproveedor']."' Union SELECT A.codproveedor,codcpfactura AS codmovimiento, 'FA' AS tipomovimiento, nrodocumento,fecha AS femision,A.codsucursal FROM cpfactura A WHERE A.codproveedor = '".$datos['codproveedor']."' Union SELECT A.codproveedor,codcpnotadebito AS codmovimiento,'ND' AS tipomovimiento,nrodocumento,fecha AS femision,A.codsucursal FROM cpnotadebito A WHERE A.codproveedor = '".$datos['codproveedor']."') AS C Inner Join cuentasporpagar a LEFT JOIN (SELECT sum(monto) AS monto,codmovimientocp,tipomovimientocp FROM cppagadas Group By codmovimientocp,tipomovimientocp) AS B ON A.codmovimiento = B.codmovimientocp AND A.tipomovimiento = B.tipomovimientocp ON C.codmovimiento = A.codmovimiento AND C.tipomovimiento = A.tipomovimiento
			where A.codmovimiento not in (select RC.codmovimiento  from retenciones_compras RC where RC.codsucursal=A.codsucursal)
			";
			//echo $sql;
			$data = $this->query($sql);
		break;
		case 3:			
			$order=' PAGOS_VENTAS.estatus,PAGOS_VENTAS.descripcion,PAGOS_VENTAS.femision';
			$criteria=" PAGOS_COMPRAS.fecha >= '".$desde." ' ";
			$criteria=$criteria." and PAGOS_COMPRAS.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['provee'],'PAGOS_COMPRAS.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
			$c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_COMPRAS.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select PAGOS_COMPRAS.codcompra,PAGOS_COMPRAS.nrodocumento, PAGOS_COMPRAS.codproveedor, PAGOS_COMPRAS.rif,PAGOS_COMPRAS.descripcion, PAGOS_COMPRAS.fecha, PAGOS_COMPRAS.baseimp2,PAGOS_COMPRAS.ivaimp2, PAGOS_COMPRAS.retencion,  PAGOS_COMPRAS.femision, PAGOS_COMPRAS.tipomovimiento, PAGOS_COMPRAS.MontoPago, PAGOS_COMPRAS.estatus, PAGOS_COMPRAS.factura, PAGOS_COMPRAS.Proveedor
			from (
			select VT.codcompra,VT.nrodocumento, VT.codproveedor,CT.rif,CT.descripcion, VT.femision as Fecha, VT.baseimp2,VT.ivaimp2, VT.retencion,VT.femision,'1COMPRAS'::text AS tipomovimiento, 0 as MontoPago, VT.estatus,VT.nrodocumento as factura, CT.descripcion as Proveedor
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			union all
			select VT.codcompra,CAP.numero as nrodocumento, VT.codproveedor,CT.rif,CASE WHEN upper(CAP.tipopago)='RETENCION' 
			THEN 'PAGO DE RETENCION' WHEN upper(CAP.tipopago)='CAJA' THEN 'PAGO EN EFECTIVO' ELSE BC.nombre 
			END as descripcion, 
			CASE WHEN upper(CAP.tipopago)='RETENCION' THEN CAP.fecharetencion 
			WHEN upper(CAP.tipopago)='CAJA' THEN VT.femision  ELSE CCP.fecha 
			END as Fecha,			 VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'2PAGOS-'||upper(CAP.tipopago) AS tipomovimiento, CAP.Monto as MontoPago, VT.estatus,VT.nrodocumento as factura, CT.descripcion as Proveedor
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cppagadas  CCP on (CCP.codmovimientocp=VT.codcompra)
			inner join cpabonopago CAP on (CAP.codcpabono=CCP.codmovimientopago)
			left join bancos BC on (BC.abreviatura=CAP.banco)
			/*union all
			select VT.codcompra, cast(DC.coddevolucioncompra as text) as nrodocumento, VT.codproveedor,CT.rif,
			'DEVOLUCION EN COMPRA' as descripcion, 
			DC.fdevolucion as Fecha, VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'3'||'DEVOLUCION'::text AS tipomovimiento, DC.montobruto*-1 as MontoPago, VT.estatus,VT.nrodocumento as factura, CT.descripcion as Proveedor
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join devolucioncompra DC on (DC.codcompra=VT.codcompra)*/
			union all
			select VT.codcompra, cast(NC.codcpnotacredito as text) as nrodocumento, VT.codproveedor,CT.rif, NC.concepto as descripcion, NC.fecha as Fecha,
			VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'4'||'NOTA DE CREDITO'::text AS tipomovimiento, NC.monto*-1 as MontoPago, VT.estatus,VT.nrodocumento as factura, CT.descripcion as Proveedor
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cpnotacredito NC on (NC.nrodocumento=VT.nrodocumento and NC.codproveedor=VT.codproveedor)
			) PAGOS_COMPRAS
			where 1=1 and ".$criteria." 
			ORDER BY PAGOS_COMPRAS.proveedor,PAGOS_COMPRAS.fecha,PAGOS_COMPRAS.tipomovimiento;";
			//echo $sql;
			$data = $this->query($sql);
		break;
		case 4:						
			$criteria=" Compra.femision >= '".$desde." ' ";
			$criteria=$criteria." and Compra.femision <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['provee'],'Compra.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Compra.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select Compra.codcompra, Compra.nrodocumento, Proveedor.rif as Proveedor__rif, Proveedor.descripcion  as Proveedor__descripcion, 
			Compraproducto.costo, Compraproducto.iva*Compraproducto.costo/100 as ivaimp, Compra.estatus,Compra.femision as fecha, 
			Producto.codigo,Producto.nombre,Compraproducto.cantidad, Compraproducto.costodescuento,Compraproducto.unidad,
			(Compraproducto.cantidad*Compraproducto.costodescuento) as TotalCostoDescuento,(Compraproducto.cantidad*Compraproducto.costo) as Total 
			from compras as Compra
			inner join proveedores Proveedor on (Proveedor.codproveedor=Compra.codproveedor)
			inner join comprasproductos Compraproducto on (Compraproducto.codcompra=Compra.codcompra)
			inner join producto Producto on (Compraproducto.codproducto=Producto.codproducto)
			where ".$criteria."
			group by Compra.codcompra, Compra.nrodocumento, Proveedor.rif, Proveedor.descripcion, 
			Compraproducto.costo, Compraproducto.iva, Compra.estatus,Compra.femision, 
			Producto.codigo,Producto.nombre,Compraproducto.cantidad, Compraproducto.costodescuento,Compraproducto.unidad
			order by Compra.codcompra";
			//echo $sql;
			$data = $this->query($sql);
		break;
		
		}
		return $data;
	}

	function calcular_retencion_compras($datos=array()){
		$criteria=" 1>1 ";
		if(count($datos['codmovimientos'])>1 and count($datos['tipomovimientos'])>1){
		$criteria=" 1=1 ";
		$c_vend='';$cont=0;
		foreach($datos['codmovimientos'] as $row){
			if($cont!=0){
				if($cont==1){
					$c_vend=$c_vend."and (";
				}else{
					$c_vend=$c_vend." or ";
				}
				$c_vend=$c_vend." (IVAREAL.codmovimiento=".$row." and IVAREAL.tipomovimiento='".$datos['tipomovimientos'][$cont]."')";
			}
			$cont=$cont+1;
		}
		if($cont>1){ $c_vend=$c_vend." )";}			

		$criteria=$criteria."  ".$c_vend;
		}
		

		$sql="select IVAREAL.poriva,sum(IVAREAL.baseiva) as base,round(round(sum(IVAREAL.baseiva)*cast(IVAREAL.poriva/100 as numeric),3),2) as iva 
		from (
		Select codmovimiento,tipomovimiento,poriva,round(cast(baseiva as numeric),2) as baseiva 
		From movimientoivacp Where  tipomovimiento = 'COMPRAS' OR tipomovimiento = 'ND' OR
		tipomovimiento = 'FA'  
		union
		select NC.codCPNotaCredito as codmovimiento,tipomovimientopago as tipomovimiento, CP.poriva, 
		round(cast(CP.baseiva*-1 as numeric),2) from cpnotacredito NC 
		inner join cppagadasiva CP on (NC.codCPNotaCredito=CP.codmovimientopago 
		and tipomovimientopago = 'NC')
		 ) IVAREAL
		 where ".$criteria."
		group by IVAREAL.poriva";
		//echo $sql;
		$data = $this->query($sql);
		return $data;
	}
	
	function ajustarDatos($opcion='pdf',$datos){
		switch ($opcion) {
		case 'pdf':
			if($datos['tipoventa']==''){
				$datos['tipoventa']=array();
			}
			if($datos['proveedore_id']==''){
				$datos['proveedore_id']=array();
			}
			$datos['provee']=$datos['proveedore_id'];$datos['tipoven']=$datos['tipoventa'];
			$datos['fechadesde']=$datos['desde'];$datos['fechahasta']=$datos['hasta'];
		break;		
		}
		return $datos;
	}

	function reporteImpuestos($datos=array(),$criteria='',$opcion=0){
		//$hasta=$this->anomesdia($datos['fechahasta']);
		//$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$ult_dia = date("d",(mktime(0,0,0,$datos['mes']+1,1,$datos['year'])-1));
			$order='Venta.numerofactura,Compra.fecha';
			$criteria=" Compra.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
			$criteria=$criteria." and Compra.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ";			
		
			//echo $criteria;
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 1:			
			//$order='Venta.estatus desc,Venta.codventa';
			$ult_dia = date("d",(mktime(0,0,0,$datos['mes']+1,1,$datos['year'])-1));
			//$order='Venta.numerofactura,Venta.fecha';
			if ($datos['tipofecha']=='FF'){
				$criteria=" Compra.femision >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
				$criteria=$criteria." and Compra.femision <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ";
			}else{
				$criteria="CASE WHEN Compra.estatus='A' THEN HCXP.fcancelacion >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ELSE Compra.femision >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' END";
				$criteria=$criteria." and CASE WHEN Compra.estatus='A' THEN HCXP.fcancelacion <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ELSE Compra.femision <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' END ";
			}
			/*$criteria=$criteria." and Cliente.tipodecliente = '".$datos['tipocontribuyente']."' ";*/
				
			//echo $criteria;
			$sql="select Compra.codcompra, Compra.nrodocumento,Compra.nrocontrol,
			 Proveedore.rif as Proveedore__rif, Proveedore.descripcion  as Proveedore__descripcion,  Compra.montoexento, Compra.estatus, Compra.femision, HCXP.fcancelacion, Comprasproducto.iva as porimp2, Compra.retencion::numeric,
			sum(Comprasproducto.cantidad::numeric * Comprasproducto.costo::numeric * Comprasproducto.iva::numeric/100::numeric) as ivaimp2, 
			sum( Comprasproducto.cantidad::numeric*Comprasproducto.costo::numeric) as baseimp2
			from compras as Compra
			inner join proveedores Proveedore on (Proveedore.codproveedor=Compra.codproveedor)
			inner join comprasproductos Comprasproducto on (Comprasproducto.codcompra=Compra.codcompra)
			left join h_cuentasporpagar HCXP on (HCXP.codmovimiento=Compra.codcompra and trim(HCXP.tipomovimiento)='COMPRAS')
			where ".$criteria."
			GROUP BY Compra.codcompra, Compra.nrodocumento,Compra.nrocontrol, Proveedore.rif,Proveedore.descripcion,  Compra.montoexento, Compra.estatus, Compra.femision,	HCXP.fcancelacion,Comprasproducto.iva, Compra.retencion::numeric";
			//echo $sql;
			$data = $this->query($sql);
		break;
		}
		return $data;	
	}			  
				  
}
?>
