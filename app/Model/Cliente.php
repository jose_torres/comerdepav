<?php
class Cliente extends AppModel
{
    public $name = 'Cliente';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';
	public $primaryKey = 'codcliente';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'clientes';
/*
 * */
	public $belongsTo = array(
					  'Vendedore' => array('className' => 'Vendedore',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codvendedor',
						'fields'=> 'Vendedore.codvendedor,Vendedore.descripcion'
					  )					  					  
			);
	/*public $hasMany = array('Venta' =>
                         array('className'   => 'Venta',
                               'conditions'  => 'Cliente.codsucursal=Venta.codclientesucursal and Cliente.codcliente=Venta.codcliente',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'codcliente',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),
                  );*/

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$order="Cliente.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Cliente'];	
			$var[$relleno.$item['codcliente']] = $item['descripcion'];
		}
		break;			
		}//Cierre de switch						
		return $var;
	}

	function reporte($datos=array(),$criteria='',$opcion=0){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$order='Cliente.descripcion';
			/*$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Cliente.fecha <= '".$hasta." 23:59:59' ";	*/
			$criteria= '1=1';		
			$c_cliente=$this->construir_or($datos['client'],'Cliente.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Cliente.codvendedor');
			$criteria=$criteria."  ".$c_vend;			
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		}
		return $data;
	}

	function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
		switch ($opcion) {
			case 'Cuadrediario':
			$texto = '';
			$condicion = "  Cliente.fechadeinicio='".$datos['Cuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Cliente.codcliente','conditions'=>$condicion,'recursive'=>-1));
			
			foreach ($valores as $reg) {
				$fila = $reg['Cliente'];
				
				$fila['diastolerancia']=$this->validar($fila['diastolerancia'],'vacio_nulo');
				$fila['porcentajepatente']=$this->validar($fila['porcentajepatente']);
				$fila['porcdescuento']=$this->validar($fila['porcdescuento']);
				$fila['credito']=$this->validar($fila['credito'],'boolean_false');
				$fila['contribuyentepatente']=$this->validar($fila['contribuyentepatente'],'boolean_false');
				$fila['validado']=$this->validar($fila['validado'],'boolean_false');
				
				$texto = $texto."INSERT INTO conclientes(codcliente, descripcion, rif, clase, representante, direccion, direccion2, telefonos, email, numerodefax, zona, codvendedor, tipodeprecio, limitecredito, diasdecredito, diastolerancia, fechadeinicio, observaciones, tipodecliente, interesdemora, convenioprecio, estatus, porcdescuento, credito, codsucursal, contribuyentepatente, porcentajepatente, modificado, validado, diacobro, diaventa, fechaultimaventa, nuevo, actualizado1, actualizado2) values (";
				$texto = $texto."".$fila['codcliente'].",'".$fila['descripcion']."','".$fila['rif']."','".$fila['clase']."','".$fila['representante']."','".$fila['direccion']."','".$fila['direccion2']."','".$fila['telefonos']."','".$fila['email']."','".$fila['numerodefax']."','".$fila['zona']."',".$fila['codvendedor'].",'".$fila['tipodeprecio']."',".$fila['limitecredito'].",".$fila['diasdecredito'].",".$fila['diastolerancia'].",'".$fila['fechadeinicio']."','".$fila['observaciones']."','".$fila['tipodecliente']."','".$fila['interesdemora']."','".$fila['convenioprecio']."','".$fila['estatus']."',".$fila['porcdescuento'].",'".$fila['credito']."',".$fila['codsucursal'].",'".$fila['contribuyentepatente']."',".$fila['porcentajepatente'].",".$fila['modificado'].",'".$fila['validado']."','".$fila['diacobro']."','".$fila['diaventa']."','".$fila['fechaultimaventa']."','".$fila['nuevo']."','".$fila['actualizado1']."','".$fila['actualizado2']."');";
				//$texto = $texto.'\n';
			}
			break;				
			case 'Mcuadrediario':
			$texto = '';
			$condicion = "  Cliente.fechadeinicio='".$datos['Mcuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Cliente.codcliente','conditions'=>$condicion,'recursive'=>-1));
			
			foreach ($valores as $reg) {
				$fila = $reg['Cliente'];
				
				$fila['diastolerancia']=$this->validar($fila['diastolerancia'],'vacio_nulo');
				$fila['porcentajepatente']=$this->validar($fila['porcentajepatente']);
				$fila['porcdescuento']=$this->validar($fila['porcdescuento']);
				$fila['credito']=$this->validar($fila['credito'],'boolean_false');
				$fila['contribuyentepatente']=$this->validar($fila['contribuyentepatente'],'boolean_false');
				$fila['validado']=$this->validar($fila['validado'],'boolean_false');
				
				$texto = $texto."INSERT INTO conclientes(codcliente, descripcion, rif, clase, representante, direccion, direccion2, telefonos, email, numerodefax, zona, codvendedor, tipodeprecio, limitecredito, diasdecredito, diastolerancia, fechadeinicio, observaciones, tipodecliente, interesdemora, convenioprecio, estatus, porcdescuento, credito, codsucursal, contribuyentepatente, porcentajepatente, modificado, validado, diacobro, diaventa, fechaultimaventa, nuevo, actualizado1, actualizado2) values (";
				$texto = $texto."".$fila['codcliente'].",'".$fila['descripcion']."','".$fila['rif']."','".$fila['clase']."','".$fila['representante']."','".$fila['direccion']."','".$fila['direccion2']."','".$fila['telefonos']."','".$fila['email']."','".$fila['numerodefax']."','".$fila['zona']."',".$fila['codvendedor'].",'".$fila['tipodeprecio']."',".$fila['limitecredito'].",".$fila['diasdecredito'].",".$fila['diastolerancia'].",'".$fila['fechadeinicio']."','".$fila['observaciones']."','".$fila['tipodecliente']."','".$fila['interesdemora']."','".$fila['convenioprecio']."','".$fila['estatus']."',".$fila['porcdescuento'].",'".$fila['credito']."',".$fila['codsucursal'].",'".$fila['contribuyentepatente']."',".$fila['porcentajepatente'].",".$fila['modificado'].",'".$fila['validado']."','".$fila['diacobro']."','".$fila['diaventa']."','".$fila['fechaultimaventa']."','".$fila['nuevo']."','".$fila['actualizado1']."','".$fila['actualizado2']."');";
				//$texto = $texto.'\n';
			}
			break;				
		}
		
		return $texto;
	}			  
}
?>
