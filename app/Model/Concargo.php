<?php
class Concargo extends AppModel
{
    public $name = 'Concargo';
    public $primaryKey = 'id';
    /**
 * Use database config
 *
 * @var string
 */
    //public $useDbConfig = 'comerdepa';
/**
* Use table
*
* @var mixed False or table name
*/
    public $useTable = 'concargos';
/*
*
* */
    public $hasMany = array('Concargoproducto' =>
                     array('className'   => 'Concargoproducto',
                           'conditions'  => '',
                           'order'       => '',
                           'limit'       => '',
                           'foreignKey'  => 'codcargo',
                     ),
              );			

    function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
        switch ($opcion) {
            case 'Cuadrediario':
            $texto = '';
            $condicion = "  Cargo.fecha>='".$datos['Cuadrediario']['fecha']." 00:00:00' and Cargo.fecha<='".$datos['Cuadrediario']['fecha']." 23:59:59' ";
            $valores = $this->find('all',array('order'=>'Cargo.codcargo','conditions'=>$condicion,'recursive'=>-1));

            foreach ($valores as $reg) {
                $fila = $reg['Cargo'];
                $texto = $texto."INSERT INTO concargos ( codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus) values (";
                $texto = $texto."".$fila['codcargo'].",".$fila['codsucursal'].",'".$fila['descripcion']."','".$fila['fecha']."','".$fila['autorizado']."','".$fila['responsable']."','".$fila['hora']."','".$fila['estatus']."');";
                //$texto = $texto.'\n';
            }	
            break;				
            case 'Mcuadrediario':
            $texto = '';
            $condicion = "  Cargo.fecha>='".$datos['Mcuadrediario']['fecha']."' and Cargo.fecha<='".$datos['Mcuadrediario']['fecha']."' ";
            $valores = $this->find('all',array('order'=>'Cargo.codcargo','conditions'=>$condicion,'recursive'=>-1));

            foreach ($valores as $reg) {
                $fila = $reg['Cargo'];

                $texto = $texto."INSERT INTO concargos ( codcargo, codsucursal, descripcion, fecha, autorizado, responsable, hora, estatus) values (";
                $texto = $texto."".$fila['codcargo'].",".$fila['codsucursal'].",'".$fila['descripcion']."','".$fila['fecha']."','".$fila['autorizado']."','".$fila['responsable']."','".$fila['hora']."','".$fila['estatus']."');";
                //$texto = $texto.'\n';
            }	
            break;				
        }	
        return $texto;
    }
    
    function reporte($datos=array(),$criteria='',$opcion='buscar'){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        switch ($opcion) {
        case 'buscar'://Usado para los reportes
            $order = " CC.fecha desc";
            $criteria=" CC.fecha >= '".$desde."' ";
            $criteria=$criteria." and CC.fecha <= '".$hasta."' ";
            if(isset($datos['codsucursal'])){
                if($datos['codsucursal'] <> 0){
                    $criteria = $criteria." and CC.codsucursal=".$datos['codsucursal'];
                }
            }
            
            $sql = 'select CC.id, CC.codcargo, CC.codsucursal, CC.descripcion, CC.fecha, CC.autorizado, CC.responsable, CC.hora, CC.conciliar, sum(CP.totalcosto) as Total from concargos CC inner join conproductoscargos CP on ( CC.codcargo = CP.codcargo and CC.codsucursal= CP.codsucursal  ) where '.$criteria.'
    group by CC.id,CC.codcargo, CC.codsucursal, CC.descripcion, CC.fecha, CC.autorizado, CC.responsable, CC.hora,CC.conciliar order by CC.codcargo';
            $data = $this->query($sql);
            break;
        }
        return $data;
    }
    
    function primeraFecha($opcion='General'){
            $fecha = date('Y-m-d');
            switch ($opcion) {
            case 'General'://Usado para los reportes
            $sql = "select min(fecha) as ultimafecha from concuadrediarios";
            $data = $this->query($sql);
            if (isset($data[0][0]['ultimafecha'])){
                    $fecha = $data[0][0]['ultimafecha'];
            }
            break;	
            case 'Por_Conciliar'://Usado para los reportes
            $sql = "select min(fecha) as ultimafecha from (

select fecha, codsucursal, sum(nro_cargo) as nro_cargo, sum(nro_descargo) as nro_descargo, 
sum(nro_desconciliado) as nro_desconciliado, sum(nro_conciliado) as nro_conciliado,
CASE WHEN sum(nro_cargo+nro_descargo)=sum(nro_conciliado) 
 THEN 'CONCILIADO' ELSE 'PENDIENTE' END as estatus

from (
        select fecha,codsucursal,count(codcargo) as nro_cargo, 0 as nro_descargo,
        sum(CASE WHEN conciliar='N' THEN 1 ELSE 0 END) as nro_desconciliado, 
 sum(CASE WHEN conciliar='C' THEN 1 ELSE 0 END) as nro_conciliado 
        from concargos group by fecha,codsucursal
        union
        select fecha,codsucursal,0 as nro_cargo, count(coddescargo) as nro_descargo,
        sum(CASE WHEN conciliar='N' THEN 1 ELSE 0 END) as nro_desconciliado, 
 sum(CASE WHEN conciliar='C' THEN 1 ELSE 0 END) as nro_conciliado 
        from condescargos group by fecha,codsucursal
        ) V
        group by fecha,codsucursal
        order by fecha desc,codsucursal
 ) W 
 where estatus = 'PENDIENTE'";
            $data = $this->query($sql);
            if (isset($data[0][0]['ultimafecha'])){
                    $fecha = $data[0][0]['ultimafecha'];
            }
            break;	
            }
            return $fecha;
    }
    
    function listadoCargosDescargos($datos=array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" V.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and V.fecha <= '".$hasta." 23:59:59' ";
        if(isset($datos['codsucursal'])){
            if ($datos['codsucursal']==''){
                $datos['codsucursal']= 0;
            }			
            $criteria=$criteria." and V.codsucursal=".$datos['codsucursal']." ";
        }
        
        if(isset($datos['solo'])){
            if ($datos['solo']=='true'){
                $criteria=$criteria." and V.estatus='PENDIENTE' ";
            }            
        }else{
            $criteria=$criteria." and V.estatus='PENDIENTE' ";
        }
        
        $sql = "select V.fecha, V.codsucursal, V.nro_cargo, V.nro_descargo, V.nro_desconciliado, V.nro_conciliado, V.nro_usointerno, V.estatus
from(
 select fecha, codsucursal, sum(nro_cargo) as nro_cargo, sum(nro_descargo) as nro_descargo, sum(nro_desconciliado) as nro_desconciliado, sum(nro_conciliado) as nro_conciliado, sum(nro_usointerno) as nro_usointerno,
 CASE WHEN sum(nro_cargo+nro_descargo)=sum(nro_conciliado) THEN 'CONCILIADO' ELSE 'PENDIENTE' END as estatus 
 from ( select fecha,codsucursal,count(codcargo) as nro_cargo, 0 as nro_descargo, 
 sum(CASE WHEN conciliar='N' THEN 1 ELSE 0 END) as nro_desconciliado, 
 sum(CASE WHEN conciliar='C' THEN 1 ELSE 0 END) as nro_conciliado, 
0 as nro_usointerno 
 from concargos 
 group by fecha,codsucursal union 
 select fecha,codsucursal,0 as nro_cargo, count(coddescargo) as nro_descargo, 
 sum(CASE WHEN conciliar='N' THEN 1 ELSE 0 END) as nro_desconciliado, 
 sum(CASE WHEN conciliar='C' THEN 1 ELSE 0 END) as nro_conciliado, 
 sum(CASE WHEN usointerno=true THEN 1 ELSE 0 END) as nro_usointerno 
 from condescargos 
 group by fecha,codsucursal 
 ) W 
  group by fecha,codsucursal 
 order by fecha desc,codsucursal 
) V
    where ".$criteria."
        ";
        $data = $this->query($sql);
        return $data;
    }
    
    function guardar_seleccionados($datos){

        $data = array();$i = 0;
        foreach ($datos as $registros){
            $v_chequeo = $registros['conciliar'];
            $data['Concargo'][$i]['id'] = $registros['id'];
            $data['Concargo'][$i]['conciliar'] = 'N';
            if ($v_chequeo>=1) {
                    $data['Concargo'][$i]['conciliar'] = 'C';
            }
            $i = $i + 1;
        }
        
        $mensaje = '<div class="alert alert-danger">Los Datos de los Cargos No se Conciliaron.</div>';	
        if($this->saveAll($data['Concargo'])){
                $mensaje =  '<div class="alert alert-success">Los Datos de los Cargos se Guardaron Correctamente.</div>';
        }
        return $mensaje;
    }
    
    function getNroUltimoFechaAnt($fecha='',$codsucursal=0){
        
        $fecha=$this->anomesdia($fecha);
        $nuevafecha = strtotime ( '-1 day' , strtotime ( $fecha ) ) ;
        $nuevafecha = date ( 'Y-m-d' , $nuevafecha );
        $sql = "select max(codcargo) as ultimonro from concargos where fecha<='".$nuevafecha."' and codsucursal=".$codsucursal;
        $data = $this->query($sql);
        $ultimonro = 0;
        if (isset($data[0][0]['ultimonro'])){
            $ultimonro = $data[0][0]['ultimonro'];
        }
        return $ultimonro;    
    }
				  
}
?>
