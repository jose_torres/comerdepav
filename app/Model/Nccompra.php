<?php
class Nccompra extends AppModel
{
    public $name = 'Nccompra';
    public $primaryKey = 'codnotacredito';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'notacredito_compras';
	
	public $belongsTo = array('Cpnotacredito' => array('className' => 'Cpnotacredito',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codnotacredito',
						'fields'=> ''
					  )				  					  
			);
	
	function busquedas($opcion='Datos',$datos=array()){
		switch ($opcion) {
		case 'Datos':
		$condicion =" ";$cont=0;
		foreach($datos as $row){
				
			if($cont==0){
				$condicion=$condicion." (";
			}else{
				$condicion=$condicion." or ";
			}
			$condicion = $condicion." (trim(Nccompra.facturaafecta) = '".trim($row['Retencioncompra']['documento'])."' and Nccompra.codproveedor=".$row['Retencioncompra']['codproveedor']." )";			
			$cont=$cont+1;
			}
			if($cont>0){ $condicion=$condicion." )";}
			//echo $condicion;
			$data = $this->find('all',array('conditions'=>$condicion));
		break;
		case 'Uno':
			$condicion = " trim(Nccompra.facturaafecta) = '".trim($datos['Retencioncompra']['documento'])."' and Nccompra.codproveedor='".$datos['Retencioncompra']['codproveedor']."' "; 
			$data = $this->find('first',array('conditions'=>$condicion));
		break;
		
		}
		return $data;
	}	
				  
}
?>
