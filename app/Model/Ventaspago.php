<?php
class Ventaspago extends AppModel
{
    public $name = 'Ventaspago';
    public $primaryKey = 'codventa';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ventaspagos';
	
	public $belongsTo = array('Venta' => array('className' => 'Venta',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codventa',
						'fields'=> ''
					  )					  					  
			);
	


	function buscariva(){
		$sql="select iva from ventasproductos group by iva ";
		$data = $this->query($sql);
	}

	function buscarpagos($condicion=''){
		
		$sql = "select Venta.fecha as Venta__fecha,Venta.estatus as Venta__estatus,
Ventaspago.codventa as Ventaspago__codventa, Ventaspago.codsucursal as Ventaspago__codsucursal, Ventaspago.tipopago as Ventaspago__tipopago, Ventaspago.monto as Ventaspago__monto, Ventaspago.banco as Ventaspago__banco, Ventaspago.nrocuenta as Ventaspago__nrocuenta, Ventaspago.numerocheque as Ventaspago__numerocheque from ventas as Venta inner join ventaspagos Ventaspago on (Ventaspago.codventa=Venta.codventa) where 1=1 ".$condicion;
		$data = $this->query($sql);
	}

	function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
		switch ($opcion) {
			case 'Cuadrediario':
			$texto = '';
			$condicion = "  Venta.fecha='".$datos['Cuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Venta.codventa','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Ventaspago'];
				if($fila['codpuntoventa']=='' || $fila['codpuntoventa']==null){
					$fila['codpuntoventa']=0;
				}
				if($fila['montorecibido']=='' || $fila['montorecibido']==null){
					$fila['montorecibido']=0;
				}
				$texto = $texto."INSERT INTO conventaspagos (codventa, codsucursal, tipopago, monto, banco, nrocuenta, numerotransferencia, cuenta, numerocheque, montorecibido, codpuntoventa,ventaspago_id) values (";
				$texto = $texto."".$fila['codventa'].",".$fila['codsucursal'].",'".$fila['tipopago']."',".$fila['monto'].",'".$fila['banco']."','".$fila['nrocuenta']."','".$fila['numerotransferencia']."','".$fila['cuenta']."','".$fila['numerocheque']."',".$fila['montorecibido'].",".$fila['codpuntoventa'].",".$fila['id'].");";
				//$texto = $texto.'\n';
			}
			break;				
			case 'Mcuadrediario':
			$texto = '';
			$condicion = "  Venta.fecha='".$datos['Mcuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Venta.codventa','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Ventaspago'];
				if($fila['codpuntoventa']=='' || $fila['codpuntoventa']==null){
					$fila['codpuntoventa']=0;
				}
				if($fila['montorecibido']=='' || $fila['montorecibido']==null){
					$fila['montorecibido']=0;
				}
				$texto = $texto."INSERT INTO conventaspagos (codventa, codsucursal, tipopago, monto, banco, nrocuenta, numerotransferencia, cuenta, numerocheque, montorecibido, codpuntoventa,ventaspago_id) values (";
				$texto = $texto."".$fila['codventa'].",".$fila['codsucursal'].",'".$fila['tipopago']."',".$fila['monto'].",'".$fila['banco']."','".$fila['nrocuenta']."','".$fila['numerotransferencia']."','".$fila['cuenta']."','".$fila['numerocheque']."',".$fila['montorecibido'].",".$fila['codpuntoventa'].",".$fila['id'].");";
				//$texto = $texto.'\n';
			}
			break;				
		}	
		
		return $texto;
	}
				  
}
?>
