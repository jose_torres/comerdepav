<?php
class Empleado extends AppModel
{
    var $name = 'Empleado';
   // var $useTable = 'empleado';
/*	var $validate = array(
      'codigo' => VALID_NOT_EMPTY,
      'cedula' => VALID_NOT_EMPTY,
      'nombre' => VALID_NOT_EMPTY,
      'ciudad_id' => VALID_NOT_EMPTY, 
      'profesion_id' => VALID_NOT_EMPTY, 
      'cargo_id' => VALID_NOT_EMPTY, 
      'departamento_id' => VALID_NOT_EMPTY, 
      'componente_id' => VALID_NOT_EMPTY, 
      'estadocivil_id' => VALID_NOT_EMPTY, 	
	  'email' =>VALID_EMAIL	    
    );*/
	var $belongsTo = array('Oficina' => array('className' => 'Oficina',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'oficina_id'
					  )
			);
    var $hasMany = array('Usuario' =>
                         array('className'   => 'Usuario',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'empleado_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),

                  );

			  			
	function adjuntar_archivo($dirtmp,$dirfinal)
	{
		if(!copy($dirtmp,$dirfinal))		
		{
			//	echo "error al copiar el archivo";
		}
		else
			{
			//	echo "archivo subido con exito";
			}
	}
	
	function redimensionar($nombre,$imagennormal,$imagenred){
		$extension=explode('.',$nombre);
		if($extension[1] == "jpg"){
			$this->redimensionar_jpeg($imagennormal,$imagenred,117,90,85);
		}elseif($extension[1] == "png"){
			$this->redimensionar_png($imagennormal,$imagenred,117,90,85);			
		}elseif($extension[1] == "gif"){
			$this->redimensionar_gif($imagennormal,$imagenred,117,90,85);
		}
	//	echo 'P:'.$nombre.' - '.$extension[1];
		
	}
	
		function redimensionar_jpeg($img_original,$img_nueva,$img_nueva_anchura, $img_nueva_altura,$img_nueva_calidad) 
	{ 
			// crear imagen desde original
			$img =  imagecreatefromjpeg($img_original); 
			 
			// redimensionar imagen original copiandola en la imagen
			$o_wd = imagesx($img); 
			$o_ht = imagesy($img);

			$w = round($o_wd * $img_nueva_anchura / $o_ht);
			$h = round($o_ht * $img_nueva_altura  / $o_wd);
			if(($img_nueva_anchura-$h)<($img_nueva_altura-$w))
				{
					$img_nueva_anchura =& $w;
				} else 
				{
				$img_nueva_altura =& $h;
				}
			
			// crear imagen nueva
			$thumb = imagecreatetruecolor($img_nueva_anchura,$img_nueva_altura);
			
//			imagecopyresized($thumb,$img,0,0,0,0,$img_nueva_anchura,$img_nueva_altura, imagesx($img),imagesy($img));
 			imagecopyresampled($thumb,$img,0,0,0,0,$img_nueva_anchura,$img_nueva_altura, imagesx($img),imagesy($img));
			// guardar la imagen redimensionada donde indicia $img_nueva
			imagejpeg($thumb,$img_nueva,$img_nueva_calidad);
	}
		
	function redimensionar_gif($img_original,$img_nueva,$img_nueva_anchura, $img_nueva_altura,$img_nueva_calidad) 
	{ 
			// crear imagen desde original
			$img =  imagecreatefromgif($img_original); 
			 
			// redimensionar imagen original copiandola en la imagen
			$o_wd = imagesx($img); 
			$o_ht = imagesy($img);

			$w = round($o_wd * $img_nueva_anchura / $o_ht);
			$h = round($o_ht * $img_nueva_altura  / $o_wd);
			if(($img_nueva_anchura-$h)<($img_nueva_altura-$w))
				{
					$img_nueva_anchura =& $w;
				} else 
				{
				$img_nueva_altura =& $h;
				}
			
			// crear imagen nueva
			$thumb = imagecreatetruecolor($img_nueva_anchura,$img_nueva_altura);
			
//			imagecopyresized($thumb,$img,0,0,0,0,$img_nueva_anchura,$img_nueva_altura, imagesx($img),imagesy($img));
 			imagecopyresampled($thumb,$img,0,0,0,0,$img_nueva_anchura,$img_nueva_altura, imagesx($img),imagesy($img));
			// guardar la imagen redimensionada donde indicia $img_nueva
			imagegif($thumb,$img_nueva,$img_nueva_calidad);
	}
		
	function redimensionar_png($img_original,$img_nueva,$img_nueva_anchura, $img_nueva_altura,$img_nueva_calidad) 
	{ 
			// crear imagen desde original
			$img =  @imagecreatefrompng($img_original); 
			 
			// redimensionar imagen original copiandola en la imagen
			$o_wd = ImageSX($img); 
			$o_ht = ImageSY($img);

			$w = round($o_wd * $img_nueva_anchura / $o_ht);
			$h = round($o_ht * $img_nueva_altura  / $o_wd);
			if(($img_nueva_anchura-$h)<($img_nueva_altura-$w))
				{
					$img_nueva_anchura =& $w;
				} else 
				{
				$img_nueva_altura =& $h;
				}
			
			// crear imagen nueva
			$thumb = imagecreatetruecolor($img_nueva_anchura,$img_nueva_altura);
			
//			imagecopyresized($thumb,$img,0,0,0,0,$img_nueva_anchura,$img_nueva_altura, imagesx($img),imagesy($img));
 			imagecopyresampled($thumb,$img,0,0,0,0,$img_nueva_anchura,$img_nueva_altura, imagesx($img),imagesy($img));
			// guardar la imagen redimensionada donde indicia $img_nueva
			//echo 'thumb:'.$thumb.' img_nueva:'.$img_nueva.' img_nueva_calidad:'.$img_nueva_calidad;
			imagepng($thumb,$img_nueva,$img_nueva_calidad);

	}
     	function mayuscula() {
			$datos=$this->query("update empleados set nombre = upper(nombre)");
		}
}

?>
