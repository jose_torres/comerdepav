<?php
class Sucgasto extends AppModel {

	public $name = 'Sucgasto';
	public $useDbConfig = 'comerdepa';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		/*'Efectivo' => array(
			'className' => 'Efectivo',
			'foreignKey' => 'efectivo_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),*/
		/*'Movbancario' => array(
			'className' => 'Movbancario',
			'foreignKey' => 'movbancario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)*/
	);

	function ajustarDatos($opcion='add',$datos){
		switch ($opcion) {
		case 'add':			
			$datos['Sucgasto']['codsucursal']=$datos['codsucursal'];
			$datos['Sucgasto']['nombre']=$datos['nombre'];
			$datos['Sucgasto']['fecha']=$datos['cuadre_fecha'];
			$datos['Sucgasto']['cuadrediario_id']=$datos['cuadre_id'];			
			$datos['Sucgasto']['monto']=$datos['totalDeposito'];			
			$datos['Sucgasto']['motivo']=$datos['motivo'];			
		break;
		case 'edit':
			$datos['Sucgasto']['codsucursal']=$datos['codsucursal'];
			$datos['Sucgasto']['nombre']=$datos['nombre'];
			$datos['Sucgasto']['fecha']=$datos['cuadre_fecha'];
			$datos['Sucgasto']['cuadrediario_id']=$datos['cuadre_id'];			
			$datos['Sucgasto']['monto']=$datos['totalDeposito'];			
			$datos['Sucgasto']['motivo']=$datos['motivo'];
		break;
		}
		return $datos;
	}

	function obtenerDepositos($fecha=''){
		$sucursal = 0;
		$sql = "select distinct codsucursal from sucdepositos where fecha='".$fecha."'";
		$data = $this->query($sql);
		if (isset($data[0][0]['codsucursal'])){
			$sucursal = $data[0][0]['codsucursal'];
		}
		return $sucursal;
	}

	function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
		switch ($opcion) {
			case 'Cuadrediario':
			$texto = '';
			$condicion = "  Sucgasto.fecha='".$datos['Cuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Sucgasto.id','conditions'=>$condicion,'recursive'=>-1));
			
			foreach ($valores as $reg) {
				$fila = $reg['Sucgasto'];
				$texto = $texto."INSERT INTO congastos (id_sucursal, codsucursal, monto, fecha, cuadrediario_id, nro, nombre, motivo, created, modified) values (";
				$texto = $texto."".$fila['id'].",".$fila['codsucursal'].",".$fila['monto'].",'".$fila['fecha']."',".$fila['cuadrediario_id'].",'".$fila['nro']."','".$fila['nombre']."','".$fila['motivo']."','".$fila['created']."','".$fila['modified']."');";
				//$texto = $texto.'\n';
			}
			break;				
			case 'Mcuadrediario':
			$texto = '';
			$condicion = "  Sucgasto.fecha='".$datos['Mcuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Sucgasto.id','conditions'=>$condicion,'recursive'=>-1));
			
			foreach ($valores as $reg) {
				$fila = $reg['Sucgasto'];
				$texto = $texto."INSERT INTO congastos (id_sucursal, codsucursal, monto, fecha, cuadrediario_id, nro, nombre, motivo, created, modified) values (";
				$texto = $texto."".$fila['id'].",".$fila['codsucursal'].",".$fila['monto'].",'".$fila['fecha']."',".$fila['cuadrediario_id'].",'".$fila['nro']."','".$fila['nombre']."','".$fila['motivo']."','".$fila['created']."','".$fila['modified']."');";
				//$texto = $texto.'\n';
			}
			break;				
		}	
		
		return $texto;
	}

}
?>
