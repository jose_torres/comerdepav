<?php
class HajusteHproducto extends AppModel
{
    public $name = 'HajusteHproducto';
    public $useTable = 'h_ajuste_hproducto';
    public $primaryKey = 'codigo';
    public $useDbConfig = 'comerdepa';
/*   	var $validate = array(
      'nombre' => array(
	  	'rule' => 'empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
    public $belongsTo = array('Hajuste' =>
                         array('className'   => 'Hajuste',
                               'foreignKey'  => 'codajuste'),
                               'Hproducto' =>  
                        array('className'   => 'Hproducto',
                            'foreignKey' => 'codproducto')                  
   
);

} 
                ?>  