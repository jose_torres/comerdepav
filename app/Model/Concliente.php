<?php
class Concliente extends AppModel
{
    public $name = 'Concliente';
 
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'conclientes';
/*
 * */
	/*public $belongsTo = array(
					  'Vendedore' => array('className' => 'Vendedore',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codvendedor',
						'fields'=> 'Vendedore.codvendedor,Vendedore.descripcion'
					  )					  					  
			);*/
	public $hasMany = array('Conventa' =>
                         array('className'   => 'Conventa',
                               'conditions'  => 'Concliente.codsucursal=Conventa.codclientesucursal and Concliente.codcliente=Conventa.codcliente',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'codcliente',
                               'dependent'   => false,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),
                  );

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Clientes registrados
		$order="Concliente.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Concliente'];	
			$var[$relleno.$item['codcliente'].'-'.$item['codsucursal']] = $item['descripcion'];
		}
		break;			
		}//Cierre de switch						
		return $var;
	}

	function reporte($datos=array(),$criteria='',$opcion=0){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$order='Cliente.descripcion';
			/*$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Cliente.fecha <= '".$hasta." 23:59:59' ";	*/
			$criteria= '1=1';		
			$c_cliente=$this->construir_or($datos['client'],'Cliente.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Cliente.codvendedor');
			$criteria=$criteria."  ".$c_vend;			
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		}
		return $data;
	}			  
}
?>
