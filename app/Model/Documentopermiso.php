<?php
class Documentopermiso extends AppModel
{
    var $name = 'Documentopermiso';
    var $useTable = 'documentopermisos';

    var $belongsTo = array('Usuario' => array('className' => 'Usuario',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'usuario_id'
					  ),'Documento' => array('className' => 'Documento',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'documento_id'
						),'Oficina' => array('className' => 'Oficina',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'oficina_id'
						)
			);			

	function guardar_permisos($id_documento=0,$data=array(),$accion='REVISAR'){
		$valores['Documentopermiso']=array();
		$cont=0;		
		foreach ($data['Documentopermiso'] as $row){
			$valores['Documentopermiso'][$cont]['usuario_id'] = $row['usuario_id'];
			$valores['Documentopermiso'][$cont]['documento_id'] = $id_documento;
			$valores['Documentopermiso'][$cont]['oficina_id'] = $row['oficina_id'];
			$valores['Documentopermiso'][$cont]['accion'] = $accion;
			$valores['Documentopermiso'][$cont]['permiso'] = $row['permiso'];
			$cont=$cont+1;
		}
		if($this->saveAll($valores['Documentopermiso'])){
			return true;
		}else{
			return false;
		};

	}

	function guardar_publico($id_documento=0){
		$valores['Documentopermiso']=array();
		$cont=0;					
		
		$valores['Documentopermiso'][$cont]['usuario_id'] = 0;
		$valores['Documentopermiso'][$cont]['documento_id'] = $id_documento;
		$valores['Documentopermiso'][$cont]['oficina_id'] = 0;
		$valores['Documentopermiso'][$cont]['accion'] = 'PUBLICAR';
		$valores['Documentopermiso'][$cont]['permiso'] = 'Editar';
		
		if($this->saveAll($valores['Documentopermiso'])){
			return true;
		}else{
			return false;
		};

	}
	
	function guardar_aprobar($id_documento=0,$id_usuario=0,$id_oficina=0,$accion='APROBAR'){
		$valores['Documentopermiso']=array();
		$cont=0;					
		
		$valores['Documentopermiso'][$cont]['usuario_id'] = $id_usuario;
		$valores['Documentopermiso'][$cont]['documento_id'] = $id_documento;
		$valores['Documentopermiso'][$cont]['oficina_id'] = $id_oficina;
		$valores['Documentopermiso'][$cont]['accion'] = $accion;
		$valores['Documentopermiso'][$cont]['permiso'] = 'Editar';
		
		if($this->saveAll($valores['Documentopermiso'])){
			return true;
		}else{
			return false;
		};

	}
	
	

	function contar_registros($usuario_id=0,$opcion=0){
		switch ($opcion) {
		case 0:
			$criteria= " Documentopermiso.usuario_id=".$usuario_id." and Documento.estatus<>'ANULADO' and Documento.estatus='ENVIADO' and Documentopermiso.accion='REVISAR'";
			$datos=$this->find('count', array('conditions' =>$criteria));
		break;
		case 1:
			$criteria= " Documentopermiso.usuario_id=".$usuario_id." and Documento.estatus<>'ANULADO' and Documento.estatus='A_APROBACION'
			and Documentopermiso.accion='APROBAR'";
			$datos=$this->find('count', array('conditions' =>$criteria));
		break;		
		case 2:// Nro de Usuario asociado al Documento
			$criteria= " Documentopermiso.documento_id=".$usuario_id." and Documento.estatus<>'ANULADO' and Documento.estatus='ENVIADO' and Documentopermiso.accion='REVISAR'";
			$datos=$this->find('count', array('conditions' =>$criteria,'field'=>'DISTINCT Documentopermiso.usuario_id'));
		break;		
		case 3:// Nro de Usuario asociado al Documento
			$criteria= " Documentopermiso.documento_id=".$usuario_id." and Documento.estatus<>'ANULADO' and Documento.estatus='A_APROBACION' and Documentopermiso.accion='APROBAR'";
			$datos=$this->find('count', array('conditions' =>$criteria,'field'=>'DISTINCT Documentopermiso.usuario_id'));
		break;		
		}//Cierre de switch						
		return $datos;
	}						  
}

?>
