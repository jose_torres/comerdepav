<?php
class Cpnotadebito extends AppModel
{
    public $name = 'Cpfactura';
    public $primaryKey = 'codcpnotadebito';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'cpnotadebito';
	

	function actualizar($datos=array()){

		$condicion = " Cpabono.codcpabono = '".$datos['Cpabonopago']['codcpabono']."' ";
		$sql="update Cpabono set concepto='PAGO DE RETENCION NRO ".$datos['Retencioncompra']['numero']."' where ".$condicion;
		$data = $this->query($sql);
	}

	function reporte($datos=array(),$criteria='',$opcion=0){
		if (isset($datos['fechahasta']))
		$hasta=$this->anomesdia($datos['fechahasta']);
		if (isset($datos['fechadesde']))
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:// Buscar Nota de Debito Asociada 
			$sql="select CND.fecha,CND.nrocontrol,CND.nrodocumento,CND.facturaafecta, MI.poriva as porimp2,
			RC.numero,RC.montoretenido,
			sum(round(cast(MI.baseiva+MI.baseiva*MI.poriva/100 as numeric),2)) as montobruto,
			sum(round(cast(MI.baseiva as numeric),2)) as baseimp2,
			sum(round(cast(MI.baseiva*MI.poriva/100 as numeric),2)) as ivaimp2
			from Cpnotadebito CND
			inner join movimientoivacp MI on (MI.codmovimiento=CND.codcpnotadebito
			and MI.tipomovimiento='ND')
			inner join retenciones_compras RC on (MI.codmovimiento=RC.codmovimiento
			and RC.tipodocumento='ND')
			where RC.numero='".$datos['numero']."'
			group by CND.fecha,CND.nrocontrol,CND.nrodocumento,CND.facturaafecta,MI.poriva,RC.numero,RC.montoretenido
			order by MI.poriva";
			$data = $this->query($sql);
		break;	
		}
		return $data;
	}	
				  
}
?>
