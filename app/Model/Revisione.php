<?php
class Revisione extends AppModel
{
    public $name = 'Revisione';

	public $belongsTo = array('Documento' => array('className' => 'Documento',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'documento_id'
					  ),'Tipojustificacione' => array('className' => 'Tipojustificacione',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'tipojustificacione_id'
						)
			);
			
	function contar_registros($datos_revision=array(),$opcion=0){
		switch ($opcion) {
		case 0://Cuentas las Revisiones que se han aprobados en Revision
			$criteria= " Revisione.documento_id=".$datos_revision['Documento']['id']." and Revisione.accion='ENVIADO' and Tipojustificacione.accion='Aprobar'";
			$datos=$this->find('count', array('conditions' =>$criteria,'field'=>'DISTINCT Revision.usuario_id'));
		break;
		case 1://Cuentas las Revisiones que se han aprobados en Aprobacion
			$criteria= " Revisione.documento_id=".$datos_revision['Documento']['id']." and Revisione.accion='A_APROBACION' and Tipojustificacione.accion='Aprobar'";
			$datos=$this->find('count', array('conditions' =>$criteria,'field'=>'DISTINCT Revision.usuario_id'));
		break;
		case 3://Cuentas las Revisiones que se han rechazadas en Aprobacion
			$criteria= " Revisione.documento_id=".$datos_revision['Documento']['id']." and Revisione.accion='A_APROBACION' and Tipojustificacione.accion='Rechazar'";
			$datos=$this->find('count', array('conditions' =>$criteria,'field'=>'DISTINCT Revision.usuario_id'));
		break;				
		}//Cierre de switch						
		return $datos;
	}
				  
}
?>
