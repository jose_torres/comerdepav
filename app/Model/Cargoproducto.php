<?php
class Cargoproducto extends AppModel
{
    public $name = 'Cargoproducto';
    public $primaryKey = 'codcargo';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';
/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'productoscargos';
/*
 *
 * */
	public $belongsTo = array('Cargo' => array('className' => 'Cargo',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codcargo',
						'fields'=> ''
					  )					  					  
			);

	function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
		switch ($opcion) {
			case 'Cuadrediario':
			$texto = '';
			$condicion = "  Cargo.fecha>='".$datos['Cuadrediario']['fecha']." 00:00:00' and Cargo.fecha<='".$datos['Cuadrediario']['fecha']." 23:59:59' ";
			$valores = $this->find('all',array('order'=>'Cargoproducto.codcargo','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Cargoproducto'];
				
				$texto = $texto."INSERT INTO conproductoscargos (codcargo, codsucursal, coddeposito, codproducto, totalcosto, cantidad, unidad, factorconversion, costounitario) values (";
				$texto = $texto."".$fila['codcargo'].",".$fila['codsucursal'].",".$fila['coddeposito'].",".$fila['codproducto'].",".$fila['totalcosto'].",".$fila['cantidad'].",'".$fila['unidad']."','".$fila['factorconversion']."',".$fila['costounitario'].");";
				//$texto = $texto.'\n';
			}	
			break;				
			case 'Mcuadrediario':
			$texto = '';
			$condicion = "  Cargo.fecha>='".$datos['Mcuadrediario']['fecha']."' and Cargo.fecha<='".$datos['Mcuadrediario']['fecha']."' ";
			$valores = $this->find('all',array('order'=>'Cargo.codcargo','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Cargoproducto'];
				
				$texto = $texto."INSERT INTO conproductoscargos (codcargo, codsucursal, coddeposito, codproducto, totalcosto, cantidad, unidad, factorconversion, costounitario) values (";
				$texto = $texto."".$fila['codcargo'].",".$fila['codsucursal'].",".$fila['coddeposito'].",".$fila['codproducto'].",".$fila['totalcosto'].",".$fila['cantidad'].",'".$fila['unidad']."','".$fila['factorconversion']."',".$fila['costounitario'].");";
				//$texto = $texto.'\n';
			}	
			break;				
		}	
		return $texto;
	}
				  
}
?>
