<?php
class Esquema extends AppModel
{
    public $name = 'Esquema';

	public $belongsTo = array('Seccione' => array('className' => 'Seccione',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'seccione_id'
					  ),'Plantilla' => array('className' => 'Plantilla',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'plantilla_id'
						)
			);
	
	
	public function guardarEsquema($id_plantilla=0,$data=array()){
		
		if(count($data['Esquema'])>0){
		$valores['Esquema']=array();
		$cont=0;
		$this->updateAll(array('Esquema.status' => "'C'"), array('Esquema.plantilla_id' => $id_plantilla));		
		foreach ($data['Esquema'] as $row){
			$esq = $this->find('first',array('conditions'=>'Esquema.plantilla_id='.$id_plantilla.' and Esquema.seccione_id='.$row['seccione_id'].' '));
			if(isset($esq['Esquema']['id'])){
				$valores['Esquema'][$cont]['id'] = $esq['Esquema']['id'];
			}
			$valores['Esquema'][$cont]['plantilla_id'] = $id_plantilla;
			$valores['Esquema'][$cont]['status'] = 'A';
			$valores['Esquema'][$cont]['seccione_id'] = $row['seccione_id'];			
			$cont=$cont+1;
		}
		if($this->saveAll($valores['Esquema'])){
			return true;
		}else{
			return false;
		};
		}else{ return false;}
	}
	
	public function cargarEsquema($id_plantilla=0,$opcion='Todos'){
		
		switch ($opcion) {
		case 'Todos'://Muestra Todos los Perfiles Padres
		$order="Esquema.id ASC ";
		$condiciones = " Esquema.plantilla_id=".$id_plantilla." and Esquema.status='A' ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order));		
		$var = array();
		foreach ($datos as $row) {
			$var['Esquema'][$row['Esquema']['seccione_id']]=array('seccione_id'=>$row['Esquema']['seccione_id'],'plantilla_id'=>$row['Esquema']['plantilla_id'],'elemento_nombre'=>$row['Seccione']['nombre']);
		}
		break;
		}		
		return $var;
	}
				  
}
?>
