<?php
class Cuadrediario extends AppModel {

	public $name = 'Cuadrediario';
	public $useDbConfig = 'comerdepa_3';
/*	var $validate = array(
		'nro' => array('numeric'),
		'fecha' => array('date')
	);*/

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	/*public $hasMany = array(
		'Efectivo' => array(
			'className' => 'Efectivo',
			'foreignKey' => 'cuadrediario_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);*/

	function reporte($datos=array(),$criteria=array(),$opcion='buscar'){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		switch ($opcion) {
		case 'buscar'://Usado para los reportes
		$order = " Cuadrediario.fecha desc";
		$criteria=" Cuadrediario.fecha >= '".$desde."' ";
		$criteria=$criteria." and Cuadrediario.fecha <= '".$hasta."' ";
		if(isset($datos['sucursal_id'])){
			if($datos['sucursal_id']<>0){
				$criteria = $criteria." and Cuadrediario.codsucursal=".$datos['sucursal_id'];
			}
		}
		if(isset($datos['estatus'])){
			if($datos['estatus']!=0){
				$criteria = $criteria." and Cuadrediario.estatus=".$datos['estatus'];
			}
		}
		
		$data=$this->find('all',array('conditions'=>$criteria,'order'=>$order));
			break;	
		}
		return $data;
	}
	
	function llenar_combo($relleno=''){
		$order="Cuadrediario.fecha desc";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>0));
		$i=1;
		$var = array();
		$var[0]='Sin Relacion';
		foreach ($datos as $row) {
			$item = $row['Cuadrediario'];	
			$var[$relleno.$item['id']] = date('d-m-Y',strtotime($item['fecha']));
			$i=$i+1;
		}		
		return $var;
	}
	
	function crear($fecha=''){
		$codigo= 1;
		$nro = $this->find('first',array('conditions'=> "Cuadrediario.fecha='".$fecha."'",'recursive'=>0,'order'=>'Cuadrediario.nro desc'));
		if(isset($nro['Cuadrediario']['nro'])){
			$codigo= $nro['Cuadrediario']['nro'] + 1;
		}
		$datos['Cuadrediario']['nro']=$codigo;
		$datos['Cuadrediario']['fecha']=$fecha;
		$datos['Cuadrediario']['estatus']=1;
		$this->save($datos);
	}
	
	function buscar($datos=array()){
		$valores = array();
		$fecha=$this->anomesdia($datos['fechahasta']);	
		$nro_reg = $this->find('count',array('conditions'=> "Cuadrediario.fecha='".$fecha."'",'recursive'=>0));
		if($nro_reg<=0){
			$this->crear($fecha);
		}	
		$valores = $this->find('first',array('conditions'=> "Cuadrediario.fecha='".$fecha."'",'recursive'=>0));
		
		return $valores;
	}

	function generar_linea_txt($datos=array()){
		$texto = "INSERT INTO concuadrediarios (id_sucursal, codsucursal ,nro, fecha, estatus, created, modified) values (";
		$texto = $texto."".$datos['Cuadrediario']['id'].",".$datos['Cuadrediario']['codsucursal'].",'".$datos['Cuadrediario']['nro']."','".$datos['Cuadrediario']['fecha']."','".$datos['Cuadrediario']['estatus']."','".$datos['Cuadrediario']['created']."','".$datos['Cuadrediario']['modified']."');";
		//$texto = $texto.'\n';
		return $texto;
	}

/*	function encriptar($texto=''){
		$texto = $this->encrypt($texto);
		return $texto;
	}
	
	function desencriptar($texto=''){
		$texto = $this->decrypt($texto);
		return $texto;
	}
*/
	function encriptar($cadena){
    $key='S';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
    return $encrypted; //Devuelve el string encriptado
 
	}
	 
	function desencriptar($cadena){
		 $key='S';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
		 $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		return $decrypted;  //Devuelve el string desencriptado
	}

	function obtenerNroVentas($datos=array()){
		$nro = 0;
		$sql = "select count(codventa) as total from ventas VT where VT.estatus <> 'E' and VT.Fecha = '".$datos['Cuadrediario']['fecha']."'
		group by VT.fecha";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$nro =$data[0][0]['total'];
		}
		return $nro;

	}

	function obtenerNroDevoluciones($datos=array()){
		$nro = 0;
		$sql = "select count(coddevolucion) as total from devolucionventa VT where VT.Fecha >= '".$datos['Cuadrediario']['fecha']." 00:00:00' and VT.Fecha <= '".$datos['Cuadrediario']['fecha']." 23:59:59'
		group by VT.fecha";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$nro =$data[0][0]['total'];
		}
		return $nro;
	}

	function obtenerMontoVentas($datos=array()){
		$monto = 0;
		$sql = "select sum(montobruto) as total from ventas VT where VT.estatus <> 'E' and VT.Fecha = '".$datos['Cuadrediario']['fecha']."' 
		group by VT.fecha";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$monto =$data[0][0]['total'];
		}
		return $monto;
	}
	
	function obtenerMontoDebitos($cuadre=array()){
		$monto = 0;
		$sql = "select sum(monto) as total from sucgastos Congasto where Congasto.fecha='".$cuadre['Cuadrediario']['fecha']."'  ;";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$monto =$data[0][0]['total'];
		}
		return $monto;
	}
	
	function obtenerMontoDeposito($cuadre=array()){
		$monto = 0;
		$sql = "select sum(monto) as total from movbancarios Conmovbancario where Conmovbancario.fecha='".$cuadre['Cuadrediario']['fecha']."'  and (Conmovbancario.tipodeposito='CHEQUE' or Conmovbancario.tipodeposito='EFECTIVO' or Conmovbancario.tipodeposito='DEBITO' or Conmovbancario.tipodeposito='TRANSFER');";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$monto =$data[0][0]['total'];
		}
		return $monto;
	}

	function obtenerMontoRetencion($datos=array()){
		$monto = 0;
		$hasta=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$desde=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";
		$criteria=$criteria."  ";	
		$sql =  " select RV.fechaemision,sum(RVD.montoretenido) as Total from retenciones_ventas RV inner join retenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion)
		where ".$criteria." group by RV.fechaemision";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$monto =$data[0][0]['total'];
		}
		return $monto;
	}

	function actualizarMontosCuadre($id=''){
		$datos_cuadre = $this->read(null, $id);
		$cuadre['Cuadrediario']['id']=$id;		
		$cuadre['Cuadrediario']['nro_venta']=$this->obtenerNroVentas($datos_cuadre);
		$cuadre['Cuadrediario']['nro_devolucion']=$this->obtenerNroDevoluciones($datos_cuadre);
		$cuadre['Cuadrediario']['monto_venta']=$this->obtenerMontoVentas($datos_cuadre);
		$cuadre['Cuadrediario']['monto_debito']=$this->obtenerMontoDebitos($datos_cuadre);
		$cuadre['Cuadrediario']['monto_deposito']=$this->obtenerMontoDeposito($datos_cuadre);
		$cuadre['Cuadrediario']['monto_retencion']=$this->obtenerMontoRetencion($datos_cuadre);
		$this->save($cuadre);
	}

}
?>
