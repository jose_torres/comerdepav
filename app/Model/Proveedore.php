<?php
class Proveedore extends AppModel
{
    public $name = 'Proveedore';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';
	public $primaryKey = 'codproveedor';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'proveedores';
/*
 * */
	public $hasMany = array('Compra' =>
                         array('className'   => 'Compra',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'codproveedor',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),
                  );

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$order="Proveedore.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Proveedore'];	
			$var[$relleno.$item['codproveedor']] = $item['descripcion'];
		}
		break;			
		}//Cierre de switch						
		return $var;
	}

	function calcularSaldo($fechadesde=''){
		$desde=$this->anomesdia($fechadesde);
		$sql="select codproveedor,sum(debito) as T_debito,sum(credito) as T_credito, sum(debito)-sum(credito) as saldo  from v_estadodecuenta where fecha<'".$desde."' group by codproveedor
		order by codproveedor";
		$datos = $this->query($sql);		
		$var = array();
		foreach ($datos as $row) {
			$item = $row[0];	
			$var[$item['codproveedor']] = $item['saldo'];
		}
		
		return $var;
	}

	function reporte($datos=array(),$criteria='',$opcion=0){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$order='Proveedore.descripcion';
			/*$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Cliente.fecha <= '".$hasta." 23:59:59' ";	*/
			$criteria= '1=1';		
			$c_cliente=$this->construir_or($datos['provee'],'Proveedore.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
							
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 1:			
			$order='Venta.estatus desc,Venta.codventa';
			//$criteria=" fcancelacion >= '".$desde." 00:00:00' ";
			$criteria=" CXP.fcancelacion >= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['provee'],'CXP.codproveedor');
			$criteria=$criteria."  ".$c_cliente;				
			//echo $criteria;
			$sql=" SELECT CXP.codmovimiento,CXP.tipomovimiento, CXP.femision, CXP.fvencimiento, CXP.numero, CXP.concepto, CXP.debito, CXP.credito, CXP.fecha, CXP.codproveedor,CXP.descripcion,CXP.rif, CXP.fcancelacion,CXP.fvencimiento - CXP.femision as dias from (
			SELECT a.codmovimiento,a.tipomovimiento, a.femision, b.fvencimiento, a.numero, b.concepto, 0 AS debito, b.monto AS credito, a.fecha, a.codproveedor,PR.descripcion,PR.rif, CAST(now() AS DATE) + CAST('1 days' AS INTERVAL) as fcancelacion
			FROM (        
			(SELECT a_1.codcompra AS codmovimiento, 'COMPRAS'::text AS tipomovimiento, a_1.femision, a_1.nrodocumento AS numero, a_1.femision AS fecha, a_1.codproveedor, a_1.codsucursal
			FROM compras a_1
			UNION 
			SELECT a_1.codcpfactura AS codmovimiento, 'FA'::text AS tipomovimiento, a_1.fecha, a_1.nrodocumento AS numero, a_1.fecha, a_1.codproveedor, a_1.codsucursal	FROM cpfactura a_1)
			UNION 
			SELECT a_1.codcpnotadebito AS codmovimiento, 'ND'::text AS tipomovimiento, a_1.fecha, a_1.nrodocumento AS numero, a_1.fecha, a_1.codproveedor, a_1.codsucursal FROM cpnotadebito a_1) a
			JOIN cuentasporpagar b ON a.codmovimiento = b.codmovimiento AND 
			a.tipomovimiento = b.tipomovimiento::text AND a.codsucursal = b.codsucursal
			inner join proveedores PR on (a.codproveedor=PR.codproveedor)
			UNION 
			SELECT a.codmovimiento, a.tipomovimiento, a.femision, bh.fvencimiento, a.numero, bh.concepto, 0 AS debito, bh.monto AS credito, a.fecha, a.codproveedor,PR.descripcion,PR.rif,bh.fcancelacion
			FROM (        
			(SELECT a_1.codcompra AS codmovimiento, 'COMPRAS'::text AS tipomovimiento, a_1.femision, a_1.nrodocumento AS numero, a_1.femision AS fecha, a_1.codproveedor, a_1.codsucursal FROM compras a_1
			UNION 
			SELECT a_1.codcpfactura AS codmovimiento, 'FA'::text AS tipomovimiento, a_1.fecha, a_1.nrodocumento AS numero, a_1.fecha, a_1.codproveedor, a_1.codsucursal		FROM cpfactura a_1)
			UNION 
			SELECT a_1.codcpnotadebito AS codmovimiento, 'ND'::text AS tipomovimiento, a_1.fecha, a_1.nrodocumento AS numero, a_1.fecha, a_1.codproveedor, a_1.codsucursal	FROM cpnotadebito a_1) a
			JOIN h_cuentasporpagar bh ON a.codmovimiento = bh.codmovimiento AND a.tipomovimiento = bh.tipomovimiento::text AND a.codsucursal = bh.codsucursal
			inner join proveedores PR on (a.codproveedor=PR.codproveedor)
			) CXP
			where ".$criteria."
			order by CXP.descripcion,CXP.femision,CXP.fvencimiento";
			//echo $sql;
			$data = $this->query($sql);
		break;
		case 2:			
			$order=' PAGOS_VENTAS.estatus,PAGOS_VENTAS.descripcion,PAGOS_VENTAS.femision';
			$criteria=" PAGOS_COMPRAS.fecha >= '".$desde." ' ";
			$criteria=$criteria." and PAGOS_COMPRAS.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['provee'],'PAGOS_COMPRAS.codproveedor');
			$criteria=$criteria."  ".$c_cliente;
			$c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_COMPRAS.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select PAGOS_COMPRAS.codcompra,PAGOS_COMPRAS.nrodocumento, PAGOS_COMPRAS.codproveedor, PAGOS_COMPRAS.rif,PAGOS_COMPRAS.descripcion, PAGOS_COMPRAS.fecha, PAGOS_COMPRAS.baseimp2,PAGOS_COMPRAS.ivaimp2, PAGOS_COMPRAS.retencion,  PAGOS_COMPRAS.femision, PAGOS_COMPRAS.tipomovimiento, PAGOS_COMPRAS.MontoPago, PAGOS_COMPRAS.estatus, PAGOS_COMPRAS.factura, PAGOS_COMPRAS.Proveedor
			from (
			select VT.codcompra,VT.nrodocumento, VT.codproveedor,CT.rif,CT.descripcion, VT.femision as Fecha, VT.baseimp2,VT.ivaimp2, VT.retencion,VT.femision,'1COMPRAS'::text AS tipomovimiento, 0 as MontoPago, VT.estatus,VT.nrodocumento as factura, CT.descripcion as Proveedor
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			union all
			select VT.codcompra,CAP.numero as nrodocumento, VT.codproveedor,CT.rif,CASE WHEN upper(CAP.tipopago)='RETENCION' 
			THEN 'PAGO DE RETENCION' WHEN upper(CAP.tipopago)='CAJA' THEN 'PAGO EN EFECTIVO' ELSE BC.nombre 
			END as descripcion, 
			CASE WHEN upper(CAP.tipopago)='RETENCION' THEN CAP.fecharetencion 
			WHEN upper(CAP.tipopago)='CAJA' THEN VT.femision  ELSE CCP.fecha 
			END as Fecha,			 VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'2PAGOS-'||upper(CAP.tipopago) AS tipomovimiento, CAP.Monto as MontoPago, VT.estatus,VT.nrodocumento as factura, CT.descripcion as Proveedor
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cppagadas  CCP on (CCP.codmovimientocp=VT.codcompra)
			inner join cpabonopago CAP on (CAP.codcpabono=CCP.codmovimientopago)
			left join bancos BC on (BC.abreviatura=CAP.banco)
			union all
			select VT.codcompra, cast(NC.codcpnotacredito as text) as nrodocumento, VT.codproveedor,CT.rif, NC.concepto as descripcion, NC.fecha as Fecha,
			VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'4'||'NOTA DE CREDITO'::text AS tipomovimiento, NC.monto*-1 as MontoPago, VT.estatus,VT.nrodocumento as factura, CT.descripcion as Proveedor
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cpnotacredito NC on (NC.nrodocumento=VT.nrodocumento and NC.codproveedor=VT.codproveedor)
			) PAGOS_COMPRAS
			where PAGOS_COMPRAS.tipomovimiento<>'1COMPRAS' and ".$criteria." 
			ORDER BY PAGOS_COMPRAS.proveedor,PAGOS_COMPRAS.fecha,PAGOS_COMPRAS.tipomovimiento;";
			//echo $sql;
			$data = $this->query($sql);
		break;
		case 3:			
			$criteria=" EC.fecha >= '".$desde." ' ";
			$criteria=$criteria." and EC.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['provee'],'EC.codproveedor');
			$criteria=$criteria."  ".$c_cliente;			
			//echo $criteria;
			$sql="select EC.codproveedor,PR.rif,PR.descripcion, EC.tipomovimiento, EC.femision, EC.fvencimiento, EC.numero, EC.concepto, EC.fecha, EC.debito,EC.credito from v_estadodecuenta EC
			inner join proveedores PR on (EC.codproveedor=PR.codproveedor)
			where ".$criteria."
			order by PR.descripcion";
			//echo $sql;
			$data = $this->query($sql);
		break;
		}
		return $data;
	}				  
}
?>
