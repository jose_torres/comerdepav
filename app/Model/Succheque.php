<?php
class Succheque extends AppModel {

	public $name = 'Succheque';
	public $useDbConfig = 'comerdepa';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Banco' => array(
			'className' => 'Banco',
			'foreignKey' => 'banco_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		/*'Movbancario' => array(
			'className' => 'Movbancario',
			'foreignKey' => 'movbancario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)*/
	);

	function ajustarDatos($opcion='add',$datos){
		switch ($opcion) {
		case 'add':			
			$valor['Succheque']['codsucursal']=$datos['Ventaspago']['codsucursal'];	
			$valor['Succheque']['banco_id']=$datos['Ventaspago']['banco'];
			$valor['Succheque']['codventa']=$datos['Ventaspago']['codventa'];
			$valor['Succheque']['fecha']=$datos['Venta']['fecha'];
			$valor['Succheque']['nro']=$datos['Ventaspago']['numerocheque'];
			$valor['Succheque']['nrocta']=$datos['Ventaspago']['nrocuenta'];
			$valor['Succheque']['cuadrediario_id']=$datos['Cuadrediario']['id'];
			$valor['Succheque']['monto']=round( $datos['Ventaspago']['monto'], 2, PHP_ROUND_HALF_UP);			
						
		break;
		case 'edit':
			$valor['Succheque']['id']=$datos['Succheque']['id'];
			$valor['Succheque']['movbancario_id']=$datos['Succheque']['movbancario_id'];
			$valor['Succheque']['monto']=round( $datos['Ventaspago']['monto'], 2, PHP_ROUND_HALF_UP);	
		break;
		case 'cuadre':
			for ($i = 1; $i <= $datos['total_reg']; $i++) {
				if(isset($datos['cheque_id'.$i])){
				$valor['Succheque'][$i]['movbancario_id']=$datos['movbancario_id'];
				$valor['Succheque'][$i]['id']=$datos['cheque_id'.$i];
				}
			}
		break;
		}
		return $valor;
	}

	function obtenerDepositos($fecha=''){
		$sucursal = 0;
		$sql = "select distinct codsucursal from sucdepositos where fecha='".$fecha."'";
		$data = $this->query($sql);
		if (isset($data[0][0]['codsucursal'])){
			$sucursal = $data[0][0]['codsucursal'];
		}
		return $sucursal;
	}

	function buscar($datos,$cuadre){
	    $var = array();
		//print_r($datos);
		foreach ($datos as $row){
			// Buscar si esta
			if(isset($row['venta'])){
				$row['Venta'] = $row['venta'];
				$row['Ventaspago'] = $row['ventaspago'];
				//echo 'paso';
			}
			
			if($row['Venta']['estatus']!='E'){
			$row['Cuadrediario']['id'] = $cuadre['Cuadrediario']['id'];
			$nro_reg = $this->find('count',array('conditions'=> "Succheque.codventa=".$row['Ventaspago']['codventa']." and Succheque.codsucursal=".$row['Ventaspago']['codsucursal']." and Succheque.banco_id=".$row['Ventaspago']['banco']." and Succheque.monto=".$row['Ventaspago']['monto']." ",'recursive'=>0));
			//echo 'Nro;'.$nro_reg.'<br>';
			if($nro_reg<=0){
				$dat= $this->ajustarDatos('add',$row);
				$this->create();
				if($this->save($dat)){
					//echo 'Guardo Nvo Registro<br>';
				}
			}else{
				$dat = $this->find('first',array('conditions'=> "Succheque.codventa=".$row['Ventaspago']['codventa']." and Succheque.codsucursal=".$row['Ventaspago']['codsucursal']." and Succheque.banco_id=".$row['Ventaspago']['banco']." and Succheque.monto=".$row['Ventaspago']['monto']." ",'recursive'=>0));
				if(isset($dat['Succheque']['id'])){
					$row['Succheque']['id']=$dat['Succheque']['id'];
					$row['Succheque']['movbancario_id']=$dat['Succheque']['movbancario_id'];
					$dat= $this->ajustarDatos('edit',$row);
					$this->create();
					$this->save($dat);
				}				
			}
			
			}
		}
		$var = $this->find('all',array('conditions'=> "Succheque.fecha='".$cuadre['Cuadrediario']['fecha']."' and Succheque.movbancario_id=0 "));
		return $var;
	}

	function buscar2($datos,$cuadre){
	    $var = array();
		print_r($datos);
		foreach ($datos as $row){
			// Buscar si esta
			
			if($row['venta']['estatus']!='E'){
			$row['Cuadrediario']['id'] = $cuadre['Cuadrediario']['id'];
			$nro_reg = $this->find('count',array('conditions'=> "Succheque.codventa=".$row['ventaspago']['codventa']." and Succheque.codsucursal=".$row['ventaspago']['codsucursal']." ",'recursive'=>0));
			if($nro_reg<=0){
				$dat= $this->ajustarDatos('add',$row);				
			}else{
				$dat = $this->find('first',array('conditions'=> "Succheque.codventa=".$row['ventaspago']['codventa']." and Succheque.codsucursal=".$row['ventaspago']['codsucursal']." ",'recursive'=>0));
				$row['Succheque']['id']=$dat['Succheque']['id'];
				$dat= $this->ajustarDatos('edit',$row);				
			}
			$this->save($dat);
			}
		}
		$var = $this->find('all',array('conditions'=> "Succheque.fecha='".$cuadre['Cuadrediario']['fecha']."' and Succheque.movbancario_id=0 ")); 
		return $var;
	}


}
?>
