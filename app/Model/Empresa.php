<?php
class Empresa extends AppModel
{
    public $name = 'Empresa';
    
    public $hasMany = array('Oficina' =>
                         array('className'   => 'Oficina',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'empresa_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )
                  );

	public function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos las Empresas
		$order="Empresa.nombre ASC ";
		$condiciones = " ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Empresa'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		case 1://Muestra Todos las Empresas
		$order="Empresa.nombre ASC ";
		$condiciones = " Empresa.tipoventa <> 'MAYORDETAL' ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Empresa'];	
			$var[$relleno.$item['id'].'-'.$item['bd']] = $item['nombre'];
		}
		break;
		case 2://Muestra Todos las Empresas con Ventas al Detal
		$order="Empresa.nombre ASC ";
		$condiciones = " Empresa.tipoventa = 'DETAL' ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Empresa'];	
			$var[$relleno.$item['id'].'-'.$item['bd']] = $item['nombre'];
		}
		break;
		case 3://Muestra Todos las Empresas con Ventas al Detal
		$order="Empresa.nombre ASC ";
		$condiciones = " Empresa.tipoventa = 'MAYOR' ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Empresa'];	
			$var[$relleno.$item['id'].'-'.$item['bd']] = $item['nombre'];
		}
		break;
		case 4://Muestra Todos las Empresas
		$order="Empresa.nombre ASC ";
		$condiciones = " Empresa.tipoventa <> 'MAYORDETAL' ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array('0-Todos'=>'Todos');
		foreach ($datos as $row) {
			$item = $row['Empresa'];	
			$var[$relleno.$item['id'].'-'.$item['bd']] = $item['nombre'];
		}
		break;
		case 5://Muestra Todos las Empresas
		$order="Empresa.nombre ASC ";
		$condiciones = " ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Empresa'];	
			$var[$relleno.$item['id'].'-'.$item['bd']] = $item['nombre'];
		}
		break;
		}		
		return $var;
	}
	
	function generar_nombre_archivo($parte_nom='',$datos='',$ubi='_li'){
		list($nombre_archivo,$ext)= explode('.',$datos);
		$nombre='';
		$nombre = $parte_nom.$nombre_archivo.$ubi.'.'.$ext;
		return $nombre;
	}

	function cargarBD(){
		$sucursal = array();
		$sucursal['comerdepa']='Principal';
		$sucursal['superdepa']='Superdepa';
		$sucursal['comerdepa_1']='Sucursal 1';
		$sucursal['comerdepa_2']='Sucursal 2';
		$sucursal['comerdepa_3']='Sucursal 3';
		return $sucursal;
	}
	
	function traer_pg($url='comerdepa1.ddns.net',$direccion='',$carpeta='CI',$usuario='comerdepa01',$clave='qwertyu'){
        //$connection = ssh2_connect($url, 22);
        $connection = ssh2_connect('192.168.1.8', 22);
        if (!$connection){
			$mensaje='<div class="alert alert-danger">No se Pudo conectar con la Direccion '.$url.'</div>';
		}else{
			if(ssh2_auth_password($connection, 'root', 'Server2014*')){
				//ssh2_scp_recv($connection, '/etc/postgresql/9.3/main/pg_hba.conf', $direccion.'/conexion/pg_hba.conf');
				
				$comando = "sshpass -p ".$clave." rsync ".$usuario."@".$url.":/etc/postgresql/9.3/main/pg_hba.conf ".$direccion."conexion/".$carpeta."/";
				//$comando = "sshpass -p ".$clave." spc ".$usuario."@".$url.":/home/comerdepa01/Documentos/repositorios/sistemas/Enviar1/*.TXT ".$direccion."conexion/";
				//echo $comando;
				ssh2_exec($connection,$comando);
				$mensaje='<div class="alert alert-success">Se Conecto a la Direccion '.$url.'.</div>';
			}else{
				$mensaje='<div class="alert alert-danger">No se Pudo conectar con la Direccion '.$url.'. Clave o Usuario Incorrecto</div>';
			}
			//ssh2_disconnect($connection);
	    } 
	   return $mensaje;
        
    }
    
    function enviar_pg($url='comerdepa1.ddns.net',$direccion='',$carpeta='CI',$usuario='comerdepa01',$clave='qwertyu'){
        //$connection = ssh2_connect($url, 22);
        $connection = ssh2_connect($url, 22);
        if (!$connection){
			$mensaje='<div class="alert alert-danger">No se Pudo conectar con la Direccion '.$url.'</div>';
		}else{
			if(ssh2_auth_password($connection, $usuario, $clave)){
				//ssh2_scp_recv($connection, '/etc/postgresql/9.3/main/pg_hba.conf', $direccion.'/conexion/pg_hba.conf');				
				$comando = "sshpass -p ".$clave." rsync ".$direccion."conexion/".$carpeta."/pg_hba.conf ".$usuario."@".$url.":/etc/postgresql/9.3/main/ ";
				ssh2_scp_send($connection, $direccion."conexion/".$carpeta."/pg_hba.conf", '/etc/postgresql/9.3/main/pg_hba.conf', 0777);
				//echo $comando;
				//ssh2_exec($connection,$comando);
				$mensaje='<div class="alert alert-success">Se Envio Archivo a la Direccion '.$url.'.</div>';
			}else{
				$mensaje='<div class="alert alert-danger">No se Pudo conectar con la Direccion '.$url.'. Clave o Usuario Incorrecto</div>';
			}
			//ssh2_disconnect($connection);
	    } 
	   return $mensaje;
        
    }
    
    function reiniciar_pg($url='comerdepa1.ddns.net',$usuario='comerdepa01',$clave='qwertyu'){
        //$connection = ssh2_connect($url, 22);
        $connection = ssh2_connect($url, 22);
        if (!$connection){
			$mensaje='<div class="alert alert-danger">No se Pudo conectar con la Direccion '.$url.'</div>';
		}else{
			if(ssh2_auth_password($connection, $usuario, $clave)){
				
				$comando = " echo ".$clave." | sudo -S  service postgresql restart ";				
				ssh2_exec($connection,$comando);
				$mensaje='<div class="alert alert-success">Se Reinicio el Servidor con la Direccion '.$url.'.</div>';
			}else{
				$mensaje='<div class="alert alert-danger">No se Pudo Reiniciar el Servidor con la Direccion '.$url.'. Clave o Usuario Incorrecto</div>';
			}
			//ssh2_disconnect($connection);
	    } 
	   return $mensaje;
        
    }
    
    function modificar_archivo($direccion='',$carpeta='CI',$ip='127.0.0.1'){
		$fichero = $direccion."conexion/".$carpeta."/pg_hba.conf";		
	
		$filas = file($fichero);
		$ultima_linea = count($filas);
		$ultima_linea_escritura = $filas[$ultima_linea-1];
		
		//echo "Aqui esta:<br>";
		//echo $ultima_linea_escritura."<br>";
		$filas[$ultima_linea-1] = " host    all             all             ".$ip."/32      md5		";
		//$actual = implode("\n",$filas);
		$actual = implode("",$filas);
		file_put_contents($fichero, $actual);
		$filas = file($fichero);
		$ultima_linea = count($filas);
		$ultima_linea_escritura2 = $filas[$ultima_linea-1];
		$mensaje = '<div class="alert alert-success">Se ha Agregado la permisolog&iacute;a '.$ultima_linea_escritura2.'</div>';
		//$archivo = fopen($direccion."conexion/".$carpeta."/pg_hba.conf", "w+");
		//fseek($archivo, $num_lineas);
		 
		return $mensaje;		
	
	}
				  
}
?>
