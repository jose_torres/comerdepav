<?php
App::uses('AppModel', 'Model');
class Cpasiini extends AppModel
{
    var $name = 'Cpasiini';
    var $useTable = 'cpasiini';
    public $useDbConfig = 'oracle';

	
	function obtener_sima($ano='R2012'){
		
		$select="SELECT A.PASSEMP FROM SIMA_USER.EMPRESA A WHERE A.TLFEMP LIKE '".$ano."%' ";		
		$datos=$this->query($select,$cachequeries = true);		
		if (isset($datos[0]['']['passemp'])){			
			$this->get_sima($datos[0]['']['passemp']);			
		}else{
			$this->get_sima('SIMA045');
		}
		return $datos;	
	}
  /* Funciones para la vista */ 	
	function reporte($datos=array(),$criteria='',$opcion=0){
		
		/*$hasta=$this->anomesdia($datos['hasta']);
		$desde=$this->anomesdia($datos['desde']);*/
		$hasta=$datos['hasta'];
		$desde=$datos['desde'];
		switch ($opcion) {
		case 0://Usado para los reportes			
			if(isset($datos['selperiodo'])){			
				if ($datos['selperiodo']=='true' or $datos['selperiodo']==1){
					$criteria=" EJECUCION_PRESUPUESTARIA_X_DOC.FECHA >= to_date('".$desde."','DD/MM/YYYY') ";
					$criteria=$criteria." and EJECUCION_PRESUPUESTARIA_X_DOC.FECHA <= to_date('".$hasta."','DD/MM/YYYY') ";					
					$this->obtener_sima($datos['centrogasto'].substr($desde, 6, 4));
				}else{
					$criteria=" TO_CHAR (EJECUCION_PRESUPUESTARIA_X_DOC.FECHA, 'MM') = '".$datos['mesconsulta']."' ";
					$criteria=$criteria." and TO_CHAR (EJECUCION_PRESUPUESTARIA_X_DOC.FECHA, 'YYYY') = '".$datos['anoconsulta']."' ";
					
					$this->obtener_sima($datos['centrogasto'].$datos['anoconsulta']);
				}
			}	
			$criteria=$criteria." and EJECUCION_PRESUPUESTARIA_X_DOC.CODPRE>='".$datos['codpredes']."'";
			$criteria=$criteria." and EJECUCION_PRESUPUESTARIA_X_DOC.CODPRE<='".$datos['codprehas']."'";
			
			$sql="SELECT EJECUCION_PRESUPUESTARIA_X_DOC.CATEGORIA, EJECUCION_PRESUPUESTARIA_X_DOC.ESTRUCTURA, EJECUCION_PRESUPUESTARIA_X_DOC.PARTIDA, EJECUCION_PRESUPUESTARIA_X_DOC.CODPRE, TO_CHAR(EJECUCION_PRESUPUESTARIA_X_DOC.FECHA,'DD/MM/YYYY') as FECHA,
			EJECUCION_PRESUPUESTARIA_X_DOC.MES, EJECUCION_PRESUPUESTARIA_X_DOC.NRO_DOCUMENTO, EJECUCION_PRESUPUESTARIA_X_DOC.DESCRIPCION, 
			SUBSTR (EJECUCION_PRESUPUESTARIA_X_DOC.DESCRIPCION, 1,50) as RESUMEN_DOCUMENTO,
			EJECUCION_PRESUPUESTARIA_X_DOC.NOMPRE, EJECUCION_PRESUPUESTARIA_X_DOC.ASIGNACION, EJECUCION_PRESUPUESTARIA_X_DOC.PRECOMPROMISO, EJECUCION_PRESUPUESTARIA_X_DOC.COMPROMISO, EJECUCION_PRESUPUESTARIA_X_DOC.CAUSADO, EJECUCION_PRESUPUESTARIA_X_DOC.PAGADO, EJECUCION_PRESUPUESTARIA_X_DOC.MODIFICADO, EJECUCION_PRESUPUESTARIA_X_DOC.DISPONIBILIDAD
			FROM ".$this->sima.".EJECUCION_PRESUPUESTARIA_X_DOC EJECUCION_PRESUPUESTARIA_X_DOC where ".$criteria." order by EJECUCION_PRESUPUESTARIA_X_DOC.CODPRE";
			//echo $sql;
			$data=$this->query($sql);
		break;	
		}
		return $data;
	}

	function bancoCupon(){
		$sql="select Banco.id,
			Banco.descripcion
			from bancos as Banco 
			INNER JOIN tarjetas AS Tarjeta ON (Tarjeta.banco_id = Banco.id) 
			INNER JOIN tarjetastipo ON (Tarjeta.tarjetastipo_id = tarjetastipo.id) 
			INNER JOIN maquinas ON (Tarjeta.maquina_id = maquinas.id)
			where  tarjetastipo.asociado='CUPON' 
			group by banco.id,Banco.descripcion";
		$data=$this->query($sql);
		return $data;
	}
					  
}
?>
