<?php
class Tipodocumento extends AppModel
{
    var $name = 'Tipodocumento';
    var $useTable = 'tipodocumentos';
/*   	var $validate = array(
      'nombre' => array(
	  	'rule' => 'empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
    var $hasMany = array('Documento' =>
                         array('className'   => 'Documento',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'tipodocumento_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )
                  );

	function eliminar($nro_registro=0,$id=0){
		$mensaje='';
		if($nro_registro<=0){
			if ($this->delete($id)){
				$mensaje='El Tipo de Documento ha sido Eliminado.';
			}		
		 }else{
			$mensaje='El Tipo de Documento no ha sido Eliminado, porque posee registro asociados.'; 
		 }
		return $mensaje; 
	}             			  
				  
}
?>
