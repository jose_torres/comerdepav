<?php
class Funcione extends AppModel
{
    public $name = 'Funcione';
    public $useTable = 'funcion';
    public $actsAs = array('Tree');

    public $belongsTo = array('Grupo' => array('className' => 'Grupo',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'grupo_id'
					  ),'Modulo' => array('className' => 'Modulo',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'modulo_id'
						)
			);
    public $hasMany = array('Configuracion' =>array('className' => 'Configuracion',
                                     'conditions' => '',
                                     'order' => '',
                                     'limit' => '',
                                     'foreignKey' => 'funcion_id',
                                     'dependent' => true,
                                     'exclusive' => false,
                                     'finderQuery' => '',
                                     'fields' => '',
                                     'offset' => '',
                                     'counterQuery' => ''
                                     )
                             );			

        
   public function getArbol(){
        $params = array(
        'recursive' => -1,
        //'fields' => 'Oficina.id, Oficina.siglas, Oficina.descripcion, Oficina.modified, Oficina.parent_id',
		);
        $var = array();
        $datos  = $this->find('threaded',$params);
       // print_r($datos);
        foreach ($datos as $row) {
            $item = $row['Funcione'];            
            $var[$item['id']]['children'] = $row['children'];
        }
        return $var;
    }
}
?>
