<?php
class Venta extends AppModel
{
    public $name = 'Venta';
    public $primaryKey = 'codventa';
    /**
 * Use database config
 *
 * @var string
 */
    public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'ventas';
/*
 *
 * */
    public $belongsTo = array('Cliente' => array('className' => 'Cliente',
  						'conditions' => 'Cliente.codsucursal=Venta.codclientesucursal and Cliente.codcliente=Venta.codcliente',
						'order' => '',
						'foreignKey' => 'codcliente',
						'fields'=> 'Cliente.codcliente,Cliente.rif,Cliente.descripcion,Cliente.direccion,Cliente.diasdecredito'
					  ),
					  'Vendedore' => array('className' => 'Vendedore',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codvendedor',
						'fields'=> 'Vendedore.codvendedor,Vendedore.descripcion'
					  )					  					  
			);

    public $hasMany = array('Ventaproducto' =>
                     array('className'   => 'Ventaproducto',
                           'conditions'  => '',
                           'order'       => '',
                           'limit'       => '',
                           'foreignKey'  => 'codventa',
                     ),
                     'Ventaspago' =>
                     array('className'   => 'Ventaspago',
                           'conditions'  => '',
                           'order'       => '',
                           'limit'       => '',
                           'foreignKey'  => 'codventa',

                     )

              );			

    function ajustarDatos($opcion='pdf',$datos){
        switch ($opcion) {
        case 'pdf':
            if(!isset($datos['tipoventa'])){
                $datos['tipoventa']=array();
            }else{
                if($datos['tipoventa']==''){
                        $datos['tipoventa']=array();
                }
            }   
            if(!isset($datos['cliente_id'])){
                $datos['cliente_id']=array();    
            }else{
                if($datos['cliente_id']==''){
                    $datos['cliente_id']=array();
                }
            }
            if(!isset($datos['vendedore_id'])){
                $datos['vendedore_id']=array();    
            }else{
                if($datos['vendedore_id']==''){
                    $datos['vendedore_id']=array();
                }
            }
            if(!isset($datos['departamento_id'])){
                 $datos['departamento_id']=array();   
            }else{
                if($datos['departamento_id']==''){
                    $datos['departamento_id']=array();
                }
            }    

            $arreglo = array(0=>0);
            $datos['client']=array_merge($arreglo,$datos['cliente_id']);
            $datos['tipoven']=$datos['tipoventa'];
            $datos['vend']=array_merge($arreglo,$datos['vendedore_id']);
            $datos['dpto']=array_merge($arreglo,$datos['departamento_id']);
            $datos['fechadesde']=$datos['desde'];$datos['fechahasta']=$datos['hasta'];
        break;
        case 'impuesto':
            $datos['mes']=$datos['mes']['month'];
            $datos['year']=$datos['year']['year'];

        break;
        case 'asociar':
            $cont=0;
            list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
            $correlativo = $datos['Reporte']['correlativo'];
            for ($i = 1; $i <= $datos['Venta']['totales']; $i++) {
                if($datos['Seleccion'][$i]['chequeo']>0){
                    $correlativo = $correlativo + 1;
                    $datos['Ventas'][$i]['codventa']=$datos['Valor'][$i]['codventa'];
                    $datos['Ventas'][$i]['codsucursal']=$datos['Valor'][$i]['codsucursal'];
                    $datos['Ventas'][$i]['codmaquina']=$maquina;
                    $datos['Ventas'][$i]['memoria']=$memoria;
                    $datos['Ventas'][$i]['correlativo']=$correlativo;
                    $datos['Ventas'][$i]['estatusz']='Asociado';
                    $datos['Ventas'][$i]['usuariow']='lrivero';
                }

            }
            $datos['Maquina']['codmaquina']=$maquina;	
            $datos['Maquina']['correlativo']=$correlativo;	

        break;
        case 'cerrar':
                $cont=0;
                list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
                //$correlativo = $datos['Reporte']['correlativo'];
                for ($i = 1; $i <= $datos['Venta']['totales']; $i++) {
                    if($datos['Seleccion'][$i]['chequeo']>0){
            //		$correlativo = $correlativo + 1;
                        $datos['Ventas'][$i]['codventa']=$datos['Valor'][$i]['codventa'];
                        $datos['Ventas'][$i]['codsucursal']=$datos['Valor'][$i]['codsucursal'];
                        $datos['Ventas'][$i]['codmaquina']=$maquina;
                        $datos['Ventas'][$i]['memoria']=$memoria;
        //		$datos['Ventas'][$i]['correlativo']=$correlativo;
                        $datos['Ventas'][$i]['estatusz']='Cerrado';
                        $datos['Ventas'][$i]['usuariow']='lrivero';
                    }

                }			
        break;
        case 'devolver':
                $cont=0;
                list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
                $correlativo = $datos['Reporte']['correlativo'];
                for ($i = 1; $i <= $datos['Venta']['totales']; $i++) {
                    if($datos['Seleccion'][$i]['chequeo']>0){
                    //	$correlativo = $correlativo + 1;
                        $datos['Ventas'][$i]['codventa']=$datos['Valor'][$i]['codventa'];
                        $datos['Ventas'][$i]['codsucursal']=$datos['Valor'][$i]['codsucursal'];
                        $datos['Ventas'][$i]['codmaquina']=0;
                        $datos['Ventas'][$i]['memoria']=0;
                        $datos['Ventas'][$i]['correlativo']=0;
                        $datos['Ventas'][$i]['estatusz']='Abierto';
                        $datos['Ventas'][$i]['usuariow']='lrivero';
                    }

                }
                $datos['Maquina']['codmaquina']=$maquina;	
                $datos['Maquina']['correlativo']=$correlativo - $datos['Venta']['totales'];	

        break;
        case 'abrir':
            $cont=0;
            list($maquina,$memoria)=explode('-',$datos['Reporte']['maquina_id']);
            //$correlativo = $datos['Reporte']['correlativo'];
            for ($i = 1; $i <= $datos['Venta']['totales']; $i++) {
                if($datos['Seleccion'][$i]['chequeo']>0){
                    $datos['Ventas'][$i]['codventa']=$datos['Valor'][$i]['codventa'];
                    $datos['Ventas'][$i]['codsucursal']=$datos['Valor'][$i]['codsucursal'];
                    $datos['Ventas'][$i]['vf']='SI';					
                }								
            }			
        break;	
        }
        return $datos;
    }

    function reporte($datos=array(),$criteria='',$opcion=0){
            $hasta=$this->anomesdia($datos['fechahasta']);
            $desde=$this->anomesdia($datos['fechadesde']);
            $data=array();
            switch ($opcion) {
            case 0:
                    $order='Venta.estatus desc,Venta.codventa';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    $fields = "Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, Venta.baseimp2, Venta.poriva2, Venta.estatus, Vendedore.descripcion, sum(Venta.ivaimp2 +Venta.ivaimp2+Venta.ivaimp3) as ivaimp2, 	sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo";
                    $group= "Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, Venta.baseimp2, Venta.poriva2, Venta.estatus, Vendedore.descripcion";
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            break;		
            case 1:			
                    $order=' Venta.codventa';
                    $criteria=" PAGOS_VENTAS.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and PAGOS_VENTAS.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'PAGOS_VENTAS.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'PAGOS_VENTAS.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_VENTAS.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;
                    $sql="select PAGOS_VENTAS.codventa,PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.fecha, PAGOS_VENTAS.baseimp2, PAGOS_VENTAS.ivaimp2, PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago,PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Monto_Pagado, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco, PAGOS_VENTAs.coddevolucion, PAGOS_VENTAS.observacion from (
                    select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' THEN VTP.numerotransferencia WHEN VTP.tipopago='CHEQUE' THEN VTP.numerocheque  ELSE 'S/N' END as Nropago, CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, DVT.coddevolucion,DVT.observacion from ventas VT 
                    inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
                    inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
                    left join ventaspagos VTP on (VTP.codventa=VT.codventa)
                    left join devolucionventa DVT on (DVT.codventa=VT.codventa)
                    left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
                    where VTP.tipopago<>'CREDITO'
                    union all
                    select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,VT.estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, CAP.numero as Nropago,CAP.banco, DVT.coddevolucion,DVT.observacion from ventas VT 
                    inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
                    inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
                    left join ventaspagos VTP on (VTP.codventa=VT.codventa)
                    left join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
                    left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
                    left join devolucionventa DVT on (DVT.codventa=VT.codventa)
                    where VTP.tipopago='CREDITO'
                    ) PAGOS_VENTAS
                    where 1=1 and ".$criteria."
                    order by PAGOS_VENTAS.estatus, PAGOS_VENTAS.fecha, PAGOS_VENTAS.descripcion, PAGOS_VENTAS.numerofactura;";

                    $data = $this->query($sql);
            break;
            case 2:			
                    $order='Venta.estatus desc,Venta.codventa';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;				
                    //echo $criteria;
                    $sql="select Venta.codventa, Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion  as Cliente__descripcion,  Venta.montoexento, Venta.estatus,Venta.fecha,Venta.hora, Vendedore.descripcion as Vendedore__descripcion, Venta.porimp2,
                    (Venta.ivaimp2+Venta.ivaimp1+Venta.ivaimp3) as ivaimp2, sum( Ventaproducto.cantidad::numeric*Ventaproducto.precio::numeric) as baseimp2, sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo from ventas as Venta
                    inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
                    inner join (select codvendedor,descripcion from vendedores group by codvendedor,descripcion) Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
                    inner join ( select codproducto,precio,cantidad,costopromedio,codventa from ventasproductos group by codproducto,precio,cantidad,costopromedio,codventa ) Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
                    where ".$criteria."
                    group by Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, 
                     Venta.porimp2,Venta.montoexento, Venta.estatus,Venta.fecha,Venta.hora, Vendedore.descripcion,Venta.ivaimp2,Venta.ivaimp1,Venta.ivaimp3
                    order by Venta.codventa";
                    //echo $sql;
                    $data = $this->query($sql);
            break;
            case 3:
                    $order='Venta.numerofactura,Venta.fecha';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;

                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            break;
            case 4:			
                    $order='Venta.estatus desc,Venta.codventa';
                    $criteria=" fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and fecha <= '".$hasta." 23:59:59' ";			
                    /*$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;*/				
                    //echo $criteria;
                    $sql="select fecha,sum(Contado) as contado, sum(MontoNetoCredito) as MontoNetoCredito,sum(MontoIvaCredito) as MontoIvaCredito, sum(MontoTotalCredito) as MontoTotalCredito, sum(Costo) as costo, sum(Cobros) as cobros, sum(CobrosDirectos) as CobrosDirectos,sum(NotaCredito) as notacredito	from
                    (
                    select Venta.fecha,
sum(CASE WHEN Venta.estatus::text = 'P'::text  THEN (Venta.baseimp1 + Venta.baseimp2 + Venta.baseimp3) + Venta.montoexento ELSE 0::numeric END) as Contado, 
sum(CASE WHEN Venta.estatus::text = 'A'::text  THEN (Venta.baseimp1 + Venta.baseimp2 + Venta.baseimp3) + Venta.montoexento ELSE 0::numeric END) as MontoNetoCredito, 
sum(CASE WHEN Venta.estatus::text <> 'E'::text  THEN (Venta.ivaimp1 + Venta.ivaimp2 + Venta.ivaimp3) ELSE 0::numeric END) as MontoIvaCredito, 
sum(CASE WHEN Venta.estatus::text = 'A'::text  THEN (Venta.baseimp1 + Venta.baseimp2 + Venta.baseimp3) + Venta.montoexento + 
(Venta.ivaimp1 + Venta.ivaimp2 + Venta.ivaimp3) ELSE 0::numeric END) as MontoTotalCredito, 
0 as Costo, 
0 as Cobros, 
0 as CobrosDirectos, 
0 as NotaCredito from ventas as Venta 
left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc)  
group by Venta.fecha 
union
select Venta.fecha, 0 as contado, 0 as montonetocredito, 0 as montoivacredito,0 as montototalcredito, 
sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo, 
0 as Cobros, 
0 as CobrosDirectos, 
0 as NotaCredito from ventas as Venta 
inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura)  
left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc)  
where  Venta.estatus::text <> 'E'::text
group by Venta.fecha
                    union all
select  FechaPago, 0 as contado, 0 as montonetocredito, 0 as montoivacredito,0 as montototalcredito,  
0 as Costo, 
sum(baseimp2) + sum(ivaimp2) - sum(retencion)as Cobros, 
0 as CobrosDirectos, 
0 as NotaCredito
from ( 
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, (VT.baseimp2+VT.baseimp1+VT.baseimp3) as baseimp2, 
(VT.ivaimp2+VT.ivaimp1+VT.ivaimp3) as ivaimp2, VT.retencion, VT.codvendedor, VD.descripcion as Nombre_Vendedor, VT.estatus, 
upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,HCXC.fvencimiento as FechaVencimiento,CCP.fecha as FechaPago,
BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,CCA.concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codventa and CCP.tipomovimientocc='VENTAS') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
 left join (select  codmovimiento,tipomovimiento,fvencimiento from cuentasporcobrar
union
select  codmovimiento,tipomovimiento,fvencimiento from h_cuentasporcobrar) HCXC 
on (VT.codventa = HCXC.codmovimiento and HCXC.tipomovimiento='VENTAS')
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) 
where VTP.tipopago='CREDITO' and upper(CAP.tipopago) <> 'RETENCION'
union all 
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 
(VTP.monto ) as baseimp2,0 as ivaimp2,0 as retencion,VT.codvendedor,
VD.descripcion as Nombre_Vendedor, VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,
VT.fecha as FechaVencimiento,VT.fecha as FechaPago, CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' 
THEN VTP.numerotransferencia WHEN VTP.tipopago='CHEQUE' THEN VTP.numerocheque ELSE 'S/N' END as Nropago, 
CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, 
'PAGO DE FACTURA AL CONTADO' as concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa) 
left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
where VTP.tipopago<>'CREDITO' and VT.estatus<>'E' and VT.estatus='P' 
union  
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 
0 as baseimp2,0 as ivaimp2,RVD.montoretenido as retencion,VT.codvendedor,
VD.descripcion as Nombre_Vendedor, VT.estatus, 'RET' as tipopago,'PAG' as tipomovimientopago ,RVD.montoretenido as Monto_Pagado,
VT.fecha as FechaVencimiento,VT.fecha as FechaPago, RVD.numero as Nropago, 
'' as banco, 
'PAGO DE FACTURA AL CONTADO' as concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join retenciones_ventas_detalles RVD on (RVD.codmovimiento=VT.codventa and RVD.codsucursal=VT.codsucursal) 
where  VT.estatus<>'E' and VT.estatus='P' 
union all  
select VT.codccfactura,VT.nrodocumento, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 
(VT.baseimp2+VT.baseimp1+VT.baseimp3) as baseimp2, (VT.ivaimp2+VT.ivaimp1+VT.ivaimp3) as ivaimp2, 0 as retencion, VT.codvendedor, 
VD.descripcion as Nombre_Vendedor, 'A' as estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,
CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4)|| '-' || BDP.numero as Banco,
CCA.concepto from ccfactura VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codccfactura and CCP.tipomovimientocc='FA') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) 
where upper(CAP.tipopago) <> 'RETENCION' 
union 
select VT.codccfactura,VT.nrodocumento, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 0 as baseimp2, 0 as ivaimp2, CAP.monto as retencion, 
VT.codvendedor, VD.descripcion as Nombre_Vendedor, 'A' as estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, 0 as Monto_Pagado,
CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,
CCA.concepto from ccfactura VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codccfactura and CCP.tipomovimientocc='FA') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) where upper(CAP.tipopago) = 'RETENCION' ) 
factura group by FechaPago
                    union all
                    select NCventas.fecha,0::numeric as Contado,0::numeric as MontoNetoCredito, 0::numeric as MontoIvaCredito, 0::numeric as MontoTotalCredito, 0::numeric as Costo, 0::numeric as Cobros, 0::numeric as CobrosDirectos, 
                    COALESCE(sum(NCventas.montobruto),0) as NotaCredito from ventas as Venta 
                    inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
                    left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
                    left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa and Ventapago.tipopago<>'CREDITO') 
                    left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
                    left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
                    Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc ) 
                    group by NCventas.fecha
                    ) RESUMEN_VENTA
                    where ".$criteria."
                    group by RESUMEN_VENTA.fecha 
                    order by RESUMEN_VENTA.fecha";
                    $data = $this->query($sql);
            break;
            case 41:			
                    $order='Venta.estatus desc,Venta.codventa';
                    $criteria=" fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and fecha <= '".$hasta." 23:59:59' ";			
                    /*$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;*/				
                    //echo $criteria;
                    $sql="select fecha,sum(Contado) as contado, sum(MontoNetoCredito) as MontoNetoCredito,sum(MontoIvaCredito) as MontoIvaCredito, sum(MontoTotalCredito) as MontoTotalCredito, sum(Costo) as costo, sum(Cobros) as cobros, sum(CobrosDirectos) as CobrosDirectos,sum(NotaCredito) as notacredito	from
                    (
                    select Venta.fecha,
sum(CASE WHEN Venta.estatus::text = 'P'::text  THEN (Venta.baseimp1 + Venta.baseimp2 + Venta.baseimp3) + Venta.montoexento ELSE 0::numeric END) as Contado, 
sum(CASE WHEN Venta.estatus::text <> 'P'::text  THEN (Venta.baseimp1 + Venta.baseimp2 + Venta.baseimp3) + Venta.montoexento ELSE 0::numeric END) as MontoNetoCredito, 
sum(CASE WHEN Venta.estatus::text <> 'P'::text  THEN (Venta.ivaimp1 + Venta.ivaimp2 + Venta.ivaimp3) ELSE 0::numeric END) as MontoIvaCredito, 
sum(CASE WHEN Venta.estatus::text <> 'P'::text  THEN (Venta.baseimp1 + Venta.baseimp2 + Venta.baseimp3) + Venta.montoexento + 
(Venta.ivaimp1 + Venta.ivaimp2 + Venta.ivaimp3) ELSE 0::numeric END) as MontoTotalCredito, 
0 as Costo, 
0 as Cobros, 
0 as CobrosDirectos, 
0 as NotaCredito from ventas as Venta 
left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc)  
group by Venta.fecha 
union
select DV.fecha::date,
sum(CASE WHEN Venta.estatus::text = 'P'::text  THEN (DV.baseimp1 + DV.baseimp2 + DV.baseimp3) + DV.montoexento ELSE 0::numeric END) as Contado, 
sum(CASE WHEN Venta.estatus::text <> 'P'::text  THEN (DV.baseimp1 + DV.baseimp2 + DV.baseimp3) + DV.montoexento ELSE 0::numeric END)*-1 as MontoNetoCredito, 
sum(CASE WHEN Venta.estatus::text <> 'P'::text  THEN (DV.ivaimp1 + DV.ivaimp2 + DV.ivaimp3) ELSE 0::numeric END)*-1 as MontoIvaCredito, 
sum(CASE WHEN Venta.estatus::text <> 'P'::text  THEN ((DV.baseimp1 + DV.baseimp2 + DV.baseimp3)::numeric + DV.montoexento::numeric + 
(DV.ivaimp1 + DV.ivaimp2 + DV.ivaimp3)::numeric) ELSE 0::numeric END)*-1 as MontoTotalCredito, 
0 as Costo, 
0 as Cobros, 
0 as CobrosDirectos, 
0 as NotaCredito from devolucionventa DV  
inner join ventas as Venta on (DV.codventa=Venta.codventa) 
group by DV.fecha::date 
union

select Venta.fecha, 0 as contado, 0 as montonetocredito, 0 as montoivacredito,0 as montototalcredito, 
sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo, 
0 as Cobros, 
0 as CobrosDirectos, 
0 as NotaCredito from ventas as Venta 
inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura)  
left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc)  
where  Venta.estatus::text <> 'E'::text
group by Venta.fecha
                    union all
select  FechaPago, 0 as contado, 0 as montonetocredito, 0 as montoivacredito,0 as montototalcredito,  
0 as Costo, 
sum(baseimp2) + sum(ivaimp2) - sum(retencion)as Cobros, 
0 as CobrosDirectos, 
0 as NotaCredito
from ( 
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, (VT.baseimp2+VT.baseimp1+VT.baseimp3) as baseimp2, 
(VT.ivaimp2+VT.ivaimp1+VT.ivaimp3) as ivaimp2, VT.retencion, VT.codvendedor, VD.descripcion as Nombre_Vendedor, VT.estatus, 
upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,HCXC.fvencimiento as FechaVencimiento,CCP.fecha as FechaPago,
BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,CCA.concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codventa and CCP.tipomovimientocc='VENTAS') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
 left join (select  codmovimiento,tipomovimiento,fvencimiento from cuentasporcobrar
union
select  codmovimiento,tipomovimiento,fvencimiento from h_cuentasporcobrar) HCXC 
on (VT.codventa = HCXC.codmovimiento and HCXC.tipomovimiento='VENTAS')
left join bancodeposito BDP on (CCA.nrodocumento::integer=BDP.nrodocumento) 
where VTP.tipopago='CREDITO' and upper(CAP.tipopago) <> 'RETENCION'
union all 
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 
(VTP.monto ) as baseimp2,0 as ivaimp2,0 as retencion,VT.codvendedor,
VD.descripcion as Nombre_Vendedor, VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,
VT.fecha as FechaVencimiento,VT.fecha as FechaPago, CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' 
THEN VTP.numero WHEN VTP.tipopago='CHEQUE' THEN VTP.numero ELSE 'S/N' END as Nropago, 
CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, 
'PAGO DE FACTURA AL CONTADO' as concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa) 
left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
where VTP.tipopago<>'CREDITO' and VT.estatus<>'E' and VT.estatus='P' 
union  
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 
0 as baseimp2,0 as ivaimp2,RVD.montoretenido as retencion,VT.codvendedor,
VD.descripcion as Nombre_Vendedor, VT.estatus, 'RET' as tipopago,'PAG' as tipomovimientopago ,RVD.montoretenido as Monto_Pagado,
VT.fecha as FechaVencimiento,VT.fecha as FechaPago, RVD.numero as Nropago, 
'' as banco, 
'PAGO DE FACTURA AL CONTADO' as concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join retenciones_ventas_detalles RVD on (RVD.codmovimiento=VT.codventa and RVD.codsucursal=VT.codsucursal) 
where  VT.estatus<>'E' and VT.estatus='P' 
union all  
select VT.codccfactura,VT.nrodocumento, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 
(VT.baseimp2+VT.baseimp1+VT.baseimp3) as baseimp2, (VT.ivaimp2+VT.ivaimp1+VT.ivaimp3) as ivaimp2, 0 as retencion, VT.codvendedor, 
VD.descripcion as Nombre_Vendedor, 'A' as estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,
CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4)|| '-' || BDP.numero as Banco,
CCA.concepto from ccfactura VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codccfactura and CCP.tipomovimientocc='FA') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) 
where upper(CAP.tipopago) <> 'RETENCION' 
union 
select VT.codccfactura,VT.nrodocumento, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 0 as baseimp2, 0 as ivaimp2, CAP.monto as retencion, 
VT.codvendedor, VD.descripcion as Nombre_Vendedor, 'A' as estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, 0 as Monto_Pagado,
CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,
CCA.concepto from ccfactura VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codccfactura and CCP.tipomovimientocc='FA') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) where upper(CAP.tipopago) = 'RETENCION' ) 
factura group by FechaPago
                    union all
                    select NCventas.fecha,0::numeric as Contado,0::numeric as MontoNetoCredito, 0::numeric as MontoIvaCredito, 0::numeric as MontoTotalCredito, 0::numeric as Costo, 0::numeric as Cobros, 0::numeric as CobrosDirectos, 
                    COALESCE(sum(NCventas.montobruto),0) as NotaCredito from ventas as Venta 
                    inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
                    left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
                    left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa and Ventapago.tipopago<>'CREDITO') 
                    left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
                    left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
                    Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc ) 
                    group by NCventas.fecha
                    ) RESUMEN_VENTA
                    where ".$criteria."
                    group by RESUMEN_VENTA.fecha 
                    order by RESUMEN_VENTA.fecha";
                    $data = $this->query($sql);
            break;		
            case 5:			
                    $order='';
            //	$criteria=" Cxc.fvencimiento >= '".$desde." 00:00:00' ";
            //	$criteria=$criteria." and Cxc.fvencimiento <= '".$hasta." 23:59:59' ";
                $criteria="  Cxc.fvencimiento <= '".$hasta." 23:59:59' ";
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;				
                    //echo $criteria;
                    $sql="select Venta.fecha, Venta.numerofactura, Cliente.rif as Cliente__rif,
                    Cliente.descripcion Cliente__descripcion,
                    Cxc.fvencimiento,
                    Cxc.fvencimiento - Venta.fecha dias,
                    sum(CASE WHEN Venta.porimp3<0  THEN Ventaproducto.cantidad*(Ventaproducto.precio+ (Ventaproducto.precio * (Ventaproducto.iva+Venta.porimp3)/100)) ELSE Ventaproducto.cantidad*(Ventaproducto.precio+ (Ventaproducto.precio * Ventaproducto.iva/100)) end ) as Monto,
                    sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,
                    COALESCE(sum(Ccpag.monto),0) as Cobros, 
                    COALESCE(sum(NCventas.montobruto),0) as NotaCredito
                    from ventas as Venta
                    inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
                    inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
                    inner join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa)
                    left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc
                    and Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc)
                    left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura)
                    where ".$criteria."
                    group by Venta.fecha,Venta.numerofactura,Cliente.rif, Cliente.descripcion,Cxc.fvencimiento
                    order by Cliente.descripcion,Cxc.fvencimiento";
                    //echo $sql;
                    $data = $this->query($sql);
            break;
            case 6:			
                    $order=' Venta.codventa';
                    $criteria=" PAGOS_VENTAS.fechapago >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and PAGOS_VENTAS.fechapago <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'PAGOS_VENTAS.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'PAGOS_VENTAS.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_VENTAS.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;

                    $sql="select PAGOS_VENTAS.numerofactura,PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.codvendedor, 
PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus,  PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Fecha, PAGOS_VENTAS.FechaVencimiento, 
PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco,
PAGOS_VENTAS.concepto, PAGOS_VENTAS.FechaPago - PAGOS_VENTAS.Fecha dias,
PAGOS_VENTAS.FechaPago - PAGOS_VENTAS.FechaVencimiento dias_venc,
sum(PAGOS_VENTAS.Monto_Pagado) as Monto_Pagado, sum(PAGOS_VENTAS.baseimp2) as baseimp2, sum(PAGOS_VENTAS.ivaimp2) as ivaimp2,
sum(PAGOS_VENTAS.retencion) as retencion 
                    from (
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, (VT.baseimp1 + VT.baseimp2 + VT.baseimp3) as baseimp2,(VT.ivaimp1 + VT.ivaimp2 + VT.ivaimp3) as ivaimp2,VT.retencion,VT.codvendedor,
VD.descripcion as Nombre_Vendedor, VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,
VT.fecha as FechaVencimiento,VT.fecha as FechaPago, CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' 
THEN VTP.numerotransferencia WHEN VTP.tipopago='CHEQUE' THEN VTP.numerocheque ELSE 'S/N' END as Nropago, 
CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, 
'PAGO DE FACTURA AL CONTADO' as concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa) 
left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
where VTP.tipopago<>'CREDITO' 
union all (
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, (VT.baseimp2+VT.baseimp1+VT.baseimp3) as baseimp2, 
(VT.ivaimp2+VT.ivaimp1+VT.ivaimp3) as ivaimp2, VT.retencion, VT.codvendedor, VD.descripcion as Nombre_Vendedor, VT.estatus, 
upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CCP.monto as Monto_Pagado,HCXC.fvencimiento as FechaVencimiento,CCP.fecha as FechaPago,
BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,CCA.concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codventa and CCP.tipomovimientocc='VENTAS' and CCP.tipomovimientopago<>'NC') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
 left join (select  codmovimiento,tipomovimiento,fvencimiento from cuentasporcobrar
union
select  codmovimiento,tipomovimiento,fvencimiento from h_cuentasporcobrar) HCXC 
on (VT.codventa = HCXC.codmovimiento and HCXC.tipomovimiento='VENTAS')
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) 
where VTP.tipopago='CREDITO' and upper(CAP.tipopago) <> 'RETENCION'
union 
select codccfactura,nrodocumento, codcliente,rif,descripcion, fecha, sum(baseimp2) as baseimp2, sum(ivaimp2) as ivaimp2, sum(retencion) as retencion, 
codvendedor, Nombre_Vendedor, estatus, tipopago, tipomovimientopago, sum(Monto_Pagado) as Monto_Pagado, FechaVencimiento, FechaPago, 
Nropago, Banco, concepto from ( 
select VT.codccfactura,VT.nrodocumento, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 
(VT.baseimp2+VT.baseimp1+VT.baseimp3) as baseimp2, (VT.ivaimp2+VT.ivaimp1+VT.ivaimp3) as ivaimp2, 0 as retencion, VT.codvendedor, 
VD.descripcion as Nombre_Vendedor, 'A' as estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CCP.monto as Monto_Pagado,
HCXC.fvencimiento as FechaVencimiento,CCP.fecha as FechaPago, BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,
CCA.concepto from ccfactura VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codccfactura and CCP.tipomovimientocc='FA' and CCP.tipomovimientopago<>'NC') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
left join (select  codmovimiento,tipomovimiento,fvencimiento from cuentasporcobrar
union
select  codmovimiento,tipomovimiento,fvencimiento from h_cuentasporcobrar) HCXC 
on (VT.codccfactura = HCXC.codmovimiento and HCXC.tipomovimiento='FA') 
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) 
where upper(CAP.tipopago) <> 'RETENCION' 
union 
select VT.codccfactura,VT.nrodocumento, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 0 as baseimp2, 0 as ivaimp2, RVD.montoretenido as retencion, 
VT.codvendedor, VD.descripcion as Nombre_Vendedor, 'A' as estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, 0 as Monto_Pagado,
HCXC.fvencimiento as FechaVencimiento,CCP.fecha as FechaPago, BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,
CCA.concepto from ccfactura VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codccfactura and CCP.tipomovimientocc='FA') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join retenciones_ventas_detalles RVD on (VT.codccfactura=RVD.codmovimiento and RVD.tipodocumento='FA')
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
left join (select  codmovimiento,tipomovimiento,fvencimiento from cuentasporcobrar
union
select  codmovimiento,tipomovimiento,fvencimiento from h_cuentasporcobrar) HCXC 
on (VT.codccfactura = HCXC.codmovimiento and HCXC.tipomovimiento='FA')
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) where upper(CAP.tipopago) = 'RETENCION' ) 
factura group by codccfactura,nrodocumento, codcliente,rif,descripcion, fecha, codvendedor, Nombre_Vendedor, estatus, tipopago, 
tipomovimientopago, FechaVencimiento, FechaPago, Nropago, Banco, concepto ) 
union all 
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, (NCV.baseimp1::numeric+NCV.baseimp2::numeric+NCV.baseimp3::numeric)*-1 as baseimp2,(NCV.ivaimp1::numeric+NCV.ivaimp2::numeric+NCV.ivaimp3::numeric)*-1 as ivaimp2, 
NCV.retencion::numeric*-1 as monto_pagado,VT.codvendedor, 
VD.descripcion as Nombre_Vendedor, VT.estatus, upper('DEVOLUCION') as TipoPago, 'NOTA DE CREDITO' AS tipomovimientopago, 
(NCV.montobruto *-1) as Monto_Pagado,CCP.fecha as FechaVencimiento,NCV.fecha as FechaPago, NCV.codnotacredito::varchar as Nropago,
'' as banco,CNC.concepto AS concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura and NCV.codcliente=VT.codcliente)
inner join ccnotacredito CNC on (CNC.codccnotacredito=NCV.codnotacredito)
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codventa and CCP.tipomovimientopago='NC')    
union all 
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2*-1,VT.ivaimp2*-1,DVT.retencion*-1,VT.codvendedor,
VD.descripcion as Nombre_Vendedor, VT.estatus, DTP.tipopago,'PAG' as tipomovimientopago ,(DTP.monto*-1) as Monto_Pagado,
VT.fecha as FechaVencimiento,VT.fecha as FechaPago, DVT.coddevolucion::varchar as Nropago, '' as banco, 'DEVOLUCION DE PAGO DE FACTURA' as concepto 
from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) inner join devolucionventa DVT on (DVT.codventa=VT.codventa) 
inner join devolucionventapago DTP on (DTP.coddevolucion=DVT.coddevolucion) where DTP.tipopago<>'CREDITO'
                    ) PAGOS_VENTAS
                    where 1=1 and  ".$criteria."
                    group by PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.codvendedor, 
PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Fecha, 
PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco, 
PAGOS_VENTAS.concepto
                    order by PAGOS_VENTAS.fechapago, PAGOS_VENTAS.descripcion  ;";
                    //echo $sql;
                    $data = $this->query($sql);
            /*	echo '<pre>';
                    print_r($data);
                    echo '</pre>';
                    die();*/
            break;
            case 61:			
                    $order=' Venta.codventa';
                    $criteria=" PAGOS_VENTAS.fechapago >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and PAGOS_VENTAS.fechapago <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'PAGOS_VENTAS.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'PAGOS_VENTAS.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'PAGOS_VENTAS.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;

                    $sql="select PAGOS_VENTAS.numerofactura,PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.codvendedor, 
PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus,  PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Fecha, PAGOS_VENTAS.FechaVencimiento, 
PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco,
PAGOS_VENTAS.concepto, PAGOS_VENTAS.FechaPago - PAGOS_VENTAS.Fecha dias,
PAGOS_VENTAS.FechaPago - PAGOS_VENTAS.FechaVencimiento dias_venc,
sum(PAGOS_VENTAS.Monto_Pagado) as Monto_Pagado, sum(PAGOS_VENTAS.baseimp2) as baseimp2, sum(PAGOS_VENTAS.ivaimp2) as ivaimp2,
sum(PAGOS_VENTAS.retencion) as retencion 
                    from (
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, (VT.baseimp1 + VT.baseimp2 + VT.baseimp3) as baseimp2,(VT.ivaimp1 + VT.ivaimp2 + VT.ivaimp3) as ivaimp2,VT.retencion,VT.codvendedor,
VD.descripcion as Nombre_Vendedor, VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,
VT.fecha as FechaVencimiento,VT.fecha as FechaPago, CASE WHEN upper(VTP.tipopago)='TRANSFER' or upper(VTP.tipopago)='TDEBITO' or upper(VTP.tipopago)='TRANSFERENCIA' 
THEN VTP.numero WHEN upper(VTP.tipopago)='CHEQUE' THEN VTP.numero ELSE 'S/N' END as Nropago, 
CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, 
'PAGO DE FACTURA AL CONTADO' as concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa) 
left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
where VTP.tipopago<>'CREDITO' 
union all (
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, (VT.baseimp2+VT.baseimp1+VT.baseimp3) as baseimp2, 
(VT.ivaimp2+VT.ivaimp1+VT.ivaimp3) as ivaimp2, VT.retencion, VT.codvendedor, VD.descripcion as Nombre_Vendedor, VT.estatus, 
upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CCP.monto as Monto_Pagado,HCXC.fvencimiento as FechaVencimiento,CCP.fecha as FechaPago,
BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,CCA.concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ventaspagos VTP on (VTP.codventa=VT.codventa) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codventa and CCP.tipomovimientocc='VENTAS' and CCP.tipomovimientopago<>'NC') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
 left join (select  codmovimiento,tipomovimiento,fvencimiento from cuentasporcobrar
union
select  codmovimiento,tipomovimiento,fvencimiento from h_cuentasporcobrar) HCXC 
on (VT.codventa = HCXC.codmovimiento and HCXC.tipomovimiento='VENTAS')
left join bancodeposito BDP on (CCA.nrodocumento::integer=BDP.nrodocumento) 
where VTP.tipopago='CREDITO' and upper(CAP.tipopago) <> 'RETENCION'
union 
select codccfactura,nrodocumento, codcliente,rif,descripcion, fecha, sum(baseimp2) as baseimp2, sum(ivaimp2) as ivaimp2, sum(retencion) as retencion, 
codvendedor, Nombre_Vendedor, estatus, tipopago, tipomovimientopago, sum(Monto_Pagado) as Monto_Pagado, FechaVencimiento, FechaPago, 
Nropago, Banco, concepto from ( 
select VT.codccfactura,VT.nrodocumento, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 
(VT.baseimp2+VT.baseimp1+VT.baseimp3) as baseimp2, (VT.ivaimp2+VT.ivaimp1+VT.ivaimp3) as ivaimp2, 0 as retencion, VT.codvendedor, 
VD.descripcion as Nombre_Vendedor, 'A' as estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CCP.monto as Monto_Pagado,
HCXC.fvencimiento as FechaVencimiento,CCP.fecha as FechaPago, BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,
CCA.concepto from ccfactura VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codccfactura and CCP.tipomovimientocc='FA' and CCP.tipomovimientopago<>'NC') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
left join (select  codmovimiento,tipomovimiento,fvencimiento from cuentasporcobrar
union
select  codmovimiento,tipomovimiento,fvencimiento from h_cuentasporcobrar) HCXC 
on (VT.codccfactura = HCXC.codmovimiento and HCXC.tipomovimiento='FA') 
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) 
where upper(CAP.tipopago) <> 'RETENCION' 
union 
select VT.codccfactura,VT.nrodocumento, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, 0 as baseimp2, 0 as ivaimp2, RVD.montoretenido as retencion, 
VT.codvendedor, VD.descripcion as Nombre_Vendedor, 'A' as estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, 0 as Monto_Pagado,
HCXC.fvencimiento as FechaVencimiento,CCP.fecha as FechaPago, BDP.numero as Nropago, BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,
CCA.concepto from ccfactura VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codccfactura and CCP.tipomovimientocc='FA') 
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago) 
left join retenciones_ventas_detalles RVD on (VT.codccfactura=RVD.codmovimiento and RVD.tipodocumento='FA')
left join ccabono CCA on (CAP.codccabono=CCA.codccabono) 
left join (select  codmovimiento,tipomovimiento,fvencimiento from cuentasporcobrar
union
select  codmovimiento,tipomovimiento,fvencimiento from h_cuentasporcobrar) HCXC 
on (VT.codccfactura = HCXC.codmovimiento and HCXC.tipomovimiento='FA')
left join bancodeposito BDP on (CAP.codccabono=BDP.nrodocumento) where upper(CAP.tipopago) = 'RETENCION' ) 
factura group by codccfactura,nrodocumento, codcliente,rif,descripcion, fecha, codvendedor, Nombre_Vendedor, estatus, tipopago, 
tipomovimientopago, FechaVencimiento, FechaPago, Nropago, Banco, concepto ) 
union all 
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, (NCV.baseimp1::numeric+NCV.baseimp2::numeric+NCV.baseimp3::numeric)*-1 as baseimp2,(NCV.ivaimp1::numeric+NCV.ivaimp2::numeric+NCV.ivaimp3::numeric)*-1 as ivaimp2, 
NCV.retencion::numeric*-1 as monto_pagado,VT.codvendedor, 
VD.descripcion as Nombre_Vendedor, VT.estatus, upper('DEVOLUCION') as TipoPago, 'NOTA DE CREDITO' AS tipomovimientopago, 
(NCV.montobruto *-1) as Monto_Pagado,CCP.fecha as FechaVencimiento,NCV.fecha as FechaPago, NCV.codnotacredito::varchar as Nropago,
'' as banco,CNC.concepto AS concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) 
inner join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura and NCV.codcliente=VT.codcliente)
inner join ccnotacredito CNC on (CNC.codccnotacredito=NCV.codnotacredito)
inner join ccpagadas CCP on (CCP.codmovimientocc=VT.codventa and CCP.tipomovimientopago='NC')    
union all 
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2*-1,VT.ivaimp2*-1,DVT.retencion*-1,VT.codvendedor,
VD.descripcion as Nombre_Vendedor, VT.estatus, DTP.tipopago,'PAG' as tipomovimientopago ,(DTP.monto*-1) as Monto_Pagado,
VT.fecha as FechaVencimiento,VT.fecha as FechaPago, DVT.coddevolucion::varchar as Nropago, '' as banco, 'DEVOLUCION DE PAGO DE FACTURA' as concepto 
from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal) 
inner join vendedores VD on (VD.codvendedor=VT.codvendedor) inner join devolucionventa DVT on (DVT.codventa=VT.codventa) 
inner join devolucionventapago DTP on (DTP.coddevolucion=DVT.coddevolucion) where DTP.tipopago<>'CREDITO'
                    ) PAGOS_VENTAS
                    where 1=1 and  ".$criteria."
                    group by PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.codvendedor, 
PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Fecha, 
PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco, 
PAGOS_VENTAS.concepto
                    order by PAGOS_VENTAS.fechapago, PAGOS_VENTAS.descripcion  ;";
                    //echo $sql;
                    $data = $this->query($sql);

            break;
            case 7:			
                    $order='Venta.estatus desc,Venta.codventa';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;				
                    //echo $criteria;
                    $sql="select Venta.codventa, Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion  as Cliente__descripcion, 
                    Ventaproducto.precio, Ventaproducto.iva*Ventaproducto.precio/100 as ivaimp, Venta.estatus,Venta.fecha, Vendedore.descripcion as Vendedore__descripcion, 
                    Producto.codigo,Producto.nombre,Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad,Ventaproducto.indice,
                    (Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,(Ventaproducto.cantidad*Ventaproducto.precio) as Total 
                    from ventas as Venta
                    inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
                    inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
                    inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
                    inner join producto Producto on (Ventaproducto.codproducto=Producto.codproducto)
                    where ".$criteria."
                    group by Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, 
                    Ventaproducto.precio, Ventaproducto.iva, Venta.estatus,Venta.fecha, Vendedore.descripcion, Producto.codigo, Producto.nombre, Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad, Ventaproducto.indice
                    order by Venta.codventa, Ventaproducto.indice";
                    $data = $this->query($sql);
            break;		
            case 8:
                    $order='Venta.codvendedor,Venta.fecha';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            break;
            case 9:
                    $order='Venta.numerofactura,Venta.fecha';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";
                    $criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Abierto' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            break;
            case 10:
                    $order='Venta.numerofactura,Venta.fecha';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";
                    $criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Asociado' ";	
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            break;
            case 11:
                    $order='Venta.numerofactura,Venta.fecha';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";
                    $criteria=$criteria." and Venta.vf = 'SI' and Venta.estatusz = 'Cerrado' ";	
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            break;
            case 12:
                    $order='Venta.numerofactura,Venta.fecha';
                    $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";
                    $criteria=$criteria." and Venta.vf = 'NO' ";	
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            break;
            case 14:			
                    $order='Venta.estatus desc,Venta.codventa';
                    $criteria=" VT.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and VT.fecha <= '".$hasta." 23:59:59' ";			
            /*	$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;*/				
                    //echo $criteria;
                    $sql="select VT.fecha,VT.hora,VT.codventa,VT.numerofactura,VT.estatus,
                    VT.codcliente,VT.codsucursal,VT.rif,VT.descripcion, VT.poriva,
                    retencion,VT.contado,VT.credito,VT.impuestos,
                    sum(pagoefectivo) as pagoefectivo,sum(pagodebito) as pagodebito,sum(pagocheque) as pagocheque,
                    sum(pagotransfer) as pagotransfer
                    from (
                    select Venta.fecha,Venta.hora,Venta.codventa,Venta.numerofactura,Venta.estatus,
                    CL.codcliente,CL.codsucursal,CL.rif,cl.descripcion, MCC.poriva,
                    CASE WHEN Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E' THEN Venta.retencion ELSE 0 END as retencion,
                    CASE WHEN Venta.estatus='P' or  MCC.poriva::text is null  THEN (Venta.baseimp1+Venta.baseimp2+Venta.baseimp3) ELSE 0 END as contado,
                    CASE WHEN Venta.estatus='A' or MCC.poriva::text is not null THEN (Venta.baseimp1+Venta.baseimp2+Venta.baseimp3) ELSE 0 END as credito, 
                    CASE WHEN Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E' THEN (Venta.ivaimp1+Venta.ivaimp2+Venta.ivaimp3) ELSE 0 END as impuestos,
                    CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and VTP.tipopago='EFECTIVO' THEN VTP.monto  ELSE 0 END as pagoefectivo, 
                    CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and VTP.tipopago='DEBITO' THEN VTP.monto  ELSE 0 END as pagodebito, 
                    CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and VTP.tipopago='CHEQUE' THEN VTP.monto  ELSE 0 END as pagocheque,
                    CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and VTP.tipopago='TRANSFER' THEN VTP.monto  ELSE 0 END as pagotransfer 
                    from ventas Venta
                    inner join clientes CL on (CL.codcliente=Venta.codcliente and CL.codsucursal=Venta.codclientesucursal)
                    left join ventaspagos VTP on (VTP.codventa=Venta.codventa)
                    left join movimientoivacc MCC on (MCC.codmovimiento=Venta.codventa)
                    ) VT
                    where ".$criteria."
                    group by VT.fecha,VT.hora,VT.codventa,VT.numerofactura,VT.estatus,
                    VT.codcliente,VT.codsucursal,VT.rif,VT.descripcion, VT.poriva,
                    retencion,retencion,VT.contado,VT.credito,VT.impuestos
                    order by VT.fecha,VT.hora";
                    $data = $this->query($sql);
            break;
            case 15:			
                    $order=' Venta.codventa';
                    $criteria=" PAGOS_VENTAS.fechapago >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and PAGOS_VENTAS.fechapago <= '".$hasta." 23:59:59' ";			
                    $criteria=$criteria." and ( PAGOS_VENTAS.tipopago like '%".$datos['tipopago']."%' )  ";			
                    //echo $criteria;
                    $sql="select PAGOS_VENTAS.numerofactura,PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, /*PAGOS_VENTAS.fecha, PAGOS_VENTAS.baseimp2, PAGOS_VENTAS.ivaimp2,*/ PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago, PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco,  PAGOS_VENTAS.concepto,sum(PAGOS_VENTAS.Monto_Pagado) Monto_Pagado
                    from (
                    select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,
                    VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, 
                    CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' or VTP.tipopago='DEBITO' THEN VTP.numerotransferencia WHEN VTP.tipopago='CHEQUE' THEN VTP.numerocheque  ELSE 'S/N' END as Nropago, 
                    CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, 'PAGO DE FACTURA AL CONTADO' as concepto 
                    from ventas VT 
                    inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
                    inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
                    inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
                    left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
                    where VTP.tipopago<>'CREDITO'
                    union all
                    select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,
                    VT.estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, 
                    CAP.numero as Nropago,CAP.banco,CCA.concepto from ventas VT 
                    inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
                    inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
                    inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
                    inner join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
                    left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
                    left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
                    where VTP.tipopago='CREDITO'
                    union all
                    select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,
                    VT.estatus, upper('DEVOLUCION') as TipoPago, 'NOTA DE CREDITO' AS tipomovimientopago, (NCV.montobruto *-1) as Monto_Pagado,CCP.fecha as FechaVencimiento,NCV.fecha as FechaPago, 
                    NCV.codnotacredito::varchar as Nropago,'' as banco,'NOTA DE CREDITO A LA FACTURA' AS concepto from ventas VT 
                    inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
                    inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
                    inner join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura and NCV.codcliente=VT.codcliente)
                    --inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
                    left join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
                    --left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
                    --left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
                    --left join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura)
                    union all
                    select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,
                    VT.estatus, DTP.tipopago,'PAG' as tipomovimientopago ,(DTP.monto*-1) as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, 
                    DVT.coddevolucion::varchar as Nropago, 
                    '' as banco, 'DEVOLUCION DE PAGO DE FACTURA' as concepto 
                    from ventas VT 
                    inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
                    inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
                    inner join devolucionventa DVT on (DVT.codventa=VT.codventa)
                    inner join devolucionventapago DTP on (DTP.coddevolucion=DVT.coddevolucion)
                    where DTP.tipopago<>'CREDITO'
                    ) PAGOS_VENTAS
                    where 1=1 and  ".$criteria."
                    group by PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago, PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco, PAGOS_VENTAS.concepto
                    order by PAGOS_VENTAS.fechapago, PAGOS_VENTAS.descripcion  ;";
/*			
$sql="select PAGOS_VENTAS.tipopago,  
PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Fecha, 
PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco,
sum(PAGOS_VENTAS.Monto_Pagado) as Monto_Pagado
from (
select 
CCA.fecha,BDP.fechadeposito as Fechapago, upper(CAP.tipopago) as tipopago, 'PAG'::text as tipomovimientopago,
CB.numerocuenta, CB.codbanco, BDP.numero as Nropago, BDP.nrodocumento,
BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,
sum(CAP.monto) as Monto_Pagado,
sum(BDP.monto) as Monto_deposito
from ccabono CCA
inner join ccabonopago CAP on (CAP.codccabono = CCA.codccabono )
inner join bancodeposito BDP on (CAP.codccabono = BDP.nrodocumento )
inner join cuentasbancarias CB on (trim(CB.numerocuenta) = trim(BDP.codcuenta) )

group by CCA.fecha, BDP.fechadeposito,CAP.tipopago,CB.numerocuenta, CB.codbanco, BDP.numero, BDP.nrodocumento,
BDP.codbanco,BDP.codcuenta
--order by BDP.nrodocumento
) PAGOS_VENTAS
where 1=1 and  ".$criteria."
group by PAGOS_VENTAS.tipopago,  
PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Fecha, 
PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco;";*/
                    //echo $sql;
                    $data = $this->query($sql);
            break;
            case 16:			
                    $order=' Venta.codventa';
                    $criteria=" PAGOS_VENTAS.fechapago >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and PAGOS_VENTAS.fechapago <= '".$hasta." 23:59:59' ";			
                    $criteria=$criteria." and ( PAGOS_VENTAS.tipopago like '%".$datos['tipopago']."%' )  ";			
                    //echo $criteria;		
$sql="select PAGOS_VENTAS.tipopago,  
PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Fecha, 
PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.codbanco, PAGOS_VENTAS.coddepbanco, PAGOS_VENTAS.numerocuenta, PAGOS_VENTAS.codcuenta,
PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco,
sum(PAGOS_VENTAS.Monto_Pagado) as Monto_Pagado
from (
select 
CCA.fecha,BDP.fechadeposito as Fechapago, BDP.coddepbanco, upper(CAP.tipopago) as tipopago, 'PAG'::text as tipomovimientopago,
CB.numerocuenta, CB.codbanco, BDP.numero as Nropago, BDP.nrodocumento, CB.codcuenta,
BDP.codbanco || '-' || substring(BDP.codcuenta from 20 for 4) as Banco,
sum(CAP.monto) as Monto_Pagado,
sum(BDP.monto) as Monto_deposito
from ccabono CCA
inner join ccabonopago CAP on (CAP.codccabono = CCA.codccabono )
inner join bancodeposito BDP on (CAP.codccabono = BDP.nrodocumento )
inner join cuentasbancarias CB on (trim(CB.numerocuenta) = trim(BDP.codcuenta) )

group by CCA.fecha, BDP.fechadeposito, BDP.coddepbanco,CAP.tipopago,CB.numerocuenta, CB.codbanco, BDP.numero, BDP.nrodocumento,
BDP.codbanco, CB.codcuenta, BDP.codcuenta
--order by BDP.nrodocumento
) PAGOS_VENTAS
where 1=1 and  ".$criteria."
group by PAGOS_VENTAS.tipopago,  
PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Fecha, 
PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.codbanco,PAGOS_VENTAS.coddepbanco, PAGOS_VENTAS.numerocuenta, PAGOS_VENTAS.codcuenta, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco;";
                    //echo $sql;
                    $data = $this->query($sql);
            break;
            }
            return $data;
    }

    function reporteImpuestos($datos=array(),$criteria='',$opcion=0){
            //$hasta=$this->anomesdia($datos['fechahasta']);
            //$desde=$this->anomesdia($datos['fechadesde']);
            $data=array();
            switch ($opcion) {
            case 0:
                    $ult_dia = date("d",(mktime(0,0,0,$datos['mes']+1,1,$datos['year'])-1));
                    $order='Venta.numerofactura,Venta.fecha';
                    $criteria=" Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ";			

                    //echo $criteria;
                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            break;
            case 1:			
                    $order='Venta.estatus desc,Venta.codventa';
                    $ult_dia = date("d",(mktime(0,0,0,$datos['mes']+1,1,$datos['year'])-1));
                    $order='Venta.numerofactura,Venta.fecha';
                    $criteria=" Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
                    $criteria=$criteria." and Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ";
            /*	if ($datos['tipofecha']=='FF'){
                            $criteria=" Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
                            $criteria=$criteria." and Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ";
                    }else{
                            $criteria="CASE WHEN Venta.estatus='A' THEN HCXC.fechacobro >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ELSE Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' END";
                            $criteria=$criteria." and CASE WHEN Venta.estatus='A' THEN HCXC.fechacobro <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' ELSE Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' END ";
                    }*/
                    /*$criteria=$criteria." and Cliente.tipodecliente = '".$datos['tipocontribuyente']."' ";*/

                    //echo $criteria;
                    $sql="select Venta.codventa, Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion  as Cliente__descripcion,  Venta.montoexento, Venta.estatus,Venta.fecha, Vendedore.descripcion as Vendedore__descripcion, Ventaproducto.iva as porimp2, Venta.retencion::numeric,
                    sum(Ventaproducto.cantidad::numeric * Ventaproducto.precio::numeric * Ventaproducto.iva::numeric/100::numeric) as ivaimp2, sum( Ventaproducto.cantidad::numeric*Ventaproducto.precio::numeric) as baseimp2, sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo from ventas as Venta
                    inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
                    inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
                    inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
                    left join h_cuentasporcobrar HCXC on (HCXC.codmovimiento=Venta.codventa and trim(HCXC.tipomovimiento)='VENTAS')
                    where ".$criteria."
                    group by Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, 
                     Ventaproducto.iva,Venta.montoexento, Venta.retencion, Venta.estatus,Venta.fecha, vendedore.descripcion
                    order by Venta.codventa";
                    //echo $sql;
                    $data = $this->query($sql);
            break;
            }
            return $data;	
    }

    function ultimaFechaAsociada(){
            $fecha = date('Y-m-d');
            $sql = "select max(fecha) as ultimafecha from ventas where vf='SI' and (estatusz='Asociado' or estatusz='Cerrado');";
            $data = $this->query($sql);
            if (isset($data[0][0]['ultimafecha'])){
                    $fecha = $data[0][0]['ultimafecha'];
            }
            return $fecha;
    }

    function obtenerSucursal(){
        $sucursal = 0;
        $sql = "select distinct codsucursal from ventas";
        $data = $this->query($sql);
        if (isset($data[0][0]['codsucursal'])){
            $sucursal = $data[0][0]['codsucursal'];
        }
        return $sucursal;
    }

    function pagosRealizados($datos=array(),$opcion='contado'){
        switch ($opcion) {
            case 'contado':
            $datos['tipopago']='EFECTIVO';
            $datapago['efectivo']=$this->reporte($datos,'',15);
            $datos['tipopago']='DEBITO';
            $datapago['debito']=$this->reporte($datos,'',15);
            $datos['tipopago']='CHEQUE';
            $datapago['cheque']=$this->reporte($datos,'',15);
            $datos['tipopago']='TRANSFER';
            $datapago['transfer']=$this->reporte($datos,'',15);
            break;
            case 'credito':
            $datos['tipopago']='EFECTIVO';
            $datapago['efectivo']=$this->reporte($datos,'',16);
            $datos['tipopago']='DEBITO';
            $datapago['debito']=$this->reporte($datos,'',16);
            $datos['tipopago']='CHEQUE';
            $datapago['cheque']=$this->reporte($datos,'',16);
            $datos['tipopago']='TRANSFER';
            $datapago['transfer']=$this->reporte($datos,'',16);
            break;
        }
        return $datapago;
    }

    function ventasContado($condicion=''){
        $sql = "select VT.fecha, sum(montobruto) as total from ventas VT where VT.estatus = 'P' ".$condicion."
        group by VT.fecha";
        $data = $this->query($sql);
        return $data;
    }

    function ventasCredito($condicion=''){
        $sql = "select VT.fecha, sum(montobruto) as total from ventas VT where VT.estatus = 'A' ".$condicion."
        group by VT.fecha";
        $data = $this->query($sql);
        return $data;
    }

    function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
            switch ($opcion) {
                    case 'Cuadrediario':
                    $texto = '';
                    $condicion = "  Venta.fecha='".$datos['Cuadrediario']['fecha']."'";
                    $valores = $this->find('all',array('order'=>'Venta.codventa','conditions'=>$condicion,'recursive'=>-1));

                    foreach ($valores as $reg) {
                            $fila = $reg['Venta'];
                            if($fila['recibido']=='' || $fila['recibido']==null){
                                    $fila['recibido']=0;
                            }
                            if($fila['nrocomprobantefiscal']=='' || $fila['nrocomprobantefiscal']==null){
                                    $fila['nrocomprobantefiscal']=0;
                            }
                            $texto = $texto."INSERT INTO conventas (codventa, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal, hora, coddeposito, codusuarioconectado, recibido, estatus, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3,  retencion, nrocomprobantefiscal, vf, codmaquina, memoria, estatusz, usuariow, correlativo) values (";
                            $texto = $texto."".$fila['codventa'].",".$fila['codsucursal'].",'".$fila['numerofactura']."','".$fila['fecha']."',".$fila['codcliente'].",".$fila['codvendedor'].",".$fila['codclientesucursal'].",'".$fila['hora']."',".$fila['coddeposito'].",".$fila['codusuarioconectado'].",".$fila['recibido'].",'".$fila['estatus']."',".$fila['montobruto'].",".$fila['montoexento'].",".$fila['baseimp1'].",".$fila['baseimp2'].",".$fila['baseimp3'].",".$fila['ivaimp1'].",".$fila['ivaimp2'].",".$fila['ivaimp3'].",".$fila['porimp1'].",".$fila['porimp2'].",".$fila['porimp3'].",".$fila['retencion'].",".$fila['nrocomprobantefiscal'].",'".$fila['vf']."',".$fila['codmaquina'].",".$fila['memoria'].",'".$fila['estatusz']."','".$fila['usuariow']."',".$fila['correlativo'].");";
                            //$texto = $texto.'\n';
                    }
                    break;
                    case 'Mcuadrediario':
                    $texto = '';
                    $condicion = "  Venta.fecha='".$datos['Mcuadrediario']['fecha']."'";
                    $valores = $this->find('all',array('order'=>'Venta.codventa','conditions'=>$condicion,'recursive'=>-1));

                    foreach ($valores as $reg) {
                            $fila = $reg['Venta'];
                            if($fila['recibido']=='' || $fila['recibido']==null){
                                    $fila['recibido']=0;
                            }
                            if($fila['nrocomprobantefiscal']=='' || $fila['nrocomprobantefiscal']==null){
                                    $fila['nrocomprobantefiscal']=0;
                            }
                            $texto = $texto."INSERT INTO conventas (codventa, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal, hora, coddeposito, codusuarioconectado, recibido, estatus, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3,  retencion, nrocomprobantefiscal, vf, codmaquina, memoria, estatusz, usuariow, correlativo) values (";
                            $texto = $texto."".$fila['codventa'].",".$fila['codsucursal'].",'".$fila['numerofactura']."','".$fila['fecha']."',".$fila['codcliente'].",".$fila['codvendedor'].",".$fila['codclientesucursal'].",'".$fila['hora']."',".$fila['coddeposito'].",".$fila['codusuarioconectado'].",".$fila['recibido'].",'".$fila['estatus']."',".$fila['montobruto'].",".$fila['montoexento'].",".$fila['baseimp1'].",".$fila['baseimp2'].",".$fila['baseimp3'].",".$fila['ivaimp1'].",".$fila['ivaimp2'].",".$fila['ivaimp3'].",".$fila['porimp1'].",".$fila['porimp2'].",".$fila['porimp3'].",".$fila['retencion'].",".$fila['nrocomprobantefiscal'].",'".$fila['vf']."',".$fila['codmaquina'].",".$fila['memoria'].",'".$fila['estatusz']."','".$fila['usuariow']."',".$fila['correlativo'].");";
                            //$texto = $texto.'\n';
                    }
                    break;				
            }

            return $texto;
    }

    function ultimaFechaPago(){
        $fecha = date('Y-m-d');
        $sql = "select max(fecha) as ultimafecha from ccabono;";
        $data = $this->query($sql);
        if (isset($data[0][0]['ultimafecha'])){
                $fecha = $data[0][0]['ultimafecha'];
        }
        return $fecha;
    }

    function obtenerNroPago($fecha=''){
        $nro = 0;
        if($fecha==''){
                $fecha = $this->ultimaFechaPago();
        }
        $sql = "select count(codccabono) as nro from ccabono where fecha='".$fecha."';";
        $data = $this->query($sql);
        if (isset($data[0][0]['nro'])){
                $nro = $data[0][0]['nro'];
        }
        return $nro;
    }
    
    function resumenVentasPorDpto($datos=array(),$criteria=''){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $data=array();
        $order='Venta.estatus desc,Venta.codventa';
        $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
        $c_dpto=$this->construir_or($datos['dpto'],'Departamento.coddepartamento');
        $criteria=$criteria."  ".$c_dpto;			
        //echo $criteria;
        $sql="select V.coddep,V.descripcion,sum(V.cantidad) as cantidad,sum(V.costo) as Costo,sum(V.total) as venta,sum(V.ivaimp) as iva
        from (
        select Producto.codigo,Producto.nombre, Departamento.codigo as coddep,Departamento.descripcion, Ventaproducto.precio, Ventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio/100 as ivaimp, Venta.estatus,Venta.fecha, 
        Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad,
         (Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,
        (Ventaproducto.cantidad*Ventaproducto.precio) as Total 
        from ventas as Venta
        inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
        inner join producto Producto on (Ventaproducto.codproducto=Producto.codproducto)
        inner join departamentos Departamento on (Departamento.coddepartamento=Producto.coddepartamento)
        where ".$criteria."
        group by Producto.codigo, Producto.nombre,Departamento.codigo,Departamento.descripcion, Ventaproducto.precio, Ventaproducto.iva, Venta.estatus,Venta.fecha,  Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad
        union
select Producto.codigo,Producto.nombre, Departamento.codigo as coddep,Departamento.descripcion,
Ventaproducto.precio, (Devolucionventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio*-1)/100 as ivaimp, Venta.estatus,Devolucionventa.fecha, 
Devolucionventaproducto.cantidad *-1, Ventaproducto.costopromedio,Ventaproducto.unidad,
Devolucionventaproducto.cantidad*Ventaproducto.costopromedio*-1 as Costo,
(Devolucionventaproducto.cantidad*Ventaproducto.precio*-1) as Total 
 from devolucionventa as Devolucionventa
inner join ventas Venta on (Devolucionventa.codventa=Venta.codventa)
inner join (select coddevolucion,codsucursal,codproducto,cantidad from devolucionventaproductos group by coddevolucion,codsucursal,codproducto,cantidad) Devolucionventaproducto on (Devolucionventa.coddevolucion=Devolucionventaproducto.coddevolucion)
inner join producto Producto on (Devolucionventaproducto.codproducto=Producto.codproducto)
inner join departamentos Departamento on (Departamento.coddepartamento=Producto.coddepartamento)
inner join ( select codproducto,precio,cantidad,costopromedio,codventa,iva,unidad from ventasproductos 
group by codproducto,precio,cantidad,costopromedio,codventa,iva,unidad ) Ventaproducto on (Ventaproducto.codventa=Devolucionventa.codventa 
and Devolucionventaproducto.codproducto=Ventaproducto.codproducto)
where ".$criteria."
group by Producto.codigo, Producto.nombre,Departamento.codigo,Departamento.descripcion,
Ventaproducto.precio, Ventaproducto.iva, Venta.estatus,Devolucionventa.fecha,  Devolucionventaproducto.cantidad, 
Ventaproducto.costopromedio,Ventaproducto.unidad
        ) V
        group by V.coddep,V.descripcion
        order by V.coddep";
        $data = $this->query($sql);
        return $data;
    }
    
    function detalladoVentasPorDpto($datos=array(),$criteria=''){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $data=array();
        $order='Venta.estatus desc,Venta.codventa';
        $criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
        $c_dpto=$this->construir_or($datos['dpto'],'Departamento.coddepartamento');
        $criteria=$criteria."  ".$c_dpto;			
        //echo $criteria;
        $sql="select  V.codigo,V.nombre,V.coddep,V.descripcion,sum(V.cantidad) as cantidad,sum(V.costo) as Costo,sum(V.total) as venta,sum(V.ivaimp) as iva
        from (
        select Producto.codigo,Producto.nombre, Departamento.codigo as coddep,Departamento.descripcion,
        Ventaproducto.precio, Ventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio/100 as ivaimp, Venta.estatus,Venta.fecha, 
        Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad,
         (Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,
        (Ventaproducto.cantidad*Ventaproducto.precio) as Total 
        from ventas as Venta
        inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
        inner join producto Producto on (Ventaproducto.codproducto=Producto.codproducto)
        inner join departamentos Departamento on (Departamento.coddepartamento=Producto.coddepartamento)
        where ".$criteria."
        group by Producto.codigo, Producto.nombre,Departamento.codigo,Departamento.descripcion,
        Ventaproducto.precio, Ventaproducto.iva, Venta.estatus,Venta.fecha,  Ventaproducto.cantidad, 
        Ventaproducto.costopromedio,Ventaproducto.unidad
        union
select Producto.codigo,Producto.nombre, Departamento.codigo as coddep,Departamento.descripcion,
Ventaproducto.precio, (Devolucionventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio*-1)/100 as ivaimp, Venta.estatus,Devolucionventa.fecha, 
Devolucionventaproducto.cantidad*-1, Ventaproducto.costopromedio,Ventaproducto.unidad,
Devolucionventaproducto.cantidad*Ventaproducto.costopromedio*-1 as Costo,
(Devolucionventaproducto.cantidad*Ventaproducto.precio*-1) as Total 
 from devolucionventa as Devolucionventa
inner join ventas Venta on (Devolucionventa.codventa=Venta.codventa)
inner join (select coddevolucion,codsucursal,codproducto,cantidad from devolucionventaproductos group by coddevolucion,codsucursal,codproducto,cantidad) Devolucionventaproducto on (Devolucionventa.coddevolucion=Devolucionventaproducto.coddevolucion)
inner join producto Producto on (Devolucionventaproducto.codproducto=Producto.codproducto)
inner join departamentos Departamento on (Departamento.coddepartamento=Producto.coddepartamento)
inner join ( select codproducto,precio,cantidad,costopromedio,codventa,iva,unidad from ventasproductos 
group by codproducto,precio,cantidad,costopromedio,codventa,iva,unidad ) Ventaproducto on (Ventaproducto.codventa=Devolucionventa.codventa 
and Devolucionventaproducto.codproducto=Ventaproducto.codproducto)
where ".$criteria."
group by Producto.codigo, Producto.nombre,Departamento.codigo,Departamento.descripcion,
Ventaproducto.precio, Ventaproducto.iva, Venta.estatus,Devolucionventa.fecha,  Devolucionventaproducto.cantidad, 
Ventaproducto.costopromedio,Ventaproducto.unidad
        ) V
        group by V.codigo,V.nombre,V.coddep,V.descripcion
        order by V.coddep,V.codigo";
        $data = $this->query($sql);
        return $data;
    }
    
    function resumenIvaVentas($datos=array(),$criteria=''){
        //$hasta=$this->anomesdia($datos['fechahasta']);
        //$desde=$this->anomesdia($datos['fechadesde']);
        $data=array();
        $ult_dia = date("d",(mktime(0,0,0,$datos['mes']+1,1,$datos['year'])-1));
        $criteria=" Venta.fecha >= '".$datos['year']."-".$datos['mes']."-01 00:00:00' ";
        $criteria=$criteria." and Venta.fecha <= '".$datos['year']."-".$datos['mes']."-".$ult_dia." 23:59:59' "; 		
        if(isset($datos['dpto'])){
            $c_dpto=$this->construir_or($datos['dpto'],'Departamento.coddepartamento');
            $criteria=$criteria."  ".$c_dpto;			
        }
        
        //echo $criteria;
        $sql="select V.fecha::date,V.iva,sum(V.total) as venta,sum(V.ivaimp) as ivaimp
        from (
        select Producto.codigo,Producto.nombre, Departamento.codigo as coddep,Departamento.descripcion, Ventaproducto.precio, Ventaproducto.iva, Ventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio/100 as ivaimp, Venta.estatus,Venta.fecha::date, 
        Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad,
         (Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,
        (Ventaproducto.cantidad*Ventaproducto.precio) as Total 
        from ventas as Venta
        inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
        inner join producto Producto on (Ventaproducto.codproducto=Producto.codproducto)
        inner join departamentos Departamento on (Departamento.coddepartamento=Producto.coddepartamento)
        where ".$criteria."
        group by Producto.codigo, Producto.nombre,Departamento.codigo,Departamento.descripcion, Ventaproducto.precio, Ventaproducto.iva, Venta.estatus,Venta.fecha,  Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad
        union
select Producto.codigo,Producto.nombre, Departamento.codigo as coddep,Departamento.descripcion,
Ventaproducto.precio, Ventaproducto.iva, (Devolucionventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio*-1)/100 as ivaimp, Venta.estatus,Devolucionventa.fecha::date, 
Devolucionventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad,
Devolucionventaproducto.cantidad*Ventaproducto.costopromedio*-1 as Costo,
(Devolucionventaproducto.cantidad*Ventaproducto.precio*-1) as Total 
 from devolucionventa as Devolucionventa
inner join ventas Venta on (Devolucionventa.codventa=Venta.codventa)
inner join (select coddevolucion,codsucursal,codproducto,cantidad from devolucionventaproductos group by coddevolucion,codsucursal,codproducto,cantidad) Devolucionventaproducto on (Devolucionventa.coddevolucion=Devolucionventaproducto.coddevolucion)
inner join producto Producto on (Devolucionventaproducto.codproducto=Producto.codproducto)
inner join departamentos Departamento on (Departamento.coddepartamento=Producto.coddepartamento)
inner join ( select codproducto,precio,cantidad,costopromedio,codventa,iva,unidad from ventasproductos 
group by codproducto,precio,cantidad,costopromedio,codventa,iva,unidad ) Ventaproducto on (Ventaproducto.codventa=Devolucionventa.codventa 
and Devolucionventaproducto.codproducto=Ventaproducto.codproducto)
where ".$criteria."
group by Producto.codigo, Producto.nombre,Departamento.codigo,Departamento.descripcion,
Ventaproducto.precio, Ventaproducto.iva, Venta.estatus,Devolucionventa.fecha,  Devolucionventaproducto.cantidad, 
Ventaproducto.costopromedio,Ventaproducto.unidad
        ) V
        group by V.fecha,V.iva
        order by V.fecha";
 //       echo $sql;
        $data = $this->query($sql);
        return $data;
    }
//-------------------------------------------------------------------------
    function resumenVentas($datos= array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" V.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and V.fecha <= '".$hasta." 23:59:59' ";
        if(isset($datos['codsucursal'])){
            if ($datos['codsucursal']==''){
                $datos['codsucursal']= 0;
            }			
            $criteria=$criteria." and V.codsucursal=".$datos['codsucursal']." ";
        }		

        $sql = "select V.fecha, V.codsucursal,  sum(V.ivaimp) as iva, sum(costo) as costo, sum(total) as neto,sum(total+ivaimp) as total   
        from (
        select  Venta.numerofactura,Venta.fecha::date,Venta.codsucursal,Ventaproducto.precio, Ventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio/100 as ivaimp, Venta.estatus, 
        Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad, 'V' as Tipo,
         (Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,
        (Ventaproducto.cantidad*Ventaproducto.precio) as Total 
        from ventas as Venta
        inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa and Ventaproducto.codsucursal=Venta.codsucursal)
        group by  Venta.numerofactura,Ventaproducto.precio,Venta.codsucursal, Ventaproducto.iva, Venta.estatus,Venta.fecha::date,  Ventaproducto.cantidad, Ventaproducto.costopromedio,Ventaproducto.unidad
        union
        select 
        Venta.numerofactura,Devolucionventa.fecha::date,Devolucionventa.codsucursal,Ventaproducto.precio, (Devolucionventaproducto.cantidad*Ventaproducto.iva*Ventaproducto.precio*-1)/100 as ivaimp, Venta.estatus, 
        Devolucionventaproducto.cantidad *-1, Ventaproducto.costopromedio,Ventaproducto.unidad, 'D' as Tipo,
        Devolucionventaproducto.cantidad*Ventaproducto.costopromedio*-1 as Costo,
        (Devolucionventaproducto.cantidad*Ventaproducto.precio*-1) as Total 
         from devolucionventa as Devolucionventa
        inner join ventas Venta on (Devolucionventa.codventa=Venta.codventa  and Devolucionventa.codsucursal=Venta.codsucursal)
        inner join (select coddevolucion,codsucursal,codproducto,cantidad from devolucionventaproductos group by coddevolucion,codsucursal,codproducto,cantidad) 
        Devolucionventaproducto on (Devolucionventa.coddevolucion=Devolucionventaproducto.coddevolucion and Devolucionventa.codsucursal=Devolucionventaproducto.codsucursal)
        inner join ( select codproducto,precio,cantidad,costopromedio,codventa,iva,unidad from ventasproductos 
        group by codproducto,precio,cantidad,costopromedio,codventa,iva,unidad ) Ventaproducto on (Ventaproducto.codventa=Devolucionventa.codventa 
        and Devolucionventaproducto.codproducto=Ventaproducto.codproducto)

        group by 
        Venta.numerofactura,Ventaproducto.precio,Devolucionventa.codsucursal, Ventaproducto.iva, Venta.estatus,Devolucionventa.fecha::date,  Devolucionventaproducto.cantidad, 
        Ventaproducto.costopromedio,Ventaproducto.unidad
        ) as V
        where ".$criteria."
        group by V.fecha,V.codsucursal
        order by V.fecha,V.codsucursal";
        $data = $this->query($sql);
        return $data;
    }
    
    function resumenPagosVentas($datos= array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" V.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and V.fecha <= '".$hasta." 23:59:59' ";
        if(isset($datos['codsucursal'])){
            if ($datos['codsucursal']==''){
                $datos['codsucursal']= 0;
            }			
            $criteria=$criteria." and V.codsucursal=".$datos['codsucursal']." ";
        }		

        $sql = "select V.fecha,V.codsucursal, V.tipopago, sum(Monto_Pagado) as monto
from (
select  Venta.numerofactura,Venta.fecha::date,Venta.codsucursal, Venta.estatus, 
VTP.tipopago,  'V' as Tipo, sum(VTP.monto) as Monto_Pagado
from ventas as Venta
inner join ventaspagos VTP on (VTP.codventa=Venta.codventa and VTP.codsucursal=Venta.codsucursal)
group by  Venta.numerofactura,Venta.codsucursal,  Venta.estatus,
Venta.fecha::date, VTP.tipopago
union
select 
Venta.numerofactura,Devolucionventa.fecha::date,Devolucionventa.codsucursal, Venta.estatus, 
VTP.tipopago, 'D' as Tipo, sum(VTP.monto*-1) as Monto_Pagado
 from devolucionventa as Devolucionventa
inner join ventas Venta on (Devolucionventa.codventa=Venta.codventa  and Devolucionventa.codsucursal=Venta.codsucursal)
inner join devolucionventapago VTP on (VTP.coddevolucion=Devolucionventa.coddevolucion and VTP.codsucursal=Devolucionventa.codsucursal)
group by 
Venta.numerofactura,Devolucionventa.codsucursal,  
Venta.estatus,Devolucionventa.fecha::date, VTP.tipopago
) V
where ".$criteria."
group by V.fecha,V.codsucursal, V.tipopago
order by V.fecha,V.codsucursal, V.tipopago
";
        $data = $this->query($sql);
        return $data;
    }
    
    function nroVentasRealizadas($datos= array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" V.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and V.fecha <= '".$hasta." 23:59:59' ";
        if(isset($datos['codsucursal'])){
            if ($datos['codsucursal']==''){
                $datos['codsucursal']= 0;
            }			
            $criteria=$criteria." and V.codsucursal=".$datos['codsucursal']." ";
        }		

        $sql = "select fecha::date, count(codventa) as nro from ventas as V where ".$criteria." group by fecha::date order by V.fecha::date";
        $data = $this->query($sql);
        //echo $sql;
        //die();
        if(($desde==$hasta) && count($data)==0 ){
            $data = array(0=>array('fecha'=>$desde,'nro'=>0));
        }
        return $data;
    }     
//---------------Funciones de Interfaces-----------------------------------
    function obtenerResumenDia($cuadre=array()){
        
        $dataResumen['VentaDia'] = $this->ventasContado(" and VT.fecha='".$cuadre['Cuadrediario']['fecha']."' ");
        if(!isset($dataResumen['VentaDia'][0][0]['total'])){
                $dataResumen['VentaDia'][0][0]['total']=0;
        }
        $datos=array('fechahasta'=>$this->diamesano($cuadre['Cuadrediario']['fecha']),'fechadesde'=>$this->diamesano($cuadre['Cuadrediario']['fecha']),'client'=>array(0),'tipoven'=>array(0));                      

        $dataResumen['Venta'] =  $this->resumenVentas($datos);
        if(!isset($dataResumen['Venta'][0][0]['neto'])){
            $dataResumen['Venta'][0][0]['neto']=0;
        }
        if(!isset($dataResumen['Venta'][0][0]['iva'])){
            $dataResumen['Venta'][0][0]['iva']=0;
        }
                
        $dataResumen['Pago'] =  $this->resumenPagosVentas($datos);
        $dataResumen['Nro'] =  $this->nroVentasRealizadas($datos);
        if(!isset($dataResumen['Nro'][0][0]['nro'])){
            $dataResumen['Nro'][0][0]['nro']=0;
        }
        return $dataResumen;
    }
    
}
?>
