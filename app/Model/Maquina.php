<?php
App::uses('AppModel', 'Model');
class Maquina extends AppModel
{
    public $name = 'Maquina';
    public $useTable = 'maquinas';
	public $primaryKey = 'codmaquina';
	public $useDbConfig = 'comerdepa';
    /*var $hasMany = array('Funcione' =>
                         array('className'   => 'Funcione',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'modulo_id',
                               'dependent'   => false,
                               'exclusive'   => '',
                               'finderSql'   => ''
                         )
                  );           	  
	*/
  /* Funciones para la vista */ 	
	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$order="Maquina.marca ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Maquina'];	
			$var[$relleno.$item['codmaquina'].'-'.$item['memoria']] = $item['marca'].' '.$item['serial'];
		}
		break;
		case 1://Muestra Todos los Bancos registrados con Cuentas Asociadas
		$order="Modulo.nombre ASC";
		$datos = $this->find('all',array('order'=> $order));
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Modulo'];
			if(count($row['Funcione'])>0){	
			$var[$relleno.$item['id']] = $item['nombre'];
			}
		}
		break;				
		}//Cierre de switch						
		return $var;
	}
  /* Fin de Funciones para la vista */  


	function bancoCupon(){
		$sql="select Banco.id,
			Banco.descripcion
			from bancos as Banco 
			INNER JOIN tarjetas AS Tarjeta ON (Tarjeta.banco_id = Banco.id) 
			INNER JOIN tarjetastipo ON (Tarjeta.tarjetastipo_id = tarjetastipo.id) 
			INNER JOIN maquinas ON (Tarjeta.maquina_id = maquinas.id)
			where  tarjetastipo.asociado='CUPON' 
			group by banco.id,Banco.descripcion";
		$data=$this->query($sql);
		return $data;
	}				  
}
?>
