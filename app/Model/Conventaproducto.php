<?php
class Conventaproducto extends AppModel
{
    public $name = 'Conventaproducto';
    public $primaryKey = 'id';
    /**
 * Use database config
 *
 * @var string
 */
	//public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'conventasproductos';

    public $belongsTo = array('Conventa' => array('className' => 'Conventa',
                                            'conditions' => 'Conventaproducto.codventa=Conventa.codventa and Conventaproducto.codsucursal=Conventa.codsucursal',
                                            'order' => '',
                                            'foreignKey' => '',
                                            'fields'=> ''
                                      ),                                                                                'Producto' => array('className' => 'Producto',
                                            'conditions' => 'Conventaproducto.codproducto=Producto.codproducto ',
                                            'order' => '',
                                            'foreignKey' => 'codproducto',
                                            'fields'=> ''
                                      )					  
                    );


    function actualizar($datos=array()){

            $condicion = " Cpabono.codcpabono = '".$datos['Cpabonopago']['codcpabono']."' ";
            $sql="update Cpabono set concepto='PAGO DE RETENCION NRO ".$datos['Retencioncompra']['numero']."' where ".$condicion;
            $data = $this->query($sql);
    }

    function buscariva(){
            $sql="select iva from ventasproductos group by iva ";
            $data = $this->query($sql);
    }

    function getProductoVenta($datos=array()){
            $sql="select * from conventasproductos where codventa=".$datos['codventa']." and codsucursal=".$datos['codsucursal'];
            $data = $this->query($sql);
            $var = array();
            foreach ($data as $row) {
                    $item = $row[0];	
                    $var[$item['codproducto']] = $item['precio'];
            }
            return $var;
    }
    
    function getProductoVentaTodo($datos=array()){
            //$sql="select * from conventasproductos where codventa=".$datos['codventa']." and codsucursal=".$datos['codsucursal'];    
        
            $criteria = " Conventaproducto.codventa=".$datos['codventa']." and Conventaproducto.codsucursal=".$datos['codsucursal']." ";
            $order = " Conventaproducto.indice ";
            $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
            //$data = $this->query($sql);
            
            return $data;
    }
				  
}
?>
