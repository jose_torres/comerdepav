<?php
class Condevolucionventa extends AppModel
{
    public $name = 'Condevolucionventa';
    public $primaryKey = 'id';
    /**
 * Use database config
 *
 * @var string
 */
	//public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'condevolucionventa';
/*
 *
 * */
	public $belongsTo = array('Conventa' => array('className' => 'Conventa',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codventa',
						'fields'=> ''
					  )					  					  
			);

		public $hasMany = array(/*'Condevolucionventaproducto' =>
                         array('className'   => 'Condevolucionventaproducto',
                               'conditions'  => 'Condevolucionventaproducto.coddevolucion=Condevolucionventa.coddevolucion and Condevolucionventaproducto.codsucursal=Condevolucionventa.codsucursal',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => '',
                         ),
                        /* 'Devolucionventapago' =>
                         array('className'   => 'Devolucionventa',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'coddevolucion',

                         ),
                        /* 'Devolucionventacreditopago' =>
                         array('className'   => 'Devolucionventa',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'coddevolucionventa',

                         )*/
                       
                  );			
			
    function ajustarDatos($opcion='pdf',$datos){
            switch ($opcion) {
            case 'pdf':
                    if($datos['tipoventa']==''){
                            $datos['tipoventa']=array();
                    }
                    if($datos['cliente_id']==''){
                            $datos['cliente_id']=array();
                    }
                    if($datos['vendedore_id']==''){
                            $datos['vendedore_id']=array();
                    }
                    $datos['client']=$datos['cliente_id'];$datos['tipoven']=$datos['tipoventa'];
                    $datos['vend']=$datos['vendedore_id'];
                    $datos['fechadesde']=$datos['desde'];$datos['fechahasta']=$datos['hasta'];
            break;		
            }
            return $datos;
    }

    function reporte($datos=array(),$criteria='',$opcion=0){
            $hasta=$this->anomesdia($datos['fechahasta']);
            $desde=$this->anomesdia($datos['fechadesde']);
            $data=array();
            switch ($opcion) {
            case 0:
                    $order='Devolucionventa.coddevolucion,Devolucionventa.fecha';
                    $criteria=" Devolucionventa.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Devolucionventa.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			

                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>2));
            break;		
            case 1:			
                    $order='Venta.estatus desc,Venta.codventa';
                    $criteria=" Devolucionventa.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Devolucionventa.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    if(isset($datos['codsucursal'])){
                    $criteria=$criteria." and Devolucionventa.codsucursal=".$datos['codsucursal']." ";
                    }
                    $criteria=$criteria."  ".$c_tipoven;			
                    //echo $criteria;
                    $sql="select Devolucionventa.id,Devolucionventa.coddevolucion, Devolucionventa.codsucursal, Venta.codventa ,Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion as Cliente__descripcion, Devolucionventa.baseimp1, Devolucionventa.baseimp2,Devolucionventa.baseimp3, Devolucionventa.ivaimp1, Devolucionventa.ivaimp2, Devolucionventa.ivaimp3, Devolucionventa.porimp2, Devolucionventa.montoexento, Venta.estatus,Devolucionventa.fecha, Devolucionventa.observacion, Devolucionventa.conciliado,
                    sum(Devolucionventaproducto.cantidad*Ventaproducto.costopromedio) as Costo from condevolucionventa as Devolucionventa
                    inner join conventas Venta on (Devolucionventa.codventa=Venta.codventa and Devolucionventa.codsucursal=Venta.codsucursal)
                    inner join condevolucionventaproductos Devolucionventaproducto on (Devolucionventa.coddevolucion=Devolucionventaproducto.coddevolucion)
                    inner join conclientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codsucursal)
                    inner join conventasproductos Ventaproducto on (Ventaproducto.codventa=Devolucionventa.codventa and Devolucionventaproducto.codproducto=Ventaproducto.codproducto)
                    where ".$criteria."
                    group by Devolucionventa.id,Devolucionventa.coddevolucion, Devolucionventa.codsucursal,Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, Devolucionventa.baseimp1, Devolucionventa.baseimp2,Devolucionventa.baseimp3, Devolucionventa.ivaimp1, Devolucionventa.ivaimp2, Devolucionventa.ivaimp3,Devolucionventa.porimp2, Devolucionventa.montoexento, Venta.estatus, Devolucionventa.fecha, Devolucionventa.observacion, Devolucionventa.conciliado
                    order by Devolucionventa.coddevolucion
                    ";

                    $data = $this->query($sql);
            break;
            case 2:
                    $order='Venta.codvendedor,Venta.fecha';
                    $criteria=" Devolucionventa.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and Devolucionventa.fecha <= '".$hasta." 23:59:59' ";			
                    $c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			

                    $data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>2));
            break;
            case 3:			
                    $order='Venta.estatus desc,Venta.codventa';
                    $criteria=" DEV.fecha >= '".$desde." 00:00:00' ";
                    $criteria=$criteria." and DEV.fecha <= '".$hasta." 23:59:59' ";			
            /*	$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
                    $criteria=$criteria."  ".$c_cliente;
                    $c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
                    $criteria=$criteria."  ".$c_vend;
                    $c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
                    $criteria=$criteria."  ".$c_tipoven;			*/
                    //echo $criteria;
                    $sql = " select DEV.fecha, DEV.coddevolucion,DEV.numerofactura,DEV.estatus, DEV.codcliente, DEV.codsucursal,DEV.rif,DEV.descripcion, DEV.poriva, contado, DEV.credito, DEV.retencion, DEV.impuestos, sum(DEV.pagoefectivo) as pagoefectivo,sum(DEV.pagodebito) as pagodebito, 
                    sum(DEV.pagocheque) as pagocheque, sum(DEV.pagotransfer) as pagotransfer
                    from (
                    select distinct DV.coddevolucion,DV.fecha , Venta.numerofactura, Venta.estatus, CL.codcliente,CL.codsucursal,CL.rif,cl.descripcion, MCC.poriva,
                    CASE WHEN Venta.estatus='P' or  MCC.poriva::text is null  THEN (DV.baseimp1+DV.baseimp2+DV.baseimp3) ELSE 0 END as contado,
                    CASE WHEN Venta.estatus='A' or MCC.poriva::text is not null THEN (DV.baseimp1+DV.baseimp2+DV.baseimp3) ELSE 0 END as credito, 
                    CASE WHEN Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E' THEN DV.retencion ELSE 0 END as retencion,
                    CASE WHEN Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E' THEN (DV.ivaimp1+DV.ivaimp2+DV.ivaimp3) ELSE 0 END as impuestos,
                    CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and DVP.tipopago='EFECTIVO' THEN DVP.monto  ELSE 0 END as pagoefectivo, 
                    CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and DVP.tipopago='DEBITO' THEN DVP.monto  ELSE 0 END as pagodebito, 
                    CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and DVP.tipopago='CHEQUE' THEN DVP.monto  ELSE 0 END as pagocheque,
                    CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and DVP.tipopago='TRANSFER' THEN DVP.monto  ELSE 0 END as pagotransfer
                    from devolucionventa DV
                    inner join ventas Venta on (DV.codventa=Venta.codventa)
                    inner join clientes CL on (CL.codcliente=Venta.codcliente and CL.codsucursal=Venta.codclientesucursal)
                    left join ventaspagos VTP on (VTP.codventa=Venta.codventa and Venta.codsucursal=VTP.codsucursal)
                    left join (select coddevolucion,codsucursal,tipopago,sum(monto) as monto from devolucionventapago group by coddevolucion,codsucursal,tipopago) DVP on (DVP.coddevolucion=DV.coddevolucion and VTP.codsucursal=DVP.codsucursal)
                    left join movimientoivacc MCC on (MCC.codmovimiento=Venta.codventa)
                    order by DV.fecha
                    ) DEV
                    where ".$criteria."
                    group by DEV.fecha, DEV.coddevolucion,DEV.numerofactura,DEV.estatus, DEV.codcliente, DEV.codsucursal,DEV.rif,DEV.descripcion, DEV.poriva,
                    contado, DEV.credito,DEV.retencion,DEV.impuestos";

                    $data = $this->query($sql);
            break;	
            }
            return $data;
    }

    function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
            switch ($opcion) {
                    case 'Cuadrediario':
                    $texto = '';
                    $condicion = "  Devolucionventa.fecha>='".$datos['Cuadrediario']['fecha']." 00:00:00' and Devolucionventa.fecha<='".$datos['Cuadrediario']['fecha']." 23:59:59' ";
                    $valores = $this->find('all',array('order'=>'Devolucionventa.coddevolucion','conditions'=>$condicion,'recursive'=>-1));

                    foreach ($valores as $reg) {
                            $fila = $reg['Devolucionventa'];
                            if($fila['codccnotacredito']=='' || $fila['codccnotacredito']==null){
                                    $fila['codccnotacredito']=0;
                            }
                            if($fila['codccnotacreditosucursal']=='' || $fila['codccnotacreditosucursal']==null){
                                    $fila['codccnotacreditosucursal']=0;
                            }
                            $texto = $texto."INSERT INTO condevolucionventa ( coddevolucion, codsucursal, codventa, codsucursalventa, fecha, codusuario, codusuariosucursal, coddeposito, codccnotacredito, codccnotacreditosucursal, devoluciontotal, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, observacion) values (";
                            $texto = $texto."".$fila['coddevolucion'].",".$fila['codsucursal'].",".$fila['codventa'].",".$fila['codsucursalventa'].",'".$fila['fecha']."',".$fila['codusuario'].",".$fila['codusuariosucursal'].",".$fila['coddeposito'].",".$fila['codccnotacredito'].",".$fila['codccnotacreditosucursal'].",'".$fila['devoluciontotal']."',".$fila['montobruto'].",".$fila['montoexento'].",".$fila['baseimp1'].",".$fila['baseimp2'].",".$fila['baseimp3'].",".$fila['ivaimp1'].",".$fila['ivaimp2'].",".$fila['ivaimp3'].",".$fila['porimp1'].",".$fila['porimp2'].",".$fila['porimp3'].",".$fila['retencion'].",'".$fila['observacion']."');";
                            //$texto = $texto.'\n';
                    }	
                    break;				
                    case 'Mcuadrediario':
                    $texto = '';
                    $condicion = "  Devolucionventa.fecha>='".$datos['Mcuadrediario']['fecha']." 00:00:00' and Devolucionventa.fecha<='".$datos['Mcuadrediario']['fecha']." 23:59:59' ";
                    $valores = $this->find('all',array('order'=>'Devolucionventa.coddevolucion','conditions'=>$condicion,'recursive'=>-1));

                    foreach ($valores as $reg) {
                            $fila = $reg['Devolucionventa'];
                            if($fila['codccnotacredito']=='' || $fila['codccnotacredito']==null){
                                    $fila['codccnotacredito']=0;
                            }
                            if($fila['codccnotacreditosucursal']=='' || $fila['codccnotacreditosucursal']==null){
                                    $fila['codccnotacreditosucursal']=0;
                            }
                            $texto = $texto."INSERT INTO condevolucionventa ( coddevolucion, codsucursal, codventa, codsucursalventa, fecha, codusuario, codusuariosucursal, coddeposito, codccnotacredito, codccnotacreditosucursal, devoluciontotal, montobruto, montoexento, baseimp1, baseimp2, baseimp3, ivaimp1, ivaimp2, ivaimp3, porimp1, porimp2, porimp3, retencion, observacion) values (";
                            $texto = $texto."".$fila['coddevolucion'].",".$fila['codsucursal'].",".$fila['codventa'].",".$fila['codsucursalventa'].",'".$fila['fecha']."',".$fila['codusuario'].",".$fila['codusuariosucursal'].",".$fila['coddeposito'].",".$fila['codccnotacredito'].",".$fila['codccnotacreditosucursal'].",'".$fila['devoluciontotal']."',".$fila['montobruto'].",".$fila['montoexento'].",".$fila['baseimp1'].",".$fila['baseimp2'].",".$fila['baseimp3'].",".$fila['ivaimp1'].",".$fila['ivaimp2'].",".$fila['ivaimp3'].",".$fila['porimp1'].",".$fila['porimp2'].",".$fila['porimp3'].",".$fila['retencion'].",'".$fila['observacion']."');";
                            //$texto = $texto.'\n';
                    }	
                    break;				
            }	
            return $texto;
    }

    function guardar_seleccionados($datos){

            $data = array();$i = 0;
            foreach ($datos as $registros){
                    $v_chequeo = $registros['conciliar'];
                    $data['Condevolucionventa'][$i]['id'] = $registros['id'];
                    $data['Condevolucionventa'][$i]['conciliado'] = 'NO';
                    if ($v_chequeo>=1) {
                            $data['Condevolucionventa'][$i]['conciliado'] = 'SI';
                    }
                    $i = $i + 1;
            }
            $mensaje = '<div class="alert alert-danger">Datos de Movimientos de las Devoluciones No se Conciliaron.</div>';	
            if($this->saveAll($data['Condevolucionventa'])){
                    $mensaje =  '<div class="alert alert-success">Datos de Movimientos de las Devoluciones se Guardaron Correctamente.</div>';
            }
            return $mensaje;
    }
    
    function nroDevolucionesRealizadas($datos= array()){
        $hasta=$this->anomesdia($datos['fechahasta']);
        $desde=$this->anomesdia($datos['fechadesde']);
        $criteria=" V.fecha >= '".$desde." 00:00:00' ";
        $criteria=$criteria." and V.fecha <= '".$hasta." 23:59:59' ";
        if(isset($datos['codsucursal'])){
            if ($datos['codsucursal']==''){
                $datos['codsucursal']= 0;
            }			
            $criteria=$criteria." and V.codsucursal=".$datos['codsucursal']." ";
        }		

        $sql = "select fecha::date, count(codventa) as nro from condevolucionventa as V where ".$criteria." group by fecha::date order by V.fecha::date";
        $data = $this->query($sql);
        //echo $sql;
        //die();
        if(($desde==$hasta) && count($data)==0 ){
            $data[0] = array(0=>array('fecha'=>$desde,'nro'=>0));
        }
        return $data;
    }   
//---------------Funciones de Interfaces-----------------------------------
    function obtenerResumenDia($cuadre=array()){
                
        $datos=array('fechahasta'=>$this->diamesano($cuadre['Concuadrediario']['fecha']),'fechadesde'=>$this->diamesano($cuadre['Concuadrediario']['fecha']),'client'=>array(0),'tipoven'=>array(0),'codsucursal'=>$cuadre['Concuadrediario']['codsucursal']);

        $dataResumen['Devoluciones'] =  $this->reporte($datos,'',1);        
        $dataResumen['Nro'] =  $this->nroDevolucionesRealizadas($datos);
        return $dataResumen;
    }    
				  
}
?>
