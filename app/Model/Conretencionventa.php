<?php
class Conretencionventa extends AppModel
{
    public $name = 'Conretencionventa';
    //public $primaryKey = 'codretencion';
    /**
 * Use database config
 *
 * @var string
 */
	//public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'conretenciones_ventas';
/*
 *
 * */
	/*public $belongsTo = array('Proveedore' => array('className' => 'Proveedore',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codproveedor',
						'fields'=> 'Proveedore.codproveedor,Proveedore.rif,Proveedore.descripcion,Proveedore.direccion,Proveedore.diasdecredito'
					  ),
					  'Compra' => array('className' => 'Compra',
  						'conditions' => "Retencioncompra.tipodocumento='COMPRAS'",
						'order' => '',
						'foreignKey' => 'codmovimiento',
						'fields'=> ''
					  )	,
					  'Cpfactura' => array('className' => 'Cpfactura',
  						'conditions' => "Retencioncompra.tipodocumento='FA'",
						'order' => '',
						'foreignKey' => 'codmovimiento',
						'fields'=> ''
					  )	,
					  'Cpnotadebito' => array('className' => 'Cpnotadebito',
  						'conditions' => "Retencioncompra.tipodocumento='ND'",
						'order' => '',
						'foreignKey' => 'codmovimiento',
						'fields'=> ''
					  )		  					  
			); */
			
	public $ART_RETENCION="Art. 11: &quot;La Administraci&oacute;n Tributaria podra designar como Responsable del Pago de Impuesto, en calidad de Agentes de Retenci&oacute;n a quienes por sus funciones p&uacute;blicas o por raz&oacute;n de sus Actividades privadas intervengan en operaciones gravadas con el Impuesto establecido en ese Decreto con rango, Valor y Fuerza de Ley&quot;";

	public $TITULO_COMPROBANTE = "COMPROBANTE DE RETENCI&Oacute;N DE IVA";
	public $PROVIDENCIA="Providencia Administrativa Nro SNAT/2015/0049 del 10/08/2015";

	function guardar_seleccionados($datos){

		$data = array();$i = 0;
		foreach ($datos as $registros){
			$v_chequeo = $registros['conciliar'];
			$data['Conretencionventa'][$i]['id'] = $registros['id'];
			$data['Conretencionventa'][$i]['estatus'] = 'A';
			if ($v_chequeo>=1) {
				$data['Conretencionventa'][$i]['estatus'] = 'C';
			}
			$i = $i + 1;
		}
		$mensaje = '<div class="alert alert-danger">Datos de Movimientos de las Retenciones de Ventas no se Conciliaron.</div>';	
		if($this->saveAll($data['Conretencionventa'])){
			$mensaje =  '<div class="alert alert-success">Datos de Movimientos de las Retenciones de Ventas se Guardaron Correctamente.</div>';
		}
		return $mensaje;
	}
	
	function ajustarDatos($opcion='add',$datos){
		switch ($opcion) {
		case 'add':
			$datos['Retencioncompra']['mesafecta']=$datos['Retencioncompra']['mes']['month'];
			$datos['Retencioncompra']['anioafecta']=$datos['Retencioncompra']['anio']['year'];
			$datos['Retencioncompra']['horaemision']=date('h:i:s');
			$datos['Retencioncompra']['fechaemision']=$this->anomesdia($datos['Retencioncompra']['fecha']);
			$datos['Retencioncompra']['fecharegistro']=$this->anomesdia($datos['Retencioncompra']['fecha']);
			$datos['Retencioncompra']['numero']=$this->get_numero($datos);
			$cont=0;
			for ($i = 0; $i < $datos['Retencione']['totales']; $i++) {
				$datos['Retencioncompras'][$i]['mesafecta']=$datos['Retencioncompra']['mes']['month'];
				$datos['Retencioncompras'][$i]['anioafecta']=$datos['Retencioncompra']['anio']['year'];
				$datos['Retencioncompras'][$i]['fechaemision']=$this->anomesdia($datos['Retencioncompra']['fecha']);
				$datos['Retencioncompras'][$i]['fecharegistro']=$this->anomesdia($datos['Retencioncompra']['fecha']);
				$datos['Retencioncompras'][$i]['tiporetencion']=$datos['Retencioncompra']['tiporetencion'];
				$datos['Retencioncompras'][$i]['horaemision']=date('h:i:s');
				$datos['Retencioncompras'][$i]['numero']=$datos['Retencioncompra']['numero'];
			}
			
		break;
		case 'edit':
			$datos['Retencioncompra']['mesafecta']=$datos['Retencioncompra']['mes']['month'];
			$datos['Retencioncompra']['anioafecta']=$datos['Retencioncompra']['anio']['year'];
			$datos['Retencioncompra']['horaemision']=date('h:i:s');
			$datos['Retencioncompra']['fechaemision']=$this->anomesdia($datos['Retencioncompra']['fecha']);
			$datos['Retencioncompra']['numeroant']=$datos['Retencioncompra']['numero'];
			$datos['Retencioncompra']['numero']=$this->get_numero($datos);
		break;
		}
		return $datos;
	}
	
	function nro_documentos($sucursal=1){
		$sql = "SELECT incrementarnumerodocumento('Pago o Abono Compra', ".$sucursal.") as numerodoc";
		$data = $this->query($sql);
		return $data[0][0]['numerodoc'];
	}

	function longitud_documentos($sucursal=1){
		$sql = "select * from numerodocumento where tipodocumento = 'Pago o Abono Compra' and codsucursal = ".$sucursal."";
		$data = $this->query($sql);
		if (isset($data[0][0]['longitud'])){
			return $data[0][0]['longitud'];
		}else{
			return 8;
		}
	}
	
	function getCabecera(){
		$encabezado['encabezado']=$this->ART_RETENCION;			
		$encabezado['providencia']=$this->PROVIDENCIA;			
		$encabezado['titulo']=$this->TITULO_COMPROBANTE;	
		return $encabezado;
	}
	
	function getQuincena($dia){
		$quincena = '1era Quincena';		
		if($dia>15){
			$quincena = '2da Quincena';
		}
		return $quincena;
	}

	function buscarResumenDia($datos=array()){
		$hasta=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$desde=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";	
		$sql =  " select RV.fechaemision,sum(RVD.montoretenido) as Total from retenciones_ventas RV inner join retenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion)
		where ".$criteria." group by RV.fechaemision";
		$data = $this->query($sql);
		if(!isset($data[0][0]['total'])){
			$data[0][0]['total']=0;
		}
        return $data[0][0];
	}

	function buscarRegistrosDia($datos=array()){
		$hasta=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$desde=$this->anomesdia($datos['Cuadrediario']['fecha']);
		$criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";	
		$sql =  " select RV.fechaemision,CL.rif,CL.descripcion,RVD.* from retenciones_ventas RV inner join retenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion) inner join clientes CL on (CL.codcliente  = RVD.codcliente and CL.codsucursal = RVD.codsucursalcliente)
		where ".$criteria." order by RVD.codretencion";
		$data = $this->query($sql);
		
        return $data;
	}

	function eliminarDetalle($codretencion='',$codsucursal=''){
		$sql =  " DELETE FROM retenciones_ventas_detalles where codretencion=".$codretencion." and codsucursal=".$codsucursal." ";
		$data = $this->query($sql);
	}

	function eliminarCabecera($codretencion=''){
		$sql =  " DELETE FROM retenciones_ventas where codretencion=".$codretencion." ";
		$data = $this->query($sql);
	}

	function nroRetencionNuevos($sucursal=array()){
		$data = array();
		$sql="select COALESCE(count(codretencion),0) as nro_reg from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select RV.codretencion, RVD.codsucursal from retenciones_ventas_detalles RVD inner join retenciones_ventas RV on (RV.codretencion=RVD.codretencion) order by RVD.codretencion') as t1(codretencion bigint,codsucursal bigint) where (codretencion,codsucursal) not in (select codretencion,codsucursal from conretenciones_ventas)";
		//echo $sql;
		$data=$this->query($sql);
		$cont = 0;
		if(isset($data[0][0]['nro_reg'])){
			$cont = $data[0][0]['nro_reg'];
		}
		return $cont;
	}

	function guardarRetencion($sucursal=array()){

		$sql="INSERT INTO conretenciones_ventas( codretencion, codsucursal, numero, fechaemision, horaemision, montototal, estatus)
		select codretencion, codsucursal, numero, fechaemision, horaemision, montototal, estatus from dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."', 'select vt.codretencion, RV.codsucursal, vt.numero, fechaemision, horaemision, montototal, estatus from retenciones_ventas vt inner join retenciones_ventas_detalles RV on (RV.codretencion=vt.codretencion) order by vt.fechaemision') as t1(codretencion bigint, codsucursal bigint, numero text, fechaemision date, horaemision time, montototal numeric, estatus text) where (codretencion,codsucursal) not in (select codretencion,codsucursal from conretenciones_ventas) ";
		$data=$this->query($sql);
		$sql = "update conretenciones_ventas set estatus='C' where codsucursal=".$sucursal['Sucursal']['codsucursal']."";
		$data=$this->query($sql);
		return $data;
	}
	
	function incluirFaltante($sucursal=''){
		// Buscar Registros que no estan en el consolidado
		$cantidad = $this->nroRetencionNuevos($sucursal);
		// Incluir si el registro es mayor a cero
		if($cantidad>0){
			$this->guardarRetencion($sucursal);
		}
	}
				  
}
?>
