<?php
class Concheque extends AppModel {

	public $name = 'Concheque';
	//public $useDbConfig = 'comerdepa';
	//The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Banco' => array(
			'className' => 'Banco',
			'foreignKey' => 'banco_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		/*'Movbancario' => array(
			'className' => 'Movbancario',
			'foreignKey' => 'movbancario_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)*/
	);

	function ajustarDatos($opcion='add',$datos){
		switch ($opcion) {
		case 'add':			
			$datos['Succheque']['codsucursal']=$datos['Ventaspago']['codsucursal'];	
			$datos['Succheque']['banco_id']=$datos['Ventaspago']['banco'];
			$datos['Succheque']['codventa']=$datos['Ventaspago']['codventa'];
			$datos['Succheque']['fecha']=$datos['Venta']['fecha'];
			$datos['Succheque']['nro']=$datos['Ventaspago']['numerocheque'];
			$datos['Succheque']['nrocta']=$datos['Ventaspago']['nrocuenta'];
			$datos['Succheque']['cuadrediario_id']=$datos['Cuadrediario']['id'];
			$datos['Succheque']['monto']=$datos['Ventaspago']['monto'];			
						
		break;
		case 'edit':
			$datos['Succheque']['id']=$datos['Succheque']['id'];
			$datos['Succheque']['monto']=round( $datos['Ventaspago']['monto'], 2, PHP_ROUND_HALF_UP);	
		break;
		case 'cuadre':
			for ($i = 1; $i <= $datos['total_reg']; $i++) {
				$datos['Succheque'][$i]['movbancario_id']=$datos['movbancario_id'];
				$datos['Succheque'][$i]['id']=$datos['cheque_id'.$i];				
			}
		break;
		}
		return $datos;
	}

	function obtenerDepositos($fecha=''){
		$sucursal = 0;
		$sql = "select distinct codsucursal from sucdepositos where fecha='".$fecha."'";
		$data = $this->query($sql);
		if (isset($data[0][0]['codsucursal'])){
			$sucursal = $data[0][0]['codsucursal'];
		}
		return $sucursal;
	}

	function buscar($datos,$cuadre){
	    $var = array();
		//print_r($datos);
		foreach ($datos as $row){
			// Buscar si esta
			if(isset($row['venta'])){
				$row['Venta'] = $row['venta'];
				$row['Ventaspago'] = $row['ventaspago'];
				//echo 'paso';
			}
			
			if($row['Venta']['estatus']!='E'){
			$row['Cuadrediario']['id'] = $cuadre['Cuadrediario']['id'];
			$nro_reg = $this->find('count',array('conditions'=> "Succheque.codventa=".$row['Ventaspago']['codventa']." and Succheque.codsucursal=".$row['Ventaspago']['codsucursal']." ",'recursive'=>0));
			if($nro_reg<=0){
				$dat= $this->ajustarDatos('add',$row);				
			}else{
				$dat = $this->find('first',array('conditions'=> "Succheque.codventa=".$row['Ventaspago']['codventa']." and Succheque.codsucursal=".$row['Ventaspago']['codsucursal']." ",'recursive'=>0));
				$row['Succheque']['id']=$dat['Succheque']['id'];
				$dat= $this->ajustarDatos('edit',$row);				
			}
			$this->save($dat);
			}
		}
		$var = $this->find('all',array('conditions'=> "Succheque.fecha='".$cuadre['Cuadrediario']['fecha']."' and Succheque.movbancario_id=0 "));
		return $var;
	}

	function buscar2($datos,$cuadre){
	    $var = array();
		print_r($datos);
		foreach ($datos as $row){
			// Buscar si esta
			
			if($row['venta']['estatus']!='E'){
			$row['Cuadrediario']['id'] = $cuadre['Cuadrediario']['id'];
			$nro_reg = $this->find('count',array('conditions'=> "Succheque.codventa=".$row['ventaspago']['codventa']." and Succheque.codsucursal=".$row['ventaspago']['codsucursal']." ",'recursive'=>0));
			if($nro_reg<=0){
				$dat= $this->ajustarDatos('add',$row);				
			}else{
				$dat = $this->find('first',array('conditions'=> "Succheque.codventa=".$row['ventaspago']['codventa']." and Succheque.codsucursal=".$row['ventaspago']['codsucursal']." ",'recursive'=>0));
				$row['Succheque']['id']=$dat['Succheque']['id'];
				$dat= $this->ajustarDatos('edit',$row);				
			}
			$this->save($dat);
			}
		}
		$var = $this->find('all',array('conditions'=> "Succheque.fecha='".$cuadre['Cuadrediario']['fecha']."' and Succheque.movbancario_id=0 ")); 
		return $var;
	}


}
?>
