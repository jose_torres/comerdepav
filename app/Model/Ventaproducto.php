<?php
class Ventaproducto extends AppModel
{
    public $name = 'Ventaproducto';
    public $primaryKey = 'codventa';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'ventasproductos';
	
	public $belongsTo = array('Venta' => array('className' => 'Venta',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codventa',
						'fields'=> ''
					  )					  					  
			);
	

	function actualizar($datos=array()){

		$condicion = " Cpabono.codcpabono = '".$datos['Cpabonopago']['codcpabono']."' ";
		$sql="update Cpabono set concepto='PAGO DE RETENCION NRO ".$datos['Retencioncompra']['numero']."' where ".$condicion;
		$data = $this->query($sql);
	}

	function buscariva(){
		$sql="select iva from ventasproductos group by iva ";
		$data = $this->query($sql);
	}

	function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
		switch ($opcion) {
			case 'Cuadrediario':
			$texto = '';
			$condicion = "  Venta.fecha='".$datos['Cuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Venta.codventa','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Ventaproducto'];
				$texto = $texto."INSERT INTO conventasproductos ( codventa, codsucursal, codproducto, cantidad, precio, unidad, iva, factorconversion, costopromedio, indice, unidad2, factorconversion2) values (";
				$texto = $texto."".$fila['codventa'].",".$fila['codsucursal'].",".$fila['codproducto'].",".$fila['cantidad'].",".$fila['precio'].",'".$fila['unidad']."',".$fila['iva'].",".$fila['factorconversion'].",".$fila['costopromedio'].",".$fila['indice'].",'".$fila['unidad2']."',".$fila['factorconversion2'].");";
				//$texto = $texto.'\n';
			}	
			break;				
			case 'Mcuadrediario':
			$texto = '';
			$condicion = "  Venta.fecha='".$datos['Mcuadrediario']['fecha']."'";
			$valores = $this->find('all',array('order'=>'Venta.codventa','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Ventaproducto'];
				$texto = $texto."INSERT INTO conventasproductos ( codventa, codsucursal, codproducto, cantidad, precio, unidad, iva, factorconversion, costopromedio, indice, unidad2, factorconversion2) values (";
				$texto = $texto."".$fila['codventa'].",".$fila['codsucursal'].",".$fila['codproducto'].",".$fila['cantidad'].",".$fila['precio'].",'".$fila['unidad']."',".$fila['iva'].",".$fila['factorconversion'].",".$fila['costopromedio'].",".$fila['indice'].",'".$fila['unidad2']."',".$fila['factorconversion2'].");";
				//$texto = $texto.'\n';
			}	
			break;				
		}
		
		return $texto;
	}
				  
}
?>
