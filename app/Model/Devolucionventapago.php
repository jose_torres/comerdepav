<?php
class Devolucionventapago extends AppModel
{
    public $name = 'Devolucionventapago';
    public $primaryKey = 'coddevolucion';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'devolucionventapago';
/*
 *
 * */
	public $belongsTo = array('Devolucionventa' => array('className' => 'Devolucionventa',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'coddevolucion',
						'fields'=> ''
					  )					  					  
			);
			
	function ajustarDatos($opcion='pdf',$datos){
		switch ($opcion) {
		case 'pdf':
			if($datos['tipoventa']==''){
				$datos['tipoventa']=array();
			}
			if($datos['cliente_id']==''){
				$datos['cliente_id']=array();
			}
			if($datos['vendedore_id']==''){
				$datos['vendedore_id']=array();
			}
			$datos['client']=$datos['cliente_id'];$datos['tipoven']=$datos['tipoventa'];
			$datos['vend']=$datos['vendedore_id'];
			$datos['fechadesde']=$datos['desde'];$datos['fechahasta']=$datos['hasta'];
		break;		
		}
		return $datos;
	}

	function reporte($datos=array(),$criteria='',$opcion=0){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$order='Venta.estatus desc,Venta.codventa';
			$criteria=" Devolucionventa.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Devolucionventa.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>2));
		break;		
		case 1:			
			$order='Venta.estatus desc,Venta.codventa';
			$criteria=" Devolucionventa.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Devolucionventa.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			//echo $criteria;
			$sql="select Devolucionventa.coddevolucion,Venta.codventa ,Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion as Cliente__descripcion, Devolucionventa.baseimp1, Devolucionventa.baseimp2,Devolucionventa.baseimp3, Devolucionventa.ivaimp1, Devolucionventa.ivaimp2, Devolucionventa.ivaimp3, Devolucionventa.porimp2, Devolucionventa.montoexento, Venta.estatus,Devolucionventa.fecha, Vendedore.descripcion as Vendedore__descripcion, Devolucionventa.observacion, 
			sum(Devolucionventaproducto.cantidad*Ventaproducto.costopromedio) as Costo from devolucionventa as Devolucionventa
			inner join ventas Venta on (Devolucionventa.codventa=Venta.codventa)
			inner join devolucionventaproductos Devolucionventaproducto on (Devolucionventa.coddevolucion=Devolucionventaproducto.coddevolucion)
			inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codsucursal)
			inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Devolucionventa.codventa and Devolucionventaproducto.codproducto=Ventaproducto.codproducto)
			where ".$criteria."
			group by Devolucionventa.coddevolucion,Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, Devolucionventa.baseimp1, Devolucionventa.baseimp2,Devolucionventa.baseimp3, Devolucionventa.ivaimp1, Devolucionventa.ivaimp2, Devolucionventa.ivaimp3,Devolucionventa.porimp2, Devolucionventa.montoexento, Venta.estatus, Devolucionventa.fecha, Vendedore.descripcion, Devolucionventa.observacion
			order by Devolucionventa.coddevolucion
			";
			$data = $this->query($sql);
		break;
		case 2:
			$order='Venta.codvendedor,Venta.fecha';
			$criteria=" Devolucionventa.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Devolucionventa.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			
			
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>2));
		break;
		case 3:			
			$order='Venta.estatus desc,Venta.codventa';
			$criteria=" DEV.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and DEV.fecha <= '".$hasta." 23:59:59' ";			
		/*	$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;			*/
			//echo $criteria;
			$sql = " select DEV.fecha, DEV.coddevolucion,DEV.numerofactura,DEV.estatus, DEV.codcliente, DEV.codsucursal,DEV.rif,DEV.descripcion, DEV.poriva, contado, DEV.credito, DEV.retencion, DEV.impuestos, sum(DEV.pagoefectivo) as pagoefectivo,sum(DEV.pagodebito) as pagodebito, 
			sum(DEV.pagocheque) as pagocheque, sum(DEV.pagotransfer) as pagotransfer
			from (
			select distinct DV.coddevolucion,DV.fecha , Venta.numerofactura, Venta.estatus, CL.codcliente,CL.codsucursal,CL.rif,cl.descripcion, MCC.poriva,
			CASE WHEN Venta.estatus='P' or  MCC.poriva::text is null  THEN (DV.baseimp1+DV.baseimp2+DV.baseimp3) ELSE 0 END as contado,
			CASE WHEN Venta.estatus='A' or MCC.poriva::text is not null THEN (DV.baseimp1+DV.baseimp2+DV.baseimp3) ELSE 0 END as credito, 
			CASE WHEN Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E' THEN DV.retencion ELSE 0 END as retencion,
			CASE WHEN Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E' THEN (DV.ivaimp1+DV.ivaimp2+DV.ivaimp3) ELSE 0 END as impuestos,
			CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and DVP.tipopago='EFECTIVO' THEN DVP.monto  ELSE 0 END as pagoefectivo, 
			CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and DVP.tipopago='DEBITO' THEN DVP.monto  ELSE 0 END as pagodebito, 
			CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and DVP.tipopago='CHEQUE' THEN DVP.monto  ELSE 0 END as pagocheque,
			CASE WHEN (Venta.estatus='A' or Venta.estatus='P' or Venta.estatus='E') and DVP.tipopago='TRANSFER' THEN DVP.monto  ELSE 0 END as pagotransfer
			from devolucionventa DV
			inner join ventas Venta on (DV.codventa=Venta.codventa)
			inner join clientes CL on (CL.codcliente=Venta.codcliente and CL.codsucursal=Venta.codclientesucursal)
			inner join ventaspagos VTP on (VTP.codventa=Venta.codventa and Venta.codsucursal=VTP.codsucursal)
			inner join (select coddevolucion,codsucursal,tipopago,sum(monto) as monto from devolucionventapago group by coddevolucion,codsucursal,tipopago) DVP on (DVP.coddevolucion=DV.coddevolucion and VTP.codsucursal=DVP.codsucursal)
			left join movimientoivacc MCC on (MCC.codmovimiento=Venta.codventa)
			order by DV.fecha
			) DEV
			where ".$criteria."
			group by DEV.fecha, DEV.coddevolucion,DEV.numerofactura,DEV.estatus, DEV.codcliente, DEV.codsucursal,DEV.rif,DEV.descripcion, DEV.poriva,
			contado, DEV.credito,DEV.retencion,DEV.impuestos";
			
			$data = $this->query($sql);
		break;	
		}
		return $data;
	}

	function generar_linea_txt($datos=array(),$opcion='Cuadrediario'){
		switch ($opcion) {
			case 'Cuadrediario':
			$texto = '';
			$condicion = "  Devolucionventa.fecha>='".$datos['Cuadrediario']['fecha']." 00:00:00' and Devolucionventa.fecha<='".$datos['Cuadrediario']['fecha']." 23:59:59' ";
			$valores = $this->find('all',array('order'=>'Devolucionventapago.coddevolucion','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Devolucionventapago'];
				$texto = $texto."INSERT INTO condevolucionventapago ( coddevolucion, codsucursal, tipopago, monto) values (";
				$texto = $texto."".$fila['coddevolucion'].",".$fila['codsucursal'].",'".$fila['tipopago']."',".$fila['monto'].");";
				//$texto = $texto.'\n';
			}	
			break;				
			case 'Mcuadrediario':
			$texto = '';
			$condicion = "  Devolucionventa.fecha>='".$datos['Mcuadrediario']['fecha']." 00:00:00' and Devolucionventa.fecha<='".$datos['Mcuadrediario']['fecha']." 23:59:59' ";
			$valores = $this->find('all',array('order'=>'Devolucionventapago.coddevolucion','conditions'=>$condicion));
			
			foreach ($valores as $reg) {
				$fila = $reg['Devolucionventapago'];
				$texto = $texto."INSERT INTO condevolucionventapago ( coddevolucion, codsucursal, tipopago, monto) values (";
				$texto = $texto."".$fila['coddevolucion'].",".$fila['codsucursal'].",'".$fila['tipopago']."',".$fila['monto'].");";
				//$texto = $texto.'\n';
			}	
			break;				
		}
		return $texto;
	}
				  
}
?>
