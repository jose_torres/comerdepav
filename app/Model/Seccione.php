<?php
class Seccione extends AppModel
{
    public $name = 'Seccione';
    //public $useTable = 'grupo';

    public $hasMany = array('Esquema' =>
                         array('className'   => 'Esquema',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'seccione_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )
                  );

	public function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Perfiles Padres
		$order="Seccione.nombre ASC ";
		$condiciones = " ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Seccione'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		}		
		return $var;
	}	
	
	public function guardartemp($post=array(),$data_enc=array()){
		$mensaje='Seleccione un Elemento de la Seccion';
		if ($_POST['elemento_id']){
			$criterio=" Seccione.id=".$_POST['elemento_id'];
			$datos_elemento = $this->find('first',array('conditions'=>$criterio));
			$data_enc['Esquema'][$_POST['elemento_id']]=array('seccione_id'=>$_POST['elemento_id'],'plantilla_id'=>$_POST['valorControl'],'elemento_nombre'=>$datos_elemento['Seccione']['nombre']);			
			$mensaje='Se Guardo el detalle';
		}
		if (!isset($data_enc['Esquema']) ){ $data_enc['Esquema']=array(); }
		return array($data_enc,$mensaje);
	}
	
	public function eliminartemp($post='0',$data_enc=array()){
		$mensaje='Se elimino el detalle';		
		unset($data_enc['Esquema'][$post]);
		if (!isset($data_enc['Esquema']) ){ $data_enc['Esquema']=array(); }		
		return array($data_enc,$mensaje);
	}		 
				  
}
?>
