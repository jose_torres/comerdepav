<?php
class Perfile extends AppModel
{
    var $name = 'Perfile';
    var $useTable = 'perfil';
/*   	var $validate = array(
      'descripcion' => array(
	  	'rule' => 'not_empty',
		'message' => 'Por favor indique escriba un nombre.'
			)
    );	*/
//	var $actsAs = 'ExtendAssociations'; 
    var $hasMany = array('Usuario' =>
                         array('className'   => 'Usuario',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'perfil_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )
                  );

	  var $hasAndBelongsToMany = array(
										'Funcione' =>
										 array('className' => 'Funcione',
										 'joinTable' => 'configuracion',
										 'foreignKey' => 'perfil_id',
										 'associationForeignKey'=> 'funcion_id',
										 'conditions' => '',
										 'order' => '',
										 'limit' => '',
										 'unique' => true,
										 'finderQuery' => '',
										 'deleteQuery' => '',
										 ) 
									 ); 				 

    function buscar_menu($id)
	{
		$datos=$this->query("SELECT funcion.id,funcion.nombre,funcion.direccion FROM funcion,configuracion where configuracion.funcion_id=funcion.id and configuracion.perfil_id=$id");
		return $datos;
	}

      /* Funciones para la vista */ 	
	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Perfiles Padres
		$order="Perfile.descripcion ASC ";
		$condiciones = "Perfile.id_padre=0 ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Perfile'];	
			$var[$relleno.$item['id']] = $item['descripcion'];
		}
		break;
		case 1://Muestra Todos los Bancos registrados con Cuentas Asociadas
		$order="Banco.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order));
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Banco'];
			if(count($row['Ctabancos'])>0){	
			$var[$relleno.$item['id']] = $item['descripcion'];
			}
		}
		break;				
		}//Cierre de switch						
		return $var;
	}	
}

?>
