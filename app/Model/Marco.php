<?php
class Marco extends AppModel
{
    public $name = 'Marco';

	public $belongsTo = array('Elemento' => array('className' => 'Elemento',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'elemento_id'
					  ),'Documento' => array('className' => 'Plantilla',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'plantilla_id'
						)
			);
	
	
	public function guardarMarco($id_plantilla=0,$data=array(),$opcion='Encabezado'){
		
		switch ($opcion) {
		case 'Encabezado':	
		if(count($data['Marco'])>0){
			$valores['Marco']=array();
			$cont=0;			
			$this->updateAll(array('Marco.status' => "'C'"), array('Marco.plantilla_id' => $id_plantilla,'Elemento.ubicacion_id'=>1));	
			foreach ($data['Marco'] as $row){
				$esq = $this->find('first',array('conditions'=>'Marco.plantilla_id='.$id_plantilla.' and Marco.elemento_id='.$row['elemento_id'].' '));
				if(isset($esq['Marco']['id'])){
					$valores['Marco'][$cont]['id'] = $esq['Marco']['id'];
				}
				$valores['Marco'][$cont]['plantilla_id'] = $id_plantilla;
				$valores['Marco'][$cont]['status'] = 'A';
				$valores['Marco'][$cont]['elemento_id'] = $row['elemento_id'];			
				$cont=$cont+1;
			}
			if($this->saveAll($valores['Marco'])){
				return true;
			}else{
				return false;
			};
		}else{ return false;}
		break;
		case 'Pie_Pagina':	
			if(count($data['Marco'])>0){
			$valores['Marco']=array();
			$cont=0;			
			$this->updateAll(array('Marco.status' => "'C'"), array('Marco.plantilla_id' => $id_plantilla,'Elemento.ubicacion_id'=>2));
			foreach ($data['Marco'] as $row){
				$esq = $this->find('first',array('conditions'=>'Marco.plantilla_id='.$id_plantilla.' and Marco.elemento_id='.$row['elemento_id'].' '));
				if(isset($esq['Marco']['id'])){
					$valores['Marco'][$cont]['id'] = $esq['Marco']['id'];
				}
				$valores['Marco'][$cont]['plantilla_id'] = $id_plantilla;
				$valores['Marco'][$cont]['status'] = 'A';
				$valores['Marco'][$cont]['elemento_id'] = $row['elemento_id'];			
				$cont=$cont+1;
			}
			if($this->saveAll($valores['Marco'])){
				return true;
			}else{
				return false;
			};
			}else{ return false;}
		break;
		}	
		
	}
	
	public function cargarMarco($id_plantilla=0,$opcion='Todos'){
		
		switch ($opcion) {
		case 'Todos'://Muestra Todos los Perfiles Padres
		$order="Elemento.nombre ASC ";
		$condiciones = " ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Elemento'];	
			$var[$relleno.$item['id']] = $item['nombre'];
		}
		break;
		case 'Encabezado':
		$order="Elemento.nombre ASC ";
		$condiciones = " Elemento.ubicacion_id=1 and Marco.plantilla_id=".$id_plantilla." and Marco.status='A' ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order));		
		$var = array();
		foreach ($datos as $row) {
			$var['Marco'][$row['Marco']['elemento_id']]=array('elemento_id'=>$row['Marco']['elemento_id'],'plantilla_id'=>$row['Marco']['plantilla_id'],'elemento_nombre'=>$row['Elemento']['nombre']);
		}
		break;
		case 'Pie_Pagina'://Muestra Todos los Perfiles Padres
		$order="Elemento.nombre ASC ";
		$condiciones = " Elemento.ubicacion_id=2 and Marco.plantilla_id=".$id_plantilla." and Marco.status='A' ";
		$datos = $this->find('all',array('conditions'=>$condiciones,'order'=> $order));		
		$var = array();
		foreach ($datos as $row) {
			$var['Marco'][$row['Marco']['elemento_id']]=array('elemento_id'=>$row['Marco']['elemento_id'],'plantilla_id'=>$row['Marco']['plantilla_id'],'elemento_nombre'=>$row['Elemento']['nombre']);
		}
		break;
		}		
		return $var;
	}
				  
}
?>
