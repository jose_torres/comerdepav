<?php
class Vendedore extends AppModel
{
    public $name = 'Vendedore';
    /**
 * Use database config
 *
 * @var string
 */
	public $useDbConfig = 'comerdepa';
	public $primaryKey = 'codvendedor';

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'vendedores';
/*
 * */
	public $hasMany = array('Venta' =>
                         array('className'   => 'Venta',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'codvendedor',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),
                  );	

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$order="Vendedore.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Vendedore'];	
			$var[$relleno.$item['codvendedor']] = $item['descripcion'];
		}
		break;			
		}//Cierre de switch						
		return $var;
	}
	
	function reporte($datos=array(),$criteria='',$opcion=0){
		$hasta=$this->anomesdia($datos['fechahasta']);
		$desde=$this->anomesdia($datos['fechadesde']);
		$data=array();
		switch ($opcion) {
		case 0:
			$order='Vendedore.descripcion';
			$criteria= '1=1';
			$c_vend=$this->construir_or($datos['vend'],'Vendedore.codvendedor');
			$criteria=$criteria."  ".$c_vend;			
			$data = $this->find('all',array('conditions'=>$criteria,'order'=> $order,'recursive'=>1));
		break;
		case 1:	
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";			
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;				
			//echo $criteria;
			$sql="select Venta.codventa, Venta.numerofactura, Cliente.rif as Cliente__rif, Cliente.descripcion  as Cliente__descripcion, Venta.baseimp2, Venta.ivaimp2,Venta.montoexento, Venta.estatus,Venta.fecha, Vendedore.descripcion as Vendedore__descripcion, Vendedore.codvendedor as Vendedore__codvendedor, sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo from ventas as Venta
			inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
			inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
			where ".$criteria."
			group by Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, 
			Venta.baseimp2, Venta.ivaimp2,Venta.montoexento, Venta.estatus,Venta.fecha, Vendedore.descripcion, Vendedore.codvendedor
			order by Vendedore.descripcion";
			$data = $this->query($sql);
		break;
		case 2:	
			$criteria=" h_cuentasporcobrar.fechacobro >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and h_cuentasporcobrar.fechacobro <= '".$hasta." 23:59:59' ";	
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;				
			//echo $criteria;
			$sql="select Venta.codventa, Venta.numerofactura, Cliente.rif as Cliente__rif, 
			Cliente.descripcion  as Cliente__descripcion, Venta.baseimp2, Venta.ivaimp2,
			Venta.montoexento, Venta.estatus,Venta.fecha, Vendedore.descripcion as Vendedore__descripcion, 
			Vendedore.codvendedor as Vendedore__codvendedor, 
			h_cuentasporcobrar.fvencimiento, h_cuentasporcobrar.fechacobro, (h_cuentasporcobrar.fvencimiento - Venta.fecha) as dias,
			sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo 
			from ventas as Venta
			inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente and Cliente.codsucursal=Venta.codclientesucursal)
			inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
			inner join ventasproductos Ventaproducto on  (Ventaproducto.codventa=Venta.codventa) inner join h_cuentasporcobrar on (h_cuentasporcobrar.codmovimiento=Venta.codventa and 
			h_cuentasporcobrar.tipomovimiento='VENTAS')
			where ".$criteria."
			group by Venta.codventa, Venta.numerofactura, Cliente.rif, Cliente.descripcion, 
			Venta.baseimp2, Venta.ivaimp2,Venta.montoexento, Venta.estatus,Venta.fecha, 
			Vendedore.descripcion, Vendedore.codvendedor,h_cuentasporcobrar.fvencimiento, h_cuentasporcobrar.fechacobro
			order by Vendedore.descripcion, h_cuentasporcobrar.fechacobro";
			$data = $this->query($sql);
		break;
		case 3:	
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";	
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;				
			//echo $criteria;
			$sql="select Departamento.codigo as Departamento__codigo, Departamento.descripcion  as Departamento__descripcion, Producto.codigo, Producto.nombre, Vendedore.descripcion as Vendedore__descripcion, sum(Ventaproducto.cantidad*Ventaproducto.precio) as Total from ventas as Venta
			inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente 
			and Cliente.codsucursal=Venta.codclientesucursal)
			inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
			inner join producto Producto on (Ventaproducto.codproducto=Producto.codproducto)
			inner join departamentos Departamento on (Departamento.coddepartamento=Producto.coddepartamento)
			where ".$criteria." and Venta.estatus<>'E'
			group by  Departamento.codigo, Departamento.descripcion,Producto.codigo, Producto.nombre, Vendedore.descripcion 
			order by Vendedore.descripcion,Departamento.codigo,Producto.codigo";
			$data = $this->query($sql);
		break;
		case 4:	
			$criteria=" Venta.fecha >= '".$desde." 00:00:00' ";
			$criteria=$criteria." and Venta.fecha <= '".$hasta." 23:59:59' ";	
			$c_cliente=$this->construir_or($datos['client'],'Venta.codcliente');
			$criteria=$criteria."  ".$c_cliente;
			$c_vend=$this->construir_or($datos['vend'],'Venta.codvendedor');
			$criteria=$criteria."  ".$c_vend;
			$c_tipoven=$this->construir_or($datos['tipoven'],'Venta.estatus','letra');
			$criteria=$criteria."  ".$c_tipoven;				
			//echo $criteria;
			$sql="select Departamento.codigo as Departamento__codigo,  Departamento.descripcion  as Departamento__descripcion, Vendedore.descripcion as Vendedore__descripcion, sum(Ventaproducto.cantidad*Ventaproducto.precio) as Total from ventas as Venta
			inner join clientes Cliente on (Cliente.codcliente=Venta.codcliente 
			and Cliente.codsucursal=Venta.codclientesucursal)
			inner join vendedores Vendedore on (Vendedore.codvendedor=Venta.codvendedor)
			inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
			inner join producto Producto on (Ventaproducto.codproducto=Producto.codproducto)
			inner join departamentos Departamento on (Departamento.coddepartamento=Producto.coddepartamento)
			where ".$criteria." and Venta.estatus<>'E'
			group by  Departamento.codigo, Departamento.descripcion,Vendedore.descripcion 
			order by Vendedore.descripcion,Departamento.codigo";
			$data = $this->query($sql);
		break;
		}
		return $data;
	}				  
}
?>
