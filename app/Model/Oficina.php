<?php
class Oficina extends AppModel
{
    var $name = 'Oficina';
    var $useTable = 'oficinas';
    public $actsAs = array('Tree');
//	var $actsAs = 'ExtendAssociations'; 
    var $hasMany = array('Empleado' =>
                         array('className'   => 'Empleado',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'oficina_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         ),
                       /*  'Documento' =>
                         array('className'   => 'Documento',
                               'conditions'  => '',
                               'order'       => '',
                               'limit'       => '',
                               'foreignKey'  => 'oficina_id',
                               'dependent'   => true,
                               'exclusive'   => false,
                               'finderSql'   => ''
                         )*/
                  );
				 

	function llenar_combo($relleno='',$opcion=0){
		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$order="Oficina.descripcion ASC";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Oficina'];	
			$var[$relleno.$item['id']] = $item['descripcion'];
		}
		break;			
		}//Cierre de switch						
		return $var;
	}

	function eliminar($nro_registro=0,$id=0){
		$mensaje='';
		if($nro_registro<=0){
			if ($this->delete($id)){
				$mensaje='La Oficina ha sido Eliminado.';
			}		
		 }else{
			$mensaje='La Oficina no ha sido Eliminado, porque posee registro asociados.'; 
		 }
		return $mensaje; 
	} 

}

?>
