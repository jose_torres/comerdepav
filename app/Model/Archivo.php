<?php
class Archivo extends AppModel
{
    public $name = 'Archivo';
    /**
 * Use database config
 *
 * @var string
 */
    public $useDbConfig = 'comerdepa';
    //public $primaryKey = 'codproducto';

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'archivos';
/*
 * */
    
    function llenar_combo($relleno='',$opcion=0){
            switch ($opcion) {
            case 0:
            $order="Producto.nombre ASC";
            $datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
            $var = array();
            foreach ($datos as $row) {
                    $item = $row['Producto'];	
                    $var[$relleno.$item['codproducto']] = $item['nombre'];
            }
            break;
            case 1://Muestra Todos los Bancos registrados
            $order="Producto.nombre ASC";
            $datos = $this->find('all',array('order'=> $order,'recursive'=>-1));		
            $var = array();
            foreach ($datos as $row) {
                    $item = $row['Producto'];	
                    $var[$relleno.$item['codproducto']] = $item['codigo'].' '.$item['nombre'];
            }
            break;			
            }				
            return $var;
    }

    function ajustarDatos($opcion='pdf',$datos){
        switch ($opcion) {
        case 'pdf':
            if($datos['producto_id']==''){
                    $datos['producto_id']=array();
            }
            if($datos['departamento_id']==''){
                    $datos['departamento_id']=array();
            }			
            if($datos['deposito_id']==''){
                    $datos['deposito_id']=array();
            }
            if($datos['precio']==''){
                    $datos['precio']=array();
            }
            $datos['condicion']=$datos['condicion_id'];
            $inicio=array(0);
            $datos['prod'] = array_merge($inicio, $datos['producto_id']);			
            //$datos['prod']=$datos['producto_id'];
            $datos['depto']=array_merge($inicio, $datos['departamento_id']);
            $datos['dep']=array_merge($inicio, $datos['deposito_id']);	
            
            
            $datos['precio']=array_merge($inicio, $datos['precio']);	
        break;		
        }
        return $datos;
    }
    
    function inicializar(){
        $sql ='ALTER SEQUENCE archivos_id_seq RESTART WITH 1';
        $data = $this->query($sql);
    }
    
    function eliminarProductos(){
        $sql ='delete from archivos';
        $data = $this->query($sql);
    }
    
    function incluirProductos($fichero_dbf,$nombreArchivo){
        $conex = dbase_open($fichero_dbf, 0);
        if($conex){
            $arrData = array();
            $total_registros = dbase_numrecords($conex);
            //dbase_get_record_with_names($conex)
            for ($i = 1; $i <= $total_registros; $i++){
                //$arrData[] = dbase_get_record($conex,$i);
                
                $arrData['Archivo'][$i-1] = dbase_get_record_with_names($conex,$i);
                $arrData['Archivo'][$i-1]['codigo'] = $arrData['Archivo'][$i-1]['CODIGO'];
                $arrData['Archivo'][$i-1]['descripcio'] = utf8_encode ($arrData['Archivo'][$i-1]['DESCRIPCIO']);
                $arrData['Archivo'][$i-1]['departamen'] = utf8_encode ($arrData['Archivo'][$i-1]['DEPARTAMEN']);
                $arrData['Archivo'][$i-1]['costoactu'] = $arrData['Archivo'][$i-1]['COSTOACTU'];
                $arrData['Archivo'][$i-1]['costoprom'] = $arrData['Archivo'][$i-1]['COSTOPROM'];
                $arrData['Archivo'][$i-1]['precio1'] = $arrData['Archivo'][$i-1]['PRECIO1'];
                $arrData['Archivo'][$i-1]['precio2'] = $arrData['Archivo'][$i-1]['PRECIO2'];
                $arrData['Archivo'][$i-1]['precio3'] = $arrData['Archivo'][$i-1]['PRECIO3'];
                $arrData['Archivo'][$i-1]['existencia'] = $arrData['Archivo'][$i-1]['EXISTENCIA'];
                if(isset($arrData['Archivo'][$i-1]['DEPOSITO'])){
                $arrData['Archivo'][$i-1]['deposito'] = $arrData['Archivo'][$i-1]['DEPOSITO'];                
                }
                if(isset($arrData['Archivo'][$i-1]['UNIDAD'])){
                $arrData['Archivo'][$i-1]['unidad'] = $arrData['Archivo'][$i-1]['UNIDAD'];
                }
                $arrData['Archivo'][$i-1]['archivo']=$nombreArchivo;
            }
            
            if($this->saveAll($arrData['Archivo'])){
                echo 'Guardo';
            }else{
                echo 'No Guardo';
            }
            
            //echo '<pre>',print_r($arrData),'</pre>';
        }else{
            echo 'No se pudo acceder al fichero dbf';
        }
    }
    
    function listadoprecio($datos=array()){
		//print_r($datos);
            $criteria= " archivo = 'STOCK21' ";
            //$c_prod=$this->construir_or($datos['prod'],'U.codproducto');
            //$criteria=$criteria."  ".$c_prod;
            $c_depto=$this->construir_or($datos['depto'],'departamen','letra');
            $criteria=$criteria."  ".$c_depto;
            if($datos['condicion']=='' || $datos['condicion']=='Todos'){				
            }else{
                    $criteria=$criteria." and (C21.existencia + C1.existencia + C2.existencia + C3.existencia)".$this->ConversionCondicional($datos['condicion']).$datos['cantidad'];
            }
            
            if(count($datos['nodepto'])>1){
			$c_depto=$this->construir_or($datos['nodepto'],'departamen','noesta_letra');
			$criteria=$criteria." and ".$c_depto;
			} 
         //   echo $criteria;
         //   die();
            $sql= "SELECT  C21.codigo, descripcio, departamen, 
             C21.precio1,    C21.precio2, C21.precio3,unidad,
             C21.precio1, C1.precio1 as p1c1, C2.precio1 as p1c2, C3.precio1 as p1c3, 
C21.precio2, C1.precio2 as p2c1, C2.precio2 as p2c2, C3.precio2 as p2c3, 
C21.precio3, C1.precio3 as p3c1, C2.precio3 as p3c2, C3.precio3 as p3c3,
C21.costoactu, C1.costoactu as c1c1, C2.costoactu as c1c2, C3.costoactu as c1c3,
C21.costoprom, C1.costoprom as cp1c1, C2.costoprom as cp1c2, C3.costoprom as cp1c3,             
              C21.existencia tc21, C1.existencia tc1,C2.existencia tc2,C3.existencia tc3, 
   (C21.existencia + C1.existencia + C2.existencia + C3.existencia) as total
FROM archivos C21
left join ( SELECT  codigo,existencia,precio1, precio2, precio3,costoprom,costoactu FROM archivos where archivo = 'STOCKC1')  C1 on (C1.codigo = C21.codigo)
left join ( SELECT  codigo,existencia,precio1, precio2, precio3,costoprom,costoactu FROM archivos where archivo = 'STOCKC2')  C2 on (C2.codigo = C21.codigo)
left join ( SELECT  codigo,existencia,precio1, precio2, precio3,costoprom,costoactu FROM archivos where archivo = 'STOCKC3')  C3 on (C3.codigo = C21.codigo)
where ".$criteria."
order by departamen ,codigo  ";
            $data=$this->query($sql);
            return $data;
    }
    
    function listadonegativo($datos=array()){
		//print_r($datos);
            $criteria= " archivo = 'STOCK21' ";
            //$c_prod=$this->construir_or($datos['prod'],'U.codproducto');
            //$criteria=$criteria."  ".$c_prod;
            $c_depto=$this->construir_or($datos['depto'],'departamen','letra');
            $criteria=$criteria."  ".$c_depto;
            if($datos['condicion']=='' || $datos['condicion']=='Todos'){				
            }else{
                    $criteria=$criteria." and (C21.existencia ".$this->ConversionCondicional($datos['condicion']).$datos['cantidad']." or C1.existencia ".$this->ConversionCondicional($datos['condicion']).$datos['cantidad']." or C2.existencia ".$this->ConversionCondicional($datos['condicion']).$datos['cantidad']." or C3.existencia ".$this->ConversionCondicional($datos['condicion']).$datos['cantidad']." )" ;
            }
         //   echo $criteria;
         //   die();
            $sql= "SELECT  C21.codigo, descripcio, departamen, costoactu, costoprom, precio1, 
   precio2, precio3,unidad, C21.existencia tc21, C1.existencia tc1,C2.existencia tc2,C3.existencia tc3, 
   (C21.existencia + C1.existencia + C2.existencia + C3.existencia) as total
FROM archivos C21
left join ( SELECT  codigo,existencia FROM archivos where archivo = 'STOCKC1')  C1 on (C1.codigo = C21.codigo)
left join ( SELECT  codigo,existencia FROM archivos where archivo = 'STOCKC2')  C2 on (C2.codigo = C21.codigo)
left join ( SELECT  codigo,existencia FROM archivos where archivo = 'STOCKC3')  C3 on (C3.codigo = C21.codigo)
where ".$criteria."
order by departamen ,codigo  ";
            $data=$this->query($sql);
            return $data;
    }
    
    function compararprecio($datos=array()){
		$criteria= "archivo = 'STOCK21' ";
        $criteria= $criteria." and ( ( C21.precio1 <> C3.precio1 or C21.precio2 <> C3.precio2 or C21.precio3 <> C3.precio3)
or ( C21.precio1 <> C2.precio1 or C21.precio2 <> C2.precio2 or C21.precio3 <> C2.precio3)
or ( C21.precio1 <> C1.precio1 or C21.precio2 <> C1.precio2 or C21.precio3 <> C1.precio3) ) ";
        $c_depto=$this->construir_or($datos['depto'],'departamen','letra');
        $criteria=$criteria."  ".$c_depto;
        
//        echo ' NoDep:'.count($datos['nodepto']); die();
        if(count($datos['nodepto'])>1){
			$c_depto=$this->construir_or($datos['nodepto'],'departamen','noesta_letra');
			$criteria=$criteria." and ".$c_depto;
		}    
            //echo $criteria;
        $sql= "SELECT  C21.codigo, descripcio, departamen, archivo,
C21.precio1, C1.precio1 as p1c1, C2.precio1 as p1c2, C3.precio1 as p1c3, 
C21.precio2, C1.precio2 as p2c1, C2.precio2 as p2c2, C3.precio2 as p2c3, 
C21.precio3, C1.precio3 as p3c1, C2.precio3 as p3c2, C3.precio3 as p3c3,
unidad 
  FROM archivos C21
left join ( SELECT  codigo,precio1, precio2, precio3 FROM archivos where archivo = 'STOCKC1')  C1 on (trim(C1.codigo) = trim(C21.codigo) )
left join ( SELECT  codigo,precio1, precio2, precio3 FROM archivos where archivo = 'STOCKC2')  C2 on (trim(C2.codigo) = trim(C21.codigo))
left join ( SELECT  codigo,precio1, precio2, precio3 FROM archivos where archivo = 'STOCKC3')  C3 on (trim(C3.codigo) = trim(C21.codigo))
where ".$criteria."
order by departamen ,C21.codigo ";
        $data=$this->query($sql);
        return $data;
    }    
    
    function compararcostoactual($datos=array()){
		$criteria= "archivo = 'STOCK21' ";		
        $criteria= $criteria." and ( ( C21.costoactu <> C3.costoactu or C21.costoprom <> C3.costoprom)
or ( C21.costoactu <> C2.costoactu  or C21.costoprom <> C2.costoprom)
or ( C21.costoactu <> C1.costoactu  or C21.costoprom <> C1.costoprom) ) ";
        $c_depto=$this->construir_or($datos['depto'],'departamen','letra');
        $criteria=$criteria."  ".$c_depto;
        
        if(count($datos['nodepto'])>1){
                $c_depto=$this->construir_or($datos['nodepto'],'departamen','noesta_letra');
                $criteria=$criteria." and ".$c_depto;
        } 
            //echo $criteria;
        $sql= "SELECT  C21.codigo, descripcio, departamen, archivo,
C21.costoactu, C1.costoactu as c1c1, C2.costoactu as c1c2, C3.costoactu as c1c3,
C21.costoprom, C1.costoprom as cp1c1, C2.costoprom as cp1c2, C3.costoprom as cp1c3, 
unidad 
  FROM archivos C21
left join ( SELECT  codigo, costoactu, costoprom FROM archivos where archivo = 'STOCKC1')  C1 on (trim(C1.codigo) = trim(C21.codigo) )
left join ( SELECT  codigo,costoactu, costoprom FROM archivos where archivo = 'STOCKC2')  C2 on (trim(C2.codigo) = trim(C21.codigo))
left join ( SELECT  codigo,costoactu, costoprom FROM archivos where archivo = 'STOCKC3')  C3 on (trim(C3.codigo) = trim(C21.codigo))
where ".$criteria."
order by departamen ,C21.codigo ";
        $data=$this->query($sql);
        return $data;
    } 
    
    function comparardescripcion($datos=array()){
		$criteria= "archivo = 'STOCK21' ";
		
        $criteria= $criteria." and (( C21.descripcio <> C3.descripcio) or ( C21.descripcio <> C2.descripcio) or ( C21.descripcio <> C1.descripcio))  ";
        $c_depto=$this->construir_or($datos['depto'],'departamen','letra');
        $criteria=$criteria."  ".$c_depto;
            
            //echo $criteria;
        $sql= "SELECT  C21.codigo,  departamen, archivo, C21.descripcio, C1.descripcio as c1c1, C2.descripcio as c1c2, C3.descripcio as c1c3, unidad 
  FROM archivos C21
left join ( SELECT  codigo, descripcio FROM archivos where archivo = 'STOCKC1')  C1 on (trim(C1.codigo) = trim(C21.codigo) )
left join ( SELECT  codigo,descripcio FROM archivos where archivo = 'STOCKC2')  C2 on (trim(C2.codigo) = trim(C21.codigo))
left join ( SELECT  codigo,descripcio FROM archivos where archivo = 'STOCKC3')  C3 on (trim(C3.codigo) = trim(C21.codigo))
where ".$criteria."
order by departamen ,C21.codigo ";
        $data=$this->query($sql);
        return $data;
    }
    
    function actualizarproductos($sucursal=array()){
        $data = array(); $reg = 0;
        list($reg,$mensaje) = $this->nroproductoscambiados($sucursal);
        $mensaje = '<div class="alert alert-info">No Se Registraron Cambios en Productos</div>';
        if( $reg > 0){
        //$datos = $this->find('all',array('recursive'=>-1)); 
        $datos = $this->productoscambiados($sucursal);
       
        $sql = "select  dblink('dbname=".$sucursal['Sucursal']['bd']." host=".$sucursal['Sucursal']['ip']." user=".$this->USUARIO." password=".$this->CLAVE." port=".$this->PUERTO."','";
        $cant = 0;
        foreach ($datos as $row) {
        $sql=$sql." UPDATE  producto set codigo=''".$row[0]['codigo']."'',nombre=''".$row[0]['nombre']."'',coddepartamento=".$row[0]['coddepartamento'].",codmarca=".$row[0]['codmarca']." , modelo=''".$row[0]['modelo']."'',referencia=''".$row[0]['referencia']."'' , codbarra=''".$row[0]['codbarra']."'',codiva=".$row[0]['codiva'].", estado=''".$row[0]['estado']."'',stockminimo=".$row[0]['stockminimo']." , stockmaximo=".$row[0]['stockmaximo'].", foto=''".$row[0]['foto']."'', unidadprincipal=''".$row[0]['unidadprincipal']."'' ,unidadalterna=''".$row[0]['unidadalterna']."'' , factorconversion=".$row[0]['factorconversion']." ,costoactual=".$row[0]['costoactual'].",costopromedio=".$row[0]['costopromedio']." ,costoanterior=".$row[0]['costoanterior']." ,preciominimo=".$row[0]['preciominimo']." ,preciomayor=".$row[0]['preciomayor']." , preciodetal=".$row[0]['preciodetal'].",preciominimo1=".$row[0]['preciominimo1']."  ,preciomayor1=".$row[0]['preciomayor1']." , preciodetal1=".$row[0]['preciodetal1'].",minimoventap=".$row[0]['minimoventap']." ,minimoventaa=".$row[0]['minimoventaa']." where codproducto=".$row[0]['codproducto']."; ";
            $cant = $cant + 1;
        }
        $sql = $sql."')";
        $data=$this->query($sql);
        $mensaje = '<div class="alert alert-success">Se Actualizaron los datos de '.$cant.'  Productos</div>';
        }
        return $mensaje;
    }
    
    function actualizardbf($direccion){
        $connection = ssh2_connect('192.168.1.10', 22);
        ssh2_auth_password($connection, 'root', 'qwertyu');
        ssh2_scp_recv($connection, '/mnt/Datos/Pedido/BDatos/STOCK21.DBF', $direccion.'/dbf/STOCK21.DBF');
        ssh2_scp_recv($connection, '/mnt/Datos/Pedido/BDatos/STOCKC1.DBF', $direccion.'/dbf/STOCKC1.DBF');
        ssh2_scp_recv($connection, '/mnt/Datos/Pedido/BDatos/STOCKC2.DBF', $direccion.'/dbf/STOCKC2.DBF');
        ssh2_scp_recv($connection, '/mnt/Datos/Pedido/BDatos/STOCKC3.DBF', $direccion.'/dbf/STOCKC3.DBF');
       // ssh2_disconnect($connection);
    }
}
?>
