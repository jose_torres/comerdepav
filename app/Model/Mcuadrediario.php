<?php
class Mcuadrediario extends AppModel {

	public $name = 'Mcuadrediario';
	public $useDbConfig = 'comerdepa';
	public $useTable = 'cuadrediarios';
/*	var $validate = array(
		'nro' => array('numeric'),
		'fecha' => array('date')
	);*/

	//The Associations below have been created with all possible keys, those that are not needed can be removed
	/*public $hasMany = array(
		'Efectivo' => array(
			'className' => 'Efectivo',
			'foreignKey' => 'cuadrediario_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);*/

	function reporte($datos=array(),$criteria=array(),$opcion=0){
		$hasta=$this->anomesdia($datos['hasta']);
		$desde=$this->anomesdia($datos['desde']);
		switch ($opcion) {
		case 0://Usado para los reportes
		$criteria=" CST.fecha >= '".$desde."' ";
		$criteria=$criteria." and CST.fecha <= '".$hasta."' ";
		if(isset($datos['selperiodo'])){			
			if ($datos['selperiodo']=='true' or $datos['selperiodo']==1){
				$criteria=" CST.fecha >= '".$desde."' ";
				$criteria=$criteria." and CST.fecha <= '".$hasta."' ";
			}else{
				$criteria=" EXTRACT('month' from CST.fecha) = '".$datos['mesconsulta']."' ";
				$criteria=$criteria." and EXTRACT('year' from CST.fecha) = '".$datos['anoconsulta']."' ";
			}
		}
		if ($datos['lote']!='' ){
		$campos='';$campos_gastos='';$campos_cupon_elec='';$campos_cupon='';
		$campos_z='';	
		foreach($datos['c_tarjeta'] as $row){
				//$campos=$campos.',';
				$campos=$campos."SUM(CASE WHEN CST.Tipo = '".$row['Tarjetastipo']['descripcion']."' THEN CST.Total ELSE 0 END ) AS ";
				$campos=$campos.'"'.trim($row['Tarjetastipo']['descripcion']).'", ';				
			}
		foreach($datos['c_tipocompragastos_fiscal'] as $row){
				//$campos=$campos.',';
				$campos_gastos=$campos_gastos."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Tipocompragasto']['descorta']."' THEN CST.Total ELSE 0 END ) AS ";
				$campos_gastos=$campos_gastos.'"'.trim($row['Tipocompragasto']['descorta']).'", ';				
			}
		foreach($datos['c_tipocompragastos_Nfiscal'] as $row){
				//$campos=$campos.',';
				$campos_gastos=$campos_gastos."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Tipocompragasto']['descorta']."' THEN CST.Total ELSE 0 END ) AS ";
				$campos_gastos=$campos_gastos.'"'.trim($row['Tipocompragasto']['descorta']).'", ';				
			}
		foreach($datos['c_tipocompragastos_Cr'] as $row){
				//$campos=$campos.',';
				$campos_gastos=$campos_gastos."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Tipocompragasto']['descorta']."' THEN CST.Total ELSE 0 END ) AS ";
				$campos_gastos=$campos_gastos.'"'.trim($row['Tipocompragasto']['descorta']).'", ';				
			}
		foreach($datos['c_tipocompragastos_Pg'] as $row){
				//$campos=$campos.',';
				$campos_gastos=$campos_gastos."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Tipocompragasto']['descorta']."' THEN CST.Total ELSE 0 END ) AS ";
				$campos_gastos=$campos_gastos.'"'.trim($row['Tipocompragasto']['descorta']).'", ';				
			}					
		foreach($datos['c_empresa_cupon'] as $row){
				$campos_cupon_elec=$campos_cupon_elec."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Empresacupon']['descripcion']."_Elec' THEN CST.Total ELSE 0 END ) AS ";
				$campos_cupon_elec=$campos_cupon_elec.'"'.trim($row['Empresacupon']['descripcion']).'_Elec", ';				
			}		
		foreach($datos['c_tarjeta_cupon'] as $row){
				$campos_cupon_elec=$campos_cupon_elec."SUM(CASE WHEN Trim(CST.Tipo) = '".$row[0]['descripcion']."_Elec' THEN CST.Total ELSE 0 END ) AS ";
				$campos_cupon_elec=$campos_cupon_elec.'"'.trim($row[0]['descripcion']).'_Elec", ';				
			}
		foreach($datos['c_empresa_cupon'] as $row){
				$campos_cupon=$campos_cupon."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Empresacupon']['descripcion']."' THEN CST.Total ELSE 0 END ) AS ";
				$campos_cupon=$campos_cupon.'"'.trim($row['Empresacupon']['descripcion']).'", ';				
			}
		foreach($datos['c_retenciones_s'] as $row){
				$campos_z=$campos_z."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Retencione']['monto']."_Base' THEN CST.Total ELSE 0 END ) AS ";
				$campos_z=$campos_z.'"Monto_Excento", ';	
			}	
		foreach($datos['c_retenciones'] as $row){
				$campos_z=$campos_z."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Retencione']['monto']."_Base' THEN CST.Total ELSE 0 END ) AS ";
				$campos_z=$campos_z.'"'.trim($row['Retencione']['monto']).'_Base", ';$campos_z=$campos_z."SUM(CASE WHEN Trim(CST.Tipo) = '".$row['Retencione']['monto']."_Iva' THEN CST.Total ELSE 0 END ) AS ";
				$campos_z=$campos_z.'"'.trim($row['Retencione']['monto']).'_Iva", ';				
			}			
		$sql="select CST.cuadrediario_id,CST.Fecha,
			SUM(CASE WHEN CST.Tipo = 'Efectivos' THEN CST.Total ELSE 0 END ) as Efectivos,
			SUM(CASE WHEN CST.Tipo = 'Cheques' THEN CST.Total ELSE 0 END ) as Cheques,
			".$campos."
			".$campos_cupon_elec."
			".$campos_cupon."
			SUM(CASE WHEN CST.Tipo = 'Cupones' THEN CST.Total ELSE 0 END ) as Cupones,
			SUM(CASE WHEN CST.Tipo = 'Fondos' THEN CST.Total ELSE 0 END ) as Fondos,
			SUM(CASE WHEN CST.Tipo = 'Prestamos' THEN CST.Total ELSE 0 END ) as Prestamos,
			".$campos_gastos."
			".$campos_z."
			sum(CST.Total) as Total
			from 
			(
			select tarjetas.cuadrediario_id,tarjetas.fecha,tarjetastipo.descripcion as Tipo,sum(tarjetas.monto) AS Total 
			from tarjetas 
			INNER JOIN tarjetastipo ON (tarjetas.tarjetastipo_id = tarjetastipo.id) 
			INNER JOIN maquinas ON (tarjetas.maquina_id = maquinas.id)
			LEFT OUTER JOIN bancos AS Banco ON (tarjetas.banco_id = Banco.id)
			LEFT OUTER JOIN empresacupons AS Empresacupon ON (tarjetas.empresacupon_id = Empresacupon.id) 
			where  tarjetastipo.asociado='BANCO' 
			group by tarjetas.cuadrediario_id,tarjetas.fecha,tarjetastipo.descripcion 
			union 
			select efectivos.cuadrediario_id,efectivos.fecha,'Efectivos' as Tipo,
			sum(efectivos.cantidad*Dinerodenominaciones.valor) as Total
			from efectivos, Dinerodenominaciones,cajas 
			where  Dinerodenominaciones.id=efectivos.Dinerodenominacione_id 
			and cajas.id=efectivos.caja_id 
			group by efectivos.cuadrediario_id,efectivos.fecha 
			union 
			select  Cheque.cuadrediario_id, Cheque.fecha,'Cheques' as Tipo, sum(Cheque.monto) 
			from cheques Cheque,bancos Banco 
			where  Banco.id=Cheque.banco_id 
			group by Cheque.cuadrediario_id, Cheque.fecha 
			union 
			select tarjetas.cuadrediario_id,tarjetas.fecha,
			Banco.descripcion ||'_Elec' as Tipo,
			sum(tarjetas.monto) AS Total 
			from tarjetas 
			INNER JOIN tarjetastipo ON (tarjetas.tarjetastipo_id = tarjetastipo.id) 
			INNER JOIN maquinas ON (tarjetas.maquina_id = maquinas.id)
			INNER JOIN bancos AS Banco ON (tarjetas.banco_id = Banco.id) 
			where  tarjetastipo.asociado='CUPON' 
			group by tarjetas.cuadrediario_id,tarjetas.fecha,Banco.descripcion
			union
			select tarjetas.cuadrediario_id,tarjetas.fecha, 
			Empresacupon.descripcion ||'_Elec' as Tipo,
			sum(tarjetas.monto) AS Total 
			from tarjetas 
			INNER JOIN tarjetastipo ON (tarjetas.tarjetastipo_id = tarjetastipo.id) 
			INNER JOIN maquinas ON (tarjetas.maquina_id = maquinas.id)
			INNER JOIN empresacupons AS Empresacupon ON (tarjetas.empresacupon_id = Empresacupon.id) 
			group by tarjetas.cuadrediario_id,tarjetas.fecha,Empresacupon.descripcion
			union
			select U.cuadrediario_id,U.fecha,U.descripcion as Tipo,sum(U.total) as Total
			from
			(select cupones.cuadrediario_id,cupones.fecha,empresacupon.descripcion,
			sum(cupones.monto*cupones.cantidad) as total
			from cupones,empresacupons as empresacupon
			where  cupones.empresacupon_id=empresacupon.id and cupones.cantidad>0
			group by cupones.cuadrediario_id,cupones.fecha,empresacupon.descripcion
			 ) as U
			group by U.cuadrediario_id,U.fecha,U.descripcion
			union
			select fondos.cuadrediario_id,cuadrediarios.fecha,'Fondos' as Tipo,sum(fondos.monto) as Total
			from fondos,cuadrediarios
			where fondos.cuadrediario_id=cuadrediarios.id
			group by fondos.cuadrediario_id,cuadrediarios.fecha
			union
			select prestamos.cuadrediario_id,prestamos.fecha,'Prestamos' as Tipo,sum(prestamos.monto) as Total 
			from prestamos,cajas,prestamotipos 
			where 1=1
			and prestamos.caja_id=cajas.id and prestamos.prestamotipo_id=prestamotipos.id 
			group by prestamos.cuadrediario_id,prestamos.fecha 			
			union
			select compragastos.cuadrediario_id,cuadrediarios.fecha,tipocompragastos.descorta as Tipo,sum(compragastos.monto) as Total 
			from compragastos,tipocompragasto tipocompragastos,cuadrediarios
			where  compragastos.tipocompragasto_id=tipocompragastos.id and /*tipocompragastos.credito='NO'
			and*/ compragastos.cuadrediario_id=cuadrediarios.id 
			group by compragastos.cuadrediario_id,cuadrediarios.fecha,tipocompragastos.descorta
			union
			select Cuadrediario.id,Movbancario.fecha,Tipocompragasto.descorta as Tipo,
			sum(Detallepago.monto) as Total 
			from detallepagos as Detallepago,tipocompragasto as Tipocompragasto, 
			movbancarios as Movbancario 
			LEFT OUTER JOIN cuadrediarios as Cuadrediario ON (Cuadrediario.fecha=Movbancario.fecha)
			where
			Detallepago.tipocompragasto_id=Tipocompragasto.id and 
			Detallepago.movbancario_id=Movbancario.id and Movbancario.monto>0
			group by Cuadrediario.id,Movbancario.fecha,Tipocompragasto.descorta
			union
			select Cuadrediario.id,Cuadrediario.fecha,Zreporte.iva || '_Base' as Tipo, sum(monto_base) as base
			from zreportes AS Zreporte
			INNER JOIN cuadrediarios AS Cuadrediario ON (Zreporte.cuadrediario_id = Cuadrediario.id) 
			group by Cuadrediario.id,Cuadrediario.fecha,Zreporte.iva
			union
			select Cuadrediario.id,Cuadrediario.fecha,Zreporte.iva|| '_Iva' as Tipo, 
			sum(monto_iva) as iva
			from zreportes AS Zreporte
			INNER JOIN cuadrediarios AS Cuadrediario ON (Zreporte.cuadrediario_id = Cuadrediario.id) 
			group by Cuadrediario.id,Cuadrediario.fecha,Zreporte.iva
			) CST
			where ".$criteria."
			group by CST.cuadrediario_id,CST.Fecha
			order by CST.Fecha
			;
		";			
		}else{
		$sql="select CST.cuadrediario_id,CST.Fecha,sum(CST.Total) as Tot
			from 
			(
			select tarjetas.cuadrediario_id,tarjetas.fecha,'Tarjetas' as Tipo,sum(tarjetas.monto) AS Total 
			from tarjetas 
			INNER JOIN tarjetastipo ON (tarjetas.tarjetastipo_id = tarjetastipo.id) 
			INNER JOIN maquinas ON (tarjetas.maquina_id = maquinas.id)
			LEFT OUTER JOIN bancos AS Banco ON (tarjetas.banco_id = Banco.id)
			LEFT OUTER JOIN empresacupons AS Empresacupon ON (tarjetas.empresacupon_id = Empresacupon.id) 
			where  tarjetastipo.asociado='BANCO' 
			group by tarjetas.cuadrediario_id,tarjetas.fecha  
			union 
			select efectivos.cuadrediario_id,efectivos.fecha,'Efectivos' as Tipo,
			sum(efectivos.cantidad*Dinerodenominaciones.valor) as Total
			from efectivos, Dinerodenominaciones,cajas 
			where  Dinerodenominaciones.id=efectivos.Dinerodenominacione_id 
			and cajas.id=efectivos.caja_id 
			group by efectivos.cuadrediario_id,efectivos.fecha 
			union 
			select  Cheque.cuadrediario_id, Cheque.fecha,'Cheques' as Tipo, sum(Cheque.monto) 
			from cheques Cheque,bancos Banco 
			where  Banco.id=Cheque.banco_id 
			group by Cheque.cuadrediario_id, Cheque.fecha 
			union 
			select tarjetas.cuadrediario_id,tarjetas.fecha,'Cupones' as Tipo,sum(tarjetas.monto) AS Total 
			from tarjetas 
			INNER JOIN tarjetastipo ON (tarjetas.tarjetastipo_id = tarjetastipo.id) 
			INNER JOIN maquinas ON (tarjetas.maquina_id = maquinas.id)
			LEFT OUTER JOIN bancos AS Banco ON (tarjetas.banco_id = Banco.id)
			LEFT OUTER JOIN empresacupons AS Empresacupon ON (tarjetas.empresacupon_id = Empresacupon.id) 
			where  tarjetastipo.asociado='CUPON' 
			group by tarjetas.cuadrediario_id,tarjetas.fecha
			union
			select U.cuadrediario_id,U.fecha,'Cupones' as Tipo,sum(U.total) as Total
			from
			(select cupones.cuadrediario_id,cupones.fecha,
			sum(cupones.monto*cupones.cantidad) as total
			from cupones,empresacupons as empresacupon
			where  cupones.empresacupon_id=empresacupon.id and cupones.cantidad>0
			group by cupones.cuadrediario_id,cupones.fecha
			 ) as U
			group by U.cuadrediario_id,U.fecha
			union
			select fondos.cuadrediario_id,cuadrediarios.fecha,'Fondos' as Tipo,sum(fondos.monto) as Total
			from fondos,cuadrediarios
			where fondos.cuadrediario_id=cuadrediarios.id
			group by fondos.cuadrediario_id,cuadrediarios.fecha
			union
			select prestamos.cuadrediario_id,prestamos.fecha,'Prestamos' as Tipo,sum(prestamos.monto) as Total 
			from prestamos,cajas,prestamotipos 
			where 1=1
			and prestamos.caja_id=cajas.id and prestamos.prestamotipo_id=prestamotipos.id 
			group by prestamos.cuadrediario_id,prestamos.fecha 
			union
			select compragastos.cuadrediario_id,cuadrediarios.fecha,'Egresos' as Tipo,sum(compragastos.monto) as Total 
			from compragastos,tipocompragasto tipocompragastos,cuadrediarios
			where  compragastos.tipocompragasto_id=tipocompragastos.id and tipocompragastos.credito='NO'
			and compragastos.cuadrediario_id=cuadrediarios.id 
			group by compragastos.cuadrediario_id,cuadrediarios.fecha 
			union
			select Cuadrediario.id,Movbancario.fecha,'Egresos' as Tipo,
			Detallepago.monto as Total 
			from detallepagos as Detallepago,tipocompragasto as Tipocompragasto, 
			movbancarios as Movbancario 
			LEFT OUTER JOIN cuadrediarios as Cuadrediario ON (Cuadrediario.fecha=Movbancario.fecha)
			where
			Detallepago.tipocompragasto_id=Tipocompragasto.id and 
			Detallepago.movbancario_id=Movbancario.id and Movbancario.monto>0
			and Tipocompragasto.credito='NO' and Tipocompragasto.descorta like '%CAJ%'
			and Tipocompragasto.descorta not like '%CH%'
			group by Cuadrediario.id,Movbancario.fecha,Tipocompragasto.descorta,Detallepago.monto
			) CST
			where ".$criteria."
			group by CST.cuadrediario_id,CST.Fecha
			order by CST.Fecha
			; ";
		}	
		//echo $sql;
			$data=$this->query($sql);
			break;	
		}
		return $data;
	}

	function llenar_combo($relleno=''){
		$order="Cuadrediario.fecha desc";
		$datos = $this->find('all',array('order'=> $order,'recursive'=>0));
		$i=1;
		$var = array();
		$var[0]='Sin Relacion';
		foreach ($datos as $row) {
			$item = $row['Mcuadrediario'];	
			$var[$relleno.$item['id']] = date('d-m-Y',strtotime($item['fecha']));
			$i=$i+1;
		}		
		return $var;
	}
	
	function crear($fecha=''){
		$codigo= 1;
		$nro = $this->find('first',array('conditions'=> "Mcuadrediario.fecha='".$fecha."'",'recursive'=>0,'order'=>'Mcuadrediario.nro desc'));
		if(isset($nro['Mcuadrediario']['nro'])){
			$codigo= $nro['Mcuadrediario']['nro'] + 1;
		}
		$datos['Mcuadrediario']['nro']=$codigo;
		$datos['Mcuadrediario']['fecha']=$fecha;
		$datos['Mcuadrediario']['estatus']=1;
		$this->save($datos);
	}
	
	function buscar($datos=array()){
		$valores = array();
		$fecha=$this->anomesdia($datos['fechahasta']);	
		$nro_reg = $this->find('count',array('conditions'=> "Mcuadrediario.fecha='".$fecha."'",'recursive'=>0));
		if($nro_reg<=0){
			$this->crear($fecha);
		}	
		$valores = $this->find('first',array('conditions'=> "Mcuadrediario.fecha='".$fecha."'",'recursive'=>0));
		
		return $valores;
	}

	function generar_linea_txt($datos=array()){
		$texto = "INSERT INTO concuadrediarios (id_sucursal, codsucursal ,nro, fecha, estatus, created, modified) values (";
		$texto = $texto."".$datos['Mcuadrediario']['id'].",".$datos['Mcuadrediario']['codsucursal'].",'".$datos['Mcuadrediario']['nro']."','".$datos['Mcuadrediario']['fecha']."','".$datos['Mcuadrediario']['estatus']."','".$datos['Mcuadrediario']['created']."','".$datos['Mcuadrediario']['modified']."');";
		//$texto = $texto.'\n';
		return $texto;
	}

/*	function encriptar($texto=''){
		$texto = $this->encrypt($texto);
		return $texto;
	}
	
	function desencriptar($texto=''){
		$texto = $this->decrypt($texto);
		return $texto;
	}
*/
	function encriptar($cadena){
    $key='S';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
    $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $cadena, MCRYPT_MODE_CBC, md5(md5($key))));
    return $encrypted; //Devuelve el string encriptado
 
	}
	 
	function desencriptar($cadena){
		 $key='S';  // Una clave de codificacion, debe usarse la misma para encriptar y desencriptar
		 $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($cadena), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
		return $decrypted;  //Devuelve el string desencriptado
	}

	function obtenerNroVentas($datos=array()){
		$nro = 0;
		$sql = "select count(codventa) as total from ventas VT where VT.estatus <> 'E' and VT.Fecha = '".$datos['Mcuadrediario']['fecha']."'
		group by VT.fecha";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$nro =$data[0][0]['total'];
		}
		return $nro;

	}

	function obtenerNroDevoluciones($datos=array()){
		$nro = 0;
		$sql = "select count(coddevolucion) as total from devolucionventa VT where VT.Fecha >= '".$datos['Mcuadrediario']['fecha']." 00:00:00' and VT.Fecha <= '".$datos['Mcuadrediario']['fecha']." 23:59:59'
		group by VT.fecha";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$nro =$data[0][0]['total'];
		}
		return $nro;
	}

	function obtenerMontoVentas($datos=array()){
		$monto = 0;
		$sql = "select sum(montobruto) as total from ventas VT where VT.estatus <> 'E' and VT.Fecha = '".$datos['Mcuadrediario']['fecha']."' 
		group by VT.fecha";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$monto =$data[0][0]['total'];
		}
		return $monto;
	}
	
	function obtenerMontoDebitos($cuadre=array()){
		$monto = 0;
		$sql = "select sum(monto) as total from sucgastos Congasto where Congasto.fecha='".$cuadre['Mcuadrediario']['fecha']."'  ;";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$monto =$data[0][0]['total'];
		}
		return $monto;
	}
	
	function obtenerMontoDeposito($cuadre=array()){
		$monto = 0;
		$sql = "select sum(monto) as total from movbancarios Conmovbancario where Conmovbancario.fecha='".$cuadre['Mcuadrediario']['fecha']."'  and (Conmovbancario.tipodeposito='CHEQUE' or Conmovbancario.tipodeposito='EFECTIVO' or Conmovbancario.tipodeposito='DEBITO' or Conmovbancario.tipodeposito='TRANSFER');";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$monto =$data[0][0]['total'];
		}
		return $monto;
	}

	function obtenerMontoRetencion($datos=array()){
		$monto = 0;
		$hasta=$this->anomesdia($datos['Mcuadrediario']['fecha']);
		$desde=$this->anomesdia($datos['Mcuadrediario']['fecha']);
		$criteria=" RV.fechaemision >= '".$desde." 00:00:00' ";
		$criteria=$criteria." and RV.fechaemision <= '".$hasta." 23:59:59' ";
		$criteria=$criteria."  ";	
		$sql =  " select RV.fechaemision,sum(RVD.montoretenido) as Total from retenciones_ventas RV inner join retenciones_ventas_detalles RVD on (RV.codretencion  = RVD.codretencion)
		where ".$criteria." group by RV.fechaemision";
		$data = $this->query($sql);
		if(isset($data[0][0]['total'])){
			$monto =$data[0][0]['total'];
		}
		return $monto;
	}

	function actualizarMontosCuadre($id=''){
		$datos_cuadre = $this->read(null, $id);
		$cuadre['Mcuadrediario']['id']=$id;		
		$cuadre['Mcuadrediario']['nro_venta']=$this->obtenerNroVentas($datos_cuadre);
		$cuadre['Mcuadrediario']['nro_devolucion']=$this->obtenerNroDevoluciones($datos_cuadre);
		$cuadre['Mcuadrediario']['monto_venta']=$this->obtenerMontoVentas($datos_cuadre);
		$cuadre['Mcuadrediario']['monto_debito']=$this->obtenerMontoDebitos($datos_cuadre);
		$cuadre['Mcuadrediario']['monto_deposito']=$this->obtenerMontoDeposito($datos_cuadre);
		$cuadre['Mcuadrediario']['monto_retencion']=$this->obtenerMontoRetencion($datos_cuadre);
		$this->save($cuadre);
	}

	function ajustarRegistrosCuadre($opcion='Movbancario_Efectivo',$cuadre_id='',$registros=array()){
		$datos = array();
		switch ($opcion) {
		case 'Movbancario_Efectivo':
			$datos_cuadre = $this->read(null, $cuadre_id); $i = 1;
			foreach ($registros as $row){
				$datos['Movbancario'][$i]['cuadrediario_id']=$cuadre_id;
				$datos['Movbancario'][$i]['cuentasbancaria_id'] = $row[0]['codcuenta'];
				$datos['Movbancario'][$i]['banco_id'] = $row[0]['codbanco'];
				$datos['Movbancario'][$i]['nrodocumento'] = $row[0]['nropago'];
				$datos['Movbancario'][$i]['nrocomprobante'] = $row[0]['coddepbanco'];
				$datos['Movbancario'][$i]['fecha'] = $row[0]['fecha'];
				$datos['Movbancario'][$i]['monto'] = $row[0]['monto_pagado'];
				$datos['Movbancario'][$i]['tipodeposito'] = 'EFECTIVO';
				$datos['Movbancario'][$i]['documentotipo_id']=1;				
				$i= $i + 1;
			}
		break;	
		}
		return $datos;
	}

}
?>
