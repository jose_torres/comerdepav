<?php
class Cuentasbancaria extends AppModel
{
    public $name = 'Cuentasbancaria';
    public $useTable = 'cuentasbancarias';
	public $primaryKey = 'codcuenta';
	public $useDbConfig = 'comerdepa';
    public $belongsTo = array('Banco' => array('className' => 'Banco',
  						'conditions' => '',
						'order' => '',
						'foreignKey' => 'codbanco'
					  	)
					);
	/*var $hasMany = array('Conmovbancario' =>array('className' => 'Conmovbancario',
					 'conditions' => '',
					 'order' => '',
					 'limit' => '',
					 'foreignKey' => 'cuentasbancaria_id',
					 'dependent' => true,
					 'exclusive' => false,
					 'finderQuery' => '',
					 'fields' => '',
					 'offset' => '',
					 'counterQuery' => ''
					 )
				 );					*/

  /* Funciones para la vista */ 	
	function llenar_combo($relleno='',$opcion=0,$condicion=''){

		switch ($opcion) {
		case 0://Muestra Todos los Bancos registrados
		$datos = $this->find('all',array('conditions'=>$condicion));
		$i=1;
		$var = array();
		foreach ($datos as $row) {
			$item = $row['Cuentasbancaria'];	
			$var[$relleno.$item['codcuenta']] = $item['numerocuenta'];
			$i=$i+1;
		}
		break;				
		}//Cierre de switch		
		return $var;
	}

	function tipo_cuenta(){
		$var['Cuenta_Corriente'] = 'Cuenta Corriente';
		$var['Cuenta_Ahorro'] = 'Cuenta de Ahorro';
		return $var;
	}

	function tipo_dirigida(){
		$var['PERSONAL'] = 'Personal';
		$var['JURIDICA'] = 'Juridica';
		return $var;
	}
  /* Fin de Funciones para la vista */ 
							  
}

?>
