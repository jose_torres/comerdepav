<?php if (count($data)>0){?>

<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center">
<thead>
    <tr>
        <th>Codigo</th>
        <th>Descripcion</th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead> 	
    <?php $x=0;foreach ($data as $row): $perfile = $row['Perfile']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $perfile['id'] ?>    
        </td>
        <td>
            <?php echo $html->link($perfile['descripcion'], '/perfiles/view/'.$perfile['id']) ?>    
        </td>      
        <td>
            <?php echo $perfile['created'] ?>
        </td>
        <td>
            <?php 
                if (!empty($perfile['modified'])) echo $perfile['modified'];
                else if (!empty($perfile['updated'])) echo $perfile['updated'];
            ?>
        </td>
        <td align="center">
	  	<?php	echo $html->link( $html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/perfiles/view/".$perfile['id'], array('escape'=>false), null); ?>
	        &nbsp;|&nbsp;
      	  <?php	 echo $html->link( $html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/configuracions/mostrar/".$perfile['id'], array('escape'=>false), null); ?>              
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<? } ?>
