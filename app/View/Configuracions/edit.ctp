<?php print_r($data_perfil); ?>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>

<h4><?php echo empty($params['data']['Configuracion']['id'])?'Add':'Edit' ?> Configuracion</h4>
<form name="frm" id="frm" method="POST" action="<?php echo $html->url(empty($params['data']['Configuracion']['id'])?'add':'edit') ?>">
<?php echo empty($params['data']['Configuracion']['id']) ? null : $html->hidden('Configuracion/id'); ?>
<p>
	Title: <?php echo $html->input('Configuracion/title', array('size'=>'40')) ?>
    <?php echo $html->tagErrorMsg('Configuracion/title', 'Title is required.') ?>
</p>	
<p>
	Body:<br/>
	<?php echo $html->textarea('Configuracion/body',array('columns'=>'5','rows'=>'10')) ?>
    <?php echo $html->tagErrorMsg('Configuracion/body', 'Body is required.') ?>
</p>
<p>
	<?php echo $html->submit('Save') ?>
</p>
</form>
