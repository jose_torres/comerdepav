<?php 
//	echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
	echo $this->Html->script('checktree/checktree'); 	
	echo $this->Html->css(array('checktree/checktree'));
?>
 <script type="text/javascript"><!--

 // USAGE NOTES: Create a new CheckTree() object like so, and pass it its own name.
 var checkmenu = new CheckTree('checkmenu');
 // You can create several such tree objects, just give each a unique name.

 // One optional property: whether to count all checkboxes beneath the current level,
 // or just to count the checkboxes immediately beneath the current level (the default).
 //checkmenu.countAllLevels = true;

 //--></script>
<script language="javascript" type="text/javascript">
	function actualizar(nombrediv,url){	
	  var ajx = new Ajax.Updater(""+nombrediv,""+url, {asynchronous:true, evalScripts:true, 
	  parameters:"&idperfil="+document.frm.ConfiguracionPerfilId.value
	  +"&idfuncion="+document.frm.ConfiguracionFuncionId.value

	  });
	}
	function modificar(nombrediv,url,id,parametros){
	 // alert ('El registro ha sido Modificado');	
	 $('cargando').update('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');	
		var ajx = new Ajax.Updater(""+nombrediv,""+url+id,{asynchronous:true, evalScripts:true, parameters:"?"+parametros,
		 onSuccess: function(){
         $('cargando').update("");
		}
		 });	
	}		
</script>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>		

<?php echo $this->Form->create('Configuracion', array('name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned','url'=>'actualizar')); ?>
<fieldset id="personal" >
<legend  class="info"><h2><strong>REGISTRAR UNA CONFIGURACI&Oacute;N</strong></h2></legend>
<div id="cargando"></div>

<div class="unidades form">
<table>
	<tr>
		<td><?php  	
 $id=1;
 foreach ($data_perfil as $row) {
	$item = $row['Perfile'];	
	$var[$item['id']]=$item['descripcion'];
	$id=$item['id'];
	}
	echo $this->Form->input('Configuracion.perfil_id', array('size'=>'40','label'=>'','options'=>$var,'type'=>'select'/*,'empty' =>'(Seleccione uno ...)'*/,'size'=>'1')); 
?>
</td>
	</tr>
	<tr>
		<td>Selecciones las funciones, y haga Click en la imagen =>:<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Guardar Registro"));
?>	
 </td>
	</tr>
</table>

<ul id="tree-checkmenu" class="checktree">
<?php
	 $x=0;$modulos='';	 $x=0;

	 foreach ($data_arbol as $row) {
		$aux=$modulos;	
		list($modulos,$accion)=explode('/',$row[0]['direccion'].'/');     	
		if ($x==0 || $aux!=$modulos ){	
			if($x>0){
				echo '</ul>';
			}
			$tit=$modulos;
			echo '<li id="show-'.$tit.'">';
			echo '<input id="check-'.$tit.'" type="checkbox" />';
			echo $tit;
			echo '<span id="count-'.$tit.'" class="count"></span>';
			echo '<ul id="tree-'.$tit.'">';
		}//else{
			if ($row[0]['seleccion']==1) $sel='checked'; else $sel='';
			if ($row[0]['seleccion']==1) $selec=true; else $selec=false;
			//echo ' <li><input type="checkbox" id="Conf'.$x.'" value="'.$row[0]['id'].'" '.$sel.'/>'.$row[0]['id'].'=>'.$row[0]['nombre'].'=>'.$row[0]['direccion'].'</li>';
			echo ' <li>'.$this->Form->input("Configuracion.funcion_id$x",array('type' =>'checkbox','value'=>$row[0]['id'],'checked'=>$selec,'div'=>false,'label'=>'')).''.$row[0]['id'].'=>'.$row[0]['nombre'].'=>'.$row[0]['direccion'].$this->Form->input("Configuracion.codigo$x",array('type' =>'hidden','value'=>$row[0]['id'],'checked'=>$selec,'div'=>false,'label'=>'')).'</li>';			
		//}

		$x=$x+1;
		}
		if($x>0){
				echo '</ul>';
				echo '</li>';
		}
?> 
</ul>
</div>

<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/configuracions", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</fieldset>
</form>
