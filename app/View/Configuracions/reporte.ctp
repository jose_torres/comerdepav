<?php
// Construccion de la Cabecera
	$fpdf->SetTitle($titulo);
	$fpdf->SetLogoIzq($logoIzq);
	$fpdf->SetLogoDer($logoDer);
	$fpdf->SetLine1($line1);
	$fpdf->SetLine2($line2);
	$fpdf->SetLine3($line3);
	$fpdf->SetLine4($line4);

// obtiene el nro de paginas    
    $fpdf->AliasNbPages(); 
    $fpdf->AddPage();
    $fpdf->SetFont('Arial','B',16);
	
 //  Anchos de Celdas
    $w=array(15,80,47,48);// maxima sumatoria 190
 	$header=array('Codigo','Nombre','Creado','Modificado');
	$datos=array();
// Este se reliza cuando se requiere mostrar varia informacion en varias tabla	
	foreach ($data as $row){
		$funcion = $row['Funcione'];
		$datos=array();
		foreach ($funcion as $registro){
			$datos[]=array($registro['id'],$registro['nombre'],$registro['created'],$registro['created']);
 		}
		$fpdf->FancyTable($header,$datos,$w,'Funciones Asociadas a '.$row['Perfile']['descripcion']);
	} 

    echo $fpdf->fpdfOutput();    
    
?> 