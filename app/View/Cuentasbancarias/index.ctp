<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<fieldset id="personal" >
<legend><h3>LISTA DE CUENTAS BANCARIAS REGISTRADOS</h3></legend>
<center>
<form action="" method="post" name="two" 'class'="pure-form pure-form-aligned">
<?php
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Buscar.codbanco',array('label'=>'Buscar Por Banco:','empty' =>'Todos','type'=>'select','div'=>false,'options'=>$bancos));
	echo $this->Form->input('Buscar.texto',array('label'=>'Buscar Por N&uacute;mero:','size' =>'40','type'=>'text','div'=>false));	
	echo $this->Html->image('img_acciones/system-search.png',array("onclick"=>"modificar('buscar','buscar/',document.two.BuscarTexto.value+'-'+document.two.BuscarCodbanco.value);", "title"=>"Buscar Registro",'div'=>false,"align"=>"Absmiddle"));
	echo '</div>';
?>
</form>
<div id="buscar">
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover">
<thead>
    <tr>
        <th ><div class="blanco"><?php echo $this->Paginator->sort('codcuenta','Codigo')?></div></th>
        <th>Banco</th>
        <th><div class="blanco"><?php echo $this->Paginator->sort('numerocuenta','Cuenta')?></div></th>
        <th>Descripci&oacute;n</th>
        <th>Tipo de Cuenta</th>  
        <th colspan="2">Acciones</th>
    </tr>
</thead>
<tbody>
    <?php $x=0; foreach ($data as $row): $funcione = $row['Cuentasbancaria']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="pure-table-odd">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $funcione['codcuenta'] ?>    
        </td>
        <td>
            <?php echo $row['Banco']['nombre'] ?>    
        </td>
        <td>
            <?php echo $this->Html->link($funcione['numerocuenta'], '/cuentasbancarias/view/'.$funcione['codcuenta']) ?>    
        </td>      
        <td>
            <?php echo $funcione['titular']; ?>
        </td>
        <td>
            <?php echo $funcione['tipocuenta']; ?>
        </td>
        <td align="center">
	  <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/cuentasbancarias/view/".$funcione['codcuenta'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/cuentasbancarias/edit/".$funcione['codcuenta'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Eliminar Registro", "title"=>"Eliminar Registro")) ,"/cuentasbancarias/delete/".$funcione['codcuenta'], array('escape'=>false),'¿Esta seguro de eliminar la cuenta "'.$funcione['numerocuenta'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>    
</table>
<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, comenzando con el registro {:start}, terminando en {:end}')
	));
	?>	</p>
	<div class="paging">
		<?php echo $this->Paginator->prev('' . $this->Html->image("img_acciones/goprevious.png", array("alt" => "Ver Pagina Anterior", "title"=>"Ver Pagina Anterior")), array('escape'=>false), null, array('class'=>'disabled','escape'=>false));?>
	  	<?php
			echo $this->Paginator->counter(array('format' => __('P&aacute;gina: %page%', true)));
	?> 
		<?php echo $this->Paginator->next($this->Html->image("img_acciones/gonext.png", array("alt" => "Ver Proxima Pagina", "title"=>"Ver Proxima Pagina ")) . '', array('escape'=>false), null, array('class' => 'disabled','escape'=>false));?><br>
		<?php echo 'P&aacute;ginas: |'.$this->Paginator->numbers();
		?>
	</div>
</div>

<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend class="accion"><strong>Acciones</strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/cuentasbancarias/add", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
</fieldset>
</div>
</center>

</fieldset>
