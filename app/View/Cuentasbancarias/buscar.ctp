<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover">
<thead>
    <tr>
        <th ><div class="blanco"><?php echo $this->Paginator->sort('codcuenta','Codigo')?></div></th>
        <th>Banco</th>
        <th><div class="blanco"><?php echo $this->Paginator->sort('numerocuenta','Cuenta')?></div></th>
        <th>Descripci&oacute;n</th>
        <th>Tipo de Cuenta</th>  
        <th colspan="2">Acciones</th>
    </tr>
</thead>
<tbody>
    <?php $x=0; foreach ($data as $row): $funcione = $row['Cuentasbancaria']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="pure-table-odd">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $funcione['codcuenta'] ?>    
        </td>
        <td>
            <?php echo $row['Banco']['nombre'] ?>    
        </td>
        <td>
            <?php echo $this->Html->link($funcione['numerocuenta'], '/cuentasbancarias/view/'.$funcione['codcuenta']) ?>    
        </td>      
        <td>
            <?php echo $funcione['titular']; ?>
        </td>
        <td>
            <?php echo $funcione['tipocuenta']; ?>
        </td>
        <td align="center">
	  <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/cuentasbancarias/view/".$funcione['codcuenta'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/cuentasbancarias/edit/".$funcione['codcuenta'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Eliminar Registro", "title"=>"Eliminar Registro")) ,"/cuentasbancarias/delete/".$funcione['codcuenta'], array('escape'=>false),'¿Esta seguro de eliminar la cuenta "'.$funcione['numerocuenta'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>    
</table>
