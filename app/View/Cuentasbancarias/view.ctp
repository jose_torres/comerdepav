<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<fieldset id="personal" >
<legend class="info"><h3>MOSTRAR UNA CUENTA BANCARIA</h3></legend>
<table>
	<tr>
		<td><label>Banco:</label></td>
		<td><?php echo $data['Banco']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>N&uacute;mero:</label></td>
		<td><?php echo $data['Cuentasbancaria']['numerocuenta']; ?> </td>
	</tr>
	<tr>
		<td><label>Titular:</label></td>
		<td><?php echo $data['Cuentasbancaria']['titular']; ?> </td>
	</tr>
	<tr>
		<td><label>Tipo:</label></td>
		<td><?php echo $data['Cuentasbancaria']['tipocuenta']; ?> </td>
	</tr>
	<tr>
		<td><label>Es una Cuenta:</label></td>
		<td>&nbsp;<?php echo $data['Cuentasbancaria']['tipo']; ?> </td>
	</tr>
		
</table>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend><strong>Acciones</strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/cuentasbancarias/index", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</fieldset>
