<?php
//echo $this->element('menuinterno',$datos_menu);
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
?>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>		
<form class="pure-form pure-form-aligned" name="frm" id="frm" method="POST" action="<?php echo 'add' ?>" enctype="multipart/form-data">	
<fieldset id="personal" >
<!-- <legend  class="info"><h2>REGISTRAR UNA CUENTA BANCARIA</h2></legend> -->
<?php

	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Banco:', array('class' => 'col-xs-4 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-6">';	
	echo $this->Form->input('Cuentasbancaria.codbanco',array('label'=>false,'type'=>'select','div'=>false,'empty' =>'(Seleccione uno ...)','class'=>"form-control select2","style"=>"width: 100%;",'options'=>$bancos));
	echo '</div>';
	echo '</div>';
	
	echo '<div class="form-group">';	
	echo $this->Html->tag('label', 'N&uacute;mero:', array('class' => 'col-xs-4 control-label','for'=>"ejemplo_email_3",'div'=>false));
	echo '<div class="col-xs-6">';
	echo $this->Form->input('Cuentasbancaria.numerocuenta',array('label'=>false,'type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo '</div>';
	
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Titular:', array('class' => 'col-xs-4 control-label','for'=>"ejemplo_email_3"));		
	echo '<div class="col-xs-6">';
	echo $this->Form->input('Cuentasbancaria.titular',array('label'=>false,'type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group">';	
	echo $this->Html->tag('label', 'Tipo de Cuenta:', array('class' => 'col-xs-4 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-6">';	
	echo $this->Form->input('Cuentasbancaria.tipocuenta',array('label'=>false,'type'=>'select','div'=>false,'empty' =>'(Seleccione uno ...)','class'=>"form-control select2","style"=>"width: 100%;",'options'=>$tipcuentas));
	echo '</div>';
	echo '</div>';

	echo '<div class="form-group">';	
	echo $this->Html->tag('label', 'Pertenece a la Cuenta:', array('class' => 'col-xs-4 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-6">';	
	echo $this->Form->input('Cuentasbancaria.tipo',array('label'=>false,'type'=>'select','div'=>false,'empty' =>'(Seleccione uno ...)','class'=>"form-control select2","style"=>"width: 100%;",'options'=>$dirigido));
	echo '</div>';
	echo '</div>';	
?>
</fieldset>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend><strong>Acciones</strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Guardar Registro"));
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/cuentasbancarias/index", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
 </fieldset>
</fieldset>
</form>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script language="javascript" type="text/javascript">
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
     $.extend($.inputmask.defaults.definitions, {
        'A': { 
            validator: "[GJVE]",
            cardinality: 1,
            casing: "upper" //auto uppercasing
        }
    });    
    $("#CuentasbancariaNumerocuenta").inputmask('9999-9999-99-9999999999', {'translation': {
                                        'A': {pattern: /[GJVE]/},
                                        9: {pattern: /[0-9]/}
                                      }
                                }); 

  });
</script>
