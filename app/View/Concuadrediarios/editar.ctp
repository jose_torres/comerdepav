<?php
	echo $this->Html->css('daterangepicker/daterangepicker.css');
	//echo $this->Html->css('daterangepicker/datepicker3.css');
	echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
	echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
	echo $this->Html->css('select2/select2.min.css');
	//echo $this->Html->css('iCheck/all.css');
	echo $this->Html->script(array('funciones.js','View/Concuadrediarios/editar'));
?>

<script language="javascript" type="text/javascript">

</script>
<?php echo $this->Form->create('Concuadrediario',array('role'=>"form",'class'=>"form-horizontal",'url'=>'editar/'.$cuadre['Concuadrediario']['id'].'-'.$cuadre['Concuadrediario']['codsucursal'],'id'=>'frm','name'=>'frm'/*,"onsubmit"=>"return validar();"*/));
?>		
<fieldset id="personal" >

<legend> Edici&oacute;n de los Movimientos del Cuadre Diario 	<?php echo date('d-m-Y',strtotime($cuadre['Concuadrediario']['fecha'])) ;?>		  </legend>
<div id="buscar">
		
	<!-- -->
	<div class="box">
			<div class="box-header">
			 <div class="row">
			  <div class="col-sm-3"><h3 class="box-title">Venta Realizadas:<br>Bs. <span id="ventas_realizadas"><strong><?php echo number_format(sprintf("%01.2f", $ventadeldia[0][0]['total']), 2, ',', '.'); ?></strong></span></h3></div>
			  <div class="col-sm-3"><h3 class="box-title">Dep&oacute;sitos:<br>Bs.<strong> <span id="depositos_realizadas"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.'); ?></span></strong></h3></div>
			  <div class="col-sm-3"><h3 class="box-title">D&eacute;bitos:<br>Bs.<strong> <span id="gastos_realizadas"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.'); ?></span></strong></h3></div>
			  <div class="col-sm-3"><h3 class="box-title">Diferencia:<br>Bs.<strong> <span id="diferencia"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.'); ?></span></strong></h3></div>
			</div>	 	
			  
			</div>
			<!-- /.box-header -->
			<div class="row">
				<div class="col-sm-12">
<?php 
	echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => 'viewpdf', $cuadre['Concuadrediario']['id'].'-'.$cuadre['Concuadrediario']['codsucursal']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
?>
				</div>
			</div>
			
	</div>			
<!-- -->
<div id="EditarDepositos" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-lg">
	  <div class="modal-content">

		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
		  </button>
		  <h4 class="modal-title" id="myModalLabel"><strong>Registrar un Deposito</strong></h4>
		</div>
		<div class="modal-body">
		<p>
		<?php
		echo $this->Form->input('Movbancario.id',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo $this->Form->input('Movbancario.codsucursal',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo $this->Form->input('Movbancario.opcion',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Movbancario.nrodocumento',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo '</div>';		
		echo $this->Html->tag('label', 'Tipo:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4" >';
		echo $this->Form->input('Movbancario.tipo',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));
		echo '</div>';
		echo '</div>';

		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Cuenta Bancario:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Cuentabancaria.numerocuenta',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));

		echo '</div>';		
		echo $this->Html->tag('label', 'Banco:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4" >';
		echo $this->Form->input('Banco.nombre',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));
		echo '</div>';
		echo '</div>';
		
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Monto:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Movbancario.monto',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));

		echo '</div>';		
		echo '</div>';
		?></p>
		<br>
		</div>
		<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardarDeposito(''+document.frm.MovbancarioOpcion.value,'../editarmovimientos/','<?php echo $cuadre['Concuadrediario']['id'];?>' , '&opcion='+document.frm.MovbancarioOpcion.value+'&nrodocumento='+document.frm.MovbancarioNrodocumento.value+'&id='+document.frm.MovbancarioId.value+'&codsucursal='+document.frm.MovbancarioCodsucursal.value);">Guardar Registro</button>
		</div>
	</div>
  </div>
</div>
	

<!-- -->
<div class="box">
	<div class="row">
		<div class="col-sm-12"><h3 class="box-title">Dep&oacute;sitos:</h3></div>
	</div>
	<!-- Depositos -->
	<div class="row">
		<div class="col-sm-6">
			<div class="box-header">
		  <h3 class="box-title">En la Sucursal</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding" id="Deposito_Juridico">
		  <table class="table table-condensed table-bordered" >
					<tr>
		  <th>Nro</th>
		  <th>Mov.</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php
	$A_DEP_SUC = 0;	
    $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    $DEP['Efectivo']=$DEP['Debito']=$DEP['Cheque']=$DEP['Transfer']=0; $GAS=0;
    foreach ($depositos['Efectivo'] as $row){
	if($row['Cuentasbancaria']['tipo']=='JURIDICA'){
		$total_monto=$total_monto+$row['Conmovbancario']['monto'];
		 $x=$x+1;$class = "danger"; $checked = false;
		if($row['Conmovbancario']['conciliado']!='' && $row['Conmovbancario']['conciliado']!='N'){
			$class = "success";
			$checked = true;
		}
      ?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo $row['Conmovbancario']['nrodocumento']; ?></td>
        <td align="center"><a href="#" data-toggle="tooltip" title="<?php echo $row['Documentotipo']['descripcion']; ?>"><?php echo $row['Documentotipo']['descorta']; ?></a></td>
        <td align="center"><a href="#" data-toggle="tooltip" title="<?php echo 'BANCO '.$row['Banco']['nombre']; ?>"><?php echo $row['Banco']['abreviatura']; ?></a></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="center">
			<a href="#" data-toggle="modal" data-target="#EditarDepositos" data-id="<?php echo $row['Conmovbancario']['id']?>" data-opcion="Deposito_Juridico" data-codsucursal="<?php echo $row['Conmovbancario']['codsucursal']?>" data-nrodocumento="<?php echo $row['Conmovbancario']['nrodocumento']; ?>" data-tipo="<?php echo $row['Documentotipo']['descorta']; ?>" data-banco="<?php echo $row['Banco']['nombre']; ?>" data-cuentabancaria="<?php echo $row['Cuentasbancaria']['numerocuenta']; ?>" data-monto="<?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?>" title="<?php echo $row['Documentotipo']['descripcion']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>
			<?php echo $this->Form->checkbox('Seleccionj.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'checked'=>$checked));
		echo $this->Form->input('Seleccionj.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row['Conmovbancario']['id']));
		echo $this->Form->input('Seleccionj.'.$x.'.tipo', array('type' =>'hidden','label' =>'','value'=>$row['Cuentasbancaria']['tipo']));
		?>
        </td>
     <?php
		}
     }
     ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan='4' align="right"><strong>Total:</strong></td>
				<td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
				$DEP['Efectivo']=$total_monto;
				?></strong></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
		  </table>
		</div>
		</div>
		<div class="col-sm-6">
			<div class="box-header">
		  <h3 class="box-title">En C.P.</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding" id="Deposito_Personal">
		  <table class="table table-condensed table-bordered" width="90%">
					<tr>
		  <th>Nro</th>
		  <th>Mov.</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Efectivo'] as $row){
		if($row['Cuentasbancaria']['tipo']=='PERSONAL'){
		$total_monto=$total_monto+$row['Conmovbancario']['monto'];
		$x=$x+1;$class = "danger";$checked = false;
		if($row['Conmovbancario']['conciliado']!='' && $row['Conmovbancario']['conciliado']!='N'){
			$class = "success";$checked = true;
		}
     ?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo $row['Conmovbancario']['nrodocumento']; ?></td>
        <td align="center"><a href="#" data-toggle="tooltip" title="<?php echo $row['Documentotipo']['descripcion']; ?>"><?php echo $row['Documentotipo']['descorta']; ?></a></td>
        <td align="center"><a href="#" data-toggle="tooltip" title="<?php echo 'BANCO '.$row['Banco']['nombre']; ?>"><?php echo $row['Banco']['abreviatura']; ?></a></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="right">
            <a href="#" data-toggle="modal" data-target="#EditarDepositos" data-id="<?php echo $row['Conmovbancario']['id']?>" data-opcion="Deposito_Personal" data-codsucursal="<?php echo $row['Conmovbancario']['codsucursal']?>" data-nrodocumento="<?php echo $row['Conmovbancario']['nrodocumento']; ?>" data-tipo="<?php echo $row['Documentotipo']['descorta']; ?>" data-banco="<?php echo $row['Banco']['nombre']; ?>" data-cuentabancaria="<?php echo $row['Cuentasbancaria']['numerocuenta']; ?>" data-monto="<?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?>" title="<?php echo $row['Documentotipo']['descripcion']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>
			<?php echo $this->Form->checkbox('Seleccionp.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'checked'=>$checked));
		echo $this->Form->input('Seleccionp.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row['Conmovbancario']['id']));
		echo $this->Form->input('Seleccionp.'.$x.'.tipo', array('type' =>'hidden','label' =>'','value'=>$row['Cuentasbancaria']['tipo']));
		?>
        </td>
     <?php
		}
     }
     ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan='4' align="right"><strong>Total:</strong></td>
				<td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
				$DEP['Debito']=$total_monto;
				 ?></strong></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
		  </table>
		</div>
		</div>
	</div>
</div>

<!-- -->
<div id="EditarGastos" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-lg">
	  <div class="modal-content">

		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
		  </button>
		  <h4 class="modal-title" id="myModalLabel"><strong>Registrar un Deposito</strong></h4>
		</div>
		<div class="modal-body">
		<p>
		<?php
		echo $this->Form->input('Gasto.id',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo $this->Form->input('Gasto.codsucursal',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo $this->Form->input('Gasto.opcion',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Gasto.nro',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo '</div>';		
		echo $this->Html->tag('label', 'Nombre:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4" >';
		echo $this->Form->input('Gasto.nombre',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));
		echo '</div>';
		echo '</div>';

		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Motivo:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Gasto.motivo',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));

		echo '</div>';		
		echo $this->Html->tag('label', 'Monto:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Gasto.monto',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));

		echo '</div>';	
		echo '</div>';
		?></p>
		<br>
		</div>
		<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardarGasto(''+document.frm.GastoOpcion.value,'../editarmovimientos/','<?php echo $cuadre['Concuadrediario']['id'];?>' , '&opcion='+document.frm.GastoOpcion.value+'&nro='+document.frm.GastoNro.value+'&id='+document.frm.GastoId.value+'&codsucursal='+document.frm.GastoCodsucursal.value);">Guardar Registro</button>
		</div>
	</div>
  </div>
</div>

<!-- -->
<div class="box">	
	<!-- Debitos -->
	<div class="row">
		<div class="col-sm-12"><h3 class="box-title">D&eacute;bitos:</h3></div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<div class="box-header">
		  <h3 class="box-title">Gastos Realizados</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding" id="Gastos">
		  <table class="table table-bordered table-hover">
		<tr>
		  <th>N&uacute;mero</th>
		  <th>Nombre</th>
		  <th>Motivo</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php
	$A_GAS_SUC = 0;
    $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($gastos['Gastos'] as $row){
		$total_monto=$total_monto+$row['Congasto']['monto'];
		$x=$x+1;$class = "danger"; $checked = false;
		if($row['Congasto']['estatus']!='' && $row['Congasto']['estatus']!='N'){
			$class = "success"; $checked = true;
		}
     ?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo $row['Congasto']['nro']; ?></td>
        <td align="left"><?php echo $row['Congasto']['nombre']; ?></td>
        <td align="left"><?php echo $row['Congasto']['motivo']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Congasto']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
			<a href="#" data-toggle="modal" data-target="#EditarGastos" data-id="<?php echo $row['Congasto']['id']?>" data-opcion="Gastos" data-codsucursal="<?php echo $row['Congasto']['codsucursal']?>" data-nrodocumento="<?php echo $row['Congasto']['nro']; ?>" data-nombre="<?php echo $row['Congasto']['nombre']; ?>" data-motivo="<?php echo $row['Congasto']['motivo']; ?>" data-monto="<?php echo number_format(sprintf("%01.2f", $row['Congasto']['monto']), 2, ',', '.'); ?>" title="<?php echo $row['Congasto']['nombre']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>
           <?php
           echo $this->Form->checkbox('Selecciong.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'checked'=>$checked));
			echo $this->Form->input('Selecciong.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row['Congasto']['id']));
			?>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="3" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			$GAS=$total_monto;
			 ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
		</div>
		<!-- /.box-body -->
	  
		</div>
		<div class="col-sm-6">
		<div class="box-header">
		  <h3 class="box-title">Retenciones Realizadas</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding" id="Retencion">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>N&uacute;mero</th>
		  <th>Factura</th>		  
		  <th>Cliente</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php
	$A_RET_SUC = 0;
    $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;$RET=0;
    foreach ($retenciones['Registros'] as $row){
		$total_monto=$total_monto+$row[0]['montoretenido'];
		$x=$x+1;$class = "danger"; $checked = false;
		if($row[0]['estatus']!='' && $row[0]['estatus']!='A'){
			$class = "success"; $checked = true;
		}
		?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo $row[0]['numero']; ?></td>
        <td align="left"><?php echo $row[0]['documento']; ?></td>
        <td align="left"><?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'); ?></td>
        <td align="center">
			<a href="#" data-toggle="modal" data-target="#EditarRetencion" data-id="<?php echo $row[0]['id']?>" data-opcion="Retencion" data-codsucursal="<?php echo $row[0]['codsucursal']?>" data-documento="<?php echo $row[0]['documento']; ?>" data-numero="<?php echo $row[0]['numero']; ?>" data-cliente="<?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?>" data-montoretenido="<?php echo number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'); ?>"  data-montobruto="<?php echo number_format(sprintf("%01.2f", $row[0]['montobruto']), 2, ',', '.'); ?>" data-montoiva="<?php echo number_format(sprintf("%01.2f", $row[0]['montoiva']), 2, ',', '.'); ?>" title="<?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>
           <?php echo $this->Form->checkbox('Seleccionr.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'class'=>"form-control text-right",'checked'=>$checked));
		echo $this->Form->input('Seleccionr.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row[0]['id_cab']));
		?>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="3" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			$RET=$total_monto;
			?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
		</div>
		<!-- /.box-body -->
	  </div>
		</div>
	</div>
</div>
<!-- -->
<div id="EditarRetencion" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-lg">
	  <div class="modal-content">

		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
		  </button>
		  <h4 class="modal-title" id="myModalLabel"><strong>Registrar un Deposito</strong></h4>
		</div>
		<div class="modal-body">
		<p>
		<?php
		echo $this->Form->input('Conretencionventadet.id',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo $this->Form->input('Conretencionventa.id',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo $this->Form->input('Conretencionventadet.codsucursal',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo $this->Form->input('Conretencionventadet.opcion',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Conretencionventadet.numero',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo '</div>';		
		echo $this->Html->tag('label', 'Factura:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4" >';
		echo $this->Form->input('Conretencionventadet.documento',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Cliente:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-6">';
		echo $this->Form->input('Conretencionventadet.cliente',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-left",'readonly'=>true));

		echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Monto Bruto:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Conretencionventadet.montobruto',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));

		echo '</div>';		
		echo $this->Html->tag('label', 'Monto Iva:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Conretencionventadet.montoiva',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));

		echo '</div>';	
		echo '</div>';

		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Monto Retenido:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Conretencionventadet.montoretenido',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));

		echo '</div>';
		echo '</div>';
		?></p>
		<br>
		<br>
		</div>
		<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardarRetencion(''+document.frm.ConretencionventadetOpcion.value,'../editarmovimientos/','<?php echo $cuadre['Concuadrediario']['id'];?>' , '&opcion='+document.frm.ConretencionventadetOpcion.value+'&numero='+document.frm.ConretencionventadetNumero.value+'&id='+document.frm.ConretencionventadetId.value+'&id_ret='+document.frm.ConretencionventaId.value+'&codsucursal='+document.frm.ConretencionventadetCodsucursal.value);">Guardar Registro</button>
		</div>
	</div>
  </div>
</div>

<script>
document.getElementById('depositos_realizadas').innerHTML='<?php echo number_format(sprintf("%01.2f", $DEP['Efectivo']+$DEP['Debito']+$DEP['Cheque']+$DEP['Transfer']), 2, ',', '.'); ?>';
document.getElementById('gastos_realizadas').innerHTML='<?php echo number_format(sprintf("%01.2f", $GAS + $RET), 2, ',', '.'); ?>';
document.getElementById('diferencia').innerHTML='<?php echo number_format(sprintf("%01.2f", $ventadeldia[0][0]['total'] - ($DEP['Efectivo']+$DEP['Debito']+$DEP['Cheque']+$DEP['Transfer']+$GAS+$RET)), 2, ',', '.'); ?>';
</script>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar a Listado", "title"=>"Regresar a Listado")) ,"/concuadrediarios/index", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</form>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/fastclick/lib/fastclick.js');
	echo $this->Html->script('plugins/iCheck/icheck.min.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $.extend($.inputmask.defaults.definitions, {
        'A': { 
            validator: "[GJVE]",
            cardinality: 1,
            casing: "upper" //auto uppercasing
        }
    });    
    $("#ConretencionventadetNumero").inputmask('9999-99-9999999999', {'translation': {
                                        'A': {pattern: /[GJVE]/},
                                        9: {pattern: /[0-9]/}
                                      }
                                });
});
// Editar Depositos
	$('#EditarDepositos').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Botón que activó el modal
	  var nrodocumento = button.data('nrodocumento') // Extraer la información de atributos de datos
	  var id = button.data('id') // Extraer la información de atributos de datos
	  var codsucursal = button.data('codsucursal') 
	  var opcion = button.data('opcion') 
	  var tipo = button.data('tipo') // Extraer la información de atributos de datos
	  var cuentabancaria = button.data('cuentabancaria') // Extraer la información de atributos de datos
	  var banco = button.data('banco') // Extraer la información de atributos de datos
	  var monto = button.data('monto') // Extraer la información de atributos de datos
	  
	  var modal = $(this)
	  modal.find('.modal-title').text('Modificar Documento nro: '+nrodocumento)
	  modal.find('.modal-body #MovbancarioId').val(id)
	  modal.find('.modal-body #MovbancarioCodsucursal').val(codsucursal)
	  modal.find('.modal-body #MovbancarioOpcion').val(opcion)
	  modal.find('.modal-body #MovbancarioNrodocumento').val(nrodocumento)
	  modal.find('.modal-body #MovbancarioTipo').val(tipo)
	  modal.find('.modal-body #CuentabancariaNumerocuenta').val(cuentabancaria)
	  modal.find('.modal-body #BancoNombre').val(banco)
	  modal.find('.modal-body #MovbancarioMonto').val(monto)
	  $('.alert').hide();//Oculto alert
	});
// Editar Gastos
$('#EditarGastos').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Botón que activó el modal
	  var nro = button.data('nrodocumento') // Extraer la información de atributos de datos
	  var id = button.data('id') // Extraer la información de atributos de datos
	  var codsucursal = button.data('codsucursal') 
	  var opcion = button.data('opcion') 
	  var nombre = button.data('nombre') // Extraer la información de atributos de datos
	  var motivo = button.data('motivo') // Extraer la información de atributos de datos
	  var monto = button.data('monto') // Extraer la información de atributos de datos
	  
	  var modal = $(this)
	  modal.find('.modal-title').text('Modificar Gastos nro: '+id)
	  modal.find('.modal-body #GastoId').val(id)
	  modal.find('.modal-body #GastoCodsucursal').val(codsucursal)
	  modal.find('.modal-body #GastoOpcion').val(opcion)
	  modal.find('.modal-body #GastoNro').val(nro)
	  modal.find('.modal-body #GastoMotivo').val(motivo)
	  modal.find('.modal-body #GastoNombre').val(nombre)
	  modal.find('.modal-body #GastoMonto').val(monto)
	  $('.alert').hide();//Oculto alert
	});
// Editar Retencion
$('#EditarRetencion').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Botón que activó el modal
	  var numero = button.data('numero') // Extraer la información de atributos de datos
	  var documento = button.data('documento') // Extraer la información de atributos de datos
	  var id = button.data('id') // Extraer la información de atributos de datos
	  var codsucursal = button.data('codsucursal') 
	  var opcion = button.data('opcion') 
	  var cliente = button.data('cliente') // Extraer la información de atributos de datos
	  var montobruto = button.data('montobruto') 
	  var montoiva = button.data('montoiva') 
	  var montoretenido = button.data('montoretenido') 
	  
	  var modal = $(this)
	  modal.find('.modal-title').text('Modificar Retencion nro: '+id)
	  modal.find('.modal-body #ConretencionventadetId').val(id)
	  modal.find('.modal-body #ConretencionventaId').val(id)
	  modal.find('.modal-body #ConretencionventadetCodsucursal').val(codsucursal)
	  modal.find('.modal-body #ConretencionventadetOpcion').val(opcion)
	  modal.find('.modal-body #ConretencionventadetNumero').val(numero)
	  modal.find('.modal-body #ConretencionventadetDocumento').val(documento)
	  modal.find('.modal-body #ConretencionventadetCliente').val(cliente)
	  modal.find('.modal-body #ConretencionventadetMontobruto').val(montobruto)
	  modal.find('.modal-body #ConretencionventadetMontoiva').val(montoiva)
	  modal.find('.modal-body #ConretencionventadetMontoretenido').val(montoretenido)
	  $('.alert').hide();//Oculto alert
	});	
</script>
