<table class="table table-bordered table-hover">
		<tr>
		  <th>N&uacute;mero</th>
		  <th>Nombre</th>
		  <th>Motivo</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php
	$A_GAS_SUC = 0;
    $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($gastos['Gastos'] as $row){
		$total_monto=$total_monto+$row['Congasto']['monto'];
		$x=$x+1;$class = "danger"; $checked = false;
		if($row['Congasto']['estatus']!='' && $row['Congasto']['estatus']!='N'){
			$class = "success"; $checked = true;
		}
     ?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo $row['Congasto']['nro']; ?></td>
        <td align="left"><?php echo $row['Congasto']['nombre']; ?></td>
        <td align="left"><?php echo $row['Congasto']['motivo']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Congasto']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
			<a href="#" data-toggle="modal" data-target="#EditarGastos" data-id="<?php echo $row['Congasto']['id']?>" data-opcion="Gastos" data-codsucursal="<?php echo $row['Congasto']['codsucursal']?>" data-nrodocumento="<?php echo $row['Congasto']['nro']; ?>" data-nombre="<?php echo $row['Congasto']['nombre']; ?>" data-motivo="<?php echo $row['Congasto']['motivo']; ?>" data-monto="<?php echo number_format(sprintf("%01.2f", $row['Congasto']['monto']), 2, ',', '.'); ?>" title="<?php echo $row['Congasto']['nombre']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>
           <?php
           echo $this->Form->checkbox('Selecciong.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'checked'=>$checked));
			echo $this->Form->input('Selecciong.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row['Congasto']['id']));
		?>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="3" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			$GAS=$total_monto;
			 ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
