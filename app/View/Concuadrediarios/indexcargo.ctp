<?php
//Funciones js propias del archivo.ctp
echo $this->Html->script(array('funciones.js','View/Concuadrediarios/index'));
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('select2/select2.min.css');
echo $this->Html->css('datatables/dataTables.bootstrap.css');
echo $this->Html->css('datatables/rowGroup.dataTables.min.css');
?>
<style>
    table.dataTable tr.group-end td {
    text-align: right;
    font-weight: normal;
}
</style>

<fieldset id="personal" >
<legend> Listado de Cargos y Descargos Transferidos</legend>
<fieldset id="personal" >
<div class="box">
	<br>	
<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'viewpdfventas/1','id'=>'frm','name'=>'frm'));
?>	
<?php
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-5">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));

echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
$fecha2=date("d-m-Y");
//$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
$fecha=date('d-m-Y',strtotime($primeraFecha));
echo $this->Html->tag('label', 'Rango de Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-3">';	
	echo $this->Form->input('Reporte.rango',array('class'=>"input-group form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha.' al '.$fecha2,'readonly'=>true));
	echo '</div>';	
	echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Mostrar Solo Movimientos Pendientes:', array('class' => 'col-xs-4 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-1 ">';	
	echo $this->Form->input('Reporte.solopendiente',array('class'=>"",'div'=>false,'label'=>false,'placeholder'=>"Desde","type"=>"checkbox"));
	echo '</div>';
echo '<div class="col-xs-4 ">';		
?>

 <button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarlistadoscargos','','fecha='+document.frm.ReporteRango.value+'&sucursal='+document.frm.ReporteSucursal.value+'&solo='+document.frm.ReporteSolopendiente.checked)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
<!-- <button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	-->
<?php
	echo '</div>';	
echo '</div>';	

	echo $this->Form->end();
?>
</div>
<center>
<div id="buscar">
<div class="box">
<div class="box-header">
  <h3 class="box-title">Listado de Cargos y Descargos para Conciliar</h3>
</div>    
<table class="display table table-bordered" id="example">
    <caption><h4>Cargos y Descargos</h4></caption> 
<thead>
    <tr>
        <th >Nro</th>
        <th>Fecha</th>
        <th>Sucursal</th>
        <th>Nro Cargos</th>
        <th>Nro Descargos</th>
        <th>Nro Conciliados</th>
        <th>Nro Desconciliado</th>
        <th>Estado</th>        
        <th >Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $registro = $row[0]; ?>    
    <?php $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $x; ?>    
        </td>
        <td>
            <?php echo $this->Html->link(date('d-m-Y',strtotime($registro['fecha'])), '/concuadrediarios/view/'.$registro['fecha'].'-'.$registro['codsucursal']) ?>    
        </td>
        <td><?php echo $sucursales[$registro['codsucursal']];  ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $registro['nro_cargo']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $registro['nro_descargo']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $registro['nro_conciliado']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $registro['nro_desconciliado']), 2, ',', '.'); ?></td>
         <td><?php echo $registro['estatus'];  ?></td> 
      
        <td align="center">
        <?php
        if($registro['nro_usointerno']>0){
            echo '<span class="btn btn-warning" >U</span>';	
            echo '&nbsp;&nbsp;&nbsp;';
        }
        echo $this->Html->link(__('<span class="glyphicon glyphicon-ok"></span>&nbsp;Conciliar'), array('action' => 'conciliarcargo', $registro['codsucursal'].'-'.$registro['fecha']),array("role"=>"button", "class"=>"btn btn-success",'escape'=>false)); 
        echo '&nbsp;&nbsp;&nbsp;';
        echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => 'viewpdfcargos', $registro['codsucursal'].'-'.$registro['fecha']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
        //echo '&nbsp;&nbsp;&nbsp;';
         ?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
</div>	
</div>
</center>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	//echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/ventas/cierre", array('escape'=>false), null);
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
<?php
      
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/daterangepicker/daterangepicker.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
  
	//Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
//Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD-MM-YYYY'});
    //Date range as a button
    $('input[id="ReporteRango"]').daterangepicker(
        {format: 'DD-MM-YYYY',
        separator: " al ",
        startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
    "locale": {
        "applyLabel": "Aceptar",
        "cancelLabel": "Cancelar",
        "fromLabel": "Desde",
        "toLabel": "Hasta",
        "customRangeLabel": "Por Rango de Fecha",
        "weekLabel": "W",
        "daysOfWeek": [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        "monthNames": [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
        "firstDay": 1
    },
          
        },
        function (start, end,label) {
          $('input[id="ReporteRango"]').html(start.format('DD-MM-YYYY') + ' al ' + end.format('DD-MM-YYYY'));
        }
    );
</script>	
