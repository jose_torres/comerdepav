<?php
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
//Funciones js propias del archivo.ctp
echo $this->Html->script(array('funciones.js','View/Concuadrediarios/transferir'));
?>

<fieldset id="personal" >
<legend> Cuadres a Transferir</legend>
<fieldset id="personal" >
<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'viewpdfventas/1','id'=>'frm','name'=>'frm'));
?>	
<?php
echo '<div class="form-group">';
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-1,date('Y')));
echo $this->Html->tag('label', 'Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="input-daterange input-group col-xs-3" id="datepicker">';

echo $this->Form->input('Reporte.hasta',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Hasta",'value'=>$fecha2,'readonly'=>true)); 
echo '</div>';		
echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));

echo '</div>';
echo '</div>';
?>
 <button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarcierre/',''+document.frm.ReporteSucursal.value,'fecha='+document.frm.ReporteHasta.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
 <?php
	echo $this->Html->link(__('<span class="glyphicon glyphicon-import"></span>&nbsp;Importar Cuadre'), array('action' => '/import'),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 		
				
?>
<!-- <button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	-->
<?php echo $this->Form->end(); ?>	
<center>
<div id="buscar">

</div>
</center>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	//echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/ventas/cierre", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
</script>
