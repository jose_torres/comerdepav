<?php
//Funciones js propias del archivo.ctp
echo $this->Html->script(array('funciones.js','View/Concuadrediarios/index'));
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('select2/select2.min.css');
?>

<fieldset id="personal" >
<legend> Ventas Diarias por Sucursales</legend>
<fieldset id="personal" >
<div class="box">
	<br>	
<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'viewpdfventas/1','id'=>'frm','name'=>'frm'));
?>	
<?php
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Mes:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-2">';
echo $this->Form->input('Reporte.mes',array('div'=>false,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Reporte",'class'=>"form-control select2","style"=>"width: 100%;",'type' => 'date', 'dateFormat' => 'M'));
echo '</div>';
echo $this->Html->tag('label', 'A&ntilde;o:', array('class' => 'col-xs-1 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-2">';
echo $this->Form->input('Reporte.year',array('div'=>false,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Reporte",'class'=>"form-control  select2","style"=>"width: 100%;",'type' => 'date', 'dateFormat' => 'Y','minYear' => 2015,'maxYear' => date('Y')));
echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Mostrar SD:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-1 ">';	
	echo $this->Form->input('Reporte.solopendiente',array('class'=>"",'div'=>false,'label'=>false,'placeholder'=>"Desde","type"=>"checkbox"));
	echo '</div>';
echo '<div class="col-xs-4 ">';		
?>

 <button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarventas','','mes='+document.frm.ReporteMesMonth.value+'&anio='+document.frm.ReporteYearYear.value+'&sdp='+document.frm.ReporteSolopendiente.checked)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
<!-- <button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	-->
<?php
	echo '</div>';	
echo '</div>';	

	echo $this->Form->end();
?>
</div>
<center>
<div id="buscar" class="box table-responsive">

</div>
</center>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	//echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/ventas/cierre", array('escape'=>false), null);
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/daterangepicker/daterangepicker.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
	//Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
//Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD-MM-YYYY'});
    //Date range as a button
    $('input[id="ReporteRango"]').daterangepicker(
        {format: 'DD-MM-YYYY',
        separator: " al ",
        startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
    "locale": {
        "applyLabel": "Aceptar",
        "cancelLabel": "Cancelar",
        "fromLabel": "Desde",
        "toLabel": "Hasta",
        "customRangeLabel": "Por Rango de Fecha",
        "weekLabel": "W",
        "daysOfWeek": [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        "monthNames": [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
        "firstDay": 1
    },
          
        },
        function (start, end,label) {
          $('input[id="ReporteRango"]').html(start.format('DD-MM-YYYY') + ' al ' + end.format('DD-MM-YYYY'));
        }
    );
</script>	
