<?php
    echo $this->Html->css('daterangepicker/daterangepicker.css');
    echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
    echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
    echo $this->Html->css('select2/select2.min.css');
    echo $this->Html->script(array('funciones.js','View/Concuadrediarios/conciliar'));  
?>

<?php echo $this->Form->create('Concuadrediario',array('role'=>"form",'class'=>"form-horizontal",'url'=>'guardarmarcadoscargo/'.$codsucursal.'-'.$fecha,'id'=>'frm','name'=>'frm'/*,"onsubmit"=>"return validar();"*/));
?>		
<fieldset id="personal" >

<legend> Conciliaci&oacute;n de Cargos y Descargo de Fecha <?php echo date('d-m-Y',strtotime($fecha)) ;?> De La <?php echo $sucursal[$codsucursal];?>		  </legend>
<div id="buscar">
		
<!-- -->
<div class="box">
<!-- /.box-header -->
    <div class="row">
            <div class="col-sm-12">
<?php 
echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => 'viewpdfcargos', $codsucursal.'-'.$fecha),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
?>					
<button type="button" class="btn btn-info" onclick="seleccionar_todo()">Marcar Todo &nbsp;<span class="glyphicon glyphicon-list-alt"></span></button>
<button type="button" class="btn btn-danger" onclick="deseleccionar_todo()">Desmarcar Todo &nbsp;<span class="glyphicon glyphicon-list-alt"></span></button>
<button type="button" class="btn btn-success" onclick='document.getElementById("frm").submit()'>Conciliar&nbsp;<span class="glyphicon glyphicon-ok"></span></button>
            </div>
    </div>

</div>			
<!-- -->
</div>
<!-- -->
	
<!-- Cargos -->
<div class="box">	
	<!-- Debitos -->
	<div class="row">
		<div class="col-sm-12"><h3 class="box-title">Cargos:</h3></div>
	</div>
        <div class="box-body">
	  <strong>Mensaje:</strong> 	  
          &Uacute;ltimo n&uacute;mero de la Fecha anterior es:&nbsp;<span class="btn btn-info"><strong><?php echo str_pad($ultimonro['cargo'], 8, "0", STR_PAD_LEFT); ?></strong></span>.
        </div>
	<div class="row">
		<div class="col-sm-12">
		<div class="box-header">
		  <h3 class="box-title">Cargos</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding" id="Cargos">	
		  <table class="table table-bordered table-hover">
		<tr>
		  <th>N&uacute;mero</th>
		  <th>Motivo</th>
		  <th>Autorizado</th>		 	  
		  <th>Responsable</th>
		  <th>Hora</th>
                  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php
	$A_DEV_SUC = 0;
    $x=0;$total_registros=0; $total_cantidad=0;$total_monto_neto=$total_monto_iva=$total_monto=0;
    foreach ($concargos as $row){
            
        $total_monto=$total_monto + $row[0]['total'];
        $x=$x+1;$class = "danger"; $checked = false;
        if($row[0]['conciliar']!='' && $row[0]['conciliar']!='N'){
                $class = "success"; $checked = true;
        }
     ?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo str_pad($row[0]['codcargo'], 8, "0", STR_PAD_LEFT); ?></td>
        <td align="left"><?php echo $row[0]['descripcion']; ?></td>
        <td align="left"><?php echo $row[0]['autorizado']; ?></td>        
        <td align="left"><?php echo $row[0]['responsable']; ?></td>
         <td align="left"><?php echo $row[0]['hora']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total'] ), 2, ',', '.'); ?></td>
        <td align="center">
		<a href="#" data-toggle="modal" data-target="#VerCargos" data-id="<?php echo $row[0]['id']?>"  data-opcion="Cargo" data-codsucursal="<?php echo $row[0]['codsucursal']?>" data-codcargo="<?php echo $row[0]['codcargo']?>"  data-numero="<?php echo $row[0]['codcargo']; ?>" data-monto="<?php echo $row[0]['total']; ?>" data-autorizado="<?php echo $row[0]['autorizado']; ?>" data-descripcion="<?php echo $row[0]['descripcion']; ?>"  title="<?php echo " Cargo Nro ".$row[0]['codcargo']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>	
<?php
        echo $this->Form->checkbox('Seleccioncar.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'checked'=>$checked));
        echo $this->Form->input('Seleccioncar.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row[0]['id']));
?>              
        </td>
     <?php } ?>
        </tbody>
        <tfoot>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>                        
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tfoot>
</table>
        </div>
        <!-- /.box-body -->	  
    </div>
</div>
<div id="VerCargos" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-lg">
	  <div class="modal-content">

		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
		  </button>
		  <h4 class="modal-title" id="myModalLabel"><strong>Cargos</strong></h4>
		</div>
		<div class="modal-body" id="cargo_detalle">
		<p>
		</p>
		<br>
		<br>
		</div>
		<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
	</div>
  </div>
</div>

</div>       
<!-- -->
<!-- Descargos -->
<div class="box">	
	<!-- Debitos -->
	<div class="row">
		<div class="col-sm-12"><h3 class="box-title">Descargos:</h3></div>
	</div>
	<div class="box-body">
            <p> <strong>Mensaje:</strong> 	  
                &Uacute;ltimo n&uacute;mero de la Fecha anterior es:&nbsp;<span class="btn btn-info"><strong><?php echo str_pad($ultimonro['descargo'], 8, "0", STR_PAD_LEFT); ?></strong></span>.  </p>   <p>   
	  <strong>Leyenda:</strong> 	  
          <span class="label label-warning">U</span>Descargo de Uso Interno.</p>
        </div>
	<div class="row">
		<div class="col-sm-12">
		<div class="box-header">
		  <h3 class="box-title">Descargos</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding" id="Descargos">	
		  <table class="table table-bordered table-hover">
		<tr>
		  <th>N&uacute;mero</th>
		  <th>Motivo</th>
		  <th>Autorizado</th>		 	  
		  <th>Responsable</th>
		  <th>Hora</th>
                  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php
	$A_DEV_SUC = 0;
    $x=0;$total_registros=0; $total_cantidad=0;$total_monto_neto=$total_monto_iva=$total_monto=0;
    foreach ($condescargos as $row){
            
        $total_monto=$total_monto + $row[0]['total'];
        $x=$x+1;$class = "danger"; $checked = false;
        if($row[0]['conciliar']!='' && $row[0]['conciliar']!='N'){
                $class = "success"; $checked = true;
        }
     ?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo str_pad($row[0]['coddescargo'], 8, "0", STR_PAD_LEFT); ?></td>
        <td align="left"><?php echo $row[0]['descripcion']; 
        if($row[0]['usointerno']=='true'){
			echo '&nbsp;<span class="label label-warning" >U</span>';		
		}
        ?>
        
        </td>
        <td align="left"><?php echo $row[0]['autorizado']; ?></td>        
        <td align="left"><?php echo $row[0]['responsable']; ?></td>
         <td align="left"><?php echo $row[0]['hora']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total'] ), 2, ',', '.'); ?></td>
        <td align="center">
		<a href="#" data-toggle="modal" data-target="#VerDescargos" data-id="<?php echo $row[0]['id']?>"  data-opcion="Descargo" data-codsucursal="<?php echo $row[0]['codsucursal']?>" data-coddescargo="<?php echo $row[0]['coddescargo']?>"  data-numero="<?php echo $row[0]['coddescargo']; ?>" data-monto="<?php echo $row[0]['total']; ?>" data-autorizado="<?php echo $row[0]['autorizado']; ?>" data-descripcion="<?php echo $row[0]['descripcion']; ?>"  title="<?php echo " Cargo Nro ".$row[0]['coddescargo']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>	
<?php
    echo $this->Form->checkbox('Selecciondescar.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'checked'=>$checked));
    echo $this->Form->input('Selecciondescar.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row[0]['id']));
?>
        </td>
     <?php } ?>
        </tbody>
        <tfoot>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>                        
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
        </tfoot>
</table>
        </div>
        <!-- /.box-body -->	  
    </div>
</div>
<div id="VerDescargos" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-lg">
	  <div class="modal-content">

		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
		  </button>
		  <h4 class="modal-title" id="myModalLabel"><strong>Descargos</strong></h4>
		</div>
		<div class="modal-body" id="descargo_detalle">
		<p>
		</p>
		<br>
		<br>
		</div>
		<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
	</div>
  </div>
</div>

</div>       
<!-- -->

<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar a Listado", "title"=>"Regresar a Listado")) ,"/concuadrediarios/indexcargo", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</form>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	//echo $this->Html->script('plugins/fastclick/lib/fastclick.js');
	echo $this->Html->script('plugins/iCheck/icheck.min.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
    $.extend($.inputmask.defaults.definitions, {
        'A': { 
            validator: "[GJVE]",
            cardinality: 1,
            casing: "upper" //auto uppercasing
        }
    });    
    $("#ConretencionventadetNumero").inputmask('9999-99-9999999999', {'translation': {
                                        'A': {pattern: /[GJVE]/},
                                        9: {pattern: /[0-9]/}
                                      }
                                });
});


// Ver Cargos
$('#VerCargos').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var numero = button.data('numero'); // Extraer la información de atributos de datos
        var id = button.data('id'); // Extraer la información de atributos de datos
        var codsucursal = button.data('codsucursal') ;
        var codcargo = button.data('codcargo') ;
        var opcion = button.data('opcion') ;
        var descripcion = button.data('descripcion') ;
        var monto = button.data('monto'); 

        actualizar("cargo_detalle","../buscarcargos","","&id="+id+"&codsucursal="+codsucursal+"&codcargo="+codcargo+"&monto="+monto);
        var modal = $(this);
        modal.find('.modal-title').text(' Cargo Nro: '+numero+" Descripcion: "+descripcion);

        $('.alert').hide();//Oculto alert
});

// Ver Descargos
$('#VerDescargos').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Botón que activó el modal
        var numero = button.data('numero'); // Extraer la información de atributos de datos
        var id = button.data('id'); // Extraer la información de atributos de datos
        var codsucursal = button.data('codsucursal') ;
        var coddescargo = button.data('coddescargo') ;
        var opcion = button.data('opcion'); 
        var descripcion = button.data('descripcion') ;
        var monto = button.data('monto') ;

        actualizar("descargo_detalle","../buscardescargos","","&id="+id+"&codsucursal="+codsucursal+"&coddescargo="+coddescargo+"&monto="+monto);
        var modal = $(this);
        modal.find('.modal-title').text(' Descargo Nro: '+numero+" Descripcion: "+descripcion);

        $('.alert').hide();//Oculto alert
});
</script>
