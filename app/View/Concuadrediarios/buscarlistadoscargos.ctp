<?php
    echo $this->Html->css('datatables/dataTables.bootstrap.css');
    //print_r($datos);
?>
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Cuadre Transferidos Encontrados</h3>
    </div>
    <div class="box-body">
          <strong>Leyenda:</strong> 
	  <span class="label label-warning">U</span>Contiene Descargo de Uso Interno.
    </div>
<table border="1" cellpadding="1" cellspacing="0" width="95%" align="center" class="table table-condensed table-bordered table-hover" id="ListadoCuadre">
<thead>
    <tr>
        <th >Nro</th>
        <th>Fecha</th>
        <th>Sucursal</th>
        <th>Nro Cargos</th>
        <th>Nro Descargos</th>
        <th>Nro Conciliados</th>
        <th>Nro Desconciliado</th>
        <th>Estado</th>        
        <th >Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $registro = $row[0]; ?>    
    <?php $x++; 
    if($x % 2 == 0){ ?>
	<tr class="roweven">
    <?php } else { ?>
	<tr>
    <?php } ?>
        <td>
    <?php echo $x; ?>    
        </td>
        <td>
            <?php echo $this->Html->link(date('d-m-Y',strtotime($registro['fecha'])), '/concuadrediarios/view/'.$registro['fecha'].'-'.$registro['codsucursal']) ?>    
        </td>
        <td><?php echo $sucursales[$registro['codsucursal']];  ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $registro['nro_cargo']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $registro['nro_descargo']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $registro['nro_conciliado']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $registro['nro_desconciliado']), 2, ',', '.'); ?></td>
         <td><?php echo $registro['estatus'];  ?></td> 
      
        <td align="center">
        <?php
        if($registro['nro_usointerno']>0){
            echo '<span class="btn btn-warning" >U</span>';	
            echo '&nbsp;&nbsp;&nbsp;';
        }
        echo $this->Html->link(__('<span class="glyphicon glyphicon-ok"></span>&nbsp;'), array('action' => 'conciliarcargo', $registro['codsucursal'].'-'.$registro['fecha']),array("role"=>"button", "class"=>"btn btn-success",'escape'=>false)); 
        echo '&nbsp;&nbsp;&nbsp;';
        echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;'), array('action' => 'viewpdfcargos', $registro['codsucursal'].'-'.$registro['fecha']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
        //echo '&nbsp;&nbsp;&nbsp;';
         ?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
<tfoot>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tfoot>
</table>
</div>
<?php
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('bootstrap.min.js');
    echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
	$('[data-toggle="tooltip"]').tooltip();

    $('#ListadoCuadre').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
   

  });
 
</script>
