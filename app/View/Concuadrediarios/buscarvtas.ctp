<?php
	//print_r($data_vent);
	//echo '<br>';
	//print_r($data);
?>
<table class="table table-bordered table-hover">
		<tr>
		  <th>C&oacute;digo</th>
		  <th>Producto</th>
		  <th>Cantidad</th>
		  <th>Precio</th>		  
		  <th>Total</th>
		</tr>
		<tbody>
<?php
	$total_cantidad = $total_monto = 0;
	foreach ($data_vent as $row){
?>
		<tr id='td_id' >
        <td align="center"><?php echo $row['Producto']['codigo']; ?></td>
        <td align="left"><?php echo $row['Producto']['nombre']; ?></td>
        <td align="right"><?php echo $row['Conventaproducto']['cantidad']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f",$row['Conventaproducto']['precio']), 2, ',', '.'); ?></td>
        <td align="right"><?php
		$calculo = $row['Conventaproducto']['precio']*$row['Conventaproducto']['cantidad'];
		$total_monto = $total_monto + $calculo;
		$total_cantidad = $total_cantidad + $row['Conventaproducto']['cantidad'];
        echo number_format(sprintf("%01.2f", $calculo), 2, ',', '.'); ?></td>
        </tr>
<?php
	}
?>
		</tbody>
		<tfoot>
		  <tr>
			<th colspan="2" align="right">Totales:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_cantidad), 2, ',', '.');
			 ?></div></th>
			 <th  align="right">&nbsp;</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			 ?></div></th>
			</tr> 
		  <tr>
			<th colspan="4" align="right"><div align="right">Total Iva:</div></th>
			<th  ><div align="right"><?php echo $datos['montoiva'];
			 ?></div></th>
		  </tr> 
		  <tr>
			<th colspan="4" align="right"><div align="right">Total:</div></th>
			<th  ><div align="right"><?php echo $datos['montobruto'];
			 ?></div></th>
		  </tr> 		  
		</tfoot>
</table>
