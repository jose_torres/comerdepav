  <table class="table table-condensed table-bordered" >
	<tr>
	  <th>Nro</th>
	  <th>Mov.</th>
	  <th>Banco</th>
	  <th>Cuenta</th>
	  <th>Monto</th>
	  <th>Accion</th>
	</tr>
	<tbody>
<?php
$A_DEP_SUC = 0;	
$x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
$DEP['Efectivo']=$DEP['Debito']=$DEP['Cheque']=$DEP['Transfer']=0; $GAS=0;
foreach ($depositos['Efectivo'] as $row){
if($row['Cuentasbancaria']['tipo']==$TIPO){
	$total_monto=$total_monto+$row['Conmovbancario']['monto'];
	$x=$x+1;$class = "danger"; $checked = false;
	if($row['Conmovbancario']['conciliado']!='' && $row['Conmovbancario']['conciliado']!='N'){
		$class = "success"; $checked = true;
	}
  ?>
 <tr id='td_id' class="<?php echo $class;?>">
	<td align="center"><?php echo $row['Conmovbancario']['nrodocumento']; ?></td>
	<td align="center"><a href="#" data-toggle="tooltip" title="<?php echo $row['Documentotipo']['descripcion']; ?>"><?php echo $row['Documentotipo']['descorta']; ?></a></td>
	<td align="center"><a href="#" data-toggle="tooltip" title="<?php echo 'BANCO '.$row['Banco']['nombre']; ?>"><?php echo $row['Banco']['abreviatura']; ?></a></td>
	<td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
	<td align="right"><?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?></td>
	<td align="center">
		<a href="#" data-toggle="modal" data-target="#EditarDepositos" data-id="<?php echo $row['Conmovbancario']['id']?>" data-opcion="Deposito_Juridico"  data-codsucursal="<?php echo $row['Conmovbancario']['codsucursal']?>" data-nrodocumento="<?php echo $row['Conmovbancario']['nrodocumento']; ?>" data-tipo="<?php echo $row['Documentotipo']['descorta']; ?>" data-banco="<?php echo $row['Banco']['nombre']; ?>" data-cuentabancaria="<?php echo $row['Cuentasbancaria']['numerocuenta']; ?>" data-monto="<?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?>" title="<?php echo $row['Documentotipo']['descripcion']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>
		<?php echo $this->Form->checkbox($SELEC.'.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'checked'=>$checked));
		echo $this->Form->input($SELEC.'.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row['Conmovbancario']['id']));
		echo $this->Form->input($SELEC.'.'.$x.'.tipo', array('type' =>'hidden','label' =>'','value'=>$row['Cuentasbancaria']['tipo']));
	?>
	</td>
 <?php
	}
 }
 ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan='4' align="right"><strong>Total:</strong></td>
			<td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			$DEP['Efectivo']=$total_monto;
			?></strong></td>
			<td>&nbsp;</td>
		</tr>
	</tfoot>
	  </table>
		
