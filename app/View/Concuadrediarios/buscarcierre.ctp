<?php
	
?>
<div class="panel panel-primary">
     <div class="panel-body">
<!-- Resumen de Venta-->
<div class="box">
	
<div class="col-md-6" >
		<!-- /.box-header -->
	<div class="box-body no-padding">
 <table class="table table-condensed table-bordered"  width="50%">
	<caption><strong>RESUMEN DE CUADRE DE LA FECHA <?php echo date('d-m-Y',strtotime($datos['fecha'])); ?> DE LA <?php echo strtoupper($sucursal['Sucursal']['descripcion']); ?></strong></caption> 
	<tr>
	  <th width="5%">#</th>
	  <th width="20%">Totales</th>
	  <th width="5%" align="right">Registros</th>
	  <th width="20%" align="right">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Depositos en Efectivos</td>
	  <td align="right"><?php echo $totales_suc['Efectivo']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Efectivo']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Depositos en Debito</td>
	  <td align="right"><?php echo $totales_suc['Debito']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Debito']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>3.</td>
	  <td>Depositos en Cheque</td>
	  <td align="right"><?php echo $totales_suc['Cheque']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Cheque']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>4.</td>
	  <td>Depositos en Transferencia</td>
	  <td align="right"><?php echo $totales_suc['Transfer']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Transfer']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>5.</td>
	  <td>Gastos Registrados</td>
	  <td align="right"><?php echo $totales_suc['Gasto']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Gasto']['monto']), 2, ',', '.');?></td>
	</tr>
		<tr>
	  <td>&nbsp;</td>
	  <td><strong>Total Depositos/Gastos</strong></td>
	  <td align="right"><?php
	  $total_reg_suc =	$totales_suc['Gasto']['reg']+$totales_suc['Transfer']['reg']+$totales_suc['Cheque']['reg']+$totales_suc['Debito']['reg']+$totales_suc['Efectivo']['reg'];
	  echo $total_reg_suc;?></td>
	  <td align="right"><strong><?php
	  $total_monto_suc =	$totales_suc['Gasto']['monto']+$totales_suc['Transfer']['monto']+$totales_suc['Cheque']['monto']+$totales_suc['Debito']['monto']+$totales_suc['Efectivo']['monto'];	
	  echo number_format(sprintf("%01.2f", $total_monto_suc), 2, ',', '.');?></strong></td>
	</tr>
	<tr>
	  <td>6.</td>
	  <td>Ventas Registrados</td>
	  <td align="right"><?php echo $totales_suc['Ventas']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Ventas']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>7.</td>
	  <td>Devoluciones Registrados</td>
	  <td align="right"><?php echo $totales_suc['Devolucion']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Devolucion']['monto']), 2, ',', '.');?></td>
	</tr>
    <tr>
	  <td>&nbsp;</td>
	  <td><strong>Total Ventas Neta</strong></td>
	  <td align="right"><?php
	  $total_reg_suc =	$totales_suc['Ventas']['reg']-$totales_suc['Devolucion']['reg'];
	  echo $total_reg_suc;?></td>
	  <td align="right"><strong><?php
	  $total_monto_suc =	$totales_suc['Ventas']['monto']-$totales_suc['Devolucion']['monto'];	
	  echo number_format(sprintf("%01.2f", $total_monto_suc), 2, ',', '.');?></strong></td>
	</tr>
	<tr>
	  <td>8.</td>
	  <td>Cargo Registrados</td>
	  <td align="right"><?php echo $totales_suc['Cargo']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Cargo']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>9.</td>
	  <td>Descargo Registrados</td>
	  <td align="right"><?php echo $totales_suc['Descargo']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_suc['Descargo']['monto']), 2, ',', '.');?></td>
	</tr>
 </table>
	</div> 
</div> 
 <!-- Resumen de Venta-->
<div class="col-md-6">
	
	<!-- /.box-header -->
	<div class="box-body no-padding">
 <table class="table table-condensed table-bordered"  width="50%">
	<caption><strong>RESUMEN DE CUADRE CONSOLIDADO DE LA FECHA <?php echo date('d-m-Y',strtotime($datos['fecha'])); ?></strong></caption> 
	<tr>
	  <th width="5%">#</th>
	  <th width="20%">Totales</th>
	  <th width="5%" align="right">Registros</th>
	  <th width="20%" align="right">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Depositos en Efectivos</td>
	  <td align="right"><?php echo $totales_pri['Efectivo']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Efectivo']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Depositos en Debito</td>
	  <td align="right"><?php echo $totales_pri['Debito']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Debito']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>3.</td>
	  <td>Depositos en Cheque</td>
	  <td align="right"><?php echo $totales_pri['Cheque']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Cheque']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>4.</td>
	  <td>Depositos en Transferencia</td>
	  <td align="right"><?php echo $totales_pri['Transfer']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Transfer']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>5.</td>
	  <td>Gastos Registrados</td>
	  <td align="right"><?php echo $totales_pri['Gasto']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Gasto']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>&nbsp;</td>
	  <td><strong>Total Depositos/Gastos</strong></td>
	  <td align="right"><?php
	  $total_reg_pri =	$totales_pri['Gasto']['reg']+$totales_pri['Transfer']['reg']+$totales_pri['Cheque']['reg']+$totales_pri['Debito']['reg']+$totales_pri['Efectivo']['reg'];
	  echo $total_reg_pri;?></td>
	  <td align="right"><strong><?php
	  $total_monto_pri =	$totales_pri['Gasto']['monto']+$totales_pri['Transfer']['monto']+$totales_pri['Cheque']['monto']+$totales_pri['Debito']['monto']+$totales_pri['Efectivo']['monto'];	
	  echo number_format(sprintf("%01.2f", $total_monto_pri), 2, ',', '.');?></strong></td>
	</tr>	
	<tr>
	  <td>6.</td>
	  <td>Ventas Registrados</td>
	  <td align="right"><?php echo $totales_pri['Ventas']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Ventas']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>7.</td>
	  <td>Devoluciones Registrados</td>
	  <td align="right"><?php echo $totales_pri['Devolucion']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Devolucion']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>&nbsp;</td>
	  <td><strong>Total Ventas Neta</strong></td>
	  <td align="right"><?php
	  $total_reg_suc =	$totales_pri['Ventas']['reg']-$totales_pri['Devolucion']['reg'];
	  echo $total_reg_suc;?></td>
	  <td align="right"><strong><?php
	  $total_monto_suc =	$totales_pri['Ventas']['monto']-$totales_pri['Devolucion']['monto'];	
	  echo number_format(sprintf("%01.2f", $total_monto_suc), 2, ',', '.');?></strong></td>
	</tr>
		<tr>
	  <td>8.</td>
	  <td>Cargo Registrados</td>
	  <td align="right"><?php echo $totales_pri['Cargo']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Cargo']['monto']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>9.</td>
	  <td>Descargo Registrados</td>
	  <td align="right"><?php echo $totales_pri['Descargo']['reg'];?></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales_pri['Descargo']['monto']), 2, ',', '.');?></td>
	</tr>
 </table>
	</div> 
</div> 
</div> 
 <?php
	if ($mensaje==''){
 ?>

 <button type="button" class="btn btn-success" onclick="actualizar('buscar','transferirprincipal/',''+document.frm.ReporteSucursal.value,'fecha='+document.frm.ReporteHasta.value)">Transferir&nbsp;<span class="glyphicon glyphicon-transfer"></span></button>
 
 <?php
	}else{
		echo ' <div class="col-md-12">';
		echo $mensaje;
		echo '</div>';
	}
 ?>
 </div>        
 </div>        
