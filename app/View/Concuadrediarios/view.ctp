<?php
	//echo $this->element('menuinterno',$datos_menu);
	//echo $javascript->link('prototype'); 
	//print_r($depositos);
?>

<script language="javascript" type="text/javascript">

</script>
<fieldset id="personal" >
<legend> Resumen de Cuadre Diario de Fecha <?php echo date('d-m-Y',strtotime($cuadre['Concuadrediario']['fecha'])) ;?> de la <?php echo $cuadre['Sucursal']['descripcion']; ?></legend>
<div id="buscar">
		
	<!-- -->
	<div class="box">
			<div class="box-header">
			 <div class="row">
			  <div class="col-sm-3"><h3 class="box-title">Venta Realizadas:<br>Bs. <span id="ventas_realizadas"><strong><?php echo number_format(sprintf("%01.2f", $ventadeldia[0][0]['total']), 2, ',', '.'); ?></strong></span></h3></div>
			  <div class="col-sm-3"><h3 class="box-title">Dep&oacute;sitos:<br>Bs.<strong> <span id="depositos_realizadas"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.'); ?></span></strong></h3></div>
			  <div class="col-sm-3"><h3 class="box-title">D&eacute;bitos:<br>Bs.<strong> <span id="gastos_realizadas"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.'); ?></span></strong></h3></div>
			  <div class="col-sm-3"><h3 class="box-title">Diferencia:<br>Bs.<strong> <span id="diferencia"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.'); ?></span></strong></h3></div>
			</div>	 	
			  
			</div>
			<!-- /.box-header -->
			
	</div>			
<!-- -->
	<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Dep&oacute;sito en Efectivo</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
		  <table class="table table-condensed table-bordered">
					<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    $DEP['Efectivo']=$DEP['Debito']=$DEP['Cheque']=$DEP['Transfer']=0; $GAS=0;
    foreach ($depositos['Efectivo'] as $row){
		$total_monto=$total_monto+$row['Conmovbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Conmovbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Conmovbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="right">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan='5' align="right"><strong>Total:</strong></td>
				<td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
				$DEP['Efectivo']=$total_monto;
				?></strong></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
		  </table>
		</div>
		<!-- /.box-body -->
	  </div>
<!-- -->
	<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Dep&oacute;sito en Punto de Ventas</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
		  <table class="table table-condensed table-bordered">
					<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Debito'] as $row){
		$total_monto=$total_monto+$row['Conmovbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Conmovbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Conmovbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="right">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan='5' align="right"><strong>Total:</strong></td>
				<td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
				$DEP['Debito']=$total_monto;
				 ?></strong></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
		  </table>
		</div>
		<!-- /.box-body -->
	  </div>
<!-- -->
	<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Dep&oacute;sito en Cheques</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
		  <table class="table table-condensed table-bordered">
					<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Cheque'] as $row){
		$total_monto=$total_monto+$row['Conmovbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Conmovbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Conmovbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="right">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan='5' align="right"><strong>Total:</strong></td>
				<td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
				$DEP['Cheque']=$total_monto;
				?></strong></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
		  </table>
		</div>
		<!-- /.box-body -->
	  </div>
<!-- -->
	<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Dep&oacute;sito en Transferencia</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
		  <table class="table table-condensed table-bordered">
					<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Transfer'] as $row){
		$total_monto=$total_monto+$row['Conmovbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Conmovbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Conmovbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Conmovbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="right">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan='5' align="right"><strong>Total:</strong></td>
				<td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
				$DEP['Transfer']=$total_monto;
				 ?></strong></td>
				<td>&nbsp;</td>
			</tr>
		</tfoot>
		  </table>
		</div>
		<!-- /.box-body -->
	  </div>
<!-- -->
<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Gastos Realizados</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
		  <table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nombre</th>
		  <th>Motivo</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($gastos['Gastos'] as $row){
		$total_monto=$total_monto+$row['Congasto']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Congasto']['id']; ?></td>
        <td align="left"><?php echo $row['Congasto']['nombre']; ?></td>
        <td align="left"><?php echo $row['Congasto']['motivo']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Congasto']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="3" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			$GAS=$total_monto;
			 ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
		</div>
		<!-- /.box-body -->
	  </div>
<!-- -->	  
</div>

<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Retenciones Realizadas</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>N&uacute;mero</th>
		  <th>Factura</th>		  
		  <th>Cliente</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;$RET=0;
    foreach ($retenciones['Registros'] as $row){
		$total_monto=$total_monto+$row[0]['montoretenido'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row[0]['codretencion']; ?></td>
        <td align="center"><?php echo $row[0]['numero']; ?></td>
        <td align="left"><?php echo $row[0]['documento']; ?></td>
        <td align="left"><?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'); ?></td>
        <td align="center">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="4" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			$RET=$total_monto;
			?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
		</div>
		<!-- /.box-body -->
	  </div>
<!-- -->
<div class="box">	
	<!-- Debitos -->
	<div class="row">
		<div class="col-sm-12"><h3 class="box-title">Devoluciones en Ventas:</h3></div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="box-header">
		  <h3 class="box-title">Devoluciones</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding" id="Devoluciones">	
		  <table class="table table-bordered table-hover">
		<tr>
		  <th>N&uacute;mero</th>
		  <th>Cliente</th>
		  <th>Motivo</th>
		  <th>Factura</th>		  
		  <th>Total</th>
		  <th>Monto Bruto</th>
		  <th>Iva</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php
	$A_DEV_SUC = 0;
    $x=0;$total_registros=0; $total_cantidad=0;$total_monto_neto=$total_monto_iva=$total_monto=0;
    foreach ($devoluciones as $row){
		$total_monto=$total_monto+$row[0]['baseimp1']+$row[0]['baseimp2']+$row[0]['baseimp3'] + $row[0]['ivaimp1']+$row[0]['ivaimp2']+$row[0]['ivaimp3'];
		$total_monto_neto=$total_monto_neto + $row[0]['baseimp1']+$row[0]['baseimp2']+$row[0]['baseimp3'];
		$total_monto_iva=$total_monto_iva + $row[0]['ivaimp1']+$row[0]['ivaimp2']+$row[0]['ivaimp3'];
		$x=$x+1;$class = "danger"; $checked = false;
		if($row[0]['conciliado']!='' && $row[0]['conciliado']!='NO'){
			$class = "success"; $checked = true;
		}
     ?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo $row[0]['coddevolucion']; ?></td>
        <td align="left"><?php echo $row['cliente']['descripcion']; ?></td>
        <td align="left"><?php echo $row[0]['observacion']; ?></td>        
        <td align="left"><?php echo $row[0]['numerofactura']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp1']+$row[0]['baseimp2']+$row[0]['baseimp3'] + $row[0]['ivaimp1']+$row[0]['ivaimp2']+$row[0]['ivaimp3']), 2, ',', '.'); ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp1']+$row[0]['baseimp2']+$row[0]['baseimp3'] ), 2, ',', '.'); ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['ivaimp1']+$row[0]['ivaimp2']+$row[0]['ivaimp3']), 2, ',', '.'); ?></td>
        <td align="center">
           <?php
           //echo $this->Form->checkbox('Selecciondev.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'checked'=>$checked));
			echo $this->Form->input('Selecciondev.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row[0]['id']));
			?>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="4" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			$DEV=$total_monto;
			 ?></div></th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto_neto), 2, ',', '.');
			 ?></div></th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto_iva), 2, ',', '.');
			 ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
		</div>
		<!-- /.box-body -->
	  
		</div>
</div>
	  
<!-- -->		  
</div>
<script>
document.getElementById('depositos_realizadas').innerHTML='<?php echo number_format(sprintf("%01.2f", $DEP['Efectivo']+$DEP['Debito']+$DEP['Cheque']+$DEP['Transfer']), 2, ',', '.'); ?>';
document.getElementById('gastos_realizadas').innerHTML='<?php echo number_format(sprintf("%01.2f", $GAS + $RET), 2, ',', '.'); ?>';
document.getElementById('diferencia').innerHTML='<?php echo number_format(sprintf("%01.2f", $ventadeldia[0][0]['total'] - ($DEP['Efectivo']+$DEP['Debito']+$DEP['Cheque']+$DEP['Transfer']+$GAS+$RET)), 2, ',', '.'); ?>';
</script>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar a Listado", "title"=>"Regresar a Listado")) ,"/concuadrediarios/index", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
