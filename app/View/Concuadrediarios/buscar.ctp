<?php
	echo $this->Html->css('datatables/dataTables.bootstrap.css');
?>
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Cuadre Transferidos Encontrados</h3>
	</div>
<table border="1" cellpadding="1" cellspacing="0" width="95%" align="center" class="table table-condensed table-bordered table-hover" id="ListadoCuadre">
<thead>
    <tr>
        <th ><?php echo $this->Paginator->sort('id','Codigo');?></th>
        <th><?php echo $this->Paginator->sort('fecha','Fecha');?></th>
        <th>Sucursal</th>
        <th>Ventas</th>
        <th>Dep&oacute;sitos</th>
        <th>Gastos</th>
        <th>Retenciones</th>
        <th>Mov. Pend.</th>
        <th>Modificado</th>
        <th >Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $registro = $row['Concuadrediario']; ?>    
    <?php $x++; ?>
	<?php if($x % 2 == 0){ ?>
	<tr class="roweven">
	<?php } else { ?>
	<tr>
	<?php } ?>
        <td>
            <?php echo $x; ?>    
        </td>
        <td>
            <?php echo $this->Html->link(date('d-m-Y',strtotime($registro['fecha'])), '/concuadrediarios/view/'.$registro['id'].'-'.$row['Sucursal']['codsucursal']) ?>    
        </td>
        <td>
            <?php echo $row['Sucursal']['descripcion'];  ?>    
        </td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Concuadrediario']['monto_venta']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Concuadrediario']['monto_deposito']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Concuadrediario']['monto_debito']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Concuadrediario']['monto_retencion']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Concuadrediario']['nro_movpendientes']), 0, ',', '.'); ?></td>
      <!--  <td>
            <?php if ($registro['estatus']=='1'){ echo 'Abierto'; }else{echo 'Cerrado';} ?>    
        </td> -->
        
        <td>
            <?php 
                if (!empty($registro['modified'])) echo date('d-m-Y',strtotime($registro['modified']));
                else if (!empty($registro['updated'])) echo date('d-m-Y',strtotime($registro['updated']));
            ?>
        </td>
        <td align="center">
			<?php
			echo $this->Html->link(__('<span class="glyphicon glyphicon-ok"></span>&nbsp;'), array('action' => 'conciliar', $registro['id'].'-'.$row['Sucursal']['codsucursal']),array("role"=>"button", "class"=>"btn btn-success",'escape'=>false)); 
			echo '&nbsp;&nbsp;&nbsp;';
			echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;'), array('action' => 'viewpdf', $registro['id'].'-'.$row['Sucursal']['codsucursal']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
			echo '&nbsp;&nbsp;&nbsp;';
			 ?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
<tfoot>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tfoot>
</table>
</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
	$('[data-toggle="tooltip"]').tooltip();

    $('#ListadoCuadre').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
   

  });
 
</script>
