<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='RESUMEN DE INGRESOS';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=8;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'Listado de Precio');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',8);

//$this->tcpdf->core->setEqualColumns(2);
// Colors, line width and bold font
$this->tcpdf->core->SetFillColor(255, 255, 255);
$this->tcpdf->core->SetTextColor(0);
//$this->tcpdf->core->SetDrawColor(128, 0, 0);
$this->tcpdf->core->SetLineWidth(0.3);
$this->tcpdf->core->SetFont('', 'B');
        
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0; $DEP='';       
// Header
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('', 'B');
    $this->tcpdf->core->Cell(190, 4, 'RESUMEN DE VENTAS', 'BT', 0, 'C', 1);
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Ln();
    $w =  array(120,70);
    $fill = 0;
    $this->tcpdf->core->SetFont('', '');
    $this->tcpdf->core->Cell($w[0], 3, 'TOTAL VENTAS A CONTADO SIN I.V.A. (Bs.)', 'B', 0, 'L', $fill);	
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $dataResumen['Venta'][0][0]['neto']), 2, ',', '.'), 'B', 0, 'R', $fill);
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Ln();    
    $this->tcpdf->core->Cell($w[0], 3, 'TOTAL VENTAS SIN I.V.A. (Bs.)', '', 0, 'L', $fill);	
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $dataResumen['Venta'][0][0]['neto']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Ln();    
    $this->tcpdf->core->Cell($w[0], 3, 'TOTAL I.V.A. (Bs.)', 'B', 0, 'L', $fill);	
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $dataResumen['Venta'][0][0]['iva']), 2, ',', '.'), 'B', 0, 'R', $fill);
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Cell($w[0], 3, 'TOTAL OPERACION (Bs.)', '', 0, 'L', $fill);	
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $dataResumen['Venta'][0][0]['neto'] + $dataResumen['Venta'][0][0]['iva']), 2, ',', '.'), '', 0, 'R', $fill);
    $TOTAL['Venta'] = $dataResumen['Venta'][0][0]['neto'] + $dataResumen['Venta'][0][0]['iva'];
//--------------RESUMEN DE CAJA---------------------------------------------------    
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('', 'B');
    $this->tcpdf->core->Cell(190, 4, 'RESUMEN DE CAJA', 'BT', 0, 'C', 1);
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Ln();

    $this->tcpdf->core->SetFont('', ''); $TOTAL['caja']=0;
    foreach($dataResumen['Pago'] as $row) {
        
        $this->tcpdf->core->Cell($w[0], 3, ''.$row[0]['tipopago'], '', 0, 'L', $fill);	
        $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $row[0]['monto']), 2, ',', '.'), '', 0, 'R', $fill);
        $this->tcpdf->core->Ln();
        $TOTAL['caja'] = $TOTAL['caja'] + $row[0]['monto'];
    }    
    $this->tcpdf->core->Cell($w[0], 3, 'TOTAL DE INGRESOS (Bs.)', 'T', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $TOTAL['caja']), 2, ',', '.'), 'T', 0, 'R', $fill);
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Cell($w[0], 3, 'NUMEROS DE VENTAS: ', '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $dataResumen['Nro'][0][0]['nro']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Cell($w[0], 3, 'NUMEROS DE DEVOLUCIONES: ', 'B', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $dataResumenDev['Nro'][0][0]['nro']), 2, ',', '.'), 'B', 0, 'R', $fill);
    $this->tcpdf->core->Ln();
    
//--------------RESUMEN DE DEPOSITO-------------------------------------------------   
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('', 'B');
    $this->tcpdf->core->Cell(190, 4, 'RESUMEN DE DEPOSITO', 'BT', 0, 'C', 1);
    $this->tcpdf->core->Ln();
     $this->tcpdf->core->SetFillColor(215,215,215);
    $this->tcpdf->core->Cell(190, 4, 'EN LA SUCURSAL', 'BT', 0, 'L', 1);
    $this->tcpdf->core->Ln();    
     $this->tcpdf->core->SetFillColor(255,255,255);
    $this->tcpdf->core->SetFont('', 'B');
    
    $header=array('Nro','Mov.','Banco','Cuenta','Monto');
    $w = array(30,40,40,40,40);
    $num_headers = count($header);
    for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
    }
    $this->tcpdf->core->Ln();
    
    $TOTAL['jur'] = 0;
    foreach ($depositos['Efectivo'] as $row){
        if($row['Cuentasbancaria']['tipo']=='JURIDICA'){
            $this->tcpdf->core->SetTextColor(0);
            $this->tcpdf->core->SetFont('');        
            $fill = 0;
            
            $this->tcpdf->core->Cell($w[0], 3, $row['Conmovbancario']['nrodocumento'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[1], 3, $row['Documentotipo']['descripcion'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[2], 3, $row['Banco']['nombre'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[3], 3, $row['Cuentasbancaria']['numerocuenta'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[4], 3, number_format(sprintf("%01.2f",$row['Conmovbancario']['monto']), 2, ',', '.'), '', 0, 'R', $fill);
            $this->tcpdf->core->Ln();
            $TOTAL['jur'] = $TOTAL['jur'] + $row['Conmovbancario']['monto'];
        }    
        
    }
    $this->tcpdf->core->SetFont('', 'B');
    $w = array(150,40);
    $this->tcpdf->core->Cell($w[0], 3, 'Subtotal:', 'T', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[1], 3, number_format(sprintf("%01.2f",$TOTAL['jur']), 2, ',', '.'), 'T', 0, 'R', $fill);
    $this->tcpdf->core->Ln();    
    
    $this->tcpdf->core->SetFont('', 'B');
     $this->tcpdf->core->SetFillColor(215,215,215);
    $this->tcpdf->core->Cell(190, 4, 'EN C.P.', 'BT', 0, 'L', 1);
    $this->tcpdf->core->Ln();    
     $this->tcpdf->core->SetFillColor(255,255,255);
    $this->tcpdf->core->SetFont('', 'B');
    
    $header=array('Nro','Mov.','Banco','Cuenta','Monto');
    $w = array(30,40,40,40,40);
    $num_headers = count($header);
    for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
    }
    $this->tcpdf->core->Ln();
    
    $TOTAL['per'] = 0;
    foreach ($depositos['Efectivo'] as $row){
        if($row['Cuentasbancaria']['tipo']=='PERSONAL'){
            $this->tcpdf->core->SetTextColor(0);
            $this->tcpdf->core->SetFont('');        
            $fill = 0;
            
            $this->tcpdf->core->Cell($w[0], 3, $row['Conmovbancario']['nrodocumento'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[1], 3, $row['Documentotipo']['descripcion'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[2], 3, $row['Banco']['nombre'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[3], 3, $row['Cuentasbancaria']['numerocuenta'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[4], 3, number_format(sprintf("%01.2f",$row['Conmovbancario']['monto']), 2, ',', '.'), '', 0, 'R', $fill);
            $this->tcpdf->core->Ln();
            $TOTAL['per'] = $TOTAL['per'] + $row['Conmovbancario']['monto'];
        }    
        
    }
    
    $this->tcpdf->core->SetFont('', 'B');
    $w = array(150,40);
    $this->tcpdf->core->Cell($w[0], 3, 'Subtotal:', 'T', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[1], 3, number_format(sprintf("%01.2f",$TOTAL['per']), 2, ',', '.'), 'T', 0, 'R', $fill);
    $this->tcpdf->core->Ln(); 

//--------------RESUMEN DE DEBITO-------------------------------------------------   
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('', 'B');
    $this->tcpdf->core->Cell(190, 4, 'RESUMEN DE DEBITOS', 'BT', 0, 'C', 1);
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->SetFillColor(215,215,215);
    $this->tcpdf->core->Cell(190, 4, 'GASTOS', 'BT', 0, 'L', 1);
    $this->tcpdf->core->Ln();    
    $this->tcpdf->core->SetFillColor(255,255,255);
    $this->tcpdf->core->SetFont('', 'B');
    
    $header=array('Nro','Nombre','Motivo','Monto');
    $w = array(30,40,80,40);
    $num_headers = count($header);
    for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
    }
    $this->tcpdf->core->Ln();
    
    $TOTAL['gas'] = 0;
    foreach ($gastos['Gastos'] as $row){
        
        $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('');        
        $fill = 0;

        $this->tcpdf->core->Cell($w[0], 3, $row['Congasto']['nro'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[1], 3, $row['Congasto']['nombre'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[2], 3, $row['Congasto']['motivo'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[3], 3, number_format(sprintf("%01.2f",$row['Congasto']['monto']), 2, ',', '.'), '', 0, 'R', $fill);
        $this->tcpdf->core->Ln();
        $TOTAL['gas'] = $TOTAL['gas'] + $row['Congasto']['monto'];
                
        
    }
    $this->tcpdf->core->SetFont('', 'B');
    $w = array(150,40);
    $this->tcpdf->core->Cell($w[0], 3, 'Subtotal:', 'T', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[1], 3, number_format(sprintf("%01.2f",$TOTAL['gas']), 2, ',', '.'), 'T', 0, 'R', $fill);
    $this->tcpdf->core->Ln();    
    
    $this->tcpdf->core->SetFont('', 'B');
//     $this->tcpdf->core->SetFillColor(215,215,215);
    $this->tcpdf->core->Cell(190, 4, 'RETENCIONES', 'BT', 0, 'L', 1);
    $this->tcpdf->core->Ln();    
//     $this->tcpdf->core->SetFillColor(255,255,255);
    $this->tcpdf->core->SetFont('', 'B');
    
    $header=array('Nro','Factura','Cliente','Monto');
    $w = array(30,40,80,40);
    $num_headers = count($header);
    for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
    }
    $this->tcpdf->core->Ln();
    
    $TOTAL['ret'] = 0;
    foreach ($retenciones['Registros'] as $row){
        
        $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('');        
        $fill = 0;

        $this->tcpdf->core->Cell($w[0], 3, $row[0]['numero'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[1], 3, $row[0]['documento'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[2], 3, $row[0]['rif']." ".$row[0]['descripcion'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[3], 3, number_format(sprintf("%01.2f",$row[0]['montoretenido']), 2, ',', '.'), '', 0, 'R', $fill);
        $this->tcpdf->core->Ln();
        $TOTAL['ret'] = $TOTAL['ret'] + $row[0]['montoretenido'];
                
        
    }
    $this->tcpdf->core->SetFont('', 'B');
    $w = array(150,40);
    $this->tcpdf->core->Cell($w[0], 3, 'Subtotal:', 'T', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[1], 3, number_format(sprintf("%01.2f",$TOTAL['ret']), 2, ',', '.'), 'T', 0, 'R', $fill);
    $this->tcpdf->core->Ln();  
    
    $this->tcpdf->core->SetFont('', 'B');
    $w = array(17,30,20,29,18,30,18,30);
    $this->tcpdf->core->Cell($w[0], 3, 'Ventas Bs.:', 'BT', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[1], 3, number_format(sprintf("%01.2f",$TOTAL['Venta']), 2, ',', '.'), 'BT', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[2], 3, 'Deposito Bs.:', 'BT', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[3], 3, number_format(sprintf("%01.2f",$TOTAL['jur']+$TOTAL['per']), 2, ',', '.'), 'BT', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[4], 3, 'Debitos Bs.:', 'BT', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[5], 3, number_format(sprintf("%01.2f",$TOTAL['gas']+$TOTAL['ret']), 2, ',', '.'), 'BT', 0, 'R', $fill);
    
    $mensaje = 'SOBRANTE';//250-210-001
    $this->tcpdf->core->SetFillColor(250,210,001);
    $this->tcpdf->core->SetTextColor(255,255,255);
    $TOTAL['dif'] = $TOTAL['Venta'] - ( ($TOTAL['jur']+$TOTAL['per']) - ($TOTAL['gas']+$TOTAL['ret']) );
    if( round($TOTAL['dif'],2) > 0){
        $mensaje = 'FALTANTE';//204-006-005
        $this->tcpdf->core->SetFillColor(204,006,005);
        $this->tcpdf->core->SetTextColor(255,255,255);
    }elseif( round($TOTAL['dif'],2) == 0){ 
        $mensaje = 'CUADRADO'; //048-132-070
        $this->tcpdf->core->SetFillColor('048','132','070');
        $this->tcpdf->core->SetTextColor(255,255,255);
    }
    $this->tcpdf->core->Cell($w[6], 3, $mensaje, 'BT', 0, 'C', 1);
    $this->tcpdf->core->SetFillColor(255,255,255);
    $this->tcpdf->core->SetTextColor(0,0,0);
    $this->tcpdf->core->Cell($w[7], 3, number_format(sprintf("%01.2f",$TOTAL['dif']), 2, ',', '.'), 'BT', 0, 'R', $fill);

    $this->tcpdf->core->Ln(); 
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('con_cuadre_diario_'.date('d-m-Y',strtotime($cuadre['Concuadrediario']['fecha'])).'.pdf', 'D');
?>
