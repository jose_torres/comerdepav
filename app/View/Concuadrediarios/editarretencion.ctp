<table class="table table-bordered table-hover">
		<tr>
		  <th>N&uacute;mero</th>
		  <th>Factura</th>		  
		  <th>Cliente</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php
	$A_RET_SUC = 0;
    $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;$RET=0;
    foreach ($retenciones['Registros'] as $row){
		$total_monto=$total_monto+$row[0]['montoretenido'];
		$x=$x+1;$class = "danger"; $checked = false;
		if($row[0]['estatus']!='' && $row[0]['estatus']!='A'){
			$class = "success"; $checked = true;
		}
		?>
     <tr id='td_id' class="<?php echo $class;?>">
        <td align="center"><?php echo $row[0]['numero']; ?></td>
        <td align="left"><?php echo $row[0]['documento']; ?></td>
        <td align="left"><?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'); ?></td>
        <td align="center">
			<a href="#" data-toggle="modal" data-target="#EditarRetencion" data-id="<?php echo $row[0]['id']?>" data-opcion="Retencion" data-codsucursal="<?php echo $row[0]['codsucursal']?>" data-documento="<?php echo $row[0]['documento']; ?>" data-numero="<?php echo $row[0]['numero']; ?>" data-cliente="<?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?>" data-montoretenido="<?php echo number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'); ?>"  data-montobruto="<?php echo number_format(sprintf("%01.2f", $row[0]['montobruto']), 2, ',', '.'); ?>" data-montoiva="<?php echo number_format(sprintf("%01.2f", $row[0]['montoiva']), 2, ',', '.'); ?>" title="<?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>
           <?php echo $this->Form->checkbox('Seleccionr.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'class'=>"form-control text-right",'checked'=>$checked));
			echo $this->Form->input('Seleccionr.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row[0]['id_cab']));
		?>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="3" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.');
			$RET=$total_monto;
			?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
