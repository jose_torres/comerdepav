<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=8;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'Listado de Cargos y Descargos');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");

$this->tcpdf->core->SetFont('helvetica','',8);

//$this->tcpdf->core->setEqualColumns(2);
// Colors, line width and bold font
$this->tcpdf->core->SetFillColor(255, 255, 255);
$this->tcpdf->core->SetTextColor(0);
//$this->tcpdf->core->SetDrawColor(128, 0, 0);
$this->tcpdf->core->SetLineWidth(0.3);
$this->tcpdf->core->SetFont('', 'B');
        
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0; $DEP='';       
// Header
    
//--------------LISTADO DE CARGO-------------------------------------------------   
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('', 'B');
    $this->tcpdf->core->Cell(190, 4, 'LISTADO DE CARGO', 'BT', 0, 'C', 1);
    $this->tcpdf->core->Ln();   
    $this->tcpdf->core->SetFont('', 'B');
    
    $header=array('Nro','Motivo','Autorizado','Responsable','Hora','Monto');
    $w = array(10,70,20,40,10,40);
    $num_headers = count($header);
    for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
    }
    $this->tcpdf->core->Ln();
    
    $fill = 0;
    $TOTAL['cargos'] = 0;
    foreach ($concargos as $row){
        
        $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('');        
        $fill = 0;

        $this->tcpdf->core->Cell($w[0], 3, $row[0]['codcargo'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[1], 3, $row[0]['descripcion'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[2], 3, $row[0]['autorizado'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[3], 3, $row[0]['responsable'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[4], 3, date('h:i:s A',strtotime($row[0]['hora'])), '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[5], 3, number_format(sprintf("%01.2f",$row[0]['total']), 2, ',', '.'), '', 0, 'R', $fill);
        $this->tcpdf->core->Ln();
        $TOTAL['cargos'] = $TOTAL['cargos'] + $row[0]['total'];            
        
    }
    $this->tcpdf->core->SetFont('', 'B');
    $w = array(150,40);
    $this->tcpdf->core->Cell($w[0], 3, 'Total:', 'T', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[1], 3, number_format(sprintf("%01.2f",$TOTAL['cargos']), 2, ',', '.'), 'T', 0, 'R', $fill);
    $this->tcpdf->core->Ln();    
    
//--------------LISTADO DE DESCARGO-------------------------------------------------   
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->Ln();
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('', 'B');
    $this->tcpdf->core->Cell(190, 4, 'LISTADO DE DESCARGO', 'BT', 0, 'C', 1);
    $this->tcpdf->core->Ln();    
    $this->tcpdf->core->SetFont('', 'B');
    
    $header=array('Nro','Motivo','Autorizado','Responsable','Hora','Monto');
    $w = array(10,70,20,40,10,40);
    $num_headers = count($header);
    for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
    }
    $this->tcpdf->core->Ln();
    
    $TOTAL['descargos'] = 0;
    foreach ($condescargos as $row){
        
        $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('');        
        $fill = 0;

        $this->tcpdf->core->Cell($w[0], 3, $row[0]['coddescargo'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[1], 3, $row[0]['descripcion'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[2], 3, $row[0]['autorizado'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[3], 3, $row[0]['responsable'], '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[4], 3, date('h:i:s A',strtotime($row[0]['hora'])), '', 0, 'L', $fill);
        $this->tcpdf->core->Cell($w[5], 3, number_format(sprintf("%01.2f",$row[0]['total']), 2, ',', '.'), '', 0, 'R', $fill);
        $this->tcpdf->core->Ln();
        $TOTAL['descargos'] = $TOTAL['descargos'] + $row[0]['total'];            
        
    }
    $this->tcpdf->core->SetFont('', 'B');
    $w = array(150,40);
    $this->tcpdf->core->Cell($w[0], 3, 'Total:', 'T', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[1], 3, number_format(sprintf("%01.2f",$TOTAL['descargos']), 2, ',', '.'), 'T', 0, 'R', $fill);
    $this->tcpdf->core->Ln();       

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('listado_cargos_descargos_'.date('d-m-Y',strtotime($fecha)).'.pdf', 'D');
?>
