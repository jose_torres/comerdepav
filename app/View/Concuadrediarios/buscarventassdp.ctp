<?php
    //print_r($data_sd);
    //echo '<br>';
    //print_r($data_sdp_pag);
    $dias_semana = array(1=>'Lun',2=>'Mar',3=>'Mie',4=>'Jue',5=>'Vie',6=>'Sab',7=>'Dom');
?>
<div class="container-fluid">
  <div class="row " style="background-color:#0099BF;color:#FFF">
    <div class="col-xs-4 col-md-4"><strong>D&iacute;as Efectivos:&nbsp;</strong><?php echo $nro_dias_mes[1]+$nro_dias_mes[2]+$nro_dias_mes[3]+$nro_dias_mes[4]+$nro_dias_mes[5]+$nro_dias_mes[6]+$nro_dias_mes[7]; ?></div>
    <div class="col-xs-4 col-md-4"><strong>Total D&iacute;as Efectivos:&nbsp;</strong><?php
     $dias_efectivo = $nro_dias_mes[1]+$nro_dias_mes[2]+$nro_dias_mes[3]+$nro_dias_mes[4]+$nro_dias_mes[5] - $fecha['feriados'];
     echo $dias_efectivo; ?></div>
    <div class="col-xs-4 col-md-4"><strong>D&iacute;as Transcurrido:&nbsp;</strong><?php echo $dias_trans; ?></div>
</div>
<div class="row" style="background-color:lightgrey;">
    <div class="col-xs-6 col-md-8"><strong>Feriados:&nbsp;</strong><?php echo $nro_dias_mes[6] + $fecha['feriados'] ?></div>
    <div class="col-xs-6 col-md-4"><strong>Domingos:&nbsp;</strong><?php echo $nro_dias_mes[7]; ?></div>
  </div>
</div>


<table class="table table-bordered table-hover">
    <tr>
      <th>Fecha</th>	  
      <th><center>C1 (21)</center></th>
      <th><center>C2 (19)</center></th>
      <th><center>C21 (22)</center></th>
      <th><center>Cr&eacute;dito</center></th>
      <th><center>Cobros</center></th>		  
      <th><center>Superdepa</center></th>
      <th><center>Cobros</center></th>		  
      <th><center>Total</center></th>
    </tr>
    <tbody>
<?php
     $total_monto['detal'] = $total_monto['suc1'] = $total_monto['suc2'] = $total_monto['credito'] = $total_monto['sdp'] = $total_monto['total'] = 0;
    //foreach ($data as $row){
    for ($i = 1; $i <= $fecha['dia_fin']; $i++) {
    $fecha_actual = $datos['anio'].'-'.$datos['mes'].'-'.str_pad($i, 2,"0", STR_PAD_LEFT);
    $elegido ='NORMAL';
    if (date('N', strtotime($fecha_actual))==6){
        $elegido ='SABADO';
    }elseif (date('N', strtotime($fecha_actual))==7){
        $elegido ='DOMINGO';	
    }elseif(isset($data[$fecha_actual])){
        $elegido = $data[$fecha_actual]['tipo'];
    }
    $row[0] = $data_sd[$fecha_actual]; 
    $row['SDP'] = $data_sdp[$fecha_actual]; 
    $row['PSDP'] = $data_sdp_pag[$fecha_actual]; 
    
    $class_tr = "";
    if (date('N', strtotime($row[0]['fecha']))<= 5 && $elegido=='NORMAL'){
            //$class_tr = "class='success'";
     
?>
    <tr id='td_id' <?php echo $class_tr;?> >
    <td align="center"><strong><?php echo $dias_semana[date('N', strtotime($row[0]['fecha']))].':&nbsp;'.date('d-m-Y',strtotime($row[0]['fecha'])); ?></strong></td>   
    <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['suc1']), 2, ',', '.'); ?></td>
    <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['suc2']), 2, ',', '.'); ?></td>
    <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['detal']), 2, ',', '.'); ?></td>
    <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['credito']), 2, ',', '.'); ?></td>
    <td align="right"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.'); ?></td>
    <td align="right"><?php echo number_format(sprintf("%01.2f",$row['SDP']['credito']), 2, ',', '.'); ?></td>
    <td align="right"><?php echo number_format(sprintf("%01.2f",$row['PSDP']['cobro']), 2, ',', '.'); ?></td>
    <td align="right"><?php
        $total_monto['detal'] = $total_monto['detal'] + $row[0]['detal'];
        $total_monto['suc1'] = $total_monto['suc1'] + $row[0]['suc1'];
        $total_monto['suc2'] = $total_monto['suc2'] + $row[0]['suc2'];
        $total_monto['credito'] = $total_monto['credito'] + $row[0]['credito'];
        $total_monto['sdp'] = $total_monto['sdp'] + $row['SDP']['credito'];
        $total = $row[0]['detal'] + $row[0]['suc1'] +$row[0]['suc2'] + $row[0]['credito'] + $row['SDP']['credito'];
        $total_monto['total'] = $total_monto['total'] +$total;
    echo number_format(sprintf("%01.2f", $total), 2, ',', '.'); ?></td>
    </tr>
<?php
        }elseif(date('N', strtotime($row[0]['fecha']))== 6){
?>
    <tr class="success" >
        <td colspan="9">&nbsp;</td>
    </tr>
<?php    
        }
    }
?>
    </tbody>
    <tfoot>
      <tr>
        <th align="right">Totales:</th>			
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto['suc1'] ), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto['suc2'] ), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f",  $total_monto['detal'] ), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto['credito']), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto['sdp']), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto['detal'] +$total_monto['suc1'] +$total_monto['suc2'] +$total_monto['credito'] ), 2, ',', '.');
         ?></div></th>
    </tr> 
    <tr>
        <th align="right">Total Proyecci&oacute;n:</th>            
        <th  ><div align="right"><?php
        $proyeccion = 0;
        if($dias_trans>0)
        $proyeccion = $total_monto['suc1']/$dias_trans * $dias_efectivo; 
        echo number_format(sprintf("%01.2f", $proyeccion ), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php
        $proyeccion = 0;
        if($dias_trans>0)
        $proyeccion = $total_monto['suc2']/$dias_trans * $dias_efectivo; 
        echo number_format(sprintf("%01.2f", $proyeccion ), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php
        $proyeccion = 0;
        if($dias_trans>0)
        $proyeccion = $total_monto['detal']/$dias_trans * $dias_efectivo; 
        echo number_format(sprintf("%01.2f", $proyeccion ), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php
        $proyeccion = 0;
        if($dias_trans>0)
        $proyeccion = $total_monto['credito']/$dias_trans * $dias_efectivo; 
        echo number_format(sprintf("%01.2f", $proyeccion), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php
        $proyeccion = 0;
        if($dias_trans>0)
        $proyeccion = $total_monto['sdp']/$dias_trans * $dias_efectivo; 
        echo number_format(sprintf("%01.2f", $proyeccion), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
         ?></div></th>
        <th  ><div align="right"><?php
        $proyeccion = 0;
        if($dias_trans>0)
        $proyeccion = ($total_monto['detal'] +$total_monto['suc1'] +$total_monto['suc2'] +$total_monto['credito']+$total_monto['sdp'])/$dias_trans * $dias_efectivo;
        echo number_format(sprintf("%01.2f",  $proyeccion), 2, ',', '.');
         ?></div></th>
    </tr> 
    </tfoot>
</table>
