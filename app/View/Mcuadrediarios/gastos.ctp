<?php
// print_r($depositos);
?>
<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nombre</th>
		  <th>Motivo</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Gastos'] as $row){
		$total_monto=$total_monto+$row['Sucgasto']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Sucgasto']['id']; ?></td>
        <td align="left"><?php echo $row['Sucgasto']['nombre']; ?></td>
        <td align="left"><?php echo $row['Sucgasto']['motivo']; ?></td>                
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Sucgasto']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('gasto','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Gasto&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&sucgasto_id=<?php echo $row['Sucgasto']['id'];?>')">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="3" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
<script>
document.getElementById('tot_gastos').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_gastos').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
calcularCuadre();
</script>
