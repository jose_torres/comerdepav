<?php
	//print_r($pdv);
?>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-condensed table-bordered" id="puntos">
<caption>  <strong>Movimientos en Puntos de Ventas</strong></caption>
<thead>
    <tr>
        <th>Reg.</th>
        <th>Modelo</th>
        <th>Marca</th>
        <th>Serial</th>
        <th>Banco</th>
        <th>Cuenta</th>
        <th>Monto</th>
        <th>Sel.</th>
    </tr>
</thead>
<tbody>
    <?php $x=0;$total_registros=0; $total_x_depositar=0;$total_monto=0;
    foreach($pdv as $row){
		$registro = $row[0];
    ?>
    <?php $x=$x+1; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven" id='td_id'>
	<? } else { ?>
	<tr id='td_id'>
	<? } ?>
        <td align="center">
            <?php echo $x; ?>
        </td>
        <td align="right">
			<?php
			//echo $this->Form->input('Cheque.id'.$x, array('type'=>'hidden','value'=>$registro['id']));
			echo $registro['modelo'];
             ?>
        </td>
        <td>&nbsp;<?php echo $registro['marca'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['serial'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['nombre'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['nrodecuenta'];?>&nbsp;
		<?php
		echo $this->Form->input('Debito.codbanco'.$x, array('type' =>'hidden','label' =>'','value'=>$registro['codbanco']));
		echo $this->Form->input('Debito.nrodecuenta'.$x, array('type' =>'hidden','label' =>'','value'=>$registro['nrodecuenta']));
		?>
        </td>
        <td align="right">
            <?php
            $total_monto=$total_monto+$registro['monto_pagado'];
            $total_x_depositar = $total_x_depositar+$registro['monto_x_depositar'];
            echo $this->Form->input('Debito.monto'.$x, array('size' => 15,'type' =>'text','label' =>'','style'=>'text-align:right;','value'=>number_format(sprintf("%01.2f",$registro['monto_x_depositar']), 2, ',', '.'),'onkeyup'=>"numero('DebitoMonto".$x."');calcularMontoDebito(formato_ingles_campo(document.getElementById('DebitoMonto".$x."').value),formato_ingles_campo(document.getElementById('total_monto_x_dep_pdv').innerHTML),'DebitoMonto".$x."')"));
            echo $this->Form->input('Debito.montosel'.$x, array('size' => 15,'type' =>'hidden','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",$registro['monto_x_depositar'])));
            ?>
        </td>
        <td>&nbsp;<?php echo $this->Form->input('Seleccion'.$x.'.debitar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'onclick'=>"calcularCheques('Seleccion".$x."Debitar','DebitoMontosel".$x."','DebitoMonto".$x."')"));
		echo $this->Form->input('Debito.codpuntoventa'.$x, array('type' =>'hidden','label' =>'','value'=>$registro['codpuntoventa']));	
		echo $this->Form->input('Debito.codcuenta'.$x, array('type' =>'hidden','label' =>'','value'=>$registro['codcuenta']));	
		?></td>
     </tr>
    <?php } ?>
</tbody>
<tfoot>
    <tr>  
        <th colspan="2" align='right' ><strong>Total Monto en Punto de Venta:</strong></th>
        <th><div align='right' id='total_monto_pdv'><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.') ;?></div> </th>
        <th>&nbsp;<strong>Total por Depositar:</strong></th>
		<th><div align='right' id='total_monto_x_dep_pdv'><?php echo number_format(sprintf("%01.2f", $total_x_depositar), 2, ',', '.') ;?></div></th>
		<th  align='right' ><strong>Total a Depositar:</strong></th>
	<th>
	<?php echo $this->Form->input('Debito.totales', array('type'=>'hidden','value'=>$x)); ?>
	<div align='right' id='total_monto_deb_dep'><?php
	echo number_format(sprintf("%01.2f", $total_x_depositar), 2, ',', '.') ;
?></div> </th>
</tfoot>
</table>
