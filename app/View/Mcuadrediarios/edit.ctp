<?php
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
?>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}

</style>
<?php echo $this->Form->create('Mcuadrediario', array('name' => 'frm','id'=>'frm','class'=>'form-horizontal','url'=>'edit')); ?>
<fieldset id="personal" >
<legend  class="info">EDITAR UN CUADRE DE CAJA</legend>
<?php 
	echo $this->Form->input('Mcuadrediario.id', array('type'=>'hidden'));
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'N&uacute;mero:', array('class' => 'col-xs-4 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-2">';	
	echo $this->Form->input('Mcuadrediario.id',array('label'=>false,'type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
	$fecha=date("d-m-Y",strtotime($this->data['Mcuadrediario']['fecha']));
	echo $this->Html->tag('label', 'Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="input-daterange col-xs-2">';
	echo $this->Form->input('Mcuadrediario.fecha2',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true)); 
	echo '</div>';
	echo '</div>';

	echo '<div class="form-group">';	
	echo $this->Html->tag('label', 'Estatus:', array('class' => 'col-xs-4 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-6">';
	$var[1]='Abierto';
	$var[2]='Cerrado';
	echo $this->Form->input('Mcuadrediario.estatus',array('label'=>false,'type'=>'select','div'=>false,'empty' =>'(Seleccione uno ...)','class'=>"form-control select2","style"=>"width: 100%;",'options'=>$var));
	echo '</div>';
	echo '</div>';
?>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Guardar Registro"));
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/mcuadrediarios/index", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset >
</form>
</fieldset >
