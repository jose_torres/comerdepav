<?php

?>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table" id="cheque">
<caption>  <strong>Cheques</strong></caption>
<thead>
    <tr>
        <th>Reg.</th>
        <th>Nro</th>
        <th>Banco</th>
        <th>Cuenta</th>
        <th>Titular</th>
        <th>Monto</th>
        <th>Sel.</th>
    </tr>
</thead>
<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($cheques as $row): $registro = $row['Succheque']; ?>
    <?php $x=$x+1; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven" id='td_id'>
	<? } else { ?>
	<tr id='td_id'>
	<? } ?>
        <td align="center">
            <?php echo $x; ?>
        </td>
        <td align="right">
			<?php
			echo $this->Form->input('Cheque.id'.$x, array('type'=>'hidden','value'=>$registro['id']));
			echo $registro['nro'];
             ?>
        </td>
        <td>&nbsp;<?php echo $row['Banco']['nombre'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['nrocta'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['titular'];?>&nbsp;</td>
        <td align="right">
            <?php
            $total_monto=$total_monto+$registro['monto'];
            echo $this->Form->input('Cheque.monto'.$x, array('size' => 15,'type' =>'text','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",$registro['monto']),'readonly'=>true));
            echo $this->Form->input('Cheque.montosel'.$x, array('size' => 15,'type' =>'hidden','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",0)));
            ?>
        </td>
        <td>&nbsp;<?php echo $this->Form->input('Seleccion'.$x.'.chequear', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'onclick'=>"calcularCheques('Seleccion".$x."Chequear','ChequeMontosel".$x."','ChequeMonto".$x."')"));
		echo $this->Form->input('Valor.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$registro['id']));
		echo $this->Form->input('Valor.'.$x.'.codsucursal', array('type' =>'hidden','label' =>'','value'=>$registro['codsucursal']));		
		?></td>
     </tr>
    <?php endforeach; ?>
</tbody>
<tfoot>
    <tr>        
        <th>&nbsp;</th>
        <th colspan="2" align='right' ><strong>Total Monto en Cheque:</strong></th>
        <th><div align='right' id='total_monto_cheque'><?php echo number_format(sprintf("%01.2f", $total_monto), 2, '.', '.') ;
?></div> </th>
		<th  align='right' ><strong>Total a Depositar:</strong></th>
	<th>
	<?php echo $this->Form->input('Cheque.totales', array('type'=>'hidden','value'=>$x)); ?>
	<div align='right' id='total_monto_che_dep'><?php
	echo number_format(sprintf("%01.2f", 0), 2, '.', '.') ;
?></div> </th>	
	<th>&nbsp;</th>
</tfoot>
</table>
