<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>N&uacute;mero</th>
		  <th>Factura</th>		  
		  <th>Cliente</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($retenciones['Registros'] as $row){
		$total_monto=$total_monto+$row[0]['montoretenido'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row[0]['codretencion']; ?></td>
        <td align="center"><?php echo $row[0]['numero']; ?></td>
        <td align="left"><?php echo $row[0]['documento']; ?></td>
        <td align="left"><?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'); ?></td>
        <td align="center">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('retencion','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Retencion&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&codretencion_id=<?php echo $row[0]['codretencion'];?>')">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="4" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
<script>
document.getElementById('tot_retencion').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_retencion').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
calcularCuadre();
</script>
