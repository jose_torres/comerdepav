<?php
	echo $this->Form->input('Cuadre.id', array('type'=>'hidden','value'=>$cuadre['Cuadrediario']['id']));
	echo $this->Form->input('Cuadre.fecha', array('type'=>'hidden','value'=>$cuadre['Cuadrediario']['fecha']));
?>
<br>
<div class="alert alert-info"><strong><?php echo $mensaje; ?></strong></div>
<div class="panel"><h2>Resumen de Cierre de Caja</h2></div>
<div class="box-body table-responsive no-padding">
<?php 
	echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => '../mcuadrediarios/viewpdf', $cuadre['Cuadrediario']['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
?>

<div id="exTab3" class="panel">	
<ul  class="nav nav-pills">
			<li class="active"><a href="#4b" data-toggle="tab"><h4><strong>Resumen</strong></h4></a>
			</li>
			<li><a href="#6b" data-toggle="tab"><h4><strong>Cuadre</strong></h4></a>
			</li>			
			<li>
			<a  href="#1b" data-toggle="tab"><h4><strong>Transacciones</strong></h4></a>
			</li>
			<li><a href="#5b" data-toggle="tab"><h4><strong>Pagos</strong></h4></a>
			</li>
<!--		<li><a href="#7b" data-toggle="tab"><h4><strong>Gastos</strong></h4></a>
			</li>
		<li><a href="#2b" data-toggle="tab"><h4><strong>Devoluciones</strong></h4></a>
			</li>
			<li><a href="#3b" data-toggle="tab"><h4><strong>Ventas - Devoluciones</strong></h4></a>
			</li>  		-->
			
		</ul>

			<div class="tab-content clearfix">
<div class="tab-pane" id="1b">
         <div class="panel panel-primary">
 
  <div class="panel-heading"><h3><strong><?php echo $titulo; ?></strong></h3></div>
 
  <table class="table table-bordered">
		<tr>
		   <th width="7%">Fecha</th>	
		   <th width="7%">Hora</th>	
		  <th width="5%">Nro</th>
		  <th width="7%">Factura</th>
		  <th width="25%">Cliente</th>
		  <th>Total</th>
		  <th>Contado</th>
		  <th>Cr&eacute;dito</th>		  
		  <th>Iva</th>
		  <th>Retenciones</th>		  
		  <th>Pag. Efectivo</th>
		  <th>Pag. Debito</th>
		  <th>Pag. Cheque</th>
		  <th>Pag. Transfer.</th>
		  <th>Tipo de Ventas</th>
		</tr>
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;$suma_efectivo = $suma_cheque = $suma_debito = $suma_transfer = 0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){
		$CONT=$CONT+1;			

		$suma_factura_contado=$suma_factura_contado + $row[0]['contado'];
		$suma_factura_credito=$suma_factura_credito + $row[0]['credito'];		
		$suma_iva=$suma_iva + $row[0]['impuestos'];
		$suma_retencion =$suma_retencion + $row[0]['retencion'];
		$suma_efectivo =$suma_efectivo + $row[0]['pagoefectivo'];
		$suma_cheque =$suma_cheque + $row[0]['pagocheque'];
		$suma_debito =$suma_debito + $row[0]['pagodebito'];
		$suma_transfer =$suma_transfer + $row[0]['pagotransfer'];
		$suma_bruto=$suma_bruto + $row[0]['contado'] + $row[0]['impuestos'] + $row[0]['credito'];
			?>
		<tr>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>
		  <td><?php echo $row[0]['hora'];?></td>
		  <td><?php echo $row[0]['codventa'];?></td>
		  <td><?php echo $row[0]['numerofactura'];?></td>
		  <td><?php echo $row[0]['rif'].'=>'.$row[0]['descripcion']?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado'] + $row[0]['impuestos'] + $row[0]['credito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['credito']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['impuestos']), 2, ',', '.');?></td>		 
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['retencion']), 2, ',', '.');?></td>		 
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagoefectivo']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagodebito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagocheque']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagotransfer']), 2, ',', '.');?></td>
		  <td><?php if($row[0]['estatus']=='P'){
				echo 'Contado';
			  }elseif($row[0]['estatus']=='A'){
				echo 'Credito';
			  }elseif($row[0]['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>
		 </tr> 		
		<?php		
		
		}
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;
$total_ventas['cheque']=$suma_cheque;
$total_ventas['efectivo']=$suma_efectivo;
$total_ventas['debito']=$suma_debito;
$total_ventas['transfer']=$suma_transfer;$total_ventas['retencion']=$suma_retencion;	
$total_ventas['factura_contado']=$suma_factura_contado;		
$total_ventas['factura_credito']=$suma_factura_credito;		
$total_ventas['numero']=$CONT;		
echo $this->Form->input('Venta.totales', array('type' =>'hidden','label' =>'','value'=>$CONT));
		?>

<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		   <td>&nbsp;</td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td> 
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_efectivo), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_debito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_cheque), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_transfer), 2, ',', '.');?></td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
 </table>
<?php
if($CONT>0){
?> 
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?>
</div>

 <div class="panel panel-danger">
 
  <div class="panel-heading"><h3><strong><?php echo 'Devoluciones de Ventas'; ?></strong></h3></div>
 
  <table class="table table-bordered">
		<tr>
		  <th width="7%">Fecha</th>	
		  <th width="7%">Hora</th>	
		  <th width="7%">Nro</th>
		  <th width="30%">Cliente</th>
		  <th>Total</th>
		  <th>Contado</th>
		  <th>Cr&eacute;dito</th>		  
		  <th>Iva</th>
		  <th>Retenciones</th>		  
		  <th>Pag. Efectivo</th>
		  <th>Pag. Debito</th>
		  <th>Pag. Cheque</th>
		  <th>Pag. Transfer.</th>
		  <th>Fact. Afecta</th>
		</tr>
<?php
$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;$suma_efectivo = $suma_cheque = $suma_debito = $suma_transfer = 0;
		$BANCO='';$CONT=0;	
		foreach ($data_dev as $row){
		$CONT=$CONT+1;	
		$suma_factura_contado=$suma_factura_contado + $row[0]['contado'];
		$suma_factura_credito=$suma_factura_credito + $row[0]['credito'];		
		$suma_iva=$suma_iva + $row[0]['impuestos'];
		$suma_retencion =$suma_retencion + $row[0]['retencion'];
		$suma_efectivo =$suma_efectivo + $row[0]['pagoefectivo'];
		$suma_cheque =$suma_cheque + $row[0]['pagocheque'];
		$suma_debito =$suma_debito + $row[0]['pagodebito'];
		$suma_transfer =$suma_transfer + $row[0]['pagotransfer'];
		$suma_bruto=$suma_bruto + $row[0]['contado'] + $row[0]['impuestos'] + $row[0]['credito'];
			?>
		<tr>
			<td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>
			<td><?php echo date("h:m:s",strtotime($row[0]['fecha']));?></td>
		  <td><?php echo str_pad($row[0]['coddevolucion'], 10, "0", STR_PAD_LEFT);?></td>
		  <td><?php echo $row[0]['rif'].'=>'.$row[0]['descripcion']?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado'] + $row[0]['impuestos'] + $row[0]['credito']), 2, ',', '.');?></td>
		   <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['credito']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['impuestos']), 2, ',', '.');?></td>		 
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['retencion']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagoefectivo']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagodebito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagocheque']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagotransfer']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['numerofactura'];?></td>
		 </tr> 		
		<?php		
		
		}
		?>		
<?php
$total_Devoluciones['factura']=$suma_factura;
$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;
$total_Devoluciones['cheque']=$suma_cheque;
$total_Devoluciones['efectivo']=$suma_efectivo;
$total_Devoluciones['debito']=$suma_debito;
$total_Devoluciones['transfer']=$suma_transfer;
$total_Devoluciones['retencion']=$suma_retencion;
$total_Devoluciones['factura_contado']=$suma_factura_contado;		
$total_Devoluciones['factura_credito']=$suma_factura_credito;
$total_Devoluciones['numero']=$CONT;
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_efectivo), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_debito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_cheque), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_transfer), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		
	</tfoot>
 </table>

</div>

<div class="panel panel-info">
 
  <div class="panel-heading"><h3><strong><?php echo 'Totales (Ventas - Devoluciones):'; ?></strong></h3></div>
<table class="table table-bordered">
	<tr>
		  <th align="center" width="35%">&nbsp;</th>
		  <th align="center">Total</th>  
		  <th align="center">Monto Contado</th>
		  <th align="center">Monto Cr&eacute;dito</th>
		  <th align="center">Iva</th>
		  <th align="center">Retenciones</th>	  
		  <th align="center">Efectivo</th>
		  <th align="center">Debito</th>
		  <th align="center">Cheque</th>
		  <th align="center">Transfer.</th>
		</tr>
	<tfoot>
		<tr>
		  <td align="right"  width='38%'><label>Totales Neto:</label></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_contado']- $total_Devoluciones['factura_contado']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_credito']- $total_Devoluciones['factura_credito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['retencion']- $total_Devoluciones['retencion']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['efectivo']- $total_Devoluciones['efectivo']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['debito']- $total_Devoluciones['debito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['cheque']- $total_Devoluciones['cheque']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['transfer']- $total_Devoluciones['transfer']), 2, ',', '.');?></td>
		</tr>
	</tfoot>
	
</table> 

</div>

				</div>
<div class="tab-pane" id="2b">
  
</div>

<div class="tab-pane" id="3b">
 
</div>


<div class="tab-pane" id="5b">
<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong><?php echo 'Pagos en Efectivo'; ?></strong></h3></div>
    <table class="table table-bordered table-hover">
		<tr>
		  <th>Pago</th>
		  <th>Movimiento</th>
		  <th>Fecha Pago</th>
		  <th>Monto Pagado</th>
		  <th>Nro</th>
		  <th>Banco</th>
		</tr>
	<?php
		$pagos['efectivo']=0;
		$suma_factura=0;	
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($datapago['efectivo'] as $row){
			$suma_factura=$suma_factura + $row[0]['monto_pagado'];
		?>
		<tr>
		  <td><?php  if($row[0]['tipopago']==''){
					echo 'SIN PAGO';
				}else{
					echo $row[0]['tipopago'];
				}
		  ;
		  ?></td>
		  <td><?php echo $row[0]['tipomovimientopago'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto_pagado']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['banco'];?></td>
		 </tr> 
		<?php
		//$BANCO= $row[0]['rif'].'='.$row[0]['numerofactura'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Pagado:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php //echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		</tr>
		</tfoot>
</table>
<?php
	$pagos['efectivo']=$suma_factura;
	$total_ventas['efectivo'] = $total_ventas['efectivo'] + $pagos['efectivo'];
?>
  </div>
<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong><?php echo 'Pagos en Debito'; ?></strong></h3></div>
    <table class="table table-bordered table-hover">
		<tr>
		  <th>Pago</th>
		  <th>Movimiento</th>
		  <th>Fecha Pago</th>
		  <th>Monto Pagado</th>
		  <th>Nro</th>
		  <th>Banco</th>
		</tr>
	<?php
		$suma_factura=0;	$pagos['debito']=0;
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($datapago['debito'] as $row){
			$suma_factura=$suma_factura + $row[0]['monto_pagado'];
		?>
		<tr>
		  <td><?php  if($row[0]['tipopago']==''){
					echo 'SIN PAGO';
				}else{
					echo $row[0]['tipopago'];
				}
		  ;
		  ?></td>
		  <td><?php echo $row[0]['tipomovimientopago'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto_pagado']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['banco'];?></td>
		 </tr> 
		<?php
		//$BANCO= $row[0]['rif'].'='.$row[0]['numerofactura'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Pagado:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php //echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
</table>
<?php
	$pagos['debito']=$suma_factura;
	$total_ventas['debito'] = $total_ventas['debito'] + $pagos['debito'];
?>
  </div>

<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong><?php echo 'Pagos en Cheque'; ?></strong></h3></div>
    <table class="table table-bordered table-hover">
		<tr>
		  <th>Pago</th>
		  <th>Movimiento</th>
		  <th>Fecha Pago</th>
		  <th>Monto Pagado</th>
		  <th>Nro</th>
		  <th>Banco</th>
		</tr>
	<?php
		$suma_factura=0;$pagos['cheque']=0;	
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($datapago['cheque'] as $row){
			$suma_factura=$suma_factura + $row[0]['monto_pagado'];
		?>
		<tr>
		  <td><?php  if($row[0]['tipopago']==''){
					echo 'SIN PAGO';
				}else{
					echo $row[0]['tipopago'];
				}
		  ;
		  ?></td>
		  <td><?php echo $row[0]['tipomovimientopago'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto_pagado']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['banco'];?></td>
		 </tr> 
		<?php
		//$BANCO= $row[0]['rif'].'='.$row[0]['numerofactura'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Pagado:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php //echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		</tr>
		</tfoot>
</table>
<?php
$pagos['cheque']=$suma_factura;
$total_ventas['cheque'] = $total_ventas['cheque'] + $pagos['cheque'];
?>
  </div>

<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong><?php echo 'Pagos en Transferencia'; ?></strong></h3></div>
    <table class="table table-bordered table-hover">
		<tr>
		  <th>Pago</th>
		  <th>Movimiento</th>
		  <th>Fecha Pago</th>
		  <th>Monto Pagado</th>
		  <th>Nro</th>
		  <th>Banco</th>
		</tr>
	<?php
		$suma_factura=0;	$pagos['transfer']=$suma_factura;
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($datapago['transfer'] as $row){
			$suma_factura=$suma_factura + $row[0]['monto_pagado'];
		?>
		<tr>
		  <td><?php  if($row[0]['tipopago']==''){
					echo 'SIN PAGO';
				}else{
					echo $row[0]['tipopago'];
				}
		  ;
		  ?></td>
		  <td><?php echo $row[0]['tipomovimientopago'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto_pagado']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['banco'];?></td>
		 </tr> 
		<?php
	//	$BANCO= $row[0]['rif'].'='.$row[0]['numerofactura'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Pagado:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php //echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
</table>
<?php
$pagos['transfer']=$suma_factura;
$total_ventas['transfer'] = $total_ventas['transfer'] + $pagos['transfer'];
?>
  </div>
</div>
<!-- </div> -->
<div class="tab-pane active" id="4b">
	<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong>Resumen de Cuadre</strong></h3></div>  
     <div class="panel-body">
<!-- Resumen de Venta-->		 
 <table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE VENTAS</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Totales</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Ventas a Contado sin Iva</td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_contado']- $total_Devoluciones['factura_contado']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Ventas a Cr&eacute;dito sin Iva</td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_credito']- $total_Devoluciones['factura_credito']), 2, ',', '.');?></td>
	</tr>
	<tr class="active">
	  <td>3.</td>
	  <td><strong>Venta sin IVA</strong></td>
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f",($total_ventas['factura_credito']- $total_Devoluciones['factura_credito'])+($total_ventas['factura_contado']- $total_Devoluciones['factura_contado'])), 2, ',', '.');?></strong></td>
	</tr>
	<tr >
	  <td>4.</td>
	  <td>Total IVA</td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.');?></td>
	</tr>
	<tr class="active">
	  <td>5.</td>
	  <td><strong>Total Operaci&oacute;n</strong></td>
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.');?></strong></td>
	</tr>
 </table>            
<!-- -->
<!-- Resumen de Caja-->
<?php
$total_ingreso=$pagos['efectivo']+$pagos['debito']+$pagos['cheque']+$pagos['transfer'] + $retenciones['Total']['total'];
$porc['efectivo'] = $porc['debito'] = $porc['cheque'] = $porc['transfer'] = $porc['retencion'] = 0;
if($pagos['efectivo']>0){$porc['efectivo']=$pagos['efectivo']/$total_ingreso*100;};
if($pagos['debito']>0){$porc['debito']=$pagos['debito']/$total_ingreso*100;};
if($pagos['cheque']>0){$porc['cheque']=$pagos['cheque']/$total_ingreso*100;};
if($pagos['transfer']>0){$porc['transfer']=$pagos['transfer']/$total_ingreso*100;};
if($retenciones['Total']['total']>0){$porc['retencion']=$retenciones['Total']['total']/$total_ingreso*100;};
?>
 <table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE CAJA</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Totales</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Ingresos por Ventas en Efectivo&nbsp;<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['efectivo']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$pagos['efectivo']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Ingresos por Ventas en D&eacute;bito<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['debito']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$pagos['debito']), 2, ',', '.');?></td>
	</tr>
	<tr >
	  <td>3.</td>
	  <td>Ingresos por Ventas en Cheque&nbsp;<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['cheque']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$pagos['cheque']), 2, ',', '.');?></td>
	</tr>
	<tr >
	  <td>4.</td>
	  <td>Ingresos por Ventas en Transferencia&nbsp;<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['transfer']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$pagos['transfer']), 2, ',', '.');?></td>
	</tr>
	<tr >
	  <td>5.</td>
	  <td>Retenciones Realizadas por Cliente&nbsp;<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['retencion']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$retenciones['Total']['total']), 2, ',', '.');?></td>
	</tr>
	<tr class="active">
	  <td>6.</td>
	  <td><strong>Total Ingresos</strong></td>
	  <td align="right"><strong><div id="total_ingreso_cuadre"><?php echo number_format(sprintf("%01.2f", $pagos['efectivo']+$pagos['debito']+$pagos['cheque']+$pagos['transfer']+$retenciones['Total']['total']), 2, ',', '.');?></div></strong></td>
	</tr>
	<tr class="active">
	  <td>7.</td>
	  <td><strong>N&uacute;mero de Facturas</strong></td>
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_ventas['numero']), 2, ',', '.');?></strong></td>
	</tr>
	<tr class="active">
	  <td>8.</td>
	  <td><strong>N&uacute;mero de Devoluciones</strong></td>
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_Devoluciones['numero']), 2, ',', '.');?></strong></td>
	</tr>
 </table>    
<!-- -->
<!-- Resumen de Depositos-->
<table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE DEP&Oacute;SITO</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Totales</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Dep&oacute;sitos en Efectivo&nbsp;</td>
	  <td align="right"><div align="right" id="tot_res_efectivo"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Dep&oacute;sitos en D&eacute;bito&nbsp;</td>
	  <td align="right"><div align="right" id="tot_res_debito"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr >
	  <td>3.</td>
	  <td>Dep&oacute;sitos en Cheque</td>
	  <td align="right"><div align="right" id="tot_res_cheque"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr >
	  <td>4.</td>
	  <td>Dep&oacute;sitos en Transferencia</td>
	   <td align="right"><div align="right" id="tot_res_trans"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr class="active">
	  <td>5.</td>
	  <td><strong>Total Dep&oacute;sitos</strong></td>
	  <td align="right"><strong><div align="right" id="tot_res_depositos"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></strong></td>
	</tr>
</table>

<table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE GASTOS Y RETENCIONES</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Totales</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Gastos Realizados</td>
	   <td align="right"><div align="right" id="tot_res_gastos"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Retenciones Realizados por Clientes Registradas</td>
	   <td align="right"><div align="right" id="tot_res_retencion"><?php echo number_format(sprintf("%01.2f",$retenciones['Total']['total']), 2, ',', '.');?></div></td>
	</tr>
 </table>
 
<table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE CAJA</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Mensaje</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td><strong><div id="mensaje_cuadre">Cuadrado</div></strong></td>
	   <td align="right"><strong><div align="right" id="total_cuadre"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></strong></td>
	</tr>
 </table>
<!-- -->
  </div>
  <div class="panel-footer">&nbsp;</div>     
	</div>
</div>

<div class="tab-pane" id="6b">
<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong>Cuadre Diario</strong></h3></div> 
 <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
		  <?php
		  $AEfectivo=0;
		  ?>
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 1 .-&nbsp;<?php echo $this->Html->image("img_acciones/efectivo.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>En Efectivos</strong>&nbsp;&nbsp;Monto Recibido:<strong><?php echo number_format(sprintf("%01.2f", $total_ventas['efectivo']- $total_Devoluciones['efectivo']), 2, ',', '.');?></strong>&nbsp;&nbsp;Depositado:<strong><span id='tot_dep_efectivo'><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">

<div id="dinero">
	<?php //print_r($depositos['Efectivo']);?>
		 <table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Efectivo'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="right">
           &nbsp;
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_efectivo').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_efectivo').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
</script>		
</div>
	  </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
		  <?php
		  $ADebito=0;
		  ?>
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
        2 .-&nbsp;<?php echo $this->Html->image("img_acciones/credit-card.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>Debitos (Punto de Venta)</strong>&nbsp;&nbsp;Monto Recibido:<strong><?php echo number_format(sprintf("%01.2f", $total_ventas['debito']- $total_Devoluciones['debito']), 2, ',', '.');?></strong>&nbsp;&nbsp;Depositado:<strong><span id="tot_dep_debito"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">
<!-- -->		  
<div id="debito">
		 <table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Debito'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="center">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_debito').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_debito').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
</script>		
</div>		   
<!-- -->		  		   
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
        3.-&nbsp;<?php echo $this->Html->image("img_acciones/cheque.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>Cheques</strong>&nbsp;&nbsp;Monto Recibido:<strong><?php echo number_format(sprintf("%01.2f", $pagos['cheque']- $total_Devoluciones['cheque']), 2, ',', '.');?></strong>&nbsp;&nbsp;Depositado:<strong><span id="tot_dep_cheque"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <div class="panel-body">
		<?php
		  $ACheque=0;
		?>	 		  
<!-- -->
<div id="cheques_dep">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Cheque'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
           &nbsp;
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_cheque').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_cheque').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_depositos').innerHTML=formato_numero(eval(formato_ingles_campo(document.getElementById('tot_res_efectivo').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_debito').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_cheque').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_trans').innerHTML)) , 2 , ',' , '.');
</script>
</div>

      </div>
    </div>
  </div>

  <div class="panel panel-default panel-faq ">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
        4.-&nbsp;<?php echo $this->Html->image("img_acciones/transferencia.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>Transferencia</strong>&nbsp;&nbsp;Monto Recibido:<strong><?php echo number_format(sprintf("%01.2f", $total_ventas['transfer']- $total_Devoluciones['transfer']), 2, ',', '.');?></strong>&nbsp;&nbsp;Depositado:<strong><span id="tot_dep_transfer"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <div class="panel-body">
<!-- -->
		  <?php
		  $ATransferencia=0;
		  ?>
<!-- -->
<div id="transfer">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Transfer'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_transfer').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_trans').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
calcularDepositos();
</script>
</div>

      </div>
    </div>
  </div>

    <div class="panel panel-default panel-faq ">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
        5.-&nbsp;<?php echo $this->Html->image("img_acciones/cart.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>Gastos</strong>&nbsp;&nbsp;Registrado:<strong><span id="tot_gastos"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
      <div class="panel-body">
<!--  -->
	  <div class="panel panel-primary">
	<div class="panel-heading"><h3><strong><?php echo 'Gastos en Caja'; ?></strong></h3></div>
<!-- -->
<div id="gasto">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nombre</th>
		  <th>Motivo</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Gastos'] as $row){
		$total_monto=$total_monto+$row['Sucgasto']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Sucgasto']['id']; ?></td>
        <td align="left"><?php echo $row['Sucgasto']['nombre']; ?></td>
        <td align="left"><?php echo $row['Sucgasto']['motivo']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Sucgasto']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="3" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
<script>
document.getElementById('tot_gastos').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_gastos').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
calcularCuadre();
</script>
</div>

	
  </div>          
</div>

 
<!--  -->		  
		</div>
    </div>
 <div class="panel panel-default panel-faq ">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
        6.-&nbsp;<?php echo $this->Html->image("img_acciones/impuestos.png", array("alt" => "Retenciones", "title"=>"Retenciones")) ;?>&nbsp;<strong>Retenciones</strong>&nbsp;&nbsp;Registrado:<strong><span id="tot_retencion"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse6" class="panel-collapse collapse">
      <div class="panel-body">
<!--  -->
	<?php
	    $ARetencion =0
	?>
	  <div class="panel panel-primary">
	<div class="panel-heading"><h3><strong><?php echo 'Retenciones Realizadas en Caja'; ?></strong></h3></div>
<!-- -->
<div id="retencion">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>N&uacute;mero</th>
		  <th>Factura</th>		  
		  <th>Cliente</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($retenciones['Registros'] as $row){
		$total_monto=$total_monto+$row[0]['montoretenido'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row[0]['codretencion']; ?></td>
        <td align="center"><?php echo $row[0]['numero']; ?></td>
        <td align="left"><?php echo $row[0]['documento']; ?></td>
        <td align="left"><?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'); ?></td>
        <td align="center">
            &nbsp;
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="4" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
<script>
document.getElementById('tot_retencion').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_retencion').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
calcularCuadre();
</script>
</div>
	
  </div>          
</div>

 
<!--  -->		  
		</div>
    </div>
  </div>	  

    
  </div>
		  
</div>

</div> 	


</div>

<!-- /.box-header -->	
<?php 
	echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => '../mcuadrediarios/viewpdf', $cuadre['Cuadrediario']['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
?>
	</div>
<!-- /.box-body -->
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/fastclick/lib/fastclick.js');
?>
