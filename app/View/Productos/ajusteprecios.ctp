<?php
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
?>

<style>
.tag-enviado{
    display: inline-block;
    padding: 6px 12px;
    border-radius: 3px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
</style>

<fieldset id="personal" >
<legend> Historial de actualizaciones</legend>
<form class="form-horizontal" id="frm" name ="frm"  method="POST">
    <div class="form-group">
        <label class="col-lg-3 control-label" >Fecha:</label>
        <div class="input-daterange input-group col-lg-4" id="datepicker">
            <input name="desde" id="desde" value="<?php echo $fecha ?>" class="input-sm form-control" placeholder="Desde" readonly>
            <span class="input-group-addon">a</span>
            <input name="hasta" id="hasta" value="<?php echo $fecha2 ?>" class="input-sm form-control" placeholder="Hasta" readonly>     
        </div>
        </div>
<div class="form-group">
<label for="cmbOrdenarPor" class="control-label col-lg-3">Filtrar Por:</label>
<div class="col-lg-4">
<select name="cmbOrdenarPor" id="cmbOrdenarPor" class="select2 form-control">
    <option value="0" selected>Seleccione</option>
    <option value="1">Pendiente</option>
    <option value="2">Finalizado</option>
</select>
</div>
<button type="button" class="btn btn-info"  onclick="prueba()">Buscar <span class="glyphicon glyphicon-search"></span></button>
<?php echo $this->Html->link(__('<span class="glyphicon glyphicon-trash"></span>&nbsp;Limpiar'), array('action' => '/ajusteprecios'),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false,'style'=>'display:none','id'=>'btnLimpiar')); ?>
</div>
</form>

</fieldset>
<div id="buscar">
<table border="1" cellpadding="1" cellspacing="0" width="95%" align="center" class="table table-condensed table-bordered table-hover" id="dtAjuste">
    <thead>
        <tr>
        <th><?php echo $this->Paginator->sort('id','Codigo');?></th>
        <th>Fecha</th>
        <th>Hora</th>        
        <th>Usuario</th>
        <th>Acciones</th>
    </tr>                        
    </thead>
    <tbody>
    <?php $x=0;foreach ($data as $row): $registro = $row['Hajuste']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $registro['codajuste']; ?>    
        </td>
        <td>
          <?php echo $this->Html->link(date('d-m-Y',strtotime($registro['fecha'])), '/productos/ajusteproductos/'.$registro['codajuste']) ?>    
        </td>
        <td>
          <?php echo date('h:i a',strtotime($registro['hora'])) ?>    
        </td>
         <td ><?php echo  $row['Usuario']['nombre'] .' '. $row['Usuario']['apellido'] ; ?></td>
        <td align="center">
			<?php
			
			if ($registro['estatus']!='F'){
				echo $this->Html->link(__('<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;Enviar'), array('action' => 'enviar', $registro['codajuste']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
				echo '&nbsp;&nbsp;&nbsp;';
				echo $this->Html->link(__('<span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar a txt'), array('action' => 'exportar', $registro['codajuste']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
			//	echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>&nbsp;Abrir'), array('action' => 'edit', $registro['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
			//	echo '&nbsp;&nbsp;&nbsp;';
			}else{
                echo '<div class="tag-enviado">Enviado</div>';
				echo '&nbsp;&nbsp;&nbsp;';
			}
			echo '&nbsp;&nbsp;&nbsp;';
			 ?>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>
<?php 
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
    echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script(array('funciones.js','View/Productos/ajusteprecios'));
    
?>