<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>		

<div class="unidades form">
<?php echo $this->Form->create('Producto', array('url' => 'importar','name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned','enctype'=>"multipart/form-data")); ?>
<fieldset id="personal" >
<legend><h4>IMPORTAR AJUSTE</h4></legend>
<?php if(count($mensaje)>0) {?>
<div class="box">
  <div class="box-header">	
	<div class="row">
			<div class="col-sm-12"><h3 class="box-title">Se realiz&oacute; las siguientes acciones: </h3></div>
	</div>
	<?php
		foreach ($mensaje as $row) {
			echo $row;
		}
	?>
	<!-- <div class="alert alert-info"><?php echo $texto; ?></div> -->
	</div>
</div>

<?php
	}
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Buscar Archivo:', array('class' => 'col-xs-2 control-label'));
	echo '<div class="col-xs-4">';
	echo $this->Form->input('Archivo.datos',array('label'=>false,'size' =>'40','type'=>'file','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo '</div>';
	
?>
</fieldset>
</form>
</div>
<form>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Imprimir Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
</form>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
     $.extend($.inputmask.defaults.definitions, {
        'A': { 
            validator: "[GJVE]",
            cardinality: 1,
            casing: "upper" //auto uppercasing
        }
    });
</script>
