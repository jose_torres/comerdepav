<?php
    echo $this->Html->css('datatables/dataTables.bootstrap.css');
	//print_r($datos);
if(count($data)>0){
?>
<!-- /.box-header -->
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Listado de Producto Encontrados</h3>
	</div>
    <table class="display table table-bordered" id="">
        <caption><label>Listado de Producto </label></caption> 
        <thead>			
       <tr>
         <th width="10%">Codigo</th>
         <th width="5%">Dpto</th>
         <th width="20%">Producto</th>
         <th width="5%">Unidad</th>
         <th width="7%">C21</th>
         <th width="7%">C1</th>
         <th width="7%">C2</th>
         <th width="7%">C3</th>
         <th width="7%">Total</th> 
<?php
        if($datos['costo']=='costoactual'){
            echo '<th width="7%">Costo Actual</th>';
            echo '<th width="7%">Valor Bs. Costo Actual</th>';
        }elseif($datos['costo']=='costopromedio'){
            echo '<th width="7%">Costo Promedio</th>';
            echo '<th width="7%">Valor Bs. Costo Promedio</th>';            
        }
?>
       </tr>
       </thead>
       <tbody>
<?php
       $suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
       $BANCO='';$EXISTENCIA['TOTAL']=$EXISTENCIA['tc21']=$EXISTENCIA['tc1']=$EXISTENCIA['tc2']=$EXISTENCIA['tc3']=$CONT=0;	
       $EXISTENCIAC['TOTAL']=$EXISTENCIAC['tc21']=$EXISTENCIAC['tc1']=$EXISTENCIAC['tc2']=$EXISTENCIAC['tc3']=0;	
       foreach ($data as $row){			
        $EXISTENCIA['tc21']= $EXISTENCIA['tc21'] + $row[0]['precio3']*$row[0]['tc21'];
        $EXISTENCIA['tc1']= $EXISTENCIA['tc1'] + $row[0]['precio3']*$row[0]['tc1'];
        $EXISTENCIA['tc2']= $EXISTENCIA['tc2'] + $row[0]['precio3']*$row[0]['tc2'];
        $EXISTENCIA['tc3']= $EXISTENCIA['tc3'] + $row[0]['precio3']*$row[0]['tc3'];
        $EXISTENCIA['TOTAL']= $EXISTENCIA['TOTAL'] + $row[0]['precio3']*$row[0]['total'];			
?>
       <tr>
         <td><?php echo $row[0]['codigo'];?></td>
         <td><?php echo $row[0]['departamen'];?></td>
         <td><?php echo $row[0]['descripcio'];?></td>
         <td><?php echo $row[0]['unidad'];?></td>         
         <?php
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['tc21']), 2, ',', '.').'</td>';       
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['tc1']), 2, ',', '.').'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['tc2']), 2, ',', '.').'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['tc3']), 2, ',', '.').'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['total']), 2, ',', '.').'</td>';        
         
        if($datos['costo']=='costoactual'){
            $suma_factura_costo= $row[0]['costoactu'];
            echo '<td align="right">'.number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.').'</td>';
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['costoactu']*$row[0]['total']), 2, ',', '.').'</td>';
                
        }elseif($datos['costo']=='costopromedio'){
            $suma_factura_costo= $row[0]['costoprom'];
            echo '<td align="right">'.number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.').'</td>';
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['costoprom']*$row[0]['total']), 2, ',', '.').'</td>';
                
        }
        
        $EXISTENCIAC['tc21']= $EXISTENCIAC['tc21'] + $suma_factura_costo*$row[0]['tc21'];
        $EXISTENCIAC['tc1']= $EXISTENCIAC['tc1'] + $suma_factura_costo*$row[0]['tc1'];
        $EXISTENCIAC['tc2']= $EXISTENCIAC['tc2'] + $suma_factura_costo*$row[0]['tc2'];
        $EXISTENCIAC['tc3']= $EXISTENCIAC['tc3'] + $suma_factura_costo*$row[0]['tc3'];
        $EXISTENCIAC['TOTAL']= $EXISTENCIAC['TOTAL'] + $suma_factura_costo*$row[0]['total'];
        $suma_factura=$row[0]['precio3'];		 
         ?>       
         
        </tr> 

       <?php
       $BANCO=$row[0]['departamen'];
       $CONT=$CONT+1;
       }
       ?>

</tbody>
<tfoot>
	<tr>
	  <td colspan='9' align="right"><strong>Totales C21</strong></td>
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['tc21']), 2, ',', '.');?></strong></td>	
	</tr>
	<tr>
	  <td colspan='9' align="right"><strong>Totales C1</strong></td>	  	
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['tc1']), 2, ',', '.');?></strong></td>	
	</tr>
	<tr>
	  <td colspan='9' align="right"><strong>Totales C2</strong></td>
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['tc2']), 2, ',', '.');?></strong></td>	
	</tr>
	<tr>
	  <td colspan='9' align="right"><strong>Totales C3</strong></td>
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['tc3']), 2, ',', '.');?></strong></td>	
	</tr>
	<tr>
	  <td colspan='9' align="right"><strong>Totales </strong></td>
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['TOTAL']), 2, ',', '.');?></strong></td>	
	</tr>
</tfoot>

 </table>
</div>                 
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	$(document).ready(function() {
    $('table.display').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "scrollX": true,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
} );
  
 
</script>
<!-- /.box-body -->
