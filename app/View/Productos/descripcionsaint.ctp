<?php
//echo $this->element('menuinterno',$datos_menu);
//echo $this->Html->script('prototype');
echo $this->Html->script('jquery.min.js');
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');

?>
<script language="javascript" type="text/javascript">
   
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	 // var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	
	function actualizar(nombrediv,url,id,datos){
		
        $('#'+nombrediv).html('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');
		
        var selected = '&productos=0-';var cont=1;
        $('#ReporteProductoId option:checked').each(function(){
        selected += $(this).val() + '-'; 
        });
        fin = selected.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
        selected = selected.substr( 0, fin ); // elimino la coma final

        var departamentos = '&departamentos=0-';var cont=1;
        $('#ReporteDepartamentoId option:checked').each(function(){
        departamentos += $(this).val() + '-'; 
        });
        fin = departamentos.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
        departamentos = departamentos.substr( 0, fin ); // elimino la coma final

        var depositos = '&depositos=0-';var cont=1;
        $('#ReporteDepositoId option:checked').each(function(){
        depositos += $(this).val() + '-'; 
        });
        fin = depositos.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
        depositos = depositos.substr( 0, fin ); // elimino la coma final

        var precios = '&precio=0-';var cont=1;
        $('#ReportePrecio option:checked').each(function(){
        precios += $(this).val() + '-'; 
        });
        fin = precios.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
        precios = precios.substr( 0, fin ); // elimino la coma final

        var vercosto = '&vercosto=0-';var cont=1;
        $('#ReporteVercosto option:checked').each(function(){
        vercosto += $(this).val() + '-'; 
        });
        fin = vercosto.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
        vercosto = vercosto.substr( 0, fin ); // elimino la coma final

        $.ajax({
            type: 'POST',
            url: url+id,
            data: datos+''+selected+''+departamentos+''+depositos+''+precios+''+vercosto,
            dataType: 'html'
    })

        .done(function( html ) {					
                $( '#'+nombrediv ).html( html );
        });

    }
	
</script>
<fieldset id="personal" >
 <legend>Seleccione las Condiciones</legend> 

<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'viewpdfdescripcion/1','id'=>'frm','name'=>'frm'));
?>	
<?php

echo '<div class="form-group hidden">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

$tiporeporte['Inv_Precio']='Listado de Precio';

		
echo '<div class="form-group hidden">';		
echo $this->Html->tag('label', 'Tipo de Reportes:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.tiporeporte',array('div'=>false,'options'=>$tiporeporte,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Ventas",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

echo '<div class="form-group hidden">';
echo $this->Html->tag('label', 'Productos:', array('class' => 'col-xs-2 control-label','for'=>"cliente_id"));
echo '<div class="col-xs-6">';
echo $this->Form->input('Reporte.producto_id',array('multiple' => true,'div'=>false,'options'=>$producto,'label'=>false,'data-placeholder'=>"Seleccione Productos",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';		
echo '</div>';

echo '<div class="form-group">';
echo $this->Html->tag('label', 'Departamento:', array('class' => 'col-xs-2 control-label','for'=>"cliente_id"));
echo '<div class="col-xs-6">';
echo $this->Form->input('Reporte.departamento_id',array('multiple' => true,'div'=>false,'options'=>$departamento,'label'=>false,'data-placeholder'=>"Seleccione los Departamento",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';		
echo '</div>';
echo '<div class="form-group hidden">';
echo $this->Html->tag('label', 'Precios:', array('class' => 'col-xs-2 control-label','for'=>"cliente_id"));
$precio['preciominimo']='Precio Minimo';
$precio['preciomayor']='Precio Mayor';
$precio['preciodetal']='Precio Detal';
echo '<div class="col-xs-5">';
echo $this->Form->input('Reporte.precio',array('multiple' => true,'div'=>false,'options'=>$precio,'label'=>false,'data-placeholder'=>"Seleccione El Precio a Mostrar",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';

/*echo $this->Html->tag('label', 'Costos:', array('class' => 'col-xs-1 control-label','for'=>"cliente_id"));
$costo['costoactual']='Costo Actual';
$costo['costopromedio']='Costo Promedio';
echo '<div class="col-xs-3">';
echo $this->Form->input('Reporte.vercosto',array('multiple' => true,'div'=>false,'options'=>$costo,'label'=>false,'data-placeholder'=>"Seleccione El Costo a Mostrar",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';*/	
echo '</div>';
echo '<div class="form-group hidden">';		
echo $this->Html->tag('label', 'Dep&oacute;sito:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-2">';
echo $this->Form->input('Reporte.deposito_id',array('multiple' => true,'div'=>false,'options'=>$deposito,'label'=>false,'data-placeholder'=>"Seleccione El Deposito",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

echo '<div class="form-group hidden">';	
$condicion['Todos']='Todos';
$condicion['Menor_Igual']='Menor o Igual a ';
$condicion['Menor']='Menor a ';
$condicion['Mayor']='Mayor a ';
$condicion['Mayor_Igual']='Mayor o Igual a';
$condicion['Igual']='Igual a';
echo $this->Html->tag('label', 'Existencia:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-2">';
echo $this->Form->input('Reporte.condicion_id',array('div'=>false,'options'=>$condicion,'label'=>false,'data-placeholder'=>"Seleccione La Condicion",'class'=>"form-control select2","style"=>"width: 100%;",'empty'=>'Todos'));
echo '</div>';
echo '<div class="col-xs-1">';
echo $this->Form->input('Reporte.cantidad',array('label'=>false,'type'=>'text','div'=>false,'class'=>"form-control text-right",'value'=>0));
echo '</div>';
	
echo '</div>';
echo '<div class="hidden form-group hidden" >';
echo $this->Html->tag('label', 'Utilidad:', array('class' => 'col-xs-2 control-label','for'=>"cliente_id"));
$utilidad['Mayor_Igual']='Mayor o Igual a';
$utilidad['Igual']='Igual a';
$utilidad['Menor_Igual']='Menor o Igual a ';
$utilidad['Menor']='Menor a ';
$utilidad['Mayor']='Mayor a ';
echo '<div class="col-xs-3">';
echo $this->Form->input('Reporte.utilidad_id',array('div'=>false,'options'=>$utilidad,'label'=>false,'data-placeholder'=>"Seleccione La Utilidad",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';

echo '<div class="col-xs-1">';
echo $this->Form->input('Reporte.utilcantidad',array('label'=>false,'type'=>'text','div'=>false,'class'=>"form-control text-right",'value'=>0));
echo '</div>';
echo $this->Html->tag('label', '&#37;', array('class' => 'col-xs-1','for'=>"utilcantidad"));
$costo['costoactual']='Costo Actual';
$costo['costopromedio']='Costo Promedio';
echo '<div class="col-xs-2">';
echo $this->Form->input('Reporte.costo',array('div'=>false,'options'=>$costo,'label'=>false,'data-placeholder'=>"Seleccione El Costo",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';
	?>
<button type="button" class="btn btn-info" onclick="actualizar('buscar','comparardescripcion','','tiporeporte='+document.frm.ReporteTiporeporte.value+'&sucursal='+document.frm.ReporteSucursal.value+'&condicion='+document.frm.ReporteCondicionId.value+'&cantidad='+document.frm.ReporteCantidad.value+'&utilidad_id='+document.frm.ReporteUtilidadId.value+'&utilcantidad='+document.frm.ReporteUtilcantidad.value+'&costo='+document.frm.ReporteCosto.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
<!--
<button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarinventario','','tiporeporte='+document.frm.ReporteTiporeporte.value+'&fechadesde='+document.frm.ReporteDesde.value+'&fechahasta='+document.frm.ReporteHasta.value+'&sucursal='+document.frm.ReporteSucursal.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>-->
<button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	
<?php echo $this->Form->end(); ?>
<center>
<form action="" method="post" name="otro">
<div id="buscar">

</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php //echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/funciones/add", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</center>
</form>
</fieldset>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
   
  
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
</script>
