<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("L");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='Fecha:'.date("d-m-Y");$tc_forma[1]='';$tc_size[1]=8;
$titulo[2]='Hora: '.date("h:m:s a");$tc_forma[2]='';$tc_size[2]=8;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'Listado de Precio');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',8);

//$this->tcpdf->core->setEqualColumns(2);
// Colors, line width and bold font
$this->tcpdf->core->SetFillColor(255, 255, 255);
$this->tcpdf->core->SetTextColor(0);
//$this->tcpdf->core->SetDrawColor(128, 0, 0);
$this->tcpdf->core->SetLineWidth(0.3);
$this->tcpdf->core->SetFont('', 'B');
        
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0; $DEP='';       
// Header
//---------- Principal con C1 ----------------------------------------------
$this->tcpdf->core->SetTextColor(0);
$this->tcpdf->core->SetFont('', 'B');

$header=array('Diferencia de Costos Actuales');
$w = array(225); //240
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'BT', 0, 'C', 1);
}
$this->tcpdf->core->Ln();
/*
$header=array(' ','Costos Actual');
$w = array(80,90,90); //260
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'B', 0, 'C', 1);
}
$this->tcpdf->core->Ln();
*/
$header=array('Codigo','Descripcion','C21','C1','Obs','C2','Obs','C3','Obs');
$w = array(12,68,25,25,15,25,15,25,15);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'B', 0, 'C', 1);
}
$this->tcpdf->core->Ln();
foreach($data as $row) {
    
    // Color and font restoration
    //$this->tcpdf->core->SetFillColor(224, 235, 255);
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('');
    // Data
    $fill = 0;
    //foreach($data as $row) {
    $this->tcpdf->core->Cell($w[0], 3, $row[0]['codigo'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 3, $row[0]['descripcio'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[2], 3, number_format(sprintf("%01.2f", $row[0]['costoactu']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[3], 3, number_format(sprintf("%01.2f", $row[0]['c1c1']), 2, ',', '.'), '', 0, 'R', $fill);
    $obs = 'Por Arriba';
    if($row[0]['costoactu']>$row[0]['c1c1']){ $obs = 'Abajo';}elseif($row[0]['costoactu']==$row[0]['c1c1']){$obs = '=';}
    $this->tcpdf->core->Cell($w[4], 3,  $obs, '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[5], 3, number_format(sprintf("%01.2f", $row[0]['c1c2']), 2, ',', '.'), '', 0, 'R', $fill);
    $obs = 'Por Arriba';
    if($row[0]['costoactu']>$row[0]['c1c2']){ $obs = 'Abajo';}elseif($row[0]['costoactu']==$row[0]['c1c2']){$obs = '=';}
    $this->tcpdf->core->Cell($w[6], 3,  $obs, '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[7], 3, number_format(sprintf("%01.2f", $row[0]['c1c3']), 2, ',', '.'), '', 0, 'R', $fill);
    $obs = 'Por Arriba';
    if($row[0]['costoactu']>$row[0]['c1c3']){ $obs = 'Abajo';}elseif($row[0]['costoactu']==$row[0]['c1c3']){$obs = '=';}
    $this->tcpdf->core->Cell($w[8], 3,  $obs, '', 0, 'R', $fill);
    $this->tcpdf->core->Ln();
    //$fill=!$fill;
    $BANCO=$row[0]['codigo'];
    $DEP=$row[0]['departamen'];
    $CONT_FILA=$CONT_FILA+1;
    $CONT=$CONT+1;
}

$this->tcpdf->core->SetFont('', 'B');
$header=array('Diferencia de Costos Promedio');
$w = array(225); //240
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'BT', 0, 'C', 1);
}
$this->tcpdf->core->Ln();
$header=array('Codigo','Descripcion','C21','C1','Obs','C2','Obs','C3','Obs');
$w = array(12,68,25,25,15,25,15,25,15);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'B', 0, 'C', 1);
}
$this->tcpdf->core->Ln();
foreach($data as $row) {
    
    // Color and font restoration
    //$this->tcpdf->core->SetFillColor(224, 235, 255);
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('');
    // Data
    $fill = 0;
    //foreach($data as $row) {
    $this->tcpdf->core->Cell($w[0], 3, $row[0]['codigo'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 3, $row[0]['descripcio'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[2], 3, number_format(sprintf("%01.2f", $row[0]['costoprom']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[3], 3, number_format(sprintf("%01.2f", $row[0]['cp1c1']), 2, ',', '.'), '', 0, 'R', $fill);
    $obs = 'Por Arriba';
    if($row[0]['costoprom']>$row[0]['cp1c1']){ $obs = 'Abajo';}elseif($row[0]['costoprom']==$row[0]['cp1c1']){$obs = '=';}
    $this->tcpdf->core->Cell($w[4], 3,  $obs, '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[5], 3, number_format(sprintf("%01.2f", $row[0]['cp1c2']), 2, ',', '.'), '', 0, 'R', $fill);
    $obs = 'Por Arriba';
    if($row[0]['costoprom']>$row[0]['cp1c2']){ $obs = 'Abajo';}elseif($row[0]['costoprom']==$row[0]['cp1c2']){$obs = '=';}
    $this->tcpdf->core->Cell($w[6], 3,  $obs, '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[7], 3, number_format(sprintf("%01.2f", $row[0]['cp1c3']), 2, ',', '.'), '', 0, 'R', $fill);
    $obs = 'Por Arriba';
    if($row[0]['costoprom']>$row[0]['cp1c3']){ $obs = 'Abajo';}elseif($row[0]['costoprom']==$row[0]['cp1c3']){$obs = '=';}
    $this->tcpdf->core->Cell($w[8], 3,  $obs, '', 0, 'R', $fill);
    $this->tcpdf->core->Ln();
    //$fill=!$fill;
    $BANCO=$row[0]['codigo'];
    $DEP=$row[0]['departamen'];
    $CONT_FILA=$CONT_FILA+1;
    $CONT=$CONT+1;
}


if($CONT>0){
        $this->tcpdf->core->SetTextColor(0);
       $this->tcpdf->core->SetFont('', 'B');
       $w = array(57, 17);
    //   $this->tcpdf->core->Cell($w[0], 7, 'Total:', 'T', 0, 'R', 1);
    //   $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.'), 'T', 0, 'R', 1);
   //    $this->tcpdf->core->Ln();
}
 //   $this->tcpdf->core->Cell(array_sum($w), 0, '', '');
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('diferencia_costo_saint.pdf', 'D');
?>
