<?php
    echo $this->Html->css('datatables/dataTables.bootstrap.css');
	//print_r($datos);
if(count($data)>0){
?>
<!-- /.box-header -->
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Listado de Producto Encontrados</h3>
	</div>
	<?php
		$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){
		//if ($BANCO!= $row['Venta']['estatus']){
		if ($BANCO!= $row[0]['departamen']){
			if($CONT!=0){
		?>
		</tbody>
		 </table>
                 <hr>    
		<?php
			}
		$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
		?>
		<table class="display table table-bordered" id="">
		 <caption><label>Dept. Nro <?php echo $row[0]['departamen']; ?></label></caption> 
                 <thead>
		<tr>
		  <th width="10%">Codigo</th>
		  <th width="20%">Producto</th>
		  <?php
			foreach($datos['precio'] as $reg){
				if($reg=='preciominimo'){
					echo '<th width="15%">Prec. Minimo</th>';
				}elseif($reg=='preciomayor'){
					echo '<th width="15%">Prec. Mayor</th>';
				}elseif($reg=='preciodetal'){
					echo '<th width="15%">Prec. Detal</th>';
				}
			}
		  ?>
                  <th width="7%">Cantidad</th>
		  <th width="7%">Unidad</th>	  		  
		</tr>
                </thead>
                <tbody>
		<?php
		}
		
	//	$suma_factura=$suma_factura + $row[0]['cantidad'];			
			?>
		<tr>
		  <td><?php echo $row[0]['codigo'];?></td>
		  <td><?php echo $row[0]['descripcio'];?></td>
		  <?php
			foreach($datos['precio'] as $reg){
				if($reg=='preciominimo'){
					echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['precio1']), 2, ',', '.').'</td>';
				}elseif($reg=='preciomayor'){
					echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['precio2']), 2, ',', '.').'</td>';
				}elseif($reg=='preciodetal'){
					echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['precio3']), 2, ',', '.').'</td>';
				}
			}
		  ?>
                  <td align="right"><?php echo $row[0]['total'];?></td> 		  
		  <td><?php echo $row[0]['unidad'];?></td>		 		  
		 </tr> 
		
		<?php
		$BANCO=$row[0]['departamen'];
		$CONT=$CONT+1;
		}
		?>

</tbody>
 </table>
</div>                 
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	$(document).ready(function() {
    $('table.display').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "scrollX": true,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
} );
  
 
</script>
<!-- /.box-body -->
