<?php
    echo $this->Html->css('datatables/dataTables.bootstrap.css');
	//print_r($datos);
if(count($data)>0){
?>
<!-- /.box-header -->
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Listado de Producto Encontrados</h3>
	</div>
    <table class="display table table-bordered" id="">
        <caption><label>Listado de Producto con Diferencia de Precios</label></caption> 
        <thead>
		<tr>
			 <th colspan='2' class="text-center">Descripci&oacute;n</th>
			 <th colspan='4' class="text-center">Precio 1</th>
			 <th colspan='4' class="text-center">Precio 2</th>
			 <th colspan='4' class="text-center">Precio 3</th>
		</tr>
			
       <tr>
         <th width="10%">Codigo</th>
         <th width="20%">Producto</th>
         <th width="7%">C21</th>
         <th width="7%">C1</th>
         <th width="7%">C2</th>
         <th width="7%">C3</th>
         <th width="7%" bgcolor="#dfdad9">C21</th>
         <th width="7%" bgcolor="#dfdad9">C1</th>
         <th width="7%" bgcolor="#dfdad9">C2</th>
         <th width="7%" bgcolor="#dfdad9">C3</th>
         <th width="7%">C21</th>
         <th width="7%">C1</th>
         <th width="7%">C2</th>
         <th width="7%">C3</th>
        
       </tr>
       </thead>
       <tbody>
<?php
       $suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
       $BANCO='';$CONT=0;	
       foreach ($data as $row){			
               ?>
       <tr>
         <td><?php echo $row[0]['codigo'];?></td>
         <td><?php echo $row[0]['descripcio'];?></td>
         <?php
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['precio1']), 2, ',', '.').'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['p1c1']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio1']>$row[0]['p1c1']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio1']==$row[0]['p1c1']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['p1c2']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio1']>$row[0]['p1c2']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio1']==$row[0]['p1c2']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['p1c3']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio1']>$row[0]['p1c3']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio1']==$row[0]['p1c3']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         ?>
         <?php
         echo '<td align="right" bgcolor="#dfdad9">'.number_format(sprintf("%01.2f", $row[0]['precio2']), 2, ',', '.').'</td>';
         echo '<td align="right" bgcolor="#dfdad9">'.number_format(sprintf("%01.2f", $row[0]['p2c1']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio2']>$row[0]['p2c1']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio2']==$row[0]['p2c1']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         echo '<td align="right" bgcolor="#dfdad9">'.number_format(sprintf("%01.2f", $row[0]['p2c2']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio2']>$row[0]['p2c2']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio2']==$row[0]['p2c2']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         echo '<td align="right"  bgcolor="#dfdad9">'.number_format(sprintf("%01.2f", $row[0]['p2c3']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio2']>$row[0]['p2c3']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio2']==$row[0]['p2c3']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         ?>
         <?php
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['precio3']), 2, ',', '.').'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['p3c1']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio3']>$row[0]['p3c1']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio3']==$row[0]['p3c1']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['p3c2']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio3']>$row[0]['p3c2']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio3']==$row[0]['p3c2']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['p3c3']), 2, ',', '.').'<br>';
         $obs = '<span class="label label-danger">Por Arriba</span>';
         if($row[0]['precio3']>$row[0]['p3c3']){ $obs = '<span class="label label-danger">Abajo</span>';}elseif($row[0]['precio3']==$row[0]['p3c3']){$obs = '<span class="label label-success">Igual</span>';}
         echo $obs.'</td>';
         ?>
         
        </tr> 

       <?php
       $BANCO=$row[0]['departamen'];
       $CONT=$CONT+1;
       }
       ?>

</tbody>
 </table>
</div>                 
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	$(document).ready(function() {
    $('table.display').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "scrollX": true,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
} );
  
 
</script>
<!-- /.box-body -->
