<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("L");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='Fecha:'.date("d-m-Y");$tc_forma[1]='';$tc_size[1]=8;
$titulo[2]='Hora: '.date("h:m:s a");$tc_forma[2]='';$tc_size[2]=8;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'Consolidado de Inventario');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',8);

//$this->tcpdf->core->setEqualColumns(2);
// Colors, line width and bold font
$this->tcpdf->core->SetFillColor(255, 255, 255);
$this->tcpdf->core->SetTextColor(0);
//$this->tcpdf->core->SetDrawColor(128, 0, 0);
$this->tcpdf->core->SetLineWidth(0.3);
$this->tcpdf->core->SetFont('', 'B');
        
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0; $DEP='';       
// Header
//---------- Principal con C1 ----------------------------------------------
$this->tcpdf->core->SetTextColor(0);
$this->tcpdf->core->SetFont('', 'B');


$header=array('Codigo','Dpto','Descripcion','Unidad','C21','C1','C2','C3','Total','Costo','Valor');
$w = array(12,12,76,10,20,20,20,20,20,30,40);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'B', 0, 'C', 1);
}
$this->tcpdf->core->Ln();
//----------------------------------------
$EXISTENCIA['TOTAL']=$EXISTENCIA['tc21']=$EXISTENCIA['tc1']=$EXISTENCIA['tc2']=$EXISTENCIA['tc3']=$CONT=0;	
$EXISTENCIAC['TOTAL']=$EXISTENCIAC['tc21']=$EXISTENCIAC['tc1']=$EXISTENCIAC['tc2']=$EXISTENCIAC['tc3']=0;
//----------------------------------------
foreach($data as $row) {
    
    $EXISTENCIA['tc21']= $EXISTENCIA['tc21'] + $row[0]['precio3']*$row[0]['tc21'];
    $EXISTENCIA['tc1']= $EXISTENCIA['tc1'] + $row[0]['precio3']*$row[0]['tc1'];
    $EXISTENCIA['tc2']= $EXISTENCIA['tc2'] + $row[0]['precio3']*$row[0]['tc2'];
    $EXISTENCIA['tc3']= $EXISTENCIA['tc3'] + $row[0]['precio3']*$row[0]['tc3'];
    $EXISTENCIA['TOTAL']= $EXISTENCIA['TOTAL'] + $row[0]['precio3']*$row[0]['total'];
    
    if($CONT==41){
        //----------------------------------------
        $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('', 'B');
        $header=array('Codigo','Dpto','Descripcion','Unidad','C21','C1','C2','C3','Total','Costo','Valor');
        $w = array(12,12,76,10,20,20,20,20,20,30,40);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
                $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'B', 0, 'C', 1);
        }
        $this->tcpdf->core->Ln();
        //----------------------------------------   
        $CONT=0;
    }
    // Color and font restoration
    //$this->tcpdf->core->SetFillColor(224, 235, 255);
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('');
    // Data
    $fill = 0;
    //foreach($data as $row) {
    $this->tcpdf->core->Cell($w[0], 3, $row[0]['codigo'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 3, $row[0]['departamen'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[2], 3, $row[0]['descripcio'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[3], 3, $row[0]['unidad'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[4], 3, number_format(sprintf("%01.2f", $row[0]['tc21']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[5], 3, number_format(sprintf("%01.2f", $row[0]['tc1']), 2, ',', '.'), '', 0, 'R', $fill);
    
    $this->tcpdf->core->Cell($w[6], 3, number_format(sprintf("%01.2f", $row[0]['tc2']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[7], 3, number_format(sprintf("%01.2f", $row[0]['tc3']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[8], 3, number_format(sprintf("%01.2f", $row[0]['total']), 2, ',', '.'), '', 0, 'R', $fill);
    if($datos['costo']=='costoactual'){
        $suma_factura_costo= $row[0]['costoactu']; 
    }elseif($datos['costo']=='costopromedio'){
        $suma_factura_costo= $row[0]['costoprom'];
    }
    
    $this->tcpdf->core->Cell($w[9], 3, number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[10], 3, number_format(sprintf("%01.2f", $row[0]['total']*$suma_factura_costo), 2, ',', '.'), '', 0, 'R', $fill);
    
    $this->tcpdf->core->Ln();
    //$fill=!$fill;
    $EXISTENCIAC['tc21']= $EXISTENCIAC['tc21'] + $suma_factura_costo*$row[0]['tc21'];
    $EXISTENCIAC['tc1']= $EXISTENCIAC['tc1'] + $suma_factura_costo*$row[0]['tc1'];
    $EXISTENCIAC['tc2']= $EXISTENCIAC['tc2'] + $suma_factura_costo*$row[0]['tc2'];
    $EXISTENCIAC['tc3']= $EXISTENCIAC['tc3'] + $suma_factura_costo*$row[0]['tc3'];
    $EXISTENCIAC['TOTAL']= $EXISTENCIAC['TOTAL'] + $suma_factura_costo*$row[0]['total'];
    
    $BANCO=$row[0]['codigo'];
    $DEP=$row[0]['departamen'];
    $CONT_FILA=$CONT_FILA+1;
    $CONT=$CONT+1;
}
//----------------------------------------
$this->tcpdf->core->SetTextColor(0);
$this->tcpdf->core->SetFont('', 'B');
$header=array('','Total C21',number_format(sprintf("%01.2f", $EXISTENCIAC['tc21']), 2, ',', '.'));
$w = array(210,30,40);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'BT', 0, 'R', 1);
}
$this->tcpdf->core->Ln();
//----------------------------------------   
//----------------------------------------
$this->tcpdf->core->SetTextColor(0);
$this->tcpdf->core->SetFont('', 'B');
$header=array('','Total C1',number_format(sprintf("%01.2f", $EXISTENCIAC['tc1']), 2, ',', '.'));
$w = array(210,30,40);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'BT', 0, 'R', 1);
}
$this->tcpdf->core->Ln();
//----------------------------------------  
//----------------------------------------
$this->tcpdf->core->SetTextColor(0);
$this->tcpdf->core->SetFont('', 'B');
$header=array('','Total C2',number_format(sprintf("%01.2f", $EXISTENCIAC['tc2']), 2, ',', '.'));
$w = array(210,30,40);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'BT', 0, 'R', 1);
}
$this->tcpdf->core->Ln();
//----------------------------------------  
//----------------------------------------
$this->tcpdf->core->SetTextColor(0);
$this->tcpdf->core->SetFont('', 'B');
$header=array('','Total C3',number_format(sprintf("%01.2f", $EXISTENCIAC['tc3']), 2, ',', '.'));
$w = array(210,30,40);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'BT', 0, 'R', 1);
}
$this->tcpdf->core->Ln();
//----------------------------------------  
//----------------------------------------
$this->tcpdf->core->SetTextColor(0);
$this->tcpdf->core->SetFont('', 'B');
$header=array('','Total',number_format(sprintf("%01.2f", $EXISTENCIAC['TOTAL']), 2, ',', '.'));
$w = array(210,30,40);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
        $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'BT', 0, 'R', 1);
}
$this->tcpdf->core->Ln();
//----------------------------------------  
if($CONT>0){
        $this->tcpdf->core->SetTextColor(0);
       $this->tcpdf->core->SetFont('', 'B');
       $w = array(57, 17);
    //   $this->tcpdf->core->Cell($w[0], 7, 'Total:', 'T', 0, 'R', 1);
    //   $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.'), 'T', 0, 'R', 1);
   //    $this->tcpdf->core->Ln();
}
 //   $this->tcpdf->core->Cell(array_sum($w), 0, '', '');
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('consolidado_saint.pdf', 'D');
?>
