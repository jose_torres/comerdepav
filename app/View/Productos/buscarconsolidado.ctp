<?php
    echo $this->Html->css('datatables/dataTables.bootstrap.css');
    echo $this->Html->css('datatables/rowGroup.dataTables.min.css');
	//print_r($datos);
if(count($data)>0){
?>
<style>
    table.dataTable tr.group-end td {
    text-align: right;
    font-weight: normal;
}
</style>
<!-- /.box-header -->
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Listado de Producto Encontrados</h3>
	</div>
	<div class="box-body">
		<strong>Leyenda:</strong> 
	  <span class="label label-danger">P</span>Diferencia de Precio en el Producto.
	  <span class="label label-warning">C</span>Diferencia de Costo en el Producto.
    </div>
    <table class="display table table-bordered" id="ListadoCuadre">
        <caption><label>Listado de Producto </label></caption> 
        <thead>			
       <tr>
         <th width="5%">Codigo</th>
         <th width="5%">Dpto</th>
         <th width="20%">Producto</th>
         <th width="5%">Alerta</th>
         <th width="5%">Unidad</th>
         <th width="7%">C21</th>
         <th width="7%">C1</th>
         <th width="7%">C2</th>
         <th width="7%">C3</th>
         <th width="7%">Total</th> 
        
<?php
        if($datos['costo']=='costoactual'){
            echo '<th width="7%">Costo Actual</th>';
            echo '<th width="7%">Precio 3</th>';
            echo '<th width="7%">Valor Bs. Costo Actual</th>';
        }elseif($datos['costo']=='costopromedio'){
            echo '<th width="7%">Costo Promedio</th>';
            echo '<th width="7%">Precio 3</th>';
            echo '<th width="7%">Valor Bs. Costo Promedio</th>';            
        }
?>
		
       </tr>
       </thead>
       <tbody>
<?php
       $suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
       $BANCO='';$EXISTENCIA['TOTAL']=$EXISTENCIA['tc21']=$EXISTENCIA['tc1']=$EXISTENCIA['tc2']=$EXISTENCIA['tc3']=$CONT=0;	
       $EXISTENCIAC['TOTAL']=$EXISTENCIAC['tc21']=$EXISTENCIAC['tc1']=$EXISTENCIAC['tc2']=$EXISTENCIAC['tc3']=0;	
       foreach ($data as $row){			
        $EXISTENCIA['tc21']= $EXISTENCIA['tc21'] + $row[0]['precio3']*$row[0]['tc21'];
        $EXISTENCIA['tc1']= $EXISTENCIA['tc1'] + $row[0]['precio3']*$row[0]['tc1'];
        $EXISTENCIA['tc2']= $EXISTENCIA['tc2'] + $row[0]['precio3']*$row[0]['tc2'];
        $EXISTENCIA['tc3']= $EXISTENCIA['tc3'] + $row[0]['precio3']*$row[0]['tc3'];
        $EXISTENCIA['TOTAL']= $EXISTENCIA['TOTAL'] + $row[0]['precio3']*$row[0]['total'];
        $MENSAJE['COSTO']='';
        $MENSAJE['PRECIO']='';
        if($row[0]['precio3']!=$row[0]['p3c1'] or $row[0]['precio3']!=$row[0]['p3c2'] or $row[0]['precio3']!=$row[0]['p3c3']){
			$MENSAJE['PRECIO']='<span class="label label-danger">P</span>';		
		}
		if($row[0]['costoactu']!=$row[0]['c1c1'] or $row[0]['costoactu']!=$row[0]['c1c2'] or $row[0]['costoactu']!=$row[0]['c1c3']){
			$MENSAJE['COSTO']='<span class="label label-warning" >C</span>';		
		}
        			
?>
       <tr>
         <td><?php echo "<span style='color:#FFFFFF;'>'</span>".$row[0]['codigo'];?></td>
         <td><?php echo $row[0]['departamen'];?></td>
         <td><?php echo $row[0]['descripcio'];?></td>
         <td><?php echo $MENSAJE['PRECIO'].$MENSAJE['COSTO'];?></td>
         <td><?php echo $row[0]['unidad'];?></td>         
         <?php
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['tc21']), 2, ',', '.').'</td>';       
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['tc1']), 2, ',', '.').'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['tc2']), 2, ',', '.').'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['tc3']), 2, ',', '.').'</td>';
         echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['total']), 2, ',', '.').'</td>';        
          
        if($datos['costo']=='costoactual'){
            $suma_factura_costo= $row[0]['costoactu'];
            echo '<td align="right">'.number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.').'</td>';
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['precio3']), 2, ',', '.').'</td>';
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['costoactu']*$row[0]['total']), 2, ',', '.').'</td>';
                
        }elseif($datos['costo']=='costopromedio'){
            $suma_factura_costo= $row[0]['costoprom'];
            echo '<td align="right">'.number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.').'</td>';
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['precio3']), 2, ',', '.').'</td>';
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['costoprom']*$row[0]['total']), 2, ',', '.').'</td>';
                
        }
       
        $EXISTENCIAC['tc21']= $EXISTENCIAC['tc21'] + $suma_factura_costo*$row[0]['tc21'];
        $EXISTENCIAC['tc1']= $EXISTENCIAC['tc1'] + $suma_factura_costo*$row[0]['tc1'];
        $EXISTENCIAC['tc2']= $EXISTENCIAC['tc2'] + $suma_factura_costo*$row[0]['tc2'];
        $EXISTENCIAC['tc3']= $EXISTENCIAC['tc3'] + $suma_factura_costo*$row[0]['tc3'];
        $EXISTENCIAC['TOTAL']= $EXISTENCIAC['TOTAL'] + $suma_factura_costo*$row[0]['total'];
        $suma_factura=$row[0]['precio3'];		 
         ?>       
         
        </tr> 

       <?php
       $BANCO=$row[0]['departamen'];
       $CONT=$CONT+1;
       }
       ?>

</tbody>
<tfoot>
	<tr>
	  <td colspan='11' align="right"><strong>Totales C21</strong></td>
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['tc21']), 2, ',', '.');?></strong></td>	
	</tr>
	<tr>
	  <td colspan='11' align="right"><strong>Totales C1</strong></td>	  	
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['tc1']), 2, ',', '.');?></strong></td>	
	</tr>
	<tr>
	  <td colspan='11' align="right"><strong>Totales C2</strong></td>
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['tc2']), 2, ',', '.');?></strong></td>	
	</tr>
	<tr>
	  <td colspan='11' align="right"><strong>Totales C3</strong></td>
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['tc3']), 2, ',', '.');?></strong></td>	
	</tr>
	<tr>
	  <td colspan='11' align="right"><strong>Totales </strong></td>
	  <td align="right">Costo:</td>	
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $EXISTENCIAC['TOTAL']), 2, ',', '.');?></strong></td>	
	</tr>
</tfoot>

 </table>
</div>                 
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	
<?php	
	 echo $this->Html->script('funciones.js');
    echo $this->Html->script('plugins/datatables/jquery-3.3.1.js');
    echo $this->Html->script('bootstrap.min.js');
    //echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
    echo $this->Html->script('plugins/datatables/dataTables.rowGroup.min.js');

    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
    echo $this->Html->script('plugins/datatables/dataTables.buttons.min.js');
    echo $this->Html->script('plugins/datatables/buttons.flash.min.js');
    echo $this->Html->script('plugins/datatables/jszip.min.js');
    echo $this->Html->script('plugins/datatables/pdfmake.min.js');
    echo $this->Html->script('plugins/datatables/vfs_fonts.js');
    
    echo $this->Html->script('plugins/datatables/buttons.html5.min.js');
    echo $this->Html->script('plugins/datatables/buttons.print.min.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	$(document).ready(function() {
    $('#ListadoCuadre').DataTable({	
	  "dom": "Blfrtip",
       "buttons": [
            "copy", "csv"/*, "excel"*/
        ],    	  
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "scrollX": false,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
} );
  
 
</script>
<!-- /.box-body -->
