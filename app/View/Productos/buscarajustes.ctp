<table border="1" cellpadding="1" cellspacing="0" width="95%" align="center" class="table table-condensed table-bordered table-hover" id="dtAjuste">
    <thead>
        <tr>
        <th><?php echo $this->Paginator->sort('id','Codigo');?></th>
        <th>Fecha</th>
        <th>Hora</th>        
        <th>Usuario</th>
        <th>Acciones</th>
    </tr>                        
    </thead>
    <tbody>
    <?php $x=0;foreach ($data as $row): $registro = $row['Hajuste']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $registro['codajuste']; ?>    
        </td>
        <td>
          <?php echo $this->Html->link(date('d-m-Y',strtotime($registro['fecha'])), '/productos/ajusteproductos/'.$registro['codajuste']) ?>    
        </td>
        <td>
          <?php echo date('h:i a',strtotime($registro['hora'])) ?>    
        </td>
         <td ><?php echo  $row['Usuario']['nombre'] .' '. $row['Usuario']['apellido'] ; ?></td>
        <td align="center">
			<?php
			
			if ($registro['estatus']!='F'){
                echo $this->Html->link(__('<span class="glyphicon glyphicon-cloud-upload"></span>&nbsp;Enviar'), array('action' => 'enviar', $registro['codajuste']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
				echo '&nbsp;&nbsp;&nbsp;';
				echo $this->Html->link(__('<span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar a txt'), array('action' => 'exportar', $registro['codajuste']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
				echo '&nbsp;&nbsp;&nbsp;';
			//	echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>&nbsp;Abrir'), array('action' => 'edit', $registro['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
			//	echo '&nbsp;&nbsp;&nbsp;';
			}else{
                echo '<div class="btn btn-success">Enviado</div>';
				echo '&nbsp;&nbsp;&nbsp;';
			}
			echo '&nbsp;&nbsp;&nbsp;';
			 ?>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>