<?php
//print_r($vercosto);
    echo $this->Html->css('datatables/dataTables.bootstrap.css');
if(count($data)>0){
?>
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Existencia de Producto Encontrados</h3>
    </div>
    <table border="1" cellpadding="1" cellspacing="0" width="100%" align="center" class="table table-condensed table-bordered table-hover" id="ListadoCuadre">
        <caption><label>Listado de Existencia de Producto</label></caption> 
        <thead>     
        <tr>
          <th width="10%">Codigo</th>  
          <th width="10%">Depto</th>
          <th width="20%">Producto</th>
          <th width="10%">Costo Promedio</th>
          <th width="10%">Costo Actual</th>
          <th width="10%">Precio 1</th>
          <th width="10%">Precio 2</th>
          <th width="10%">Precio 3</th>
        </tr>
        </thead>
        <tbody>
	<?php
            $suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
            $BANCO='';$CONT=0;	
            foreach ($data as $row){         	
        ?> 
            <tr>    
            <td><?php echo $row['Producto']['codigo'];?></td>
            <td><?php echo $row['Producto']['coddepartamento'];?></td>
            <td><?php echo $row['Producto']['nombre'];?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Producto']['costoactual']), 2, ',', '.');?></td> 
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Producto']['costopromedio']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Producto']['preciominimo']), 2, ',', '.');?></td> 
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Producto']['preciomayor']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Producto']['preciodetal']), 2, ',', '.');?></td>
            </tr>
             <?php
            
            $CONT=$CONT+1;
            }
            ?>
        </tbody>    
    </table>    
</div>
    
<?php
}else{
    echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
<?php
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('bootstrap.min.js');
    //echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
    echo $this->Html->script('plugins/datatables/dataTables.buttons.min.js');
    echo $this->Html->script('plugins/datatables/buttons.flash.min.js');
    echo $this->Html->script('plugins/datatables/jszip.min.js');
    echo $this->Html->script('plugins/datatables/pdfmake.min.js');
    echo $this->Html->script('plugins/datatables/vfs_fonts.js');
    
    echo $this->Html->script('plugins/datatables/buttons.html5.min.js');
    echo $this->Html->script('plugins/datatables/buttons.print.min.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
	$('[data-toggle="tooltip"]').tooltip();

    $('#ListadoCuadre').DataTable({
       "dom": "Bfrtip",
       "buttons": [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ], 
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      //"scrollX": true,
      "language": {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
   

  });
 
</script>
<!-- /.box-body -->
