<?php
		echo $this->Html->css('datatables/dataTables.bootstrap.css');

?>
<fieldset id="personal" >
<legend> Resumen de Ajuste de Precio del <?php echo date('d-m-Y',strtotime($ajuste['Hajuste']['fecha'])); ?> a las <?php echo date('h:i a',strtotime($ajuste['Hajuste']['hora'])); ?>  </legend>
<div id="buscar">
		
	<div class="box">
		<div class="box-header">
		  <h3 class="box-title">Listado de Productos</h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
		  <table class="table table-condensed table-bordered" id="dtProductos">
				<thead>
					<tr>
		  <th>Código</th>
		  <th>Descripción</th>
		  <th colspan="2" style="text-align: center">Costo</th>
		  <th colspan="2" style="text-align: center">Precio Minimo</th>
		  <th colspan="2" style="text-align: center">Precio Mayor</th>
		  <th colspan="2" style="text-align: center">Precio Detal</th>
		  <th  style="text-align: center">S1</th>
		  <th  style="text-align: center">S2</th>
			
		</tr>
		</thead>
		<tbody>
    <?php foreach ($resumen as $row){?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['codigo']; ?></td>
        <td><?php echo $row['nombre']; ?></td>
        <td align="center"><?php echo number_format(sprintf("%01.2f", $row['costoanterior']), 2, ',', '.');  ?></td>
        <td align="center" bgcolor = "#64BC60"><?php echo number_format(sprintf("%01.2f", $row['costonuevo']), 2, ',', '.');  ?></td>
        <td align="center"><?php echo number_format(sprintf("%01.2f", $row['preciominimoanterior']), 2, ',', '.');  ?></td>
        <td align="center" bgcolor = "#64BC60"><?php echo number_format(sprintf("%01.2f", $row['preciominimonuevo']), 2, ',', '.');  ?></td>
        <td align="center"><?php echo number_format(sprintf("%01.2f", $row['preciomayoranterior']), 2, ',', '.');  ?></td>
        <td align="center" bgcolor = "#64BC60"><?php echo number_format(sprintf("%01.2f", $row['preciomayornuevo']), 2, ',', '.');  ?></td>
        <td align="center"><?php echo number_format(sprintf("%01.2f", $row['preciodetalanterior']), 2, ',', '.');  ?></td>
        <td align="center" bgcolor = "#64BC60"><?php echo number_format(sprintf("%01.2f", $row['preciodetalnuevo']), 2, ',', '.');  ?></td>
				<td align="center" style="color:<?php echo ($row['sucursal2']=="F")? "green":"red"?>"><span class="glyphicon <?php echo ($row['sucursal2'] == "F") ? "glyphicon-ok-circle":"glyphicon-remove-circle" ?>"></span></td>
				<td align="center" style="color:<?php echo ($row['sucursal3']=="F")? "green":"red"?>"><span class="glyphicon <?php echo ($row['sucursal3'] == "F") ? "glyphicon-ok-circle":"glyphicon-remove-circle" ?>"></span></td>
				
     <?php } ?>
		</tbody>
		  </table>
		</div>
		<!-- /.box-body -->
	  </div>
<!-- -->
		  
</div>

<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">				 
<?php echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar a Listado", "title"=>"Regresar a Listado")) ,"/productos/ajusteprecios", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
<?php echo $this->Html->link($this->Html->image("img_acciones/go-export.png", array("alt" => "Eportar a txt", "title"=>"Exportar a txt")) , array('action' => 'exportar', $ajuste['Hajuste']['codajuste']) , array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;	
<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
<?php
echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	
	?>
<script>
  	$(document).ready(function() {
    $('#dtProductos').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "scrollX": true,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
} );
</script>