<?php
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',6);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>		 
		  
		  <th width="7%" align="center"><strong>Fecha</strong></th>
		  <th width="5%" align="center"><strong>Hora</strong></th>
		  <th width="7%" align="center"><strong>Factura</strong></th>
		  <th width="22%" align="center"><strong>Cliente</strong></th>		  
		  <th  align="right"><strong>Monto Base</strong></th>
		  <th  align="right"><strong>Iva</strong></th>
		  <th  align="right"><strong>Total</strong></th>
		  <th  align="right"><strong>Costos</strong></th>
		  <th  align="right"><strong>Utilidad</strong></th>
		  <th  align="right"><strong>&#37; Utilidad</strong></th>
		  <th width="10%" align="center"><strong>Vendedor</strong></th>	  				
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$total_ventas['factura']=$total_ventas['iva']=$total_ventas['bruto']=$total_ventas['retencion']=0;
$total_Devoluciones['factura']=$total_Devoluciones['iva']=$total_Devoluciones['bruto']=$total_Devoluciones['retencion']=0;
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;$suma_costo=0;
foreach ($data as $row){	
	$suma_factura=$suma_factura + $row[0]['baseimp2'] +$row[0]['montoexento'];
	/*$suma_iva=$suma_iva + round(($row[0]['baseimp2']*$row[0]['porimp2']/100),2);
	$suma_bruto=$suma_bruto + $row[0]['baseimp2'] + round(($row[0]['baseimp2']*$row[0]['porimp2']/100),2);*/
	$suma_iva=$suma_iva + ( $row[0]['ivaimp2']);
	$suma_bruto=$suma_bruto + $row[0]['baseimp2'] + ( $row[0]['ivaimp2'] );	$suma_costo=$suma_costo + $row[0]['costo'];	
//	$suma_retencion = $suma_retencion + $row['Venta']['retencion'];	
	if($row[0]['estatus']=='P'){
		$tipo_compra='Contado';
	  }elseif($row[0]['estatus']=='A'){
		$tipo_compra='A Credito';
	  }elseif($row[0]['estatus']=='E'){
		$tipo_compra='Devuelto';
	  }
	  
	  $basexcien= 0;
		if ($row[0]['baseimp2']>0){
			$basexcien = ($row[0]['baseimp2']+$row[0]['montoexento']-$row[0]['costo'])/$row[0]['baseimp2']*100;
		}
$texto=$texto.'
	<tr>
		  <td width="7%">'.date("d-m-Y",strtotime($row[0]['fecha'])).'</td>
		  <td width="5%">'.$row[0]['hora'].'</td>
		  <td width="7%" align="left">'.$row[0]['numerofactura'].'</td>
		  <td width="22%" align="left">'.$row['cliente']['descripcion'].'</td>
		  
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['baseimp2']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", round(( $row[0]['ivaimp2'] ),2)), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['baseimp2'] + round(( $row[0]['ivaimp2'] ),2)), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['costo']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['baseimp2']+$row[0]['montoexento']-$row[0]['costo']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $basexcien), 2, ',', '.').'</td>
		  <td>'.$row['vendedore']['descripcion'].'</td>
	</tr>';	
}
$porc_dev=0;
if ($suma_factura>0){
$porc_dev=($suma_factura-$suma_costo)/$suma_factura*100;
}		
$texto=$texto.'
	<tr>	
	<td  >&nbsp;</td>
	<td>&nbsp;</td>	
	<td >&nbsp;</td>		
	<td align="right"><b>TOTALES:</b></td>	
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</label> </td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</label></td>		  
	<td align="right">'.number_format(sprintf("%01.2f", $suma_costo), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $suma_factura-$suma_costo), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $porc_dev), 2, ',', '.').'</td>
	<td>&nbsp;</td>		
	</tr>';
//$texto=$texto.'</tbody>';
$total_ventas['factura']=$suma_factura;$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;$total_ventas['costo']=$suma_costo;
$texto=$texto.'</table>';
$this->tcpdf->core->writeHTML('<b>VENTAS</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');
// Compras
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>				 
		<th width="7%"><strong>Fecha</strong></th>
		<th width="5%"><strong>Hora</strong></th>
		<th width="7%"><strong>Devoluci&oacute;n</strong></th>
		<th width="22%"><strong>Cliente</strong></th>
		<th align="right"><strong>Monto Base</strong></th>
		<th align="right"><strong>Iva</strong></th>
		<th align="right"><strong>Total</strong></th>
		<th align="right"><strong>Costos</strong></th>
		<th align="right"><strong>Utilidad</strong></th>
		<th width="7%" align="right"><strong>&#37; Utilidad</strong></th>
		<th align="center"><strong>Fact. Afecta</strong></th>	  				
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0; $suma_costo=0;		$base = 0; $iva=0;
foreach ($data_dev as $row){	
	$base =	$row[0]['baseimp2'] + $row[0]['baseimp1'] + $row[0]['baseimp3'];
	$iva = $row[0]['ivaimp2'] + $row[0]['ivaimp1'] + $row[0]['ivaimp3'];
	$suma_factura=$suma_factura + $base;
	$suma_iva=$suma_iva + $iva;
	$suma_bruto=$suma_bruto + $base + $iva;
	$suma_costo = $suma_costo + $row[0]['costo'];	
	if($row[0]['estatus']=='P'){
		$tipo_compra='Contado';
	  }elseif($row[0]['estatus']=='A'){
		$tipo_compra='A Credito';
	  }elseif($row[0]['estatus']=='E'){
		$tipo_compra='Devuelto';
	  }
	$basexcien= 0;
	if ($base>0){
		$basexcien = ($base+$row[0]['montoexento']-$row[0]['costo'])/$base*100;
	}	
$texto=$texto.'
	<tr>
		 <td width="7%">'.date("d-m-Y",strtotime($row[0]['fecha'])).'</td>
		 <td width="5%">'.date("h:i:s",strtotime($row[0]['fecha'])).'</td>
	      <td width="7%" align="left">'.str_pad($row[0]['coddevolucion'], 9, "0", STR_PAD_LEFT).'</td>
		  <td width="22%" align="left">'.$row['cliente']['descripcion'].'</td>
		  
		  <td align="right">'.number_format(sprintf("%01.2f", $base), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $iva), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $base + $iva), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['costo']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['baseimp2']+$row[0]['montoexento']-$row[0]['costo']), 2, ',', '.').'</td>
		  <td  align="right">'.number_format(sprintf("%01.2f", $basexcien), 2, ',', '.').'</td>
		  <td>'.$row[0]['numerofactura'].'</td>
	</tr>';	
}
$porc_dev=0;
if ($suma_factura>0){
$porc_dev=($suma_factura-$suma_costo)/$suma_factura*100;
}			
	
$texto=$texto.'
	<tr>	
	<td  >&nbsp;</td>	
	<td  >&nbsp;</td>	
	<td >&nbsp;</td>	
	<td align="right"><b>TOTALES:</b></td>	
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</label> </td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</label></td>		  
	<td align="right">'.number_format(sprintf("%01.2f", $suma_costo), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $suma_factura-$suma_costo), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $porc_dev), 2, ',', '.').'</td>	
	<td>&nbsp;</td>				
	</tr>';
$texto=$texto.'</table>';
$total_Devoluciones['factura']=$suma_factura;$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;$total_Devoluciones['costo']=$suma_costo;
$this->tcpdf->core->writeHTML('<b>DEVOLUCIONES</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

$this->tcpdf->core->writeHTML('<b>TOTALES (VENTAS - DEVOLUCIONES)</b>', true, false, true, false,'L');

$this->tcpdf->core->MultiCell(30,1,' '.number_format(sprintf("%01.2f",$total_ventas['factura']- $total_Devoluciones['factura']), 2, ',', '.'),0,'R',false,0,69,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,'  '.number_format(sprintf("%01.2f", $total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.'),0,'R',false,0,90,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,' '.number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.'),0,'R',false,0,110,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,' '.number_format(sprintf("%01.2f", $total_ventas['costo']- $total_Devoluciones['costo']), 2, ',', '.'),0,'R',false,0,130,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,' '.number_format(sprintf("%01.2f", ($total_ventas['factura']- $total_Devoluciones['factura'])-($total_ventas['costo']- $total_Devoluciones['costo'])), 2, ',', '.'),0,'R',false,0,149,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(10,1,' '.number_format(sprintf("%01.2f", (($total_ventas['factura']- $total_Devoluciones['factura'])-($total_ventas['costo']- $total_Devoluciones['costo']))/($total_ventas['factura']- $total_Devoluciones['factura'])*100), 2, ',', '.'),0,'R',false,0,182,'',true,0, false,true, 0,'M',false);
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('ventasdiariasdet.pdf', 'D');
?>
