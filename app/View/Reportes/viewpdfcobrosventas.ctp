<?php

$this->tcpdf->core->SetPageOrientation("L");
$this->tcpdf->core->SetOrientacion("L");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>
		 <th width="25%" align="center">Cliente</th>
		  <th width="5%" align="center">N&uacute;mero</th>
		  <th width="5%" align="center">Ope</th>
		  <th width="7%" align="center">Descripcion</th>	
		  <th width="7%">Fecha Emisi&oacute;n</th>
		  <th width="7%">Fecha Cobranza</th>
		   <th width="5%">Dias</th>
		  <th width="5%">Dias Venc.</th>	  
		  <th width="7%" align="center">Documento</th>		  
		  
		  <th  align="center">Monto</th>
		  <th  align="center">Retenci&oacute;n</th>
		  <th  align="center">Total</th>
		</tr></thead>	
	';
//$texto=$texto.'<tbody>';
$suma_factura_t=$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;
$suma_factura_retencion_t=$suma_factura_retencion=0;$suma_factura_pagado_t=$suma_factura_pagado=0;
//$texto="";
$BANCO='';$CONT=0;
foreach ($data as $row){
 if($row[0]['tipomovimientopago']=='ABO'){
		$retencion=0;
		$pagado=$row[0]['monto_pagado'];
	}else{
		$retencion=$row[0]['retencion'];			
		//$pagado=$row[0]['baseimp2'] + ($row[0]['ivaimp2'] - $row[0]['retencion']);	
		$pagado=$row[0]['monto_pagado'] ;					
	}	 	
	
	
 if($BANCO!= $row[0]['fechapago']){
	 
	if($CONT>0){
		
	$texto=$texto.'
		<tr>
		  <td>&nbsp;</td>			  				  
		  <td>&nbsp;</td> <td>&nbsp;</td> 				  		  				  		
		  <td>&nbsp;</td> <td>&nbsp;</td>			  		  				  		  				  
		  <td>&nbsp;</td> <td>&nbsp;</td> 				  		  				  		  				  
		  <td>&nbsp;</td>		  				  		  				  
		  <td align="right"><b>Totales:</b></td>	
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</b></td>		  
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_retencion), 2, ',', '.').'</b></td>		  
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_pagado), 2, ',', '.').'</b></td>		  
		</tr>';
		$suma_factura_t=$suma_factura_t+$suma_factura;
	$suma_factura_retencion_t=$suma_factura_retencion_t+$suma_factura_retencion;
	$suma_factura_pagado_t=$suma_factura_pagado_t+$suma_factura_pagado;
		$suma_factura=0; $suma_factura_retencion=0;$suma_factura_pagado=0;
	}	 
	$texto=$texto.'
	<tr>
	  <td width="10%" align="left"><b>DIA:&nbsp;</b></td>	
	  <td align="left" colspan="11"><b>'.date("d-m-Y",strtotime($row[0]['fechapago'])).'</b></td>			  
	</tr>';
	
	}
    $suma_factura=$suma_factura + $pagado /*+ $retencion*/;
	$suma_factura_retencion=$suma_factura_retencion+$retencion;
	$suma_factura_pagado=$suma_factura_pagado+$pagado - $retencion ;
	
	
			
if ($row[0]['dias_venc']<0 || $row[0]['dias_venc']==''){
		$dias_venc = 0;
	}else{
		$dias_venc = $row[0]['dias_venc'];
	}	
$texto=$texto.'
	<tr>
	    <td width="25%" align="left">'.$row[0]['descripcion'].'</td>	  
		<td width="5%" align="left">'.$row[0]['nropago'].'</td>
		<td width="5%" align="left">'.substr($row[0]['concepto'], 0, 3).' </td>
		<td width="7%">'.$row[0]['banco'].'-'.$row[0]['nropago'].'</td>
		<td width="7%">'.date("d-m-Y",strtotime($row[0]['fecha'])).'</td> 
		<td width="7%">'.date("d-m-Y",strtotime($row[0]['fechapago'])).'</td>
		<td width="5%">'.$row[0]['dias'].'</td> 
		<td width="5%">'.$dias_venc.'</td> 
		<td width="5%">'.$row[0]['numerofactura'].'</td>				  
		<td align="right">'.number_format(sprintf("%01.2f", $pagado), 2, ',', '.').'</td>
		<td align="right">'.number_format(sprintf("%01.2f", $retencion), 2, ',', '.').'</td>
		<td align="right">'.number_format(sprintf("%01.2f", $pagado-$retencion), 2, ',', '.').'</td>  
	</tr>';
	$CONT=$CONT+1;
	
	$BANCO=$row[0]['fechapago'];
}
	
//$suma_factura_t=$suma_factura_t+$suma_factura;
//	$suma_factura_retencion_t=$suma_factura_retencion_t+$suma_factura_retencion;
//	$suma_factura_pagado_t=$suma_factura_pagado_t+$suma_factura_pagado;
$suma_factura_t=$suma_factura_t+$suma_factura;
	$suma_factura_retencion_t=$suma_factura_retencion_t+$suma_factura_retencion;
	$suma_factura_pagado_t=$suma_factura_pagado_t+$suma_factura_pagado;
$texto=$texto.'
		<tr>
		  <td>&nbsp;</td>		  				  
		  <td>&nbsp;</td> <td>&nbsp;</td> 			  		  				  		  				  
		  <td>&nbsp;</td> <td>&nbsp;</td>		  		  				  		  				  
		  <td>&nbsp;</td> <td>&nbsp;</td> 				  		  				  		  				  
		  <td>&nbsp;</td>		  				  		  				  
		  <td align="right"><b>Totales:</b></td>	
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</b></td>		  
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_retencion), 2, ',', '.').'</b></td>		  
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_pagado), 2, ',', '.').'</b></td>		  
		</tr>';
$texto=$texto.'
		<tr>
		  <td>&nbsp;</td>			  				  
		  <td>&nbsp;</td> <td>&nbsp;</td> 				  		  				  		  				  
		  <td>&nbsp;</td> <td>&nbsp;</td> 				  		  				  		  				  
		  <td>&nbsp;</td> <td>&nbsp;</td> 				  		  				  		  				  
		  
		  <td align="right" colspan="2"><b>Total Per&iacute;odo:</b></td>	
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_t), 2, ',', '.').'</b></td>		  
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_retencion_t), 2, ',', '.').'</b></td>		  
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_pagado_t), 2, ',', '.').'</b></td>		  
		</tr>';		
$texto=$texto.'</table>';

//$this->tcpdf->core->Ln(2);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('cobros_de_ventas.pdf', 'D');
?>
