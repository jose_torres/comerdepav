<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='Fecha:'.date("d-m-Y");$tc_forma[1]='';$tc_size[1]=8;
$titulo[2]='Hora: '.date("h:m:s a");$tc_forma[2]='';$tc_size[2]=8;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'Listado de Precio');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',8);

//$this->tcpdf->core->setEqualColumns(2);
// Colors, line width and bold font
$this->tcpdf->core->SetFillColor(255, 255, 255);
$this->tcpdf->core->SetTextColor(0);
//$this->tcpdf->core->SetDrawColor(128, 0, 0);
$this->tcpdf->core->SetLineWidth(0.3);
$this->tcpdf->core->SetFont('', 'B');
        
$suma_factura=0;$suma_factura_iva=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0; $DEP=''; $suma_factura_costo = 0; $suma_cantidad = 0;
$TOTAL['factura'] = $TOTAL['precio'] = $TOTAL['cantidad'] = $TOTAL['costo'] = $TOTAL['iva'] = 0;      
// Header
    foreach($data as $row) {

    if($DEP!= $row[0]['descripcion'] && $CONT_FILA>0){
       $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('', 'B');
        $w = array(90,15,10,25,30);
     
		$this->tcpdf->core->Cell($w[0], 3, 'Totales por:', 'T', 0, 'R', 1);
		$this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $suma_cantidad), 2, ',', '.'), 'T', 0, 'R', 1);
		$this->tcpdf->core->Cell($w[2], 3,  '', 'T', 0, 'R', 1);
		$this->tcpdf->core->Cell($w[3], 3,  '', 'T', 0, 'R', 1);    
		$this->tcpdf->core->Cell($w[4], 3,  number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.'), 'T', 0, 'R', 1);
		$this->tcpdf->core->Ln();
		$this->tcpdf->core->Ln();
        
        $header=array('Codigo','Producto','Cant.','Unidad','Costo','Total');        
        $w = array(15,75,15,10,25,30);

       $suma_factura=0;$suma_factura_iva=0;$suma_factura_precio=0;$suma_factura_costo = 0; $suma_cantidad = 0;
    }
    //----Suma Por Departamentos--------------
     $suma_factura=$suma_factura + ($row[0]['cantidad'] * $row[0]['costo']);          
     $suma_cantidad = $suma_cantidad + $row[0]['cantidad'];
     $suma_factura_costo = $suma_factura_costo + $row[0]['costo']; 
    //----Suma Globales----------------
    $TOTAL['factura'] = $TOTAL['factura']  + ($row[0]['cantidad'] * $row[0]['costo']);
    $TOTAL['cantidad'] = $TOTAL['cantidad'] + $row[0]['cantidad'];
    $TOTAL['costo'] = $TOTAL['costo'] + $row[0]['costo'];
    
    if($DEP!= $row[0]['descripcion'] ){
        $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('', 'B');
        $this->tcpdf->core->Cell(170, 4, str_pad($row[0]['codcargo'], 8, "0", STR_PAD_LEFT).' Fecha: '.date("d-m-Y",strtotime($row[0]['fecha'])).' Motivo:'.$row[0]['descripcion'], 'T', 0, 'L', 1);
        $this->tcpdf->core->Ln();
        $this->tcpdf->core->Cell(170, 4, 'Autorizado por:'.$row[0]['autorizado'].'. Ejecutado por:'.$row[0]['responsable'], 'B', 0, 'L', 1);
        $this->tcpdf->core->Ln();	

        $this->tcpdf->core->SetFont('', 'B');
        $header=array('Codigo','Producto','Cant.','Unidad','Costo','Total');        
        $w = array(15,75,15,10,25,30);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
                $this->tcpdf->core->Cell($w[$i], 4, $header[$i], 'B', 0, 'C', 1);
        }
        $this->tcpdf->core->Ln();
    }
    
    // Color and font restoration
    //$this->tcpdf->core->SetFillColor(224, 235, 255);
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('');
    // Data
    $fill = 0;
    
    $this->tcpdf->core->Cell($w[0], 3, $row[0]['codigo'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 3, $row[0]['nombre'], '', 0, 'L', $fill);
	$this->tcpdf->core->Cell($w[2], 3, number_format(sprintf("%01.2f", $row[0]['cantidad']), 2, ',', '.'), '', 0, 'R', $fill);
	$this->tcpdf->core->Cell($w[3], 3, $row[0]['unidad'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[4], 3, number_format(sprintf("%01.2f", $row[0]['costo']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[5], 3, number_format(sprintf("%01.2f",  $row[0]['cantidad'] * $row[0]['costo']), 2, ',', '.'), '', 0, 'R', $fill);
    
	
  
    $this->tcpdf->core->Ln();
    //$fill=!$fill;
   // $BANCO=$row[0]['coddep'];
    $DEP=$row[0]['descripcion'];
    $CONT_FILA=$CONT_FILA+1;
    $CONT=$CONT+1;
}


if($CONT>0){
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('', 'B');
    //$w = array(90,30,30,30);
     $w = array(90,15,10,25,30);
     
    $this->tcpdf->core->Cell($w[0], 3, 'Totales por:', 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $suma_cantidad), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[2], 3,  '', 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[3], 3,  '', 'T', 0, 'R', 1);    
    $this->tcpdf->core->Cell($w[4], 3,  number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Ln();


    
    $this->tcpdf->core->Cell($w[0], 3, 'Total Global:', 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $TOTAL['cantidad']), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[2], 3,  '', 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[3], 3,  '', 'T', 0, 'R', 1);    
    $this->tcpdf->core->Cell($w[4], 3,  number_format(sprintf("%01.2f", $TOTAL['factura']), 2, ',', '.'), 'T', 0, 'R', 1);
   
    $this->tcpdf->core->Ln();
    
}

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('cargos_realizados.pdf', 'D');
?>
