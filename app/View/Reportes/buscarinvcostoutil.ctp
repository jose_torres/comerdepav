<?php
    echo $this->Html->css('datatables/dataTables.bootstrap.css');
    echo $this->Html->css('datatables/rowGroup.dataTables.min.css');
    
if(count($data)>0){
?>
<style>
    table.dataTable tr.group-end td {
    text-align: right;
    font-weight: normal;
}
</style>
<!-- /.box-header -->
<div class="box">
<div class="box-header">
  <h3 class="box-title">Listado de Producto Encontrados</h3>
</div>
<table border="1" cellpadding="1" cellspacing="0" width="100%" align="center" class="table table-condensed table-bordered table-hover" id="ListadoCuadre">
<caption><label><?php echo $titulo; ?></label></caption> 
<thead> 
    <tr>
        <th width="20%">Departamento</th>
        <th width="10%">Codigo</th>        
        <th width="20%">Producto</th>
        <?php
        if($datos['costo']=='costoactual'){
            echo '<th width="10%">Costo Actual</th>';
        }else{
            echo '<th width="10%">Costo Promedio</th>';
        }
        foreach($datos['precio'] as $reg){
            if($reg=='preciominimo'){
                echo '<th width="15%">Prec. Minimo</th>';
                echo '<th width="15%">&#37; Utilidad</th>';
            }elseif($reg=='preciomayor'){
                echo '<th width="15%">Prec. Mayor</th>';
                echo '<th width="15%">&#37; Utilidad</th>';
            }elseif($reg=='preciodetal'){
                echo '<th width="15%">Prec. Detal</th>';
                echo '<th width="15%">&#37; Utilidad</th>';
            }
        }
        ?>	  		  
    </tr>
<thead> 
<tbody>    
<?php
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	
foreach ($data as $row){
?>
    <tr>
        <td><?php echo $row['dp']['codigo'].' '.$row[0]['descripcion'];?></td>
        <td><?php echo $row[0]['codigo'];?></td>        
        <td><?php echo $row[0]['nombre'];?></td>
<?php
    if($datos['costo']=='costoactual'){
       echo '<td >'.number_format(sprintf("%01.2f", $row[0]['costoactual']), 2, ',', '.').'</td>';
   }else{
       echo '<td>'.number_format(sprintf("%01.2f", $row[0]['costopromedio']), 2, ',', '.').'</td>';
   }
    foreach($datos['precio'] as $reg){
        if($reg=='preciominimo'){
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['preciominimo']), 2, ',', '.').'</td>';
            $camp="utilidad_min_act";
            if($datos['costo'] == 'costopromedio') $camp="utilidad_min_prom";
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0][$camp]), 2, ',', '.').'</td>';
        }elseif($reg=='preciomayor'){
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['preciomayor']), 2, ',', '.').'</td>';
            $camp="utilidad_may_act";
            if($datos['costo'] == 'costopromedio') $camp="utilidad_may_prom";
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0][$camp]), 2, ',', '.').'</td>';
        }elseif($reg=='preciodetal'){
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['preciodetal']), 2, ',', '.').'</td>';
            $camp="utilidad_det_act";
            if($datos['costo'] == 'costopromedio') $camp="utilidad_det_prom";
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0][$camp]), 2, ',', '.').'</td>';
        }
    }
      ?>		  
    </tr> 
<?php
$BANCO=$row['dp']['codigo'];
$CONT=$CONT+1;
}
?>

<tbody>
</table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	</div>
<?php
    //echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('funciones.js');
    echo $this->Html->script('plugins/datatables/jquery-3.3.1.js');
    echo $this->Html->script('bootstrap.min.js');
    //echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
    echo $this->Html->script('plugins/datatables/dataTables.rowGroup.min.js');

    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
    echo $this->Html->script('plugins/datatables/dataTables.buttons.min.js');
    echo $this->Html->script('plugins/datatables/buttons.flash.min.js');
    echo $this->Html->script('plugins/datatables/jszip.min.js');
    echo $this->Html->script('plugins/datatables/pdfmake.min.js');
    echo $this->Html->script('plugins/datatables/vfs_fonts.js');
    
    echo $this->Html->script('plugins/datatables/buttons.html5.min.js');
    echo $this->Html->script('plugins/datatables/buttons.print.min.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
	$('[data-toggle="tooltip"]').tooltip();
    var groupColumn = 0;
    $('#ListadoCuadre').DataTable({        
       "dom": "Blfrtip",
       "buttons": [
            "copy", "csv"/*, "excel"*/
        ],      
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		},
      "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],        
        order: [[0, 'asc']],
        rowGroup: {            
            endRender: function ( rows, group ) {
                return  ' Total de Registro '+rows.count()+' ';                
                    
            },
            dataSrc:0
        }          
    });
   

  });
 
</script>
<!-- /.box-body -->
