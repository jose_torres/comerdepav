<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='Fecha:'.date("d-m-Y");$tc_forma[1]='';$tc_size[1]=8;
$titulo[2]='Hora: '.date("h:m:s a");$tc_forma[2]='';$tc_size[2]=8;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'Listado de Precio');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);

//$this->tcpdf->core->setEqualColumns(2);
// Colors, line width and bold font
$this->tcpdf->core->SetFillColor(255, 255, 255);
$this->tcpdf->core->SetTextColor(0);
//$this->tcpdf->core->SetDrawColor(128, 0, 0);
$this->tcpdf->core->SetLineWidth(0.3);
$this->tcpdf->core->SetFont('', 'B');
        
$suma_factura_costo = $suma_factura=0;$suma_factura_iva=0;$suma_factura_precio=0; $suma_cantidad = $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0; $DEP='';       
// Header
foreach($data as $row) {
    
    $suma_factura_iva = $suma_factura_iva + $row[0]['iva'];
    $suma_factura_precio = $suma_factura_precio + $row[0]['venta'];   
    $suma_factura = $suma_factura + $row[0]['venta'] + $row[0]['iva'];
    $suma_cantidad = $suma_cantidad + $row[0]['cantidad'];
    $suma_factura_costo = $suma_factura_costo + $row[0]['costo'];
    $UTIL = 0;
    if($row[0]['venta']>0){
            $UTIL = ($row[0]['venta'] - $row[0]['costo'])/$row[0]['venta']*100;
    }
        
    if($CONT_FILA==0 || $CONT_FILA==42){

        $this->tcpdf->core->SetFont('', 'B');
        $header=array('Codigo','Departamento','Cant.','Costo','Monto Base','Iva','Total','Util.');
        $w = array(10,60,12,25,25,20,28,10);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
        }
        $this->tcpdf->core->Ln();
        $CONT_FILA=0;
    }
    // Color and font restoration
    //$this->tcpdf->core->SetFillColor(224, 235, 255);
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('');
    // Data
    $fill = 0;
    
    $this->tcpdf->core->Cell($w[0], 3, $row[0]['coddep'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 3, $row[0]['descripcion'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[2], 3, number_format(sprintf("%01.2f", $row[0]['cantidad']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[3], 3, number_format(sprintf("%01.2f", $row[0]['costo']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[4], 3, number_format(sprintf("%01.2f", $row[0]['venta']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[5], 3, number_format(sprintf("%01.2f", $row[0]['iva']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[6], 3, number_format(sprintf("%01.2f", $row[0]['venta'] + $row[0]['iva']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[7], 3, number_format(sprintf("%01.2f", $UTIL), 2, ',', '.'), '', 0, 'R', $fill);
  
    $this->tcpdf->core->Ln();
    //$fill=!$fill;
    $BANCO=$row[0]['coddep'];
    $DEP=$row[0]['descripcion'];
    $CONT_FILA=$CONT_FILA+1;
    $CONT=$CONT+1;
}
$UTIL = 0;
if($suma_factura>0){
	$UTIL = ($suma_factura-$suma_factura_costo)/$suma_factura*100;
}

if($CONT>0){
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('', 'B');
    $w = array(70,12,25,25,20,28,10);
    
    $this->tcpdf->core->Cell($w[0], 3, 'Totales:', 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[1], 3,  number_format(sprintf("%01.2f", $suma_cantidad), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[2], 3,  number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[3], 3,  number_format(sprintf("%01.2f", $suma_factura_precio), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[4], 3,  number_format(sprintf("%01.2f", $suma_factura_iva), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[5], 3,  number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Cell($w[6], 3,  number_format(sprintf("%01.2f", $UTIL), 2, ',', '.'), 'T', 0, 'R', 1);
    $this->tcpdf->core->Ln();
}

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('ventas_x_dpto.pdf', 'D');
?>
