<?php

$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>		  
		  <th width="8%" align="center">C&oacute;digo</th>
		  <th width="28%" align="center">Producto</th>
		  <th width="10%" align="center">U&#47;R</th>
		  <th width="7%" align="center">Cant.</th>
		  <th align="center">Costo</th>
		  <th align="center">Costo&#47;Total</th>		  		  
		  <th align="center">Descuento</th>
		  <th align="center">Total</th>		
		</tr></thead>	
	';
//$texto=$texto.'<tbody>';
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0;
foreach ($data as $row){
	 	 	
	if($BANCO!= $row[0]['nrodocumento'] && $CONT_FILA>0){
	$texto=$texto.'
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>			  
		  <td align="right"><b>TOTALES:</b></td>	
		  <td align="right">'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $suma_factura_precio), 2, ',', '.').'</td>		  
		  <td align="right">'.number_format(sprintf("%01.2f", ($suma_factura_precio-$suma_utilidad)), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $suma_utilidad), 2, ',', '.').'</td>	  		  
		</tr>';
		$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
	}	 	 	
	 	 	
	$suma_factura=$suma_factura + $row[0]['cantidad'];		
	$suma_factura_costo=$suma_factura_costo + $row[0]['costo'];		
	$suma_factura_precio=$suma_factura_precio + $row[0]['total'];		
	$suma_utilidad=$suma_utilidad + ($row[0]['total']-($row[0]['total']-$row[0]['totalcostodescuento']));	
	
 if($BANCO!= $row[0]['nrodocumento']){
	$texto=$texto.'
	<tr>
	  <td width="10%" align="left"><b>&#35;'.$row[0]['nrodocumento'].'</b></td>	
	  <td width="30%" align="left"><b>'.$row['proveedor']['descripcion'].'</b></td>	
	  <td align="left" colspan="5"><b>&nbsp;Fecha:&nbsp;'.date("d-m-Y",strtotime($row[0]['fecha'])).'</b></td>			  
	</tr>';
	$CONT_FILA=0;
	}	
$texto=$texto.'
	<tr>	  
	     <td width="8%" align="left">'.$row[0]['codigo'].'</td>
		  <td width="28%" align="left">'.$row[0]['nombre'].'</td>
		  <td width="10%" align="left">'.$row[0]['unidad'].'</td>		  
		  <td width="7%" align="right">'.$row[0]['cantidad'].'</td>		  		  
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['costo']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['total']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['total']-$row[0]['totalcostodescuento']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['total']-($row[0]['total']-$row[0]['totalcostodescuento'])), 2, ',', '.').'</td>  
	</tr>';
	$BANCO=$row[0]['nrodocumento'];
	$CONT_FILA=$CONT_FILA+1;
	$CONT=$CONT+1;
}	
$texto=$texto.'
		<tr>
		   <td>&nbsp;</td>	
		  <td>&nbsp;</td>			  
		  <td align="right"><b>TOTALES:</b></td>	
		  <td align="right">'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $suma_factura_precio), 2, ',', '.').'</td>		  
		  <td align="right">'.number_format(sprintf("%01.2f", ($suma_factura_precio-$suma_utilidad)), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $suma_utilidad), 2, ',', '.').'</td>	  		  
		</tr>';
$texto=$texto.'</table>';

//$this->tcpdf->core->Ln(2);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('transacciones_de_compras.pdf', 'D');
?>
