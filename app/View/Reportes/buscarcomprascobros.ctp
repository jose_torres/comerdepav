<?php
	//echo 'Esto es Buscar Ventas';
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';*/	
?>
<!-- /.box-header 
	<div class="box-body table-responsive no-padding">-->
	<div class="box-body">
		<?php
		$suma_factura=0;	
		$suma_pago=0;	
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($data as $row){
		if ($BANCO!= $row[0]['rif'].'='.$row[0]['codcompra']){
			if($CONT!=0){
		?>
		<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Deuda:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura-$suma_pago), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_pago), 2, ',', '.');?></td>	  
		</tr>
		</tfoot>
		</table>
		 
		</section>
		<?php
			}
			$suma_iva=$suma_factura=$suma_pago=0;
		?>
		<section class="invoice">
		<div class="col-xs-12">
          <h2 class="header">
            <small class="pull-left"><?php echo '<label>Factura:&nbsp;</label>'.$row[0]['nrodocumento'];?>&nbsp;&nbsp;&nbsp;<?php echo '<label>Proveedor:&nbsp;</label>'.$row[0]['rif'].'&nbsp;'.$row[0]['descripcion'];?>
            </small>
            <small class="pull-right">Fecha :<?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></small>
          </h2>
        </div>
        
         <div class="col-sm-12 invoice-col">
			<div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Tipo de Factura: 
				<?php if($row[0]['estatus']=='P'){
				echo '<span class="label label-success">Contado</span>';
			  }elseif($row[0]['estatus']=='A'){
				echo '<span class="label label-primary">A Credito</span>';
			  }elseif($row[0]['estatus']=='E'){
				echo '<span class="label label-danger">Devuelto</span>';
			  }
				?>
                </th>
                
				<th>Base:</th>
                <td><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp2']), 2, ',', '.');?></td>
                <th>Iva:</th>
                <td><?php echo number_format(sprintf("%01.2f", $row[0]['ivaimp2']), 2, ',', '.');?></td>               
              
                <th>Total:</th>
                <td><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp2']+ $row[0]['ivaimp2']), 2, ',', '.');?></td>
              </tr>
            </table>
          </div> 

        </div>
			
		<table class="table table-bordered table-hover">
		<tr>
		  <th>Nro</th>	
		  <th>Movimiento</th>
		  <th>Descripcion</th>
		  <th>Fecha </th>		  
		  <th>Deuda</th>
		  <th>Pago</th>
		</tr>
		<?php	

		}

			
		$suma_pago=$suma_pago + $row[0]['montopago'];
		$suma_iva=$suma_iva + $row[0]['ivaimp2'];
			?>
		<tr>
		  <td><?php echo $row[0]['nrodocumento'];?></td>	
		  <td><?php  echo substr($row[0]['tipomovimiento'], 1);  ?></td>
		  <td><?php echo $row[0]['descripcion'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>		  
		  <td align="right"><?php
			if ($row[0]['tipomovimiento']=='1COMPRAS'){
			echo number_format(sprintf("%01.2f",$row[0]['baseimp2']+ $row[0]['ivaimp2']), 2, ',', '.');
			$suma_factura=$suma_factura + $row[0]['baseimp2']+ $row[0]['ivaimp2'];
			}else{ echo '0,00';}
		  ?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montopago']), 2, ',', '.');?></td>
		 </tr> 
		<?php
		$BANCO= $row[0]['rif'].'='.$row[0]['codcompra'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Deuda:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura-$suma_pago), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_pago), 2, ',', '.');?></td>	  
		</tr>
		</tfoot>
</table>
</section>
	</div>
<!-- /.box-body -->
