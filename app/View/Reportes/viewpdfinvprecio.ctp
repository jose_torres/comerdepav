<?php
/*ini_set('memory_limit', '1024M');
$orient='P';$poc_prod='25%';$poc_dpto='25%';
if(count($datos['precio'])>3){
$orient='L';$poc_prod='20%';$poc_dpto='20%';
}elseif(count($datos['precio'])<=2){
$poc_prod='35%';$poc_dpto='35%';
}
$this->tcpdf->core->SetPageOrientation($orient);
$this->tcpdf->core->SetOrientacion($orient);
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
//-------------------
$tot_detalle=0; $tamTitulo=0;
// column titles
$header[] = 'Codigo'; $w[] =15; $tamTitulo = $tamTitulo + 15;
$header[] = 'Departamento';$w[] =20; $tamTitulo = $tamTitulo + 20;
$header[] = 'Producto'; $w[] =60; $tamTitulo = $tamTitulo + 60;
$cont = 0;
foreach($datos['precio'] as $reg){
	if($cont>=1){
	if($reg=='preciominimo'){
		$header[] ='Prec. Minimo'; $w[] =33; $tamTitulo = $tamTitulo + 33;
	}elseif($reg=='preciomayor'){
		$header[] ='Prec. Mayor'; $w[] =33; $tamTitulo = $tamTitulo + 33;
	}elseif($reg=='preciodetal'){
		$header[] ='Prec. Detal'; $w[] =34; $tamTitulo = $tamTitulo + 34;
	}
	}
	$cont = $cont + 1;
}

		$alineacion=array('C','C','L','R','R','R');// R=>Derecha L=>izquierda C=> Centrado
		$datos1=array();$pos=0;
		foreach ($data as $row) {

			$datos1[$pos][]=$row[0]['codigo'];
			$datos1[$pos][]=$row[0]['codigodp'];
			$datos1[$pos][]=$row[0]['nombre'];
			$cont = 0;
			foreach($datos['precio'] as $reg){
				if($cont>=1){
				if($reg=='preciominimo'){
					$datos1[$pos][]=number_format(sprintf("%01.2f", $row[0]['preciominimo']), 2, ',', '.');
				}elseif($reg=='preciomayor'){
					$datos1[$pos][]=number_format(sprintf("%01.2f", $row[0]['preciomayor']), 2, ',', '.');
				}elseif($reg=='preciodetal'){
					$datos1[$pos][]=number_format(sprintf("%01.2f", $row[0]['preciodetal']), 2, ',', '.');
				}
				}
				$cont = $cont + 1;
			}

			
			$pos=$pos+1;
			
			//$tot_detalle=$tot_detalle+$row['Movbancario']['monto'];
		}
//		print_r ($datos1);
//die();
		$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>8,'tamTitulo'=>$tamTitulo);
		//$this->tcpdf->core->SetFont('Arial','',8);
		$this->tcpdf->core->SetFont('helvetica','',7);
		$this->tcpdf->core->FancyTable($header,$datos1,$w,html_entity_decode('Listado de Precio: '),0,$alineacion,$conf_titulo);

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('listado_precio.pdf', 'D');*/
//$this->fpdf = new $this->fpdf(null,'P',$unit='mm',$format='letter');
$this->fpdf->setup('P');

// Construccion de la Cabecera
$this->fpdf->SetTitulo($encabezado['titulo_reporte']);
$this->fpdf->SetLogoIzq($empresa['Empresa']['logo_izquierdo']);
$this->fpdf->SetLogoDer($empresa['Empresa']['logo_izquierdo']);
$this->fpdf->SetLine1($empresa['Empresa']['nombre']);
$this->fpdf->SetLine2('');
$this->fpdf->SetLine3('');
$this->fpdf->SetLine4('');
	
$this->fpdf->obtener_pie($slogan="  ");
// obtiene el nro de paginas    
$this->fpdf->fpdf->AliasNbPages(); 
$this->fpdf->fpdf->AddPage();
$this->fpdf->fpdf->SetFont('Arial','',8);
//$this->fpdf->Header();
$tot_detalle=0; $tamTitulo=0;
// column titles
		
$tot_compra=0;$sub_total=0;
$BANCO='';$CONT=0;
	// Comienzo De Carga de Datos
    foreach ($data as $row) {

    if ($BANCO!=$row['dp']['codigo']){	
        if($CONT!=0){
            //Impresion de Registro por Banco
            $conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);
            $this->fpdf->fpdf->SetFont('Arial','',7);
            $this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode('Depto. '.$BANCO.' '.$DEP),2,$alineacion,$conf_titulo);
        } 
            // Inicializacion de la Cabecera
        $header = $w = array(); $tamTitulo = 0;
        $header[] = 'Codigo'; $w[] =15; $tamTitulo = $tamTitulo + 15;
        //$header[] = 'Departamento';$w[] =20; $tamTitulo = $tamTitulo + 20;
        $header[] = 'Producto'; $w[] =50; $tamTitulo = $tamTitulo + 50;
        $cont = 0;
        foreach($datos['precio'] as $reg){
            if($cont>=1){
            if($reg=='preciominimo'){
                    $header[] ='Prec. Minimo'; $w[] =33; $tamTitulo = $tamTitulo + 33;
            }elseif($reg=='preciomayor'){
                    $header[] ='Prec. Mayor'; $w[] =33; $tamTitulo = $tamTitulo + 33;
            }elseif($reg=='preciodetal'){
                    $header[] ='Prec. Detal'; $w[] =34; $tamTitulo = $tamTitulo + 34;
            }
            }
            $cont = $cont + 1;
        }
        $alineacion=array('C','L','R','R','R');// R=>Derecha L=>izquierda C=> Centrado
        $datos1=array();$pos=0;
    }
        $datos1[$CONT][]=$row[0]['codigo'];
        //$datos1[$CONT][]=$row[0]['codigodp'];
        $datos1[$CONT][]=$row[0]['nombre'];
        $cont = 0;
        foreach($datos['precio'] as $reg){
            if($cont>=1){
                if($reg=='preciominimo'){
                    $datos1[$CONT][]=number_format(sprintf("%01.2f", $row[0]['preciominimo']), 2, ',', '.');
                }elseif($reg=='preciomayor'){
                    $datos1[$CONT][]=number_format(sprintf("%01.2f", $row[0]['preciomayor']), 2, ',', '.');
                }elseif($reg=='preciodetal'){
                    $datos1[$CONT][]=number_format(sprintf("%01.2f", $row[0]['preciodetal']), 2, ',', '.');
                }
            }
            $cont = $cont + 1;
        }

        //$pos=$pos+1;
        //Carga de los registros por Banco
        $BANCO=$row['dp']['codigo'];
        $DEP = $row[0]['descripcion'];
        $CONT=$CONT+1;

    }
    $conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);
    $this->fpdf->fpdf->SetFont('Arial','',7);
    //$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode('Depto. '.$BANCO.' '.$DEP),0,$alineacion,$conf_titulo);
    $this->fpdf->PrintChapter(1,'A RUNAWAY REEF',$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode('Depto. '.$BANCO.' '.$DEP),0,$alineacion,$conf_titulo));

 // Fin de Inclusion de los datos a mostrar en el pdf
    echo $this->fpdf->fpdfOutput('listado_precio.pdf','D');   
?>
