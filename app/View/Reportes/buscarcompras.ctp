<?php
	//echo 'Esto es Buscar Ventas';
	//print_r($data);
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<?php
		$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){
		if ($BANCO!= $row['Compra']['estatus']){
			if($CONT!=0){
		?>
		<tfoot>
		<tr>
		  
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.');?></td>		  		  	  
		  <td>&nbsp;</td>		  	  
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0; $suma_iva=0; $suma_bruto=0; $suma_retencion=0;	
		?>
		<table class="table table-bordered">
		 <caption><h2><?php echo $titulo.''; ?></h2></caption> 
		<tr>		   
		  <th width="7%">Documento</th>
		  <th width="25%">Proveedores</th>
		  <th width="7%">Fecha Emisi&oacute;n</th>
		  <th width="7%">Dep&oacute;sitos</th>
		  <th>Monto Neto</th>
		  <th>Iva</th>
		  <th>Total</th>
		  <th>Retenci&oacute;n</th>
		  <th>Tipo de Compras</th>
		</tr>
		<?php
		}
		?>
		<?php
		$base = ($row['Compra']['baseimp1']+$row['Compra']['baseimp2']+$row['Compra']['baseimp3']);
		$iva =	$row['Compra']['ivaimp1']+$row['Compra']['ivaimp2']+$row['Compra']['ivaimp3'];
		$suma_factura=$suma_factura + $base;
		$suma_iva=$suma_iva + $iva;
		$suma_bruto=$suma_bruto + $base + $iva;
		$suma_retencion = $suma_retencion + $row['Compra']['retencion'];
			?>
		<tr>		  
		  <td><?php echo $row['Compra']['nrodocumento'];?></td>
		  <td><?php echo $row['Proveedore']['rif'].'=>'.$row['Proveedore']['descripcion']?></td>		  
		  <td><?php echo date("d-m-Y",strtotime($row['Compra']['femision']));?></td>
		  <td><?php echo $deposito[$row['Compra']['coddeposito']];?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $base), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $base + $iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Compra']['retencion']), 2, ',', '.');?></td>
		  <td><?php if($row['Compra']['estatus']=='P'){
				echo 'Contado';
			  }elseif($row['Compra']['estatus']=='A'){
				echo 'A Credito';
			  }elseif($row['Compra']['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>
		 </tr> 
		
		<?php
		$BANCO=$row['Compra']['estatus'];
		$CONT=$CONT+1;
		}
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></label></td>
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></label> </td>
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></label></td>		  
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.');?></label></td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
 </table>
 
 
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
<table class="table table-bordered">
		 <caption><h2><?php echo ' Devoluciones de Compras a Credito'; ?></h2></caption> 
		<tr>
		  <th width="7%">Devoluci&oacute;n</th>
		  <th width="25%">Proveedores</th>
		  <th width="7%">Fecha Devoluci&oacute;n</th>
		  <th width="7%">Deposito</th>
		  <th>Monto Neto</th>		  
		  <th>Iva</th>
		  <th>Total</th>
		  <th>Retenci&oacute;n</th>
		  <th>Tipo de Ventas</th>
		  <th>Fact. Afecta</th>
		</tr>
<?php
$suma_factura=0;$suma_factura_contado=$suma_factura_credito=0; $suma_iva=0; $suma_bruto=0;
		$BANCO='';$CONT=0;	
		foreach ($data_dev as $row){
		$base = ($row['Devolucioncompra']['baseimp1']+$row['Devolucioncompra']['baseimp2']+$row['Devolucioncompra']['baseimp3']);
		$iva =	$row['Devolucioncompra']['ivaimp1']+$row['Devolucioncompra']['ivaimp2']+$row['Devolucioncompra']['ivaimp3'];	
		$suma_factura=$suma_factura + $base;
		$suma_iva=$suma_iva + $iva;
		$suma_bruto=$suma_bruto + $base + $iva;
			?>
		<tr>
		  <td><?php echo str_pad($row['Devolucioncompra']['coddevolucioncompra'], 10, "0", STR_PAD_LEFT);?></td>
		  <td><?php echo $row['Compra']['Proveedore']['rif'].'=>'.$row['Compra']['Proveedore']['descripcion']?></td>
		  <td><?php echo date("d-m-Y",strtotime($row['Devolucioncompra']['fdevolucion']));?></td>
		  <td><?php echo $deposito[$row['Compra']['coddeposito']];?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $base), 2, ',', '.');?></td>	<td align="right"><?php echo number_format(sprintf("%01.2f", $iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $base + $iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Devolucioncompra']['retencion']), 2, ',', '.');?></td>
		  <td><?php if($row['Compra']['estatus']=='P'){
				echo 'Contado';
			  }elseif($row['Compra']['estatus']=='A'){
				echo 'Credito';
			  }elseif($row['Compra']['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>
		  <td><?php echo $row['Compra']['nrodocumento'];?></td>
		 </tr> 		
		<?php		
		$CONT=$CONT+1;
		}
		?>		
<?php
$total_Devoluciones['factura']=$suma_factura;
$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;
$total_Devoluciones['factura_contado']=$suma_factura_contado;		
$total_Devoluciones['factura_credito']=$suma_factura_credito;	
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		
	</tfoot>
 </table>

	</div>
<!-- /.box-body -->
