<?php

$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'COMPRAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>		  
		  <th align="center" width="18%">Tipo de Pago</th>	
		  <th align="center" width="12%">Documento</th>
		  <th align="center" width="20%">Descripci&oacute;n</th>
		  <th align="center" width="7%">Fecha Emisi&oacute;n</th>		  		 		  
		  <th align="center" width="7%">Fecha Pago</th>		  		 		  
		  <th align="center">Monto</th>	
		  <th align="center">Retenci&oacute;n</th>	
		  <th align="center">Total</th>	
		</tr></thead>	
	';
//$texto=$texto.'<tbody>';
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;
//$texto="";
$BANCO='';$CONT=0;
foreach ($data as $row){
	
	
 if (substr($row[0]['tipomovimiento'], 1,7)!='COMPRAS'){	 	
	$suma_factura=$suma_factura + $row[0]['montopago'];
	if (substr($row[0]['tipomovimiento'], 7,9)!='RETENCION'){
		$pago = $row[0]['montopago'];$retencion = 0;
		$suma_bruto = $suma_bruto + $pago;
	}else{
		$retencion = $row[0]['montopago'];$pago = 0; 
		$suma_iva=$suma_iva + $retencion;
	}	
 if($BANCO!= $row[0]['proveedor']){
	$texto=$texto.'
	<tr>
	  <td width="12%" align="left"><b>'.$row[0]['rif'].'</b></td>	
	  <td align="left" colspan="6"><b>'.$row[0]['proveedor'].'</b></td>			  
	</tr>';
	
	}	
$texto=$texto.'
	<tr>	  
	  <td width="18%" align="left">'.substr($row[0]['tipomovimiento'], 1).'</td>	
		  <td width="12%" align="left">'.$row[0]['nrodocumento'].'</td>
		  <td width="20%" align="left">'.$row[0]['descripcion'].'</td>		  
		  <td width="7%">'.date("d-m-Y",strtotime($row[0]['femision'])).'</td>		  		  
		  <td width="7%">'.date("d-m-Y",strtotime($row[0]['fecha'])).'</td>		  		  
		  <td align="right">'.number_format(sprintf("%01.2f", $pago), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $retencion), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['montopago']), 2, ',', '.').'</td>	  
	</tr>';
	$CONT=$CONT+1;
	}
	$BANCO=$row[0]['proveedor'];
}	
$texto=$texto.'
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  				  
		  <td>&nbsp;</td>		  				  		  				  
		  <td align="right"><b>Totales:</b></td>	
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</b></td>		  
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</b></td>		  
		  <td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</b></td>		  
		</tr>';
$texto=$texto.'</table>';

//$this->tcpdf->core->Ln(2);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('pagos_de_compras.pdf', 'D');
?>
