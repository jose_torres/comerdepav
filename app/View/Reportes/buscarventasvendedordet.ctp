<?php
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';*/
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_costo=0;	
		$BANCO='';$CONT=0;
		foreach ($data as $row){
		
		if ($BANCO!= $row['vendedore']['descripcion']){
			if($CONT!=0){
		?>
		<tfoot>
		<tr>		  	
		   <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_costo=0;	
		?>
		<table class="table table-bordered">
		 <caption><label><?php echo 'Vendedor:&nbsp;'.$row['vendedore']['descripcion']; ?></label></caption> 
		<tr>
		  <th width="5%">Nro</th>
		  <th width="7%">Factura</th>
		  <th width="25%">Cliente</th>
		  <th width="7%">Fecha</th>
		  <th>Monto Neto</th>
		  <th>Contado</th>
		  <th>Cr&eacute;dito</th>		  
		  <th>Iva</th>
		  <th>Total</th>
		  <th>Costos</th>
		  <th>Utilidad</th>
		  <th>&#37; Utilidad</th>
		  <th>Tipo de Ventas</th>		  
		</tr>
		<?php
		}
		?>
		<?php		
		$suma_factura=$suma_factura + $row[0]['baseimp2'];
		$suma_iva=$suma_iva + $row[0]['ivaimp2'];
		$suma_bruto=$suma_bruto + $row[0]['baseimp2'] + $row[0]['ivaimp2'];
		$suma_costo=$suma_costo + $row[0]['costo'];	
			?>
		<tr>
		 <td><?php echo $row[0]['codventa'];?></td>
		  <td><?php echo $row[0]['numerofactura'];?></td>
		  <td><?php echo $row['cliente']['rif'].'=>'.$row['cliente']['descripcion']?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp2']), 2, ',', '.');?></td>
		  <td align="right"><?php 
		  if($row[0]['estatus']=='P' || substr($row[0]['numerofactura'], 0, 1)=='B'){
				echo number_format(sprintf("%01.2f", $row[0]['baseimp2']), 2, ',', '.');
				$suma_factura_contado=$suma_factura_contado + $row[0]['baseimp2'];				
		  }else{
				echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			  }			
		  ?></td>
		  <td align="right"><?php 
		  if($row[0]['estatus']=='A'  || substr($row[0]['numerofactura'], 0, 1)=='A'){
				echo number_format(sprintf("%01.2f", $row[0]['baseimp2']), 2, ',', '.');
				$suma_factura_credito=$suma_factura_credito + $row[0]['baseimp2'];
		  }else{
				echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			  }			
		  ?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['ivaimp2']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp2'] + $row[0]['ivaimp2']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['costo']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp2']+$row[0]['montoexento']-$row[0]['costo']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", ($row[0]['baseimp2']+$row[0]['montoexento']-$row[0]['costo'])/$row[0]['baseimp2']*100), 2, ',', '.');?></td>
		  <td><?php if($row[0]['estatus']=='P'){
				echo 'Contado';
			  }elseif($row[0]['estatus']=='A'){
				echo 'Credito';
			  }elseif($row[0]['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>		  
		 </tr> 
		
		<?php
		$BANCO=$row['vendedore']['descripcion'];
		$CONT=$CONT+1;
		}
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>		  	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>	  
		  <td>&nbsp;</td>	  
		  <td>&nbsp;</td>	  
		  <td>&nbsp;</td>	  
		</tr>
		</tfoot>
 </table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	</div>
<!-- /.box-body -->
