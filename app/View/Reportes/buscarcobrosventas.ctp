<?php
	//echo 'Esto es Buscar Ventas';
	//print_r($data);
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<h3><?php echo $titulo; ?></h3>
	<?php
		$suma_factura=0;$suma_factura_retencion=0;$suma_factura_pagado=0; $suma_iva=0; $suma_bruto=0;$totales['neto']=$totales['retencion']=$totales['total']=0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){
		if ($BANCO!= $row[0]['fechapago']){
			if($CONT!=0){
		?>
		<tfoot>
		<tr>
		  <td>&nbsp;</td>
		<!--  <td>&nbsp;</td>	-->
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		 
		  <td>&nbsp;</td>		
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_pagado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_retencion), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>  
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0;$suma_factura_retencion=0;$suma_factura_pagado=0; $suma_iva=0; $suma_bruto=0;	
		?>
		<table class="table table-bordered">
		 <caption><label><?php echo 'DIA:&nbsp;'.date("d-m-Y",strtotime($row[0]['fechapago'])); ?></label></caption> 
		<tr>
		  <th width="20%">Cliente</th>	
		<!--  <th width="10%">Operaci&oacute;n</th> -->
		  <th width="7%">N&uacute;mero</th>
		  <th width="25%">Descripcion</th>
		  <th width="7%">Fecha Emisi&oacute;n</th>
		  <th width="7%">Fecha Cobranza</th>
		  <th width="7%">Dias</th>
		  <th width="7%">Dias Venc.</th>
		  		  
		  <th>Documento</th>
		  	  
		  <th>Banco</th>
		  <th>Monto</th>
		  <th>Retenci&oacute;n</th>
		  <th>Total</th>
		</tr>
		<?php
		}
		?>
		<?php
		if($row[0]['tipomovimientopago']=='ABO'){
			$retencion=0;
			$pagado=$row[0]['monto_pagado'];
		}else{
			$retencion=$row[0]['retencion'];			
			//$pagado=$row[0]['baseimp2'] + ($row[0]['ivaimp2'] - $row[0]['retencion']);	
			$pagado=$row[0]['monto_pagado'] ;					
		}	
		$suma_factura_retencion=$suma_factura_retencion+$retencion;
		$totales['retencion']=$totales['retencion'] + $retencion;	
		
		$suma_factura_pagado=$suma_factura_pagado+$pagado /*+ $retencion*/;
		$totales['total']=$totales['total'] + $pagado /*+ $retencion*/;
	
		$suma_factura=$suma_factura + $pagado - $retencion;
		$totales['neto']=$totales['neto'] + $pagado - $retencion;
		//if($row[0]['tipomovimientopago']=='ABO'){
		//	$retencion=$row[0]['retencion'];
		//	$suma_factura_retencion=$suma_factura_retencion+$retencion;
		//	$totales['retencion']=$totales['retencion'] + $retencion;
		//}else{
			
		//}		
			?>
		<tr>
		  <td><?php echo $row[0]['descripcion'];?></td>	
	<!--	  <td><?php echo $row[0]['tipopago'];?></td> -->
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['concepto'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td> 
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td><?php echo $row[0]['dias'];?></td> 
		  <td><?php 
				if ($row[0]['dias_venc']<0 || $row[0]['dias_venc']==''){
					echo 0;
				}else{
					echo $row[0]['dias_venc'];
				}		
		  ?></td> 
		  <td><?php echo $row[0]['numerofactura'];?></td>		  
		 
		  <td><?php echo $row[0]['banco'];?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $pagado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $retencion), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $pagado-$retencion), 2, ',', '.');?></td>	  
		  
		 </tr> 
		
		<?php
		$BANCO=$row[0]['fechapago'];
		$CONT=$CONT+1;
		}
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  	
		<!--  <td>&nbsp;</td>	-->
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		 
		  <td>&nbsp;</td>			  	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_pagado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_retencion), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>  
		</tr>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>

		  <td>&nbsp;</td>	
		  
		  <td>&nbsp;</td>	
		  <td align="right" colspan="2"><label>Total General:</label></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales['total']), 2, ',', '.');?></td>			  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $totales['retencion']), 2, ',', '.');?></td>
		   <td align="right"><?php echo number_format(sprintf("%01.2f", $totales['neto']), 2, ',', '.');?></td> 
		</tr>
		</tfoot>
 </table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	</div>
<!-- /.box-body -->
