<?php
	//echo 'Esto es Buscar Ventas';
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';	*/
?>
<!-- /.box-header -->
<table class="table table-bordered table-hover">
		<tr>
		  <th>Fecha</th>
		  <th>Contado</th>
		  <th>Venta Neta</th>
		  <th>Impuesto</th>
		  <th>Cr&eacute;dito</th>
		  <th>Cobros</th>
		  <th>N. Cr&eacute;dito/Descuento</th>
		</tr>	
		<?php
		$suma_facturas['contado']=$suma_facturas['montonetocredito']=$suma_facturas['montoivacredito']=$suma_facturas['montototalcredito']=$suma_facturas['cobros']=$suma_facturas['notacredito']=0;$CONT=0;	
		foreach ($data as $row){			
			$suma_facturas['contado']=$suma_facturas['contado'] + $row[0]['contado'];
			$suma_facturas['montonetocredito']=$suma_facturas['montonetocredito'] + $row[0]['montonetocredito'];
			$suma_facturas['montoivacredito']=$suma_facturas['montoivacredito'] + $row[0]['montoivacredito'];
			$suma_facturas['montototalcredito']=$suma_facturas['montototalcredito'] + $row[0]['montototalcredito'];
			$suma_facturas['cobros']=$suma_facturas['cobros'] + $row[0]['cobros'] + $row[0]['cobrosdirectos'];
			$suma_facturas['notacredito']=$suma_facturas['notacredito'] + $row[0]['notacredito'];
			?>
		<tr>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montonetocredito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montoivacredito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montototalcredito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['cobros'] + $row[0]['cobrosdirectos']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['notacredito']), 2, ',', '.');?></td>
		 </tr> 
		<?php
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_facturas['contado']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_facturas['montonetocredito']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_facturas['montoivacredito']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_facturas['montototalcredito']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_facturas['cobros']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_facturas['notacredito']), 2, ',', '.');?></td>		  	  
		</tr>
		</tfoot>
</table>
<!-- /.box-body -->
