<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'INVENTARIO');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);

$this->tcpdf->core->setEqualColumns(2);
// Colors, line width and bold font
        $this->tcpdf->core->SetFillColor(255, 255, 255);
        $this->tcpdf->core->SetTextColor(0);
        //$this->tcpdf->core->SetDrawColor(128, 0, 0);
        $this->tcpdf->core->SetLineWidth(0.3);
        $this->tcpdf->core->SetFont('', 'B');
        
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0; $DEP='';       
// Header
	foreach($data as $row) {
				
		if($BANCO!= $row[0]['codigo'] && $CONT_FILA>0){
			 $this->tcpdf->core->SetTextColor(0);
			$this->tcpdf->core->SetFont('', 'B');
			$w = array(57, 17);
			$this->tcpdf->core->Cell($w[0], 7, 'Total:', 'T', 0, 'R', 1);
			$this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.'), 'T', 0, 'R', 1);
			$this->tcpdf->core->Ln();
			//$this->tcpdf->core->Ln();
			
			$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
		}
		$suma_factura=$suma_factura + $row[0]['cantidad'];
		
		if($DEP!= $row['dp']['codigo'] ){
			 $this->tcpdf->core->SetTextColor(0);
			$this->tcpdf->core->SetFont('', 'B');
			$this->tcpdf->core->Cell(74, 7, $row['dp']['codigo'].' '.$row[0]['descripcion'], 'B', 0, 'L', 1);
			$this->tcpdf->core->Ln();			
			//$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
		}
		if($BANCO!= $row[0]['codigo']){	
			$this->tcpdf->core->Cell(74, 7, $row[0]['codigo'].' '.$row[0]['nombre'], 0, 0, 'L', 1);
			$this->tcpdf->core->Ln();

			$this->tcpdf->core->SetFont('', 'B');
			$header=array('Depto','Deposito','Unidad','Cant.');
			$w = array(20, 17, 20, 17);
			$num_headers = count($header);
			for($i = 0; $i < $num_headers; ++$i) {
				$this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
			}
			$this->tcpdf->core->Ln();
			$CONT_FILA=0;
		}
        // Color and font restoration
        //$this->tcpdf->core->SetFillColor(224, 235, 255);
        $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('');
        // Data
        $fill = 0;
        //foreach($data as $row) {
           $this->tcpdf->core->Cell($w[0], 6, $row['dp']['codigo'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[1], 6, $row['dep']['descripcion'], '', 0, 'L', $fill);
            $this->tcpdf->core->Cell($w[2], 6, $row[0]['unidad'], '', 0, 'R', $fill);
            $this->tcpdf->core->Cell($w[3], 6, number_format(sprintf("%01.2f", $row[0]['cantidad']), 2, ',', '.'), '', 0, 'R', $fill);
            $this->tcpdf->core->Ln();
            //$fill=!$fill;
            $BANCO=$row[0]['codigo'];
            $DEP=$row['dp']['codigo'];
	$CONT_FILA=$CONT_FILA+1;
	$CONT=$CONT+1;
    }
    
    if($CONT>0){
			 $this->tcpdf->core->SetTextColor(0);
			$this->tcpdf->core->SetFont('', 'B');
			$w = array(57, 17);
			$this->tcpdf->core->Cell($w[0], 7, 'Total:', 'T', 0, 'R', 1);
			$this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.'), 'T', 0, 'R', 1);
			$this->tcpdf->core->Ln();
	}
    $this->tcpdf->core->Cell(array_sum($w), 0, '', '');
$texto="";
/*$texto='<table border="0" cellpadding="1"  align="center" cellspacing="0">
	<thead><tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>			  
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>		  
		</tr>
	</thead>	
	';*/
//$texto=$texto.'<tbody>';
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0;
foreach ($data as $row){
	 	 	
	if($BANCO!= $row[0]['codigo'] && $CONT_FILA>0){
	$texto=$texto.'
		<tr>
		  <td width="40%">&nbsp;</td>	
		  <td width="20%">&nbsp;</td>			  
		  <td width="14%" align="right"><b>Totales:</b></td>	
		  <td width="20%" align="right">'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</td> 	  		  
		</tr>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>			  
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>		  
		</tr></table>
		';
		$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
	}	 	 	
	 	 	
	$suma_factura=$suma_factura + $row[0]['cantidad'];
	
 if($BANCO!= $row[0]['codigo']){
	$texto=$texto.'
	<table border="0" cellpadding="1"  align="center" cellspacing="0">
	<thead><tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>			  
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>		  
		</tr>
	</thead>
	<tr>
	  <td width="30%" align="left"><b>'.$row[0]['codigo'].'</b></td>	
	  <td width="64%" align="left" colspan="3"><b>'.$row[0]['nombre'].'</b></td>
	</tr>
	<tr>		  
		  <th width="30%" align="center">Depto</th>
		  <th width="20%" align="center">Deposito</th>
		  <th width="14%" align="center">Unidad</th>
		  <th width="20%" align="center">Cant.</th>	 	
	</tr></table>
	';
	$CONT_FILA=0;
	}	
$texto=$texto.'
	<tr>	  
	     <td width="40%" align="left">'.$row['dp']['codigo'].' '.$row[0]['descripcion'].'</td>
		  <td width="20%" align="left">'.$row['dep']['descripcion'].'</td>
		  <td width="14%">'.$row[0]['unidad'].'</td>		  
		  <td width="20%" align="right">'.number_format(sprintf("%01.2f", $row[0]['cantidad']), 2, ',', '.').'</td>  
	</tr>';
	$BANCO=$row[0]['codigo'];
	$CONT_FILA=$CONT_FILA+1;
	$CONT=$CONT+1;
}	
$texto=$texto.'
		<tr>
		  <td width="40%">&nbsp;</td>	
		  <td width="20%">&nbsp;</td>			  
		  <td width="14%" align="right"><b>Totales:</b></td>	
		  <td width="20%" align="right">'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</td> 	  		  
		</tr>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>			  
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>		  
		</tr>
		';
$texto=$texto.'</table>';

//$this->tcpdf->core->Ln(2);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
//$this->tcpdf->core->SetX(2);
//$this->tcpdf->core->setEqualColumns(2);
//$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');
//$this->tcpdf->core->PrintChapter(2, 'LOREM IPSUM [HTML]', $texto, true);

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('existencia.pdf', 'D');
?>
