<?php
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',6);
// Devolucion en Ventas
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>				 
		  <th width="7%" align="center"><strong>Devoluci&oacute;n</strong></th>
		  <th width="7%" align="center"><strong>Factura</strong></th>		  
		  <th width="22%" align="center"><strong>Cliente</strong></th>
		  <th width="7%" align="center"><strong>Fecha</strong></th>		  
		  <th align="right" width="10%"><strong>Monto Neto</strong></th>
		  <th align="right" width="7%"><strong>Iva</strong></th>
		  <th align="right" width="10%"><strong>Total</strong></th>
		  <th width="1%">&nbsp;</th>
		  <th align="center" width="23%"><strong>Observaci&oacute;n</strong></th>
		  <th align="center" width="7%"><strong>Vendedor</strong></th>
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0; $suma_costo=0;	
foreach ($data_dev as $row){	
	  $base =	$row[0]['baseimp2'] + $row[0]['baseimp1'] + $row[0]['baseimp3'];
	  $iva = $row[0]['ivaimp2'] + $row[0]['ivaimp1'] + $row[0]['ivaimp3'];
	   $suma_factura=$suma_factura + $base;
		$suma_iva=$suma_iva + $iva;
		$suma_bruto=$suma_bruto + $base + $iva;
		$suma_costo=$suma_costo + $row[0]['costo'];	
		$basexcien= 0;
		if ($base>0){
			$basexcien = ($base+$row[0]['montoexento']-$row[0]['costo'])/$base*100;
		}
		
	if($row[0]['estatus']=='P'){
		$tipo_compra='Contado';
	  }elseif($row[0]['estatus']=='A'){
		$tipo_compra='A Credito';
	  }elseif($row[0]['estatus']=='E'){
		$tipo_compra='Devuelto';
	  }
$texto=$texto.'
	<tr>
		  <td width="7%" align="left">'.str_pad($row[0]['coddevolucion'], 9, "0", STR_PAD_LEFT).'</td>
		   <td  width="7%" align="left">'.$row[0]['numerofactura'].'</td>
		  <td width="22%" align="left">'.$row['cliente']['descripcion'].'</td>
		  <td width="7%">'.date("d-m-Y",strtotime($row[0]['fecha'])).'</td>
		  <td align="right" width="10%">'.number_format(sprintf("%01.2f", $base), 2, ',', '.').'</td>
		  <td align="right" width="7%">'.number_format(sprintf("%01.2f", $iva), 2, ',', '.').'</td>
		  <td align="right" width="10%">'.number_format(sprintf("%01.2f", $base + $iva), 2, ',', '.').'</td>
		  <td width="1%">&nbsp;</td>
		  <td align="left" width="23%">'.$row[0]['observacion'].'</td>
		  <td align="left" width="7%">'.$row['vendedore']['descripcion'].'</td>
	</tr>';	
}
$porc_dev=0;
if ($suma_factura>0){
$porc_dev=($suma_factura-$suma_costo)/$suma_factura*100;
}			
$texto=$texto.'
	<tr>	
	<td width="7%" >&nbsp;</td>	
	<td width="7%" >&nbsp;</td>	
	<td width="22%">&nbsp;</td>
	<td width="7%" align="right"><b>TOTALES:</b></td>	
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</label> </td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</label></td>		  
	<td >&nbsp;</td>
	<td >&nbsp;</td>			
	<td >&nbsp;</td>			
	</tr>';
$texto=$texto.'</table>';
$total_Devoluciones['factura']=$suma_factura;$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;$total_Devoluciones['costo']=$suma_costo;
$this->tcpdf->core->writeHTML('<b>DEVOLUCIONES</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('devolucionesventas.pdf', 'D');
?>
