<?php

$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'COMPRAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>		
		  <th width="8%" align="center">Documento</th>
		  <th width="20%" align="center">Proveedores</th>
		  <th align="center">Fecha Emisi&oacute;n</th>
		  <th align="center">Dep&oacute;sitos</th>
		  <th align="center">Monto Neto</th>
		  <th align="center">Iva</th>
		  <th align="center">Total</th>
		  <th align="center">Retenci&oacute;n</th>		  				
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;
foreach ($data as $row){	
	$suma_factura=$suma_factura + $row['Compra']['baseimp2'];
	$suma_iva=$suma_iva + $row['Compra']['ivaimp2'];
	$suma_bruto=$suma_bruto + $row['Compra']['baseimp2'] + $row['Compra']['ivaimp2'];
	$suma_retencion = $suma_retencion + $row['Compra']['retencion'];	
	if($row['Compra']['estatus']=='P'){
		$tipo_compra='Contado';
	  }elseif($row['Compra']['estatus']=='A'){
		$tipo_compra='A Credito';
	  }elseif($row['Compra']['estatus']=='E'){
		$tipo_compra='Devuelto';
	  }
$texto=$texto.'
	<tr>
	  <td width="8%" align="left">'.$row['Compra']['nrodocumento'].'</td>
	  <td width="20%" align="left">'.$row['Proveedore']['descripcion'].'</td>		  
	  <td>'.date("d-m-Y",strtotime($row['Compra']['femision'])).'</td>
	  <td>'.$deposito[$row['Compra']['coddeposito']].'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row['Compra']['baseimp2']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row['Compra']['ivaimp2']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row['Compra']['baseimp2'] + $row['Compra']['ivaimp2']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row['Compra']['retencion']), 2, ',', '.').'</td>	  
	</tr>';	
}	
$texto=$texto.'
	<tr>	
	<td width="8%">&nbsp;</td>	
	<td width="20%">&nbsp;</td>
	<td>&nbsp;</td>		
	<td align="right"><b>TOTALES:</b></td>	
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</label> </td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</label></td>		  
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.').'</label></td>		
	</tr>';
//$texto=$texto.'</tbody>';
$texto=$texto.'</table>';

//$this->tcpdf->core->Ln(2);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
//$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('compras.pdf', 'D');
?>
