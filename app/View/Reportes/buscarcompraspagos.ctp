<?php
	//print_r($data);
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<h3><?php echo $titulo; ?></h3>
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){

		if ($BANCO!= $row[0]['proveedor']){
			
			if($CONT!=0){
		?>
		<tfoot>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  				  	  				  
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></label></td>		  
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></label></td>		  
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></label></td>		  
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;	
		?>
		<table class="table table-bordered">
		 <caption><label><?php echo $row[0]['rif'].' '.$row[0]['proveedor']; ?></label></caption> 
		<tr>
		  <th width="15%">Tipo de Pago</th>	
		  <th width="10%">Documento</th>
		  <th width="25%">Descripci&oacute;n</th>		  		 		  
		  <th width="5%">Fecha</th>		  		 		 
		  <th>Monto</th>	
		  <th>Retenci&oacute;n</th>	
		  <th>Total</th>	
		</tr>
		<?php
		}
		?>
		<?php		
		if (substr($row[0]['tipomovimiento'], 1,7)!='COMPRAS'){
		$suma_factura=$suma_factura + $row[0]['montopago'];	
		if (substr($row[0]['tipomovimiento'], 7,9)!='RETENCION'){
			$pago = $row[0]['montopago'];$retencion = 0;
			$suma_bruto = $suma_bruto + $pago;
		}else{
			$retencion = $row[0]['montopago'];$pago = 0; 
			$suma_iva=$suma_iva + $retencion;
		}	
			?>
		<tr>
		  <td><?php echo substr($row[0]['tipomovimiento'], 1);  ?></td>	
		  <td><?php echo $row[0]['nrodocumento'];?></td>
		  <td><?php echo $row[0]['descripcion'];?></td>		  		  		  
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>		  		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $pago), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $retencion), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montopago']), 2, ',', '.');?></td>
		  
		 </tr> 
		
		<?php
		
		$CONT=$CONT+1;		
		}
		$BANCO=$row[0]['proveedor'];
		}
		
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		   		   
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></label></td>		  
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></label></td>		  
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></label></td>		  
		</tr>
		</tfoot>
 </table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
</div>
<!-- /.box-body -->
