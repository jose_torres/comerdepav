<?php
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';*/
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_costo=0;	$suma_exento=0;
		$BANCO='';$CONT=0;
		foreach ($data as $row){
		
		if ($BANCO!= $row['vendedore']['descripcion']){
			if($CONT!=0){
		?>
		<tfoot>
		<tr>		  	
		   <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td>&nbsp;</td>		  
		   <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura +$suma_exento-$suma_costo), 2, ',', '.');?></td>	 	  		  
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_costo=0;$suma_exento=0;	
		?>
		<table class="table table-bordered">
		 <caption><label><?php echo 'Vendedor:&nbsp;'.$row['vendedore']['descripcion']; ?></label></caption> 
		<tr>
		  <th width="5%">Nro</th>
		  <th width="30%">Cliente</th>
		  <th width="7%">Factura</th>
		  <th width="7%">F/Emisi&oacute;n</th>
		  <th width="7%">F/Vcto.</th>
		  <th width="7%">F/Cobro.</th>
		  <th width="7%">D&iacute;as</th>
		  <th>Monto Neto</th>
		  <th>&#37; Utilidad</th>
		  <th>Utilidad</th>
		</tr>
		<?php
		}
		?>
		<?php		
		$suma_factura=$suma_factura + $row[0]['baseimp2'];
		$suma_iva=$suma_iva + $row[0]['ivaimp2'];
		$suma_bruto=$suma_bruto + $row[0]['baseimp2'] + $row[0]['ivaimp2'];
		$suma_costo=$suma_costo + $row[0]['costo'];
		$suma_exento=$suma_exento + $row[0]['montoexento'];
			?>
		<tr>
		 <td><?php echo $row[0]['codventa'];?></td>
		  <td><?php echo $row['cliente']['rif'].'=>'.$row['cliente']['descripcion']?></td>
		  <td><?php echo $row[0]['numerofactura'];?></td>		 
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fvencimiento']));?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechacobro']));?></td>
		  <td><?php echo $row[0]['dias'];?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp2']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", ($row[0]['baseimp2']+$row[0]['montoexento']-$row[0]['costo'])/$row[0]['baseimp2']*100), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['baseimp2']+$row[0]['montoexento']-$row[0]['costo']), 2, ',', '.');?></td>	  
		 </tr> 
		
		<?php
		$BANCO=$row['vendedore']['descripcion'];
		$CONT=$CONT+1;
		}
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>		  	
		   <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>		  
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td>&nbsp;</td>		  
		   <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura +$suma_exento-$suma_costo), 2, ',', '.');?></td>	  
		</tr>
		</tfoot>
 </table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	</div>
<!-- /.box-body -->
