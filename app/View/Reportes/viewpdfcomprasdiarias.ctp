<?php

$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'COMPRAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>		
		  <th width="8%" align="center">Documento</th>
		  <th width="20%" align="center">Proveedores</th>
		  <th width="7%" align="center">Fecha Emisi&oacute;n</th>
		  <th width="7%" align="center">Dep&oacute;sitos</th>
		  <th align="center">Monto Neto</th>
		  <th>Contado</th>
		  <th>Cr&eacute;dito</th>
		  <th align="center">Iva</th>
		  <th align="center">Total</th>
		  <th align="center">Retenci&oacute;n</th>		  				
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;$suma_factura_contado=$suma_factura_credito=0;
foreach ($data as $row){
	$base = ($row['Compra']['baseimp1']+$row['Compra']['baseimp2']+$row['Compra']['baseimp3']);
	$iva =	$row['Compra']['ivaimp1']+$row['Compra']['ivaimp2']+$row['Compra']['ivaimp3'];
	$suma_factura=$suma_factura + $base;
	$suma_iva=$suma_iva + $iva;
	$suma_bruto=$suma_bruto + $base + $iva;
	$suma_retencion = $suma_retencion + $row['Compra']['retencion'];		
	$contado=0;	$credito=0;
	if($row['Compra']['estatus']=='P'){
		$tipo_compra='Contado';$contado=$base;
		$suma_factura_contado=$suma_factura_contado + $base;
	  }elseif($row['Compra']['estatus']=='A'){
		$tipo_compra='Credito';$credito=$base;
		$suma_factura_credito=$suma_factura_credito + $base;
	  }elseif($row['Compra']['estatus']=='E'){
		$tipo_compra='Devuelto';
	  }  
$texto=$texto.'
	<tr>
	  <td width="8%" align="left">'.$row['Compra']['nrodocumento'].'</td>
	  <td width="20%" align="left">'.$row['Proveedore']['descripcion'].'</td>		  
	  <td width="7%" align="center">'.date("d-m-Y",strtotime($row['Compra']['femision'])).'</td>
	  <td width="7%">'.$deposito[$row['Compra']['coddeposito']].'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $base), 2, ',', '.').'</td>	  
	  <td align="right">'.number_format(sprintf("%01.2f", $contado), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $credito), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $iva), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $base + $iva), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row['Compra']['retencion']), 2, ',', '.').'</td>	  
	</tr>';	
}	
$texto=$texto.'
	<tr>	
	<td width="8%">&nbsp;</td>	
	<td width="20%">&nbsp;</td>
	<td  width="7%">&nbsp;</td>		
	<td align="right"><b>TOTALES:</b></td>	
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</label> </td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</label></td>		  
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.').'</label></td>		
	</tr>';
//$texto=$texto.'</tbody>';
$texto=$texto.'</table>';
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;		
$total_ventas['factura_contado']=$suma_factura_contado;		
$total_ventas['factura_credito']=$suma_factura_credito;	
//$this->tcpdf->core->Ln(2);
$this->tcpdf->core->writeHTML('<b>COMPRAS</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');
//Devoluciones en Compras
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>				 
		  <th width="8%" align="center">Devoluci&oacute;n</th>
		  <th width="20%" align="center">Proveedor</th>
		  <th width="7%" align="center">Fecha Devoluci&oacute;n</th>
		  <th width="7%" align="center">Deposito</th>
		  <th align="center">Monto Neto</th>
		  <th align="center">Contado</th>
		  <th align="center">Cr&eacute;dito</th>
		  <th align="center">Iva</th>
		  <th align="center">Total</th>
		  <th align="center">Retenci&oacute;n</th>
		  <th align="center">Fact. Afecta</th>	  				
	</tr></thead>
	';
$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
foreach ($data_dev as $row){
	$base = ($row['Devolucioncompra']['baseimp1']+$row['Devolucioncompra']['baseimp2']+$row['Devolucioncompra']['baseimp3']);
	$iva =	$row['Devolucioncompra']['ivaimp1']+$row['Devolucioncompra']['ivaimp2']+$row['Devolucioncompra']['ivaimp3'];
	$suma_factura=$suma_factura + $base;
	$suma_iva=$suma_iva + $iva;
	$suma_bruto=$suma_bruto + $base + $iva;
	$contado=0;	$credito=0;
	if($row['Compra']['estatus']=='P'){
		$tipo_compra='Contado';$contado=$base;
		$suma_factura_contado=$suma_factura_contado + $base;
	  }elseif($row['Compra']['estatus']=='A'){
		$tipo_compra='Credito';$credito=$base;
		$suma_factura_credito=$suma_factura_credito + $base;
	  }elseif($row['Compra']['estatus']=='E'){
		$tipo_compra='Devuelto';
	  }
$texto=$texto.'
	<tr>
		  <td width="8%" align="left">'.str_pad($row['Devolucioncompra']['coddevolucioncompra'], 9, "0", STR_PAD_LEFT).'</td>
		  <td width="20%" align="left">'.$row['Compra']['Proveedore']['descripcion'].'</td>
		  <td width="7%">'.date("d-m-Y",strtotime($row['Devolucioncompra']['fdevolucion'])).'</td>
		  <td width="7%">'.$deposito[$row['Compra']['coddeposito']].'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $base), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $contado), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $credito), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $iva), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $base + $iva), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row['Compra']['retencion']), 2, ',', '.').'</td>
		  <td width="9%">'.$row['Compra']['nrodocumento'].'</td>
	</tr>';	
}	
$total_Devoluciones['factura']=$suma_factura;$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;$total_Devoluciones['factura_contado']=$suma_factura_contado;$total_Devoluciones['factura_credito']=$suma_factura_credito;
$texto=$texto.'
	<tr>	
	<td width="8%" >&nbsp;</td>	
	<td width="20%" >&nbsp;</td>	
	<td width="7%">&nbsp;</td>	
	<td width="7%" align="right"><b>TOTALES:</b></td>	
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</label></td>
	<td align="right">'.number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.').'</td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</label> </td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</label></td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>			  			
	</tr>
	<tr>	
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td>&nbsp;</td>	
	<td>&nbsp;</td>
	<td>&nbsp;</td>			  			
	</tr>
	<tr>
	  <td width="8%">&nbsp;</td>	  
	  <td align="right" colspan="3" ><b>TOTALES (VENTAS - DEVOLUCIONES):</b></td>	
	  <td align="right"><b>'.number_format(sprintf("%01.2f",$total_ventas['factura']- $total_Devoluciones['factura']), 2, ',', '.').'</b></td>
	  <td align="right"><b>'.number_format(sprintf("%01.2f",$total_ventas['factura_contado']- $total_Devoluciones['factura_contado']), 2, ',', '.').'</b></td>
	  <td align="right"><b>'.number_format(sprintf("%01.2f",$total_ventas['factura_credito']- $total_Devoluciones['factura_credito']), 2, ',', '.').'</b></td>
	  <td align="right"><b>'.number_format(sprintf("%01.2f", $total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.').'</b></td>
	  <td align="right"><b>'.number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.').'</b></td>
	  <td>&nbsp;</td>
	  <td>&nbsp;</td>		  
	</tr>
	';
$texto=$texto.'</table>';

$this->tcpdf->core->writeHTML('<b>DEVOLUCIONES</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

/*$this->tcpdf->core->writeHTML('<b>TOTALES (COMPRAS - DEVOLUCIONES)</b>', true, false, true, false,'L');

$this->tcpdf->core->MultiCell(35,1,' '.number_format(sprintf("%01.2f",$total_ventas['factura']- $total_Devoluciones['factura']), 2, ',', '.'),0,'R',false,0,64,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(35,1,' '.number_format(sprintf("%01.2f",$total_ventas['factura_contado']- $total_Devoluciones['factura_contado']), 2, ',', '.'),0,'R',false,0,84,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(35,1,' '.number_format(sprintf("%01.2f",$total_ventas['factura_credito']- $total_Devoluciones['factura_credito']), 2, ',', '.'),0,'R',false,0,104,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,'  '.number_format(sprintf("%01.2f", $total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.'),0,'R',false,0,129,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,' '.number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.'),0,'R',false,0,149,'',true,0, false,true, 0,'M',false);
*/

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('comprasdiarias.pdf', 'D');
?>
