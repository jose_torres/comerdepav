<?php
	echo $this->Form->input('Reporte.cliente_id',array('multiple' => true,'div'=>false,'options'=>$cliente,'label'=>false,'data-placeholder'=>"Seleccione Clientes",'class'=>"form-control select2","style"=>"width: 100%;"));
?>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$("#ReporteClienteId").select2({
		language: "es"
		});  

  });
</script>
