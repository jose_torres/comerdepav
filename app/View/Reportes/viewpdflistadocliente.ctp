<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>
		  <th width="9%" align="center">Rif</th>
		  <th width="30%" align="center">Nombre</th>
		  <th width="7%" align="center">Fecha Ingreso</th>
		  <th width="40%" align="center">Direccion</th>
		  <th width="10%" align="center">Telefono</th>		  
		  <th width="10%" align="center">Vendedor</th>		  
		</tr></thead>
	';
//$texto=$texto.'<tbody>';
/*foreach ($data as $row){	
		
$texto=$texto.'
	<tr>
	  <td width="9%">'.$row['Cliente']['rif'].'</td>
	  <td width="30%">'.$row['Cliente']['descripcion'].'</td>
	  <td width="7%">'.date("d-m-Y",strtotime($row['Cliente']['fechadeinicio'])).'</td>
	  <td width="40%">'.$row['Cliente']['direccion'].'&nbsp;&nbsp;'.$row['Cliente']['direccion2'].'</td>
	  <td width="10%">'.$row['Cliente']['telefonos'].'</td>	
	  <td width="10%">'.$row['Vendedore']['descripcion'].'</td>
	</tr>';	
}*/	

$texto=$texto.'</table>';
//$this->tcpdf->core->writeHTML('<b>CLIENTES</b>', true, false, true, false, 'L');
//$this->tcpdf->core->SetX(2);
//$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');
// Compras
	$this->tcpdf->core->SetX(2);
	$this->tcpdf->core->SetFillColor(255, 255, 255);
	$this->tcpdf->core->SetTextColor(0);
	$this->tcpdf->core->SetFont('helvetica', 'B',7);
	$header=array('Rif','Nombre','Fec. Ingreso','Direccion','Telefono');
	$w = array(10, 80, 10, 80, 30);
	$num_headers = count($header);
	for($i = 0; $i < $num_headers; ++$i) {
		$this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
	}
	$this->tcpdf->core->Ln();
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('Listado_de_Clientes.pdf', 'D');
?>
