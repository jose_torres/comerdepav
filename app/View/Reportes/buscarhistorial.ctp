<?php
     echo $this->Html->css('datatables/dataTables.bootstrap.css');
     echo $this->Html->css('datatables/rowGroup.dataTables.min.css');
?>
<style>
    table.dataTable tr.group-end td {
    text-align: right;
    font-weight: normal;
}
</style>
<!-- /.box-header -->
<div class="box">
<div class="box-header">
  <h3 class="box-title">Listado de Operaciones de Inventario</h3>
</div>
<table class="display table table-bordered" width="95%" id="example">
    <caption><h4><?php echo $titulo; ?></h4></caption> 
    <thead>	
        <tr>		  
            <th width="25%">Fecha</th>
            <th width="7%">Codigo</th>		  
            <th width="25%">Descripci&oacute;n</th> 
            <th align="right">Oper</th>
            <th align="right">N&uacute;mero</th>
            <th align="right">Nombre</th>
            <th align="right">Cantidad</th>            		              
            <th align="right">Costo</th>
        </tr>
    </thead>	
    <tbody>
<?php
       $suma_factura=0;$suma_factura_costo=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
       $BANCO='';$CONT=0;$IVA=0;	 $suma_cantidad = 0;	
       foreach ($data as $row){	
       //$suma_factura=$suma_factura + ($row[0]['cantidad'] * $row[0]['costo']);       
       //$suma_iva=$suma_iva + $row[0]['iva'];       
       //$suma_bruto=$suma_bruto + $row[0]['venta'] + $row[0]['iva'];
       $suma_cantidad = $suma_cantidad + $row[0]['cantidad'];
       $suma_factura_costo = $suma_factura_costo + $row[0]['costo_total'];        
?>
       <tr>
         <td><?php echo ' Fecha: '.date("d-m-Y",strtotime($row[0]['fecha']));?></td>  
         <td><?php echo $row[0]['codigo'];?></td>
         <td><?php echo $row[0]['nombre'];?></td>
         <td><?php echo $row[0]['tipo'];?></td>
         <td><?php echo $row[0]['numero'];?></td>
         <td><?php echo $row[0]['descripcion'];?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['cantidad']), 2, ',', '.');?></td>         
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['costo_total']), 2, ',', '.');?></td>   
 
        </tr> 		
<?php		
        $CONT=$CONT+1;
        //$BANCO= $row[0]['codproduct'];
        }
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;	
$UTIL = 0;
if($suma_factura>0){
	$UTIL = ($suma_factura-$suma_factura_costo)/$suma_factura*100;
}	
?>

    </tbody>
    <tfoot>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>      	
            <td>&nbsp;</td>
            <td>&nbsp;</td>  				  
            <td align="right"><label>Totales:</label></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_cantidad), 2, ',', '.');?></td>             
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.');?></td>            
        </tr>
    </tfoot>
</table>
 
</div>
<?php
    //echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('funciones.js');
    echo $this->Html->script('plugins/datatables/jquery-3.3.1.js');
    echo $this->Html->script('bootstrap.min.js');
    echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
    echo $this->Html->script('plugins/datatables/dataTables.rowGroup.min.js');
    //echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
    $(document).ready(function() {
    var groupColumn = 0;
        
    $('#example').DataTable( {
        "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		},
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],        
        order: [[0, 'asc']],
        rowGroup: {
            
            endRender: function ( rows, group ) {
                //return group +' ('+rows.count()+')';
                var totalCant = rows
                    .data()
                    .pluck(6)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                totalCant = $.fn.dataTable.render.number('.', ',', 2, 'Unid. ').display( totalCant );
                
                 var salaryAvg = rows
                    .data()
                    .pluck(7)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                salaryAvg = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( salaryAvg );
                
                
                 return $('<tr/>')
                    
                    .append( '<td><strong></strong></td>' )
                    .append( '<td><strong></strong></td>' )
                    .append( '<td><strong></strong></td>' ) 
                    .append( '<td colspan="2"><strong>Totales por:</strong></td>' )
                    
                    .append( '<td><strong>'+totalCant+'</strong></td>' )                   
                    .append( '<td><strong>'+salaryAvg+'</strong></td>' )
                    
                    ;
                    
            },
            dataSrc:0
        }
    } );
} );  
 
</script>	
<!-- /.box-body -->
