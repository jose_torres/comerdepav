<?php	
	//print_r($data);
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
		$BANCO='';$CONT=0;
		foreach ($data as $row){
		
		if ($BANCO!= $row['Vendedore']['descripcion']){
			if($CONT!=0){
		?>
		<tfoot>
		<tr>		  	
		   <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
		?>
		<table class="table table-bordered">
		 <caption><label><?php echo 'Vendedor:&nbsp;'.$row['Vendedore']['descripcion']; ?></label></caption> 
		<tr>
		  <th width="5%">Nro</th>
		  <th width="7%">Factura</th>
		  <th width="25%">Cliente</th>
		  <th width="7%">Fecha</th>
		  <th>Monto Neto</th>
		  <th>Contado</th>
		  <th>Cr&eacute;dito</th>		  
		  <th>Iva</th>
		  <th>Total</th>
		  <th>Tipo de Ventas</th>		  
		</tr>
		<?php
		}
		?>
		<?php		
		$suma_factura=$suma_factura + $row['Venta']['baseimp2'];
		$suma_iva=$suma_iva + $row['Venta']['ivaimp2'];
		$suma_bruto=$suma_bruto + $row['Venta']['baseimp2'] + $row['Venta']['ivaimp2'];		
			?>
		<tr>
		 <td><?php echo $row['Venta']['codventa'];?></td>
		  <td><?php echo $row['Venta']['numerofactura'];?></td>
		  <td><?php echo $row['Cliente']['rif'].'=>'.$row['Cliente']['descripcion']?></td>
		  <td><?php echo date("d-m-Y",strtotime($row['Venta']['fecha']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.');?></td>
		  <td align="right"><?php 
		  if($row['Venta']['estatus']=='P' || substr($row['Venta']['numerofactura'], 0, 1)=='B'){
				echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.');
				$suma_factura_contado=$suma_factura_contado + $row['Venta']['baseimp2'];				
		  }else{
				echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			  }			
		  ?></td>
		  <td align="right"><?php 
		  if($row['Venta']['estatus']=='A'  || substr($row['Venta']['numerofactura'], 0, 1)=='A'){
				echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.');
				$suma_factura_credito=$suma_factura_credito + $row['Venta']['baseimp2'];
		  }else{
				echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			  }			
		  ?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Venta']['ivaimp2']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2'] + $row['Venta']['ivaimp2']), 2, ',', '.');?></td>
		  <td><?php if($row['Venta']['estatus']=='P'){
				echo 'Contado';
			  }elseif($row['Venta']['estatus']=='A'){
				echo 'Credito';
			  }elseif($row['Venta']['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>		  
		 </tr> 
		
		<?php
		$BANCO=$row['Vendedore']['descripcion'];
		$CONT=$CONT+1;
		}
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>		  	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>	  
		</tr>
		</tfoot>
 </table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	</div>
<!-- /.box-body -->
