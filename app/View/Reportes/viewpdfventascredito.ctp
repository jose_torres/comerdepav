<?php
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>				 
		  <th width="8%" align="center">Factura</th>
		  <th width="25%" align="center">Cliente</th>
		  <th width="8%" align="center">Fecha</th>
		  <th align="center">Monto Neto</th>
		  <th align="center">Iva</th>
		  <th align="center">Total</th>
		  <th align="center">Retencion</th>
		  <th align="center">Vendedor</th>	  				
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$total_ventas['factura']=$total_ventas['iva']=$total_ventas['bruto']=$total_ventas['retencion']=0;
$total_Devoluciones['factura']=$total_Devoluciones['iva']=$total_Devoluciones['bruto']=$total_Devoluciones['retencion']=0;
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;
foreach ($data as $row){
		$base_vp=$iva_vp=0;
		foreach($row['Ventaproducto'] as $registro){
			$base_vp = $base_vp + round($registro['cantidad']*$registro['precio'],2);
			$iva_vp = $iva_vp + round($registro['cantidad']*$registro['precio'] * $registro['iva']/100,2);
		}
		$row['Venta']['baseimp2'] = $base_vp;
		$suma_factura=$suma_factura + $row['Venta']['baseimp2'];
		/*$suma_iva=$suma_iva + round($row['Venta']['baseimp2']*$row['Venta']['porimp2']/100,2);
		$suma_bruto=$suma_bruto + $row['Venta']['baseimp2'] + round($row['Venta']['baseimp2']*$row['Venta']['porimp2']/100,2); */
		$suma_iva=$suma_iva + ($row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] + $row['Venta']['ivaimp3']);
		$suma_bruto=$suma_bruto + $row['Venta']['baseimp2'] + ($row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] + $row['Venta']['ivaimp3']);
	$suma_retencion = $suma_retencion + $row['Venta']['retencion'];	
	if($row['Venta']['estatus']=='P'){
		$tipo_compra='Contado';
	  }elseif($row['Venta']['estatus']=='A'){
		$tipo_compra='A Credito';
	  }elseif($row['Venta']['estatus']=='E'){
		$tipo_compra='Devuelto';
	  }
$texto=$texto.'
	<tr>
		  <td width="8%" align="left">'.$row['Venta']['numerofactura'].'</td>
		  <td width="25%" align="left">'.$row['Cliente']['descripcion'].'</td>
		  <td width="8%">'.date("d-m-Y",strtotime($row['Venta']['fecha'])).'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", round($row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] + $row['Venta']['ivaimp3'],2)), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row['Venta']['baseimp2'] + round($row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] + $row['Venta']['ivaimp3'],2)), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row['Venta']['retencion']), 2, ',', '.').'</td>
		  <td>'.$row['Vendedore']['descripcion'].'</td>
	</tr>';	
}	
$texto=$texto.'
	<tr>	
	<td width="8%" >&nbsp;</td>	
	<td width="25%">&nbsp;</td>	
	<td align="right"><b>TOTALES:</b></td>	
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</label> </td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</label></td>		  
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.').'</label></td>	
	<td >&nbsp;</td>		
	</tr>';
//$texto=$texto.'</tbody>';
$total_ventas['factura']=$suma_factura;$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;$total_ventas['retencion']=$suma_retencion;
$texto=$texto.'</table>';
$this->tcpdf->core->writeHTML('<b>VENTAS</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');
// Compras
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>				 
		  <th width="8%" align="center">Devoluci&oacute;n</th>
		  <th width="25%" align="center">Cliente</th>
		  <th width="8%" align="center">Fecha</th>
		  <th align="center">Monto Neto</th>
		  <th align="center">Iva</th>
		  <th align="center">Total</th>
		  <th align="center">Retencion</th>
		  <th align="center">Fact. Afecta</th>	  				
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$total_ventas['factura']=$suma_factura;$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;$total_ventas['retencion']=$suma_retencion;
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;
foreach ($data_dev as $row){	
	$suma_factura=$suma_factura + $row['Devolucionventa']['baseimp2'];
		$suma_iva=$suma_iva + $row['Devolucionventa']['ivaimp2'];
		$suma_bruto=$suma_bruto + $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['ivaimp2'];
	$suma_retencion = $suma_retencion + $row['Devolucionventa']['retencion'];	
	if($row['Venta']['estatus']=='P'){
		$tipo_compra='Contado';
	  }elseif($row['Venta']['estatus']=='A'){
		$tipo_compra='A Credito';
	  }elseif($row['Venta']['estatus']=='E'){
		$tipo_compra='Devuelto';
	  }
$texto=$texto.'
	<tr>
		  <td width="8%" align="left">'.str_pad($row['Devolucionventa']['coddevolucion'], 10, "0", STR_PAD_LEFT).'</td>
		  <td width="25%" align="left">'.$row['Venta']['Cliente']['descripcion'].'</td>
		  <td width="8%">'.date("d-m-Y",strtotime($row['Venta']['fecha'])).'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row['Devolucionventa']['baseimp2']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row['Devolucionventa']['ivaimp2']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['ivaimp2']), 2, ',', '.').'</td>
		  <td align="right">'.number_format(sprintf("%01.2f", $row['Devolucionventa']['retencion']), 2, ',', '.').'</td>
		  <td>'.$row['Venta']['numerofactura'].'</td>
	</tr>';	
}	
$texto=$texto.'
	<tr>	
	<td width="8%" >&nbsp;</td>	
	<td width="25%">&nbsp;</td>	
	<td align="right"><b>TOTALES:</b></td>	
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</label></td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</label> </td>
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</label></td>		  
	<td align="right"><label>'.number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.').'</label></td>	
	<td >&nbsp;</td>		
	</tr>';
$texto=$texto.'</table>';
$total_Devoluciones['factura']=$suma_factura;$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;$total_Devoluciones['retencion']=$suma_retencion;
$this->tcpdf->core->writeHTML('<b>DEVOLUCIONES</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

$this->tcpdf->core->writeHTML('<b>TOTALES (VENTAS - DEVOLUCIONES)</b>', true, false, true, false,'L');

$this->tcpdf->core->MultiCell(35,1,' '.number_format(sprintf("%01.2f",$total_ventas['factura']- $total_Devoluciones['factura']), 2, ',', '.'),0,'R',false,0,73,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,'  '.number_format(sprintf("%01.2f", $total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.'),0,'R',false,0,103,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,' '.number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.'),0,'R',false,0,128,'',true,0, false,true, 0,'M',false);
$this->tcpdf->core->MultiCell(30,1,' '.number_format(sprintf("%01.2f", $total_ventas['retencion']- $total_Devoluciones['retencion']), 2, ',', '.'),0,'R',false,0,153,'',true,0, false,true, 0,'M',false);
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('ventascredito.pdf', 'D');
?>
