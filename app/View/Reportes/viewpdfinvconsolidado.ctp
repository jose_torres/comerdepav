<?php
set_time_limit(240);
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='Fecha:'.date("d-m-Y");$tc_forma[1]='';$tc_size[1]=8;
$titulo[2]='Hora: '.date("h:m:s a");$tc_forma[2]='';$tc_size[2]=8;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'Listado de Precio');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);

$this->tcpdf->core->setEqualColumns(1);
// Colors, line width and bold font
$this->tcpdf->core->SetFillColor(255, 255, 255);
$this->tcpdf->core->SetTextColor(0);
//$this->tcpdf->core->SetDrawColor(128, 0, 0);
$this->tcpdf->core->SetLineWidth(0.3);
$this->tcpdf->core->SetFont('', 'B');
        
$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
$BANCO='';$CONT=0;	$CONT_FILA=0; $DEP='';
$sucP = $suc1 = $suc2 = $suc3 = 0; 
$TCAsucP = $TCAsuc1 = $TCAsuc2 = $TCAsuc3 = 0;
$TCPsucP = $TCPsuc1 = $TCPsuc2 = $TCPsuc3 = 0;
// Header
    foreach($data as $row) {

        if($BANCO!= $row[0]['codigo'] && $CONT_FILA>0){
           $this->tcpdf->core->SetTextColor(0);
           $this->tcpdf->core->SetFont('', 'B');
          // $w = array(57, 17);
           //$this->tcpdf->core->Cell($w[0], 7, 'Total:', 'T', 0, 'R', 1);
           //$this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.'), 'T', 0, 'R', 1);
           //$this->tcpdf->core->Ln();
           //$this->tcpdf->core->Ln();

           $suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
        }
        $suma_factura=$suma_factura + $row[0]['cantidad'];
        
        $TCAsucP = $TCAsucP + ($row[0]['cant_principal']*$row[0]['costoactual']);
        $TCAsuc1 = $TCAsuc1 + ($row[0]['costoactual']*$row[0]['cant_suc1']);
        $TCAsuc2 = $TCAsuc2 + ($row[0]['costoactual']*$row[0]['cant_suc2']);
        $TCAsuc3 = $TCAsuc3 + ($row[0]['costoactual']*$row[0]['cant_suc3']);

        $TCPsucP = $TCPsucP + ($row[0]['cant_principal']*$row[0]['costopromedio']);
        $TCPsuc1 = $TCPsuc1 + ($row[0]['costopromedio']*$row[0]['cant_suc1']);
        $TCPsuc2 = $TCPsuc2 + ($row[0]['costopromedio']*$row[0]['cant_suc2']);
        $TCPsuc3 = $TCPsuc3 + ($row[0]['costopromedio']*$row[0]['cant_suc3']);

        $sucP = $sucP + $row[0]['cant_principal'];
        $suc1 = $suc1 + $row[0]['cant_suc1'];
        $suc2 = $suc2 + $row[0]['cant_suc2'];
        $suc3 = $suc3 + $row[0]['cant_suc3'];

        if($DEP!= $row['dp']['codigo'] ){
            $this->tcpdf->core->SetTextColor(0);
            $this->tcpdf->core->SetFont('', 'B');
            $this->tcpdf->core->Cell(190, 7, '('.$row['dp']['codigo'].') '.$row[0]['descripcion'], 'BT', 0, 'L', 1);
            $this->tcpdf->core->Ln();	
            
            $this->tcpdf->core->SetFont('', 'B');
            $tipo_costo['tit'] = 'Costo Actual';
            $tipo_costo['campo'] = 'costoactual';
            if (in_array("costopromedio", $datos['vercosto'])) {
                $tipo_costo['tit'] = 'Costo Prom.';
                $tipo_costo['campo'] = 'costopromedio';
            }
            $header=array('Codigo','Descripcion','Unidad','Exist. C21','Exist. CI','Exist. CII','Exist. CIII','Exist. Total','Valor en Bs');
         $w = array(9,60,10,15,15,15,15,12,40);
            $num_headers = count($header);
            for($i = 0; $i < $num_headers; ++$i) {
                    $this->tcpdf->core->Cell($w[$i], 7, $header[$i], 'B', 0, 'C', 1);
            }
            $this->tcpdf->core->Ln();
        }
    if($BANCO!= $row[0]['codigo']){

        $this->tcpdf->core->SetFont('', 'B');
        $tipo_costo['tit'] = 'Costo Actual';
        $tipo_costo['campo'] = 'costoactual';
        if (in_array("costopromedio", $datos['vercosto'])) {
            $tipo_costo['tit'] = 'Costo Prom.';
            $tipo_costo['campo'] = 'costopromedio';
        }
        $header=array('Codigo','Descripcion','Unidad','Exist. C21','Exist. CI','Exist. CII','Exist. CIII','Exist. Total',''.$tipo_costo['tit'],'Valor en Bs');
        $w = array(9,60,10,15,15,15,15,12,40);
      
        $CONT_FILA=0;
    }
    // Color and font restoration
    //$this->tcpdf->core->SetFillColor(224, 235, 255);
    $this->tcpdf->core->SetTextColor(0);
    $this->tcpdf->core->SetFont('');
    // Data
    $fill = 0;
    //foreach($data as $row) {
    $this->tcpdf->core->Cell($w[0], 6, $row[0]['codigo'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[1], 6, $row[0]['nombre'], '', 0, 'L', $fill);
    $this->tcpdf->core->Cell($w[2], 6, $row[0]['unidad'], '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[3], 6, number_format(sprintf("%01.2f", $row[0]['cant_principal']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[4], 6, number_format(sprintf("%01.2f", $row[0]['cant_suc1']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[5], 6, number_format(sprintf("%01.2f", $row[0]['cant_suc2']), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[6], 6, number_format(sprintf("%01.2f", $row[0]['cant_suc3']), 2, ',', '.'), '', 0, 'R', $fill);    
    $this->tcpdf->core->Cell($w[7], 6, number_format(sprintf("%01.2f", $row[0]['cantidad']), 2, ',', '.'), '', 0, 'R', $fill);
    //$this->tcpdf->core->Cell($w[8], 6, number_format(sprintf("%01.2f", $row[0][$tipo_costo['campo']]), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Cell($w[8], 6, number_format(sprintf("%01.2f", $row[0]['cantidad'] * $row[0][$tipo_costo['campo']]), 2, ',', '.'), '', 0, 'R', $fill);
    $this->tcpdf->core->Ln();
    //$fill=!$fill;
    $BANCO=$row[0]['codigo'];
    $DEP=$row['dp']['codigo'];
    $CONT_FILA=$CONT_FILA+1;
    $CONT=$CONT+1;
}

    if($CONT>0){
        $this->tcpdf->core->SetTextColor(0);
        $this->tcpdf->core->SetFont('', 'B');
        $w = array(150, 40);
        $this->tcpdf->core->Cell($w[0], 7, 'Principal:', 'T', 0, 'R', 1);
        if (in_array("costopromedio", $datos['vercosto'])) {
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCPsucP), 2, ',', '.'), 'T', 0, 'R', 1);
        }else{
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCAsucP), 2, ',', '.'), 'T', 0, 'R', 1);
        }
        $this->tcpdf->core->Ln();
        $this->tcpdf->core->Cell($w[0], 7, 'Suc 1:', 'T', 0, 'R', 1);
        if (in_array("costopromedio", $datos['vercosto'])) {
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCPsuc1), 2, ',', '.'), 'T', 0, 'R', 1);
        }else{
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCAsuc1), 2, ',', '.'), 'T', 0, 'R', 1);
        }
        $this->tcpdf->core->Ln();
        $this->tcpdf->core->Cell($w[0], 7, 'Suc 2:', 'T', 0, 'R', 1);
        if (in_array("costopromedio", $datos['vercosto'])) {
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCPsuc2), 2, ',', '.'), 'T', 0, 'R', 1);
        }else{
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCAsuc2), 2, ',', '.'), 'T', 0, 'R', 1);
        }
        $this->tcpdf->core->Ln();
        $this->tcpdf->core->Cell($w[0], 7, 'Suc 3:', 'T', 0, 'R', 1);
        if (in_array("costopromedio", $datos['vercosto'])) {
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCPsuc3), 2, ',', '.'), 'T', 0, 'R', 1);
        }else{
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCAsuc3), 2, ',', '.'), 'T', 0, 'R', 1);
        }
        $this->tcpdf->core->Ln();
        $this->tcpdf->core->Cell($w[0], 7, 'Total:', 'T', 0, 'R', 1);
        if (in_array("costopromedio", $datos['vercosto'])) {
        $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCPsucP+$TCPsuc1+$TCPsuc2+$TCPsuc3), 2, ',', '.'), 'T', 0, 'R', 1);
        }else{
            $this->tcpdf->core->Cell($w[1], 7,  number_format(sprintf("%01.2f", $TCAsucP+$TCAsuc1+$TCAsuc2+$TCAsuc3), 2, ',', '.'), 'T', 0, 'R', 1);
        }
        $this->tcpdf->core->Ln();
    }
 //   $this->tcpdf->core->Cell(array_sum($w), 0, '', '');
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('consolidado.pdf', 'D');
?>
