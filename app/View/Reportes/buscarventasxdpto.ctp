<?php
     echo $this->Html->css('datatables/dataTables.bootstrap.css');
      echo $this->Html->css('datatables/rowGroup.dataTables.min.css');
?>
<!-- /.box-header -->
<div class="box">
<div class="box-header">
  <h3 class="box-title">Resumen de Ventas por Departamentos</h3>
</div>
<table class="display table table-bordered" id="example">
    <caption><h4><?php echo $titulo; ?></h4></caption> 
    <thead>	
        <tr>
            <th width="7%">Codigo</th>		  
            <th width="25%">Departamento</th>
            <th align="right">Cantidad</th>
            <th align="right">Costo</th>            		  
            <th align="right">Monto Base</th>		  
            <th align="right">Iva</th>
            <th align="right">Total</th>
            <th align="right">Utilidad</th>
        </tr>
    </thead>	
    <tbody>
<?php
       $suma_factura=0;$suma_factura_costo=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
       $BANCO='';$CONT=0;$IVA=0; $suma_cantidad = 0;	
       foreach ($data as $row){		
            
       $suma_factura=$suma_factura + $row[0]['venta'];       
       $suma_iva=$suma_iva + $row[0]['iva'];       
       $suma_bruto=$suma_bruto + $row[0]['venta'] + $row[0]['iva'];
       $suma_cantidad = $suma_cantidad + $row[0]['cantidad'];
       $suma_factura_costo = $suma_factura_costo + $row[0]['costo'];
       $UTIL = 0;
		if($row[0]['venta']>0){
			$UTIL = ($row[0]['venta'] - $row[0]['costo'])/$row[0]['venta']*100;
		}
?>
       <tr>
         <td><?php echo $row[0]['coddep'];?></td>
         <td><?php echo $row[0]['descripcion'];?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['cantidad']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['costo']), 2, ',', '.');?></td>         
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['venta']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['iva']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['venta'] + $row[0]['iva'] ), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$UTIL), 2, ',', '.');?></td>         
        </tr> 		
<?php		
        $CONT=$CONT+1;
        }
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;	
$UTIL = 0;
if($suma_factura>0){
	$UTIL = ($suma_factura-$suma_factura_costo)/$suma_factura*100;
}
	
?>

    </tbody>
    <tfoot>
        <tr>
            <td>&nbsp;</td>      	
            <td align="right"><label>Totales:</label></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_cantidad), 2, ',', '.');?></td>		  
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.');?></td>		  
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>	
             <td align="right"><?php echo number_format(sprintf("%01.2f", $UTIL), 2, ',', '.');?></td>	 
        </tr>
    </tfoot>
</table>
 
</div>
<?php
    //echo $this->Html->script('jquery.min.js');
        echo $this->Html->script('funciones.js');
    echo $this->Html->script('plugins/datatables/jquery-3.3.1.js');
    echo $this->Html->script('bootstrap.min.js');
     echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
    //echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
         echo $this->Html->script('plugins/datatables/dataTables.buttons.min.js');
    echo $this->Html->script('plugins/datatables/buttons.flash.min.js');
    echo $this->Html->script('plugins/datatables/jszip.min.js');
    echo $this->Html->script('plugins/datatables/pdfmake.min.js');
    echo $this->Html->script('plugins/datatables/vfs_fonts.js');
    
    echo $this->Html->script('plugins/datatables/buttons.html5.min.js');
    echo $this->Html->script('plugins/datatables/buttons.print.min.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	$(document).ready(function() {
    $('#example').DataTable({
	  "dom": "Blfrtip",
       "buttons": [
            "copy", "csv"/*, "excel"*/
        ],		  
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "scrollX": true,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		},

    });
} );  
 
</script>	
<!-- /.box-body -->
