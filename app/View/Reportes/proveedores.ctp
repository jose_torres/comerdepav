<?php
//echo $this->element('menuinterno',$datos_menu);
//echo $this->Html->script('prototype');
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');

?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	 // var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	
	function actualizar(nombrediv,url,id,datos){
		
        $('#'+nombrediv).html('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');
		
		var selected = '&proveedores=0-';var cont=1;
		$('#ReporteProveedoreId option:checked').each(function(){
		selected += $(this).val() + '-'; 
		});
		fin = selected.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
		selected = selected.substr( 0, fin ); // elimino la coma final
		
		var tipoventa = '&tipoventa=0-';var cont=1;
		$('#ReporteTipoventa option:checked').each(function(){
		tipoventa += $(this).val() + '-'; 
		});
		fin = tipoventa.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
		tipoventa = tipoventa.substr( 0, fin ); // elimino la coma final
				
		$.ajax({
			type: 'POST',
			url: url+id,
			data: datos+''+selected+''+tipoventa,
			dataType: 'html'
		})

		.done(function( html ) {					
			$( '#'+nombrediv ).html( html );
		});

    }
	
</script>
<fieldset id="personal" >
<legend>REPORTE DE COMPRAS POR PROVEEDORES REGISTRADAS</legend>

<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'viewpdfventas/1','id'=>'frm','name'=>'frm'));
?>	
<?php

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';


$tiporeporte['Proveedore_Listado']='Listado de Proveedores';
$tiporeporte['Proveedore_Cxp']='Cuentas por Pagar';
$tiporeporte['Compras_Pagos']='Relacion Pagos';
$tiporeporte['Proveedore_Estado']='Estado de Cuenta';
/*$tiporeporte['Ventas_Resumen_Cxc']='Resumen de Cuentas por Cobrar';
$tiporeporte['Listado_Cliente']='Listado de Clientes';*/
		
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Tipo de Reportes:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.tiporeporte',array('div'=>false,'options'=>$tiporeporte,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Reporte",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

$tipoventa['P']='Contado';$tipoventa['A']='A Credito';$tipoventa['E']='Devueltos';		
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Tipo de Compras:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-2">';
echo $this->Form->input('Reporte.tipoventa',array('multiple' => true,'div'=>false,'options'=>$tipoventa,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Ventas",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
echo $this->Html->tag('label', 'Rango de Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="input-daterange input-group col-xs-3" id="datepicker">';

echo $this->Form->input('Reporte.desde',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true));
echo '<span class="input-group-addon">a</span>';
echo $this->Form->input('Reporte.hasta',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Hasta",'value'=>$fecha2,'readonly'=>true)); 
echo '</div>';		
echo '</div>';

echo '<div class="form-group">';
echo $this->Html->tag('label', 'Proveedores:', array('class' => 'col-xs-2 control-label','for'=>"cliente_id"));
echo '<div class="col-xs-6">';
echo $this->Form->input('Reporte.proveedore_id',array('multiple' => true,'div'=>false,'options'=>$proveedore,'label'=>false,'data-placeholder'=>"Seleccione Proveedores",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';		
echo '</div>';

	?>
<button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarproveedores','','tiporeporte='+document.frm.ReporteTiporeporte.value+'&fechadesde='+document.frm.ReporteDesde.value+'&fechahasta='+document.frm.ReporteHasta.value+'&sucursal='+document.frm.ReporteSucursal.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
<button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	
<?php echo $this->Form->end(); ?>
<center>
<form action="" method="post" name="otro">
<div id="buscar">

</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php //echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/funciones/add", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</center>
</form>
</fieldset>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
</script>
