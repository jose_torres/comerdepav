<?php

$this->tcpdf->core->SetPageOrientation("L");
$this->tcpdf->core->SetOrientacion("LIMP");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($encabezado['titulo_reporte'],'B',12);
$titulo[1]='Rif: '.$empresa['Empresa']['rif'];$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='Mes: '.$datos['MES'].'/'.$datos['year'];$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla('Empresa: '.$empresa['Empresa']['nombre'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'LIBRO DE VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>		  
		  <th width="4%" align="center">Nro</th>	
		  <th width="7%" align="center">Fecha</th>	
		  <th width="7%" align="center">Factura</th>
		  <th width="25%" align="center">Nombre/Raz&oacute;n Social</th>
		  <th width="7%" align="center">Rif</th>
		  <th width="7%" align="center">Total Ventas</th>
		  <th width="7%" align="center">Ventas Exenta</th>		  
		  <th width="7%" align="center">Ventas Exportaci&oacute;n</th>		  
		  <th width="7%" align="center">Base</th>	  
		  <th width="4%" align="center">&#37;</th>
		  <th width="7%" align="center">Impuesto</th>
		  <th width="7%" align="center">Iva Retenido</th>	
		</tr></thead>	
	';
//$texto=$texto.'<tbody>';
$suma_factura=0;$suma_factura_credito=0;$suma_factura_exenta=$suma_factura_contado=0; $suma_iva=0; $suma_iva_retenido=$suma_bruto=0;
$BANCO='';$X=$CONT=0;
foreach ($data as $row){
	$X++; $total_ventas=$monto_exento=$base_imp=$monto_iva=$iva=$monto_retencion=0;	 	
	if($row[0]['estatus']!='E'){
		$suma_factura=$suma_factura + $row[0]['baseimp2'];
		$suma_iva=$suma_iva + round($row[0]['baseimp2']*$row[0]['porimp2']/100,2);
		$suma_bruto=$suma_bruto + $row[0]['baseimp2'] + round($row[0]['baseimp2']*$row[0]['porimp2']/100,2);
		$suma_factura_exenta=$suma_factura_exenta + $row[0]['montoexento'];
		$suma_iva_retenido = $suma_iva_retenido + $row[0]['retencion'];
		//------------------------------------------------------------
		$total_ventas = $row[0]['baseimp2'] + round($row[0]['baseimp2']*$row[0]['porimp2']/100,2);
		$monto_exento = $row[0]['montoexento'];
		$base_imp = $row[0]['baseimp2'];
		$iva = round($row[0]['porimp2'],2);
		$monto_iva = round($row[0]['baseimp2']*$row[0]['porimp2']/100,2);
		$monto_retencion = round($row[0]['retencion'],2);
	}

$texto=$texto.'
	<tr>	  
	     <td width="4%" >'.$X.'</td>	
		  <td width="7%" >'.date("d-m-Y",strtotime($row[0]['fecha'])).'</td>
		  <td width="7%" >'.$row[0]['numerofactura'].'</td>
		  <td width="25%" align="left">'.$row['cliente']['descripcion'].'</td>
		  <td width="7%">'.$row['cliente']['rif'].'</td>
		  <td width="7%" align="right">'.number_format(sprintf("%01.2f", $total_ventas), 2, ',', '.').'</td>
		  <td width="7%" align="right">'.number_format(sprintf("%01.2f", $monto_exento), 2, ',', '.').'</td>
		  <td width="7%" align="right">'.number_format(sprintf("%01.2f", 0), 2, ',', '.').'</td>
		  <td width="7%" align="right">'.number_format(sprintf("%01.2f", $base_imp), 2, ',', '.').'</td>		  
		  <td width="4%" align="right">'.number_format(sprintf("%01.2f", $iva), 2, ',', '.').'</td>
		  <td width="7%" align="right">'.number_format(sprintf("%01.2f", $monto_iva), 2, ',', '.').'</td>
		  <td width="7%" align="right">'.number_format(sprintf("%01.2f", $monto_retencion), 2, ',', '.').'</td>  
	</tr>';
	$CONT=$CONT+1;
}
/*$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;		
$total_ventas['factura_contado']=$suma_factura_contado;		
$total_ventas['factura_credito']=$suma_factura_credito;*/

$texto=$texto.'
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><strong><label>Totales:</label></strong></td>
		  <td align="right"><strong>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</strong></td>
		  <td align="right"><strong>'.number_format(sprintf("%01.2f", $suma_factura_exenta), 2, ',', '.').'</strong></td>
		  <td align="right"><strong>'.number_format(sprintf("%01.2f", 0), 2, ',', '.').'</strong></td>
		  <td align="right"><strong>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</strong></td> 
		  <td>&nbsp;</td>	
		  <td align="right"><strong>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</strong></td>
		  <td align="right"><strong>'.number_format(sprintf("%01.2f", $suma_iva_retenido), 2, ',', '.').'</strong></td>  		  
		</tr>';
$texto=$texto.'</table>';

//$this->tcpdf->core->Ln(2);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('libro_de_ventas_especial.pdf', 'D');
?>
