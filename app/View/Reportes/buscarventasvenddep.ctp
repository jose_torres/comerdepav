<?php
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<h3><?php echo $titulo; ?></h3>
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_costo=0;	
		$BANCO='';$CONT=0;
		foreach ($data as $row){
		
		if ($BANCO!= $row['vendedore']['descripcion']){
			if($CONT!=0){
		?>
		<tfoot>
		<tr>		  	
		   <td>&nbsp;</td>		
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>		  		  
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_costo=0;	
		?>
		<table class="table table-bordered">
		 <caption><label><?php echo 'Vendedor:&nbsp;'.$row['vendedore']['descripcion']; ?></label></caption> 
		<tr>
		  <th width="15%">C&oacute;digo</th>
		  <th width="30%">Descripcion</th>		  
		  <th>Monto Neto</th>
		</tr>
		<?php
		}
		?>
		<?php		
		$suma_factura=$suma_factura + $row[0]['total'];
		?>
		<tr>
		 <td><?php echo $row['departamento']['codigo'];?></td>
		  <td><?php echo $row['departamento']['descripcion'];?></td>		 
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total']), 2, ',', '.');?></td>	  
		 </tr>	
		<?php
		$BANCO=$row['vendedore']['descripcion'];
		$CONT=$CONT+1;
		}
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>		  	
		   <td>&nbsp;</td>		
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>	  
		</tr>
		</tfoot>
 </table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	</div>
<!-- /.box-body -->
