<?php
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';*/
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<table class="table table-bordered">
		 <caption><h2><?php echo $titulo; ?></h2></caption> 
		<tr>
		  <th width="5%">Nro</th>	
		  <th width="7%">Fecha</th>	
		  <th width="7%">Factura</th>
		  <th width="25%">Nombre/Raz&oacute;n Social</th>
		  <th>Rif</th>
		  <th>Total Compras</th>
		  <th>Compras Exenta</th>		  
		  <th>Compras Importaci&oacute;n</th>		  
		  <th>Base</th>	  
		  <th>&#37;</th>
		  <th>Impuesto</th>
		</tr>
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_exenta=$suma_factura_contado=0; $suma_iva=0; $suma_iva_retenido=$suma_bruto=0;
		$BANCO='';$X=$CONT=0;	
		foreach ($data as $row){		
			$base_vp=$iva_vp=0;
			$X++;
			/*foreach($row['Ventaproducto'] as $registro){
				$base_vp = $base_vp + round($registro['cantidad']*$registro['precio'],2);
				$iva_vp = $iva_vp + round($registro['cantidad']*$registro['precio'] * $registro['iva']/100,2);
			}*/
		//	$row[0]['baseimp2'] = $base_vp;
		if($row[0]['estatus']!='E'){
		$suma_factura=$suma_factura + $row[0]['baseimp2'];
		$suma_iva=$suma_iva + round($row[0]['baseimp2']*$row[0]['porimp2']/100,2);
		$suma_bruto=$suma_bruto + $row[0]['baseimp2'] + round($row[0]['baseimp2']*$row[0]['porimp2']/100,2);
		$suma_factura_exenta=$suma_factura_exenta + $row[0]['montoexento'];
		$suma_iva_retenido = $suma_iva_retenido + $row[0]['retencion'];
		}
			?>
		<tr>
		  <td><?php echo $X; ?></td>	
		  <td><?php echo date("d-m-Y",strtotime($row[0]['femision']));?></td>
		  <td><?php echo $row[0]['nrodocumento'];?></td>
		  <td><?php echo $row['proveedore']['descripcion']?></td>
		  <td><?php echo $row['proveedore']['rif'];?></td>
		  <td align="right"><?php
		  if($row[0]['estatus']!='E'){
			echo number_format(sprintf("%01.2f", $row[0]['baseimp2'] + round($row[0]['baseimp2']*$row[0]['porimp2']/100,2)), 2, ',', '.');
		  }else{ echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			}
			?></td>
		  <td align="right"><?php
		  if($row[0]['estatus']!='E'){
			echo number_format(sprintf("%01.2f", $row[0]['montoexento']), 2, ',', '.');
		  }else{
			  echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
		  }
		  ?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></td>
		  <td align="right"><?php
		  if($row[0]['estatus']!='E'){
			echo number_format(sprintf("%01.2f", $row[0]['baseimp2']), 2, ',', '.');
		  }else{
			  echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
		  } 
		  ?></td>		  
		  <td align="right"><?php
		  if($row[0]['estatus']!='E'){
			echo number_format(sprintf("%01.2f", round($row[0]['porimp2'],2)), 2, ',', '.');
		  }else{
			echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
		  }
		  ?></td>
		  <td align="right"><?php
		  if($row[0]['estatus']!='E'){
			echo number_format(sprintf("%01.2f", round($row[0]['baseimp2']*$row[0]['porimp2']/100,2)), 2, ',', '.');
		  }else{
			echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
		  }
		  ?></td>
		 </tr> 		
		<?php		
		$CONT=$CONT+1;
		}
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;		
$total_ventas['factura_contado']=$suma_factura_contado;		
$total_ventas['factura_credito']=$suma_factura_credito;		
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_exenta), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td> 
		  <td>&nbsp;</td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>  	  
		</tr>
		</tfoot>
 </table>
 
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?>
</div>
<!-- /.box-body -->
