<?php
	//echo 'Esto es Buscar Ventas';
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';*/	
?>
<!-- /.box-header -->
<table class="table table-bordered table-hover">
		<tr>
		  <th>Cliente</th>
		  <th>Nro Factura</th>
		  <th>Fecha Emisi&oacute;n</th>		  
		  <th>Fecha de Vencimiento</th>		  
		  <th>D&iacute;as</th>
		  <th>Monto</th>
		  <th>Saldos</th>
		</tr>	
		<?php
		$suma_facturas['monto']=$suma_facturas['saldo']=0;$CONT=0;	
		foreach ($data as $row){			
			$suma_facturas['monto']=$suma_facturas['monto'] + $row[0]['monto'];
			$suma_facturas['saldo']=$suma_facturas['saldo'] + ($row[0]['monto']-$row[0]['cobros']-$row[0]['notacredito']);
			?>
		<tr>
		  <td><?php echo $row['cliente']['rif'].'&nbsp;'.$row['cliente']['descripcion'];?></td>
		  <td><?php echo $row[0]['numerofactura'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>				  	
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fvencimiento']));?></td>
		  <td><?php echo $row[0]['dias'];?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto']-$row[0]['cobros']), 2, ',', '.');?></td>
		 </tr> 
		<?php
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_facturas['monto']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_facturas['saldo']), 2, ',', '.');?></td>		  	  
		</tr>
		</tfoot>
</table>
<!-- /.box-body -->
