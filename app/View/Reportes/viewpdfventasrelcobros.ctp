<?php
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>				 
		  <th align="center" width="8%" >Fecha</th>
		  <th align="center">Contado</th>
		  <th align="center">Venta Neta</th>
		  <th align="center">Impuesto</th>
		  <th align="center">Cr&eacute;dito</th>
		  <th align="center">Cobros</th>
		  <th align="center">N. Cr&eacute;dito/Descuento</th>			
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$suma_facturas['contado']=$suma_facturas['montonetocredito']=$suma_facturas['montoivacredito']=$suma_facturas['montototalcredito']=$suma_facturas['cobros']=$suma_facturas['notacredito']=0;$CONT=0;	
foreach ($data as $row){	
	$suma_facturas['contado']=$suma_facturas['contado'] + $row[0]['contado'];
	$suma_facturas['montonetocredito']=$suma_facturas['montonetocredito'] + $row[0]['montonetocredito'];
	$suma_facturas['montoivacredito']=$suma_facturas['montoivacredito'] + $row[0]['montoivacredito'];
	$suma_facturas['montototalcredito']=$suma_facturas['montototalcredito'] + $row[0]['montototalcredito'];
	$suma_facturas['cobros']=$suma_facturas['cobros'] + $row[0]['cobros'] + $row[0]['cobrosdirectos'];
	$suma_facturas['notacredito']=$suma_facturas['notacredito'] + $row[0]['notacredito'];
$texto=$texto.'
	<tr>
	  <td width="8%" >'.date("d-m-Y",strtotime($row[0]['fecha'])).'</td>	
	  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['contado']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['montonetocredito']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['montoivacredito']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['montototalcredito']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['cobros'] + $row[0]['cobrosdirectos']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['notacredito']), 2, ',', '.').'</td>
	</tr>';	
}	
$texto=$texto.'
	<tr>	
	  <td align="right" width="8%" ><b>Totales:</b></td>	
	  <td align="right">'.number_format(sprintf("%01.2f", $suma_facturas['contado']), 2, ',', '.').'</td>		  
	  <td align="right">'.number_format(sprintf("%01.2f", $suma_facturas['montonetocredito']), 2, ',', '.').'</td>		  
	  <td align="right">'.number_format(sprintf("%01.2f", $suma_facturas['montoivacredito']), 2, ',', '.').'</td>		  
	  <td align="right">'.number_format(sprintf("%01.2f", $suma_facturas['montototalcredito']), 2, ',', '.').'</td>		  
	  <td align="right">'.number_format(sprintf("%01.2f", $suma_facturas['cobros']), 2, ',', '.').'</td>		  
	  <td align="right">'.number_format(sprintf("%01.2f", $suma_facturas['notacredito']), 2, ',', '.').'</td>		
	</tr>';
$texto=$texto.'</table>';
//$this->tcpdf->core->writeHTML('<b>VENTAS</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('ventasrelacioncobros.pdf', 'D');
?>
