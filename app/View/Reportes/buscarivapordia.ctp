<?php
     echo $this->Html->css('datatables/dataTables.bootstrap.css');
     echo $this->Html->css('datatables/rowGroup.dataTables.min.css');
?>
<style>
    table.dataTable tr.group-end td {
    text-align: right;
    font-weight: normal;
}
</style>
<!-- /.box-header -->
<div class="box">
<div class="box-header">
    <h3 class="box-title">Resumen de Iva por D&iacute;a</h3>
</div>
<table class="display table table-bordered" id="example">
    <caption><h4><?php echo $titulo; ?></h4></caption> 
    <thead>	
        <tr>		  
            <th width="7%">Fecha</th>		  
            <th width="25%">&percnt; de Iva</th>                        	  
            <th align="right">Monto Base</th>		  
            <th align="right">Monto Iva</th>
            <th align="right">Total</th>
        </tr>
    </thead>	
    <tbody>
<?php
       $suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; 
       $suma_iva=0; $suma_bruto=0;
       $BANCO='';$CONT=0;$IVA=0;	
       foreach ($data as $row){		
            
       $suma_factura=$suma_factura + $row[0]['venta'];       
       $suma_iva=$suma_iva + $row[0]['ivaimp'];       
       $suma_bruto=$suma_bruto + $row[0]['venta'] + $row[0]['ivaimp'];
       
?>
       <tr>
         <td><?php echo date('d-m-Y',strtotime($row[0]['fecha']));?></td>  
         <td><?php echo $row[0]['iva'];?></td>         
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['venta']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['ivaimp']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['venta'] + $row[0]['ivaimp'] ), 2, ',', '.');?></td>         
        </tr> 		
<?php		
        $CONT=$CONT+1;
      //  $BANCO= $row[0]['coddep'];
        }
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;		
?>

    </tbody>
    <tfoot>
        <tr>            
            <td>&nbsp;</td>      	
            <td align="right"><label>Totales:</label></td>		  
            <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></strong></td>
            <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></strong></td>
            <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></strong></td>	  
        </tr>
    </tfoot>
</table>
 
</div>
<?php
    //echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('funciones.js');
    echo $this->Html->script('plugins/datatables/jquery-3.3.1.js');
    echo $this->Html->script('bootstrap.min.js');
    echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
    echo $this->Html->script('plugins/datatables/dataTables.rowGroup.min.js');
    //echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
    $(document).ready(function() {
    var groupColumn = 0;
    
    
    $('#example').DataTable( {
        "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		},
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],        
        order: [[0, 'asc']],
        rowGroup: {
            
            endRender: function ( rows, group ) {
                //return group +' ('+rows.count()+')';
                 var salaryAvg = rows
                    .data()
                    .pluck(4)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                salaryAvg = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( salaryAvg );
                var totalBase = rows
                    .data()
                    .pluck(2)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                totalBase = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( totalBase );
                
                var totalIva = rows
                    .data()
                    .pluck(3)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                totalIva = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( totalIva );
                
                 return $('<tr/>')
                    .append( '<td ><strong>Totales por Fecha</strong></td>' )
                    .append( '<td><strong>'+totalBase+'</strong></td>' )
                    .append( '<td><strong>'+totalIva+'</strong></td>' )
                    .append( '<td><strong>'+salaryAvg+'</strong></td>' );
            },
            dataSrc:0
        }
    } );
} );  
 
</script>	
<!-- /.box-body -->
