<?php
//print_r($vercosto);
    echo $this->Html->css('datatables/dataTables.bootstrap.css');
if(count($data)>0){
?>
<!-- /.box-header -->
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Existencia de Producto Encontrados</h3>
	</div>
    <table border="1" cellpadding="1" cellspacing="0" width="100%" align="center" class="table table-condensed table-bordered table-hover" id="ListadoCuadre">
             <caption><label>Listado de Existencia de Producto</label></caption> 
             <thead>     
        <tr>
          <th width="10%">Codigo</th>  
          <th width="10%">Depto</th>
          <th width="20%">Producto</th>
          <th width="7%">Unidad</th>
          <th width="10%">Exist. C21</th>
          <th width="10%">Exist. CI</th>
          <th width="10%">Exist. CII</th>
          <th width="10%">Exist. CIII</th>
          <th width="10%">Exist. Total</th>	
<?php 
        if (in_array("costoactual", $vercosto)) {
           echo '<th width="10%">Costo Actual</th>';
        } 
        
        if (in_array("costopromedio", $vercosto)) {
           echo '<th width="10%">Costo Promedio</th>';
        }
        
        if (in_array("costoactual", $vercosto)) {
           echo '<th width="10%">Total Costo Actual</th>';
        }
        
        if (in_array("costopromedio", $vercosto)) {
           echo '<th width="10%">Total Costo Promedio</th>';
        }
        
        if (in_array("preciominimo", $precio)) {
           echo '<th width="10%">Precio Min.</th>';
        }
        if (in_array("preciomayor", $precio)) {
           echo '<th width="10%">Precio May.</th>';
        }
        if (in_array("preciodetal", $precio)) {
           echo '<th width="10%">Precio Det.</th>';
        }
?>          
        </tr>
        </thead>
        <tbody>
	<?php
            $suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
            $BANCO='';$CONT=0;	$sucP = $suc1 = $suc2 = $suc3 = 0; 
            $TCAsucP = $TCAsuc1 = $TCAsuc2 = $TCAsuc3 = 0;
            $TCPsucP = $TCPsuc1 = $TCPsuc2 = $TCPsuc3 = 0;
            foreach ($data as $row){
            $suma_factura=$suma_factura + $row[0]['cantidad'];		
            $suma_factura_costo=$suma_factura_costo + $row[0]['cantidad']*$row[0]['costoactual'];	
            $TCAsucP = $TCAsucP + ($row[0]['cant_principal']*$row[0]['costoactual']);
            $TCAsuc1 = $TCAsuc1 + ($row[0]['costoactual']*$row[0]['cant_suc1']);
            $TCAsuc2 = $TCAsuc2 + ($row[0]['costoactual']*$row[0]['cant_suc2']);
            $TCAsuc3 = $TCAsuc3 + ($row[0]['costoactual']*$row[0]['cant_suc3']);
            
            $TCPsucP = $TCPsucP + ($row[0]['cant_principal']*$row[0]['costopromedio']);
            $TCPsuc1 = $TCPsuc1 + ($row[0]['costopromedio']*$row[0]['cant_suc1']);
            $TCPsuc2 = $TCPsuc2 + ($row[0]['costopromedio']*$row[0]['cant_suc2']);
            $TCPsuc3 = $TCPsuc3 + ($row[0]['costopromedio']*$row[0]['cant_suc3']);
            
            $sucP = $sucP + $row[0]['cant_principal'];
            $suc1 = $suc1 + $row[0]['cant_suc1'];
            $suc2 = $suc2 + $row[0]['cant_suc2'];
            $suc3 = $suc3 + $row[0]['cant_suc3'];
            
           $suma_factura_precio=$suma_factura_precio + $row[0]['cantidad']*$row[0]['costopromedio'];		
             /*$suma_utilidad=$suma_utilidad + ($row[0]['total']-($row[0]['total']-$row[0]['totalcostodescuento']));*/		
                    ?>
            <tr>
               <td><?php echo $row[0]['codigo'];?></td>
               <td><?php echo $row['dp']['codigo'];?></td>
               <td><?php echo $row[0]['nombre'];?></td>
               <td><?php echo $row[0]['unidad'];?></td>
               <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['cant_principal']), 2, ',', '.');?></td>
               <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['cant_suc1']), 2, ',', '.');?></td>
               <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['cant_suc2']), 2, ',', '.');?></td>
               <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['cant_suc3']), 2, ',', '.');?></td>
               <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['cantidad']), 2, ',', '.');?></td>
<?php 
        if (in_array("costoactual", $vercosto)) {
           echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['costoactual']), 2, ',', '.').'</td>';
        } 
        
        if (in_array("costopromedio", $vercosto)) {
           echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['costopromedio']), 2, ',', '.').'</td>';
        }
        
        if (in_array("costoactual", $vercosto)) {
           echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['cantidad']*$row[0]['costoactual']), 2, ',', '.').'</td>';
        }
        
        if (in_array("costopromedio", $vercosto)) {
           echo '<td align="right">'.number_format(sprintf("%01.2f",$row[0]['cantidad']*$row[0]['costopromedio']), 2, ',', '.').'</td>';
        }
        
        if (in_array("preciominimo", $precio)) {
           echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['preciominimo']), 2, ',', '.').'</td>';
        }
        if (in_array("preciomayor", $precio)) {
           echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['preciomayor']), 2, ',', '.').'</td>';
        }
        if (in_array("preciodetal", $precio)) {
            echo '<td align="right">'.number_format(sprintf("%01.2f", $row[0]['preciodetal']), 2, ',', '.').'</td>';
        }
?>                        		  
             </tr> 

            <?php
            $BANCO=$row[0]['codigo'];
            $CONT=$CONT+1;
            }
            ?>
</tbody>
<tfoot>
        <tr>		  	
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="right"><label>Totales:</label></td>
          <td align="right"><?php echo number_format(sprintf("%01.2f", $sucP), 2, ',', '.');?></td>
          <td align="right"><?php echo number_format(sprintf("%01.2f", $suc1), 2, ',', '.');?></td>
          <td align="right"><?php echo number_format(sprintf("%01.2f", $suc2), 2, ',', '.');?></td>
          <td align="right"><?php echo number_format(sprintf("%01.2f", $suc3), 2, ',', '.');?></td>
          <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
<?php 
        if (in_array("costoactual", $vercosto)) {
           echo '<td>&nbsp;</td>';
        } 
        
        if (in_array("costopromedio", $vercosto)) {
            echo '<td>&nbsp;</td>';
        }
        
        if (in_array("costoactual", $vercosto)) {
           echo '<td align="right"><strong>'.number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.').'</strong></td>';
        }
        
        if (in_array("costopromedio", $vercosto)) {
           echo '<td align="right"><strong>'.number_format(sprintf("%01.2f", $suma_factura_precio), 2, ',', '.').'</strong></td>';
        }
        
        if (in_array("preciominimo", $precio)) {
           echo '<td>&nbsp;</td>';
        }
        if (in_array("preciomayor", $precio)) {
           echo '<td>&nbsp;</td>';
        }
        if (in_array("preciodetal", $precio)) {
           echo '<td>&nbsp;</td>';
        }
?>   
        </tr>
        <tr>		  	
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="right"><label>Totales:</label></td>
          <?php 
          if (in_array("costopromedio", $vercosto)) {
            echo '<td align="right">'.number_format(sprintf("%01.2f", $TCPsucP), 2, ',', '.').'</td>';
           }else{
            echo '<td align="right">'.number_format(sprintf("%01.2f", $TCAsucP), 2, ',', '.').'</td>';   
           }  
           if (in_array("costopromedio", $vercosto)) {
            echo '<td align="right">'.number_format(sprintf("%01.2f", $TCPsuc1), 2, ',', '.').'</td>';
           }else{
            echo '<td align="right">'.number_format(sprintf("%01.2f", $TCAsuc1), 2, ',', '.').'</td>';   
           }
           if (in_array("costopromedio", $vercosto)) {
            echo '<td align="right">'.number_format(sprintf("%01.2f", $TCPsuc2), 2, ',', '.').'</td>';
           }else{
            echo '<td align="right">'.number_format(sprintf("%01.2f", $TCAsuc2), 2, ',', '.').'</td>';   
           }
           if (in_array("costopromedio", $vercosto)) {
            echo '<td align="right">'.number_format(sprintf("%01.2f", $TCPsuc3), 2, ',', '.').'</td>';
           }else{
            echo '<td align="right">'.number_format(sprintf("%01.2f", $TCAsuc3), 2, ',', '.').'</td>';   
           }
          ?>
          
          <td align="right">&nbsp;</td>
<?php 
        if (in_array("costoactual", $vercosto)) {
           echo '<td>&nbsp;</td>';
        } 
        
        if (in_array("costopromedio", $vercosto)) {
            echo '<td>&nbsp;</td>';
        }
        
        if (in_array("costoactual", $vercosto)) {
           echo '<td>&nbsp;</td>';
        }
        
        if (in_array("costopromedio", $vercosto)) {
           echo '<td>&nbsp;</td>';
        }
        
        if (in_array("preciominimo", $precio)) {
           echo '<td>&nbsp;</td>';
        }
        if (in_array("preciomayor", $precio)) {
           echo '<td>&nbsp;</td>';
        }
        if (in_array("preciodetal", $precio)) {
           echo '<td>&nbsp;</td>';
        }
?>   
        </tr>
        </tfoot>
</table>
</div>
<?php
}else{
    echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
<?php
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('bootstrap.min.js');
//echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
    echo $this->Html->script('plugins/datatables/dataTables.buttons.min.js');
    echo $this->Html->script('plugins/datatables/buttons.flash.min.js');
    echo $this->Html->script('plugins/datatables/jszip.min.js');
    echo $this->Html->script('plugins/datatables/pdfmake.min.js');
    echo $this->Html->script('plugins/datatables/vfs_fonts.js');
    
    echo $this->Html->script('plugins/datatables/buttons.html5.min.js');
    echo $this->Html->script('plugins/datatables/buttons.print.min.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
	$('[data-toggle="tooltip"]').tooltip();

    $('#ListadoCuadre').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "dom": "Bfrtip",
       "buttons": [
            'copy', 'csv', 'excel', 'print'
        ],
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
            "sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
            "sSearch": "Buscar:",
        "slengthMenu": "Mostrar _MENU_ records per page"
	} 
    });
   

  });
 
</script>
<!-- /.box-body -->
