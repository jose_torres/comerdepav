<?php
	//print_r($data);
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<h3><?php echo $titulo; ?></h3>
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){
		
		if ($BANCO!= $row[0]['descripcion']){
			
			if($CONT!=0){
		?>
		<tfoot>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  				  
		  <td>&nbsp;</td>		  				  
		  <td>&nbsp;</td>		  				  
		  <td align="right"><label>Totales:</label></td>		  
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></label></td>		  
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;	
		?>
		<table class="table table-bordered">
		 <caption><label><?php echo $row[0]['rif'].' '.$row[0]['descripcion']; ?></label></caption> 
		<tr>
		  <th width="10%">Tipo de Opr.</th>	
		  <th width="10%">F/Emisi&oacute;n</th>
		  <th width="10%">F/Vcto</th>
		  <th width="5%">D&iacute;as</th>
		  <th width="5%">Numero</th>		  		 		  
		  <th width="30%">Descripcion</th>		  		 		  
		  <th>Monto</th>	
		</tr>
		<?php
		}
		?>
		<?php		
		$suma_factura=$suma_factura + $row[0]['credito'];	
		?>
		<tr>
		  <td><?php echo $row[0]['tipomovimiento'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['femision']));?></td>	  		  
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fvencimiento']));?></td>	
		  <td><?php echo $row[0]['dias'];;?></td>	
		  <td><?php echo $row[0]['numero'];?></td>
		  <td><?php echo $row[0]['concepto'];?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['credito']), 2, ',', '.');?></td>		  
		 </tr> 
		
		<?php
		$BANCO=$row[0]['descripcion'];
		$CONT=$CONT+1;		
		}
		
		
		
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		   
		  <td>&nbsp;</td>		   
		  <td align="right"><label>Totales:</label></td>			  	  
		  <td align="right"><label><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></label></td>		  
		</tr>
		</tfoot>
 </table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
</div>
<!-- /.box-body -->
