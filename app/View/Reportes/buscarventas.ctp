<?php
	echo $this->Html->css('datatables/dataTables.bootstrap.css');
?>
<!-- /.box-header -->
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Listado de Ventas y Devoluciones Encontrados</h3>
	</div>
<table class="display table table-bordered" id="">
		 <caption><h4><?php echo ' Devoluciones de Ventas a Credito'; ?></h4></caption> 
		  <thead>
		<tr>
		   <th width="7%">Fecha</th>
		  <th width="7%">Hora</th>
		  <th width="7%">Devoluci&oacute;n</th>
		  <th width="25%">Cliente</th>
		  <th>Monto Base</th>
		  <th>Iva</th>
		  <th>Total</th>
		 <th>Motivo</th>
		  <th>Fact. Afecta</th>
		  <th>Vendedor</th>		  
		</tr>
		 </thead>
		  <tbody>
 <?php
/*
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;
$total_ventas['costo']=$suma_costo;
*/
		$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_costo=0;
		$BANCO='';$CONT=0;	$base = 0; $iva=0;
		foreach ($data as $row){
		$base =	$row[0]['baseimp2'] + $row[0]['baseimp1'] + $row[0]['baseimp3'];
		$iva = $row[0]['ivaimp2'] + $row[0]['ivaimp1'] + $row[0]['ivaimp3'];
		$suma_factura=$suma_factura + $base;
		$suma_iva=$suma_iva + $iva;
		$suma_bruto=$suma_bruto + $base + $iva;
		$suma_costo=$suma_costo + $row[0]['costo'];	
		$basexcien= 0;
		if ($base>0){
			$basexcien = ($base+$row[0]['montoexento']-$row[0]['costo'])/$base*100;
		}
			?>
		<tr>
		 <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>
		 <td><?php echo date("h:i:s",strtotime($row[0]['fecha']));?></td>
		  <td><?php echo str_pad($row[0]['coddevolucion'], 10, "0", STR_PAD_LEFT);?></td>
		  <td><?php echo $row['cliente']['rif'].'=>'.$row['cliente']['descripcion']?></td>
		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $base), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $base + $iva), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['observacion'];?></td>
		  <td><?php echo $row[0]['numerofactura'];?></td>
		  <td><?php echo $row['vendedore']['descripcion'];?></td>
		 </tr> 
		
		<?php
		$BANCO=$row[0]['estatus'];
		$CONT=$CONT+1;
		}
		?>
<?php
$total_Devoluciones['factura']=$suma_factura;
$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;
$total_Devoluciones['costo']=$suma_costo;

$basexcien1= 0;
if ($suma_factura>0){
	$basexcien1 = ($suma_factura-$suma_costo)/$suma_factura*100;
}


?>
 </tbody>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		   <td>&nbsp;</td>
		   <td>&nbsp;</td>			  
		</tr>
	</tfoot>
 </table>	
	
	</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	$(document).ready(function() {
    $('table.display').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "scrollX": true,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		},

    });
} );
  
 
</script>		
<!-- /.box-body -->
