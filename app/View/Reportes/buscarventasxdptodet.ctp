<?php
     echo $this->Html->css('datatables/dataTables.bootstrap.css');
     echo $this->Html->css('datatables/rowGroup.dataTables.min.css');
?>
<style>
    table.dataTable tr.group-end td {
    text-align: right;
    font-weight: normal;
}
</style>
<!-- /.box-header -->
<div class="box">
<div class="box-header">
  <h3 class="box-title">Resumen de Ventas por Departamentos Detallado por Productos</h3>
</div>
<table class="display table table-bordered" id="example">
    <caption><h4><?php echo $titulo; ?></h4></caption> 
    <thead>	
        <tr>		  
            <th width="25%">Departamento</th>
            <th width="7%">Codigo</th>		  
            <th width="25%">Productos</th> 
             <th align="right">Cantidad</th>
            <th align="right">Costo</th>            		  
            <th align="right">Monto Base</th>		  
            <th align="right">Iva</th>
            <th align="right">Total</th>
            <th align="right">Utilidad</th>
        </tr>
    </thead>	
    <tbody>
<?php
       $suma_factura=0;$suma_factura_costo=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
       $BANCO='';$CONT=0;$IVA=0;	 $suma_cantidad = 0;	
       foreach ($data as $row){		
            
       $suma_factura=$suma_factura + $row[0]['venta'];       
       $suma_iva=$suma_iva + $row[0]['iva'];       
       $suma_bruto=$suma_bruto + $row[0]['venta'] + $row[0]['iva'];
       $suma_cantidad = $suma_cantidad + $row[0]['cantidad'];
       $suma_factura_costo = $suma_factura_costo + $row[0]['costo']; 
        $UTIL = 0;
		if($row[0]['venta']>0){
			$UTIL = ($row[0]['venta'] - $row[0]['costo'])/$row[0]['venta']*100;
		}
        
       if($BANCO!=$row[0]['coddep']){
         /* echo '<tr>';
          echo '<td colspan="6">'.$row[0]['descripcion'].'</td>';
          echo '</tr>';*/
       }
?>
       <tr>
         <td><?php echo $row[0]['coddep'].' '.$row[0]['descripcion'];?></td>  
         <td><?php echo $row[0]['codigo'];?></td>
         <td><?php echo $row[0]['nombre'];?></td>         
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['cantidad']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['costo']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['venta']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f",$row[0]['iva']), 2, ',', '.');?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['venta'] + $row[0]['iva'] ), 2, ',', '.');?></td>
          <td align="right"><?php echo number_format(sprintf("%01.2f",$UTIL), 2, ',', '.');?></td>          
        </tr> 		
<?php		
        $CONT=$CONT+1;
        $BANCO= $row[0]['coddep'];
        }
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;	
$UTIL = 0;
if($suma_factura>0){
	$UTIL = ($suma_factura-$suma_factura_costo)/$suma_factura*100;
}	
?>

    </tbody>
    <tfoot>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>      	
            <td align="right"><label>Totales:</label></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_cantidad), 2, ',', '.');?></td>		  
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.');?></td>				  
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
             <td align="right"><?php echo number_format(sprintf("%01.2f", $UTIL), 2, ',', '.');?></td>		  
        </tr>
    </tfoot>
</table>
 
</div>
<?php
    //echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('funciones.js');
    echo $this->Html->script('plugins/datatables/jquery-3.3.1.js');
    echo $this->Html->script('bootstrap.min.js');
    echo $this->Html->script('plugins/datatables/jquery.dataTables.min.js');
    echo $this->Html->script('plugins/datatables/dataTables.rowGroup.min.js');
    //echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
    echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
    
        echo $this->Html->script('plugins/datatables/dataTables.buttons.min.js');
    echo $this->Html->script('plugins/datatables/buttons.flash.min.js');
    echo $this->Html->script('plugins/datatables/jszip.min.js');
    echo $this->Html->script('plugins/datatables/pdfmake.min.js');
    echo $this->Html->script('plugins/datatables/vfs_fonts.js');
    
    echo $this->Html->script('plugins/datatables/buttons.html5.min.js');
    echo $this->Html->script('plugins/datatables/buttons.print.min.js');
    echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
    $(document).ready(function() {
    var groupColumn = 0;
    var table =$('#ventasDptoDet').DataTable({
      "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],
        "order": [[ groupColumn, 'asc' ]],
      "rowGroup": {
            endRender: function ( rows, group ) {
                var avg = rows
                    .data()
                    .pluck(5)
                    .reduce( function (a, b) {
                        b = formato_ingles_c(b);
                        return a + b.replace(/[^\d]/g, '')*1;
                    }, 0);
 
                return 'Average salary in '+group+': '+
                    $.fn.dataTable.render.number(',', '.', 2, 'Bs').display( avg );
            },
            dataSrc: 0
        },  
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      //"scrollX": true,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		},
      "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
 
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );
 
                    last = group;
                }
            } );
        }, 
       "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
                     
            
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    //a = formato_ingles_c(a);
                   //alert ('a:'+a);
                    b = formato_ingles_c(b);
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    //a = formato_ingles_c(a);
                    b = formato_ingles_c(b);
                    return parseFloat(a+b);
                }, 0 );
pageTotal = $.fn.dataTable.render.number('.', ',', 2, '').display( pageTotal );
total = $.fn.dataTable.render.number('.', ',', 2, '').display( total );
            // Update footer
            $( api.column( 5).footer() ).html(
                'Pag. Bs. '+pageTotal +' (Global Bs.'+ total +')'
            );
        },
        

    });
   
   // Order by the grouping
    $('#ventasDptoDet tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === groupColumn && currentOrder[1] === 'asc' ) {
            table.order( [ groupColumn, 'desc' ] ).draw();
        }
        else {
            table.order( [ groupColumn, 'asc' ] ).draw();
        }
    } );
    
    $('#example').DataTable( {
		"dom": "Blfrtip",
       "buttons": [
            "copy", "csv"/*, "excel"*/
        ],
        "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		},
        "columnDefs": [
            { "visible": false, "targets": groupColumn }
        ],        
        order: [[0, 'asc']],
        rowGroup: {
            
            endRender: function ( rows, group ) {
                //return group +' ('+rows.count()+')';
                var totalCant = rows
                    .data()
                    .pluck(3)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                totalCant = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( totalCant );
                
                var totalCosto = rows
                    .data()
                    .pluck(4)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                totalCosto = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( totalCosto );
                
                 var salaryAvg = rows
                    .data()
                    .pluck(7)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                salaryAvg = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( salaryAvg );
                var totalBase = rows
                    .data()
                    .pluck(5)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                totalBase = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( totalBase );
                
                var totalIva = rows
                    .data()
                    .pluck(6)
                    .reduce( function (a, b) {
                     b = formato_ingles_c(b);                        
                    return parseFloat(a+b);
                    }, 0);
                totalIva = $.fn.dataTable.render.number('.', ',', 2, 'Bs. ').display( totalIva );
                
                 return $('<tr/>')
                    .append( '<td colspan="2"><strong>Totales por '+group+'</strong></td>' )
                    
                    .append( '<td><strong>'+totalCant+'</strong></td>' )
                    .append( '<td><strong>'+totalCosto+'</strong></td>' )
                    .append( '<td><strong>'+totalBase+'</strong></td>' )
                    .append( '<td><strong>'+totalIva+'</strong></td>' )
                    .append( '<td><strong>'+salaryAvg+'</strong></td>' )
                    .append( '<td><strong></strong></td>' )
                    ;
                    
            },
            dataSrc:0
        }
    } );
} );  
 
</script>	
<!-- /.box-body -->
