<?php
ini_set('memory_limit', '512M');
$orient='P';$poc_prod='25%';$poc_dpto='25%';
if(count($datos['precio'])>3){
$orient='L';$poc_prod='20%';$poc_dpto='20%';
}elseif(count($datos['precio'])<=2){
$poc_prod='35%';$poc_dpto='35%';
}
$this->tcpdf->core->SetPageOrientation($orient);
$this->tcpdf->core->SetOrientacion($orient);
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>
		  <th width="5%" align="center">Codigo</th>
		  <th width="'.$poc_prod.'">Departamento</th>
		  <th width="'.$poc_dpto.'" align="center">Producto</th>			
	';
if($datos['precio']){

}
$cont=0;

foreach($datos['precio'] as $reg){
	if($cont>=1){
	if($reg=='preciominimo'){
		$texto=$texto.'<th width="12%" align="center">Prec. Minimo</th>';
		$texto=$texto.'<th width="7%" align="center">&#37; Utilidad</th>';
	}elseif($reg=='preciomayor'){
		$texto=$texto.'<th width="12%" align="center">Prec. Mayor</th>';
		$texto=$texto.'<th width="7%" align="center">&#37; Utilidad</th>';
	}elseif($reg=='preciodetal'){
		$texto=$texto.'<th width="12%" align="center">Prec. Detal</th>';
		$texto=$texto.'<th width="7%" align="center">&#37; Utilidad</th>';
	}
	}
	$cont = $cont + 1;
}	
$texto=$texto.'</tr></thead>';	
//$texto=$texto.'<tbody>';
$suma_factura=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;
$suma_factura_retencion=0;$suma_factura_pagado=0;
//$texto="";
$BANCO='';$CONT=0;
foreach ($data as $row){
	 	 	

 if($BANCO!= $row['dp']['codigo']){
/*	$texto=$texto.'
	<tr>
	  <td width="5%" align="left"><b>'.$row['dp']['codigo'].'</b></td>	
	  <td align="left" colspan="7"><b>'.$row[0]['descripcion'].'</b></td>			  
	</tr>';*/
	
	}	
$texto=$texto.'
    <tr>	  
        <td width="5%" align="left">'.$row[0]['codigo'].'</td>
        <td width="'.$poc_prod.'" align="left">'.$row['dp']['codigo'].'</td>
        <td width="'.$poc_dpto.'" align="left">'.$row[0]['nombre'].'</td>
';
$cont=0;
foreach($datos['precio'] as $reg){
	if($cont>=1){
	if($reg=='preciominimo'){
		$texto=$texto.'<td align="right" width="12%">'.number_format(sprintf("%01.2f", $row[0]['preciominimo']), 2, ',', '.').'</td>';
		$camp="utilidad_min_act";
		if($datos['costo'] == 'costopromedio') $camp="utilidad_min_prom";
		$texto=$texto.'<td align="right" width="7%">'.number_format(sprintf("%01.2f", $row[0][$camp]), 2, ',', '.').'</td>';
	}elseif($reg=='preciomayor'){
		$texto=$texto.'<td align="right" width="12%">'.number_format(sprintf("%01.2f", $row[0]['preciomayor']), 2, ',', '.').'</td>';
		$camp="utilidad_may_act";
		if($datos['costo'] == 'costopromedio') $camp="utilidad_may_prom";
		$texto=$texto.'<td align="right" width="7%">'.number_format(sprintf("%01.2f", $row[0][$camp]), 2, ',', '.').'</td>';
	}elseif($reg=='preciodetal'){
		$texto=$texto.'<td align="right" width="12%">'.number_format(sprintf("%01.2f", $row[0]['preciodetal']), 2, ',', '.').'</td>';
		$camp="utilidad_det_act";
		if($datos['costo'] == 'costopromedio') $camp="utilidad_det_prom";
		$texto=$texto.'<td align="right" width="7%">'.number_format(sprintf("%01.2f", $row[0][$camp]), 2, ',', '.').'</td>';
	}
	}
	$cont = $cont + 1;
}

$texto=$texto.'	</tr>';
	$CONT=$CONT+1;
	
	$BANCO=$row['dp']['codigo'];
}	

$texto=$texto.'</table>';

//$this->tcpdf->core->Ln(2);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('costos_utilidad.pdf', 'D');
?>
