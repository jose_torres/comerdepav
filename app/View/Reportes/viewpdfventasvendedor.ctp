<?php

$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>		  
		   <th width="7%"  align="center">Factura</th>
		  <th width="24%" align="center">Cliente</th>
		  <th width="7%" align="center">Fecha</th>
		  <th align="center">Monto Neto</th>
		  <th align="center">Contado</th>
		  <th align="center">Cr&eacute;dito</th>		  
		  <th align="center">Iva</th>
		  <th align="center">Total</th>
		  <th width="7%" align="center">Tipo</th>>	
		</tr></thead>	
	';
//$texto=$texto.'<tbody>';
$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
$BANCO='';$CONT=0;$CONT_FILA=0;
foreach ($data as $row){
	 	 	
	if($BANCO!= $row['Vendedore']['descripcion'] && $CONT_FILA>0){
	$texto=$texto.'
		<tr>
		  <td width="7%" >&nbsp;</td>	
			<td width="25%">&nbsp;</td>	
			<td align="right"><b>TOTALES:</b></td>	
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</b></td>
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.').'</b></td>
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.').'</b></td>
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</b> </td>
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</b></td>		  		
			<td >&nbsp;</td>			 		  
		</tr>';
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
	}	 	 	
	 	 	
	$suma_factura=$suma_factura + $row['Venta']['baseimp2'];
	$suma_iva=$suma_iva + $row['Venta']['ivaimp2'];
	$suma_bruto=$suma_bruto + $row['Venta']['baseimp2'] + $row['Venta']['ivaimp2'];	
	if($row['Venta']['estatus']=='P' || substr($row['Venta']['numerofactura'], 0, 1)=='B'){
		$tipo_compra='Contado';$suma_factura_contado=$suma_factura_contado + $row['Venta']['baseimp2'];$contado=$row['Venta']['baseimp2'];$credito=0;
	  }elseif($row['Venta']['estatus']=='A' || substr($row['Venta']['numerofactura'], 0, 1)=='A'){
		$tipo_compra='A Credito';$suma_factura_credito=$suma_factura_credito + $row['Venta']['baseimp2'];$credito=$row['Venta']['baseimp2'];$contado=0;
	  }elseif($row['Venta']['estatus']=='E'){
		$tipo_compra='Devuelto';$credito=0;$contado=0;
	  }
	
 if($BANCO!= $row['Vendedore']['descripcion']){
	$texto=$texto.'
	<tr>
	  <td width="10%" align="left"><b>Vendedor:</b></td>	
	  <td align="left" colspan="7"><b>'.$row['Vendedore']['descripcion'].'</b></td>		  
	</tr>';
	$CONT_FILA=0;
	}	
$texto=$texto.'
	<tr>	  
	  <td width="7%" align="left">'.$row['Venta']['numerofactura'].'</td>
	  <td width="25%" align="left">'.$row['Cliente']['descripcion'].'</td>
	  <td width="7%">'.date("d-m-Y",strtotime($row['Venta']['fecha'])).'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $contado), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $credito), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row['Venta']['ivaimp2']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row['Venta']['baseimp2'] + $row['Venta']['ivaimp2']), 2, ',', '.').'</td>
	  <td width="9%">'.$tipo_compra.'</td>	  
	</tr>';
	$BANCO=$row['Vendedore']['descripcion'];	
	$CONT_FILA=$CONT_FILA+1;
	$CONT=$CONT+1;
}	

$texto=$texto.'
		<tr>
			<td width="7%" >&nbsp;</td>	
			<td width="25%">&nbsp;</td>	
			<td align="right"><b>TOTALES:</b></td>	
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.').'</b></td>
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.').'</b></td>
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.').'</b></td>
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.').'</b> </td>
			<td align="right"><b>'.number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.').'</b></td>		  		
			<td >&nbsp;</td>				  
		</tr>';
$texto=$texto.'</table>';

//$this->tcpdf->core->Ln(2);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('ventas_por_vendedor.pdf', 'D');
?>
