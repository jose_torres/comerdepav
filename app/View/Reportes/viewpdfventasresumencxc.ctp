<?php
$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetOrientacion("P");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($empresa['Empresa']['nombre'],'',10);
$titulo[1]='';$tc_forma[1]='';$tc_size[1]=10;
$titulo[2]='';$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
$this->tcpdf->core->SetTituloPlantilla($encabezado['titulo_reporte'],'',10);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(8);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(30);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'VENTAS');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
//Datos de la Retencion
$this->tcpdf->core->SetFont('helvetica','',7);
$texto="";
$texto='<table border="0" cellpadding="1" cellspacing="0">
	<thead><tr>				 
		  <th width="30%" align="center">Cliente</th>
		  <th width="8%" align="center">Nro Factura</th>
		  <th width="12%" align="center">Fecha Emisi&oacute;n</th>		  
		  <th width="12%" align="center">Fecha de Vencimiento</th>
		  <th width="6%" align="center">Dias</th>
		  <th align="center">Monto</th>
		  <th align="center">Saldos</th>			
	</tr></thead>
	';
$suma_facturas['monto']=$suma_facturas['saldo']=0;$CONT=0;
foreach ($data as $row){	
	$suma_facturas['monto']=$suma_facturas['monto'] + $row[0]['monto'];
	$suma_facturas['saldo']= $suma_facturas['saldo'] + ($row[0]['monto']-$row[0]['cobros']);
$texto=$texto.'
	<tr>
	  <td width="30%" align="left">'.$row['cliente']['descripcion'].'</td>	
	  <td width="8%" align="left">'.$row[0]['numerofactura'].'</td>			
	  <td width="12%">'.date("d-m-Y",strtotime($row[0]['fecha'])).'</td>	  
	  <td width="12%">'.date("d-m-Y",strtotime($row[0]['fvencimiento'])).'</td>
	  <td width="6%" align="center">'.$row[0]['dias'].'</td>	
	  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['monto']), 2, ',', '.').'</td>
	  <td align="right">'.number_format(sprintf("%01.2f", $row[0]['monto']-$row[0]['cobros']-$row[0]['notacredito']), 2, ',', '.').'</td>
	</tr>';	
}	
$texto=$texto.'
	<tr>	
	  <td width="30%">&nbsp;</td>	
	  <td width="8%">&nbsp;</td>	
	  <td width="12%">&nbsp;</td>	
	  <td width="12%">&nbsp;</td>	
	  <td width="6%" align="right"><b>Totales:</b></td>	
	  <td align="right">'.number_format(sprintf("%01.2f", $suma_facturas['monto']), 2, ',', '.').'</td>		  
	  <td align="right">'.number_format(sprintf("%01.2f", $suma_facturas['saldo']), 2, ',', '.').'</td>			
	</tr>';
$texto=$texto.'</table>';
//$this->tcpdf->core->writeHTML('<b>VENTAS</b>', true, false, true, false, 'L');
$this->tcpdf->core->SetX(2);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('resumencxc.pdf', 'D');
?>
