<?php
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';*/
	 echo $this->Html->css('datatables/dataTables.bootstrap.css');

?>
<!-- /.box-header -->
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Listado de Ventas y Devoluciones Encontrados</h3>
	</div>
	<table class="display table table-bordered" id="">
		 <caption><h4><?php echo $titulo; ?></h4></caption> 
		  <thead>	
		<tr>
		  <th width="7%">Fecha</th>
		  <th width="7%">Hora</th>
		  <th width="7%">Factura</th>
		  <th width="25%">Cliente</th>
		  
		  <th>Monto Base</th>		  
		  <th>Iva</th>
		  <th>Total</th>
		  <th>Tipo de Ventas</th>
		  <th>Vendedor</th>
		</tr>
		 </thead>	
		  <tbody>
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
		$BANCO='';$CONT=0;$IVA=0;	
		foreach ($data as $row){		
			$base_vp=$iva_vp=0;
			/*foreach($row['Ventaproducto'] as $registro){
				$base_vp = $base_vp + round($registro['cantidad']*$registro['precio'],2);
				$iva_vp = $iva_vp + round($registro['cantidad']*$registro['precio'] * $registro['iva']/100,2);
				$IVA = $registro['iva'];
			}*/
			//$row['Venta']['porimp2'] = $IVA;
			$base_vp = $row['Venta']['baseimp1'] + $row['Venta']['baseimp2'] + $row['Venta']['baseimp3'];
			$row['Venta']['baseimp2'] = $base_vp;	
		$suma_factura=$suma_factura + $row['Venta']['baseimp2'];
		//$suma_iva=$suma_iva + round($row['Venta']['baseimp2']*$row['Venta']['porimp2']/100,2);
		$suma_iva=$suma_iva + ($row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] + $row['Venta']['ivaimp3']);
		//$suma_bruto=$suma_bruto + $row['Venta']['baseimp2'] + round($row['Venta']['baseimp2']*$row['Venta']['porimp2']/100,2);
		$suma_bruto=$suma_bruto + $row['Venta']['baseimp2'] + ($row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] + $row['Venta']['ivaimp3']);
			?>
		<tr>
		  <td><?php echo date("d-m-Y",strtotime($row['Venta']['fecha']));?></td>
		  <td><?php echo $row['Venta']['hora'];?></td>		
		  <td><?php echo $row['Venta']['numerofactura'];?></td>
		  <td><?php echo $row['Cliente']['rif'].'=>'.$row['Cliente']['descripcion']?></td>
		  	  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f",  round(($row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] + $row['Venta']['ivaimp3']),2)), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2'] + round(($row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] + $row['Venta']['ivaimp3']),2)), 2, ',', '.');?></td>		  
		  <td><?php if($row['Venta']['estatus']=='P'){
				echo 'Contado';
			  }elseif($row['Venta']['estatus']=='A'){
				echo 'Credito';
			  }elseif($row['Venta']['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>
		  <td><?php echo $row['Vendedore']['descripcion'];?></td>
		 </tr> 		
		<?php		
		$CONT=$CONT+1;
		}
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;		
$total_ventas['factura_contado']=$suma_factura_contado;		
$total_ventas['factura_credito']=$suma_factura_credito;		
		?>

 </tbody>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
 </table>
 

<table class="display table table-bordered" id="">
		 <caption><h4><?php echo ' Devoluciones de Ventas '; ?></h4></caption> 
		  <thead>
		<tr>
		   <th width="7%">Fecha</th>
		  <th width="7%">Hora</th>
		  <th width="7%">Devoluci&oacute;n</th>
		  <th width="25%">Cliente</th>	  
		  <th>Monto Base</th>
		  <th>Iva</th>
		  <th>Total</th>
		  <th>Tipo de Ventas</th>
		  <th>Fact. Afecta</th>
		</tr>
		 </thead>
		  <tbody>
<?php
$suma_factura=0;$suma_factura_contado=$suma_factura_credito=0; $suma_iva=0; $suma_bruto=0;
		$BANCO='';$CONT=0;	$base = 0; $iva=0;
		foreach ($data_dev as $row){
		$base =	$row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3'];
		$iva = $row['Devolucionventa']['ivaimp2'] + $row['Devolucionventa']['ivaimp1'] + $row['Devolucionventa']['ivaimp3'];
		$suma_factura=$suma_factura + $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3'];
		//$suma_iva=$suma_iva + round($row['Devolucionventa']['baseimp2']*$row['Devolucionventa']['porimp2']/100,2) + round($row['Devolucionventa']['baseimp1']*$row['Devolucionventa']['porimp1']/100,2) + round($row['Devolucionventa']['baseimp3']*$row['Devolucionventa']['porimp3']/100,2);
		$suma_iva=$suma_iva + $row['Devolucionventa']['ivaimp2'] + $row['Devolucionventa']['ivaimp1'] + $row['Devolucionventa']['ivaimp3'];
		$suma_bruto=$suma_bruto + $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3'] + $row['Devolucionventa']['ivaimp2'] + $row['Devolucionventa']['ivaimp1'] + $row['Devolucionventa']['ivaimp3'];
			?>
		<tr>
		  <td><?php echo date("d-m-Y",strtotime($row['Devolucionventa']['fecha']));?></td>
		  <td><?php echo date("h:i:s",strtotime($row['Devolucionventa']['fecha']));?></td>
		  <td><?php echo str_pad($row['Devolucionventa']['coddevolucion'], 10, "0", STR_PAD_LEFT);?></td>
		  <td><?php echo $row['Venta']['Cliente']['rif'].'=>'.$row['Venta']['Cliente']['descripcion']?></td>
		 		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $base), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Devolucionventa']['ivaimp2'] + $row['Devolucionventa']['ivaimp1'] + $row['Devolucionventa']['ivaimp3']), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $base + $iva), 2, ',', '.');?></td>
		  <td><?php if($row['Venta']['estatus']=='P'){
				echo 'Contado';
			  }elseif($row['Venta']['estatus']=='A'){
				echo 'Credito';
			  }elseif($row['Venta']['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>
		  <td><?php echo $row['Venta']['numerofactura'];?></td>
		 </tr> 		
		<?php		
		$CONT=$CONT+1;
		}
		?>		
<?php
$total_Devoluciones['factura']=$suma_factura;
$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;
$total_Devoluciones['factura_contado']=$suma_factura_contado;		
$total_Devoluciones['factura_credito']=$suma_factura_credito;	
?>
 </tbody>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		<tr>
		  <td align="right"  colspan='4'><label>Totales (Ventas - Devoluciones):</label></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura']- $total_Devoluciones['factura']), 2, ',', '.');?></td>		 
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.');?></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.');?></td>	
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	  
		</tr>
	</tfoot>
 </table>
		
	</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	$(document).ready(function() {
    $('table.display').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "scrollX": true,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		},
/*	{
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 6 ).footer() ).html(
                '$'+pageTotal +' ( $'+ total +' total)'
            );
        }	
	  }	 */
    });
} );
  
 
</script>	
<!-- /.box-body -->
