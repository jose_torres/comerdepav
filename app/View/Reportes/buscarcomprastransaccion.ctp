<?php
	//echo 'Esto es Buscar Ventas';
	//print_r($data);
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<?php
		$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){
		//if ($BANCO!= $row['Venta']['estatus']){
		if ($BANCO!= $row[0]['nrodocumento']){
			if($CONT!=0){
		?>
		<tfoot>
		<tr>		  	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>			  
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_precio), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", ($suma_factura_precio-$suma_utilidad)), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_utilidad), 2, ',', '.');?></td>		  
		    
		</tr>
		</tfoot>
		 </table>
		<?php
			}
		$suma_factura=0;$suma_factura_costo=0;$suma_factura_precio=0; $suma_utilidad=0;
		?>
		<table class="table table-bordered">
		 <caption><label><?php echo $row[0]['nrodocumento'].' '.$row['proveedor']['descripcion'].'&nbsp;&nbsp;&nbsp;Fecha:&nbsp;'.date("d-m-Y",strtotime($row[0]['fecha'])); ?></label></caption> 
		<tr>
		  <th width="7%">C&oacute;digo</th>
		  <th width="25%">Producto</th>
		  <th width="10%">U&#47;R</th>
		  <th width="7%">Cant.</th>
		  <th width="7%">Costo</th>
		  <th>Costo&#47;Total</th>		  		  
		  <th>Descuento</th>
		  <th>Total</th>		  		  
		</tr>
		<?php
		}
		?>
		<?php		
		$suma_factura=$suma_factura + $row[0]['cantidad'];		
		$suma_factura_costo=$suma_factura_costo + $row[0]['costo'];		
		$suma_factura_precio=$suma_factura_precio + $row[0]['total'];		
		$suma_utilidad=$suma_utilidad + ($row[0]['total']-($row[0]['total']-$row[0]['totalcostodescuento']));		
			?>
		<tr>
		  <td><?php echo $row[0]['codigo'];?></td>
		  <td><?php echo $row[0]['nombre'];?></td>
		  <td><?php echo $row[0]['unidad'];?></td>		  
		  <td align="right"><?php echo $row[0]['cantidad'];?></td>		  		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['costo']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total']-$row[0]['totalcostodescuento']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total']-($row[0]['total']-$row[0]['totalcostodescuento'])), 2, ',', '.');?></td>			  
		 </tr> 
		
		<?php
		$BANCO=$row[0]['nrodocumento'];
		$CONT=$CONT+1;
		}
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>		  	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>			  
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_costo), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_precio), 2, ',', '.');?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", ($suma_factura_precio-$suma_utilidad)), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_utilidad), 2, ',', '.');?></td>		  
		    
		</tr>
		</tfoot>
 </table>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
	</div>
<!-- /.box-body -->
