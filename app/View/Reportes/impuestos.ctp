<?php
//echo $this->element('menuinterno',$datos_menu);
//echo $this->Html->script('prototype');
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');

?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	 // var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	
	function actualizar(nombrediv,url,id,datos){
		
        $('#'+nombrediv).html('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');
		
		var tipoventa = '&tipocontribuyente=0-';var cont=1;
		$('#ReporteTipocontribuyente option:checked').each(function(){
		tipoventa += $(this).val() + '-'; 
		});
		fin = tipoventa.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
		tipoventa = tipoventa.substr( 0, fin ); // elimino la coma final
				
		$.ajax({
			type: 'POST',
			url: url+id,
			data: datos,
			dataType: 'html'
		})

		.done(function( html ) {					
			$( '#'+nombrediv ).html( html );
		});

    }
	
</script>
<fieldset id="personal" >
<legend>REPORTE DE IMPUESTOS</legend>

<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'viewpdfimpuestos/1','id'=>'frm','name'=>'frm'));
?>	
<?php
/*$sucursal['comerdepa']='Principal';
$sucursal['superdepa']='Superdepa';
$sucursal['comerdepa_3']='Sucursal 3';*/
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';
$tiporeporte['Iva_Dia']='Resumen de Iva Por Dia';
$tiporeporte['Libro_Ventas']='Libro de Ventas';
$tiporeporte['Libro_Compras']='Libro de Compras';
		
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Tipo de Reportes:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.tiporeporte',array('div'=>false,'options'=>$tiporeporte,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Reporte",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Mes:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-2">';
echo $this->Form->input('Reporte.mes',array('div'=>false,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Reporte",'class'=>"form-control select2","style"=>"width: 100%;",'type' => 'date', 'dateFormat' => 'M'));
echo '</div>';
echo $this->Html->tag('label', 'A&ntilde;o:', array('class' => 'col-xs-1 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-2">';
echo $this->Form->input('Reporte.year',array('div'=>false,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Reporte",'class'=>"form-control  select2","style"=>"width: 100%;",'type' => 'date', 'dateFormat' => 'Y','minYear' => 2015,'maxYear' => date('Y')));
echo '</div>';
echo '</div>';

$tipofecha['FF']='Fecha Factura';$tipofecha['FP']='Fecha de Pago';		
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Buscar por:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.tipofecha',array('div'=>false,'options'=>$tipofecha,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Contribuyente",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));	
echo '</div>';
$tipoventa['especial']='Especial';$tipoventa['ordinario']='Ordinario';$tipoventa['especial_retencion']='Especial + Retencion';		
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Tipo de Contribuyente:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.tipocontribuyente',array('div'=>false,'options'=>$tipoventa,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Contribuyente",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));	
echo '</div>';

	?>
<button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarimpuestos','','tiporeporte='+document.frm.ReporteTiporeporte.value+'&sucursal='+document.frm.ReporteSucursal.value+'&tipofecha='+document.frm.ReporteTipofecha.value+'&tipocontribuyente='+document.frm.ReporteTipocontribuyente.value+'&mes='+document.frm.ReporteMesMonth.value+'&year='+document.frm.ReporteYearYear.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
<button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	
<?php echo $this->Form->end(); ?>
<center>
<form action="" method="post" name="otro">
<div id="buscar">

</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php //echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/funciones/add", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</center>
</form>
</fieldset>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
</script>
