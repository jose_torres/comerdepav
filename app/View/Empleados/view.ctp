<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<fieldset id="personal" >
<legend class="info"><h2>MOSTRAR UNA EMPLEADO</h2></legend>
<table>
	<tr>
		<td><label>C&eacute;dula:</label></td>
		<td><?php echo $data['Empleado']['cedula']; ?> </td>
	</tr>
	<tr>
		<td><label>Nombre:</label></td>
		<td><?php echo $data['Empleado']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Oficina:</label></td>
		<td><?php echo $data['Oficina']['siglas'].'-'.$data['Oficina']['descripcion']; ?> </td>
	</tr>
</table>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend><strong>Acciones</strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/empleados", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</fieldset>
