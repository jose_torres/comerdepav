<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>		
<form class="pure-form pure-form-aligned" name="frm" id="frm" method="POST" action="<?php echo 'add' ?>">	
<fieldset id="personal" >
<legend  class="info"><h2>REGISTRAR UN EMPLEADO</h2></legend>
<?php
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Empleado.cedula',array('label'=>'C&eacute;dula:','size' =>'40','type'=>'text','div'=>false));
		echo '</div>';
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Empleado.nombre',array('label'=>'Nombre:','size' =>'40','type'=>'text','div'=>false));
		echo '</div>';
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Empleado.oficina_id',array('label'=>'Oficina:','size' =>'1','type'=>'select','div'=>false,'empty' =>'(Seleccione uno ...)'));
		echo '</div>';
	/*	$var2['NO']='NO';$var2['SI']='SI';
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Empleado.revisar',array('label'=>'Revisar Documentos:','size' =>'1','type'=>'select','div'=>false,'options'=>$var2));
		echo '</div>';
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Empleado.aprobar',array('label'=>'Aprobar Documentos:','size' =>'1','type'=>'select','div'=>false,'options'=>$var2));
		echo '</div>';
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Empleado.publicar',array('label'=>'Publicar Documentos:','size' =>'1','type'=>'select','div'=>false,'options'=>$var2));
		echo '</div>';
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Empleado.imprimir',array('label'=>'Imprimir Documentos:','size' =>'1','type'=>'select','div'=>false,'options'=>$var2));
		echo '</div>';
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Empleado.descargar',array('label'=>'Descargar Documentos:','size' =>'1','type'=>'select','div'=>false,'options'=>$var2));
		echo '</div>';*/
?>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend><strong>Acciones</strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Guardar Registro"));
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/empleados", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</fieldset>
</form>
