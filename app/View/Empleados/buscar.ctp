<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="pure-table pure-table-horizontal">
<thead>
    <tr>
        <th ><div class="blanco">C&oacute;digo</div></th>
        <th><div class="blanco">C&eacute;dula</div></th>
        <th>Nombre</th>
        <th>Tipo Trab.</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
    <?php $x=0; foreach ($data as $row): $funcione = $row['Empleado']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="pure-table-odd">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $funcione['id'] ?>    
        </td>
        <td>
            <?php echo $this->Html->link($funcione['cedula'], '/empleados/view/'.$funcione['id']) ?>    
        </td>      
        <td>
            <?php echo $funcione['nombre'] ?>
        </td>
        <td>
            <?php 
		echo $funcione['tipotrab'];
            ?>
        </td>
        <td align="center">
	  <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/empleados/view/".$funcione['id'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/empleados/edit/".$funcione['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Eliminar Registro", "title"=>"Eliminar Registro")) ,"/empleados/delete/".$funcione['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$funcione['nombre'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
