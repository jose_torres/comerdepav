<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php echo $this->Html->charset(); ?>
<title><?php echo $page_title; ?></title>

<?php if (Configure::read() == 0) { ?>
<meta http-equiv="Refresh" content="<?php echo $pause; ?>;url=<?php echo $urlNo; ?>"/>
<?php } ?>
<?php 
	echo $html->css(array('default'));
	echo $javascript->link(array('prototype','windowfiles/dhtmlwindow')); 
	echo $html->css(array('windowfiles/dhtmlwindow'));

?>
<style><!--
P { text-align:center; font:bold 1.1em sans-serif }
A { color:#444; text-decoration:none }
A:HOVER { text-decoration: underline; color:#44E }
--></style>
</head>
<body>
<!--
<div id="wrapper">
  <div id="header">
    <div id="logo">&nbsp;<?//= $html->image("img_acciones32/desktop.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema","align"=>"absmiddle"))?>
    </div> 
    <!-- end div#logo -->
  </div>
  <!-- end div#header -->
<!--  <div id="menu">
    <ul>
      <li class="active"><a href="#">Home</a></li>
      <li><a href="#">Products</a></li>
      <li><a href="#">About</a></li>
      <li><a href="#">Contact</a></li>
    </ul>
  </div> -->
  <!-- end div#menu -->
  <div id="page">
  <!--  <div id="page-bgtop"> -->
      <div id="content">

		<p><!-- <a href="<?php echo $url; ?>"><?php echo $message; ?></a> --></p>
<br /><br /><br /><br /><br /><br /><br /><br /><br />

      </div>
      <!-- end div#content -->
	<br /><br />
      <div style="clear: both; height: 1px"></div>
    </div>
 <!-- </div> -->
  <!-- end div#page -->
  <div id="footer">
    <p>Fecha es <? 
    echo date('l').' '.date('d').' del mes '.date('F').' del a&ntilde;o '.date('Y').' ';?>
    </p>
  </div>
  <!-- end div#footer -->
</div>
<!-- end div#wrapper -->
      <script language="javascript" type="text/javascript">
		if (confirm("<?php echo $message; ?>")) {
		// Respuesta afirmativa...
			//alert("<?php echo $urlSi; ?>")
			 window.open("<?php echo $urlSi; ?>", "", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=400, height=400");
			// alert("Debe permitir el acceso a la ventana emergente ");
			 if('<?php echo $seguir; ?>'=='N'){
				document.location.href="<?php echo $urlNo; ?>";
			}
		}else{
			location.href="<?php echo $urlNo; ?>";
			}  
        //alert("<?php echo $message; ?>");        
		//location.href="<?php echo $url; ?>";
      </script>
</body>
</html>
