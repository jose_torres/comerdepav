<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<?php echo $this->Html->script('prototype'); ?>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<fieldset id="personal" >
<legend>Listado de Usuarios Registrados</legend>
<center>
<form action="" method="post" name="two" 'class'="pure-form pure-form-aligned">
<?php
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Tipohectarea.buscar_tipo',array('label'=>'Buscar Por el usuario:','size' =>'40','type'=>'text','div'=>false));
	echo $this->Html->image('img_acciones/system-search.png',array("onclick"=>"modificar('buscar','usuarios/buscar/',document.two.TipohectareaBuscarTipo.value);", "title"=>"Buscar Registro",'div'=>false,"align"=>"Absmiddle"));
	echo '</div>';
?>
</form>
<form action="" method="post" name="otro">
<div id="buscar" >

<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover">
<thead>
    <tr>
        <th ><div class="blanco"><?php echo $this->Paginator->sort('id','Codigo')?></div></th>
        <th><div class="blanco"><?php echo $this->Paginator->sort('usuario','Usuario')?></div></th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $usuario = $row['Usuario']; ?>    
    <?php 
	 if($x % 2 == 0){ ?>
	<tr class="pure-table-odd">
	<?php } else { ?>
	<tr>
	<?php $x++; } ?>
        <td>
            <?php echo $usuario['id'] ?>    
        </td>
        <td>
            <?php echo $this->Html->link($usuario['usuario'], '/usuarios/view/'.$usuario['id']) ?>    
        </td>      
        <td>
            <?php echo $usuario['created'] ?>
        </td>
        <td>
            <?php 
                if (!empty($usuario['modified'])) echo $usuario['modified'];
                else if (!empty($usuario['updated'])) echo $usuario['updated'];
            ?>
        </td>
        <td align="center">
	  <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/usuarios/view/".$usuario['id'], array('escape'=>false), null); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/usuarios/edit/".$usuario['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/usuarios/delete/".$usuario['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$usuario['usuario'].'"?', false);
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, comenzando con el registro {:start}, terminando en {:end}')
	));
	?>	</p>
	<div class="paging">
		<?php echo $this->Paginator->prev('' . $this->Html->image("img_acciones/goprevious.png", array("alt" => "Ver Pagina Anterior", "title"=>"Ver Pagina Anterior")), array('escape'=>false), null, array('class'=>'disabled','escape'=>false));?>
	  	<?php
			echo $this->Paginator->counter(array('format' => __('P&aacute;gina: %page%', true)));
	?> 
		<?php echo $this->Paginator->next($this->Html->image("img_acciones/gonext.png", array("alt" => "Ver Proxima Pagina", "title"=>"Ver Proxima Pagina ")) . '', array('escape'=>false), null, array('class' => 'disabled','escape'=>false));?><br>
		<?php echo 'P&aacute;ginas: |'.$this->Paginator->numbers();
		?>
	</div>
</div>
	
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/usuarios/add", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
</center>
</form>
</fieldset>
