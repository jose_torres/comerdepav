<?php
	echo $this->element('menu',array('id'=>'2'));
	echo $html->css('login/style');
?>
<br>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
.login_mensaje{
color:#000000;
}   
</style>
<?php //echo $html->css(array('form/pseudoclasses'));?>
<?php echo $javascript->link('seguridad/sha1'); ?>
<script language="javascript" type="text/javascript">
function validar(){
	if (document.two.UsuarioUsuario.value=="") {
			 alert("Introduzca el nombre de usuario"); return false; 
	}else if (document.two.UsuarioClave.value=="") {
			 alert("Introduzca la contraseña"); return false; 
	}else {

/*			objetoform.UsuarioClave2.value = "";*/
			document.two.UsuarioClave2.value = "";
			document.two.submit();
	}
//	return true;
}
</script>
<center>

 <section class="container">
    <div class="login">
      <h1><?php echo $html->image('img_acciones/system-lock-screen.png',array("title"=>"Guardar Registro","align"=>"Absbottom")); ?>Acceso al Sistema</h1>
      <form  method="POST" class="login" action="../usuarios/login">
        <p><?php echo $form->input('Usuario.usuario', array('type' =>'text','label' =>'','placeholder'=>"Usuario",'div'=>false));?></p>
        <p>
			<?php echo $form->password('Usuario.clave2', array('size' =>20,'type'=>'password','onKeyUp'=>'UsuarioClave.value = hex_sha1(UsuarioClave2.value)','placeholder'=>"Clave",'div'=>false));  echo $form->input('Usuario.clave', array('type'=>'hidden')); ?>
        </p>
        <p><a href="javascript:void(0);" onclick="javascript:document.images.captcha.src='<?php echo $html->url('/usuarios/captcha_image');?>?' + Math.round(Math.random(0)*1000)+1"><img id="captcha" src="<?php echo $html->url('/usuarios/captcha_image');?>" alt="Haga click para Refrescar" /> </a>
        </p>
        <p>
         <?php echo $form->input('Usuario.captcha', array('size' => 20,'type' =>'text','label' =>'','placeholder'=>"Tipee las letras de la imagen",'div'=>false));?>
         </p>
        <p class="submit"><?php echo $form->submit('Entrar', array('class'=>'boton','onClick' => 'return validar()')); ?></p>         
      </form>
    </div>	
  </section>
  <section class="about">
    <p class="about-author">
      &copy; 2016 <a href="http://www.audiconsultweb.com" target="_blank">Audiconsult</a> -
      Desarrollado por <a href="http://www.audiconsultweb.com" target="_blank">Ing. Leiban Rivero - Ing. Eliemar Gomez</a>
  </section>
</center>
