<?php
echo $this->renderElement('menu',$datos_menu);
?>
<br />
<script type="text/JavaScript" src="<? echo RUTA_JAVASCRIPT_ROOT;?>prototype.js"></script>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>
<script type="text/JavaScript" src="<? echo RUTA_JAVASCRIPT_ROOT;?>validarvacios.js"></script>
<script type="text/JavaScript" src="<? echo RUTA_JAVASCRIPT_ROOT;?>sha1.js"></script>
<script language="javascript" type="text/javascript">

function Validar(){
		 var objetoform = document.frm;
	 	if (validarnoiguales(objetoform.UsuarioClave,objetoform.UsuarioClave1.value,"Las Contraseñas no son iguales")=='F'){
		return false;}else{
			document.frm.submit();
		}  
}
</script>		
<h1 class="logo2"><a  href=""></a></h1>
<form class="wufoo" name="frm" id="frm" method="POST" action="<?php echo $html->url(empty($params['data']['Usuario']['id'])?'add':'cambiocontrasena') ?>">
	
<fieldset id="personal" >
<legend><label> <?php echo empty($params['data']['Usuario']['id'])?'REGISTRAR':'EDITAR' ?> UN USUARIO</label></legend>
<?php echo empty($params['data']['Usuario']['id']) ? null : $html->hidden('Usuario/id'); ?>

<table>
	<tr>
		<td><label>Empleado:</label></td>
		<td>
				<?  	
 $i=1;
 foreach ($data_empleado as $row) {
	$item = $row['Empleado'];	
	$var[$item['id']]=$item['nombre'];
	$i=$i+1;
	}
?>
<?php echo $html->selectTag('Usuario/empleado_id',$var,$selected = null, $selectAttr = array('class'=>'selectgrande','disabled'=>'true')); ?>
<?php echo $html->tagErrorMsg('Usuario/empleado_id', 'El Empleado es requerido.')?>				

		</td>
	</tr>
	<tr>
		<td><label>Perfil:</label></td>
		<td>
				<?  	
 $i=1;
 if(count($data_perfiles)==0)
 $var2[0]='No tiene';
 foreach ($data_perfiles as $row) {
	$item = $row['0'];	
	$var2[$item['id']]=$item['descripcion'];
	$i=$i+1;
	}
?>
<?php echo $html->selectTag('Usuario/perfil_id',$var2,$selected = null, $selectAttr = array('class'=>'selectgrande',"onchange"=>"modificar('hijos','../buscarhijos/',this.form.UsuarioPerfilId.value);",'disabled'=>'true')); ?>
<?php echo $html->tagErrorMsg('Usuario/perfil_id', 'El Perfil es requerido.')?>				

		</td>
	</tr>			
	<tr>
		<td><label>Supervisor:</label></td>
		<td> <div id="hijos"><?  	
		 $i=1;
if(count($data_perfiles2)==0)
 $var3[0]='No tiene';
		 
 foreach ($data_perfiles2 as $row) {
	$item = $row['0'];	
	$var3[$item['id']]=$item['usuario'].'->'.$item['nombre'];
	$i=$i+1;
	}
?>
<?php echo $html->selectTag('Usuario/supervisor_id',$var3,$selected = null, $selectAttr = array('class'=>'selectgrande','disabled'=>'true')); ?>
 <?php echo $html->tagErrorMsg('Usuario/supervisor_id', 'El Grupo es requerida.')?>	
		</div></td>
	</tr>	
	<tr>
		<td><label>Usuario:</label></td>
		<td> <?php echo $html->input('Usuario/usuario', array('size'=>'40','class'=>"field text medium",'readonly'=>'true')) ?>
        <?php echo $html->tagErrorMsg('Usuario/usuario', 'El usuario es requerido.')?>		
		</td>
	</tr>
	<tr>
		<td><label>Contrase&ntilde;a:</label></td>
		<td> <?php echo $html->password('Usuario/clave', array('size'=>'40','class'=>"field text medium")) ?>
        <?php echo $html->tagErrorMsg('Usuario/clave', 'La clave es requerida.')?>				
		</td>
	</tr>
	<tr>
		<td><label>Repita Contrase&ntilde;a:</label></td>
		<td> <?php echo $html->password('Usuario/clave1', array('size'=>'40','class'=>"field text medium")) ?></td>
	</tr>		
</table>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend><strong>Acciones</strong></legend>
<div align="center">
<?php	$h='<img src="'.IMAGE_ROOT.'img_acciones/go-jump.png"  alt="Regresar al listado" title="Regresar al listado">';		
	echo $html->link($h, '/usuarios', null,null,false);
?>	
<?php	$h='<img src="'.IMAGE_ROOT.'img_acciones/media-floppy.png"  alt="Guardar Registro" onclick="document.frm.submit()" title="Imprimir Guardar Registro">';		
	echo $html->image('img_acciones/media-floppy.png',array("onclick"=>"return Validar();", "title"=>"Imprimir Guardar Registro"));
?>	
<?php	$h='<img src="'.IMAGE_ROOT.'img_acciones/users_back.png" alt="Salir del Sistema" title="Salir del Sistema">';		
	echo $html->link($h, '/usuarios/logout', null,null,false);
?>
</div>
</fieldset>

</fieldset>
</form>
