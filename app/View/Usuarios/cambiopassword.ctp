<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
	echo $this->Html->script('validarvacios');
	echo $this->Html->script('seguridad/sha1');
?>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>
<script language="javascript" type="text/javascript">
function Validar(){
	 var objetoform = document.frm;
	 alert('clave4:'+objetoform.UsuarioClave4.value+' Clave:'+objetoform.UsuarioClave.value);
	if (validarnoiguales(objetoform.UsuarioClave4,objetoform.UsuarioClave.value,"Las Contraseñas actual no es correcta")=='F'){
		
	return false;}else
	if (validarnoiguales(objetoform.UsuarioClavenueva,objetoform.UsuarioClavenueva2.value,"Las Contraseñas no son iguales")=='F'){
	return false;}else
	{			
		objetoform.UsuarioClave1.value = "";
		objetoform.UsuarioClave2.value = "";
		objetoform.UsuarioClave4.value = "";
		document.frm.submit();
	}  
}
</script>
<div class="span11">
        <div class="well"> 
<?php echo $this->Form->create('Usuario', array('url' => 'cambiopassword','name' => 'frm','id'=>'frm','role'=>"form",'class'=>"form-horizontal")); ?>	
<fieldset>
<legend>CAMBIO DE CONTRASE&Ntilde;A</legend>
<?php	 echo $this->Form->input('Usuario.id', array('type'=>'hidden'));
		echo '<div class="form-group">';		
		echo $this->Html->tag('label', 'C&eacute;dula:', array('class' => 'col-lg-2 control-label','for'=>"cedula"));
		echo '<div class="col-lg-2">';
		echo $this->Form->input('Empleado.cedula',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control"));		
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Empleado:', array('class' => 'col-lg-2 control-label','for'=>"empleado"));
		echo '<div class="col-lg-2">';
		echo $this->Form->input('Usuario.empleado_id', array('type'=>'hidden'));
		echo $this->Form->input('Empleado.nombre',array('label'=>':','size' =>'40','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Nombre del Empleado",'class'=>"form-control"));
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Perfil:', array('class' => 'col-lg-2 control-label','for'=>"empleado"));
		echo '<div class="col-lg-2">';
		echo $this->Form->input('Usuario.perfile_id', array('type'=>'hidden'));
		echo $this->Form->input('Perfile.descripcion',array('label'=>'Perfil:','size' =>'30','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Nombre del Empleado",'class'=>"form-control"));
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Usuario:', array('class' => 'col-lg-2 control-label','for'=>"usuario"));
		echo '<div class="col-lg-2">';
		echo $this->Form->input('Usuario.usuario',array('label'=>'Usuario:','size' =>'30','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Nombre del Empleado",'class'=>"form-control"));
		echo '</div>';
		echo '</div>';		
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Escriba Contrase&ntilde;a Actual:', array('class' => 'col-lg-2 control-label','for'=>"usuario"));
		echo '<div class="col-lg-2">';
		echo $this->Form->input('Usuario.claveactual', array('label'=>':','size' =>20,'type'=>'password','onKeyUp'=>'UsuarioClave4.value = hex_sha1(UsuarioClaveactual.value)','div'=>false,'label'=>false,'placeholder'=>"Escriba Contraseña Actual",'class'=>"form-control"));
		echo $this->Form->input('Usuario.clave4', array('type'=>'hidden'));
		echo $this->Form->input('Usuario.clave', array('type'=>'hidden'));
		echo '</div>';
		echo '</div>';		
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Contrase&ntilde;a Nueva:', array('class' => 'col-lg-2 control-label','for'=>"usuario"));
		echo '<div class="col-lg-2">';
		echo $this->Form->input('Usuario.clave2', array('label'=>':','size' =>20,'type'=>'password','onKeyUp'=>'UsuarioClavenueva.value = hex_sha1(UsuarioClave2.value)','div'=>false,'label'=>false,'placeholder'=>"Escriba Clave Nueva",'class'=>"form-control"));
		echo $this->Form->input('Usuario.clavenueva', array('type'=>'hidden'));
		echo '</div>';
		echo '</div>';
		
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Repita Contrase&ntilde;a Nueva:', array('class' => 'col-lg-2 control-label','for'=>"usuario"));
		echo '<div class="col-lg-2">';
		echo $this->Form->input('Usuario.clave1', array('label'=>':','size' =>20,'type'=>'password','onKeyUp'=>'UsuarioClavenueva2.value = hex_sha1(UsuarioClave1.value)','div'=>false,'label'=>false,'placeholder'=>"Repita Clave Nueva",'class'=>"form-control"));
		echo $this->Form->input('Usuario.clavenueva2', array('type'=>'hidden'));
		echo '</div>';
		echo '</div>';
				
?>
       </div>
    </div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"Validar()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/usuarios/home", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>

</fieldset>
</form>
