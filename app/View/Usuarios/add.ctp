<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<?php
	echo $this->Html->script('prototype');
	echo $this->Html->script('validarvacios');
	echo $this->Html->script('seguridad/sha1');
?>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>	

<script language="javascript" type="text/javascript">

function Validar(){
		 var objetoform = document.frm;
	 	if (validarnoiguales(objetoform.UsuarioClave2,objetoform.UsuarioClave1.value,"Las Contraseñas no son iguales")=='F'){
		return false;}else{
			objetoform.UsuarioClave1.value = "";
			objetoform.UsuarioClave2.value = "";
			document.frm.submit();
		}  
}
</script>		
<?php echo $this->Form->create('Usuario', array('name' => 'frm','id'=>'frm','class'=>'wufoo','url'=>'add','onsubmit'=>'return Validar();')); ?>	
<fieldset id="personal" >
<legend><h2>REGISTRAR UN USUARIO</h2></legend>
<table>
	<tr>
		<td><label>Empleado:</label></td>
		<td>
<?php  	
 $i=1;$var=array();
 foreach ($data_empleado as $row) {
	$item = $row['Empleado'];	
	$var[$item['id']]=$item['cedula'].'=>'.$item['nombre'];
	$i=$i+1;
	}
?>
<?php echo $this->Form->input('Usuario.empleado_id', array('size'=>'40','label'=>'','options'=>$var,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','escape' => false)) ?>
		</td>
	</tr>
	<tr>
		<td><label>Perfil:</label></td>
		<td>
<?php  	
 $var2=array();
 $i=1;
 foreach ($data_perfiles as $row) {
	$item = $row['Perfile'];	
	$var2[$item['id']]=$item['descripcion'];
	$i=$i+1;
	}
?>
<?php echo $this->Form->input('Usuario.perfil_id', array('size'=>'40','label'=>'','options'=>$var2,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1')) ?>
		</td>
	</tr>			

	<tr>
		<td><label>Usuario:</label></td>
		<td><?php echo $this->Form->input('Usuario.usuario', array('size' => 40,'type' =>'text','label' =>''));?>		
		</td>
	</tr>
	<tr>
		<td><label>Contrase&ntilde;a:</label></td>
		<td> 
			<?php echo $this->Form->password('Usuario.clave2', array('size' =>20,'type'=>'password','onKeyUp'=>'UsuarioClave.value = hex_sha1(UsuarioClave2.value)')); ?>
			<input type="hidden" name="data[Usuario][clave]" id="UsuarioClave">
		</td>
	</tr>
	<tr>
		<td><label>Repita Contrase&ntilde;a:</label></td>
		<td> 	<?php echo $this->Form->password('Usuario.clave1', array('size' =>20,'type'=>'password')); ?>
			<?php //echo $html->password('Usuario/clave1', array('size'=>'40','class'=>"field text medium")) ?></td>
	</tr>		
</table>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Imprimir Guardar Registro"));
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/usuarios", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout",array('escape'=>false), null);
?>
</div>
</fieldset>

</fieldset>
</form>
