<?
echo $this->renderElement('menu',$datos_menu);
?>
<br />

<fieldset id="personal" >
<legend><label>LISTA DE USUARIOS REGISTRADOS</label></legend>
<?php
$pagination->setPaging($paging);
?>

<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center">
<thead>
    <tr>
        <th ><div class="blanco"><? echo $pagination->sortBy('id','Codigo')?></div></th>
        <th><div class="blanco"><? echo $pagination->sortBy('usuario','Usuario')?></div></th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $usuario = $row['Usuario']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $usuario['id'] ?>    
        </td>
        <td>
            <?php echo $html->link($usuario['usuario'], '/usuarios/view/'.$usuario['id']) ?>    
        </td>      
        <td>
            <?php echo $usuario['created'] ?>
        </td>
        <td>
            <?php 
                if (!empty($usuario['modified'])) echo $usuario['modified'];
                else if (!empty($usuario['updated'])) echo $usuario['updated'];
            ?>
        </td>
        <td align="center">
	  <?php 
		$v='<img src="'.IMAGE_ROOT.'img_acciones/book_open2.png"  alt="Ver Registro" title="Ver Registro">';
	    	echo $html->link($v, '/usuarios/view/'.$usuario['id'], null,null,false); ?>
	        &nbsp;|&nbsp;
      	  <?php	$e='<img src="'.IMAGE_ROOT.'img_acciones/pda_write.png"  alt="Editar Registro" title="Editar Registro">';
	    	echo $html->link($e, '/usuarios/edit/'.$usuario['id'], null,null,false); ?>

            &nbsp;|&nbsp;
          <?php	$d='<img src="'.IMAGE_ROOT.'img_acciones/book_blue_delete.png" alt="Eliminar Registro" title="Eliminar Registro">';		
		echo $html->link($d, '/usuarios/delete/'.$usuario['id'], null,'¿Esta seguro de eliminar "'.$usuario['usuario'].'"?',false); ?>    
        <!-- &nbsp;|&nbsp; -->
          <?php	$d='<img src="'.IMAGE_ROOT.'img_acciones/edit-paste.png" alt="Ver Proyectos Asignados" title="Ver Proyectos Asignados">';		
	//	echo $html->link($d, '/usuarios/verproyectos/'.$usuario['id'], null,null,false); ?>    

        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
<? echo $this->renderElement('pagination'); ?>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend class="accion"><strong>Acciones</strong></legend>
<div align="center">
<?php	$h='<img src="'.IMAGE_ROOT.'img_acciones/book_blue_new.png"  alt="Insertar un nuevo Registro" title="Insertar un nuevo Registro">';		
	echo $html->link($h, '/usuarios/add', null,null,false);
?>	
<?php	$h='<img src="'.IMAGE_ROOT.'img_acciones/printer_002.png"  alt="Imprimir Listado" title="Imprimir Listado">';		
	echo $html->link($h, '/usuarios/reporte', null,null,false);
?>	
<?php	$h='<img src="'.IMAGE_ROOT.'img_acciones/users_back.png" alt="Salir del Sistema" title="Salir del Sistema">';		
	echo $html->link($h, '/usuarios/logout', null,null,false);
?>	
</div>
</fieldset>

</fieldset>

