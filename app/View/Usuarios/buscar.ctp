<?php
?>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="pure-table pure-table-horizontal">
<thead>
    <tr>
        <th ><div class="blanco">Codigo</div></th>
        <th><div class="blanco"><? echo $pagination->sortBy('usuario','Usuario')?></div></th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $usuario = $row['Usuario']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $usuario['id'] ?>    
        </td>
        <td>
            <?php echo $html->link($usuario['usuario'], '/usuarios/view/'.$usuario['id']) ?>    
        </td>      
        <td>
            <?php echo $usuario['created'] ?>
        </td>
        <td>
            <?php 
                if (!empty($usuario['modified'])) echo $usuario['modified'];
                else if (!empty($usuario['updated'])) echo $usuario['updated'];
            ?>
        </td>
        <td align="center">
	  <?php 
		echo $html->link( $html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/usuarios/view/".$usuario['id'], array('escape'=>false), null); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $html->link( $html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/usuarios/edit/".$usuario['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $html->link( $html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/usuarios/delete/".$usuario['id'], array('escape'=>false), '¿Esta seguro de eliminar "'.$usuario['usuario'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
