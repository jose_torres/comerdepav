<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<fieldset id="personal" >
<legend class="info"><h2>MOSTRAR UN USUARIO</h2></legend>
<table>
	<tr>
		<td><label>Usuario:</label></td>
		<td><?php echo $data['Usuario']['usuario']; ?> </td>
	</tr>
	<tr>
		<td><label>Empleado:</label></td>
		<td><?php echo $data['Empleado']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Perfil:</label></td>
		<td><?php echo $data['Perfile']['descripcion']; ?> </td>
	</tr>
	<tr>
		<td><label>Creado:</label></td>
		<td><?php echo $data['Empleado']['created']; ?> </td>
	</tr>
</table>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<div align="center">
<?php	
	//echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/usuarios/edit/".$data['Usuario']['id'], array('escape'=>false), null);
	//echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Eliminar Registro", "title"=>"Eliminar Registro")) ,"/usuarios/delete/".$data['Usuario']['id'], array('escape'=>false), '¿Esta seguro de eliminar "'.$data['Usuario']['usuario'].'"?');
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/usuarios", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</div>
</fieldset>
</fieldset>
