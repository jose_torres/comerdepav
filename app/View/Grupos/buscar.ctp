<?php  if (count($data)>0){?>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="pure-table pure-table-horizontal">
<thead>
    <tr>
        <th >Codigo</th>
        <th>Nombre</th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>
    <?php $x=0; foreach ($data as $row): $grupo = $row['Grupo']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $grupo['id'] ?>    
        </td>
        <td>
            <?php echo $this->Html->link($grupo['nombre'], '/grupos/view/'.$grupo['id']) ?>    
        </td>      
        <td>
            <?php echo $grupo['created'] ?>
        </td>
        <td>
            <?php 
                if (!empty($grupo['modified'])) echo $grupo['modified'];
                else if (!empty($grupo['updated'])) echo $grupo['updated'];
            ?>
        </td>
        <td align="center">
	   <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/grupos/view/".$grupo['id'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/grupos/edit/".$grupo['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/grupos/delete/".$grupo['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$grupo['nombre'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php }else{
		echo 'Ningun registros encontrados';
	}

?>
