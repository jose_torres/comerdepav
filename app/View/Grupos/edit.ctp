<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>		

<div class="unidades form">
<?php echo $this->Form->create('Grupo', array('url' => 'edit','name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned')); ?>		
<fieldset id="personal" >
<legend><h2>REGISTRAR UN GRUPO</h2></legend>
<?php
	echo  $this->Form->input('Grupo.id', array('type'=>'hidden'));
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Grupo.nombre',array('label'=>'Nombre:','size' =>'30','type'=>'text','div'=>false));
	echo '</div>';
	if ($handle = opendir('../../app/webroot/img/img_acciones')) {
	$i=0;$primero='';
	while (false !== ($file = readdir($handle)))
	{
		if ( preg_match( "/[.]png$/", $file ) ) {
			preg_match( "/_(\d+)_(\d+)[.]/", $file, $found );
			if ($i==0){ $primero=$file; }
			$ban[$file]=$file;
			$i=$i+1;
		}
	}
	closedir($handle);
	}
	$IMAGE_ROOT=FULL_BASE_URL.'/'.$nombreProyecto.'/img/';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Grupo.imagen', array('size'=>'40','label'=>'Imagen:','options'=>$ban,'div'=>false,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','escape'=>false,'onchange'=>"document.getElementById('im').src ='".$IMAGE_ROOT."img_acciones/'+ document.frm.GrupoImagen.value")).'->' ;
	echo $this->Html->image('img_acciones/'.$this->data['Grupo']['imagen'],array('align'=>"absmiddle",'id'=>"im"));
	echo '</div>';
?>
</fieldset>
</form>
</div>
<form>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Imprimir Guardar Registro"));
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/grupos", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
</form>
