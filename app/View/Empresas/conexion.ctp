<?php
//Funciones js propias del archivo.ctp
echo $this->Html->script(array('funciones.js','View/Productos/activar'));
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('select2/select2.min.css');
    
?>

<fieldset id="personal" >
<div class="box">
	<br>	
<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'guardarmarcados','id'=>'frm','name'=>'frm'));
?>	
<?php
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Ip P&uacute;blica:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.ip',array('div'=>false,'type'=>'text','label'=>false,'placeholder'=>"Escriba Ej. 181.208.191.212",'class'=>"form-control "));
echo '</div>';
echo '</div>';
    
?>
 <button type="button" class="btn btn-info" onclick="actualizar('buscar','realizarajuste/',''+document.frm.ReporteSucursal.value,'sucursal='+document.frm.ReporteIp.value)">Ajustar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
<div id="buscar" class="form-group">
    <div class="col-xs-4">
        &nbsp;
    </div>
</div>

<?php
    echo '</div>';
echo '</div>';
?>

 
<?php
    echo $this->Form->end();
?>
</div>


<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
    echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/daterangepicker/daterangepicker.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js')
?>
<script>
	//Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
	
	var options =  { 
            onKeyPress: function(cep, event, currentField, options){
//            console.log('An key was pressed!:', cep, ' event: ', event,'currentField: ', currentField, ' options: ', options);
                if(cep){
                  var ipArray = cep.split(".");
                  var lastValue = ipArray[ipArray.length-1];
                  if(lastValue !== "" && parseInt(lastValue) > 255){
                      ipArray[ipArray.length-1] =  '255';
                      var resultingValue = ipArray.join(".");
                      currentField.attr('value',resultingValue);
                  }
            }             
        }};

	/*$("#ReporteIp").mask("000.000.000.000", options);*/
	
		
	/*$("#ReporteIp").inputmask('900.900.900.900', {'translation': {
                                        '0': { pattern: /[0-9]/, optional: true },
                                        9: {pattern: /[0-9]/}
                                      }
                                });	*/
      
  
                                      
  
            
//Date range picker
    $('#reservation').daterangepicker();
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'DD-MM-YYYY'});
    //Date range as a button
    $('input[id="ReporteRango"]').daterangepicker(
        {format: 'DD-MM-YYYY',
        separator: " al ",
        startDate: moment().subtract(29, 'days'),
          endDate: moment(),
          ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          },
    "locale": {
        "applyLabel": "Aceptar",
        "cancelLabel": "Cancelar",
        "fromLabel": "Desde",
        "toLabel": "Hasta",
        "customRangeLabel": "Por Rango de Fecha",
        "weekLabel": "W",
        "daysOfWeek": [
            "Do",
            "Lu",
            "Ma",
            "Mi",
            "Ju",
            "Vi",
            "Sa"
        ],
        "monthNames": [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ],
        "firstDay": 1
    },
          
        },
        function (start, end,label) {
          $('input[id="ReporteRango"]').html(start.format('DD-MM-YYYY') + ' al ' + end.format('DD-MM-YYYY'));
        }
    );
</script>	
</fieldset>
