<?php
//echo $this->element('menuinterno',$datos_menu);
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
?>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>		

<div class="unidades form">
<?php echo $this->Form->create('Empresa', array('url' => 'add','name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned','enctype'=>"multipart/form-data")); ?>
<fieldset id="personal" >
<legend><h2>REGISTRAR UNA EMPRESA</h2></legend>
<?php
	echo '<div class="form-group">';		
	echo $this->Html->tag('label', 'Rif:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Empresa.rif',array('label'=>false,'size' =>'30','type'=>'text','maxlength'=>12,'div'=>false,'class'=>"form-control",'data-inputmask'=>'"mask": "J-99999999-9"'));
	echo '</div>';
	echo $this->Html->tag('label', 'Nombre:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-6">';
	echo $this->Form->input('Empresa.nombre',array('label'=>false,'size' =>'30','type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo '</div>';
	
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Descripci&oacute;n:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-10">';
	echo $this->Form->input('Empresa.descripcion',array('label'=>false,'type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo '</div>';
	
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Direcci&oacute;n:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-10">';
	echo $this->Form->input('Empresa.direccion',array('label'=>false,'type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo '</div>';
	
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Telef&oacute;nos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-10">';
	echo $this->Form->input('Empresa.telefono',array('label'=>false,'type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo '</div>';
	
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Logo Izquierdo:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-4">';
	echo $this->Form->input('Empresa.logoizq',array('label'=>false,'size' =>'40','type'=>'file','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo $this->Html->tag('label', 'Nombre Archivo:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-4">';
	echo $this->Form->input('Empresa.logo_izquierdo',array('label'=>false,'size' =>'40','type'=>'text','readonly'=>true,'div'=>false,'class'=>"form-control"));
	echo '</div>';	
	echo '</div>';
	
	
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Logo Derecho:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-4">';
	echo $this->Form->input('Empresa.logoder',array('label'=>false,'type'=>'file','div'=>false,'class'=>"form-control"));
	echo '</div>';
	echo $this->Html->tag('label', 'Nombre Archivo:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-4">';
	echo $this->Form->input('Empresa.logo_derecho',array('label'=>false,'type'=>'text','readonly'=>true,'div'=>false,'class'=>"form-control"));
	echo '</div>';	
	echo '</div>';
	
/*	$sucursal['comerdepa']='Principal';
	$sucursal['superdepa']='Superdepa';
	$sucursal['comerdepa_1']='Sucursal 1';
	$sucursal['comerdepa_2']='Sucursal 2';
	$sucursal['comerdepa_3']='Sucursal 3'; */
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-10">';
echo $this->Form->input('Empresa.bd',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

	$tipoventa['DETAL']='DETAL';
	$tipoventa['MAYOR']='MAYOR';
	$tipoventa['MAYORDETAL']='MAYOR Y DETAL';
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Tipo de Ventas:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-10">';
echo $this->Form->input('Empresa.tipoventa',array('div'=>false,'options'=>$tipoventa,'label'=>false,'data-placeholder'=>"Seleccione Ventas Permitidas en la Sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';
?>
</fieldset>
</form>
</div>
<form>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Imprimir Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/empresas", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
</form>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Money Euro
    //$.mask.definitions['A'] = "[VJGE]";
     $.extend($.inputmask.defaults.definitions, {
        'A': { 
            validator: "[GJVE]",
            cardinality: 1,
            casing: "upper" //auto uppercasing
        }
    });
     $("#EmpresaRif").inputmask('A-99999999-9', {'translation': {
                                        'A': {pattern: /[GJVE]/},
                                        9: {pattern: /[0-9]/}
                                      }
                                });                            
   

  });
</script>
