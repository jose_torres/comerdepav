<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<div class="unidades view">
<fieldset id="personal" >
<legend class="info"><h2>MOSTRAR UNA EMPRESA</h2></legend>
<table>
	<tr>
		<td><label>Rif:</label></td>
		<td><?php echo $data['Empresa']['rif']; ?> </td>
	</tr>
	<tr>
		<td><label>Nombre:</label></td>
		<td><?php echo $data['Empresa']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Telefono:</label></td>
		<td><?php echo $data['Empresa']['telefono']; ?> </td>
	</tr>
	<tr>
		<td><label>Logo Izquierdo:</label></td>
		<td><?php 
		if ($data['Empresa']['logo_izquierdo']!=''){
			echo $this->Html->image('logo/'.$data['Empresa']['logo_izquierdo'],array('align'=>"absmiddle",'id'=>"im"));
		}	
			?> </td>
	</tr>
	<tr>
		<td><label>Logo Derecho:</label></td>
		<td><?php 
		if ($data['Empresa']['logo_derecho']!=''){
			echo $this->Html->image('logo/'.$data['Empresa']['logo_derecho'],array('align'=>"absmiddle",'id'=>"im"));
		}
			?> </td>
	</tr>
	<tr>
		<td><label>Creado:</label></td>
		<td><?php echo $data['Empresa']['created']; ?> </td>
	</tr>
</table>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/empresas", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>

</fieldset>
