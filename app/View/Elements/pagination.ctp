<div id='pagination' align="center">
<?php
    if($pagination->setPaging($paging)):
    $leftArrow = $html->image("img_acciones/goprevious.png", Array('alt'=>'Ver P&aacute;gina Anterior','title'=>'Ver P&aacute;gina Anterior'));
    $rightArrow = $html->image("img_acciones/gonext.png", Array('alt'=>'Ver Proxima P&aacute;gina','title'=>'Ver Proxima P&aacute;gina'));
    
    $prev = $pagination->prevPage($leftArrow,false);
    $prev = $prev?$prev:$leftArrow;
    $next = $pagination->nextPage($rightArrow,false);
    $next = $next?$next:$rightArrow;

    $pages = $pagination->pageNumbers(" | ");

    echo $pagination->result()."<br>";
    echo $prev." P&aacute;gina: ".$pages." ".$next."<br>";
    echo $pagination->resultsPerPage(' Registro por P&aacute;ginas: ', ' ');
    endif;
?>
</div>
