<?php 
?>
<ul class="glossymenu">
	<li <?php if($id=='1'){echo 'class="current"';  }?> >
         <?php	echo $html->link( '<b>Ingreso</b>',"/efectivos/add".$opcion, array('escape'=>false),null);?>
</li>
	<li <?php if($id=='2'){echo 'class="current"';  }?> >
         <?php	echo $html->link( '<b>Egreso</b>',"/compragastos/add".$opcion, array('escape'=>false),null);?>
</li>
	<li <?php if($id=='3'){echo 'class="current"';  }?> ><?php	echo $html->link( '<b>Reporte Z</b>',"/zreportes/add".$opcion, array('escape'=>false),null);?>
</li>
	<li <?php if($id=='4'){echo 'class="current"';  }?> ><?php	echo $html->link( '<b>Cuentas x Cobrar</b>',"/cccobranzas/add".$opcion, array('escape'=>false),null);?>
</li>
</ul>

<div id='menu_borde'>
<?php if($id=='1'){ 

?>
	<div id="header_tab">
	<ul id="primary">	
		<li><?php echo $html->link( 'Efectivo',"/efectivos/add".$opcion, array('escape'=>false,'class'=>$id_sub==1 ?'current2':''),null);?>
			<?php if($id_sub=='1'){ ?>
			<ul id="secondary">
				<li><?php echo $html->link( 'Incluir',"/efectivos/add".$opcion, array('escape'=>false,'class'=>$id_sub_op==1 ?'current2':''),null);?></li>
			<li><?php echo $html->link( 'Resumen',"/efectivos/resumen".$opcion, array('escape'=>false,'class'=>$id_sub_op==2 ?'current2':''),null);?></li>
				<li><?php echo $html->link( 'Cerrar Lote',"/efectivos/cerrarlote".$opcion, array('escape'=>false,'class'=>$id_sub_op==3 ?'current2':''),null);?></li>
			</ul> 
			<? }?>
</li>
		<li><?php echo $html->link( 'Cheque',"/cheques/add".$opcion, array('escape'=>false,'class'=>$id_sub==2 ?'current2':''),null);?></li>
		<li><?php echo $html->link( 'Tarjeta',"/tarjetas/add".$opcion, array('escape'=>false,'class'=>$id_sub==3 ?'current2':''),null);?></li>
		<li><?php echo $html->link( 'Cupones',"/cupones/add".$opcion, array('escape'=>false,'class'=>$id_sub==4 ?'current2':''),null);?></li>
		<li><?php echo $html->link( 'Fondos',"/fondos/add".$opcion, array('escape'=>false,'class'=>$id_sub==5 ?'current2':''),null);?></li>
	</ul>
	</div>
	<? }else if($id=='2'){ ?>
	<div id="header_tab">
	<ul id="primary">
		<li><?php	echo $html->link( '<b>Compra y Gastos</b>',"/compragastos/add".$opcion, array('escape'=>false,'class'=>$id_sub==1 ?'current2':''),null);?></li>
		<li><?php	echo $html->link( '<b>Vales</b>',"/prestamos/add".$opcion, array('escape'=>false,'class'=>$id_sub==2 ?'current2':''),null);?>
		</li>
		<li><?php	echo $html->link( '<b>Cheques por Banco</b>',"/cheques/gastos".$opcion, array('escape'=>false,'class'=>$id_sub==3 ?'current2':''),null);?>
		</li>
	</ul>
	</div>

	<?php }else if($id=='4'){ 
?>
	<div id="header_tab">
	<ul id="primary">
		<li><?php echo $html->link( 'Ventas x Creditos',"/cccobranzas/add".$opcion, array('escape'=>false,'class'=>$id_sub==5 ?'current2':''),null);
		?>
			<?php if($id_sub=='5'){ ?>
			<ul id="secondary">
				<li><?php echo $html->link( 'Peque&ntilde;a',"/cccobranzas/add".$opcion, array('escape'=>false,'class'=>$id_sub_op==1 ?'current2':''),null);?></li>
			<li><?php echo $html->link( 'Grande',"/cccobranzas/addlegal".$opcion, array('escape'=>false,'class'=>$id_sub_op==2 ?'current2':''),null);?></li>
			</ul> 
			<? }?>
		</li>
		<li><?php echo $html->link( 'Efectivo',"/ccefectivos/add".$opcion, array('escape'=>false,'class'=>$id_sub==1 ?'current2':''),null);?>
			<?php if($id_sub=='1'){ ?>
			<ul id="secondary">
				<li><?php echo $html->link( 'Peque&ntilde;a',"/ccefectivos/add".$opcion, array('escape'=>false,'class'=>$id_sub_op==1 ?'current2':''),null);?></li>
			<li><?php echo $html->link( 'Grande',"/ccefectivos/addlegal".$opcion, array('escape'=>false,'class'=>$id_sub_op==2 ?'current2':''),null);?></li>
			<li><?php echo $html->link( 'Resumen',"/ccefectivos/resumen".$opcion, array('escape'=>false,'class'=>$id_sub_op==3 ?'current2':''),null);?></li>
				<li><?php echo $html->link( 'Cerrar Lote',"/ccefectivos/cerrarlote".$opcion, array('escape'=>false,'class'=>$id_sub_op==4 ?'current2':''),null);?></li>
			</ul> 
			<? }?>
		</li>
		<li><?php echo $html->link( 'Cheque',"/cccheques/add".$opcion, array('escape'=>false,'class'=>$id_sub==2 ?'current2':''),null);?></li>
		<?php if($id_sub=='2'){ ?>
			<ul id="secondary">
				<li><?php echo $html->link( 'Peque&ntilde;a',"/cccheques/add".$opcion, array('escape'=>false,'class'=>$id_sub_op==1 ?'current2':''),null);?></li>
			<li><?php echo $html->link( 'Grande',"/cccheques/addlegal".$opcion, array('escape'=>false,'class'=>$id_sub_op==2 ?'current2':''),null);?></li>
			</ul> 
			<? }?>
		<li><?php echo $html->link( 'Tarjeta',"/cctarjetas/add".$opcion, array('escape'=>false,'class'=>$id_sub==3 ?'current2':''),null);?></li>
		<?php if($id_sub=='3'){ ?>
			<ul id="secondary">
				<li><?php echo $html->link( 'Peque&ntilde;a',"/cctarjetas/add".$opcion, array('escape'=>false,'class'=>$id_sub_op==1 ?'current2':''),null);?></li>
			<li><?php echo $html->link( 'Grande',"/cctarjetas/addlegal".$opcion, array('escape'=>false,'class'=>$id_sub_op==2 ?'current2':''),null);?></li>
			</ul> 
			<? }?>
		<li><?php echo $html->link( 'Transferencia',"/cctrans/add".$opcion, array('escape'=>false,'class'=>$id_sub==4 ?'current2':''),null);?></li>
		<?php if($id_sub=='4'){ ?>
			<ul id="secondary">
				<li><?php echo $html->link( 'Peque&ntilde;a',"/cctrans/add".$opcion, array('escape'=>false,'class'=>$id_sub_op==1 ?'current2':''),null);?></li>
			<li><?php echo $html->link('Grande',"/cctrans/addlegal".$opcion, array('escape'=>false,'class'=>$id_sub_op==2 ?'current2':''),null);?></li>
			</ul> 
			<? }?>
	</ul>
	</div>
<?php } ?>	
<br />
