<?php
   function recursivo($unidade,$espacio='',$nro,$imprimir,$img){
	$espacio=$espacio.'&nbsp;=>';
	$class = null;			
	$nro=$nro+1;   
	if ($nro % 2 == 0) {
			$class = ' class="pure-table-odd"';
		}		
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $unidade['Oficina']['id']; ?>&nbsp;-<? echo $nro;?></td>
		<td><?php echo $espacio.$unidade['Oficina']['descripcion']; ?>&nbsp;</td>
		<td><?php echo $unidade['Oficina']['modified']; ?>&nbsp;</td>
		<td class="actions" align="center">
			<?php echo $imprimir->link($img->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")), array('action' => 'view', $unidade['Oficina']['id']), array('escape'=>false)); ?>&nbsp;&nbsp;
			<?php echo $imprimir->link($img->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")), array('action' => 'edit', $unidade['Oficina']['id']), array('escape'=>false)); ?>&nbsp;&nbsp;
			<?php echo $imprimir->link($img->image("img_acciones/book_blue_delete.png", array("alt" => "Eliminar Registro", "title"=>"Eliminar Registro")), array('action' => 'delete', $unidade['Oficina']['id']),  array('escape'=>false), sprintf(__('Esta seguro de eliminar el registro # %s?', true), $unidade['Oficina']['id'])); ?>
		</td>
	</tr>
<?php
		foreach ($unidade['children'] as $r_hijos){			
			recursivo($r_hijos,$espacio,$nro,$imprimir,$img) ;
			}
}
?>	
