<?php
$lclass[1]='';$aclass[1]='btn btn-default';
$lclass[2]='';$aclass[2]='btn btn-default';
$lclass[3]='';$aclass[3]='btn btn-default';

$lclass[$id]='class="active"';$aclass[$id]='btn btn-info';
if($id==1){
	$mensaje='Se listaran las Retenciones transferidas de las sucursales, solo para cambiar el n&uacute;mero de la misma.';
}elseif($id==2){
	$mensaje='Se listaran las Retenciones conciliadas en el Cuadre, solo para asignarles el mes y a&ntilde;o que afecta para cerrarlas.';
}elseif($id==3){
	$mensaje='Se listaran las Retenciones Cerradas en Retenciones Conciliadas.';
}
?>
<ul class="nav nav-pills nav-justified">
  <li <?php echo $lclass[1]; ?> ><?php	echo $this->Html->link( '1.- Sin Conciliar&nbsp;<span class="glyphicon glyphicon-chevron-right"></span>',"/conretencionventadets/index", array('escape'=>false, 'class'=>$aclass[1]),null);?></li>
  <li <?php echo $lclass[2]; ?>><?php	echo $this->Html->link( '2.- Conciliados&nbsp;<span class="glyphicon glyphicon-chevron-right"></span>',"/conretencionventadets/conciliados", array('escape'=>false, 'class'=>$aclass[2]),null);?></li>
  <li <?php echo $lclass[3]; ?>><?php	echo $this->Html->link( '3.- Cerrados',"/conretencionventadets/cerrados", array('escape'=>false, 'class'=>$aclass[3]),null);?></li>
</ul>
<div class="alert alert-info"><?php echo $this->Html->image("img_acciones/x-directory-desktop.png", array("alt" => "Informacion", "title"=>"Informacion")).'&nbsp;'.$mensaje;?></div>
<br />
