<div class="pure-g">
<div class="pure-u-1-1">	
<div class="pure-menu pure-menu-horizontal">
    <ul class="pure-menu-list">
        <li class="pure-menu-item<?php if($id=='1'){echo ' pure-menu-selected';  }?> "><?php	echo $this->Html->link( '<b>Creados sin Enviar ('.$sin_enviar.')</b>',"/documentos", array('escape'=>false, 'class'=>"pure-menu-link"),null);?></li>
        <li class="pure-menu-item<?php if($id=='2'){echo ' pure-menu-selected';  }?> "><?php	echo $this->Html->link( '<b>Pendientes por Revisi&oacute;n ('.$enviado.')</b>',"/documentos/enviados", array('escape'=>false, 'class'=>"pure-menu-link"),null);?></li>
        <li class="pure-menu-item<?php if($id=='3'){echo ' pure-menu-selected';  }?> "><?php	echo $this->Html->link( '<b>Pendiente por Aprobaci&oacute;n ('.$poraprobar.')</b>',"/documentos/poraprobar", array('escape'=>false, 'class'=>"pure-menu-link"),null);?></li>
        <li class="pure-menu-item<?php if($id=='4'){echo ' pure-menu-selected';  }?> "><?php	echo $this->Html->link( '<b>Pendiente por Publicaci&oacute;n ('.$porpublicar.')</b>',"/documentos/porpublicar", array('escape'=>false, 'class'=>"pure-menu-link"),null);?></li>
    </ul>
</div>
</div>
<div class="pure-u-1-1">	
<div class="pure-menu pure-menu-horizontal">
    <ul class="pure-menu-list">
        <li class="pure-menu-item<?php if($id=='5'){echo ' pure-menu-selected';  }?> "><?php	echo $this->Html->link( '<b>Recibidos para Revisi&oacute;n ('.$recibido.')</b>',"/documentos/recibidos", array('escape'=>false, 'class'=>"pure-menu-link"),null);?></li>
        <li class="pure-menu-item<?php if($id=='6'){echo ' pure-menu-selected';  }?> "><?php	echo $this->Html->link( '<b>Recibidos para Aprobaci&oacute;n ('.$recibido_aprob.')</b>',"/documentos/recibidosaprobar", array('escape'=>false, 'class'=>"pure-menu-link"),null);?></li>
        <li class="pure-menu-item<?php if($id=='7'){echo ' pure-menu-selected';  }?> "><?php	echo $this->Html->link( '<b>Anulados ('.$anulado.')</b>',"/documentos/anulados", array('escape'=>false, 'class'=>"pure-menu-link"),null);?></li>
    </ul>
</div>
</div>
</div>
