<script language="javascript" type="text/javascript">
// Busqueda de Ajax	
function modificar(nombrediv,url,id,parametros){
	 // alert ('El registro ha sido Modificado');	
	 $('cargando').update('<? echo $html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');	
		var ajx = new Ajax.Updater(""+nombrediv,""+url+id,{asynchronous:true, evalScripts:true, parameters:"?"+parametros,
		 onSuccess: function(){
         $('cargando').update("");
		}
		 });	
}
// Convierte en el nro en formato Latinoamericano
// Ej. 2380.89 => 2.380,89
function number_one(nombreCampo){
		// Obtener última tecla pulsada 11/Abr/2012 V.L.
		enny=document.getElementById(""+nombreCampo).value;
		cad1=enny.substring(0,enny.length-1);
		cad2=enny.substr(enny.length-1,1);
		if(cad2 == '.' || cad2 == ',' ){
			var num=cad1+',';
		}else{
			if(isNaN(cad2)){
				var num=cad1;
			}else{
				var num=cad1+cad2;
			}
		}
		obj=document.getElementById(""+nombreCampo).value;
		simbolo='';
		punto = "";
		posicion = num.indexOf(',');
		ubicacion = num.indexOf(',');
		cents = "";
		if(posicion != -1){
			punto = ",";
			cents = num.substring(posicion);
			if(cents.length >= 4) cents = cents.substring(0, 3);
			posicion += posicion / 3;
			num = num.split(',');
			num = num.toString();
			num = num.substring(0,ubicacion);
		}		
		num = num.replace(/\$|\./g,'');
		sign = (num == (num = Math.abs(num)));
		num = Math.floor(num*100+0.50000000001);
		num = Math.floor(num/100).toString();				
		for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
		num = num.substring(0,num.length-(4*i+3))+'.'+ num.substring(num.length-(4*i+3));
		if(simbolo) simbolo = '$'
		else simbolo = ''
		document.getElementById(""+nombreCampo).value= (((sign)?'':'-') + simbolo + num + ((cents != "00" && cents.length > 0) ? cents : ''));
}
// Se usa para seleccionar la consulta si es mensual o es por rango de Fecha en los reportes
function mensual_periodo(nombre_mensual,nombre_periodo){
		
		if (document.getElementById(""+nombre_mensual).checked==false){
				document.getElementById(""+nombre_mensual).checked=false;
				document.getElementById(""+nombre_periodo).checked=true;
			}else if (document.getElementById(""+nombre_mensual).checked==true){
				document.getElementById(""+nombre_mensual).checked=true;
				document.getElementById(""+nombre_periodo).checked=false;
			}			
	}
</script>
