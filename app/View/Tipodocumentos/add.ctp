<?php
	//echo $this->element('menuinterno',$datos_menu);	
	echo $this->Html->script('prototype'); 
?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	function validar(){
		if (document.getElementById("TipodocumentoNombre").value=="")
		{
			alert ('Por favor escriba un Tipo de Documento');
			return false;
		}else{
			document.frm.submit();	
		}	
	}		
</script>
<div class="unidades form">
<?php echo $this->Form->create('Tipodocumento',array('name' => 'frm','id'=>'frm','class'=>"pure-form pure-form-aligned"));?>
	<fieldset>
 		<legend class="info"><h2><strong><?php echo ('REGISTRAR UN TIPO DE DOCUMENTO'); ?></strong></h2></legend>
	<?php
		echo '<div class="pure-control-group">';
		echo $this->Form->input('nombre',array('label'=>'Descripci&oacute;n:','size' =>'40','type'=>'text','div'=>false));
		echo '</div>';		
	 ?>
	<br>
	</fieldset>
<?php echo $this->Form->end();?>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">	
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"validar()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/tipodocumentos", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
