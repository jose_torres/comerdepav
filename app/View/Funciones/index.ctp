<?php
//echo $this->element('menuinterno',$datos_menu);
echo $this->Html->script('prototype');
//print_r($datos_menu['hijos']);
?>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<fieldset id="personal" >
<legend><h2>LISTA DE FUNCIONES REGISTRADAS</h2></legend>
<center>
<form action="" method="post" name="two" class="pure-form pure-form-aligned">
<?php
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Tipohectarea.buscar_tipo',array('label'=>'Buscar Por el Nombre:','size' =>'40','type'=>'text','div'=>false));
	echo $this->Html->image('img_acciones/system-search.png',array("onclick"=>"modificar('buscar','funciones/buscar/',document.two.TipohectareaBuscarTipo.value);", "title"=>"Buscar Registro",'div'=>false,"align"=>"Absmiddle"));
	echo '</div>';
?>
</form>
<form action="" method="post" name="otro">
<div id="buscar">
<table border="1" cellpadding="1" cellspacing="0" width="100%" align="center" class="table table-bordered table-hover">
<thead>
    <tr>
        <th ><div class="blanco"><?php echo $this->Paginator->sort('id','Codigo')?></div></th>
        <th><div class="blanco"><?php echo $this->Paginator->sort('nombre','Nombre')?></div></th>
        <th>M&oacute;dulo</th>
        <th>Direccion</th>
        <th>Modified</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
    <?php $x=0; foreach ($data as $row): $funcione = $row['Funcione']; ?>    
    <?php $x++; ?>
	<?php if($x % 2 == 0){ ?>
	<tr class="roweven">
	<?php } else { ?>
	<tr>
	<?php } ?>
        <td>
            <?php echo $funcione['id'] ?>    
        </td>
        <td>
            <?php echo $this->Html->link($funcione['nombre'], '/funciones/view/'.$funcione['id']) ?>    
        </td>
        <td>
            <?php echo $row['Modulo']['nombre'] ?>
        </td>      
        <td>
            <?php echo $funcione['direccion'] ?>
        </td>
        <td>
            <?php 
                if (!empty($funcione['modified'])) echo $funcione['modified'];
                else if (!empty($funcione['updated'])) echo $funcione['updated'];
            ?>
        </td>
        <td align="center">
	  <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/funciones/view/".$funcione['id'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/funciones/edit/".$funcione['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/funciones/delete/".$funcione['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$funcione['nombre'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, comenzando con el registro {:start}, terminando en {:end}')
	));
	?>	</p>
	<div class="paging">
		<?php echo $this->Paginator->prev('' . $this->Html->image("img_acciones/goprevious.png", array("alt" => "Ver Pagina Anterior", "title"=>"Ver Pagina Anterior")), array('escape'=>false), null, array('class'=>'disabled','escape'=>false));?>
	  	<?php
			echo $this->Paginator->counter(array('format' => __('P&aacute;gina: %page%', true)));
	?> 
		<?php echo $this->Paginator->next($this->Html->image("img_acciones/gonext.png", array("alt" => "Ver Proxima Pagina", "title"=>"Ver Proxima Pagina ")) . '', array('escape'=>false), null, array('class' => 'disabled','escape'=>false));?><br>
		<?php echo 'P&aacute;ginas: |'.$this->Paginator->numbers();
		?>
	</div>
</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/funciones/add", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</center>
</form>
</fieldset>
</fieldset>
