<?php
//echo $this->element('menuinterno',$datos_menu);
echo $this->Html->script('jquery.min.js');
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
?>		
<div class="unidades form">
		
<fieldset id="personal" >
<legend  class="info"><h3>REGISTRAR UNA FUNCI&Oacute;N</h3></legend>
<?php echo $this->Form->create('Funcione', array('role'=>"form",'url' => 'add','name' => 'frm','id'=>'frm','class'=>'form-horizontal')); ?>
<?php
	echo '<div class="form-group">';
        echo $this->Html->tag('label', 'Nombre:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
        echo '<div class="col-xs-6">';
	echo $this->Form->input('Funcione.nombre',array('label'=>false,'size' =>'30','type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
        echo '</div>';
	echo '<div class="form-group">';
        echo $this->Html->tag('label', 'Direcci&oacute;n:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
        echo '<div class="col-xs-6">';
	echo $this->Form->input('Funcione.direccion',array('label'=>false,'size' =>'30','type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';
        echo '</div>';
	echo '<div class="form-group">';
        echo $this->Html->tag('label', 'M&oacute;dulo:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
        echo '<div class="col-xs-6">';
	echo $this->Form->input('Funcione.modulo_id',array('label'=>false,'size' =>'30','type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','div'=>false,'class'=>"form-control select2"));
	echo '</div>';
        echo '</div>';
	echo '<div class="form-group">';
         echo $this->Html->tag('label', 'Grupo:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
        echo '<div class="col-xs-6">';
	echo $this->Form->input('Funcione.grupo_id', array('size'=>'40','label'=>false,'options'=>$grupos,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','div'=>false,'class'=>"form-control select2"));
	echo '</div>';
        echo '</div>';
	$var2['SI']='SI';$var2['NO']='NO';
	echo '<div class="form-group">';
         echo $this->Html->tag('label', 'Visible:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
        echo '<div class="col-xs-6">';
	echo $this->Form->input('Funcione.visible', array('size'=>'40','label'=>false,'options'=>$var2,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','div'=>false,'class'=>"form-control select2"));
	echo '</div>';
        echo '</div>';
        $i=1;$var3=array(); $var3[0]='No tiene';
	foreach ($funciones as $row) {
	$item = $row['Funcione'];	
	$var3[$item['id']]=$item['nombre'];
	$i=$i+1;
	}
        echo '<div class="form-group">';
         echo $this->Html->tag('label', 'Padre:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
        echo '<div class="col-xs-6">';
	echo $this->Form->input('Funcione.parent_id', array('size'=>'40','label'=>false,'options'=>$var3,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','div'=>false,'class'=>"form-control select2"));
	echo '</div>';
        echo '</div>';
	echo $this->Form->end();
?>
</fieldset>
</div>
<form>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/funciones", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
</form>
<?php
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('bootstrap.min.js');
    echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
    echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
    echo $this->Html->script('plugins/select2/select2.full.min.js');
    echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
  
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
</script>