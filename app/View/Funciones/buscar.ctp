<?php if (count($data)>0){?>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="pure-table pure-table-horizontal">
<thead>
    <tr>
        <th >Codigo</th>
        <th>Nombre</th>
        <th>Created</th>
        <th>Modified</th>
        <th colspan="2">Actions</th>
    </tr>
</thead>	
    <?php $x=0; foreach ($data as $row): $funcione = $row['Funcione']; ?>    
    <?php $x++; ?>
	<?php if($x % 2 == 0){ ?>
	<tr class="roweven">
	<?php } else { ?>
	<tr>
	<?php } ?>
        <td><?php echo $funcione['id'] ?></td>
        <td><?php echo $this->Html->link($funcione['nombre'], '/funciones/view/'.$funcione['id']) ?></td>      
        <td><?php echo $funcione['created'] ?></td>
        <td>
            <?php 
                if (!empty($funcione['modified'])) echo $funcione['modified'];
                else if (!empty($funcione['updated'])) echo $funcione['updated'];
            ?>
        </td>
        <td align="center">
	  <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/funciones/view/".$funcione['id'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/funciones/edit/".$funcione['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/funciones/delete/".$funcione['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$funcione['nombre'].'"?');
		?> 
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php }else{
	echo 'No se consiguio Registros';
} ?>
