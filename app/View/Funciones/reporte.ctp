<?php
// Construccion de la Cabecera
	$fpdf->SetTitle($titulo);
	$fpdf->SetLogoIzq($logoIzq);
	$fpdf->SetLogoDer($logoDer);
	$fpdf->SetLine1($line1);
	$fpdf->SetLine2($line2);
	$fpdf->SetLine3($line3);
	$fpdf->SetLine4($line4);

// obtiene el nro de paginas    
    $fpdf->AliasNbPages(); 
    $fpdf->AddPage();
    $fpdf->SetFont('Arial','B',16);
	
 //  Anchos de Celdas
    $w=array(15,80,47,48);// maxima sumatoria 190
 	$header=array('Codigo','Nombre','Grupo','Modificado');
	$datos=array();
	foreach ($data as $row){
		$registro = $row['Funcione'];
		$datos[]=array($registro['id'],$registro['nombre'],$row['Grupo']['nombre'],$registro['created']);
	} 
 	$fpdf->FancyTable($header,$datos,$w);
    echo $fpdf->fpdfOutput();    
    
?> 