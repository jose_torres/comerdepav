<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<fieldset id="personal" >
<legend class="info"><h2>MOSTRAR UNA FUNCI&Oacute;N</h2></legend>
<table>
	<tr>
		<td><label>Nombre:</label></td>
		<td><?php echo $data['Funcione']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Direcci&oacute;n:</label></td>
		<td><?php echo $data['Funcione']['direccion']; ?> </td>
	</tr>
	<tr>
		<td><label>Grupo:</label></td>
		<td><?php echo $data['Grupo']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Creado:</label></td>
		<td><?php echo $data['Funcione']['created']; ?> </td>
	</tr>
</table>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/funciones", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
</fieldset>
