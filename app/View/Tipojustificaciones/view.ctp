<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype'); 
?>
<div class="unidades view">
<fieldset id="personal" >
<legend class="info"><h2> MOSTRAR DATOS DE UN TIPO DE JUSTIFICACI&Oacute;N </h2></legend>
	<table>
	<tr>
		<td><label>ID:</label></td>
		<td><?php echo $unidade['Tipojustificacione']['id']; ?> </td>
	</tr>
	<tr>
		<td><label>Nombre:</label></td>
		<td><?php echo $unidade['Tipojustificacione']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Acci&oacute;n:</label></td>
		<td><?php echo $unidade['Tipojustificacione']['accion']; ?> </td>
	</tr>
	<tr>
		<td><label>Creado:</label></td>
		<td><?php echo date("d-m-Y H:i:s",strtotime($unidade['Tipojustificacione']['created'])); ?> </td>
	</tr>
	<tr>
		<td><label>Modificado:</label></td>
		<td><?php echo date("d-m-Y H:i:s",strtotime($unidade['Tipojustificacione']['modified'])); ?> </td>
	</tr>
</table>
</fieldset>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/tipojustificaciones", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
<fieldset>
<div class="related">
	
	<?php if (!empty($unidade['Documento'])):?>
	<h3><?php __(' Documentos Relacionados');?></h3>
	<table cellpadding = "0" cellspacing = "0" width="90%" class="pure-table pure-table-horizontal">
	<thead>	
	<tr>
		<th><?php __('Nro'); ?></th>
		<th><?php __('Id'); ?></th>
		<th><?php __('Nombre'); ?></th>
		<th><?php __('estatus'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php
		$i = 0;
		foreach ($unidade['Documento'] as $articulo):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="pure-table-odd"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $i;?></td>
			<td><?php echo $articulo['id'];?></td>
			<td><?php echo $articulo['titulo'];?></td>
			<td><?php echo $articulo['usuario_id'];?></td>
			<td><?php echo date("d-m-Y H:i:s",strtotime($articulo['created']));?></td>
			<td><?php echo date("d-m-Y H:i:s",strtotime($articulo['modified']));?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
<?php endif; ?>

</div>
</fieldset>
