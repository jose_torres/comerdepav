<?php
echo $this->Html->css('daterangepicker/daterangepicker.css');
//echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
//Funciones js propias del archivo.ctp
echo $this->Html->script(array('funciones.js','View/Ventas/cierre'));
?>
<fieldset id="personal" >
<legend>CUADRE DE CAJA </legend>
<?php 
echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'cierre','id'=>'frm','name'=>'frm',"onsubmit"=>"return validar();"));

echo '<div class="form-group">';
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-1,date('Y')));
echo $this->Html->tag('label', 'Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
//echo '<div class="input-daterange input-group col-xs-1" id="datepicker">';
echo '<div class="input-daterange col-xs-2" id="datepicker">';

/*echo $this->Form->input('Reporte.desde',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true));
echo '<span class="input-group-addon">a</span>';*/
echo $this->Form->input('Reporte.hasta',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Hasta",'value'=>$fecha2,'readonly'=>true)); 
echo '</div>';		
//echo '</div>';

//echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-1 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));

echo '</div>';
//echo '</div>';

$tiporeporte['Ventas_Diarias_Cierre']='Ventas_Diarias_Cierre';
echo $this->Form->input('Reporte.tiporeporte',array('div'=>false,'type'=>'hidden','label'=>false,'data-placeholder'=>"Seleccione Tipo de Ventas",'value'=>$tiporeporte['Ventas_Diarias_Cierre']));

echo '<div class="col-xs-2">';
	?>
<button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarventas/','','tiporeporte='+document.frm.ReporteTiporeporte.value+'&fechadesde='+document.frm.ReporteHasta.value+'&fechahasta='+document.frm.ReporteHasta.value+'&sucursal='+document.frm.ReporteSucursal.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>

<?php 
echo '</div>';
echo '</div>'; 
?>
<center>
<!-- <form action="" method="post" name="otro">  -->
<div id="buscar">

</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
  &nbsp;&nbsp;&nbsp;
<?php 
    echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</center>
</form>
</fieldset>
</fieldset>
<?php
    echo $this->Html->script('jquery.min.js');
    echo $this->Html->script('bootstrap.min.js');
    echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
    echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
    echo $this->Html->script('plugins/select2/select2.full.min.js');
    echo $this->Html->script('plugins/select2/i18n/es.js');
    echo $this->Html->script('plugins/slimScroll/jquery.slimscroll.min.js');
    //echo $this->Html->script('plugins/fastclick/fastclick.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
</script>
