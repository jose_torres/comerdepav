<?php
    /*echo '<pre>';
    print_r($banco);
    echo '</pre>';*/
    echo $this->Form->input('Cuadre.id', array('type'=>'hidden','value'=>$cuadre['Cuadrediario']['id']));
    echo $this->Form->input('Cuadre.fecha', array('type'=>'hidden','value'=>$cuadre['Cuadrediario']['fecha']));
?>

<div class="panel"><h2>Resumen de Cierre de Caja</h2></div>
<div class="box-body table-responsive no-padding">
    <button type="submit" class="btn btn-primary">Cerrar Caja&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
<?php 
    echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => '../cuadrediarios/viewpdf', $cuadre['Cuadrediario']['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
?>	

<div id="exTab3" class="panel">	
<ul  class="nav nav-pills">
    <li class="active"><a href="#4b" data-toggle="tab"><h4><strong>Resumen</strong></h4></a>
    </li>
    <li><a href="#6b" data-toggle="tab"><h4><strong>Cuadre</strong></h4></a>
    </li>			
    <li>
    <a href="#1b" data-toggle="tab"><h4><strong>Transacciones</strong></h4></a>
    </li>
    <li><a href="#5b" data-toggle="tab"><h4><strong>Pagos</strong></h4></a>
    </li>
    <li><a href="#2b" data-toggle="tab"><h4><strong>Cargos</strong></h4></a>
    </li>
    <li><a href="#3b" data-toggle="tab"><h4><strong>Descargos</strong></h4></a></li>
<!--		
    <li><a href="#7b" data-toggle="tab"><h4><strong>Gastos</strong></h4></a>
    </li>
    
      		-->
			
</ul>

<div class="tab-content clearfix">
<div class="tab-pane" id="1b">
    <div class="panel panel-primary">
 
    <div class="panel-heading"><h3><strong><?php echo $titulo; ?></strong></h3></div>
 
    <table class="table table-bordered">
        <tr>
            <th width="7%">Fecha</th>	
            <th width="7%">Hora</th>	
            <th width="5%">Nro</th>
            <th width="7%">Factura</th>
            <th width="25%">Cliente</th>
            <th>Total</th>
            <th>Contado</th>
            <th>Cr&eacute;dito</th>		  
            <th>Iva</th>
            <th>Retenciones</th>		  
            <th>Pag. Efectivo</th>
            <th>Pag. Debito</th>
            <th>Pag. Cheque</th>
            <th>Pag. Transfer.</th>
            <th>Tipo de Ventas</th>
        </tr>
	<?php
            $suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;$suma_efectivo = $suma_cheque = $suma_debito = $suma_transfer = 0;
            $BANCO='';$CONT=0;	
            foreach ($data as $row){
            $CONT=$CONT+1;			

            $suma_factura_contado=$suma_factura_contado + $row[0]['contado'];
            $suma_factura_credito=$suma_factura_credito + $row[0]['credito'];		
            $suma_iva=$suma_iva + $row[0]['impuestos'];
            $suma_retencion =$suma_retencion + $row[0]['retencion'];
            $suma_efectivo =$suma_efectivo + $row[0]['pagoefectivo'];
            $suma_cheque =$suma_cheque + $row[0]['pagocheque'];
            $suma_debito =$suma_debito + $row[0]['pagodebito'];
            $suma_transfer =$suma_transfer + $row[0]['pagotransfer'];
            $suma_bruto=$suma_bruto + $row[0]['contado'] + $row[0]['impuestos'] + $row[0]['credito'];
        ?>
        <tr>
            <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>
            <td><?php echo $row[0]['hora'];?></td>
            <td><?php echo $row[0]['codventa'];?></td>
            <td><?php echo $row[0]['numerofactura'];?></td>
            <td><?php echo $row[0]['rif'].'=>'.$row[0]['descripcion']?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado'] + $row[0]['impuestos'] + $row[0]['credito']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['credito']), 2, ',', '.');?></td>		  
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['impuestos']), 2, ',', '.');?></td>		 
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['retencion']), 2, ',', '.');?></td>		 
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagoefectivo']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagodebito']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagocheque']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagotransfer']), 2, ',', '.');?></td>
            <td><?php 
                if($row[0]['estatus']=='P'){
                    echo 'Contado';
                }elseif($row[0]['estatus']=='A'){
                    echo 'Credito';
                }elseif($row[0]['estatus']=='E'){
                    echo 'Devuelto';
                }
            ?>
            </td>
        </tr> 		
<?php		
            }
    $total_ventas['factura']=$suma_factura;
    $total_ventas['iva']=$suma_iva;
    $total_ventas['bruto']=$suma_bruto;
    $total_ventas['cheque']=$suma_cheque;
    $total_ventas['efectivo']=$suma_efectivo;
    $total_ventas['debito']=$suma_debito;
    $total_ventas['transfer']=$suma_transfer;$total_ventas['retencion']=$suma_retencion;	
    $total_ventas['factura_contado']=$suma_factura_contado;		
    $total_ventas['factura_credito']=$suma_factura_credito;		
    $total_ventas['numero']=$CONT;		
    echo $this->Form->input('Venta.totales', array('type' =>'hidden','label' =>'','value'=>$CONT));
?>

        <tfoot>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>	
                <td>&nbsp;</td>	
                <td align="right"><label>Totales:</label></td>	
                 <td>&nbsp;</td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td> 
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.');?></td>		  
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_efectivo), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_debito), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_cheque), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_transfer), 2, ',', '.');?></td>
                <td>&nbsp;</td>		  
            </tr>
        </tfoot>
 </table>
<?php
if($CONT>0){
}else{
    echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?>
</div>

<div class="panel panel-danger">
 
    <div class="panel-heading"><h3><strong><?php echo 'Devoluciones de Ventas'; ?></strong></h3></div>
 
  <table class="table table-bordered">
        <tr>
            <th width="7%">Fecha</th>	
            <th width="7%">Hora</th>	
            <th width="7%">Nro</th>
            <th width="30%">Cliente</th>
            <th>Total</th>
            <th>Contado</th>
            <th>Cr&eacute;dito</th>		  
            <th>Iva</th>
            <th>Retenciones</th>		  
            <th>Pag. Efectivo</th>
            <th>Pag. Debito</th>
            <th>Pag. Cheque</th>
            <th>Pag. Transfer.</th>
            <th>Fact. Afecta</th>
        </tr>
<?php
    $suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;$suma_efectivo = $suma_cheque = $suma_debito = $suma_transfer = 0;
        $BANCO='';$CONT=0;	
        foreach ($data_dev as $row){
        $CONT=$CONT+1;	
        $suma_factura_contado=$suma_factura_contado + $row[0]['contado'];
        $suma_factura_credito=$suma_factura_credito + $row[0]['credito'];		
        $suma_iva=$suma_iva + $row[0]['impuestos'];
        $suma_retencion =$suma_retencion + $row[0]['retencion'];
        $suma_efectivo =$suma_efectivo + $row[0]['pagoefectivo'];
        $suma_cheque =$suma_cheque + $row[0]['pagocheque'];
        $suma_debito =$suma_debito + $row[0]['pagodebito'];
        $suma_transfer =$suma_transfer + $row[0]['pagotransfer'];
        $suma_bruto=$suma_bruto + $row[0]['contado'] + $row[0]['impuestos'] + $row[0]['credito'];
?>
        <tr>
            <td><?php echo date("d-m-Y",strtotime($row[0]['fecha']));?></td>
            <td><?php echo date("h:m:s",strtotime($row[0]['fecha']));?></td>
            <td><?php echo str_pad($row[0]['coddevolucion'], 10, "0", STR_PAD_LEFT);?></td>
            <td><?php echo $row[0]['rif'].'=>'.$row[0]['descripcion']?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado'] + $row[0]['impuestos'] + $row[0]['credito']), 2, ',', '.');?></td>
             <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['contado']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['credito']), 2, ',', '.');?></td>		  
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['impuestos']), 2, ',', '.');?></td>		 
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['retencion']), 2, ',', '.');?></td>		  
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagoefectivo']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagodebito']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagocheque']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['pagotransfer']), 2, ',', '.');?></td>
            <td><?php echo $row[0]['numerofactura'];?></td>
         </tr> 		
<?php
}

$total_Devoluciones['factura']=$suma_factura;
$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;
$total_Devoluciones['cheque']=$suma_cheque;
$total_Devoluciones['efectivo']=$suma_efectivo;
$total_Devoluciones['debito']=$suma_debito;
$total_Devoluciones['transfer']=$suma_transfer;
$total_Devoluciones['retencion']=$suma_retencion;
$total_Devoluciones['factura_contado']=$suma_factura_contado;		
$total_Devoluciones['factura_credito']=$suma_factura_credito;
$total_Devoluciones['numero']=$CONT;
?>
        <tfoot>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>	
                <td>&nbsp;</td>	
                <td align="right"><label>Totales:</label></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_retencion), 2, ',', '.');?></td>		  
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_efectivo), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_debito), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_cheque), 2, ',', '.');?></td>
                <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_transfer), 2, ',', '.');?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>		  
            </tr>
		
	</tfoot>
    </table>

</div>

<div class="panel panel-info">
 
    <div class="panel-heading"><h3><strong><?php echo 'Totales (Ventas - Devoluciones):'; ?></strong></h3></div>
    <table class="table table-bordered">
	<tr>
            <th align="center" width="35%">&nbsp;</th>
            <th align="center">Total</th>		  
            <th align="center">Monto Contado</th>
            <th align="center">Monto Cr&eacute;dito</th>
            <th align="center">Iva</th>
            <th align="center">Retenciones</th>		  		  
            <th align="center">Efectivo</th>
            <th align="center">Debito</th>
            <th align="center">Cheque</th>
            <th align="center">Transfer.</th>
        </tr>
    <tfoot>
        <tr>
            <td align="right"  width='38%'><label>Totales Neto:</label></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_contado']- $total_Devoluciones['factura_contado']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_credito']- $total_Devoluciones['factura_credito']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['retencion']- $total_Devoluciones['retencion']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['efectivo']- $total_Devoluciones['efectivo']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['debito']- $total_Devoluciones['debito']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['cheque']- $total_Devoluciones['cheque']), 2, ',', '.');?></td>
            <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['transfer']- $total_Devoluciones['transfer']), 2, ',', '.');?></td>
        </tr>
    </tfoot>
	
</table> 

</div>

</div>

<div class="tab-pane" id="2b">
<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong>Cargos</strong></h3>
  </div>
<!-----Listado de Cargos-------->   
    <table class="table table-bordered table-hover">
        <tr>
          <th>N&uacute;mero</th>
          <th>Motivo</th>
          <th>Autorizado</th>		 	  
          <th>Responsable</th>          
          <th>Monto</th>          
        </tr>
        <tbody>
        <?php
        $A_DEV_SUC = 0;
        $x=0;$total_registros=0; $total_cantidad=0;
        $total_monto_neto=$total_monto_iva=$total_monto=0;
        
        foreach ($cargos as $row){
            $total_monto=$total_monto + $row[0]['total'];
            $x=$x+1;$class = ""; $checked = false;            
         ?>
         <tr id='td_id' class="<?php echo $class;?>">
            <td align="center"><?php echo str_pad($row[0]['codcargo'], 8, "0", STR_PAD_LEFT); ?></td>
            <td align="left"><?php echo $row[0]['descripcion']; ?></td>
            <td align="left"><?php echo $row[0]['autorizado']; ?></td>        
            <td align="left"><?php echo $row[0]['responsable']; ?></td>            
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total'] ), 2, ',', '.'); ?></td>
         </tr>    
         <?php } ?>
        </tbody>
        <tfoot>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>                        
            <th>&nbsp;</th>
            <th><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto ), 2, ',', '.');  ?></div></th>            
        </tfoot>
    </table>
</div>    
    
</div>

<div class="tab-pane" id="3b">
 <div class="panel panel-primary">
  <div class="panel-heading"><h3><strong>Descargos</strong></h3>
  </div>
<!-----Listado de Descargos-------->   
<table class="table table-bordered table-hover">
        <tr>
          <th>N&uacute;mero</th>
          <th>Motivo</th>
          <th>Autorizado</th>		 	  
          <th>Responsable</th>          
          <th>Monto</th>          
        </tr>
        <tbody>
        <?php
        $A_DEV_SUC = 0;
        $x=0;$total_registros=0; $total_cantidad=0;
        $total_monto_neto=$total_monto_iva=$total_monto=0;
        
        foreach ($descargos as $row){
            $total_monto=$total_monto + $row[0]['total'];
            $x=$x+1;$class = ""; $checked = false;            
         ?>
         <tr id='td_id' class="<?php echo $class;?>">
            <td align="center"><?php echo str_pad($row[0]['coddescargo'], 8, "0", STR_PAD_LEFT); ?></td>
            <td align="left"><?php echo $row[0]['descripcion']; ?></td>
            <td align="left"><?php echo $row[0]['autorizado']; ?></td>        
            <td align="left"><?php echo $row[0]['responsable']; ?></td>            
            <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total'] ), 2, ',', '.'); ?></td>
         </tr>    
         <?php } ?>
        </tbody>
        <tfoot>
            <th>&nbsp;</th>
            <th>&nbsp;</th>
            <th>&nbsp;</th>                        
            <th>&nbsp;</th>
            <th><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto ), 2, ',', '.');  ?></div></th>            
        </tfoot>
    </table>
</div>
</div>

<div class="tab-pane" id="5b">
<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong><?php echo 'Pagos en Efectivo'; ?></strong></h3></div>
    <table class="table table-bordered table-hover">
		<tr>
		  <th>Pago</th>
		  <th>Movimiento</th>
		  <th>Fecha Pago</th>
		  <th>Monto Pagado</th>
		  <th>Nro</th>
		  <th>Banco</th>
		  <th>Vendedor</th>
		</tr>
	<?php
		$pagos['efectivo']=0;
		$suma_factura=0;	
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($datapago['efectivo'] as $row){
			$suma_factura=$suma_factura + $row[0]['monto_pagado'];
		?>
		<tr>
		  <td><?php  
                if($row[0]['tipopago']==''){
                    echo 'SIN PAGO';
                }else{
                    echo $row[0]['tipopago'];
                }
		  ;
		  ?></td>
		  <td><?php echo $row[0]['tipomovimientopago'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto_pagado']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['banco'];?></td>
		  <td><?php echo $row[0]['nombre_vendedor'];?></td>
		 </tr> 
		<?php
		$BANCO= $row[0]['rif'].'='.$row[0]['numerofactura'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Pagado:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php //echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
</table>
<?php
	$pagos['efectivo']=$suma_factura;
?>
  </div>
<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong><?php echo 'Pagos en Debito'; ?></strong></h3></div>
    <table class="table table-bordered table-hover">
		<tr>
		  <th>Pago</th>
		  <th>Movimiento</th>
		  <th>Fecha Pago</th>
		  <th>Monto Pagado</th>
		  <th>Nro</th>
		  <th>Banco</th>
		  <th>Vendedor</th>
		</tr>
	<?php
		$suma_factura=0;	$pagos['debito']=0;
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($datapago['debito'] as $row){
			$suma_factura=$suma_factura + $row[0]['monto_pagado'];
		?>
		<tr>
		  <td><?php  if($row[0]['tipopago']==''){
					echo 'SIN PAGO';
				}else{
					echo $row[0]['tipopago'];
				}
		  ;
		  ?></td>
		  <td><?php echo $row[0]['tipomovimientopago'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto_pagado']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['banco'];?></td>
		  <td><?php echo $row[0]['nombre_vendedor'];?></td>
		 </tr> 
		<?php
		$BANCO= $row[0]['rif'].'='.$row[0]['numerofactura'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Pagado:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php //echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
</table>
<?php
	$pagos['debito']=$suma_factura;
?>
  </div>

<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong><?php echo 'Pagos en Cheque'; ?></strong></h3></div>
    <table class="table table-bordered table-hover">
		<tr>
		  <th>Pago</th>
		  <th>Movimiento</th>
		  <th>Fecha Pago</th>
		  <th>Monto Pagado</th>
		  <th>Nro</th>
		  <th>Banco</th>
		  <th>Vendedor</th>
		</tr>
	<?php
		$suma_factura=0;$pagos['cheque']=0;	
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($datapago['cheque'] as $row){
			$suma_factura=$suma_factura + $row[0]['monto_pagado'];
		?>
		<tr>
		  <td><?php  if($row[0]['tipopago']==''){
					echo 'SIN PAGO';
				}else{
					echo $row[0]['tipopago'];
				}
		  ;
		  ?></td>
		  <td><?php echo $row[0]['tipomovimientopago'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto_pagado']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['banco'];?></td>
		  <td><?php echo $row[0]['nombre_vendedor'];?></td>
		 </tr> 
		<?php
		$BANCO= $row[0]['rif'].'='.$row[0]['numerofactura'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Pagado:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php //echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
</table>
<?php
$pagos['cheque']=$suma_factura;
?>
  </div>

<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong><?php echo 'Pagos en Transferencia'; ?></strong></h3></div>
    <table class="table table-bordered table-hover">
		<tr>
		  <th>Pago</th>
		  <th>Movimiento</th>
		  <th>Fecha Pago</th>
		  <th>Monto Pagado</th>
		  <th>Nro</th>
		  <th>Banco</th>
		  <th>Vendedor</th>
		</tr>
	<?php
		$suma_factura=0;	$pagos['transfer']=$suma_factura;
		$suma_iva=0;	$BANCO='';$CONT=0;	
		foreach ($datapago['transfer'] as $row){
			$suma_factura=$suma_factura + $row[0]['monto_pagado'];
		?>
		<tr>
		  <td><?php  if($row[0]['tipopago']==''){
					echo 'SIN PAGO';
				}else{
					echo $row[0]['tipopago'];
				}
		  ;
		  ?></td>
		  <td><?php echo $row[0]['tipomovimientopago'];?></td>
		  <td><?php echo date("d-m-Y",strtotime($row[0]['fechapago']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['monto_pagado']), 2, ',', '.');?></td>
		  <td><?php echo $row[0]['nropago'];?></td>
		  <td><?php echo $row[0]['banco'];?></td>
		  <td><?php echo $row[0]['nombre_vendedor'];?></td>
		 </tr> 
		<?php
		$BANCO= $row[0]['rif'].'='.$row[0]['numerofactura'];
		$CONT=$CONT+1;
		 }?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td align="right"><label>Total Pagado:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php //echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
</table>
<?php
$pagos['transfer']=$suma_factura;
?>
  </div>



</div>
<!-- </div> -->
<div class="tab-pane active" id="4b">
	<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong>Resumen de Cuadre</strong></h3></div>  
     <div class="panel-body">
<!-- Resumen de Venta-->		 
 <table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE VENTAS</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Totales</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Ventas a Contado sin Iva</td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_contado']- $total_Devoluciones['factura_contado']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Ventas a Cr&eacute;dito sin Iva</td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_credito']- $total_Devoluciones['factura_credito']), 2, ',', '.');?></td>
	</tr>
	<tr class="active">
	  <td>3.</td>
	  <td><strong>Venta sin IVA</strong></td>
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f",($total_ventas['factura_credito']- $total_Devoluciones['factura_credito'])+($total_ventas['factura_contado']- $total_Devoluciones['factura_contado'])), 2, ',', '.');?></strong></td>
	</tr>
	<tr >
	  <td>4.</td>
	  <td>Total IVA</td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.');?></td>
	</tr>
	<tr class="active">
	  <td>5.</td>
	  <td><strong>Total Operaci&oacute;n</strong></td>
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.');?></strong></td>
	</tr>
 </table>            
<!-- -->
<!-- Resumen de Caja-->
<?php
$total_ingreso=$pagos['efectivo']+$pagos['debito']+$pagos['cheque']+$pagos['transfer'] + $retenciones['Total']['total'];
$porc['efectivo'] = $porc['debito'] = $porc['cheque'] = $porc['transfer'] = $porc['retencion'] = 0;
if($pagos['efectivo']>0){$porc['efectivo']=$pagos['efectivo']/$total_ingreso*100;};
if($pagos['debito']>0){$porc['debito']=$pagos['debito']/$total_ingreso*100;};
if($pagos['cheque']>0){$porc['cheque']=$pagos['cheque']/$total_ingreso*100;};
if($pagos['transfer']>0){$porc['transfer']=$pagos['transfer']/$total_ingreso*100;};
if($retenciones['Total']['total']>0){$porc['retencion']=$retenciones['Total']['total']/$total_ingreso*100;};
?>
 <table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE CAJA</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Totales</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Ingresos por Ventas en Efectivo&nbsp;<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['efectivo']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$pagos['efectivo']), 2, ',', '.');?></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Ingresos por Ventas en D&eacute;bito<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['debito']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$pagos['debito']), 2, ',', '.');?></td>
	</tr>
	<tr >
	  <td>3.</td>
	  <td>Ingresos por Ventas en Cheque&nbsp;<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['cheque']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$pagos['cheque']), 2, ',', '.');?></td>
	</tr>
	<tr >
	  <td>4.</td>
	  <td>Ingresos por Ventas en Transferencia&nbsp;<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['transfer']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$pagos['transfer']), 2, ',', '.');?></td>
	</tr>
	<tr >
	  <td>5.</td>
	  <td>Retenciones Realizadas por Cliente&nbsp;<span class="badge bg-green"><?php echo number_format(sprintf("%01.2f",$porc['retencion']), 2, ',', '.');?>&#37;</span></td>
	  <td align="right"><?php echo number_format(sprintf("%01.2f",$retenciones['Total']['total']), 2, ',', '.');?></td>
	</tr>
	<tr class="active">
	  <td>6.</td>
	  <td><strong>Total Ingresos</strong></td>
	  <td align="right"><strong><div id="total_ingreso_cuadre"><?php echo number_format(sprintf("%01.2f", $pagos['efectivo']+$pagos['debito']+$pagos['cheque']+$pagos['transfer']+$retenciones['Total']['total']), 2, ',', '.');?></div></strong></td>
	</tr>
	<tr class="active">
	  <td>7.</td>
	  <td><strong>N&uacute;mero de Facturas</strong></td>
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_ventas['numero']), 2, ',', '.');?></strong></td>
	</tr>
	<tr class="active">
	  <td>8.</td>
	  <td><strong>N&uacute;mero de Devoluciones</strong></td>
	  <td align="right"><strong><?php echo number_format(sprintf("%01.2f", $total_Devoluciones['numero']), 2, ',', '.');?></strong></td>
	</tr>
 </table>    
<!-- -->
<!-- Resumen de Depositos-->
<table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE DEP&Oacute;SITO</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Totales</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Dep&oacute;sitos en Efectivo&nbsp;</td>
	  <td align="right"><div align="right" id="tot_res_efectivo"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Dep&oacute;sitos en D&eacute;bito&nbsp;</td>
	  <td align="right"><div align="right" id="tot_res_debito"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr >
	  <td>3.</td>
	  <td>Dep&oacute;sitos en Cheque</td>
	  <td align="right"><div align="right" id="tot_res_cheque"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr >
	  <td>4.</td>
	  <td>Dep&oacute;sitos en Transferencia</td>
	   <td align="right"><div align="right" id="tot_res_trans"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr class="active">
	  <td>5.</td>
	  <td><strong>Total Dep&oacute;sitos</strong></td>
	  <td align="right"><strong><div align="right" id="tot_res_depositos"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></strong></td>
	</tr>
</table>

<table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE GASTOS Y RETENCIONES</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Totales</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td>Gastos Realizados</td>
	   <td align="right"><div align="right" id="tot_res_gastos"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></td>
	</tr>
	<tr>
	  <td>2.</td>
	  <td>Retenciones Realizados por Clientes Registradas</td>
	   <td align="right"><div align="right" id="tot_res_retencion"><?php echo number_format(sprintf("%01.2f",$retenciones['Total']['total']), 2, ',', '.');?></div></td>
	</tr>
 </table>
 
<table class="table table-condensed table-bordered"  width="75%">
	<caption><strong>RESUMEN DE CAJA</strong></caption>
	<tr>
	  <th width="5%">#</th>
	  <th width="55%">Mensaje</th>
	  <th width="40%" align="center">Monto</th>
	</tr>
	<tr>
	  <td>1.</td>
	  <td><strong><div id="mensaje_cuadre">Cuadrado</div></strong></td>
	   <td align="right"><strong><div align="right" id="total_cuadre"><?php echo number_format(sprintf("%01.2f",0), 2, ',', '.');?></div></strong></td>
	</tr>
 </table>
<!-- -->
  </div>
  <div class="panel-footer">&nbsp;</div>     
	</div>
</div>

<div class="tab-pane" id="6b">
<div class="panel panel-primary">
  <div class="panel-heading"><h3><strong>Cuadre Diario</strong></h3></div> 
 <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
		  <?php
		  $AEfectivo=0;
		  ?>
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 1 .-&nbsp;<?php echo $this->Html->image("img_acciones/efectivo.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>En Efectivos</strong>&nbsp;&nbsp;Monto Recibido:<strong><?php echo number_format(sprintf("%01.2f", $total_ventas['efectivo']- $total_Devoluciones['efectivo']), 2, ',', '.');?></strong>&nbsp;&nbsp;Depositado:<strong><span id='tot_dep_efectivo'><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
		  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Registrar Deposito&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
		  <div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-lg">
			  <div class="modal-content">

				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				  </button>
				  <h4 class="modal-title" id="myModalLabel"><strong>Registrar un Deposito</strong></h4>
				</div>
				<div class="modal-body">
				
				  <p>
<!---          -->
<?php
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Tipo Documentos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Sucdeposito.documentotipo',array('div'=>false,'options'=>$documentotipo['dp'],'label'=>false,'data-placeholder'=>"Seleccione Tipo de Deposito",'class'=>"form-control select2","style"=>"width: 100%;",'empty'=>'Seleccione Tipo de Deposito ...'));

echo '</div>';		
echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" >';
echo $this->Form->input('Sucdeposito.nrodocumento',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Bancos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Sucdeposito.banco',array('div'=>false,'options'=>$banco,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;",'onchange'=>"actualizar('cuenta','buscarctabanco/',''+document.frm.SucdepositoBanco.value+'-'+document.frm.ReporteSucursal.value)",'empty'=>'Seleccione Banco ...'));

echo '</div>';
//echo '</div>';
//echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Cuentas:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" id="cuenta">';
echo $this->Form->input('Sucdeposito.cuentasbancaria_id',array('div'=>false,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';
?>

<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-condensed table-bordered" id="efectivo">
<caption>  <strong>Denominaciones</strong></caption>
<thead>
    <tr>
        <th >Nro</th>
        <th>Denominacion</th>
        <th>Tipo</th>
        <th>Cantidad</th>
        <th>Monto</th>
    </tr>
</thead>
<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($denominacion as $row): $registro = $row['Dinerodenominacione']; ?>
    <?php $x=$x+1; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven" id='td_id'>
	<? } else { ?>
	<tr id='td_id'>
	<? } ?>
        <td align="center">
            <?php echo $x; ?>
        </td>
        <td align="right">
			<?php
			echo $this->Form->input('Efectivo.denominacione_id'.$x, array('type'=>'hidden','value'=>$registro['id']));
			echo $this->Form->input('Efectivo.valor'.$x, array('type'=>'hidden','value'=>$registro['valor']));
			if(isset($data['Efectivo'][$registro['id']]['id']))
			$ident=$data['Efectivo'][$registro['id']]['id'];
            else $ident=0;
			echo $this->Form->input('Efectivo.id'.$x, array('type'=>'hidden','value'=>$ident));
			?>
            <?php echo $registro['denominacion'];
            //echo number_format(sprintf("%01.3f",$registro['denominacion']), 3, ',', '.'); ?>

        </td>
        <td>&nbsp;<?php echo $registro['fin'];
          ?>&nbsp;</td>
        <td align="right">
            <?php
            if(isset($data['Efectivo'][$registro['id']]['cantidad']))$cant=$data['Efectivo'][$registro['id']]['cantidad'];
            else $cant=0;
            $total_cantidad=$total_cantidad+$cant;
            echo $this->Form->input('Efectivo.cantidad'.$x, array('size' => 15,'type' =>'text','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.0f",$cant),'onkeyup'=>"calcular('".$registro['valor']."','".$x."')",'onkeypress'=>'return acceptNum(event)'));
             ?>
        </td>

        <td align="right">
            <?php
            $total_monto=$total_monto+($cant*$registro['valor']);
            echo $this->Form->input('Efectivo.monto'.$x, array('size' => 15,'type' =>'text','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",$cant*$registro['valor']),'readonly'=>true));?>
        </td>
     </tr>
    <?php endforeach; ?>
</tbody>
<tfoot>
    <tr>
        <th colspan="2">Totales</th>
        <th>&nbsp;</th>
	<th>
	<?php echo $this->Form->input('Efectivo.totales', array('type'=>'hidden','value'=>$x)); ?>
	<?php //echo $this->Form->input('Efectivo.lote', array('type'=>'hidden','value'=>$lote['Efectivo']['nro']+1)); ?>

	<div align='right' id='total_cantidad'><?php
	echo number_format(sprintf("%01.2f", $total_cantidad), 2, '.', '.') ;
?></div> </th>
	<th><div align='right' id='total_monto'><?php echo number_format(sprintf("%01.2f", $total_monto), 2, '.', '.') ;
?></div> </th>
</tfoot>
</table>

<!---          -->
			  </p>
			  
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardar('dinero','depositar/',''+document.frm.ReporteSucursal.value,'&opcion=Efectivo&nrodocumento='+document.frm.SucdepositoNrodocumento.value+'&cuentabancaria_id='+document.frm.SucdepositoCuentasbancariaId.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&documentotipo_id='+document.frm.SucdepositoDocumentotipo.value+'&totalDeposito='+document.getElementById('total_monto').innerHTML+'&banco_id='+document.frm.SucdepositoBanco.value+'&total_reg='+document.frm.EfectivoTotales.value)">Guardar Registro</button>
			</div>

		  </div>
		</div>
	  </div>

<div id="dinero">
	<?php //print_r($depositos['Efectivo']);?>
		 <table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Efectivo'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="right">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('dinero','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Efectivo&nrodocumento='+document.frm.SucdepositoNrodocumento.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&movbancario_id=<?php echo $row['Movbancario']['id'];?>')">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_efectivo').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_efectivo').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
</script>		
</div>
	  </div>
    </div>
  </div>
  
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
		  <?php
		  $ADebito=0;
		  ?>
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
        2 .-&nbsp;<?php echo $this->Html->image("img_acciones/credit-card.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>Debitos (Punto de Venta)</strong>&nbsp;&nbsp;Monto Recibido:<strong><?php echo number_format(sprintf("%01.2f", $total_ventas['debito']- $total_Devoluciones['debito']), 2, ',', '.');?></strong>&nbsp;&nbsp;Depositado:<strong><span id="tot_dep_debito"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse">
      <div class="panel-body">
<!-- -->		  
		   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModaldeb">Registrar Deposito&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
		   <div id="myModaldeb" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-lg">
			  <div class="modal-content">

				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				  </button>
				  <h4 class="modal-title" id="myModalLabel"><strong>Registrar un Deposito</strong></h4>
				</div>
				<div class="modal-body">
				
				  <p>
<!---          -->
<?php
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Tipo Documentos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Debito.documentotipo',array('div'=>false,'options'=>$documentotipo['nc'],'label'=>false,'data-placeholder'=>"Seleccione Tipo de Deposito",'class'=>"form-control select2","style"=>"width: 100%;",'empty'=>'Seleccione Tipo de Deposito ...'));

echo '</div>';		
echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" >';
echo $this->Form->input('Debito.nrodocumento',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right","data-toggle"=>"tooltip", "title"=>"Colocar Numero de Lote Generado por el  Punto de Ventas"));
echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Maquinas:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-6">';
echo $this->Form->input('Debito.maquinas',array('div'=>false,'options'=>$puntos,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;",'onchange'=>"actualizar('puntos_ventas','buscarpdvxdep/',''+document.frm.DebitoMaquinas.value+'-'+document.frm.ReporteSucursal.value,'&opcion=Debito&fecha='+document.frm.CuadreFecha.value)",'empty'=>'Seleccione Maquina de Punto de Venta ...'));
echo '</div>';

echo $this->Html->tag('label', 'Cantidad:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-2" >';
echo $this->Form->input('Debito.cantidad',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Cantidad de Movimiento",'onkeyup'=>"numero('DebitoCantidad')",'class'=>"form-control text-right","data-toggle"=>"tooltip", "title"=>"Colocar Cantidad de Movimiento Generado por el Punto de Ventas"));
echo '</div>';
echo '</div>';

?>
<div id="puntos_ventas">
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table  table-condensed table-bordered" id="puntos">
<caption>  <strong>Movimientos en Puntos de Ventas</strong></caption>
<thead>
    <tr>
        <th>Reg.</th>
        <th>Modelo</th>
        <th>Marca</th>
        <th>Serial</th>
        <th>Banco</th>
        <th>Cuenta</th>
        <th>Monto</th>
        <th>Sel.</th>
    </tr>
</thead>
<tbody>

</tbody>
<tfoot>
    <tr>        
        <th>&nbsp;</th>
        <th colspan="2" align='right' ><strong>Total Monto en Punto de Venta:</strong></th>
        <th><div align='right' id='total_monto_pdv'><?php echo number_format(sprintf("%01.2f", 0), 2, '.', '.') ;
?></div> </th>
		<th  align='right' ><strong>Total a Depositar:</strong></th>
	<th>
	<?php echo $this->Form->input('Debito.totales', array('type'=>'hidden','value'=>$x)); ?>
	<div align='right' id='total_monto_deb_dep'><?php
	echo number_format(sprintf("%01.2f", 0), 2, '.', '.') ;
?></div> </th>	
	<th>&nbsp;</th>
</tfoot>
</table>
</div>
<!---          -->
			  </p>
			  
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardardeb('debito','depositar/',''+document.frm.ReporteSucursal.value,'&opcion=Debito&nrodocumento='+document.frm.DebitoNrodocumento.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&documentotipo_id='+document.frm.DebitoDocumentotipo.value+'&totalDeposito='+document.getElementById('total_monto_deb_dep').innerHTML+'&total_reg='+document.frm.DebitoTotales.value+'&cantidad='+document.frm.DebitoCantidad.value)">Guardar Registro</button>
			</div>

		  </div>
		</div>
	  </div>

<div id="debito">
	<?php //print_r($depositos['Efectivo']);?>
		 <table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Debito'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
		<td align="center">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('debito','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Debito&nrodocumento='+document.frm.SucdepositoNrodocumento.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&movbancario_id=<?php echo $row['Movbancario']['id'];?>')">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_debito').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_debito').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
</script>		
</div>		   
		   
<!-- -->		  		   
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
        3.-&nbsp;<?php echo $this->Html->image("img_acciones/cheque.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>Cheques</strong>&nbsp;&nbsp;Monto Recibido:<strong><?php echo number_format(sprintf("%01.2f", $pagos['cheque']- $total_Devoluciones['cheque']), 2, ',', '.');?></strong>&nbsp;&nbsp;Depositado:<strong><span id="tot_dep_cheque"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse">
      <div class="panel-body">
		<?php
		  $ACheque=0;
		?>	  
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalCheque">Registrar Deposito&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
		  <div id="myModalCheque" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-lg">
			  <div class="modal-content">

				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				  </button>
				  <h4 class="modal-title" id="myModalLabel"><strong>Registrar un Deposito</strong></h4>
				</div>
				<div class="modal-body">
				
				  <p>
<!---          -->
<?php
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Tipo Documentos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Cheque.documentotipo',array('div'=>false,'options'=>$documentotipo['dp'],'label'=>false,'data-placeholder'=>"Seleccione Tipo de Deposito",'class'=>"form-control select2","style"=>"width: 100%;",'empty'=>'Seleccione Tipo de Deposito ...'));

echo '</div>';		
echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" >';
echo $this->Form->input('Cheque.nrodocumento',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Bancos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Cheque.banco',array('div'=>false,'options'=>$banco,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;",'onchange'=>"actualizar('cuentaCheque','buscarctabanco/',''+document.frm.ChequeBanco.value+'-'+document.frm.ReporteSucursal.value,'&opcion=Cheque')",'empty'=>'Seleccione Banco ...'));

echo '</div>';
//echo '</div>';
//echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Cuentas:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" id="cuentaCheque">';
echo $this->Form->input('Cheque.cuentasbancaria_id',array('div'=>false,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';
?>
<div id="cheques_x_dep">
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table" id="cheque">
<caption>  <strong>Cheques</strong></caption>
<thead>
    <tr>
        <th>Reg.</th>
        <th>Nro</th>
        <th>Banco</th>
        <th>Cuenta</th>
        <th>Titular</th>
        <th>Monto</th>
        <th>Sel.</th>
    </tr>
</thead>
<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($cheques as $row): $registro = $row['Succheque']; ?>
    <?php $x=$x+1; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven" id='td_id'>
	<? } else { ?>
	<tr id='td_id'>
	<? } ?>
        <td align="center">
            <?php echo $x; ?>
        </td>
        <td align="right">
			<?php
			echo $this->Form->input('Cheque.id'.$x, array('type'=>'hidden','value'=>$registro['id']));
			echo $registro['nro'];
             ?>
        </td>
        <td>&nbsp;<?php echo $row['Banco']['nombre'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['nrocta'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['titular'];?>&nbsp;</td>
        <td align="right">
            <?php
            $total_monto=$total_monto+$registro['monto'];
            echo $this->Form->input('Cheque.monto'.$x, array('size' => 15,'type' =>'text','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",$registro['monto']),'readonly'=>true));
            echo $this->Form->input('Cheque.montosel'.$x, array('size' => 15,'type' =>'hidden','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",0)));
            ?>
        </td>
        <td>&nbsp;<?php echo $this->Form->input('Seleccion'.$x.'.chequear', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'onclick'=>"calcularCheques('Seleccion".$x."Chequear','ChequeMontosel".$x."','ChequeMonto".$x."')"));
		echo $this->Form->input('Valor.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$registro['id']));
		echo $this->Form->input('Valor.'.$x.'.codsucursal', array('type' =>'hidden','label' =>'','value'=>$registro['codsucursal']));		
		?></td>
     </tr>
    <?php endforeach; ?>
</tbody>
<tfoot>
    <tr>        
        <th>&nbsp;</th>
        <th colspan="2" align='right' ><strong>Total Monto en Cheque:</strong></th>
        <th><div align='right' id='total_monto_cheque'><?php echo number_format(sprintf("%01.2f", $total_monto), 2, '.', '.') ;
?></div> </th>
		<th  align='right' ><strong>Total a Depositar:</strong></th>
	<th>
	<?php echo $this->Form->input('Cheque.totales', array('type'=>'hidden','value'=>$x)); ?>
	<div align='right' id='total_monto_che_dep'><?php
	echo number_format(sprintf("%01.2f", 0), 2, '.', '.') ;
?></div> </th>	
	<th>&nbsp;</th>
</tfoot>
</table>
</div>
<!---          -->
			  </p>
			  
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardarcheque('cheques_dep','depositar/',''+document.frm.ReporteSucursal.value,'&opcion=Cheque&nrodocumento='+document.frm.ChequeNrodocumento.value+'&cuentabancaria_id='+document.frm.ChequeCuentasbancariaId.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&documentotipo_id='+document.frm.ChequeDocumentotipo.value+'&totalDeposito='+document.getElementById('total_monto_che_dep').innerHTML+'&banco_id='+document.frm.ChequeBanco.value+'&total_reg='+document.frm.ChequeTotales.value); actualizar('cheques_x_dep','buscarchequesxdep/',''+document.frm.ReporteSucursal.value,'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value)">Guardar Registro</button>
			</div>

		  </div>
		</div>
	  </div>
<!-- -->
<div id="cheques_dep">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Cheque'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('cheques_dep','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Cheque&nrodocumento='+document.frm.SucdepositoNrodocumento.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&movbancario_id=<?php echo $row['Movbancario']['id'];?>');actualizar('cheques_x_dep','buscarchequesxdep/',''+document.frm.ReporteSucursal.value,'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value)">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_cheque').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_cheque').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_depositos').innerHTML=formato_numero(eval(formato_ingles_campo(document.getElementById('tot_res_efectivo').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_debito').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_cheque').innerHTML)) + eval(formato_ingles_campo(document.getElementById('tot_res_trans').innerHTML)) , 2 , ',' , '.');
</script>
</div>


      </div>
    </div>
  </div>

  <div class="panel panel-default panel-faq ">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
        4.-&nbsp;<?php echo $this->Html->image("img_acciones/transferencia.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>Transferencia</strong>&nbsp;&nbsp;Monto Recibido:<strong><?php echo number_format(sprintf("%01.2f", $total_ventas['transfer']- $total_Devoluciones['transfer']), 2, ',', '.');?></strong>&nbsp;&nbsp;Depositado:<strong><span id="tot_dep_transfer"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse">
      <div class="panel-body">
<!-- -->
		  <?php
		  $ATransferencia=0;
		  ?>
		   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModaltran">Registrar Deposito&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
		   <div id="myModaltran" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-lg">
			  <div class="modal-content">

				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				  </button>
				  <h4 class="modal-title" id="myModalLabel"><strong>Registrar un Deposito</strong></h4>
				</div>
				<div class="modal-body">
				
				  <p>
<!---          -->
<?php
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Tipo Documentos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Transfer.documentotipo',array('div'=>false,'options'=>$documentotipo['nc'],'label'=>false,'data-placeholder'=>"Seleccione Tipo de Deposito",'class'=>"form-control select2","style"=>"width: 100%;",'empty'=>'Seleccione Tipo de Deposito ...'));

echo '</div>';		
echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" >';
echo $this->Form->input('Transfer.nrodocumento',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right","data-toggle"=>"tooltip", "title"=>"Colocar Numero de Documento de la Transferencia"));
echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Bancos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Transfer.banco',array('div'=>false,'options'=>$banco,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;",'onchange'=>"actualizar('cuentatran','buscarctabanco/',''+document.frm.TransferBanco.value+'-'+document.frm.ReporteSucursal.value,'&opcion=Transfer')",'empty'=>'Seleccione Banco ...'));

echo '</div>';
//echo '</div>';
//echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Cuentas:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" id="cuentatran">';
echo $this->Form->input('Transfer.cuentasbancaria_id',array('div'=>false,'label'=>false,'data-placeholder'=>"Cuentas Bancarias",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

echo '<div class="form-group">';
echo $this->Html->tag('label', 'Pagos:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-8" id="transfer_x_dep">';
echo $this->Form->input('Transfer.pagos',array('div'=>false,'options'=>$transfer,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;",'onchange'=>"actualizar('montotrans','buscartransferencia/',''+document.frm.TransferPagos.value+'-'+document.frm.ReporteSucursal.value,'&opcion='+document.frm.TransferPagos.value)",'empty'=>'Seleccione Transferencia Realizada ...'));

echo '</div>';
echo '</div>';		
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Monto:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" id="montotrans">';
echo $this->Form->input('Transfer.monto',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Monto de la Transferencia",'onkeyup'=>"numero('TransferMonto')",'class'=>"form-control text-right","data-toggle"=>"tooltip", "title"=>"Colocar Monto Total del Generado por Transferencia",'value'=>0,'readonly'=>true));
echo '</div>';
echo '</div>';
?>

<!---          -->
			  </p>
			  
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardartrans('transfer','depositar/',''+document.frm.ReporteSucursal.value,'&opcion=Transfer&nrodocumento='+document.frm.TransferNrodocumento.value+'&cuentabancaria_id='+document.frm.TransferCuentasbancariaId.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&documentotipo_id='+document.frm.TransferDocumentotipo.value+'&totalDeposito='+document.frm.TransferMonto.value+'&banco_id='+document.frm.TransferBanco.value+'&datospagos='+document.frm.TransferPagos.value);actualizar('transfer_x_dep','buscartransferxdep/',''+document.frm.ReporteSucursal.value,'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value)">Guardar Registro</button>
			</div>

		  </div>
		</div>
	  </div>
<!-- -->
<div id="transfer">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Transfer'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('transfer','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Transfer&nrodocumento='+document.frm.SucdepositoNrodocumento.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&movbancario_id=<?php echo $row['Movbancario']['id'];?>');setTimeout(actualizar('transfer_x_dep','buscartransferxdep/',''+document.frm.ReporteSucursal.value,'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value),8500);">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_transfer').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_trans').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
calcularDepositos();
</script>
</div>

      </div>
    </div>
  </div>


    <div class="panel panel-default panel-faq ">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
        5.-&nbsp;<?php echo $this->Html->image("img_acciones/cart.png", array("alt" => "Efectivo", "title"=>"Efectivo")) ;?>&nbsp;<strong>Gastos</strong>&nbsp;&nbsp;Registrado:<strong><span id="tot_gastos"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse5" class="panel-collapse collapse">
      <div class="panel-body">
<!--  -->
	  <div class="panel panel-primary">
	<div class="panel-heading"><h3><strong><?php echo 'Gastos en Caja'; ?></strong></h3></div>
			   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalgasto">Registrar Gastos&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
		   <div id="myModalgasto" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-lg">
			  <div class="modal-content">

				<div class="modal-header">
				  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				  </button>
				  <h4 class="modal-title" id="myModalLabel"><strong>Registrar Gastos</strong></h4>
				</div>
				<div class="modal-body">
				
				  <p>
<!---          -->
<?php
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" >';
echo $this->Form->input('Gasto.nro',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Colocar el Numero del Gasto",'class'=>"form-control text-right","data-toggle"=>"tooltip", "title"=>"Colocar el Numero del Gasto"));
echo '</div>';

echo $this->Html->tag('label', 'Beneficiario:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Gasto.nombre',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Beneficiario",'class'=>"form-control text-right","data-toggle"=>"tooltip", "title"=>"Colocar Persona o Empresa que se realizo el gasto"));
echo '</div>';		

echo '</div>';


echo '<div class="form-group">';
echo $this->Html->tag('label', 'Motivo:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" >';
echo $this->Form->input('Gasto.motivo',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Colocar Motivo del Gasto",'class'=>"form-control text-right","data-toggle"=>"tooltip", "title"=>"Colocar Motivo del Gasto"));
echo '</div>';
	
echo $this->Html->tag('label', 'Monto:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4" >';
echo $this->Form->input('Gasto.monto',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Monto Total del Gasto",'onkeyup'=>"numero('GastoMonto')",'class'=>"form-control text-right","data-toggle"=>"tooltip", "title"=>"Colocar Monto Total del Gasto"));
echo '</div>';
echo '</div>';
?>

<!---          -->
			  </p>
			  
			</div>
			<div class="modal-footer">
			  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
			  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardargasto('gasto','depositar/',''+document.frm.ReporteSucursal.value,'&opcion=Gasto&motivo='+document.frm.GastoMotivo.value+'&nombre='+document.frm.GastoNombre.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&totalDeposito='+document.frm.GastoMonto.value+'&nro='+document.frm.GastoNro.value)">Guardar Registro</button>
			</div>

		  </div>
		</div>
	  </div>
<!-- -->
<div id="gasto">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nombre</th>
		  <th>Motivo</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Gastos'] as $row){
		$total_monto=$total_monto+$row['Sucgasto']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Sucgasto']['id']; ?></td>
        <td align="left"><?php echo $row['Sucgasto']['nombre']; ?></td>
        <td align="left"><?php echo $row['Sucgasto']['motivo']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Sucgasto']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('gasto','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Gasto&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&sucgasto_id=<?php echo $row['Sucgasto']['id'];?>')">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="3" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
<script>
document.getElementById('tot_gastos').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_gastos').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
</script>
</div>

	
  </div>          
</div>

 
<!--  -->		  
		</div>
    </div>
 

	 <div class="panel panel-default panel-faq ">
    <div class="panel-heading">
      <h4 class="panel-title text-left">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
        6.-&nbsp;<?php echo $this->Html->image("img_acciones/impuestos.png", array("alt" => "Retenciones", "title"=>"Retenciones")) ;?>&nbsp;<strong>Retenciones</strong>&nbsp;&nbsp;Registrado:<strong><span id="tot_retencion"><?php echo number_format(sprintf("%01.2f", 0), 2, ',', '.');?></span></strong></a>
      </h4>
    </div>
    <div id="collapse6" class="panel-collapse collapse">
      <div class="panel-body">
<!--  -->
	<?php
	    $ARetencion =0
	?>
	  <div class="panel panel-primary">
	<div class="panel-heading"><h3><strong><?php echo 'Retenciones Realizadas en Caja'; ?></strong></h3></div>
<!-- -->
<div id="retencion">
	<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>N&uacute;mero</th>
		  <th>Factura</th>		  
		  <th>Cliente</th>		  
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($retenciones['Registros'] as $row){
		$total_monto=$total_monto+$row[0]['montoretenido'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row[0]['codretencion']; ?></td>
        <td align="center"><?php echo $row[0]['numero']; ?></td>
        <td align="left"><?php echo $row[0]['documento']; ?></td>
        <td align="left"><?php echo $row[0]['rif']." ".$row[0]['descripcion']; ?></td>        
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'); ?></td>
        <td align="center">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('retencion','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Retencion&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&codretencion=<?php echo $row[0]['codretencion'];?>')">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		<tfoot>
			<th colspan="4" align="right">Total:</th>
			<th  ><div align="right"><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?></div></th>
			<th>&nbsp;</th>
		</tfoot>
</table>
<script>
document.getElementById('tot_retencion').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_retencion').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
calcularCuadre();
</script>
</div>

	
  </div>          
</div>

 
<!--  -->		  
		</div>
    </div>
  </div>	  
</div>

</div> 	


</div>

<!-- /.box-header -->	
 <button type="submit" class="btn btn-primary">Cerrar Caja&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
<?php 
	echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => '../cuadrediarios/viewpdf', $cuadre['Cuadrediario']['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
?>	
<!-- <button type="button" class="btn btn-info" onclick="seleccionar_todo()">Marcar Todo &nbsp;<span class="glyphicon glyphicon-list-alt"></span></button>
	<button type="button" class="btn btn-danger" onclick="deseleccionar_todo()">Desmarcar Todo &nbsp;<span class="glyphicon glyphicon-list-alt"></span></button> 
	</div> -->
<!-- /.box-body -->
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/fastclick/lib/fastclick.js');
?>
