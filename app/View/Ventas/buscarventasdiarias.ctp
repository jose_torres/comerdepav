<?php
	/*echo '<pre>';
	print_r($data);
	echo '</pre>';*/
	
?>
<!-- /.box-header -->
<div class="box-body table-responsive no-padding">
	<button type="submit" class="btn btn-primary">Guardar&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
	<button type="button" class="btn btn-info" onclick="seleccionar_todo()">Marcar Todo &nbsp;<span class="glyphicon glyphicon-list-alt"></span></button>
	<button type="button" class="btn btn-danger" onclick="deseleccionar_todo()">Desmarcar Todo &nbsp;<span class="glyphicon glyphicon-list-alt"></span></button>
	<table class="table table-bordered">
		 <caption><h2><?php echo $titulo; ?></h2></caption> 
		<tr>
		  <th width="5%">Nro</th>
		  <th width="7%">Factura</th>
		  <th width="25%">Cliente</th>
		  <th width="7%">Fecha</th>
		  <th>Monto Neto</th>
		  <th>Contado</th>
		  <th>Cr&eacute;dito</th>		  
		  <th>Iva</th>
		  <th>Total</th>
		  <th>Tipo de Ventas</th>
		  <th>Asociar</th>
		</tr>
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){
		$CONT=$CONT+1;			
			$base_vp=$iva_vp=0;
			foreach($row['Ventaproducto'] as $registro){
				$base_vp = $base_vp + round($registro['cantidad']*$registro['precio'],2);
				$iva_vp = $iva_vp + round($registro['cantidad']*$registro['precio'] * $registro['iva']/100,2);
			}
			$row['Venta']['baseimp2'] = $base_vp;	
		$suma_factura=$suma_factura + $row['Venta']['baseimp2'];
		$suma_iva=$suma_iva + round($row['Venta']['baseimp2']*$row['Venta']['porimp2']/100,2);
		$suma_bruto=$suma_bruto + $row['Venta']['baseimp2'] + round($row['Venta']['baseimp2']*$row['Venta']['porimp2']/100,2);
			?>
		<tr>
		  <td><?php echo $row['Venta']['codventa'];?></td>
		  <td><?php echo $row['Venta']['numerofactura'];?></td>
		  <td><?php echo $row['Cliente']['rif'].'=>'.$row['Cliente']['descripcion']?></td>
		  <td><?php echo date("d-m-Y",strtotime($row['Venta']['fecha']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.');?></td>
		  <td align="right"><?php 
		  if($row['Venta']['estatus']=='P' || substr($row['Venta']['numerofactura'], 0, 1)=='B'){
				echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.');
				$suma_factura_contado=$suma_factura_contado + $row['Venta']['baseimp2'];				
		  }else{
				echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			  }			
		  ?></td>
		  <td align="right"><?php 
		  if($row['Venta']['estatus']=='A'  || substr($row['Venta']['numerofactura'], 0, 1)=='A'){
				echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2']), 2, ',', '.');
				$suma_factura_credito=$suma_factura_credito + $row['Venta']['baseimp2'];
		  }else{
				echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			  }			
		  ?></td>		  
		  <td align="right"><?php echo number_format(sprintf("%01.2f", round($row['Venta']['baseimp2']*$row['Venta']['porimp2']/100,2)), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Venta']['baseimp2'] + round($row['Venta']['baseimp2']*$row['Venta']['porimp2']/100,2)), 2, ',', '.');?></td>
		  <td><?php if($row['Venta']['estatus']=='P'){
				echo 'Contado';
			  }elseif($row['Venta']['estatus']=='A'){
				echo 'Credito';
			  }elseif($row['Venta']['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>
		  <td>&nbsp;<?php echo $this->Form->checkbox('Seleccion.'.$CONT.'.chequeo', array('size' => 5,'type' =>'radio','label' =>'','value'=>$CONT,'div'=>false));
		echo $this->Form->input('Valor.'.$CONT.'.codventa', array('type' =>'hidden','label' =>'','value'=>$row['Venta']['codventa']));
		echo $this->Form->input('Valor.'.$CONT.'.codsucursal', array('type' =>'hidden','label' =>'','value'=>$row['Venta']['codsucursal']));		
		?>
		  
		  </td>
		 </tr> 		
		<?php		
		
		}
$total_ventas['factura']=$suma_factura;
$total_ventas['iva']=$suma_iva;
$total_ventas['bruto']=$suma_bruto;		
$total_ventas['factura_contado']=$suma_factura_contado;		
$total_ventas['factura_credito']=$suma_factura_credito;		
echo $this->Form->input('Venta.totales', array('type' =>'hidden','label' =>'','value'=>$CONT));
		?>
<?php
if($CONT>0){
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		</tfoot>
 </table>
 <button type="submit" class="btn btn-primary">Guardar&nbsp;<span class="glyphicon glyphicon-floppy-saved"></span></button>
 <button type="button" class="btn btn-info" onclick="seleccionar_todo()">Marcar Todo &nbsp;<span class="glyphicon glyphicon-list-alt"></span></button>
	<button type="button" class="btn btn-danger" onclick="deseleccionar_todo()">Desmarcar Todo &nbsp;<span class="glyphicon glyphicon-list-alt"></span></button>
<?php
}else{
	echo '<div class="alert alert-info"><strong>No se Consiguieron Registro. Por Favor Seleccione otros parametros.</strong></div>';
}
?> 
<!--
<table class="table table-bordered">
		 <caption><h2><?php echo ' Devoluciones de Ventas a Credito'; ?></h2></caption> 
		<tr>
		  <th width="5%">Nro</th>
		  <th width="7%">Devoluci&oacute;n</th>
		  <th width="25%">Cliente</th>
		  <th width="7%">Fecha</th>
		  <th>Monto Neto</th>
		  <th>Contado</th>
		  <th>Cr&eacute;dito</th>
		  <th>Iva</th>
		  <th>Total</th>
		  <th>Tipo de Ventas</th>
		  <th>Fact. Afecta</th>
		</tr>
<?php
$suma_factura=0;$suma_factura_contado=$suma_factura_credito=0; $suma_iva=0; $suma_bruto=0;
		$BANCO='';$CONT=0;	
		foreach ($data_dev as $row){
		$suma_factura=$suma_factura + $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3'];
		$suma_iva=$suma_iva + round($row['Devolucionventa']['baseimp2']*$row['Devolucionventa']['porimp2']/100,2) + round($row['Devolucionventa']['baseimp1']*$row['Devolucionventa']['porimp1']/100,2) + round($row['Devolucionventa']['baseimp3']*$row['Devolucionventa']['porimp3']/100,2);
		$suma_bruto=$suma_bruto + $row['Devolucionventa']['baseimp2'] + round($row['Devolucionventa']['baseimp2']*$row['Devolucionventa']['porimp2']/100,2) + $row['Devolucionventa']['baseimp1'] + round($row['Devolucionventa']['baseimp1']*$row['Devolucionventa']['porimp1']/100,2) + $row['Devolucionventa']['baseimp3'] + round($row['Devolucionventa']['baseimp3']*$row['Devolucionventa']['porimp3']/100,2);
			?>
		<tr>
		  <td><?php echo $row['Venta']['codventa'];?></td>
		  <td><?php echo str_pad($row['Devolucionventa']['coddevolucion'], 10, "0", STR_PAD_LEFT);?></td>
		  <td><?php echo $row['Venta']['Cliente']['rif'].'=>'.$row['Venta']['Cliente']['descripcion']?></td>
		  <td><?php echo date("d-m-Y",strtotime($row['Devolucionventa']['fecha']));?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3']), 2, ',', '.');?></td>
		  <td align="right"><?php 
		  if($row['Venta']['estatus']=='P' || substr($row['Venta']['numerofactura'], 0, 1)=='B'){
				echo number_format(sprintf("%01.2f", $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3']), 2, ',', '.');
				$suma_factura_contado=$suma_factura_contado + $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3'];				
		  }else{
				echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			  }			
		  ?></td>
		  <td align="right"><?php 
		  if($row['Venta']['estatus']=='A' || substr($row['Venta']['numerofactura'], 0, 1)=='A'){
				echo number_format(sprintf("%01.2f", $row['Devolucionventa']['baseimp2'] + + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3']), 2, ',', '.');
				$suma_factura_credito=$suma_factura_credito + $row['Devolucionventa']['baseimp2'] + $row['Devolucionventa']['baseimp1'] + $row['Devolucionventa']['baseimp3'];
		  }else{
				echo number_format(sprintf("%01.2f", 0), 2, ',', '.');
			  }			
		  ?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", round($row['Devolucionventa']['baseimp2']*$row['Devolucionventa']['porimp2']/100,2) + round($row['Devolucionventa']['baseimp1']*$row['Devolucionventa']['porimp1']/100,2) + round($row['Devolucionventa']['baseimp3']*$row['Devolucionventa']['porimp3']/100,2)), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Devolucionventa']['baseimp2'] + round($row['Devolucionventa']['baseimp2']*$row['Devolucionventa']['porimp2']/100,2) + $row['Devolucionventa']['baseimp1'] + round($row['Devolucionventa']['baseimp1']*$row['Devolucionventa']['porimp1']/100,2) + $row['Devolucionventa']['baseimp3'] + round($row['Devolucionventa']['baseimp3']*$row['Devolucionventa']['porimp3']/100,2)), 2, ',', '.');?></td>
		  <td><?php if($row['Venta']['estatus']=='P'){
				echo 'Contado';
			  }elseif($row['Venta']['estatus']=='A'){
				echo 'Credito';
			  }elseif($row['Venta']['estatus']=='E'){
				echo 'Devuelto';
			  }
		  ?>
			  </td>
		  <td><?php echo $row['Venta']['numerofactura'];?></td>
		 </tr> 		
		<?php		
		$CONT=$CONT+1;
		}
		?>		
<?php
$total_Devoluciones['factura']=$suma_factura;
$total_Devoluciones['iva']=$suma_iva;
$total_Devoluciones['bruto']=$suma_bruto;
$total_Devoluciones['factura_contado']=$suma_factura_contado;		
$total_Devoluciones['factura_credito']=$suma_factura_credito;	
?>
<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_contado), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura_credito), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>		  
		</tr>
		
	</tfoot>
 </table>
<table class="table table-bordered">
	<tr>
		  <th align="center" width="44%">&nbsp;</th>		  
		  <th align="center">Monto Neto</th>
		  <th align="center">Monto Contado</th>
		  <th align="center">Monto Cr&eacute;dito</th>
		  <th align="center">Iva</th>
		  <th align="center">Total</th>
		</tr>
	<tfoot>
		<tr>
		  <td align="right"  width='44%'><label>Totales (Ventas - Devoluciones):</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura']- $total_Devoluciones['factura']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_contado']- $total_Devoluciones['factura_contado']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f",$total_ventas['factura_credito']- $total_Devoluciones['factura_credito']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['iva']- $total_Devoluciones['iva']), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $total_ventas['bruto']- $total_Devoluciones['bruto']), 2, ',', '.');?></td>
	  
		</tr>
	</tfoot>
	
</table> 		
-->

	</div>
<!-- /.box-body -->
