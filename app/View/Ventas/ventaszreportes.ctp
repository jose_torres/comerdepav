<?php
//echo $this->element('menuinterno',$datos_menu);
//echo $this->Html->script('prototype');
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');

?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	 // var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	
	function seleccionar_todo(){
	  for (i=0;i<document.frm.elements.length;i++)
      if(document.frm.elements[i].type == "checkbox")
         document.frm.elements[i].checked=1
	} 
	
	function deseleccionar_todo(){
	  for (i=0;i<document.frm.elements.length;i++)
      if(document.frm.elements[i].type == "checkbox")
         document.frm.elements[i].checked=0
	}
	
	function actualizar(nombrediv,url,id,datos){
		
        $('#'+nombrediv).html('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');
		
		var selected = '&clientes=0-';var cont=1;
		$('#ReporteClienteId option:checked').each(function(){
		selected += $(this).val() + '-'; 
		});
		fin = selected.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
		selected = selected.substr( 0, fin ); // elimino la coma final
		
		var vendedores = '&vendedores=0-';var cont=1;
		$('#ReporteVendedoreId option:checked').each(function(){
		vendedores += $(this).val() + '-'; 
		});
		fin = vendedores.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
		vendedores = vendedores.substr( 0, fin ); // elimino la coma final
		
		var tipoventa = '&tipoventa=0-';var cont=1;
		$('#ReporteTipoventa option:checked').each(function(){
		tipoventa += $(this).val() + '-'; 
		});
		fin = tipoventa.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
		tipoventa = tipoventa.substr( 0, fin ); // elimino la coma final
				
		$.ajax({
			type: 'POST',
			url: url+id,
			data: datos+''+selected+''+vendedores+''+tipoventa,
			dataType: 'html'
		})

		.done(function( html ) {					
			$( '#'+nombrediv ).html( html );
		});

    }
	
</script>
<fieldset id="personal" >
<legend>CERRAR VENTAS Z REGISTRADAS</legend>

<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'cerrar','id'=>'frm','name'=>'frm'));
?>	
<?php
/*$sucursal['comerdepa']='Principal';
$sucursal['superdepa']='Superdepa';
$sucursal['comerdepa_3']='Sucursal 3';*/
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-lg-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-lg-4">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;",'onchange'=>"actualizar('maquinas','buscarmaquinas/',''+document.frm.ReporteSucursal.value)"));
echo '</div>';
echo '</div>';

$tiporeporte['Ventas_Diarias_Cerradas']='Ventas z Cerradas';
		
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Tipo de Reportes:', array('class' => 'col-lg-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-lg-4">';
echo $this->Form->input('Reporte.tiporeporte',array('div'=>false,'options'=>$tiporeporte,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Ventas",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
echo '</div>';

$tipoventa['P']='Contado';$tipoventa['A']='A Credito';$tipoventa['E']='Devueltos';		
echo '<div class="form-group">';		
/*echo $this->Html->tag('label', 'Tipo de Ventas:', array('class' => 'col-lg-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-lg-2">';
echo $this->Form->input('Reporte.tipoventa',array('multiple' => true,'div'=>false,'options'=>$tipoventa,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Ventas",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';
* */
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-1,date('Y')));
echo $this->Html->tag('label', 'Rango de Fecha:', array('class' => 'col-lg-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="input-daterange input-group col-lg-3" id="datepicker">';

echo $this->Form->input('Reporte.desde',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true));
echo '<span class="input-group-addon">a</span>';
echo $this->Form->input('Reporte.hasta',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Hasta",'value'=>$fecha2,'readonly'=>true)); 
echo '</div>';		
echo '</div>';

echo '<div class="form-group">';
echo $this->Html->tag('label', 'Maquina:', array('class' => 'col-lg-2 control-label','for'=>"cliente_id"));
echo '<div class="col-lg-6" id="maquinas">';
echo $this->Form->input('Reporte.maquina_id',array('multiple' => true,'div'=>false,'options'=>$maquinas,'label'=>false,'data-placeholder'=>"Seleccione Maquina",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';		
echo '</div>';

/*
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Clientes:', array('class' => 'col-lg-2 control-label','for'=>"cliente_id"));
echo '<div class="col-lg-6">';
echo $this->Form->input('Reporte.cliente_id',array('multiple' => true,'div'=>false,'options'=>$cliente,'label'=>false,'data-placeholder'=>"Seleccione Clientes",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';		
echo '</div>';

echo '<div class="form-group">';
echo $this->Html->tag('label', 'Vendedores:', array('class' => 'col-lg-2 control-label','for'=>"cliente_id"));
echo '<div class="col-lg-6">';
echo $this->Form->input('Reporte.vendedore_id',array('multiple' => true,'div'=>false,'options'=>$vendedores,'label'=>false,'data-placeholder'=>"Seleccione los Vendedores",'class'=>"form-control select2","style"=>"width: 100%;"));
echo '</div>';		
echo '</div>';
*/
	?>
<button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarventas/','','tiporeporte='+document.frm.ReporteTiporeporte.value+'&fechadesde='+document.frm.ReporteDesde.value+'&fechahasta='+document.frm.ReporteHasta.value+'&sucursal='+document.frm.ReporteSucursal.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
<!-- <button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	
-->
<?php //echo $this->Form->end(); ?>
<center>
<!-- <form action="" method="post" name="otro">  -->
<div id="buscar">

</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php //echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/funciones/add", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</center>
</form>
</fieldset>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
</script>
