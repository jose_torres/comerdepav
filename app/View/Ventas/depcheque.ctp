<?php
// print_r($depositos);
?>
<table class="table table-bordered table-hover">
		<tr>
		  <th>ID</th>
		  <th>Nro</th>
		  <th>Deposito</th>
		  <th>Banco</th>
		  <th>Cuenta</th>
		  <th>Monto</th>
		  <th>Accion</th>
		</tr>
		<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($depositos['Cheque'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
     $x=$x+1; ?>
     <tr id='td_id'>
        <td align="center"><?php echo $row['Movbancario']['id']; ?></td>
        <td align="center"><?php echo $row['Movbancario']['nrodocumento']; ?></td>
        <td align="center"><?php echo $row['Documentotipo']['descorta']; ?></td>
        <td align="center"><?php echo $row['Banco']['nombre']; ?></td>
        <td align="center"><?php echo $row['Cuentasbancaria']['numerocuenta']; ?></td>
        <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'); ?></td>
        <td align="center">
            <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="actualizar('cheques_dep','eliminardeposito/',''+document.frm.ReporteSucursal.value,'&opcion=Cheque&nrodocumento='+document.frm.ChequeNrodocumento.value+'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value+'&movbancario_id=<?php echo $row['Movbancario']['id'];?>');actualizar('cheques_x_dep','buscarchequesxdep/',''+document.frm.ReporteSucursal.value,'&sucursal='+document.frm.ReporteSucursal.value+'&cuadre_id='+document.frm.CuadreId.value+'&cuadre_fecha='+document.frm.CuadreFecha.value);">Eliminar Registro &nbsp;<span class="glyphicon glyphicon-remove-circle"></span></button>
        </td>
     <?php } ?>
		</tbody>
		</table>
<script>
document.getElementById('tot_dep_cheque').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
document.getElementById('tot_res_cheque').innerHTML='<?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.'); ?>';
calcularDepositos();
calcularCuadre();
</script>
