<?php
    //print_r($data);
    //echo '<br>';
    //print_r($dias_trans);
    $tipos = array('NORMAL' =>'Normal','FERIADO'=>'Feriado','SABADO'=>'Sabado','DOMINGO'=>'Domingo' );
    $dias_semana = array(1=>'Lun',2=>'Mar',3=>'Mie',4=>'Jue',5=>'Vie',6=>'Sab',7=>'Dom');
?>
<form id="frm1" name="frm1" action="/comerdepav2/dias/guardarmarcados" class="form-horizontal"  method="post">
<div class="container-fluid">
  <div class="row " style="background-color:#0099BF;color:#FFF">
	<div class="col-xs-3 col-md-3"><strong>D&iacute;as Efectivos:&nbsp;</strong><?php echo $nro_dias_mes[1]+$nro_dias_mes[2]+$nro_dias_mes[3]+$nro_dias_mes[4]+$nro_dias_mes[5]+$nro_dias_mes[6]+$nro_dias_mes[7]; ?></div>
	<div class="col-xs-3 col-md-3"><strong>Total D&iacute;as Efectivos:&nbsp;</strong><?php
	 $dias_efectivo = $nro_dias_mes[1]+$nro_dias_mes[2]+$nro_dias_mes[3]+$nro_dias_mes[4]+$nro_dias_mes[5] - $fecha['feriados'];
	 echo $dias_efectivo; ?></div>
	<div class="col-xs-4 col-md-4"><strong>D&iacute;as Feriados y Fin de Semana:&nbsp;</strong><?php echo $nro_dias_mes[6]+$nro_dias_mes[7]+$fecha['feriados']; ?></div>
	<div class="col-xs-2 col-md-2"><strong>Domingos:&nbsp;</strong><?php echo $nro_dias_mes[7]; ?></div>
  </div>
</div>


<table class="table table-condensed table-bordered table-hover">
    <tr>
      <th>Fecha</th>
      <th>Tipo <input type="submit" class="btn btn-info" value="Guardar"></th>
    </tr>
    <tbody>
<?php
    $total_monto['detal'] = $total_monto['suc1'] = $total_monto['suc2'] = $total_monto['credito'] = $total_monto['total'] = 0;
   for ($i = 1; $i <= $fecha['dia_fin']; $i++) {
    $fecha_actual = $datos['anio'].'-'.$datos['mes'].'-'.str_pad($i, 2, "0", STR_PAD_LEFT);
    $row[0]['fecha'] = $fecha_actual;
    $class_tr = "";
    if (date('N', strtotime($fecha_actual))>=6){	
           $class_tr = "class='success'";
    }
?>
    <tr id='td_id' <?php echo $class_tr;?> >
        <td align="center"><strong><?php echo $dias_semana[date('N', strtotime($row[0]['fecha']))].':&nbsp;'.date('d-m-Y',strtotime($row[0]['fecha'])); ?></strong></td>   
        <td align="right">
        <?php 
        $elegido ='NORMAL';$id=0;
        if (date('N', strtotime($fecha_actual))==6){
            $elegido ='SABADO';
        }elseif (date('N', strtotime($fecha_actual))==7){
            $elegido ='DOMINGO';	
        }elseif(isset($data[$fecha_actual])){
            $elegido = $data[$fecha_actual]['tipo'];
        }
        if(isset($data[$fecha_actual])){
            $id = $data[$fecha_actual]['id'];
        }
        echo $this->Form->input('Seleccionj.'.$i.'.tipo',array('div'=>false,'options'=>$tipos,'selected'=>$elegido,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 50%;"));
	echo $this->Form->input('Seleccionj.'.$i.'.fecha', array('type' =>'hidden','label' =>'','value'=>$fecha_actual));
        echo $this->Form->input('Seleccionj.'.$i.'.id', array('type' =>'hidden','label' =>'','value'=>$id));
	?>
        </td>
        </tr>
<?php
	}
?>
    </tbody>
    <tfoot>
      <tr>
            <th align="right">Total de Dias del Mes:</th>
            <th  ><div align="right"><?php echo number_format(sprintf("%01.2f",  $fecha['dia_fin'] ), 2, ',', '.');
             ?></div></th>
    </tfoot>
</table>
<?php
    echo $this->Form->end('Guardar');
?>