<?php
//echo $this->element('menuinterno',$datos_menu);
//echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<div class="unidades form">
<?php echo $this->Form->create('Perfile', array('url' => 'edit','name' => 'frm','id'=>'frm','role'=>"form",'class'=>'form-horizontal')); ?>	
<fieldset id="personal" >
<legend  class="info">EDITAR UN PERFIL</legend>
<?php
	echo $this->Form->input('Perfile.id', array('type'=>'hidden'));
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Nombre:', array('class' => 'col-xs-1 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-4">';
	echo $this->Form->input('Perfile.descripcion',array('label'=>false,'size' =>'30','class'=>"form-control",'type'=>'text','div'=>false));
	echo '</div>';
	echo '</div>';

	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Panel:', array('class' => 'col-xs-1 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-4">';
	echo $this->Form->input('Perfile.redireccion',array('label'=>false,'type'=>'text','class'=>"form-control",'div'=>false));
	echo '</div>';
	echo '</div>';
	//echo '<div class="pure-control-group">';
	//echo $this->Form->input('Perfile.familia', array('size'=>'40','label'=>'Familia','options'=>$perfiles,'type'=>'select','empty' =>'(Es Perfil Padre)','size'=>'1',"onchange"=>"modificar('hijos','buscarhijos/',this.form.PerfileFamilia.value);")); 
	echo $this->Form->input('Perfile.familia', array('size'=>'40','type'=>'hidden')); 
	//echo '</div>';
	$var2[0]='No tiene';
	//echo '<div id="hijos" class="pure-control-group">';
	//echo $this->Form->input('Perfile.id_padre', array('size'=>'40','label'=>'Hijos:','options'=>$var2,'type'=>'select','size'=>'1')); 
	echo $this->Form->input('Perfile.id_padre', array('size'=>'40','type'=>'hidden')); 
	//ho '</div>';
	echo $this->Form->end();
?>
</fieldset>
</div>
<form>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">	
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Guardar Registro"));
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/perfiles", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</form>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
?>
