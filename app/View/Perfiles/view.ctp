<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<div class="unidades view">
<fieldset id="personal" >
<legend class="info">MOSTRAR UN PERFIL</legend>

<table>
	<tr>
		<td><label>Nombre:</label></td>
		<td><?php echo $data['Perfile']['descripcion']; ?> </td>
	</tr>
	<tr>
		<td><label>Panel:</label></td>
		<td><?php echo $data['Perfile']['redireccion']; ?> </td>
	</tr>
	<tr>
		<td><label>Grupo:</label></td>
		<td><?php echo $data['Perfile']['id_padre']; ?> </td>
	</tr>
	<tr>
		<td><label>Creado:</label></td>
		<td><?php echo $data['Perfile']['created']; ?> </td>
	</tr>
</table>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/perfiles", array('escape'=>false), null);
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</div>
</fieldset>

</fieldset>
