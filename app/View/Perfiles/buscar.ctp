<?php if (count($data)>0){?>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="pure-table pure-table-horizontal">
<thead>
    <tr>
        <th >Codigo</th>
        <th>Descripcion</th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $perfile = $row['Perfile']; ?>    
    <?php $x++; ?>
	<?php if($x % 2 == 0){ ?>
	<tr class="roweven">
	<?php } else { ?>
	<tr>
	<?php } ?>
        <td><?php echo $perfile['id'] ?></td>
        <td><?php echo $this->Html->link($perfile['descripcion'], '/perfiles/view/'.$perfile['id']) ?></td>      
        <td><?php echo $perfile['created'] ?></td>
        <td><?php 
                if (!empty($perfile['modified'])) echo $perfile['modified'];
                else if (!empty($perfile['updated'])) echo $perfile['updated'];
            ?></td>
        <td align="center">
	  <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/perfiles/view/".$perfile['id'], array('escape'=>false), null );
		echo '&nbsp;|&nbsp;';
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/perfiles/edit/".$perfile['id'], array('escape'=>false), null);
		echo '&nbsp;|&nbsp;'; 	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/perfiles/delete/".$perfile['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$perfile['descripcion'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
<?php }else{
	echo 'No hay Registro';
} ?>
