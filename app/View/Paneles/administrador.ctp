<?php
//	print_r($NroUsuario);
echo $this->Html->css('morris/morris.css');
?>
     <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $ultimaNroPago;?></h3>

              <p>Pagos Recibidos de fecha&nbsp;<?php echo date("d-m-Y",strtotime($ultimaFechaPago));?></p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="../reportes/clientes" class="small-box-footer">Ver m&aacute;s <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $NroCuadreConMovPend;?></h3>

              <p>Cuadres con Movimientos Pendientes</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="../concuadrediarios/index" class="small-box-footer">Ver m&aacute;s <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo $NroUsuario; ?></h3>

              <p>Usuarios Registrados</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="../usuarios" class="small-box-footer">Ver M&aacute;s <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $NroRetSinAsociar; ?></h3>

              <p>Retenciones de Ventas por Asociar</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="../conretencionventadets/declarar" class="small-box-footer">Ver m&aacute;s <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-7 connectedSortable">
		 <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#revenue-chart" data-toggle="tab">Bs.</a></li>
              
              <li class="pull-left header"><i class="fa fa-inbox"></i> Ventas &Uacute;ltimos 30 D&iacute;as</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
            
            </div>
          </div>
	 </section>
     </div>
 <br>    
 <br>    
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/morris/raphael-min.js');
	echo $this->Html->script('plugins/morris/morris.min.js');
?>
<script>
  	
  $(function () {

	  
    //Initialize Select2 Elements
	 var area = new Morris.Area({
    element: 'revenue-chart',
    resize: true,
    data: [
 <?php
	foreach ($registrosVentas as $row){
		echo "{y:'".$row[0]['fecha']."',item1:".$row[0]['princ'].",item2:".$row[0]['detal_1'].",item3:".$row[0]['detal_2'].",item4:".$row[0]['detal_3']."},";
	}
 ?>
    ],
    xkey: 'y',
        xLabels:'day',
    ykeys: ['item1', 'item2', 'item3', 'item4'],
    labels: ['Principal', 'Detal', 'Suc 3', 'Suc 4'],
    lineColors: ['#a0d0e0', '#3c8dbc','#00A65A', '#F39C12'],
    hideHover: 'auto'
  });
  
	 var area2 = new Morris.Area({
    element: 'sales-chart',
    resize: true,
    data: [
      {y: '2017-08-01', item1: 2666, item2: 2666},
      {y: '2017-08-02', item1: 2778, item2: 2294},
      {y: '2017-08-03', item1: 4912, item2: 1969},
      {y: '2017-08-04', item1: 3767, item2: 3597},
      {y: '2017-08-07', item1: 6810, item2: 1914},
      {y: '2017-08-08', item1: 5670, item2: 4293},
      {y: '2017-08-09', item1: 4820, item2: 3795},
      {y: '2017-08-10', item1: 15073, item2: 5967},
      {y: '2017-08-11', item1: 10687, item2: 4460},
      {y: '2017-08-12', item1: 8432, item2: 5713}
    ],
    xkey: 'y',

    ykeys: ['item1', 'item2'],
    labels: ['Suc. 1', 'Suc 2'],
    lineColors: ['#a0d0e0', '#3c8dbc'],
    hideHover: 'auto'
  });

  var line = new Morris.Line({
    element: 'sales-chart',
    resize: true,
    data: [
      {y: '2011 Q1', item1: 2666},
      {y: '2011 Q2', item1: 2778},
      {y: '2011 Q3', item1: 4912},
      {y: '2011 Q4', item1: 3767},
      {y: '2012 Q1', item1: 6810},
      {y: '2012 Q2', item1: 5670},
      {y: '2012 Q3', item1: 4820},
      {y: '2012 Q4', item1: 15073},
      {y: '2013 Q1', item1: 10687},
      {y: '2013 Q2', item1: 8432}
    ],
    xkey: 'y',
    ykeys: ['item1'],
    labels: ['Item 1'],
    lineColors: ['#efefef'],
    lineWidth: 2,
    hideHover: 'auto',
    gridTextColor: "#fff",
    gridStrokeWidth: 0.4,
    pointSize: 4,
    pointStrokeColors: ["#efefef"],
    gridLineColor: "#efefef",
    gridTextFamily: "Open Sans",
    gridTextSize: 10
  });

  //Donut Chart
 

  //Fix for charts under tabs
  $('.box ul.nav a').on('shown.bs.tab', function () {
    area.redraw();
    //area2.redraw();
   // donut.redraw();
    //line.redraw();
  });
   

  });
</script>
