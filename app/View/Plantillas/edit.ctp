<?php
//echo $this->element('menuinterno',$datos_menu);
echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id,parametros,icono){
	 // alert ('El registro ha sido Modificado');	
	 $(''+icono).update('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');	
		var ajx = new Ajax.Updater(""+nombrediv,""+url+id,{asynchronous:true, evalScripts:true, parameters:"?"+parametros,
		 onSuccess: function(){
         $(''+icono).update("");
		}
		 });	
	}
</script>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>		

<div class="unidades form">
<?php echo $this->Form->create('Plantilla', array('url' => 'edit','name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned')); ?>		
<fieldset id="personal" >
<legend><h2>EDITAR UNA PLANTILLA</h2></legend>
<?php
	echo  $this->Form->input('Plantilla.id', array('type'=>'hidden'));
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.nombre',array('label'=>'Nombre:','size' =>'30','type'=>'text','div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.descripcion',array('label'=>'Descripci&oacute;n:','rows'=>"3", 'cols'=>"30",'type'=>'textarea','div'=>false));
	echo '</div>';
	$var2['V']='Vertical'; $var2['H']='Horizontal';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.orientacion',array('label'=>'Orientaci&oacute;n:','size' =>'1','type'=>'select','div'=>false,'options'=>$var2));
	//echo '</div>';
	$var3['M']='Mayusculas'; $var3['L']='Minusculas'; $var3['O']='Oracion';
	//echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.titulo',array('label'=>'Visualizaci&oacute;n de T&iacute;tulos:','size' =>'1','type'=>'select','div'=>false,'options'=>$var3));
	//echo '</div>';
	$var4['N']='Numeros'; $var4['L']='Letras'; $var4['S']='Sin Nada';
	//echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.numeracion',array('label'=>'Numeraci&oacute;n:','size' =>'1','type'=>'select','div'=>false,'options'=>$var4));
	echo '</div>';
	
?>
</fieldset>
<?php echo $this->Form->end();?>
<?php 
	$var2['Usuario']='Usuario';$var2['Oficina']='Oficina';
	//$var=array();$var['Ver']='Ver';
	$var['Editar']='Editar';
	echo '<form class="pure-form pure-form-aligned" id="cabecera" name="cabecera">';
	echo '<fieldset>';
	echo '<legend class="info"><strong>1.- Encabezado:</strong></legend>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Cabecera.elementos', array('label'=>'Elementos de Cabeceras:','options'=>$cabecera,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','escape'=>false,'div'=>false));echo '&nbsp;&nbsp;&nbsp;';	
	echo $this->Html->image('img_acciones/BGuardar_Detalle.png',array("onclick"=>'modificar("guardardetallec","../guardardetalle/","Encabezado","elemento_id="+document.cabecera.CabeceraElementos.value+"&valorControl='.$this->data['Plantilla']['id'].'","cargando");',"align"=>"Absmiddle", "title"=>"Guardar Detalle",'div'=>false));
	echo '</div>';
	echo '</fieldset>';	
	echo '</form>';
?>
<div id="cargando"></div>	
<div id="guardardetallec">
<script language="javascript" type="text/javascript">
modificar("guardardetallec","../guardardetalle/","Encabezado","elemento_id="+document.cabecera.CabeceraElementos.value+"&valorControl=<?php echo $this->data['Plantilla']['id'];?>","cargando");
</script>
</div>
<?php 
	$var2['Usuario']='Usuario';$var2['Oficina']='Oficina';
	//$var=array();$var['Ver']='Ver';
	$var['Editar']='Editar';
	echo '<form class="pure-form pure-form-aligned" id="seccione" name="seccione">';
	echo '<fieldset>';
	echo '<legend class="info"><strong>2.- Secci&oacute;n:</strong></legend>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Cabecera.elementos', array('label'=>'Secciones:','options'=>$seccion,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','escape'=>false,'div'=>false));echo '&nbsp;&nbsp;&nbsp;';	
	echo $this->Html->image('img_acciones/BGuardar_Detalle.png',array("onclick"=>'modificar("guardardetalles","../guardardetalle/","Seccion","elemento_id="+document.seccione.CabeceraElementos.value+"&valorControl='.$this->data['Plantilla']['id'].'","cargandos");',"align"=>"Absmiddle", "title"=>"Guardar Detalle",'div'=>false));
	echo '</div>';
	echo '</fieldset>';	
	echo '</form>';
?>
<div id="cargandos"></div>	
<div id="guardardetalles">
<script language="javascript" type="text/javascript">
modificar("guardardetalles","../guardardetalle/","Seccion","elemento_id="+document.seccione.CabeceraElementos.value+"&valorControl=<?php echo $this->data['Plantilla']['id'];?>","cargandos");
</script>
</div>
<?php 
	$var2['Usuario']='Usuario';$var2['Oficina']='Oficina';
	//$var=array();$var['Ver']='Ver';
	$var['Editar']='Editar';
	echo '<form class="pure-form pure-form-aligned" id="pie" name="pie">';
	echo '<fieldset>';
	echo '<legend class="info"><strong>3.- Pie de P&aacute;gina:</strong></legend>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Cabecera.elementos', array('label'=>'Elementos de Pie de P&aacute;gina:','options'=>$pie,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','escape'=>false,'div'=>false));echo '&nbsp;&nbsp;&nbsp;';	
	echo $this->Html->image('img_acciones/BGuardar_Detalle.png',array("onclick"=>'modificar("guardardetallep","../guardardetalle/","Pie_Pagina","elemento_id="+document.pie.CabeceraElementos.value+"&valorControl='.$this->data['Plantilla']['id'].'","cargandop");',"align"=>"Absmiddle", "title"=>"Guardar Detalle",'div'=>false));
	echo '</div>';
	echo '</fieldset>';	
	echo '</form>';
?>
<div id="cargandop"></div>	
<div id="guardardetallep">
<script language="javascript" type="text/javascript">
modificar("guardardetallep","../guardardetalle/","Pie_Pagina","elemento_id="+document.pie.CabeceraElementos.value+"&valorControl=<?php echo $this->data['Plantilla']['id'];?>","cargandop");
</script>
</div>	
</div>
<form>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Imprimir Guardar Registro"));echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/plantillas", array('escape'=>false), null);echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
</form>
