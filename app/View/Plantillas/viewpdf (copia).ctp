<?php
$this->tcpdf->core->SetPageOrientation("P");

$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Leiban Rivero');
$this->tcpdf->core->SetMargins(15, 30, 15);
$this->tcpdf->core->SetHeaderMargin(PDF_MARGIN_HEADER);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);

$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, $data['Plantilla']['nombre'], 'SISTEMA DE GESTIÓN ');

//Desactivamos corte automatico de las paginas
//$this->tcpdf->core->SetAutoPageBreak( true );
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );


$this->tcpdf->core->setHeaderFont(array($textfont,'',12));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

//Desactivamos el Header y Footer Original del TCPDF
//$this->tcpdf->core->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");
/*$pago=sprintf("%01.2f", $sueldo);
$numerosaletras->numerosALetras($pago);
$data_persona['Persona']['sueldoletras']=$numerosaletras->resultado;
$data_persona['Persona']['sueldoletras']=$numerosaletras->mayusculas_oraciones(strtolower($data_persona['Persona']['sueldoletras']));		
$pago=number_format($pago, 2, ',', '.');	

$max = $cantidadcopias;
for($i=1;$i<=$max;$i++){
$this->tcpdf->Header();
$this->tcpdf->constancia($data_persona,$data_cargo,$data_ubicacion,$pago,$calculos,$text,$verificado);
$this->tcpdf->Footer();
if ($i<$max){
$this->tcpdf->core->SetMargins(6, 12, 6);
$this->tcpdf->core->AddPage();
}
}*/
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->SetTituloCodigo('CODIGO:XX-XX-001:','B',12);
$this->tcpdf->SetTituloPlantilla($data['Plantilla']['nombre'].':','B');
$this->tcpdf->SetTituloContenido('Titulo del Contenido. Un Nombre Largo Para Probar. Espero se vea Bien.');
$texto_dividido = str_split($texto, 1500);
//print_r($texto_dividido);
//foreach ($texto_dividido as $registro) {
//$this->tcpdf->Header();
//----------------------------------------------------------------------
//-----Def. de Datos Contenidos------------------------------------------
$this->tcpdf->texto($texto);
//$this->tcpdf->texto($registro);

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//$this->tcpdf->Footer();
//----------------------------------------------------------------------
//$this->tcpdf->core->AddPage();
//$this->tcpdf->core->SetMargins(6, 12, 6);
//}	
$this->tcpdf->tcpdfOutput('plantilla_Nro_'.$data['Plantilla']['id'].'.pdf', 'D');
?>
