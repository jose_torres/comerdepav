<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>		

<div class="unidades form">
<?php echo $this->Form->create('Plantilla', array('url' => 'add','name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned')); ?>		
<fieldset id="personal" >
<legend><h2>REGISTRAR UNA PLANTILLA</h2></legend>
<?php
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.nombre',array('label'=>'Nombre:','size' =>'30','type'=>'text','div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.descripcion',array('label'=>'Descripci&oacute;n:','size' =>'30','type'=>'text','div'=>false));
	echo '</div>';
	$var2['V']='Vertical'; $var2['H']='Horizontal';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.orientacion',array('label'=>'Orientaci&oacute;n:','size' =>'1','type'=>'select','div'=>false,'options'=>$var2));
	echo '</div>';
	$var3['M']='Mayusculas'; $var3['L']='Minusculas'; $var3['O']='Oracion';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.titulo',array('label'=>'Visualizaci&oacute;n de T&iacute;tulos:','size' =>'1','type'=>'select','div'=>false,'options'=>$var3));
	echo '</div>';
	$var4['N']='Numeros'; $var4['L']='Letras'; $var4['S']='Sin Nada';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Plantilla.numeracion',array('label'=>'numeraci&oacute;n:','size' =>'1','type'=>'select','div'=>false,'options'=>$var4));
	echo '</div>';
?>
</fieldset>
</form>
</div>
<form>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"document.frm.submit()", "title"=>"Imprimir Guardar Registro"));echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/plantillas", array('escape'=>false), null);echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
</form>
