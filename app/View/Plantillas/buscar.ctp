<?php  if (count($data)>0){?>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="pure-table pure-table-horizontal">
<thead>
    <tr>
        <th >Codigo</th>
        <th>Nombre</th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>
    <?php $x=0; foreach ($data as $row): $grupo = $row['Plantilla']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $grupo['id'] ?>    
        </td>
        <td>
            <?php echo $this->Html->link($grupo['nombre'], '/plantillas/view/'.$grupo['id']) ?>    
        </td>      
        <td>
            <?php echo date("d-m-Y H:i:s",strtotime($grupo['created'])); ?>
        </td>
        <td>
            <?php 
                if (!empty($grupo['modified'])) echo date("d-m-Y H:i:s",strtotime($grupo['modified']));
                else if (!empty($grupo['updated'])) echo date("d-m-Y H:i:s",strtotime($grupo['updated']));
            ?>
        </td>
        <td align="center">
	   <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/plantillas/view/".$grupo['id'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/plantillas/edit/".$grupo['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/plantillas/delete/".$grupo['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$grupo['nombre'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php }else{
		echo 'Ningun registros encontrados';
	}

?>
