<?php
//echo $this->element('menuinterno',$datos_menu);
echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<fieldset id="personal" >
<legend><h2>LISTA DE SECCIONES REGISTRADOS</h2></legend>
<center>
<form action="" method="post" name="two" 'class'="pure-form pure-form-aligned">
<?php
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Tipohectarea.buscar_tipo',array('label'=>'Buscar Por el Nombre:','size' =>'40','type'=>'text','div'=>false));
	echo $this->Html->image('img_acciones/system-search.png',array("onclick"=>"modificar('buscar','secciones/buscar/',document.two.TipohectareaBuscarTipo.value);", "title"=>"Buscar Registro",'div'=>false,"align"=>"Absmiddle"));
	echo '</div>';
?>
</form>
<form action="" method="post" name="otro">
<div id="buscar">
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="pure-table pure-table-horizontal">
<thead>
    <tr>
        <th ><div class="blanco"><?php echo $this->Paginator->sort('id','Codigo')?></div></th>
        <th><div class="blanco"><?php echo $this->Paginator->sort('nombre','Nombre')?></div></th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>
<tbody>
    <?php $x=0; foreach ($data as $row): $grupo = $row['Seccione']; ?>    
    <?php $x++; ?>
	<?php if($x % 2 == 0){ ?>
	<tr class="roweven">
	<?php } else { ?>
	<tr>
	<?php } ?>
        <td>
            <?php echo $grupo['id'] ?>    
        </td>
        <td>
            <?php echo $this->Html->link($grupo['nombre'], '/secciones/view/'.$grupo['id']) ?>    
        </td>      
        <td>
            <?php echo date("d-m-Y H:i:s",strtotime($grupo['created'])); ?>
        </td>
        <td>
            <?php 
                if (!empty($grupo['modified'])) echo date("d-m-Y H:i:s",strtotime($grupo['modified']));
                else if (!empty($grupo['updated'])) echo date("d-m-Y H:i:s",strtotime($grupo['updated']));
            ?>
        </td>
        <td align="center">
	  <?php 
		echo $this->Html->link( $this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/secciones/view/".$grupo['id'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/secciones/edit/".$grupo['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $this->Html->link( $this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/secciones/delete/".$grupo['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$grupo['nombre'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
    </tbody>
</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, comenzando con el registro {:start}, terminando en {:end}')
	));
	?>	</p>
	<div class="paging">
		<?php echo $this->Paginator->prev('' . $this->Html->image("img_acciones/goprevious.png", array("alt" => "Ver Pagina Anterior", "title"=>"Ver Pagina Anterior")), array('escape'=>false), null, array('class'=>'disabled','escape'=>false));?>
	  	<?php
			echo $this->Paginator->counter(array('format' => __('P&aacute;gina: %page%', true)));
	?> 
		<?php echo $this->Paginator->next($this->Html->image("img_acciones/gonext.png", array("alt" => "Ver Proxima Pagina", "title"=>"Ver Proxima Pagina ")) . '', array('escape'=>false), null, array('class' => 'disabled','escape'=>false));?><br>
		<?php echo 'P&aacute;ginas: |'.$this->Paginator->numbers();
		?>
	</div>
</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend class="accion"><strong>Acciones</strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/secciones/add", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
</fieldset>
</center>
</form>
</fieldset>
</fieldset>
