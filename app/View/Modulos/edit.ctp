<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	function validar(){
		if (document.getElementById("ModuloNombre").value=="")
		{
			alert ('Por favor escriba un Nombre de Modulo');
			return false;
		}else{
			document.frm.submit();	
		}	
	}		
</script>
<div class="unidades form">
<?php echo $this->Form->create('Modulo',array('url' => 'edit','name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned'));?>
	<fieldset>
 		<legend class="info"><h2><strong><?php __('EDITAR UN MODULO'); ?></strong></h2></legend>
<?php
	echo $this->Form->input('id');
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Modulo.nombre',array('label'=>'Nombre:','size' =>'30','type'=>'text','div'=>false));
	echo '</div>';		
	if ($handle = opendir('../../app/webroot/img/img_acciones')) {
		$i=0;$primero='';
		while (false !== ($file = readdir($handle)))
		{
			if ( preg_match( "/[.]png$/", $file ) ) {
				preg_match( "/_(\d+)_(\d+)[.]/", $file, $found );
				if ($i==0){ $primero=$file; }
				$ban[$file]=$file;
				$i=$i+1;
			}
		}
		closedir($handle);
		}

	$IMAGE_ROOT=FULL_BASE_URL.'/'.$nombreProyecto.'/img/';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Modulo.imagen', array('size'=>'40','div'=>false,'label'=>'Imagen:','options'=>$ban,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','escape'=>false,'onchange'=>"document.getElementById('im').src ='".$IMAGE_ROOT."img_acciones/'+ document.frm.ModuloImagen.value")) ;
	echo $this->Html->image('img_acciones/'.$this->data['Modulo']['imagen'],array('align'=>"absmiddle",'id'=>"im"));
	echo '</div>';
?>
	</fieldset>
<?php echo $this->Form->end();?>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">	
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"validar()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/modulos", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
