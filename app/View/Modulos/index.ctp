<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
?>
<fieldset id="personal" >
<legend> <h2>LISTA DE MODULOS REGISTRADOS </h2></legend>
<center>
<form action="" method="post" name="two">
<div class="unidades index">
	<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover">
	<thead>	
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('Nombre');?></th>
			<th><?php echo $this->Paginator->sort('Imagen');?></th>
			<th><?php echo $this->Paginator->sort('Modificado');?></th>
			<th class="actions"><?php __('Acciones');?></th>
	</tr>
	</thead>
	<?php
	$i = 0;
	foreach ($unidades as $unidade):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $i; ?>&nbsp;</td>
		<td><?php echo $unidade['Modulo']['nombre']; ?>&nbsp;</td>
		<td><?php echo $this->Html->image("img_acciones/".$unidade['Modulo']['imagen'], array("alt" => "Ver Registro", "title"=>"Ver Registro")); ?>&nbsp;</td>
		<td><?php echo $unidade['Modulo']['modified']; ?>&nbsp;</td>
		<td class="actions" align="center">
			<?php echo $this->Html->link($this->Html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")), array('action' => 'view', $unidade['Modulo']['id']), array('escape'=>false)); ?>&nbsp;&nbsp;
			<?php echo $this->Html->link($this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")), array('action' => 'edit', $unidade['Modulo']['id']), array('escape'=>false)); ?>&nbsp;&nbsp;
			<?php echo $this->Html->link($this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Eliminar Registro", "title"=>"Eliminar Registro")), array('action' => 'delete', $unidade['Modulo']['id']),  array('escape'=>false), sprintf(__('Esta seguro de eliminar el registro # %s?', true), $unidade['Modulo']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, comenzando con el registro {:start}, terminando en {:end}')
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('' . $this->Html->image("img_acciones/goprevious.png", array("alt" => "Ver Pagina Anterior", "title"=>"Ver Pagina Anterior")), array('escape'=>false), null, array('class'=>'disabled','escape'=>false));?>
	  	<?php
			echo $this->Paginator->counter(array('format' => __('P&aacute;gina: %page%', true)));
	?> 
		<?php echo $this->Paginator->next($this->Html->image("img_acciones/gonext.png", array("alt" => "Ver Proxima Pagina", "title"=>"Ver Proxima Pagina ")) . '', array('escape'=>false), null, array('class' => 'disabled','escape'=>false));?><br>
		<?php echo 'P&aacute;ginas: |'.$this->Paginator->numbers();
		?>
	</div>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/modulos/add", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
</fieldset>
</div>
</form>
</center>
</fieldset>
