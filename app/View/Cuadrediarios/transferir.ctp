<?php
//Funciones js propias del archivo.ctp
echo $this->Html->script(array('funciones.js','View/Cuadrediarios/transferir'));
?>

<fieldset id="personal" >
<legend> Cuadres Realizados</legend>
<fieldset id="personal" >
<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'viewpdfventas/1','id'=>'frm','name'=>'frm'));
?>	
<?php
echo '<div class="form-group">';		
echo $this->Html->tag('label', 'Sucursal:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="col-xs-4">';
echo $this->Form->input('Reporte.sucursal',array('div'=>false,'options'=>$sucursal,'label'=>false,'data-placeholder'=>"Seleccione sucursal",'class'=>"form-control select2","style"=>"width: 100%;"));

echo '</div>';
echo '</div>';

echo '<div class="form-group">';		
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
echo $this->Html->tag('label', 'Rango de Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="input-daterange input-group col-xs-3" id="datepicker">';

echo $this->Form->input('Reporte.desde',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true));
echo '<span class="input-group-addon">a</span>';
echo $this->Form->input('Reporte.hasta',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Hasta",'value'=>$fecha2,'readonly'=>true)); 
echo '</div>';		
echo '</div>';

	?>
<!-- <button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarretenciones','','fechadesde='+document.frm.ReporteDesde.value+'&fechahasta='+document.frm.ReporteHasta.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
 <button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	-->
<?php echo $this->Form->end(); ?>	
<center>
<div id="buscar">
<table border="0" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-condensed table-bordered table-hover">
<thead>
    <tr>
        <th ><?php echo $this->Paginator->sort('id','Codigo');?></th>
        <th><?php echo $this->Paginator->sort('fecha','Fecha');?></th>
        <th>Estatus</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $registro = $row['Cuadrediario']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $x; ?>    
        </td>
        <td>
            <?php echo $this->Html->link(date('d-m-Y',strtotime($registro['fecha'])), '/cuadrediarios/view/'.$registro['id']) ?>    
        </td>      
        <td>
            <?php if ($registro['estatus']=='1'){ echo 'Abierto'; }else{echo 'Cerrado';} ?>    
        </td>
        <td>
            <?php 
                if (!empty($registro['modified'])) echo date('d-m-Y',strtotime($registro['modified']));
                else if (!empty($registro['updated'])) echo date('d-m-Y',strtotime($registro['updated']));
            ?>
        </td>
        <td align="center">
			<?php 
			echo '&nbsp;&nbsp;&nbsp;';
			echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => 'viewpdf', $registro['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
			echo '&nbsp;&nbsp;&nbsp;';
			 ?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, comenzando con el registro {:start}, terminando en {:end}')
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('' . $this->Html->image("img_acciones/goprevious.png", array("alt" => "Ver Pagina Anterior", "title"=>"Ver Pagina Anterior")), array('escape'=>false), null, array('class'=>'disabled','escape'=>false));?>
	  	<?php
			echo $this->Paginator->counter(array('format' => __('P&aacute;gina: %page%', true)));
	?> 
		<?php echo $this->Paginator->next($this->Html->image("img_acciones/gonext.png", array("alt" => "Ver Proxima Pagina", "title"=>"Ver Proxima Pagina ")) . '', array('escape'=>false), null, array('class' => 'disabled','escape'=>false));?><br>
		<?php echo 'P&aacute;ginas: |'.$this->Paginator->numbers();
		?>
	</div>
</div>
</center>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/ventas/cierre", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
