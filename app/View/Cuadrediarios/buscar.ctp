<?php
	echo $this->Html->css('datatables/dataTables.bootstrap.css');
?>
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Cuadre Transferidos Encontrados</h3>
	</div>
<table border="1" cellpadding="1" cellspacing="0" width="95%" align="center" class="table table-condensed table-bordered table-hover" id="ListadoCuadre">
<thead>
    <tr>
        <th>Id</th>
        <th>Fecha</th>
        <th>Ventas</th>
        <th>Dep&oacute;sitos</th>
        <th>Gastos</th>
        <th>Retenciones</th>
        <th>Estatus</th>
        <th>Modificado</th>
        <th >Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $registro = $row['Cuadrediario']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $x; ?>    
        </td>
        <td>
          <?php echo $this->Html->link(date('d-m-Y',strtotime($registro['fecha'])), '/cuadrediarios/view/'.$registro['id']); ?>   
        </td>
       <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Cuadrediario']['monto_venta']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Cuadrediario']['monto_deposito']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Cuadrediario']['monto_debito']), 2, ',', '.'); ?></td>
         <td align="right"><?php echo number_format(sprintf("%01.2f", $row['Cuadrediario']['monto_retencion']), 2, ',', '.'); ?></td>      
        <td>
            <?php if ($registro['estatus']=='1'){ echo 'Abierto'; }else{echo 'Cerrado';} ?>    
        </td>
        <td>
            <?php 
                if (!empty($registro['modified'])) echo date('d-m-Y',strtotime($registro['modified']));
                else if (!empty($registro['updated'])) echo date('d-m-Y',strtotime($registro['updated']));
            ?>
        </td>
        <td align="center">
			<?php
			
			if ($registro['estatus']!='1'){
				echo $this->Html->link(__('<span class="glyphicon glyphicon-download-alt"></span>&nbsp;Exportar a txt'), array('action' => 'exportar', $registro['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
				echo '&nbsp;&nbsp;&nbsp;';
			//	echo $this->Html->link(__('<span class="glyphicon glyphicon-edit"></span>&nbsp;Abrir'), array('action' => 'edit', $registro['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
			//	echo '&nbsp;&nbsp;&nbsp;';
			}else{
				echo $this->Html->link(__('<span class="glyphicon glyphicon-remove"></span>&nbsp;Cerrar'), array('action' => 'edit', $registro['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
				echo '&nbsp;&nbsp;&nbsp;';
			}
			echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => 'viewpdf', $registro['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
			echo '&nbsp;&nbsp;&nbsp;';
			 ?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
	$('[data-toggle="tooltip"]').tooltip();

    $('#ListadoCuadre').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
   

  });
 
</script>
