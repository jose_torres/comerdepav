  <table class="table table-bordered">
		<tr>
		   <th width="7%">Fecha</th>	
		   <th width="7%">Hora</th>	
		  <th width="5%">Nro</th>
		  <th width="7%">Factura</th>
		  <th width="25%">Cliente</th>
		  <th>Total</th>
		  <th>Iva</th>
		  <th>Base</th>
</tr>
	<?php
		$suma_factura=0;$suma_factura_credito=0;$suma_factura_contado=0; $suma_iva=0; $suma_bruto=0;$suma_retencion=0;$suma_efectivo = $suma_cheque = $suma_debito = $suma_transfer = 0;
		$BANCO='';$CONT=0;	
		foreach ($data as $row){
		$CONT=$CONT+1;			
		$monto_neto = $row['Venta']['baseimp1'] + $row['Venta']['baseimp2'] +$row['Venta']['baseimp3'];
		$monto_iva = $row['Venta']['ivaimp1'] + $row['Venta']['ivaimp2'] +$row['Venta']['ivaimp3'];
		$monto_bruto = $monto_neto + $monto_iva;
		$suma_bruto = $suma_bruto + $monto_bruto;
		$suma_factura = $suma_factura + $monto_neto;
		$suma_iva = $suma_iva + $monto_iva; 
			?>
		<tr>
		  <td><?php echo date("d-m-Y",strtotime($row['Venta']['fecha']));?></td>
		  <td><?php echo $row['Venta']['hora'];?></td>
		  <td><?php echo $row['Venta']['codventa'];?></td>
		  <td><?php echo $row['Venta']['numerofactura'];?></td>
		  <td><?php echo $row['Cliente']['rif'].'=>'.$row['Cliente']['descripcion']?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $monto_bruto), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $monto_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $monto_neto), 2, ',', '.');?></td>
		  <td><?php	
		echo $this->Html->link(  "Eliminar&nbsp;<span class='glyphicon glyphicon-remove-sign'></span>" ,"/cuadrediarios/deleteVentaEspera/".$row['Venta']['codventa'], array('escape'=>false,'class'=>"btn btn-danger"),'¿Esta seguro de eliminar la Factura '.$row['Venta']['numerofactura'].'?');
		?></td>
 </tr> 		
		<?php		
		
		}

		?>

<tfoot>
		<tr>
		  <td>&nbsp;</td>
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td>&nbsp;</td>	
		  <td align="right"><label>Totales:</label></td>	
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_bruto), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_iva), 2, ',', '.');?></td>
		  <td align="right"><?php echo number_format(sprintf("%01.2f", $suma_factura), 2, ',', '.');?></td>
		  <td>&nbsp;</td>			  
		   		</tfoot>
 </table>		  		  
