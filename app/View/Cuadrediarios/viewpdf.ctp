<?php
//print_r($empresa);
$this->fpdf->setup('P');

// Construccion de la Cabecera
	$this->fpdf->SetTitulo($encabezado['titulo_reporte']);
	$this->fpdf->SetLogoIzq($empresa['Empresa']['logo_izquierdo']);
	$this->fpdf->SetLogoDer($empresa['Empresa']['logo_izquierdo']);
	$this->fpdf->SetLine1($empresa['Empresa']['nombre']);
	$this->fpdf->SetLine2('');
	$this->fpdf->SetLine3('');
	$this->fpdf->SetLine4('');
	
	$this->fpdf->obtener_pie($slogan="  ");
// obtiene el nro de paginas    
    $this->fpdf->fpdf->AliasNbPages(); 
    $this->fpdf->fpdf->AddPage();
    $this->fpdf->fpdf->SetFont('Arial','',8);
	$this->fpdf->Header();
$tot_detalle=0; $tamTitulo=0;
// column titles
// Inicializacion de la Cabecera
	$header = $w = array(); $tamTitulo = 190;
	$w=array(55,55,40,40);// maxima sumatoria 190
 	$header=array(html_entity_decode('Venta Realizadas'),html_entity_decode('Dep&oacute;sitos',ENT_COMPAT, 'ISO-8859-1'),html_entity_decode('D&eacute;bitos',ENT_COMPAT, 'ISO-8859-1'),html_entity_decode('Diferencia'));
    $alineacion=array('R','R','R','R');// R=>Derecha L=>izquierda C=> Centrado

	$datos['Efectivo']=$datos['Debito']=$datos['Cheque']=$datos['Transfer']=$datos['Gasto']=array();
	$DEP['Efectivo']=$DEP['Debito']=$DEP['Cheque']=$DEP['Transfer']=0; $GAS=0;
	$total_monto = 0;
    foreach ($depositos['Efectivo'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
		$datos['Efectivo'][]=array($row['Movbancario']['id'],$row['Movbancario']['nrodocumento'],$row['Documentotipo']['descorta'],$row['Banco']['nombre'],$row['Cuentasbancaria']['numerocuenta'],number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'));
	}
	$DEP['Efectivo']=$total_monto;
	$total_monto = 0;
	foreach ($depositos['Debito'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
		$datos['Debito'][]=array($row['Movbancario']['id'],$row['Movbancario']['nrodocumento'],$row['Documentotipo']['descorta'],$row['Banco']['nombre'],$row['Cuentasbancaria']['numerocuenta'],number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'));
	}
	$DEP['Debito']=$total_monto;
	$total_monto = 0;
	foreach ($depositos['Cheque'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
		$datos['Cheque'][]=array($row['Movbancario']['id'],$row['Movbancario']['nrodocumento'],$row['Documentotipo']['descorta'],$row['Banco']['nombre'],$row['Cuentasbancaria']['numerocuenta'],number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'));
	}
	$DEP['Cheque']=$total_monto;
	$total_monto = 0;
	foreach ($depositos['Transfer'] as $row){
		$total_monto=$total_monto+$row['Movbancario']['monto'];
		$datos['Transfer'][]=array($row['Movbancario']['id'],$row['Movbancario']['nrodocumento'],$row['Documentotipo']['descorta'],$row['Banco']['nombre'],$row['Cuentasbancaria']['numerocuenta'],number_format(sprintf("%01.2f", $row['Movbancario']['monto']), 2, ',', '.'));
	}
	$DEP['Transfer']=$total_monto;
	$total_monto = 0;
	foreach ($gastos['Gastos'] as $row){
		$total_monto=$total_monto+$row['Sucgasto']['monto'];
		$datos['Gasto'][]=array($row['Sucgasto']['id'],$row['Sucgasto']['nombre'],$row['Sucgasto']['motivo'],number_format(sprintf("%01.2f", $row['Sucgasto']['monto']), 2, ',', '.'));
	}
	$GAS=$total_monto;
	$total_monto = 0; $datos['Retencion']=array();
	foreach ($retenciones['Registros'] as $row){
		$total_monto=$total_monto+$row[0]['montoretenido'];
		$datos['Retencion'][]=array($row[0]['codretencion'],$row[0]['numero'],$row[0]['documento'],$row[0]['rif']." ".$row[0]['descripcion'],number_format(sprintf("%01.2f", $row[0]['montoretenido']), 2, ',', '.'));
	}
	$RET=$total_monto;
	//$datos1[]=array();
	$datos1[]=array(number_format(sprintf("%01.2f", $ventadeldia[0][0]['total']), 2, ',', '.'),number_format(sprintf("%01.2f", ($DEP['Efectivo']+$DEP['Debito']+$DEP['Cheque']+$DEP['Transfer'])), 2, ',', '.'),number_format(sprintf("%01.2f", ($GAS+$RET)), 2, ',', '.'),number_format(sprintf("%01.2f", $ventadeldia[0][0]['total'] - ($DEP['Efectivo']+$DEP['Debito']+$DEP['Cheque']+$DEP['Transfer']+$GAS+$RET)), 2, ',', '.'));	

	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);
	$this->fpdf->fpdf->SetFont('Arial','B',7);
	$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode(''),0,$alineacion,$conf_titulo);
	
 // Efectivo
	$header = $w = array(); $tamTitulo = 190;
	$w=array(10,20,40,40,40,40);// maxima sumatoria 190
 	$header=array(html_entity_decode('ID'),html_entity_decode('Nro'),html_entity_decode('Dep&oacute;sitos',ENT_COMPAT, 'ISO-8859-1'),html_entity_decode('Banco'),html_entity_decode('Cuenta'),html_entity_decode('Monto'));
    $alineacion=array('C','L','L','L','L','R');// R=>Derecha L=>izquierda C=> Centrado
    
	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);
	$this->fpdf->fpdf->SetFont('Arial','B',7);

	$this->fpdf->FancyTable($header,$datos['Efectivo'],$w,html_entity_decode(' Depositos en Efectivo '),0,$alineacion,$conf_titulo);

	$w=array(150,40);// maxima sumatoria 190
 	$header=array();
    $alineacion=array('R','R');// R=>Derecha L=>izquierda C=> Centrado
	$datos1 = array();
	$datos1[]=array('Total',number_format(sprintf("%01.2f", ($DEP['Efectivo'])), 2, ',', '.'));
	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo,'EstiloD'=>'B');
	$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode(''),0,$alineacion,$conf_titulo);
	
 //  Datos Punto de Ventas
	$w=array(10,20,40,40,40,40);// maxima sumatoria 190
 	$header=array(html_entity_decode('ID'),html_entity_decode('Nro'),html_entity_decode('Dep&oacute;sitos',ENT_COMPAT, 'ISO-8859-1'),html_entity_decode('Banco'),html_entity_decode('Cuenta'),html_entity_decode('Monto'));
    $alineacion=array('C','L','L','L','L','R');// R=>Derecha L=>izquierda C=> Centrado
    $conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);
	$this->fpdf->FancyTable($header,$datos['Debito'],$w,html_entity_decode(' Depositos en Punto de Ventas '),0,$alineacion,$conf_titulo);
 // Totales
 	$w=array(150,40);// maxima sumatoria 190
 	$header=array();
    $alineacion=array('R','R');// R=>Derecha L=>izquierda C=> Centrado
	$datos1 = array();
	$datos1[]=array('Total',number_format(sprintf("%01.2f", ($DEP['Debito'])), 2, ',', '.'));
	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo,'EstiloD'=>'B');
	$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode(''),0,$alineacion,$conf_titulo);
	
 //  Datos Cheque	
	$w=array(10,20,40,40,40,40);// maxima sumatoria 190
 	$header=array(html_entity_decode('ID'),html_entity_decode('Nro'),html_entity_decode('Dep&oacute;sitos',ENT_COMPAT, 'ISO-8859-1'),html_entity_decode('Banco'),html_entity_decode('Cuenta'),html_entity_decode('Monto'));
    $alineacion=array('C','L','L','L','L','R');// R=>Derecha L=>izquierda C=> Centrado
    $conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);
	$this->fpdf->FancyTable($header,$datos['Cheque'],$w,html_entity_decode(' Depositos en Cheques '),0,$alineacion,$conf_titulo);
 // Totales
 	$w=array(150,40);// maxima sumatoria 190
 	$header=array();
    $alineacion=array('R','R');// R=>Derecha L=>izquierda C=> Centrado
	$datos1 = array();
	$datos1[]=array('Total',number_format(sprintf("%01.2f", ($DEP['Cheque'])), 2, ',', '.'));
	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo,'EstiloD'=>'B');
	$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode(''),0,$alineacion,$conf_titulo);
		
 //  Datos Transferencia
	$w=array(10,20,40,40,40,40);// maxima sumatoria 190
 	$header=array(html_entity_decode('ID'),html_entity_decode('Nro'),html_entity_decode('Dep&oacute;sitos',ENT_COMPAT, 'ISO-8859-1'),html_entity_decode('Banco'),html_entity_decode('Cuenta'),html_entity_decode('Monto'));
    $alineacion=array('C','L','L','L','L','R');// R=>Derecha L=>izquierda C=> Centrado
    $conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);	
	$this->fpdf->FancyTable($header,$datos['Transfer'],$w,html_entity_decode(' Depositos en Transferencia '),0,$alineacion,$conf_titulo);
// Totales
 	$w=array(150,40);// maxima sumatoria 190
 	$header=array();
    $alineacion=array('R','R');// R=>Derecha L=>izquierda C=> Centrado
	$datos1 = array();
	$datos1[]=array('Total',number_format(sprintf("%01.2f", ($DEP['Transfer'])), 2, ',', '.'));
	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo,'EstiloD'=>'B');
	$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode(''),0,$alineacion,$conf_titulo);	

 //  Datos Gastos
	$w=array(10,60,80,40);// maxima sumatoria 190
 	$header=array(html_entity_decode('ID'),html_entity_decode('Nombre'),html_entity_decode('Motivo'),html_entity_decode('Monto'));
    $alineacion=array('C','L','L','R');// R=>Derecha L=>izquierda C=> Centrado
    $conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);	
	$this->fpdf->FancyTable($header,$datos['Gasto'],$w,html_entity_decode(' Gastos Realizados '),0,$alineacion,$conf_titulo);
// Totales
 	$w=array(150,40);// maxima sumatoria 190
 	$header=array();
    $alineacion=array('R','R');// R=>Derecha L=>izquierda C=> Centrado
	$datos1 = array();
	$datos1[]=array('Total',number_format(sprintf("%01.2f", ($GAS)), 2, ',', '.'));
	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo,'EstiloD'=>'B');
	$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode(''),0,$alineacion,$conf_titulo);

//  Datos Retencion
	$w=array(10,20,20,100,40);// maxima sumatoria 190
 	$header=array(html_entity_decode('ID'),html_entity_decode('Numero'),html_entity_decode('Factura'),html_entity_decode('Cliente'),html_entity_decode('Monto'));
    $alineacion=array('C','L','L','L','R');// R=>Derecha L=>izquierda C=> Centrado
    $conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo);	
	$this->fpdf->FancyTable($header,$datos['Retencion'],$w,html_entity_decode(' Retenciones Realizadas en Caja por Clientes '),0,$alineacion,$conf_titulo);
// Totales
 	$w=array(150,40);// maxima sumatoria 190
 	$header=array();
    $alineacion=array('R','R');// R=>Derecha L=>izquierda C=> Centrado
	$datos1 = array();
	$datos1[]=array('Total',number_format(sprintf("%01.2f", ($RET)), 2, ',', '.'));
	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo,'EstiloD'=>'B');
	$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode(''),0,$alineacion,$conf_titulo);
//	echo date('d-m-Y',strtotime($cuadre['Cuadrediario']['fecha']));
 // Fin de Inclusion de los datos a mostrar en el pdf
    echo $this->fpdf->fpdfOutput('cuadre_diario_'.date('d-m-Y',strtotime($cuadre['Cuadrediario']['fecha'])).'.pdf','D');   
?>
