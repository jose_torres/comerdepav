<?php
//echo $this->element('menuinterno',$datos_menu);
//echo $this->Html->script('prototype');
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
echo $this->Html->css('iCheck/all.css');
?>
<script language="javascript" type="text/javascript">
	
	function actualizar(nombrediv,url,id,datos){

        $('#'+nombrediv).html('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');	
				
		$.ajax({
			type: 'POST',
			url: url+id,
			data: datos,
			dataType: 'html'
		})

		.done(function( html ) {					
			$( '#'+nombrediv ).html( html );
		});

    }
    function limpiar(){
		$( '#retencion' ).html( '' );	
	}
	function deseleccionar_todo(){
		for (i=0;i<document.frm.elements.length;i++)
		if(document.frm.elements[i].type == "checkbox")
			document.frm.elements[i].checked=0
	} 
    function calculo_marcar(nombre_tag,codigo){
	//	alert ("Hola");
		var elemento = document.getElementById(""+nombre_tag); 			 			
		var valor_sel=true;
		if (elemento.checked==true){
			valor_sel=elemento.checked;
			deseleccionar_todo();
			elemento.checked=valor_sel;			
			actualizar('retencion','calcularretencion','',''+codigo)		
		}else{
			$( '#retencion' ).html( ' <div class="alert alert-info">Seleccione una Factura</div> ' );			
		}
		
	}
	
</script>
<fieldset id="personal" >

<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'add','id'=>'frm','name'=>'frm'));
?>	
<?php
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Proveedores:', array('class' => 'col-lg-2 control-label','for'=>"proveedore_id"));
echo '<div class="col-lg-6">';
echo $this->Form->input('Reporte.proveedore_id',array('div'=>false,'options'=>$proveedore,'label'=>false,'data-placeholder'=>"Seleccione el Proveedor",'empty' =>'(Seleccione uno ...)','class'=>"form-control select2","style"=>"width: 100%;","onchange"=>"actualizar('buscar','buscarfacturas','','codproveedor='+document.frm.ReporteProveedoreId.value)"));
echo '</div>';		
echo '</div>';
	?>
<button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarfacturas','','codproveedor='+document.frm.ReporteProveedoreId.value);limpiar();">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>	

<center>

<div id="buscar">

</div>
</center>
<div id="retencion">
</div>
<center>
<?php echo $this->Form->end(); ?>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php //echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/funciones/add", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</center>

</fieldset>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/iCheck/icheck.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script language="javascript" type="text/javascript">
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    
    //iCheck for checkbox and radio inputs
 /*   $('input[type="checkbox"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue'
    });*/

  });
</script>
