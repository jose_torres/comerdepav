<?php
//echo $this->element('menuinterno',$datos_menu);
//echo $this->Html->script('prototype');
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
echo $this->Html->css('iCheck/all.css');
?>
<script language="javascript" type="text/javascript">
	
	function actualizar(nombrediv,url,id,datos){

        $('#'+nombrediv).html('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');	
				
		$.ajax({
			type: 'POST',
			url: url+id,
			data: datos,
			dataType: 'html'
		})

		.done(function( html ) {					
			$( '#'+nombrediv ).html( html );
		});

    }
    function limpiar(){
		$( '#retencion' ).html( '' );	
	}
	function deseleccionar_todo(){
		for (i=0;i<document.frm.elements.length;i++)
		if(document.frm.elements[i].type == "checkbox")
			document.frm.elements[i].checked=0
	} 
    function calculo_marcar(nombre_tag,codigo){
	//	alert ("Hola");
		var elemento = document.getElementById(""+nombre_tag); 			 			
		var valor_sel=true;
		if (elemento.checked==true){
			valor_sel=elemento.checked;
			deseleccionar_todo();
			elemento.checked=valor_sel;			
			actualizar('retencion','calcularretencion','',''+codigo)		
		}else{
			$( '#retencion' ).html( ' <div class="alert alert-info">Seleccione una Factura</div> ' );			
		}
		
	}
	
</script>
<div class="span11">
	<div class="well">
	<?php echo $this->Form->create('Retencioncompra', array('url' => 'edit/'.$this->data['Retencioncompra']['codretencion'],'name' => 'frmRet','id'=>'frmRet','role'=>"form",'class'=>"form-horizontal")); ?>
	<fieldset>
	<legend>DATOS DE RETENCI&Oacute;N</legend>
	<?php
	$tiporeporte['Normal']='Normal';
	//$tiporeporte['Transito']='Transito';
	echo $this->Form->input('Retencioncompra.codretencion');
	echo '<div class="form-group">';		
	echo $this->Html->tag('label', 'Tipo de retenci&oacute;n:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.tiporetencion',array('div'=>false,'options'=>$tiporeporte,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Retencion",'class'=>"form-control select2","style"=>"width: 100%;"));
	echo '</div>';

	echo $this->Html->tag('label', 'Mes que Afecta:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.mes',array('label'=>'','type' => 'date','dateFormat' => 'M','div'=>false,'label'=>false,'placeholder'=>"",'class'=>"form-control"));
	echo '</div>';

	echo $this->Html->tag('label', 'A&ntilde;o Afecta:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.anio',array('label'=>'','type' => 'date','dateFormat' => 'Y','div'=>false,'label'=>false,'placeholder'=>"",'class'=>"form-control text-right",'maxYear' => date('Y'),'minYear' => 2016));
	echo '</div>';
	echo '</div>';
	
	echo '<div class="form-group">';
		
	echo $this->Html->tag('label', 'Monto Iva:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.iva1',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>number_format(sprintf("%01.2f", $this->data['Retencioncompra']['montoiva']), 2, ',', '.')));
	echo $this->Form->input('Retencioncompra.montoiva', array('type'=>'hidden','value'=>$this->data['Retencioncompra']['montoiva']));	
	echo '</div>';
	//$datos['Retencion']=round(round($datos['Iva']*0.75,3),2);
	echo $this->Html->tag('label', 'Monto Retencion:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.ret1',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>number_format(sprintf("%01.2f", $this->data['Retencioncompra']['montoretenido']), 2, ',', '.')));
	echo $this->Form->input('Retencioncompra.montoretenido', array('type'=>'hidden','value'=>$this->data['Retencioncompra']['montoretenido']));	
	echo '</div>';
	echo '</div>';

	echo '<div class="form-group">';

	echo $this->Html->tag('label', 'Factura:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.documento',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>$this->data['Retencioncompra']['documento']));
	echo $this->Form->input('Retencioncompra.tipodocumento', array('type'=>'hidden','value'=>$this->data['Retencioncompra']['tipodocumento']));	
	echo $this->Form->input('Retencioncompra.codsucursal', array('type'=>'hidden','value'=>$this->data['Retencioncompra']['codsucursal']));	
	echo $this->Form->input('Retencioncompra.codproveedor', array('type'=>'hidden','value'=>$this->data['Retencioncompra']['codproveedor']));	
	echo $this->Form->input('Retencioncompra.codmovimiento', array('type'=>'hidden','value'=>$this->data['Retencioncompra']['codmovimiento']));	
	echo $this->Form->input('Retencioncompra.numero', array('type'=>'hidden','value'=>$this->data['Retencioncompra']['numero']));	
	echo '</div>';
	
	$fecha2=date("d-m-Y");
	//$fecha=date('d-m-Y',$this->data['Retencioncompra']['fechaemision']);
	$fecha=date("d-m-Y",strtotime($this->data['Retencioncompra']['fechaemision']));
	echo $this->Html->tag('label', 'Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="input-daterange col-xs-2">';
	echo $this->Form->input('Retencioncompra.fecha',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha2,'readonly'=>true)); 
	echo '</div>';		
	echo '</div>';
	
	?>
	</fieldset>
	<script language="javascript" type="text/javascript">
  	
  //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
</script>
	<?php echo $this->Form->end('Guardar',array('input'=>array('class'=>"btn btn-primary"))); ?>
	</div>
</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php //echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/funciones/add", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</center>

</fieldset>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/iCheck/icheck.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script language="javascript" type="text/javascript">
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    
    //iCheck for checkbox and radio inputs
 /*   $('input[type="checkbox"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue'
    });*/

  });
</script>
