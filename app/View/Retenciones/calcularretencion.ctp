<?php
	//print_r($data);
	$datos['Base']=0;$datos['Iva']=0;
	foreach ($data as $row){
		$datos['Base']=$datos['Base']+$row[0]['base'];
		$datos['Iva']=$datos['Iva']+$row[0]['iva'];
	}
?>
<div class="span11">
	<div class="well">
	<?php echo $this->Form->create('Retencioncompra', array('url' => 'incluir','name' => 'frmRet','id'=>'frmRet','role'=>"form",'class'=>"form-horizontal")); ?>
	<fieldset>
	<legend>DATOS DE RETENCI&Oacute;N</legend>
	<?php
	$tiporeporte['Normal']='Normal';
	$tiporeporte['Transito']='Transito';

	echo '<div class="form-group">';		
	echo $this->Html->tag('label', 'Tipo de retenci&oacute;n:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.tiporetencion',array('div'=>false,'options'=>$tiporeporte,'label'=>false,'data-placeholder'=>"Seleccione Tipo de Ventas",'class'=>"form-control select2","style"=>"width: 100%;"));
	echo '</div>';

	echo $this->Html->tag('label', 'Mes que Afecta:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.mes',array('label'=>'','type' => 'date','dateFormat' => 'M','div'=>false,'label'=>false,'placeholder'=>"",'class'=>"form-control"));
	echo '</div>';

	echo $this->Html->tag('label', 'A&ntilde;o Afecta:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.anio',array('label'=>'','type' => 'date','dateFormat' => 'Y','div'=>false,'label'=>false,'placeholder'=>"",'class'=>"form-control text-right",'maxYear' => date('Y'),'minYear' => 2016));
	echo '</div>';
	echo '</div>';
	
	echo '<div class="form-group">';		
	echo $this->Html->tag('label', 'Monto Base:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.base1',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>number_format(sprintf("%01.2f", $datos['Base']), 2, ',', '.')));
	echo $this->Form->input('Retencioncompra.base', array('type'=>'hidden','value'=>$datos['Base']));	
	echo '</div>';
		
	echo $this->Html->tag('label', 'Monto Iva:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.iva1',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>number_format(sprintf("%01.2f", $datos['Iva']), 2, ',', '.')));
	echo $this->Form->input('Retencioncompra.montoiva', array('type'=>'hidden','value'=>$datos['Iva']));	
	echo '</div>';
	$datos['Retencion']=round(round($datos['Iva']*0.75,3),2);
	echo $this->Html->tag('label', 'Monto Retencion:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.ret1',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>number_format(sprintf("%01.2f", $datos['Retencion']), 2, ',', '.')));
	echo $this->Form->input('Retencioncompra.montoretenido', array('type'=>'hidden','value'=>$datos['Retencion']));	
	echo '</div>';
	echo '</div>';

	echo '<div class="form-group">';

	echo $this->Html->tag('label', 'Factura:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retencioncompra.documento',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>$registros['documento']));
	echo $this->Form->input('Retencioncompra.tipodocumento', array('type'=>'hidden','value'=>$registros['tipodocumento']));	
	echo $this->Form->input('Retencioncompra.codsucursal', array('type'=>'hidden','value'=>$registros['codsucursal']));	
	echo $this->Form->input('Retencioncompra.codproveedor', array('type'=>'hidden','value'=>$registros['codproveedor']));	
	echo $this->Form->input('Retencioncompra.codmovimiento', array('type'=>'hidden','value'=>$registros['codmovimiento']));	
	echo '</div>';
	
	$fecha2=date("d-m-Y");
	$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
	echo $this->Html->tag('label', 'Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="input-daterange col-xs-2" id="datepicker">';
	echo $this->Form->input('Retencioncompra.fecha',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha2,'readonly'=>true)); 
	echo '</div>';		
	echo '</div>';
	
	?>
	<table class="table table-bordered">
	<thead class="thead-inverse">
	<tr>
		<th>Id</th>
		<th>Fecha de Emisi&oacute;n</th>
		<th>Documento</th>
		<th>Fecha de Vencimiento</th>
		<th>Tipo</th>
		<th>Concepto</th>
		<th>Total Factura</th>
		<th>Saldo</th>
		<th>Iva</th>
		<th><?php echo ('Retencion');?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	$t = 0; $retencion=$iva=$factura=$saldo=0;
	foreach ($facturas as $row){						
	?>
	<tr><td><?php
		echo $row[0]['codmovimiento'];
		echo $this->Form->input('Retencioncompras.'.$t.'.codsucursal', array('type'=>'hidden','value'=>$registros['codsucursal']));	
		echo $this->Form->input('Retencioncompras.'.$t.'.fecha', array('type'=>'hidden','value'=>$fecha2));
		echo $this->Form->input('Retencioncompras.'.$t.'.codproveedor', array('type'=>'hidden','value'=>$registros['codproveedor']));
		echo $this->Form->input('Retencioncompras.'.$t.'.codmovimiento', array('type'=>'hidden','value'=>$row[0]['codmovimiento']));	
		?></td>
		<td ><?php echo date("d-m-Y",strtotime($row['0']['femision']));?></td>
		<td><?php echo $row[0]['nrodocumento'];
				echo $this->Form->input('Retencioncompras.'.$t.'.documento', array('type'=>'hidden','value'=>$row[0]['nrodocumento']));
		?></td>
		<td><?php echo date("d-m-Y",strtotime($row['0']['fvencimiento']));?></td>
		<td><?php echo $row[0]['tipomovimiento'];
				echo $this->Form->input('Retencioncompras.'.$t.'.tipodocumento', array('type'=>'hidden','value'=>$row[0]['tipomovimiento']));	
		?></td>
		<td><?php echo $row[0]['concepto'];?></td>
		<td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total']), 2, ',', '.');?></td>
		<td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['vmonto']), 2, ',', '.');?></td>
		<td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['iva']), 2, ',', '.');
			echo $this->Form->input('Retencioncompras.'.$t.'.montoiva', array('type'=>'hidden','value'=>$row[0]['iva']));
		?></td>
		<td align="right"><?php
		$retenciones=round(round($row[0]['iva']*0.75,3),2);
		echo $this->Form->input('Retencioncompras.'.$t.'.montoretenido', array('type'=>'hidden','value'=>$retenciones));
		echo number_format(sprintf("%01.2f", $retenciones), 2, ',', '.');?></td>
		
	</tr>
	<?php
		$saldo=$saldo+ $row[0]['vmonto'];
		$factura=$factura+ $row[0]['total'];
		$iva=$iva+ $row[0]['iva'];
		$retencion=$retencion+$retenciones;
		$t=$t+1;
	}
	echo $this->Form->input('Retencione.totales', array('type' =>'hidden','label' =>'','value'=>$t));
	?>
	</tbody>
	<tfoot>
		<tr>
			<td align="right" colspan="6"><label>Total:</label></td>
			<td align="right">&nbsp;<?php echo number_format(sprintf("%01.2f", $factura), 2, ',', '.');?></td>
			<td align="right">&nbsp;<?php echo number_format(sprintf("%01.2f", $saldo), 2, ',', '.');?></td>
			<td align="right">&nbsp;<?php echo number_format(sprintf("%01.2f", $iva), 2, ',', '.');?></td>
			<td align="right">&nbsp;<?php echo number_format(sprintf("%01.2f", $retencion), 2, ',', '.');?></td>
			
		</tr>
	</tfoot>

</table>
	</fieldset>
	<script language="javascript" type="text/javascript">
  	
  //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
</script>
	<?php echo $this->Form->end('Guardar',array('input'=>array('class'=>"btn btn-primary"))); ?>
	</div>
</div>			
