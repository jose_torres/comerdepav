<?php

$this->tcpdf->core->SetPageOrientation("L");
$this->tcpdf->core->SetOrientacion("LRET");
$this->tcpdf->core->SetProtection(array(), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloEncabezado($encabezado['encabezado'],'',10);
$titulo[1]=$encabezado['titulo'];$tc_forma[1]='B';$tc_size[1]=10;
$titulo[2]=$encabezado['providencia'];$tc_forma[2]='';$tc_size[2]=10;
$this->tcpdf->core->SetTituloContenido($titulo,$tc_forma,$tc_size);
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Siscom System');
$this->tcpdf->core->SetHeaderMargin(10);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(40);
$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, '', 'RETENCIONES ');
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );
$this->tcpdf->core->setHeaderFont(array($textfont,'',11));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';


$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");

//-----Def. de Datos Contenidos------------------------------------------
//-----Cuadros---------------------------------------------
$this->tcpdf->core->Ln(2);
$this->tcpdf->core->Rect(10,41,140,14);//Nombre
$this->tcpdf->core->Rect(155,41,50,14);//RIF
$this->tcpdf->core->Rect(210,41,70,14);//Nro Comprobante
$this->tcpdf->core->Rect(10,57,195,14);//Direccion
$this->tcpdf->core->Rect(210,57,70,14);//Fecha Comprobante
$this->tcpdf->core->Rect(10,73,140,14);//Nombre Del Sujeto
$this->tcpdf->core->Rect(155,73,50,14);//RIF Del Sujeto
$this->tcpdf->core->Rect(210,73,70,14);//Periodo Fiscal
//-----------------------------------------------------------------------
$this->tcpdf->core->SetFont('helvetica','',9);
$this->tcpdf->core->MultiCell(140,0.5,html_entity_decode('NOMBRE O RAZ&Oacute;N SOCIAL DEL AGENTE DE RETENCI&Oacute;N',ENT_COMPAT ,'UTF-8'),0,'L',false,1,10,'',true,0, false,true, 0,'M',false);		
$this->tcpdf->core->Ln(2);
$this->tcpdf->core->MultiCell(140,0.5,html_entity_decode($empresa['Empresa']['nombre'],ENT_COMPAT ,'UTF-8'),0,'R',false,1,10,'',true,0, false,true, 0,'M',false);
// Rif Empresa
$this->tcpdf->core->SetXY(155,42);
$this->tcpdf->core->Cell(20,0.5,'RIF',0,1,'L');
$this->tcpdf->core->SetXY(155,49);
$this->tcpdf->core->Cell(50,0.5,html_entity_decode($empresa['Empresa']['rif'],ENT_COMPAT ,'UTF-8'),0,1,'R');
//Nro Comprobante
$this->tcpdf->core->SetXY(210,42);
$this->tcpdf->core->Cell(25,0.5,'NRO. COMPROBANTE',0,1,'L');
$this->tcpdf->core->SetXY(220,49);
$this->tcpdf->core->Cell(60,0.5,html_entity_decode($data[0]['Retencioncompra']['numero'],ENT_COMPAT ,'UTF-8'),0,1,'R');
//Direccion del Agente
$this->tcpdf->core->SetXY(10,58);
$this->tcpdf->core->Cell(25,0.5,html_entity_decode('DIRECCI&Oacute;N DEL AGENTE DE  RETENCI&Oacute;N',ENT_COMPAT ,'UTF-8'),0,1,'L');
$this->tcpdf->core->SetXY(10,65);
$this->tcpdf->core->Cell(190,0.5,html_entity_decode($empresa['Empresa']['direccion'],ENT_COMPAT ,'UTF-8'),0,1,'R');
//Fecha del Comprobante
$this->tcpdf->core->SetXY(210,58);
$this->tcpdf->core->Cell(25,0.5,html_entity_decode('FECHA DEL COMPROBANTE',ENT_COMPAT ,'UTF-8'),0,1,'L');
$this->tcpdf->core->SetXY(210,65);
$this->tcpdf->core->Cell(70,0.5,date("d-m-Y",strtotime($data[0]['Retencioncompra']['fechaemision'])),0,1,'R');
//Nombre del Sujeto Retenido
$this->tcpdf->core->SetXY(10,74);
$this->tcpdf->core->Cell(140,0.5,html_entity_decode('NOMBRE O RAZ&Oacute;N SOCIAL DEL SUJETO RETENIDO',ENT_COMPAT ,'UTF-8'),0,1,'L');
$this->tcpdf->core->SetXY(10,81);
$this->tcpdf->core->Cell(140,0.5,html_entity_decode($data[0]['Proveedore']['descripcion'],ENT_COMPAT ,'UTF-8'),0,1,'R');
//Rif del Proveedor
$this->tcpdf->core->SetXY(155,74);
$this->tcpdf->core->Cell(20,0.5,'RIF',0,1,'L');
$this->tcpdf->core->SetXY(155,81);
$this->tcpdf->core->Cell(50,0.5,html_entity_decode($data[0]['Proveedore']['rif'],ENT_COMPAT ,'UTF-8'),0,1,'R');
//Periodo Fiscal
$this->tcpdf->core->SetXY(210,74);
$this->tcpdf->core->Cell(25,0.5,html_entity_decode('PER&Iacute;ODO FISCAL',ENT_COMPAT ,'UTF-8'),0,1,'L');
$this->tcpdf->core->SetXY(220,81);
$this->tcpdf->core->Cell(60,0.5,html_entity_decode('A&Ntilde;O:'.$data[0]['Retencioncompra']['anioafecta'].'/ MES:'.$data[0]['Retencioncompra']['mesafecta'].'/ '.$quincena,ENT_COMPAT ,'UTF-8'),0,1,'R');
//Datos de la Retencion
$texto="";
$texto='<table border="1" cellpadding="1" cellspacing="0">
	<thead><tr>
		<th align="center">Fecha<br> de Factura</th>
		<th align="center">Numero de Factura</th>
		<th align="center">Numero Ctrol de Factura</th>
		<th align="center">Numero Nota de Debito</th>
		<th align="center">Numero Nota de Credito</th>
		<th align="center">Tipo de Transacci&oacute;n</th>
		<th align="center">Nro de Factura Afectada</th>
		<th align="center">Total Compras Incluyendo Iva</th>
		<th align="center">Compras sin Derecho a Credito Iva</th>
		<th align="center">Base Imponible</th>		
		<th align="center" width="5%">%<br>Al&iacute;cuota</th>		
		<th align="center">Impuesto Iva</th>
		<th align="center">Iva Retenido</th>				
	</tr></thead>
	';
//$texto=$texto.'<tbody>';
$retencion['montobruto']=$retencion['scredito']=$retencion['baseimp2']=$retencion['ivaimp2']=$retencion['retencion']=$retencion['exento']=0;
foreach ($data as $ret){
	$ret['Retencioncompra']['tipo_trans'] = '1 reg';
if ($ret['Retencioncompra']['tipodocumento']=='COMPRAS'){	
$texto=$texto.'
	<tr>
	<td>'.date("d/m/Y",strtotime($ret['Compra']['femision'])).'</td>
	<td>'.$ret['Retencioncompra']['documento'].'</td>
	<td>'.$ret['Compra']['nrocontrol'].'</td>	
	<td></td>
	<td></td>
	<td>'.$ret['Retencioncompra']['tipo_trans'].'</td>
	<td></td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret['Compra']['montobruto']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret['Compra']['montoexento']), 2, ',', '.').'</td>
	<td align="right" >'.number_format(sprintf("%01.2f", $ret['Compra']['baseimp2']), 2, ',', '.').'</td>
	<td  width="5%">'.number_format(sprintf("%01.2f", $ret['Compra']['porimp2']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret['Compra']['ivaimp2']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret['Retencioncompra']['montoretenido']), 2, ',', '.').'</td>
	</tr>';
	$retencion['montobruto']= $retencion['montobruto'] + $ret['Compra']['montobruto'];
	$retencion['exento']=$retencion['exento']  + $ret['Compra']['montoexento'];
	$retencion['scredito']= $retencion['scredito'] + 0;
	$retencion['baseimp2']= $retencion['baseimp2'] + $ret['Compra']['baseimp2'];
	$retencion['ivaimp2']= $retencion['ivaimp2'] + $ret['Compra']['ivaimp2'];
	$retencion['retencion']= $retencion['retencion'] + $ret['Retencioncompra']['montoretenido'];
}elseif ($ret['Retencioncompra']['tipodocumento']=='FA'){	
$texto=$texto.'
	<tr>
	<td>'.date("d/m/Y",strtotime($ret['Cpfactura']['fecha'])).'</td>
	<td>'.$ret['Retencioncompra']['documento'].'</td>
	<td>'.$ret['Cpfactura']['nrocontrol'].'</td>	
	<td></td>
	<td></td>
	<td>'.$ret['Retencioncompra']['tipo_trans'].'</td>
	<td></td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret['Cpfactura']['monto']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret['Cpfactura']['montoexento']), 2, ',', '.').'</td>
	<td align="right" >'.number_format(sprintf("%01.2f", $ret['Cpfactura']['baseimp2']), 2, ',', '.').'</td>
	<td  width="5%">'.number_format(sprintf("%01.2f", $ret['Cpfactura']['porimp2']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret['Cpfactura']['ivaimp2']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret['Retencioncompra']['montoretenido']), 2, ',', '.').'</td>
	</tr>';
	$retencion['montobruto']= $retencion['montobruto'] + $ret['Cpfactura']['monto'];
	$retencion['exento']=$retencion['exento']  + $ret['Cpfactura']['montoexento'];
	$retencion['scredito']= $retencion['scredito'] + 0;
	$retencion['baseimp2']= $retencion['baseimp2'] + $ret['Cpfactura']['baseimp2'];
	$retencion['ivaimp2']= $retencion['ivaimp2'] + $ret['Cpfactura']['ivaimp2'];
	$retencion['retencion']= $retencion['retencion'] + $ret['Retencioncompra']['montoretenido'];
}
	
}
foreach ($datos_nd as $ret){
	$ret['Retencioncompra']['tipo_trans'] = '3 reg';
	$ret[0]['montoexento'] = 0;
$texto=$texto.'
	<tr>
	<td>'.date("d/m/Y",strtotime($ret[0]['fecha'])).'</td>
	<td></td>
	<td>'.$ret[0]['nrocontrol'].'</td>	
	<td>'.$ret[0]['nrodocumento'].'</td>
	<td></td>
	<td>'.$ret['Retencioncompra']['tipo_trans'].'</td>
	<td>'.$ret[0]['facturaafecta'].'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret[0]['montobruto']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret[0]['montoexento']), 2, ',', '.').'</td>
	<td align="right" >'.number_format(sprintf("%01.2f", $ret[0]['baseimp2']), 2, ',', '.').'</td>
	<td  width="5%">'.number_format(sprintf("%01.2f", $ret[0]['porimp2']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", $ret[0]['ivaimp2']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", round(round(1* $ret[0]['ivaimp2']*0.75,3),2)), 2, ',', '.').'</td>
	</tr>';
	$retencion['montobruto']= $retencion['montobruto'] + $ret[0]['montobruto'];
	$retencion['exento']=$retencion['exento']  + $ret[0]['montoexento'];
	$retencion['scredito']= $retencion['scredito'] + 0;
	$retencion['baseimp2']= $retencion['baseimp2'] + $ret[0]['baseimp2'];
	$retencion['ivaimp2']= $retencion['ivaimp2'] + $ret[0]['ivaimp2'];
	$retencion['retencion']= $retencion['retencion'] + round(round(1* $ret[0]['ivaimp2']*0.75,3),2);

}	
$totales['montobruto']=$totales['scredito']=$totales['baseimp2']=$totales['ivaimp2']=$totales['retencion']=$totales['exento']=0;
foreach ($datos_nc as $nc){	
$texto=$texto.'
	<tr>
	<td>'.date("d/m/Y",strtotime($data['Compra']['femision'])).'</td>
	<td></td>		
	<td>'.$nc['Cpnotacredito']['nrocontrol'].'</td>
	<td></td>
	<td>'.str_pad($nc['Nccompra']['codnotacredito'], 8, "0", STR_PAD_LEFT).'</td>
	<td> 3 reg.</td>
	<td>'.$nc['Nccompra']['facturaafecta'].'</td>
	<td align="right">'.number_format(sprintf("%01.2f", -1*$nc['Nccompra']['montobruto']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", -1*$nc['Nccompra']['montoexento']), 2, ',', '.').'</td>	
	<td align="right">'.number_format(sprintf("%01.2f", -1*$nc['Nccompra']['baseimp2']), 2, ',', '.').'</td>
	<td  width="5%">'.number_format(sprintf("%01.2f", $nc['Nccompra']['porimp2']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", -1*$nc['Nccompra']['ivaimp2']), 2, ',', '.').'</td>
	<td align="right">'.number_format(sprintf("%01.2f", round(round(-1*$nc['Nccompra']['ivaimp2']*0.75,3),2)), 2, ',', '.').'</td>
	</tr>';
	$totales['montobruto']= $totales['montobruto'] + $nc['Nccompra']['montobruto'];
	$totales['exento']=$totales['exento']  + $nc['Nccompra']['montoexento'];
	$totales['scredito']= $totales['scredito'] + 0;
	$totales['baseimp2']= $totales['baseimp2'] + $nc['Nccompra']['baseimp2'];
	$totales['ivaimp2']= $totales['ivaimp2'] + $nc['Nccompra']['ivaimp2'];
	$totales['retencion']= $totales['retencion'] + round(round($nc['Nccompra']['ivaimp2']*0.75,3),2);
}	
$texto=$texto.'
	<tr>	
	<td colspan="7" align="right"><b>Totales:</b></td>
	<td align="right"><b>'.number_format(sprintf("%01.2f", $retencion['montobruto'] - $totales['montobruto']), 2, ',', '.').'</b></td>
	<td align="right"><b>'.number_format(sprintf("%01.2f", $retencion['exento'] - $totales['exento']), 2, ',', '.').'</b></td>
	<td align="right"><b>'.number_format(sprintf("%01.2f", $retencion['baseimp2'] - $totales['baseimp2']), 2, ',', '.').'</b></td>
	<td  width="5%"></td>
	<td align="right"><b>'.number_format(sprintf("%01.2f", $retencion['ivaimp2'] - $totales['ivaimp2']), 2, ',', '.').'</b></td>
	<td align="right"><b>'.number_format(sprintf("%01.2f", $retencion['retencion'] - $totales['retencion']), 2, ',', '.').'</b></td>
	</tr>';
//$texto=$texto.'</tbody>';
$texto=$texto.'</table>';

$this->tcpdf->core->Ln(4);
//$this->tcpdf->core->writeHTMLCell(280,0, '', '', $texto, 1, 0, 0, true, 'J', true);
$this->tcpdf->core->writeHTML($texto, true, false, true, false, 'C');
$style = array('width' => 0.5, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(0, 0, 0));
$this->tcpdf->core->Line(50,168,120,168,$style);
$this->tcpdf->core->Line(190,168,260,168,$style);
$this->tcpdf->core->SetXY(60,169);
$this->tcpdf->core->Cell(50,0.5,'FIRMA DEL BENEFICIARIO',0,1,'C');
$this->tcpdf->core->SetXY(200,169);
$this->tcpdf->core->Cell(50,0.5,'FIRMA DEL AGENTE DE RETENCION',0,1,'C');
$this->tcpdf->core->SetXY(183,175);
$this->tcpdf->core->Cell(60,0.5,'Fecha de Entrega Del Comprobante _____/_____/_______',0,1,'C');

//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
$this->tcpdf->tcpdfOutput('retenciones.pdf', 'D');
?>
