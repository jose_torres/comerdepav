<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover">
	<thead>	
	<tr>
			<th>Id</th>
			<th>Numero</th>
			<th>Proveedor</th>
			<th>Facturas</th>
			<th class="actions"><?php echo 'Monto Bruto';?></th>			
			<th>Monto Iva</th>
			<th>Monto Retenido</th>
			<th>Fecha</th>
			<th class="actions"><?php echo 'Acciones';?></th>
	</tr>
	</thead>
	<?php
	$i = 0;
	foreach ($data as $unidade):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $i; ?>&nbsp;</td>
		<td><?php echo $unidade['Retencioncompra']['numero']; ?>&nbsp;</td>
		<td><?php echo $unidade['Proveedore']['descripcion']; ?>&nbsp;</td>
		<td><?php //echo $unidade['Retencioncompra']['documento']; ?>&nbsp;</td>
		<td class="text-right"><?php //echo number_format(sprintf("%01.2f", $unidade['Compra']['montobruto']), 2, ',', '.'); ?>&nbsp;</td>
		<td class="text-right"><?php echo number_format(sprintf("%01.2f", $unidade[0]['montoiva']), 2, ',', '.'); ?>&nbsp;</td>
		<td class="text-right"><?php echo number_format(sprintf("%01.2f", $unidade[0]['montoretenido']), 2, ',', '.'); ?>&nbsp;</td>
		<td><?php echo date("d-m-Y",strtotime($unidade['Retencioncompra']['fechaemision'])); ?>&nbsp;</td>
		<td class="actions" align="center">
			<?php 
			echo '&nbsp;&nbsp;&nbsp;';
			echo $this->Html->link(__('<span class="glyphicon glyphicon-print"></span>&nbsp;Imprimir'), array('action' => 'viewpdf', $unidade['Retencioncompra']['numero']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
			echo '&nbsp;&nbsp;&nbsp;';
			 ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
