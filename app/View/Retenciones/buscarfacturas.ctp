<?php
	//print_r($data);
?>
<script language="javascript" type="text/javascript">

</script>	
<br>
<div class="box-body table-responsive no-padding">
<table class="table table-bordered">
	<thead class="thead-inverse">
	<tr>
			<th>Id</th>
			<th>Fecha de Emisi&oacute;n</th>
			<th>Documento</th>
			<th>Fecha de Vencimiento</th>
			<th>Tipo</th>
			<th>Concepto</th>
			<th>Total Factura</th>
			<th>Saldo</th>
			<th>Iva</th>
			<th><?php echo ('Pagar');?></th>
	</tr>
	</thead>
	<tbody>
	<?php
	$t = 0; $iva=$factura=$saldo=0;
	foreach ($data as $row){
		$t=$t+1;		
	?>
	<tr><td><?php echo $row[0]['codmovimiento'];?></td>
		<td ><?php echo date("d-m-Y",strtotime($row['0']['femision']));?></td>
		<td><?php echo $row[0]['nrodocumento'];?></td>
		<td><?php echo date("d-m-Y",strtotime($row['0']['fvencimiento']));?></td>
		<td><?php echo $row[0]['tipomovimiento'];?></td>
		<td><?php echo $row[0]['concepto'];?></td>
		<td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['total']), 2, ',', '.');?></td>
		<td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['vmonto']), 2, ',', '.');?></td>
		<td align="right"><?php echo number_format(sprintf("%01.2f", $row[0]['iva']), 2, ',', '.');?></td>
		<td>&nbsp;<?php echo $this->Form->checkbox('Seleccion.'.$t.'.chequeo', array('size' => 5,'type' =>'radio','label' =>'','value'=>$t,'div'=>false,'onclick'=>"calculo_marcar('Seleccion".$t."Chequeo','codmovimiento=".$row[0]['codmovimiento']."&tipodocumento=".$row[0]['tipomovimiento']."&documento=".$row[0]['nrodocumento']."&codsucursal=".$row[0]['codsucursal']."&codproveedor=".$datos['codproveedor']."')"));
		echo $this->Form->input('Valor.'.$t.'.codmovimiento', array('type' =>'hidden','label' =>'','value'=>$row[0]['codmovimiento']));
		echo $this->Form->input('Valor.'.$t.'.tipomovimiento', array('type' =>'hidden','label' =>'','value'=>$row[0]['tipomovimiento']));
		echo $this->Form->input('Valor.'.$t.'.nrodocumento', array('type' =>'hidden','label' =>'','value'=>$row[0]['nrodocumento']));
		?>&nbsp;&nbsp;</td>
	</tr>
	<?php
		$saldo=$saldo+ $row[0]['vmonto'];
		$factura=$factura+ $row[0]['total'];
		$iva=$iva+ $row[0]['iva'];
	}
	echo $this->Form->input('Retencione.totales', array('type' =>'hidden','label' =>'','value'=>$t));
	?>
	</tbody>
	<tfoot>
		<tr>
			<td align="right" colspan="6"><label>Total:</label></td>
			<td align="right">&nbsp;<?php echo number_format(sprintf("%01.2f", $factura), 2, ',', '.');?></td>
			<td align="right">&nbsp;<?php echo number_format(sprintf("%01.2f", $saldo), 2, ',', '.');?></td>
			<td align="right">&nbsp;<?php echo number_format(sprintf("%01.2f", $iva), 2, ',', '.');?></td>
			<td>&nbsp;</td>
		</tr>
	</tfoot>

</table>
</div>
<script language="javascript" type="text/javascript">
  	
  $(function () {
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue'
    });

  });
</script>
