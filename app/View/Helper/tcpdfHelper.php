<?php
App::import('Vendor','XTCPDF' ,array('file'=>'tcpdf/xtcpdf.php'));

class TcpdfHelper  extends AppHelper
{	
	public $helpers=array('App');
	public $core;
	
    function TcpdfHelper() {
        $this->core = new XTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
    }
	
 //Page footer
	/*function Footer()
    	{
		//Position at 1.5 cm from bottom
		$this->core->SetY(-45);
		$this->core->SetFont('Times','I',10);
		$this->core->setCellHeightRatio(1.1);
		//$this->core->Cell(0,5,'_______________________________________________________________________________________',0,0,'C');
		$this->core->Ln();
		$slogan=' &quot;LA UNIVERSIDAD TÉCNICA DEL ESTADO VENEZOLANO&quot; ';
		$this->core->MultiCell(0,4,html_entity_decode($this->slogan),0,'C');						
		//Times Roman italic 8
		$this->core->SetFont('Times','I',8);
		//Page number
		$this->core->Ln(2);
		$this->core->Cell(0,2,'Pag. '.$this->core->PageNo().'/'.$this->core->getNumPages(),0,0,'C');
		setlocale(LC_ALL, 'es_ES');
		//$this->core->Ln(2);
		//$this->core->Cell(0,2,' '.date("F j, Y, g:i a"),0,0,'R');
	} 	*/

	function obtener_pie($slogan="  "){
		$this->slogan= $slogan;
	}

	public function texto($contenido){
		$this->core->SetFont('helvetica','',12);
		//$this->core->Ln(10);
		//$this->core->setCellHeightRatio(1.5);
		//$this->core->Cell(10);
		//MultiCell 	($w,$h,$txt,$border = 0,	$align = 'J',$fill = false,	$ln = 1,$x = '',$y = '',$reseth = true,$stretch = 0,$ishtml = false,	  	$autopadding = true,$maxh = 0,$valign = 'T',$fitcell = false ) 
		//$this->core->writeHTMLCell(180, 5, '', '', $contenido, 0, 1, 0, true, 'J');
		//$this->core->writeHTML($contenido, 0, 1, 0, true, 'J');
		// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true);
		$this->core->writeHTMLCell(180,180, '', '', $contenido, 0, 0, 0, true, 'J', true);
		//$this->core->writeHTML($contenido, true, false, false, false, 'J');
		//writeHTML	
	}

function constancia($persona,$cargo,$ubicacion,$sueldo,$calculos,$text,$verificado)
	{
		$this->core->SetFont('Times','BI',12);
 		$this->core->Cell(39,0,$persona['Persona']['codigo'],0,1,'R');
		$this->core->Ln();
		$this->core->SetLeftMargin(15);	
		$this->core->SetFont('Times','BI',14);
 		$this->core->Cell(180,4,'CONSTANCIA DE TRABAJO ',0,1,'C');
		$this->core->SetFillColor(0,128,192);
		$this->core->SetTextColor(255);
		$this->core->SetDrawColor(0,64,128);
		$this->core->SetLineWidth(0.05);
		$this->core->SetFont('Times','I',12);
		
		//Restauración de colores y fuentes
		$this->core->SetFillColor(224,235,255);
		$this->core->SetTextColor(0);
		$this->core->SetFont('Times','I',12);
		$this->core->Ln(10);
		$cont=1;$asig=0;$deduc=0;
		setlocale(LC_ALL, 'es_ES');
		$fecha=date('d  F  Y',strtotime($persona['Persona']['PERSO001FECH_ING']));
		$contenido='<i><i>'.$text.'</i></i>';	
		$this->core->SetFont('times','',12);
		
		//setCellHeightRatio define el interlineado del texto
		$this->core->setCellHeightRatio(1.5);
		$this->core->Cell(10);
		$this->core->writeHTMLCell(160, 10, '', '', $contenido, 0, 1, 0, true, 'J');
							
		$this->core->Ln(15);
		$this->core->SetFont('Times','I',12);		
		//$this->core->MultiCell(180,0,'Atentamente',0,'C');			
		if (count($calculos)>0){
			$this->core->Ln(14);						
		}else{
			$this->core->Ln(20);
		}

		//$this->core->MultiCell(180,5,'___________________________________________',0,'C');			
		$this->core->SetFont('times', 'BI', 12);
		$jefa='Lcda. Blanca Olivera';	
		$this->core->MultiCell(180,8,html_entity_decode($jefa),0,'C');			
		$dpto='Jefa de la Unidad Nacional de Recursos Humanos';		
		$this->core->MultiCell(180,7,html_entity_decode($dpto),0,'C');
		if (count($calculos)>0){
			$this->core->Ln(2);						
		}else{
				$this->core->Ln(20);
			 }	
		if (!empty($verificado))
		{	
			$this->core->SetFont('times', '', 8);
			$this->core->MultiCell(40,4,html_entity_decode($verificado),0,'C');
			$this->core->setCellHeightRatio(0.8); 
			$this->core->MultiCell(40,4,date("d/m/Y"),0,'C');
		}							
		
		$this->core->Ln();
	}


function constancia_hist($persona,$verificado)
	{
		$this->core->SetFont('Times','BI',12);
 		$this->core->Cell(39,0,$persona[0]['Historial']['codigodoc'],0,1,'R');
		$this->core->Ln();
		$this->core->SetLeftMargin(15);	
		$this->core->SetFont('Times','BI',14);
 		$this->core->Cell(180,4,'CONSTANCIA DE TRABAJO ',0,1,'C');
		$this->core->SetFillColor(0,128,192);
		$this->core->SetTextColor(255);
		$this->core->SetDrawColor(0,64,128);
		$this->core->SetLineWidth(0.05);
		$this->core->SetFont('Times','I',12);
		
		//Restauración de colores y fuentes
		$this->core->SetFillColor(224,235,255);
		$this->core->SetTextColor(0);
		$this->core->SetFont('Times','I',12);
		$this->core->Ln(10);
		setlocale(LC_ALL, 'es_ES');
		
		foreach ($persona as $p)
		{
			if (!empty($p['Historial']['textoconstancia']))
			{
				$contenido='<i><i>'.$p['Historial']['textoconstancia'].'</i></i>';	
			}
		}
		$this->core->SetFont('times','',12);
		
		//setCellHeightRatio define el interlineado del texto
		$this->core->setCellHeightRatio(1.5);
		$this->core->Cell(10);
		$this->core->writeHTMLCell(160, 10, '', '', $contenido, 0, 1, 0, true, 'J');

		$this->core->Ln(10);
		$this->core->SetFont('Times','I',12);		
		//$this->core->MultiCell(180,0,'Atentamente',0,'C');	
		$this->core->Ln(10);
		//$this->core->MultiCell(180,5,'___________________________________________',0,'C');			
		$this->core->SetFont('times', 'BI', 12);
		$jefa='Lcda. Blanca Olivera';	
		$this->core->MultiCell(180,8,html_entity_decode($jefa),0,'C');			
		$dpto='Jefa de la Unidad Nacional de Recursos Humanos';		
		$this->core->MultiCell(180,7,html_entity_decode($dpto),0,'C');
		
		if (!empty($verificado[0]['Solicitude']['jefe']))
		{	
			$this->core->SetFont('times', '', 8);
			$this->core->MultiCell(40,2,html_entity_decode($verificado[0]['Solicitude']['jefe']),0,'C');
			$this->core->setCellHeightRatio(0.8); 
			$this->core->MultiCell(40,2,date("d/m/Y"),0,'C');
		}							
		$this->core->Ln();	
								
	}

function tcpdfOutput ($name = 'page.pdf', $destination = 'I') {
		// I: send the file inline to the browser. The plug-in is used if available. 
		//    The name given by name is used when one selects the "Save as" option on the link generating the PDF.
		// D: send to the browser and force a file download with the name given by name.
		// F: save to a local file with the name given by name.
		// S: return the document as a string. name is ignored.
		return $this->core->Output($name, $destination);
	}
}
?>
