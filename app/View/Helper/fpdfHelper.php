<?php 
App::import('Vendor','FPDF' ,array('file'=>'fpdf/fpdf.php')); 
if (!defined('PARAGRAPH_STRING')) define('PARAGRAPH_STRING', '~~~');
App::uses('AppHelper', 'View/Helper');
//class FpdfHelper extends FPDF {
class fpdfHelper extends AppHelper{
   // var $title; 
	public $helpers=array('App');
	public $fpdf = null;
	
	/*function __construct($w=null, $orientation='P',$unit='mm',$format='A4')
	{
		parent::__construct($w,'P',$unit,$format);
	}*/
	
	//function SetTitle($w='')
	function SetTitulo($w='')
	{
	    //Set the array of column widths
	    $this->title=$w;
	}

	function SetLine1($w)
	{
	    //Set the array of column widths
	    $this->line1=$w;
	}

	function SetLine2($w)
	{
	    //Set the array of column widths
	    $this->line2=$w;
	}
	
	function SetLine3($w)
	{
	    //Set the array of column widths
	    $this->line3=$w;
	}

	function SetLine4($w)
	{
	    //Set the array of column widths
	    $this->line4=$w;
	}				

	function SetLogoIzq($w,$margenIzq=10,$margenDer=8,$ancho=20)
	{
	    //Set the array of column widths
	    $this->logoIzq=$w;
  	    $this->logoIzqmargenIzq=$margenIzq;
	    $this->logoIzqmargenDer=$margenDer;
	    $this->logoIzqancho=$ancho;

	}

	function SetLogoDer($w,$margenIzq=165,$margenDer=8,$ancho=20)
	{
	    //Set the array of column widths
	    $this->logoDer=$w;
  	    $this->logoDermargenIzq=$margenIzq;
	    $this->logoDermargenDer=$margenDer;
	    $this->logoDerancho=$ancho;
	}	

    /**
    * Allows you to change the defaults set in the FPDF constructor
    *
    * @param string $orientation page orientation values: P, Portrait, L, or Landscape    (default is P)
    * @param string $unit values: pt (point 1/72 of an inch), mm, cm, in. Default is mm
    * @param string $format values: A3, A4, A5, Letter, Legal or a two element array with the width and height in unit given in $unit
    */
    function setup ($orientacion='P',$unit='mm',$format='letter') {
        $this->fpdf = new FPDF(null,$orientacion, $unit, $format); 
        //$this->fpdf(null,$orientacion, $unit, $format); 
        //$this->FPDF(null,$orientation, $unit, $format); 
    }
    
    /**
    * Allows you to control how the pdf is returned to the user, most of the time in CakePHP you probably want the string
    *
    * @param string $name name of the file.
    * @param string $destination where to send the document values: I, D, F, S
    * @return string if the $destination is S
    */
    function fpdfOutput ($name = 'page.pdf', $destination = 'S') {
        // I: send the file inline to the browser. The plug-in is used if available. 
        //    The name given by name is used when one selects the "Save as" option on the link generating the PDF.
        // D: send to the browser and force a file download with the name given by name.
        // F: save to a local file with the name given by name.
        // S: return the document as a string. name is ignored.
        return $this->fpdf->Output($name, $destination);
    }

    function SetFontHeader($font='Arial',$style='IB',$size=10)
	{
	    //Set the array of column widths
	    $this->fontH=$font;
	    $this->styleH=$style;
	    $this->sizeH=$size;
	}	
	
	function Header()
    {
        //Logo
        //$this->Image(WWW_ROOT.DS.'img/logo/'. $this->logoIzq,10,8,20);
        //Logo
        $this->fpdf->Image(WWW_ROOT.DS.'img/logo/'. $this->logoIzq,8,4,20);   	        	    
		//$this->fpdf->Image(WWW_ROOT.DS.$this->logo_Izq,10,9,20);        	        	    
        //$this->Image(WWW_ROOT.DS.'img/logo/'. $this->logoDer,$this->logoDermargenIzq,$this->logoDermargenDer,$this->logoDerancho);  

		//$this->Image(WWW_ROOT.DS.'img/logosangrado.png',60,90,150);
		//$this->fpdf->Image(WWW_ROOT.DS.'img/blanco.jpg',191,90,100);
        // you can use jpeg or pngs see the manual for fpdf for more info
        //Arial bold 15
		if(!isset($this->fontH)){
			$this->fpdf->SetFont('Arial','IB',10);
		}else{
			$this->fpdf->SetFont($this->fontH,$this->styleH,$this->sizeH);
		}
		$this->fpdf->SetX(20);
		$this->fpdf->Cell(0,0.5,'            '.$this->line1,0,1,'L');
		$this->fpdf->Ln(4);
		$this->fpdf->SetX(20);
		$this->fpdf->Cell(0,0.5,'            '.$this->line2,0,1,'L');
		$this->fpdf->Ln(4);
		$this->fpdf->SetX(20);
		$this->fpdf->Cell(0,0.5,'            '.$this->line3,0,1,'L');
		$this->fpdf->Ln(4);
		$this->fpdf->SetX(20);
		$this->fpdf->Cell(0,0.5,'            '.$this->line4,0,1,'L');

	//	$this->Ln(12);
		$this->fpdf->Ln(6);
		$this->fpdf->SetFont('Arial','I',9);
		$this->fpdf->Cell(0,0.5,''.$this->title,0,1,'C');
        //Move to the right
      //  $this->Cell(80);
        //Title
      //  $this->Cell(30,10,$this->title,1,0,'C');
        //Line break
        $this->fpdf->Ln(2);
    }

	function obtener_pie($slogan=" "){
		$this->piepag=$slogan;
	}
    //Page footer
	function Footer()
    	{
	//Position at 1.5 cm from bottom
		
		$this->fpdf->SetY(-45);

		$this->fpdf->SetFont('Arial','BI',11);
		//$this->Cell(0,5,'_______________________________________________________________________________________',0,0,'C');
		$this->fpdf->Ln();
		$slogan=$this->piepag;
		$this->fpdf->MultiCell(0,5,html_entity_decode($slogan),0,'C');			
		//Arial italic 8
		$this->fpdf->SetFont('Arial','I',8);
		//Page number
		$this->fpdf->Ln();
		$this->fpdf->Cell(0,10,'Pag. '.$this->fpdf->PageNo().'/{nb}',0,0,'C');
		setlocale(LC_ALL, 'es_ES');
		$this->fpdf->Cell(0,10,' '.date("F j, Y, g:i a"),0,0,'R');
	} 	
/*    function Footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial','I',8);
        //Page number
        $this->Cell(0,10,'Pag. '.$this->PageNo().'/{nb} '.date("d-m-Y H:m:s"),0,0,'C');
    } */
	
	function basicTable($header,$data)
    {
	  //Colores, ancho de l�nea y fuente en negrita
		$this->SetFillColor(0,128,192);
		$this->SetTextColor(255);
		$this->SetDrawColor(0,64,128);
		$this->SetLineWidth(0.05);
		$this->SetFont('Arial','B');
		//Restauraci�n de colores y fuentes
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('Arial');
		//Header
        foreach($header as $col)
            $this->Cell(40,7,$col,1);
        $this->Ln();
        //Data
        foreach($data as $row) {
            foreach($row as $col) {
                $this->Cell(40,6,$col,1);
            }
            $this->Ln();
        }
    } 
	
	function FancyTable2($header,$result)
	{
		//Colores, ancho de l�nea y fuente en negrita
		$this->SetFillColor(0,128,192);
		$this->SetTextColor(255);
		$this->SetDrawColor(0,64,128);
		$this->SetLineWidth(0.05);
		$this->SetFont('','B');
		//Cabecera
		$w=array(1,2,7,7);
		$this->Cell(1);
		for($i=0;$i<count($header);$i++)
			$this->Cell($w[$i],0.7,$header[$i],1,0,'C',1);
		$this->Ln();
		//Restauraci�n de colores y fuentes
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(0);
		$this->SetFont('');
		//Datos
		$fill=0; $i=0;
		foreach($result as $row)
		{
			$i=$i+1;
			$this->Cell(1);
			$this->Cell($w[0],0.5,$i,'LR',0,'L',$fill);
			$this->Cell($w[1],0.5,$row[0],'LR',0,'L',$fill);
			$this->Cell($w[2],0.5,$row[1],'LR',0,'L',$fill);
			$this->Cell($w[3],0.5,$row[2],'LR',0,'L',$fill);
			$this->Ln();
			$fill=!$fill;
		}
		$this->Cell(1);
		$this->Cell(array_sum($w),0,'','T');
	} 
	

	function NbLines($w,$txt)
	{
	    //Computes the number of lines a MultiCell of width w will take
	    //$cw=&$this->CurrentFont['cw'];
	    $cw=$this->fpdf->CurrentFont['cw'];
	    if($w==0)
	        $w=$this->fpdf->w-$this->fpdf->rMargin-$this->fpdf->x;
	    $wmax=($w-2*$this->fpdf->cMargin)*1000/$this->fpdf->FontSize;
	    $s=str_replace("\r",'',$txt);
	    $nb=strlen($s);
	    if($nb>0 and $s[$nb-1]=="\n")
	        $nb--;
	    $sep=-1;
	    $i=0;
	    $j=0;
	    $l=0;
	    $nl=1;
	    while($i<$nb)
	    {
	        $c=$s[$i];
	        if($c=="\n")
	        {
	            $i++;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	            continue;
	        }
	        if($c==' ')
	            $sep=$i;
	        $l+=$cw[$c];
	        if($l>$wmax)
	        {
	            if($sep==-1)
	            {
	                if($i==$j)
	                    $i++;
	            }
	            else
	                $i=$sep+1;
	            $sep=-1;
	            $j=$i;
	            $l=0;
	            $nl++;
	        }
	        else
	            $i++;
	    }
	    return $nl;
	}
	 	
	function SetWidths($w)
	{
	    //Set the array of column widths
	    $this->widths=$w;
	}

	function SetAligns($a)
	{
	    //Set the array of column widths
	    $this->aligns=$a;
	}

	function CheckPageBreak($h)
	{
	    //If the height h would cause an overflow, add a new page immediately
	    if($this->fpdf->GetY()+$h>$this->fpdf->PageBreakTrigger)
	        $this->fpdf->AddPage($this->fpdf->CurOrientation);
	} 	
	 
	function Row($data,$fill)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<count($data);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    for($i=0;$i<count($data);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
	        //Save the current position
	        $x=$this->fpdf->GetX();
	        $y=$this->fpdf->GetY();
	        //Draw the border
//			$this->SetDrawColor(54,54,54);
			$this->fpdf->SetFillColor(255,255,255);
	    	//$this->SetFillColor(224,235,255);// Se parametriza, en formato RGB, el color a mostrar en el fondo de las celdas de las tablas 
//			$this->SetFillColor(204,204,204);
			//$this->SetDrawColor(0,0,0);
			$this->fpdf->SetDrawColor(204,204,204);// Se parametriza, en formato RGB, el color a mostrar, las lineas de la tabla
			//if($fill==1){
				$this->fpdf->Rect($x,$y,$w,$h,'FD');//F => permites colorear el fondo. D => permite colorear las lineas
			//}else{
			//	$this->Rect($x,$y,$w,$h,$fill);
			//}
	        
	        //Print the text
	        $this->fpdf->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->fpdf->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->fpdf->Ln($h);
	}

	function RowMarcado($data,$fill)
	{
	    //Calculate the height of the row
	    $nb=0;
	    for($i=0;$i<(count($data)-1);$i++)
	        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
	    $h=5*$nb;
	    //Issue a page break first if needed
	    $this->CheckPageBreak($h);
	    //Draw the cells of the row
	    $ultimo = count($data)-1;
	    /*print_r($data);
	    echo ' UL:'.$ultimo;
	    die();*/
	    for($i=0;$i<(count($data)-1);$i++)
	    {
	        $w=$this->widths[$i];
	        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
	        //Save the current position
	        $x=$this->fpdf->GetX();
	        $y=$this->fpdf->GetY();
	        //Draw the border
//			$this->SetDrawColor(54,54,54);rgb(248, 215, 30)rgb(252, 245, 131)
			if ($data[$ultimo]=='SI'){
			
			$this->fpdf->SetFillColor(252, 245, 131);
			}else{
			$this->fpdf->SetFillColor(255,255,255);
			}
	    	//$this->SetFillColor(224,235,255);// Se parametriza, en formato RGB, el color a mostrar en el fondo de las celdas de las tablas 
//			$this->SetFillColor(204,204,204);
			//$this->SetDrawColor(0,0,0);
			$this->fpdf->SetDrawColor(204,204,204);// Se parametriza, en formato RGB, el color a mostrar, las lineas de la tabla
			//if($fill==1){
				$this->fpdf->Rect($x,$y,$w,$h,'FD');//F => permites colorear el fondo. D => permite colorear las lineas
			//}else{
			//	$this->Rect($x,$y,$w,$h,$fill);
			//}
	        
	        //Print the text
	        $this->fpdf->MultiCell($w,5,$data[$i],0,$a);
	        //Put the position to the right of the cell
	        $this->fpdf->SetXY($x+$w,$y);
	    }
	    //Go to the next line
	    $this->fpdf->Ln($h);
	}

	/*public function AliasNbPages(){
		 $this->fpdf->AliasNbPages(); 
	}

	public function AddPage(){
		 $this->fpdf->AddPage(); 
	}*/
/*
	$header => Es un arreglo de la Cabecera a imprimir
	$data => Los datos a mostrar en la tabla
	$w => Ancho de las columnas
	$subenc => Titulo de la Tabla
	$espacAbajo => Espacio que se quiere dejar hacia abajo despues de imprimir la tabla
	$valTitulo => Config del titulo array('pos'=>'C','border'=>0,'EstiloT'=>'B','EstiloD'=>'')
*/	
	function FancyTable($header=array(),$data,$w,$subenc=NULL,$espacAbajo=0,$alineacion=array(),$valTitulo=array('pos'=>'C','border'=>0,'EstiloT'=>'B','EstiloD'=>'','tamTitulo'=>190,'tamLetra'=>8,'marcado'=>'N'))
	{
		if(!isset($valTitulo['pos'])){$valTitulo['pos']='C';}
		if(!isset($valTitulo['border'])){$valTitulo['border']=0;}
		if(!isset($valTitulo['EstiloT'])){$valTitulo['EstiloT']='B';}
		if(!isset($valTitulo['EstiloD'])){$valTitulo['EstiloD']='';}
		if(!isset($valTitulo['tamLetra'])){$valTitulo['tamLetra']=8;}
		if(!isset($valTitulo['tamTitulo'])){$valTitulo['tamTitulo']=190;}
	    //Colors, line width and bold font
//	    $this->SetFillColor(65,105,225);
	    $this->fpdf->SetFillColor(255,255,255);
	    $this->fpdf->SetTextColor(255);
	    $this->fpdf->SetDrawColor(54,54,54);
	    $this->fpdf->SetLineWidth(.3);
//	    $this->SetFont('','B');
		$this->fpdf->SetFont('Arial',$valTitulo['EstiloT'],$valTitulo['tamLetra']);
	    //Header
	    //$w=array(40,35,40,45);
		if($subenc!=NULL){

		    $this->fpdf->SetTextColor(0);	
//		    $this->SetDrawColor(65,105,225);	
//		    $this->SetDrawColor(255,255,255);	
			$this->fpdf->SetDrawColor(204,204,204);	    		
		    $this->fpdf->Cell($valTitulo['tamTitulo'],7,$subenc,$valTitulo['border'],0,$valTitulo['pos'],1);
		    $this->fpdf->Ln();	

		}
/* Recordar colocar una opcion para desactivar impresion del header */
	    if (count($header)>0){
			$this->fpdf->SetTextColor(0);		
		    $this->fpdf->SetDrawColor(204,204,204);			 		     
		    for($i=0;$i<count($header);$i++)
			$this->fpdf->Cell($w[$i],7,$header[$i],1,0,'C',1);
		    $this->fpdf->Ln();
	    }	
	    	    $this->fpdf->SetDrawColor(54,54,54);		
/*Fin de Recordatorio colocar una opcion para desactivar impresion del header */		 	
	    //Color and font restoration
	    //$this->SetFillColor(224,235,255);
	    $this->fpdf->SetFillColor(255,255,255);
	    $this->fpdf->SetTextColor(0);
	    $this->fpdf->SetFont('Arial',$valTitulo['EstiloD'],$valTitulo['tamLetra']);
	    //$this->SetFont('');
	    //Data
	    $fill=0;$y=30;
		$this->SetWidths($w);
		if (count($alineacion)>0)
			$this->SetAligns($alineacion);

		foreach($data as $row){
			//$fill=!$fill;
			if(isset($valTitulo['marcado'])){
			if ($valTitulo['marcado']=='N'){
			$this->Row($row,$fill);
			}else{
			$this->RowMarcado($row,$fill);
			}
			}else{
				$this->Row($row,$fill);
			}		
			
		}
		    
        $this->fpdf->Cell(array_sum($w),0,'','T');
		$this->fpdf->Ln($espacAbajo);
	}
   
}
?>
