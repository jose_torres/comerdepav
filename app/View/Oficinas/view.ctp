<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
?>
<div class="unidades view">
<fieldset id="personal" >
<legend class="info"><h2> MOSTRAR DATOS DE LA OFICINA </h2></legend>
	<table>
	<tr>
		<td><label>ID:</label></td>
		<td><?php echo $unidade['Oficina']['id']; ?> </td>
	</tr>
	<tr>
		<td><label>Nombre:</label></td>
		<td><?php echo $unidade['Oficina']['descripcion']; ?> </td>
	</tr>
	<tr>
		<td><label>Siglas:</label></td>
		<td><?php echo $unidade['Oficina']['siglas']; ?> </td>
	</tr>
	<tr>
		<td><label>Creado:</label></td>
		<td><?php echo $unidade['Oficina']['created'] ?> </td>
	</tr>
	<tr>
		<td><label>Modificado:</label></td>
		<td><?php echo $unidade['Oficina']['modified'] ?> </td>
	</tr>
</table>
</fieldset>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	//echo $$this->Html->link( $$this->Html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/unidades/edit/".$unidade['Oficina']['id'], array('escape'=>false), null);
	//echo '&nbsp;&nbsp;&nbsp;';
	//echo $$this->Html->link( $$this->Html->image("img_acciones/book_blue_delete.png", array("alt" => "Eliminar Registro", "title"=>"Eliminar Registro")) ,"/unidades/delete/".$unidade['Oficina']['id'], array('escape'=>false), '¿Esta seguro de eliminar '.$unidade['Oficina']['nombre'].'?');
	//echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/oficinas", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
<div class="related">
	
	<?php if (!empty($unidade['Funcione'])):?>
	<h3><?php __(' Funciones Relacionadas');?></h3>
	<table cellpadding = "0" cellspacing = "0" width="90%" class="chrome">
	<tr>
		<th><?php __('Nro'); ?></th>
		<th><?php __('Id'); ?></th>
		<th><?php __('Nombre'); ?></th>
		<th><?php __('Direccion'); ?></th>
		<th><?php __('Created'); ?></th>
		<th><?php __('Modified'); ?></th>
		<th class="actions"><?php __('Acciones');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($unidade['Funcione'] as $articulo):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $i;?></td>
			<td><?php echo $articulo['id'];?></td>
			<td><?php echo $articulo['nombre'];?></td>
			<td><?php echo $articulo['direccion'];?></td>
			<td><?php echo $articulo['created'];?></td>
			<td><?php echo $articulo['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View', true), array('controller' => 'funciones', 'action' => 'view', $articulo['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

</div>
