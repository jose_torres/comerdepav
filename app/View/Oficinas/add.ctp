<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	function validar(){
		if (document.getElementById("OficinaDescripcion").value=="")
		{
			alert ('Por favor escriba una Descripcion de la Oficina');
			return false;
		}else{
			document.frm.submit();	
		}	
	}		
</script>
<div class="unidades form">
<?php echo $this->Form->create('Oficina',array('url'=>'add','name' => 'frm','id'=>'frm','class'=>"pure-form pure-form-aligned"));?>
	<fieldset>
 		<legend class="info"><h2>REGISTRAR UNA OFICINA</h2></legend>
	<?php
		echo '<div class="pure-control-group">';
		echo $this->Form->input('descripcion',array('label'=>'Descripci&oacute;n:','size' =>'40','type'=>'text','div'=>false));
		echo '</div>';
		echo '<div class="pure-control-group">';			
		echo $this->Form->input('siglas',array('label'=>'Siglas:','size' =>'40','type'=>'text','div'=>false));
		echo '</div>';			
	$i=1;
	$var[0]='No tiene';	
	foreach ($data_perfil as $row) {
		$item = $row['Oficina'];	
		$var[$item['id']]=$item['descripcion'];
		$i=$i+1;
	}
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Oficina.empresa_id',array('label'=>'Empresa:','size' =>'1','type'=>'select','div'=>false,'options'=>$empresa));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Oficina.familia', array('size'=>'40','label'=>'Nivel de Oficina:','options'=>$var,'type'=>'select','size'=>'1',"onchange"=>"modificar('hijos','buscarhijos/',this.form.OficinaFamilia.value);"));
	echo '</div>';	 	
	echo '<div id="hijos" class="pure-control-group">';	
	$i=1;$var2=array(); $var2[0]='No tiene';
	foreach ($data_oficina as $row) {
	$item = $row['Oficina'];	
	$var2[$item['id']]=$item['descripcion'];
	$i=$i+1;
	}
	echo $this->Form->input('Oficina.parent_id', array('size'=>'40','label'=>'Oficina Padre:','options'=>$var2,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1'));
	echo '</div>';	
	?>	
	<br>
	</fieldset>
<?php echo $this->Form->end();?>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">	
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"validar()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/oficinas", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>
</div>
