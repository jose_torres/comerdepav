<?php
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('datatables/dataTables.bootstrap.css');
echo $this->Html->css('select2/select2.min.css');
echo $this->Html->script(array('funciones.js','View/Conretencionventadets/declarar'));
//print_r($declaracion);
?>
<div class="span11">
	<div class="well">
<?php echo $this->Form->create('Conretencionventadet', array('url' => 'asociar','name' => 'frm','id'=>'frm','role'=>"form",'class'=>"form-horizontal")); ?>
	<fieldset>
	<legend>MES Y A&Ntilde;O A AFECTAR:</legend>
<?php
	echo $this->Form->input('Retgenventa.id',array('label'=>false,'type'=>'hidden','div'=>false,'value'=>$declaracion['Retgenventa']['id']));
	echo $this->Form->input('Retgenventa.estatus',array('label'=>false,'type'=>'hidden','div'=>false,'value'=>$declaracion['Retgenventa']['estatus']));
	
	echo '<div class="form-group">';
	echo $this->Html->tag('label', 'Mes:', array('class' => 'col-xs-1 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retgenventa.mes',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>$declaracion['Retgenventa']['mes']));
	echo '</div>';
	
	echo $this->Html->tag('label', 'A&ntilde;o:', array('class' => 'col-xs-1 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Retgenventa.anio',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>$declaracion['Retgenventa']['anio']));
	echo '</div>';	
	echo '</div>';
?>
	</fieldset>
<!-- -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><h4>&nbsp;<a href="#tab_1" data-toggle="tab">1.&nbsp;RETENCIONES SIN ASOCIAR</a>&nbsp;</h4></li>
              <li><h4>&nbsp;<a href="#tab_2" data-toggle="tab">2.&nbsp;RETENCIONES ASOCIADAS</a>&nbsp;</h4></li>         
              <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                	<fieldset>
<!--	<legend>BUSCAR RETENCIONES SIN ASOCIAR:</legend> -->
<?php
echo '<div class="form-group">';		
$fecha2=date("d-m-Y");
//$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
$fecha=date("d-m-Y",strtotime($primeraFecha['Conretencionventadet']['fechaemision']));
echo $this->Html->tag('label', 'Rango de Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="input-daterange input-group col-xs-3" id="datepicker">';
echo $this->Form->input('Reporte.desde',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true));
echo '<span class="input-group-addon">al</span>';
echo $this->Form->input('Reporte.hasta',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Hasta",'value'=>$fecha2,'readonly'=>true)); 
echo '</div>';
echo '</div>';
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Buscar:', array('class' => 'col-xs-2 control-label','for'=>"cliente_id"));
echo '<div class="col-xs-8 input-group">';
echo $this->Form->input('Reporte.consulta',array('type' => 'text','div'=>false,'label'=>false,'placeholder'=>"Ingresa el nombre del Cliente o Número de Retención o Número de Factura",'class'=>"form-control"));
?>
<div class="input-group-btn">
<button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarlistado','','fechadesde='+document.frm.ReporteDesde.value+'&fechahasta='+document.frm.ReporteHasta.value+'&consulta='+document.frm.ReporteConsulta.value+'&opcion=no_asociado')">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
</div>
<?php
echo '</div>';	

echo '</div>';
?>	
<div id="buscar" class="unidades index">
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Retenciones Encontradas</h3>
	</div>	
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover" id="retencion">
<caption>  <strong>Retenciones</strong></caption>
<thead>
    <tr>
        <th>Fecha</th>
        <th>Nro</th>
        <th>Factura</th>
        <th>Cliente</th>
        <th>Iva</th>
        <th>IVA Retenido</th>
        <th>Sel.</th>
    </tr>
</thead>
<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($noasociado as $row): $registro = $row[0]; ?>
    <?php $x=$x+1; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven" id='td_id'>
	<? } else { ?>
	<tr id='td_id'>
	<? } ?>
        <td><?php echo date("d-m-Y",strtotime($registro['fecha']));?></td>
        <td>&nbsp;<?php echo $registro['numero'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['documento'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $row['Cliente']['nombre'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['montoretenido'];?>&nbsp;</td>
        <td align="right">
            <?php
            $total_monto=$total_monto+$registro['montoretenido'];
            echo $this->Form->input('Cheque.monto'.$x, array('size' => 15,'type' =>'text','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",$registro['montoretenido']),'readonly'=>true));
            echo $this->Form->input('Cheque.montosel'.$x, array('size' => 15,'type' =>'hidden','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",0)));
            ?>
        </td>
        <td>&nbsp;<?php echo $this->Form->input('Seleccion'.$x.'.chequear', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'onclick'=>"calcularCheques('Seleccion".$x."Chequear','ChequeMontosel".$x."','ChequeMonto".$x."')"));
		echo $this->Form->input('Valor.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$registro['id']));
		echo $this->Form->input('Valor.'.$x.'.codsucursal', array('type' =>'hidden','label' =>'','value'=>$registro['codsucursal']));		
		?></td>
     </tr>
    <?php endforeach; ?>
</tbody>
<tfoot>
    <tr>        
        <th>&nbsp;</th>
        <th colspan="2" align='right' ><strong>Total Monto en Cheque:</strong></th>
        <th><div align='right' id='total_monto_cheque'><?php echo number_format(sprintf("%01.2f", $total_monto), 2, '.', '.') ;
?></div> </th>
		<th  align='right' ><strong>Total a Depositar:</strong></th>
	<th>
	<?php echo $this->Form->input('Cheque.totales', array('type'=>'hidden','value'=>$x)); ?>
	<div align='right' id='total_monto_che_dep'><?php
	echo number_format(sprintf("%01.2f", 0), 2, '.', '.') ;
?></div> </th>	
	<th>&nbsp;</th>
</tfoot>
</table>
</div>
	</div>	
	</fieldset>
<?php
$options = array('label' => 'Asociar','class' => "btn btn-primary",'escape'=>false,
);
echo $this->Form->end($options);
?>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">

<?php echo $this->Form->create('Conretencionventadet', array('url' => 'desasociar','name' => 'frmdes','id'=>'frmdes','role'=>"form",'class'=>"form-horizontal")); ?>
	<fieldset>

<?php
	echo $this->Form->input('Retgenventa.id',array('label'=>false,'type'=>'hidden','div'=>false,'value'=>$declaracion['Retgenventa']['id']));
	echo $this->Form->input('Retgenventa.estatus',array('label'=>false,'type'=>'hidden','div'=>false,'value'=>$declaracion['Retgenventa']['estatus']));
	echo $this->Form->input('Retgenventa.mes',array('label'=>'','type'=>'hidden','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>$declaracion['Retgenventa']['mes']));
	echo $this->Form->input('Retgenventa.anio',array('label'=>'','type'=>'hidden','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>$declaracion['Retgenventa']['anio']));
?>
               	<fieldset>
<!--	<legend>BUSCAR RETENCIONES SIN ASOCIAR:</legend> -->
<?php
echo '<div class="form-group">';		
$fecha2=date("d-m-Y");
//$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
$fecha=date("d-m-Y",strtotime($primeraFechaAsociado['Conretencionventa']['fechaemision']));
echo $this->Html->tag('label', 'Rango de Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="input-daterange input-group col-xs-3" id="datepicker">';
echo $this->Form->input('Reporte.desde',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true));
echo '<span class="input-group-addon">al</span>';
echo $this->Form->input('Reporte.hasta',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Hasta",'value'=>$fecha2,'readonly'=>true)); 
echo '</div>';
echo '</div>';
echo '<div class="form-group">';
echo $this->Html->tag('label', 'Buscar:', array('class' => 'col-xs-2 control-label','for'=>"cliente_id"));
echo '<div class="col-xs-8 input-group">';
echo $this->Form->input('Reporte.consulta',array('type' => 'text','div'=>false,'label'=>false,'placeholder'=>"Ingresa el nombre del Cliente o Número de Retención o Número de Factura",'class'=>"form-control"));
?>
<div class="input-group-btn">
<button type="button" class="btn btn-info" onclick="actualizar('buscar2','buscarlistado','','fechadesde='+document.frmdes.ReporteDesde.value+'&fechahasta='+document.frmdes.ReporteHasta.value+'&consulta='+document.frmdes.ReporteConsulta.value+'&opcion=asociado+&nombretabla=retencion2&nombreformulario=frmdes')">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
</div>
<?php
echo '</div>';	

echo '</div>';
?>	
<div id="buscar2" class="unidades index">
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Retenciones Encontradas</h3>
	</div>	
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover" id="retencion2">
<caption>  <strong>Retenciones</strong></caption>
<thead>
    <tr>
        <th>Fecha</th>
        <th>Nro</th>
        <th>Factura</th>
        <th>Cliente</th>
        <th>Iva</th>
        <th>IVA Retenido</th>
        <th>Sel.</th>
    </tr>
</thead>
<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_monto=0;
    foreach ($noasociado as $row): $registro = $row[0]; ?>
    <?php $x=$x+1; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven" id='td_id'>
	<? } else { ?>
	<tr id='td_id'>
	<? } ?>
        <td><?php echo date("d-m-Y",strtotime($registro['fecha']));?></td>
        <td>&nbsp;<?php echo $registro['numero'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['documento'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $row['Cliente']['nombre'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $registro['montoretenido'];?>&nbsp;</td>
        <td align="right">
            <?php
            $total_monto=$total_monto+$registro['montoretenido'];
            echo $this->Form->input('Cheque.monto'.$x, array('size' => 15,'type' =>'text','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",$registro['montoretenido']),'readonly'=>true));
            echo $this->Form->input('Cheque.montosel'.$x, array('size' => 15,'type' =>'hidden','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",0)));
            ?>
        </td>
        <td>&nbsp;<?php echo $this->Form->input('Seleccion'.$x.'.chequear', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'onclick'=>"calcularCheques('Seleccion".$x."Chequear','ChequeMontosel".$x."','ChequeMonto".$x."')"));
		echo $this->Form->input('Valor.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$registro['id']));
		echo $this->Form->input('Valor.'.$x.'.codsucursal', array('type' =>'hidden','label' =>'','value'=>$registro['codsucursal']));		
		?></td>
     </tr>
    <?php endforeach; ?>
</tbody>
<tfoot>
    <tr>        
        <th>&nbsp;</th>
        <th colspan="2" align='right' ><strong>Total Monto:</strong></th>
        <th><div align='right' id='total_monto_cheque'><?php echo number_format(sprintf("%01.2f", $total_monto), 2, '.', '.') ;
?></div> </th>
		<th  align='right' ><strong>Total a Depositar:</strong></th>
	<th>
	<?php echo $this->Form->input('Cheque.totales', array('type'=>'hidden','value'=>$x)); ?>
	<div align='right' id='total_monto_che_dep'><?php
	echo number_format(sprintf("%01.2f", 0), 2, '.', '.') ;
?></div> </th>	
	<th>&nbsp;</th>
</tfoot>
</table>
</div>
	</div>	
	</fieldset>
<?php
$options = array('label' => 'Desasociar','class' => "btn btn-primary",'escape'=>false,
);
echo $this->Form->end($options);
?>
<!-- -->                
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
<!-- -->	

	</div>
</div>
<!-- Listado  de Retenciones Asociados -->

<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/conretencionventadets/conciliados", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });

     $('#retencion').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      language: {
        decimal: ",",
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",
	  }	
    });

     $('#retencion2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      language: {
        decimal: ",",
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",
	  }	
    });    
   

  });
</script>
