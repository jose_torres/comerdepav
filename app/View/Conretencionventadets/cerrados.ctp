<?php
	echo $this->Html->css('daterangepicker/daterangepicker.css');
	echo $this->Html->css('daterangepicker/datepicker3.css');
	echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
	echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
	echo $this->Html->css('select2/select2.min.css');
//	print_r($unidades);
?>
<script language="javascript" type="text/javascript">
	
	function actualizar(nombrediv,url,id,datos){
		
        $('#'+nombrediv).html('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');
		
		var selected = '&proveedores=0-';var cont=1;
		$('#ReporteProveedoreId option:checked').each(function(){
		selected += $(this).val() + '-'; 
		});
		fin = selected.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
		selected = selected.substr( 0, fin ); // elimino la coma final
		
		var tipoventa = '&tipoventa=0-';var cont=1;
		$('#ReporteTipoventa option:checked').each(function(){
		tipoventa += $(this).val() + '-'; 
		});
		fin = tipoventa.length - 1; // calculo cantidad de caracteres menos 1 para eliminar la coma final
		tipoventa = tipoventa.substr( 0, fin ); // elimino la coma final
				
		$.ajax({
			type: 'POST',
			url: url+id,
			data: datos+''+selected+''+tipoventa,
			dataType: 'html'
		})

		.done(function( html ) {					
			$( '#'+nombrediv ).html( html );
		});

    }
	
</script>
<?php echo $this->element('menuretventas',array('id'=>3)); ?>
<fieldset id="personal" >
<?php echo $this->Form->create('Reporte',array('role'=>"form",'class'=>"form-horizontal",'url'=>'viewpdfventas/1','id'=>'frm','name'=>'frm'));
?>	
<?php	
echo '<div class="form-group">';		
$fecha2=date("d-m-Y");
$fecha=date('d-m-Y',mktime(0,0,0,date('m'),date('d')-30,date('Y')));
echo $this->Html->tag('label', 'Rango de Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
echo '<div class="input-daterange input-group col-xs-3" id="datepicker">';

echo $this->Form->input('Reporte.desde',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true));
echo '<span class="input-group-addon">a</span>';
echo $this->Form->input('Reporte.hasta',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Hasta",'value'=>$fecha2,'readonly'=>true)); 
echo '</div>';		
echo '</div>';

	?>
<button type="button" class="btn btn-info" onclick="actualizar('buscar','buscarretenciones','','fechadesde='+document.frm.ReporteDesde.value+'&fechahasta='+document.frm.ReporteHasta.value)">Buscar&nbsp;<span class="glyphicon glyphicon-search"></span></button>
<button type="submit" class="btn btn-info">Imprimir&nbsp;<span class="glyphicon glyphicon-book"></span></button>	
<?php echo $this->Form->end(); ?>	
<center>
<form action="" method="post" name="two">
<div id="buscar" class="unidades index">
	<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover">
	<thead>	
	<tr>
			<th><?php echo $this->Paginator->sort('id');?></th>
			<th><?php echo $this->Paginator->sort('nro','Numero');?></th>
			<th><?php echo $this->Paginator->sort('mes','Mes');?></th>
			<th><?php echo $this->Paginator->sort('anio','A&ntilde;o',array('escape'=>false));?></th>
			<th><?php echo $this->Paginator->sort('created','Creado');?></th>
			<th><?php echo $this->Paginator->sort('modified','Actualizado');?></th>
			<th class="actions"><?php echo 'Acciones';?></th>
	</tr>
	</thead>
	<?php
	$i = 0;
	foreach ($unidades as $unidade):
		$class = null;
		if ($i++ % 2 == 0) {
			$class = ' class="altrow"';
		}
	?>
	<tr<?php echo $class;?>>
		<td><?php echo $i; ?>&nbsp;</td>
		<td><?php echo $unidade['Retgenventa']['nro']; ?>&nbsp;</td>
		<td><?php echo $unidade['Retgenventa']['mes']; ?>&nbsp;</td>
		<td><?php echo $unidade['Retgenventa']['anio']; ?>&nbsp;</td>
		<td><?php echo date("d-m-Y",strtotime($unidade['Retgenventa']['created'])); ?>&nbsp;</td>
		<td><?php echo date("d-m-Y",strtotime($unidade['Retgenventa']['modified'])); ?>&nbsp;</td>
		<td class="actions" align="center">
			<?php
			echo $this->Html->link(__('Imprimir&nbsp;<span class="glyphicon glyphicon-print"></span>'), array('action' => 'imprimir', $unidade['Retgenventa']['id']),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false));
			if($unidade['Retgenventa']['estatus']==0){
				echo '&nbsp;&nbsp;&nbsp;';
			echo $this->Html->link(__('Editar&nbsp;<span class="glyphicon glyphicon-edit"></span>'), array('action' => 'declarar'),array("role"=>"button", "class"=>"btn btn-primary",'escape'=>false)); 
			echo '&nbsp;&nbsp;&nbsp;';
			echo $this->Html->link(__('Cerrar&nbsp;<span class="glyphicon glyphicon-remove-circle"></span>'), array('action' => 'cerrar', $unidade['Retgenventa']['id']),array("role"=>"button", "class"=>"btn btn-danger",'escape'=>false),'¿Esta seguro de CERRAR el Registro de Retenciones perteneciente al mes "'.$unidade['Retgenventa']['mes'].'" del año "'.$unidade['Retgenventa']['anio'].'" ?');
			}
			
			 ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, comenzando con el registro {:start}, terminando en {:end}')
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('' . $this->Html->image("img_acciones/goprevious.png", array("alt" => "Ver Pagina Anterior", "title"=>"Ver Pagina Anterior")), array('escape'=>false), null, array('class'=>'disabled','escape'=>false));?>
	  	<?php
			echo $this->Paginator->counter(array('format' => __('P&aacute;gina: %page%', true)));
	?> 
		<?php echo $this->Paginator->next($this->Html->image("img_acciones/gonext.png", array("alt" => "Ver Proxima Pagina", "title"=>"Ver Proxima Pagina ")) . '', array('escape'=>false), null, array('class' => 'disabled','escape'=>false));?><br>
		<?php echo 'P&aacute;ginas: |'.$this->Paginator->numbers();
		?>
	</div>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php	
	//echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/retenciones/add", array('escape'=>false), null);
	//echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
</fieldset>
</div>
</form>
</center>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
?>
<script>
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      language: 'es',
      format: "dd-mm-yyyy",
      todayBtn: "linked",
    });
   

  });
</script>
