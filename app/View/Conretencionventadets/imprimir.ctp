<?php
//print_r($empresa);
$this->fpdf->setup('P');
// Construccion de la Cabecera
	$this->fpdf->SetTitulo($encabezado['titulo_reporte']);
	$this->fpdf->SetLogoIzq($empresa['Empresa']['logo_izquierdo']);
	$this->fpdf->SetLogoDer($empresa['Empresa']['logo_izquierdo']);
	$this->fpdf->SetLine1($empresa['Empresa']['nombre']);
	$this->fpdf->SetLine2($empresa['Empresa']['rif']);
	$this->fpdf->SetLine3($encabezado['line2']);
	$this->fpdf->SetLine4($encabezado['line3']);
	
	$this->fpdf->obtener_pie($slogan="  ");
// obtiene el nro de paginas    
    $this->fpdf->fpdf->AliasNbPages(); 
    $this->fpdf->fpdf->AddPage();
    $this->fpdf->fpdf->SetFont('Arial','',8);
	$this->fpdf->Header();
$tot_detalle=0; $tamTitulo=0;
// column titles
// Inicializacion de la Cabecera

	$RET=0; $total_iva = $total_monto = 0; $datos['Retencion']=array();
	foreach ($asociado as $row){
		$total_monto=$total_monto+$row['Conretencionventadet']['montoretenido'];
		$total_iva= $total_iva + ($row['Conventa']['ivaimp1']+$row['Conventa']['ivaimp2']+$row['Conventa']['ivaimp3']);
		$datos['Retencion'][]=array(date("d-m-Y",strtotime($row['Conventa']['fecha'])),$row['Conretencionventadet']['documento'],$row['Concliente']['rif'],$row['Concliente']['descripcion'],$row['Conretencionventadet']['numero'],date("d-m-Y",strtotime($row['Conretencionventa']['fechaemision'])),number_format(sprintf("%01.2f",  $row['Conventa']['ivaimp1']+$row['Conventa']['ivaimp2']+$row['Conventa']['ivaimp3']), 2, ',', '.'),number_format(sprintf("%01.2f", $row['Conretencionventadet']['montoretenido']), 2, ',', '.'));
	}
	$RET=$total_monto;

//  Datos Retencion
	$header = $w = array(); $tamTitulo = 200;
	$w=array(14,14,16,70,23,13,25,25);// maxima sumatoria 190
 	$header=array(html_entity_decode('Fecha Fact.'),html_entity_decode('Factura'),html_entity_decode('Rif'),html_entity_decode('Cliente'),html_entity_decode('Nro'),html_entity_decode('Fecha'),html_entity_decode('Iva'),html_entity_decode('Iva Ret.'));
    $alineacion=array('C','L','L','L','L','L','R','R');// R=>Derecha L=>izquierda C=> Centrado
    $conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>6,'tamTitulo'=>$tamTitulo);	
	$this->fpdf->FancyTable($header,$datos['Retencion'],$w,html_entity_decode(' Retenciones Asociadas'),0,$alineacion,$conf_titulo);
// Totales
 	$w=array(150,25,25);// maxima sumatoria 190
 	$header=array();
    $alineacion=array('R','R','R');// R=>Derecha L=>izquierda C=> Centrado
	$datos1 = array();
	$datos1[]=array('Total',number_format(sprintf("%01.2f", ($total_iva)), 2, ',', '.'),number_format(sprintf("%01.2f", ($RET)), 2, ',', '.'));
	$conf_titulo=array('pos'=>'C','border'=>1,'tamLetra'=>7,'tamTitulo'=>$tamTitulo,'EstiloD'=>'B');
	$this->fpdf->FancyTable($header,$datos1,$w,html_entity_decode(''),0,$alineacion,$conf_titulo);

 // Fin de Inclusion de los datos a mostrar en el pdf
    echo $this->fpdf->fpdfOutput('retenciones_ventas_'.$retgenventa['Retgenventa']['mes'].'_'.$retgenventa['Retgenventa']['anio'].'.pdf','D');   
?>
