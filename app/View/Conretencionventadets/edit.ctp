<?php
//echo $this->element('menuinterno',$datos_menu);
//echo $this->Html->script('prototype');
echo $this->Html->css('daterangepicker/daterangepicker.css');
echo $this->Html->css('daterangepicker/datepicker3.css');
echo $this->Html->css('colorpicker/bootstrap-colorpicker.min.css');
echo $this->Html->css('timepicker/bootstrap-timepicker.min.css');
echo $this->Html->css('select2/select2.min.css');
echo $this->Html->css('iCheck/all.css');
?>
<script language="javascript" type="text/javascript">
	
	function actualizar(nombrediv,url,id,datos){

        $('#'+nombrediv).html('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');	
				
		$.ajax({
			type: 'POST',
			url: url+id,
			data: datos,
			dataType: 'html'
		})

		.done(function( html ) {					
			$( '#'+nombrediv ).html( html );
		});

    }
    function limpiar(){
		$( '#retencion' ).html( '' );	
	}
	function deseleccionar_todo(){
		for (i=0;i<document.frm.elements.length;i++)
		if(document.frm.elements[i].type == "checkbox")
			document.frm.elements[i].checked=0
	} 
    function calculo_marcar(nombre_tag,codigo){
	//	alert ("Hola");
		var elemento = document.getElementById(""+nombre_tag); 			 			
		var valor_sel=true;
		if (elemento.checked==true){
			valor_sel=elemento.checked;
			deseleccionar_todo();
			elemento.checked=valor_sel;			
			actualizar('retencion','calcularretencion','',''+codigo)		
		}else{
			$( '#retencion' ).html( ' <div class="alert alert-info">Seleccione una Factura</div> ' );			
		}
		
	}
	
</script>
<div class="span11">
	<div class="well">
	<?php echo $this->Form->create('Conretencionventadet', array('url' => 'edit/'.$this->data['Conretencionventadet']['id'],'name' => 'frmRet','id'=>'frmRet','role'=>"form",'class'=>"form-horizontal")); ?>
	<fieldset>
	<legend>DATOS DE RETENCI&Oacute;N</legend>
	<?php
	$tiporeporte['Normal']='Normal';
	//$tiporeporte['Transito']='Transito';
	echo $this->Form->input('Conretencionventadet.id');
	echo $this->Form->input('Conretencionventa.id');
	echo '<div class="form-group">';		
	echo $this->Html->tag('label', 'N&uacute;mero:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="col-xs-3">';
	echo $this->Form->input('Conretencionventadet.numero',array('label'=>false,'size' =>'18','type'=>'text','div'=>false,'class'=>"form-control"));
	echo '</div>';

	$fecha2=date("d-m-Y");
	$fecha=date("d-m-Y",strtotime($this->data['Conretencionventa']['fechaemision']));
	echo $this->Html->tag('label', 'Fecha:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
	echo '<div class="input-daterange col-xs-2">';
	echo $this->Form->input('Conretencionventa.fecha',array('class'=>"input-sm form-control",'div'=>false,'label'=>false,'placeholder'=>"Desde",'value'=>$fecha,'readonly'=>true)); 
	echo '</div>';
	
	echo '</div>';

	echo '<div class="form-group">';

	echo $this->Html->tag('label', 'Factura:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-3">';
	echo $this->Form->input('Conretencionventadet.documento',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>$this->data['Conretencionventadet']['documento']));
	echo '</div>';
	
	echo $this->Html->tag('label', 'Monto Total:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Conventa.montobruto',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>number_format(sprintf("%01.2f", $this->data['Conventa']['montobruto']), 2, ',', '.')));
	echo '</div>';	
	echo '</div>';
	
	echo '<div class="form-group">';
		
	echo $this->Html->tag('label', 'Monto Iva:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-3">';
	echo $this->Form->input('Conretencionventadet.iva1',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>number_format(sprintf("%01.2f",  $this->data['Conventa']['ivaimp1']+ $this->data['Conventa']['ivaimp2']+ $this->data['Conventa']['ivaimp3']), 2, ',', '.')));
	echo $this->Form->input('Retencioncompra.montoiva', array('type'=>'hidden','value'=>$this->data['Conventa']['ivaimp1']+ $this->data['Conventa']['ivaimp2']+ $this->data['Conventa']['ivaimp3']));	
	echo '</div>';
	//$datos['Retencion']=round(round($datos['Iva']*0.75,3),2);
	echo $this->Html->tag('label', 'Monto Retencion:', array('class' => 'col-xs-2 control-label','for'=>"cedula"));
	echo '<div class="col-xs-2">';
	echo $this->Form->input('Conretencionventadet.ret1',array('label'=>'','type'=>'text','div'=>false,'readonly'=>true,'label'=>false,'placeholder'=>"Cedula del Empleado",'class'=>"form-control text-right",'value'=>number_format(sprintf("%01.2f", $this->data['Conretencionventadet']['montoretenido']), 2, ',', '.')));
	echo $this->Form->input('Conretencionventadet.montoretenido', array('type'=>'hidden','value'=>$this->data['Conretencionventadet']['montoretenido']));	
	echo '</div>';
	echo '</div>';
	
	?>
	</fieldset>
	<?php
	$options = array('label' => 'Guardar','class' => "btn btn-primary",'escape'=>false,
    /*'div' => array('class' => "btn btn-primary"),*/
   /* 'input' => array('class' => "btn btn-primary")*/
    );
	echo $this->Form->end($options); ?>
	</div>
</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
	<?php echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/conretencionventadets/index", array('escape'=>false), null);?>  &nbsp;&nbsp;&nbsp;
	<?php echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);?>
</div>
</fieldset>
</center>

</fieldset>
</fieldset>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datepicker/bootstrap-datepicker.js');
	echo $this->Html->script('plugins/datepicker/locales/bootstrap-datepicker.es.js');
	echo $this->Html->script('plugins/select2/select2.full.min.js');
	echo $this->Html->script('plugins/iCheck/icheck.min.js');
	echo $this->Html->script('plugins/select2/i18n/es.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script language="javascript" type="text/javascript">
  	
  $(function () {
    //Initialize Select2 Elements
	$(".select2").select2({
		language: "es"
		});
     $.extend($.inputmask.defaults.definitions, {
        'A': { 
            validator: "[GJVE]",
            cardinality: 1,
            casing: "upper" //auto uppercasing
        }
    });    
    $("#ConretencionventadetNumero").inputmask('9999-99-9999999999', {'translation': {
                                        'A': {pattern: /[GJVE]/},
                                        9: {pattern: /[0-9]/}
                                      }
                                }); 

  });
</script>
