<?php
	echo $this->Html->css('datatables/dataTables.bootstrap.css');
?>
<div class="box">
	<div class="box-header">
	  <h3 class="box-title">Retenciones Encontradas</h3>
	</div>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center" class="table table-bordered table-hover" id="<?php echo $variables['nombretabla'];?>">
<caption>  <strong>Retenciones</strong></caption>
<thead>
    <tr>
        <th>Fecha</th>
        <th>Nro</th>
        <th>Factura</th>
        <th>Cliente</th>
        <th>Iva</th>
        <th>IVA Retenido</th>
        <th>Sel.</th>
    </tr>
</thead>
<tbody>
    <?php $x=0;$total_registros=0; $total_cantidad=0;$total_iva=$total_monto=0;
    foreach ($noasociado as $row): $registro = $row['Conretencionventadet']; ?>
    <?php $x=$x+1; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven" id='td_id'>
	<? } else { ?>
	<tr id='td_id'>
	<? } ?>
        <td><span style='display: none;'><?php echo date("Ymd",strtotime($row['Conretencionventa']['fechaemision']));?></span><?php echo date("d-m-Y",strtotime($row['Conretencionventa']['fechaemision']));?></td>
        <td>&nbsp;<?php echo $registro['numero'];?>&nbsp;
<a href="#" data-toggle="modal" data-target="#EditarRetencion" data-id="<?php echo $row[0]['id']?>" data-idret="<?php echo $row['Conretencionventa']['id']?>" data-opcion="Retencion" data-codretencion=<?php echo $registro['codretencion']?> data-codsucursal="<?php echo $registro['codsucursal']?>" data-documento="<?php echo $registro['documento']; ?>" data-numero="<?php echo $registro['numero']; ?>" data-cliente="<?php echo $row['Concliente']['descripcion']; ?>" data-montoretenido="<?php echo number_format(sprintf("%01.2f", $registro['montoretenido']), 2, ',', '.'); ?>" data-montoiva="<?php echo number_format(sprintf("%01.2f", $row['Conventa']['ivaimp1']+$row['Conventa']['ivaimp2']+$row['Conventa']['ivaimp3']), 2, ',', '.'); ?>" title="<?php echo $row['Concliente']['descripcion']; ?>"><?php echo $this->Html->image("img_acciones/edit.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ?></a>
        </td>
        <td>&nbsp;<?php echo $registro['documento'];?>&nbsp;</td>
        <td>&nbsp;<?php echo $row['Concliente']['descripcion'];?>&nbsp;</td>
        <td align="right">&nbsp;<?php echo number_format(sprintf("%01.2f",  $row['Conventa']['ivaimp1']+$row['Conventa']['ivaimp2']+$row['Conventa']['ivaimp3']), 2, ',', '.');?>&nbsp;</td>
        <td align="right">
            <?php
            $total_iva= $total_iva + ($row['Conventa']['ivaimp1']+$row['Conventa']['ivaimp2']+$row['Conventa']['ivaimp3']);
            $total_monto=$total_monto+$registro['montoretenido'];
            echo $this->Form->input('Retven.monto'.$x, array('size' => 15,'type' =>'text','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",$registro['montoretenido']),'readonly'=>true));
            echo $this->Form->input('Retven.montosel'.$x, array('size' => 15,'type' =>'hidden','label' =>'','style'=>'text-align:right;','value'=>sprintf("%01.2f",0)));
            ?>
        </td>
        <td>&nbsp;<?php
        //echo $this->Form->input('Seleccion'.$x.'.chequear', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'onclick'=>"calcularRetencion('Seleccion".$x."Chequear','RetvenMontosel".$x."','RetvenMonto".$x."')"));
        echo $this->Form->input('Seleccion.'.$x.'.conciliar', array('size' => 5,'type' =>'checkbox','label' =>'','value'=>$x,'div'=>false,'onclick'=>""));
		echo $this->Form->input('Seleccion.'.$x.'.id', array('type' =>'hidden','label' =>'','value'=>$row[0]['id']));
		echo $this->Form->input('Seleccion.'.$x.'.codsucursal', array('type' =>'hidden','label' =>'','value'=>$registro['codsucursal']));		
		?></td>
     </tr>
    <?php endforeach; ?>
</tbody>
<tfoot>
    <tr>        
        <th>&nbsp;</th>
        <th>&nbsp;</th>        
        <th colspan="2" align='right' ><strong>Totales:</strong></th>
        <th><div align='right' id='total_monto_iva_retencion'><?php echo number_format(sprintf("%01.2f", $total_iva), 2, ',', '.') ;
?></div> </th>
        <th><div align='right' id='total_monto_retencion'><?php echo number_format(sprintf("%01.2f", $total_monto), 2, ',', '.') ;
		echo $this->Form->input('Retven.totales', array('type'=>'hidden','value'=>$x)); ?></div> </th>
	
	<th>&nbsp;</th>
</tfoot>
</table>
</div>
<div id="EditarRetencion" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-lg">
	  <div class="modal-content">

		<div class="modal-header">
		  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
		  </button>
		  <h4 class="modal-title" id="myModalLabel"><strong>Registrar un Deposito</strong></h4>
		</div>
		<div class="modal-body">
		<p>
		<?php
		echo $this->Form->input('Conretencionventadet.id',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo $this->Form->input('Conretencionventa.id',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo $this->Form->input('Conretencionventadet.codsucursal',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo $this->Form->input('Conretencionventadet.codretencion',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo $this->Form->input('Conretencionventa.codretencion',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo $this->Form->input('Conretencionventadet.opcion',array('label'=>'','type'=>'hidden','div'=>false,'label'=>false,'class'=>"form-control text-right"));
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Nro:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Conretencionventadet.numero',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right"));
		echo '</div>';		
		echo $this->Html->tag('label', 'Factura:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4" >';
		echo $this->Form->input('Conretencionventadet.documento',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));
		echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
		echo $this->Html->tag('label', 'Cliente:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-6">';
		echo $this->Form->input('Conretencionventadet.cliente',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-left",'readonly'=>true));

		echo '</div>';
		echo '</div>';
		echo '<div class="form-group">';
	
		echo $this->Html->tag('label', 'Monto Iva:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Conretencionventadet.montoiva',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));
		echo '</div>';
		echo $this->Html->tag('label', 'Monto Retenido:', array('class' => 'col-xs-2 control-label','for'=>"ejemplo_email_3"));
		echo '<div class="col-xs-4">';
		echo $this->Form->input('Conretencionventadet.montoretenido',array('label'=>'','type'=>'text','div'=>false,'label'=>false,'placeholder'=>"Numero de Documento",'class'=>"form-control text-right",'readonly'=>true));

		echo '</div>';		
		echo '</div>';

		?></p>
		<br>
		<br>
		</div>
		<div class="modal-footer">
	  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	  <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="guardarRetencion('<?php echo $variables['nombrediv']; ?>','editarmovimientos/','', '&opcion='+document.<?php echo $variables['nombreformulario']; ?>.ConretencionventadetOpcion.value+'&numero='+document.<?php echo $variables['nombreformulario']; ?>.ConretencionventadetNumero.value+'&id='+document.<?php echo $variables['nombreformulario']; ?>.ConretencionventadetId.value+'&id_ret='+document.<?php echo $variables['nombreformulario']; ?>.ConretencionventaId.value+'&codsucursal='+document.<?php echo $variables['nombreformulario']; ?>.ConretencionventadetCodsucursal.value+'&fechadesde='+document.<?php echo $variables['nombreformulario']; ?>.ReporteDesde.value+'&fechahasta='+document.<?php echo $variables['nombreformulario']; ?>.ReporteHasta.value+'&consulta='+document.<?php echo $variables['nombreformulario']; ?>.ReporteConsulta.value+'&opcion=<?php echo $variables['opcion']; ?>&nombretabla=<?php echo $variables['nombretabla']; ?>&nombreformulario=<?php echo $variables['nombreformulario']; ?>');">Guardar Registro</button>
		</div>
	</div>
  </div>
</div>
<?php
	echo $this->Html->script('jquery.min.js');
	echo $this->Html->script('bootstrap.min.js');
	echo $this->Html->script('plugins/datatables/jquery.dataTables.js');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap.js');
	echo $this->Html->script('plugins/input-mask/jquery.inputmask.js');
?>
<script>
  	
  $(function () {
	$('[data-toggle="tooltip"]').tooltip();
    $.extend($.inputmask.defaults.definitions, {
        'A': { 
            validator: "[GJVE]",
            cardinality: 1,
            casing: "upper" //auto uppercasing
        }
    });    
    $("#ConretencionventadetNumero").inputmask('9999-99-9999999999', {'translation': {
                                        'A': {pattern: /[GJVE]/},
                                        9: {pattern: /[0-9]/}
                                      }
                                });

    $('#<?php echo $variables['nombretabla'];?>').DataTable({	  
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      language: {
        decimal: ",",
        url: "/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json", 
        Search: "Buscar:",
        "lengthMenu": "Mostrar _MENU_ records per page"
      },
      "oLanguage": {
		"sUrl": "/comerdepav2/theme/CakeAdminLTE/js/plugins/datatables/Spanish.json",  
		} 
    });
   

  });
 $('#EditarRetencion').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget) // Botón que activó el modal
	  var numero = button.data('numero') // Extraer la información de atributos de datos
	  var documento = button.data('documento') // Extraer la información de atributos de datos
	  var id = button.data('id') // Extraer la información de atributos de datos
	  var idret = button.data('idret') // Extraer la información de atributos de datos
	  var codsucursal = button.data('codsucursal') 
	  var codretencion = button.data('codretencion') 
	  var opcion = button.data('opcion') 
	  var cliente = button.data('cliente') // Extraer la información de atributos de datos
	  var montobruto = button.data('montobruto') 
	  var montoiva = button.data('montoiva') 
	  var montoretenido = button.data('montoretenido') 
	  
	  var modal = $(this)
	  modal.find('.modal-title').text('Modificar Retencion nro: '+id)
	  modal.find('.modal-body #ConretencionventadetId').val(id)
	  modal.find('.modal-body #ConretencionventadetCodretencion').val(codretencion)
	  modal.find('.modal-body #ConretencionventaId').val(idret)
	  modal.find('.modal-body #ConretencionventaCodretencion').val(codretencion)
	  modal.find('.modal-body #ConretencionventadetCodsucursal').val(codsucursal)
	  modal.find('.modal-body #ConretencionventadetOpcion').val(opcion)
	  modal.find('.modal-body #ConretencionventadetNumero').val(numero)
	  modal.find('.modal-body #ConretencionventadetDocumento').val(documento)
	  modal.find('.modal-body #ConretencionventadetCliente').val(cliente)
	  modal.find('.modal-body #ConretencionventadetMontoiva').val(montoiva)
	  modal.find('.modal-body #ConretencionventadetMontoretenido').val(montoretenido)
	  $('.alert').hide();//Oculto alert
	});	 
</script>
