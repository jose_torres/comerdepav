<div class="box">
  <div class="box-header">	
	<div class="row">
			<div class="col-sm-12"><h3 class="box-title">Se realiz&oacute; las siguientes acciones: </h3></div>
	</div>
	<?php
		foreach ($mensaje as $row) {
			echo $row;
		}
	?>

		<div class="row">
			<div class="col-sm-12"><h3 class="box-title">Que desea hacer?: </h3></div>
		</div>
		<div class="row">
			<div class="col-sm-12">
<?php 
	echo $this->Html->link(__('<span class="glyphicon glyphicon-list"></span>&nbsp;Ir al Listados de Retenciones Conciliados'), array('action' => '/conciliados'),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false));
	echo $this->Html->link(__('<span class="glyphicon glyphicon-save"></span>&nbsp;Seguir Registrando Retenciones'), array('action' => 'declarar'),array("role"=>"button", "class"=>"btn btn-info",'escape'=>false)); 
?>					
			</div>
		</div>
	</div>
</div>
