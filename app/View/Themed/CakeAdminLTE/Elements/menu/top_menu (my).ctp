<!-- header logo: style can be found in header.less -->
        <header class="header">
			<?php
				echo $this->Html->link( 'Argos' ,"/usuarios/home", array('escape'=>false,'class'=>"logo"), null)
			?>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->

                        <!-- Notifications: style can be found in dropdown.less -->
                        
                        <!-- Tasks: style can be found in dropdown.less -->
 
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<i class="glyphicon glyphicon-user"></i>
								<?php
									if(isset($datos_empleado)){
										echo '<span>EMPLEADO:'.$datos_empleado.'<i class="caret"></i></span>';
									}
								?>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-black">
                                     <?= $this->Html->image('avatar04.png', array('class' => 'img-circle')); ?>
                                     <?php
									if(isset($datos_empleado)){
										echo '<p>'.$datos_empleado.'</p>';
									}
								?>                                    
                                </li>
                                <!-- Menu Body -->
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <?php echo $this->Html->link('Panel' ,"/usuarios/home", array('escape'=>false,'class'=>"btn btn-default btn-flat"), null);
                                         ?>
                                    </div>
                                    <div class="pull-right">
                                        <?php echo $this->Html->link( 'Salir' ,"/usuarios/logout", array('escape'=>false,'class'=>"btn btn-default btn-flat"), null); ?>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
