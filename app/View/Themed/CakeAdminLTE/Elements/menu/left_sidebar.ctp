
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">                
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= $this->Html->image('avatar04.png', array('class' => 'img-circle')); ?>
            </div>
            <div class="pull-left info">
            <?php
            if(isset($datos_empleado)){
                    echo '<p>Saludos, '.$datos_empleado.'</p>';
            }
            ?>
                <a href="#"><i class="fa fa-circle text-success"></i> En l&iacute;nea</a>
            </div>
        </div>
        <!-- search form Revisar la Copia -->        
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <?php
			$i=0;$item=0;$cod_grupo=0;
            if (isset($datos_menu)){
            echo '<li class="active">'.$this->Html->link( $this->Html->image("img_acciones/apacheconf.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;Panel' ,$datos_paneles['redireccion'], array('escape'=>false), null).'</li>'; 
                if (!isset($datos_menu['funciones'])){
                    $datos_menu['funciones'] = $datos_menu;
                }
                if (!isset($datos_menu['hijos'])){
                    $datos_menu['hijos'] = array();
                }
            
            
                foreach ($datos_menu['funciones'] as $menu) {
                //Mysql	if ($cod_grupo!=$menu['grupo']['id'])
                if ($cod_grupo!=$menu['0']['id']){
                        $item=$item+1;
                        if($i>0){
                            echo '</ul>';
                            echo '</li>';
                        }
                        echo '<li class="treeview">';
                        echo  $this->Html->link( '<i class="fa fa-bar-chart-o"></i><span>'.$this->Html->image("img_acciones/".$menu['0']['imagen'], array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;'.$menu['0']['gnombre'].'</span><i class="fa fa-angle-left pull-right"></i>' ,"#", array('escape'=>false,'rel'=>"ddsubmenu".$item), null);
                        echo '<ul class="treeview-menu">';
                }
                if (count($datos_menu['hijos'])<=0){
                        echo '<li>'.$this->Html->link( '<i class="fa fa-angle-double-right"></i>'.$this->Html->image("img_horizontal/format-indent-more.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;'.$menu['0']['fnombre'] ,"/".$menu['0']['direccion'], array('escape'=>false), null).'</li>';
                }else{
                    if (count($datos_menu['hijos'][$menu['0']['fid']]['children'])>0){
                        echo '        <li class="treeview">
          <a href="#">
            <i class="fa fa-share"></i> <span>'.$this->Html->image("img_horizontal/list-add.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;'.$menu['0']['fnombre'].'</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">';
            foreach ($datos_menu['hijos'][$menu['0']['fid']]['children'] as $menuhijo) {
                echo '<li>'.$this->Html->link( '<i class="fa fa-angle-double-right"></i>'.$this->Html->image("img_horizontal/format-indent-more.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;'.$menuhijo['Funcione']['nombre'] ,"/".$menuhijo['Funcione']['direccion'], array('escape'=>false), null).'</li>';
            }          
          echo  '</ul></li>';
                    }else{
                        echo '<li>'.$this->Html->link( '<i class="fa fa-angle-double-right"></i>'.$this->Html->image("img_horizontal/format-indent-more.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;'.$menu['0']['fnombre'] ,"/".$menu['0']['direccion'], array('escape'=>false), null).'</li>';
                    }
                }        
                        $cod_grupo=$menu['0']['id'];
                        $i=$i+1;
                }
                }
                if($i>0){
                    echo '</ul>';
                    echo '</li>';
                }
            echo '<li class="active">'.$this->Html->link( $this->Html->image("img_acciones/users_back22.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;Salir' ,"/usuarios/logout", array('escape'=>false), null).'</li>';
            ?>           
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
