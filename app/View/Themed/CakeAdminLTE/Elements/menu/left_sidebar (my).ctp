
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">                
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <?= $this->Html->image('avatar04.png', array('class' => 'img-circle')); ?>
            </div>
            <div class="pull-left info">
				<?php
				if(isset($datos_empleado)){
					echo '<p>Saludos, '.$datos_empleado.'</p>';
					echo '<a href="#"><i class="fa fa-circle text-success"></i> En l&iacute;nea</a>';
				}
				?>                
            </div>
        </div>
        <!-- search form Revisar la Copia -->        
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <?php echo '<li class="active">'.$this->Html->link( $this->Html->image("img_acciones/apacheconf.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;Panel' ,"/usuarios/home", array('escape'=>false), null).'</li>'; 
            $i=0;$item=0;$cod_grupo=0;
            if (isset($datos_menu)){		
			foreach ($datos_menu as $menu) {
			//Mysql	if ($cod_grupo!=$menu['grupo']['id'])
			if ($cod_grupo!=$menu['grupo']['id']){
				$item=$item+1;
				if($i>0){
					echo '</ul>';
					echo '</li>';
				}
				echo '<li class="treeview">';
				echo  $this->Html->link( '<i class="fa fa-bar-chart-o"></i><span>'.$this->Html->image("img_acciones/".$menu['grupo']['imagen'], array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;'.$menu['grupo']['gnombre'].'</span><i class="fa fa-angle-left pull-right"></i>' ,"#", array('escape'=>false,'rel'=>"ddsubmenu".$item), null);
				echo '<ul class="treeview-menu">';
			}
				echo '<li>'.$this->Html->link( '<i class="fa fa-angle-double-right"></i>'.$this->Html->image("img_horizontal/format-indent-more.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;'.$menu['funcion']['fnombre'] ,"/".$menu['funcion']['direccion'], array('escape'=>false), null).'</li>';
				$cod_grupo=$menu['grupo']['id'];
				$i=$i+1;
			}
			}
			if($i>0){
				echo '</ul>';
				echo '</li>';
			}
            echo '<li class="active">'.$this->Html->link( $this->Html->image("img_acciones/users_back22.png", array('align'=>"absmiddle", "title"=>"Inicio del Sistema")).'&nbsp;Salir' ,"/usuarios/logout", array('escape'=>false), null).'</li>';
            ?>           
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
