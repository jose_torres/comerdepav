<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	function validar(){	
		if(document.getElementById("RevisioneJustificacion").value=='' ){
			alert ('Debe escribir una Justificacion.');
			return false;
		}
		document.frm.submit();
	}
</script>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>	
<div class="unidades form">
<?php echo $this->Form->create('Documento', array('name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned','enctype'=>"multipart/form-data",'url'=>'revisar')); ?>
<fieldset>
<h2><strong>DATOS DEL DOCUMENTO</strong></h2></legend>
<table border='1' class="pure-table pure-table-horizontal" align="center">
	<title>Datos del Documento</title>
	<tr>
		<td><label><b>C&oacute;digo:</b></label></td>
		<td><?php echo $data['Documento']['codigo']; ?> </td>
		<td><label><b>T&iacute;tulo:</b></label></td>
		<td><?php echo $data['Documento']['titulo']; ?> </td>
	</tr>
	<tr>
		<td><label>Nombre:</label></td>
		<td><?php echo $data['Documento']['descripcion']; ?></td>
		<td><label>Archivo:</label></td>
		<td><?php echo $this->Html->link($data['Documento']['archivo_nombre'], '/files/archivos/'.$data['Documento']['archivo_nombre'],array('target'=>'_blank','div'=>false));	?> </td>
	</tr>
	<tr>
		<td><label>&Aacute;rea de Origen:</label></td>
		<td><?php echo $data['Oficina']['siglas'].'=>'.$data['Oficina']['descripcion']; ?> </td>
		<td><label>Tipo de Documento:</label></td>
		<td><?php echo $data['Tipodocumento']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Justificaci&oacute;n:</label></td>
		<td><?php echo $data['Tipojustificacione']['nombre']; ?> </td>
		<td><label>Estatus:</label></td>
		<td><?php echo $data['Documento']['estatus']; ?> </td>
	</tr>
	<tr>
		<td><label>Realizado por:</label></td>
		<td><?php echo $data['Usuario']['usuario']; ?> </td>
		<td><label>Modificado:</label></td>
		<td><?php echo date("d-m-Y H:i:s",strtotime($data['Documento']['modified'])); ?> </td>
	</tr>
</table>
</fieldset>	
<fieldset id="personal" >
<legend  class="info">
<h2><strong>REGISTRAR UNA REVISI&Oacute;N</strong></h2></legend>
<?php
	echo $this->Form->input('Revisione.documento_id', array('type'=>'hidden','value'=>$data['Documento']['id']));
	echo $this->Form->input('Documento.id', array('type'=>'hidden','value'=>$data['Documento']['id']));
	echo $this->Form->input('Revisione.accion', array('type'=>'hidden','value'=>$data['Documento']['estatus']));
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Revisione.tipojustificacione_id',array('label'=>'Decisi&oacute;n:','size' =>'1','type'=>'select','div'=>false,'empty' =>'(Seleccione uno ...)','options'=>$tipojustificaciones,'onchange'=>'RevisioneJustificacion.value = document.getElementById("RevisioneTipojustificacioneId").options[document.getElementById("RevisioneTipojustificacioneId").selectedIndex].text'));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Revisione.justificacion',array('label'=>'Observaci&oacute;n:','rows' =>'4','cols'=>"60",'type'=>'textarea','div'=>false));
	echo '</div>';
?>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">	
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"validar()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/documentos/recibidos", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</div>
</fieldset>
</fieldset>
