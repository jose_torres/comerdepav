<?php
//echo $this->element('menuinterno',$datos_menu);
?>
<form>
<fieldset id="personal" >
<legend class="info"><h2> MOSTRAR EL DOCUMENTO </h2></legend>

<table border='1' class="pure-table pure-table-horizontal" align="center">
	<tr>
		<td><label>T&iacute;tulo:</label></td>
		<td><?php echo $data['Documento']['titulo']; ?> </td>
	</tr>
	<tr>
		<td><label>Nombre:</label></td>
		<td><?php echo $data['Documento']['descripcion']; ?></td>
	</tr>
	<tr>
		<td><label>Archivo:</label></td>
		<td><?php echo $this->Html->link($data['Documento']['archivo_nombre'], '/files/archivos/'.$data['Documento']['archivo_nombre'],array('target'=>'_blank','div'=>false));	?> </td>
	</tr>
	<tr>
		<td><label>&Aacute;rea de Origen:</label></td>
		<td><?php echo $data['Oficina']['siglas'].'=>'.$data['Oficina']['descripcion']; ?> </td>
	</tr>
	<tr>
		<td><label>Tipo de Documento:</label></td>
		<td><?php echo $data['Tipodocumento']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Justificaci&oacute;n:</label></td>
		<td><?php echo $data['Tipojustificacione']['nombre']; ?> </td>
	</tr>
	<tr>
		<td><label>Estatus:</label></td>
		<td><?php echo $data['Documento']['estatus']; ?> </td>
	</tr>
	<tr>
		<td><label>Realizado por:</label></td>
		<td><?php echo $data['Usuario']['usuario']; ?> </td>
	</tr>
	<tr>
		<td><label>Creado:</label></td>
		<td><?php echo date("d-m-Y H:i:s",strtotime($data['Documento']['created'])); ?> </td>
	</tr>
	<tr>
		<td><label>Modificado:</label></td>
		<td><?php echo date("d-m-Y H:i:s",strtotime($data['Documento']['modified'])); ?> </td>
	</tr>
</table>

<?php if (!empty($data['Revisione'])):?>
	<h3><?php __('Revisiones Realizadas');?></h3>
	<table cellpadding = "0" cellspacing = "0" width="100%" class="pure-table pure-table-horizontal">
	<thead>	
	<tr>
		<th><?php echo ('Nro'); ?></th>
		<th><?php echo('Id'); ?></th>
		<th><?php echo('Revision'); ?></th>
		<th><?php echo('Usuario'); ?></th>
		<th><?php echo('Creado'); ?></th>
		<th><?php echo('Modificado'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php
		$i = 0;
		foreach ($data['Revisione'] as $articulo):
			$class = null;
			if ($i++ % 2 == 0) {
				$class = ' class="altrow"';
			}
		?>
		<tr<?php echo $class;?>>
			<td><?php echo $i;?></td>
			<td><?php echo $articulo['id'];?></td>
			<td><?php echo $articulo['justificacion'];?></td>
			<td><?php echo $usuarios[$articulo['usuario_id']];?></td>
			<td><?php echo date("d-m-Y H:i:s",strtotime($articulo['created']));?></td>
			<td><?php echo date("d-m-Y H:i:s",strtotime($articulo['modified']));?></td>
		</tr>
	<?php endforeach; ?>
	</tbody>
	</table>
<?php endif; ?>

<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php
echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/documentos".$url, array('escape'=>false), null);
echo '&nbsp;&nbsp;&nbsp;';
echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</fieldset>

</fieldset>
</form>
