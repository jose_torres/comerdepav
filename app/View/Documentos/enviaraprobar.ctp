<?php
//echo $this->element('menuinterno',$datos_menu);
echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id,parametros){
	 // alert ('El registro ha sido Modificado');	
	 $('cargando').update('<?php echo $this->Html->image('ajax-loader.gif',array('title'=>'Espere...','align'=>"absmiddle"));?><br><strong>Cargando...</strong>');	
		var ajx = new Ajax.Updater(""+nombrediv,""+url+id,{asynchronous:true, evalScripts:true, parameters:"?"+parametros,
		 onSuccess: function(){
         $('cargando').update("");
		}
		 });	
	}
	
	function validar(){	
		if(document.getElementById("DocumentoDescripcion").value=='' ){
			alert ('Debe escribir una descripcion.');
			return false;
		}
		if(document.getElementById("DocumentoTitulo").value=='' ){
			alert ('Debe escribir un Titulo.');
			return false;
		}
		document.frm.submit();
	}
</script>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>	
<div class="unidades form">
<?php echo $this->Form->create('Documento', array('name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned','enctype'=>"multipart/form-data",'url'=>'enviaraprobar')); ?>	
<fieldset id="personal" >
<legend  class="info">
<h2><strong>ENVIAR DOCUMENTO A APROBACI&Oacute;N</strong></h2></legend>
<?php
	echo $this->Form->input('Documento.id', array('type'=>'hidden'));
	echo $this->Form->input('Documento.nrorevision', array('type'=>'hidden'));
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.titulo',array('label'=>'Titulo:','size' =>'40','type'=>'text','div'=>false,'readonly'=>true));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.descripcion',array('label'=>'Descripci&oacute;n:','rows' =>'2','cols'=>"39",'type'=>'textarea','readonly'=>true,'div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.archivo_nombre',array('label'=>'Nombre Archivo:','size' =>'40','type'=>'text','readonly'=>true,'div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.archivo',array('label'=>'Subir Archivo Corregido:','size' =>'40','type'=>'file','div'=>false));
	echo '</div>';
	echo $this->Form->end();

	$var2['Usuario']='Usuario';
	//$var=array();$var['Ver']='Ver';
	$var['Editar']='Editar';
	echo '<form class="pure-form pure-form-stacked" id="envia" name="envia">';
	echo '<fieldset>';
	echo '<legend class="info">Enviar a:</legend>';
	echo '<div class="pure-u-1-4">';
	echo $this->Form->input('Envio.dirigido', array('label'=>'Responsable de Aprobaci&oacute;n:','options'=>$var2,'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','escape'=>false,"onchange"=>"modificar('remitente','../../documentos/buscarremitente/',document.envia.EnvioDirigido.value,'aprobar=SI');")) ;
	echo '</div>';
	echo '<div class="pure-u-1-3" id="remitente">';
	echo $this->Form->input('Envio.remitente', array('label'=>'Remitente:','options'=>array(),'type'=>'select','empty' =>'(Seleccione uno ...)','size'=>'1','escape'=>false)) ;
	echo '</div>';
	echo '<div class="pure-u-1-3">';
	echo $this->Form->input('Envio.permiso', array('label'=>'Permiso:','options'=>$var,'type'=>'select','size'=>'1','escape'=>false,'div'=>false)) ;
	echo $this->Html->image('img_acciones/BGuardar_Detalle.png',array("onclick"=>'modificar("guardardetalle","../guardardetalle/",'.'document.envia.EnvioDirigido.value,"remitente="+document.envia.EnvioRemitente.value+"&permiso="+document.envia.EnvioPermiso.value+"&valorControl='.$this->data['Documento']['id'].'");',"align"=>"Absmiddle", "title"=>"Guardar Detalle",'div'=>false));
	echo '</div>';
	echo '</fieldset>';	
	echo '</form>';
	?>
<div id="cargando"></div>	
<div id="guardardetalle"></div>	
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">	
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"validar()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/documentos/enviados", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</div>
</fieldset>
</fieldset>
<!-- </form> -->
