<?php
	//echo $this->element('menuinterno',$datos_menu);
	echo $this->Html->script('prototype');
	echo $this->Html->script('tinymce/tinymce.min');	
?><script>tinymce.init({selector:'textarea',plugins: [
    'advlist autolink lists link image charmap print preview anchor',
    'searchreplace visualblocks code fullscreen',
    'insertdatetime media table contextmenu paste code'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent'});</script>
<script language="javascript" type="text/javascript">
	function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
	function validar(){	
		if(document.getElementById("DocumentoDescripcion").value=='' ){
			alert ('Debe escribir una descripcion.');
			return false;
		}
		if(document.getElementById("DocumentoTitulo").value=='' ){
			alert ('Debe escribir un Titulo.');
			return false;
		}
		document.frm.submit();
	}
</script>
<style type="text/css">
    div.error_message {background-color:#FFE8E8;border:1px solid red;padding:4px;margin:2px}
</style>	
<div class="unidades form">
<?php echo $this->Form->create('Documento', array('name' => 'frm','id'=>'frm','class'=>'pure-form pure-form-aligned','enctype'=>"multipart/form-data")); ?>	
<fieldset id="personal" >
<legend  class="info">
<h2><strong>REGISTRAR UN DOCUMENTO</strong></h2></legend>
<?php
	echo $this->Form->input('Documento.id', array('type'=>'hidden'));
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.codigo',array('label'=>'C&oacute;digo:','size' =>'40','type'=>'text','readonly'=>true,'div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.tipodocumento_id',array('label'=>'Tipo de Documentos:','size' =>'1','type'=>'select','div'=>false,'empty' =>'(Seleccione uno ...)'));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.oficina_id',array('label'=>'Area De Origen:','size' =>'1','type'=>'select','div'=>false,'empty' =>'(Seleccione uno ...)'));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.tipojustificacione_id',array('label'=>'Justificaci&oacute;n:','size' =>'1','type'=>'select','div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.titulo',array('label'=>'Titulo:','size' =>'40','type'=>'text','div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.descripcion',array('label'=>'Descripci&oacute;n:','size' =>'80','type'=>'text','div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.nrorevision',array('label'=>'Nro de Revisi&oacute;n:','size' =>'10','type'=>'text','div'=>false));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.plantilla_id',array('label'=>'Plantillas:','size' =>'1','type'=>'select','div'=>false,'options'=>$plantilla));
	echo '</div>';
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.archivo_nombre',array('label'=>'Nombre Archivo:','size' =>'40','type'=>'text','readonly'=>true,'div'=>false));
	echo '</div>';
	if($this->data['Documento']['plantilla_id']<=0){
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Documento.archivo',array('label'=>'Archivo:','size' =>'40','type'=>'file','div'=>false));
	echo '</div>';
	}else{
	$cont=0;
	foreach ($contenidos as $row){
		echo $this->Form->input('Documento.archivo',array('label'=>'Archivo:','size' =>'40','type'=>'hidden','div'=>false));
		$cont=$cont+1;
		echo '<div class="pure-control-group">';
		echo $this->Form->input('Documento.'.$cont.'.seccion',array('label'=>'<strong>Secci&oacute;n '.$row['Esquema']['Seccione']['nombre'].':</strong>','rows' =>'10','cols'=>"39",'type'=>'textarea','div'=>false,'value'=>$row['Contenido']['texto']));
		echo $this->Form->input('Documento.'.$cont.'.esquema', array('type' =>'hidden','label' =>'','value'=>$row['Esquema']['id']));
		echo $this->Form->input('Documento.'.$cont.'.contenido', array('type' =>'hidden','label' =>'','value'=>$row['Contenido']['id']));
		echo '</div>';	
		}
		echo $this->Form->input('Documento.totales', array('type' =>'hidden','label' =>'','value'=>$cont));
	}
	/*echo '<pre>';
	print_r($contenidos);
	echo '</pre>';*/
?>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">	
<?php	
	echo $this->Html->image('img_acciones/media-floppy.png',array("onclick"=>"validar()", "title"=>"Guardar Registro"));
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/go-jump.png", array("alt" => "Regresar al listado", "title"=>"Regresar al listado")) ,"/documentos", array('escape'=>false), null);
	echo '&nbsp;&nbsp;&nbsp;';
	echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>
</div>
</div>
</fieldset>
</fieldset>
<!-- </form> -->
