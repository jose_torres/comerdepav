<?php
//echo $this->element('menuinterno',$datos_menu);
echo $this->Html->script('prototype');
?>
<script language="javascript" type="text/javascript">
		function modificar(nombrediv,url,id){
	 // alert ('El registro ha sido Modificado');		
	  var ajx = new Ajax.Updater(""+nombrediv,""+url+id, {asynchronous:true, evalScripts:true});
	}
</script>
<fieldset id="personal" >
<legend> <h2>LISTADO DE DOCUMENTOS </h2></legend>
<center>
<?php
echo $this->element('menudocumentos',$datos_resumen);
//$pagination->setPaging($paging);
?>
<form action="" method="post" name="two" 'class'="pure-form pure-form-aligned">
<?php
	echo '<div class="pure-control-group">';
	echo $this->Form->input('Tipohectarea.buscar_tipo',array('label'=>'Buscar Por Nro:','size' =>'40','type'=>'text','div'=>false));
	echo $this->Html->image('img_acciones/system-search.png',array("onclick"=>"modificar('buscar','documentos/buscar/',document.two.TipohectareaBuscarTipo.value);", "title"=>"Buscar Registro",'div'=>false,"align"=>"Absmiddle"));
	echo '</div>';
?>
</form>
<div id="buscar">
<table border="1" cellpadding="1" cellspacing="0" width="100%" align="center" class="pure-table pure-table-horizontal">
<thead>
    <tr>
        <th ><?php echo $this->Paginator->sort('id','Nro');?></th>
        <th><?php echo $this->Paginator->sort('titulo','Titulo');?></th>
        <th><?php echo $this->Paginator->sort('descripcion','Descripcion');?></th>
        <th>Enviado por</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $banco = $row['Documento']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $banco['codigo'] ?>    
        </td>
        <td>
            <?php echo $banco['titulo']; ?>
        </td>
         <td>
            <?php 
            if ($banco['plantilla_id']<=0){
				echo $this->Html->link($banco['descripcion'], '/files/archivos/'.$banco['archivo_nombre'],array('target'=>'_blank','div'=>false)); 
			}else{
				echo $this->Html->link($banco['descripcion'], '/documentos/viewpdf/'.$banco['id'],array('target'=>'_blank','div'=>false)); 
			}
            ?>    
        </td>   
        <td>
            <?php echo $usuarios[$banco['usuario_id']] ?>
        </td>
        <td>
			<?php echo date("d-m-Y H:i:s",strtotime($banco['modified']));?>
        </td>
        <td align="center">
	  <?php	    
		echo $this->Html->link( $this->Html->image("img_acciones/view_text.png", array("alt" => "Enviar Documentos", "title"=>"Enviar Documentos")) ,"/documentos/view/".$banco['id']."-recibidosaprobar", array('escape'=>false), null );
		if ($row['Documentopermiso']['permiso']=='Editar'){
		echo '&nbsp;|&nbsp;';
		echo $this->Html->link( $this->Html->image("img_acciones/ok.png", array("alt" => "Incluir Revision de Documentos", "title"=>"Incluir Revision de Documentos")) ,"/documentos/aprobar/".$banco['id'], array('escape'=>false), null);
		}
		 ?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
</div>
<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('P&aacute;gina {:page} de {:pages}, mostrando {:current} registro de un total {:count}, comenzando con el registro {:start}, terminando en {:end}')
	));
	?>	</p>

	<div class="paging">
		<?php echo $this->Paginator->prev('' . $this->Html->image("img_acciones/goprevious.png", array("alt" => "Ver Pagina Anterior", "title"=>"Ver Pagina Anterior")), array('escape'=>false), null, array('class'=>'disabled','escape'=>false));?>
	  	<?php
			echo $this->Paginator->counter(array('format' => __('P&aacute;gina: %page%', true)));
	?> 
		<?php echo $this->Paginator->next($this->Html->image("img_acciones/gonext.png", array("alt" => "Ver Proxima Pagina", "title"=>"Ver Proxima Pagina ")) . '', array('escape'=>false), null, array('class' => 'disabled','escape'=>false));?><br>
		<?php echo 'P&aacute;ginas: |'.$this->Paginator->numbers();
		?>
	</div>
<fieldset id="acciones" style="background-color: rgb(242, 242, 242);">
<legend id="herramienta"><?php			
	echo $this->Html->image('img_acciones/applications-accessories.png',array("onclick"=>"", "title"=>"Barra de Herramientas","align"=>'absmiddle'));
?>&nbsp;<strong><em>Acciones</em></strong></legend>
<div align="center">
<?php echo $this->Html->link( $this->Html->image("img_acciones/book_blue_new.png", array("alt" => "Insertar un nuevo Registro", "title"=>"Insertar un nuevo Registro")) ,"/documentos/add", array('escape'=>false), null);
echo '&nbsp;&nbsp;&nbsp;'; 
echo $this->Html->link( $this->Html->image("img_acciones/users_back.png", array("alt" => "Salir del Sistema", "title"=>"Salir del Sistema")) ,"/usuarios/logout", array('escape'=>false), null);
?>	
</div>
</fieldset>
</center>
</fieldset>
</fieldset>
