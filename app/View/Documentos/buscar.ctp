<?php if (count($data)>0){?>
<table border="1" cellpadding="1" cellspacing="0" width="90%" align="center">
<thead>
    <tr>
        <th ><div class="blanco"><? echo $pagination->sortBy('id','Codigo')?></div></th>
        <th><div class="blanco"><? echo $pagination->sortBy('descripcion','Descripcion')?></div></th>
        <th>Creado</th>
        <th>Modificado</th>
        <th colspan="2">Acciones</th>
    </tr>
</thead>	
<tbody>
    <?php $x=0;foreach ($data as $row): $banco = $row['Banco']; ?>    
    <? $x++; ?>
	<? if($x % 2 == 0){ ?>
	<tr class="roweven">
	<? } else { ?>
	<tr>
	<? } ?>
        <td>
            <?php echo $banco['id'] ?>    
        </td>
        <td>
            <?php echo $html->link($banco['descripcion'], '/bancos/view/'.$banco['id']) ?>    
        </td>      
        <td>
            <?php echo $banco['created'] ?>
        </td>
        <td>
            <?php 
                if (!empty($banco['modified'])) echo $banco['modified'];
                else if (!empty($banco['updated'])) echo $banco['updated'];
            ?>
        </td>
        <td align="center">
	  <?php 
		echo $html->link( $html->image("img_acciones/book_open2.png", array("alt" => "Ver Registro", "title"=>"Ver Registro")) ,"/bancos/view/".$banco['id'], array('escape'=>false), null ); ?>
	        &nbsp;|&nbsp;
      	  <?php	
		echo $html->link( $html->image("img_acciones/pda_write.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/bancos/edit/".$banco['id'], array('escape'=>false), null); ?>
            &nbsp;|&nbsp;
          <?php	
		echo $html->link( $html->image("img_acciones/book_blue_delete.png", array("alt" => "Editar Registro", "title"=>"Editar Registro")) ,"/bancos/delete/".$banco['id'], array('escape'=>false),'¿Esta seguro de eliminar "'.$banco['descripcion'].'"?');
		?>
        </td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
<? } ?>
