<?php

$this->tcpdf->core->SetPageOrientation("P");
$this->tcpdf->core->SetProtection(array('print', 'modify', 'copy', 'annot-forms', 'fill-forms', 'extract', 'assemble', 'print-high'), '', null, 0, null);;
//-----Def. de Datos Cabeceras------------------------------------------
$this->tcpdf->core->SetLogos($empresa['Empresa']['logo_izquierdo']);
$this->tcpdf->core->SetTituloCodigo('Codigo:'.$doc['Documento']['codigo'],'B',10);
$this->tcpdf->core->SetTituloPlantilla($plantilla['Plantilla']['nombre'].':','B',10);
$this->tcpdf->core->SetTituloContenido($doc['Documento']['titulo'].'','',10);
$this->tcpdf->core->SetFechaCreacion(date("d/m/Y",strtotime($doc['Documento']['created'])).'');
$this->tcpdf->core->SetFechaRevision(date("d/m/Y",strtotime($doc['Documento']['modified'])).'');
//----------------------------------------------------------------------
$this->tcpdf->core->SetCreator(PDF_CREATOR);
$this->tcpdf->core->SetAuthor ('Argos System');

$this->tcpdf->core->SetHeaderMargin(10);
$this->tcpdf->core->SetFooterMargin(PDF_MARGIN_FOOTER);
$this->tcpdf->core->SetTopMargin(40);

$textfont = 'helvetica';
$this->tcpdf->core->SetHeaderData('logo.png', 22, $plantilla['Plantilla']['nombre'], 'SISTEMA DE GESTIÓN ');

//Desactivamos corte automatico de las paginas
$this->tcpdf->core->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );

$this->tcpdf->core->setHeaderFont(array($textfont,'',12));
$this->tcpdf->core->xheadercolor = array(250,250,250);
$this->tcpdf->core->xheadertext = '';

//Desactivamos el Header y Footer Original del TCPDF
//$this->tcpdf->core->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$this->tcpdf->core->setPrintHeader(true);
$this->tcpdf->core->setPrintFooter(true);

$this->tcpdf->core->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$this->tcpdf->core->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//Agregar Página
$this->tcpdf->core->AddPage();
$this->tcpdf->obtener_pie($slogan="");

//foreach ($texto_dividido as $registro) {
//$this->tcpdf->Header();
//----------------------------------------------------------------------
//-----Def. de Datos Contenidos------------------------------------------
$this->tcpdf->core->texto($texto);
//----------------------------------------------------------------------
//-----Def. de Datos Pie de Pagina------------------------------------------
//----------------------------------------------------------------------
//}	
$this->tcpdf->tcpdfOutput('plantilla_Nro_'.$doc['Documento']['id'].'.pdf', 'D');
?>
