﻿select PAGOS_VENTAS.codventa,PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.codcliente,PAGOS_VENTAS.rif,PAGOS_VENTAS.descripcion, PAGOS_VENTAS.fecha, PAGOS_VENTAS.baseimp2, PAGOS_VENTAS.ivaimp2, PAGOS_VENTAS.codvendedor, PAGOS_VENTAS.Nombre_Vendedor, PAGOS_VENTAS.estatus, PAGOS_VENTAS.tipopago,
PAGOS_VENTAS.tipomovimientopago, PAGOS_VENTAS.Monto_Pagado, PAGOS_VENTAS.FechaVencimiento, PAGOS_VENTAS.FechaPago, PAGOS_VENTAS.Nropago, PAGOS_VENTAS.banco,  
PAGOS_VENTAS.concepto
from (
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor,VD.descripcion as Nombre_Vendedor,
VT.estatus, VTP.tipopago,'PAG' as tipomovimientopago ,VTP.monto as Monto_Pagado,VT.fecha as FechaVencimiento,VT.fecha as FechaPago, 
CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='TDEBITO' THEN VTP.numerotransferencia WHEN VTP.tipopago='CHEQUE' THEN VTP.numerocheque  ELSE 'S/N' END as Nropago, 
CASE WHEN VTP.tipopago='TRANSFER' or VTP.tipopago='CHEQUE' THEN BC.nombre ELSE VTP.banco END as banco, 'PAGO DE FACTURA AL CONTADO' as concepto 
from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
inner join ventaspagos VTP on (VTP.codventa=VT.codventa)

left join bancos BC on (BC.codbanco=REGEXP_REPLACE(COALESCE(VTP.banco, '0'), '[^0-9]*' ,'0')::integer)
where VTP.tipopago<>'CREDITO'
union all
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,
VT.estatus, upper(CAP.tipopago) as TipoPago, CCP.tipomovimientopago, CAP.monto as Monto_Pagado,CCP.fecha as FechaVencimiento,CCP.fecha as FechaPago, 
CAP.numero as Nropago,CAP.banco,CCA.concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
inner join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
where VTP.tipopago='CREDITO'
union all
select VT.codventa,VT.numerofactura, VT.codcliente,CT.rif,CT.descripcion, VT.fecha, VT.baseimp2,VT.ivaimp2,VT.codvendedor, VD.descripcion as Nombre_Vendedor,
VT.estatus, upper('DEVOLUCION') as TipoPago, 'NOTA DE CREDITO' AS tipomovimientopago, (NCV.montobruto *-1) as Monto_Pagado,CCP.fecha as FechaVencimiento,NCV.fecha as FechaPago, 
NCV.codnotacredito::varchar as Nropago,'' as banco,'NOTA DE CREDITO A LA FACTURA' AS concepto from ventas VT 
inner join clientes CT on (CT.codcliente=VT.codcliente and CT.codsucursal=VT.codclientesucursal)
inner join vendedores VD on (VD.codvendedor=VT.codvendedor)
inner join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura and NCV.codcliente=VT.codcliente)
--inner join ventaspagos VTP on (VTP.codventa=VT.codventa)
left join ccpagadas  CCP on (CCP.codmovimientocc=VT.codventa)
--left join ccabonopago CAP on (CAP.codccabono=CCP.codmovimientopago)
--left join ccabono CCA on (CAP.codccabono=CCA.codccabono)
--left join notacredito_ventas NCV on (NCV.facturaafecta=VT.numerofactura)
) PAGOS_VENTAS
where 1=1 
order by PAGOS_VENTAS.numerofactura, PAGOS_VENTAS.fechapago ;