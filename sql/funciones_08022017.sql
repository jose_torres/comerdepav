--
-- PostgreSQL database dump
--

-- Started on 2017-02-08 21:51:20 VET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 146 (class 1259 OID 331938)
-- Dependencies: 1819 1820 1821 1822 6
-- Name: funcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE funcion (
    id integer NOT NULL,
    grupo_id integer NOT NULL,
    nombre text,
    clave text,
    direccion text,
    nombreimagen text,
    visible text,
    titulopanel text,
    imagenpanel text,
    status text,
    panel text DEFAULT 0,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modulo_id bigint DEFAULT 1 NOT NULL
);


ALTER TABLE public.funcion OWNER TO postgres;

--
-- TOC entry 147 (class 1259 OID 331948)
-- Dependencies: 6 146
-- Name: funcion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE funcion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.funcion_id_seq OWNER TO postgres;

--
-- TOC entry 1829 (class 0 OID 0)
-- Dependencies: 147
-- Name: funcion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE funcion_id_seq OWNED BY funcion.id;


--
-- TOC entry 1830 (class 0 OID 0)
-- Dependencies: 147
-- Name: funcion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('funcion_id_seq', 93, true);


--
-- TOC entry 1823 (class 2604 OID 332009)
-- Dependencies: 147 146
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY funcion ALTER COLUMN id SET DEFAULT nextval('funcion_id_seq'::regclass);


--
-- TOC entry 1826 (class 0 OID 331938)
-- Dependencies: 146
-- Data for Name: funcion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (2, 1, 'Perfiles', NULL, 'perfiles', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '1900-01-01 00:00:00', '2009-06-11 15:05:07', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (10, 1, 'Grabar una Configuracion', NULL, 'configuracions/grabar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:00:03', '2009-06-12 16:00:03', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (4, 1, 'Funcion', NULL, 'funciones', NULL, 'SI', NULL, NULL, '1', '1', 1, '1900-01-01 00:00:00', '2009-06-11 15:05:18', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (5, 1, 'Grupos', NULL, 'grupos', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-03-11 15:42:24', '2009-03-11 15:42:24', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (11, 1, 'Cambiar una Configuracion de permiso', NULL, 'configuracions/cambiar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:00:44', '2009-06-12 16:00:44', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (9, 1, 'Cambio de Contrase&ntilde;a', NULL, 'usuarios/cambiopassword', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-06-09 16:51:27', '2009-06-09 16:51:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (12, 1, 'Eliminar una Configuracion de permiso', NULL, 'configuracions/eliminar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:01:27', '2009-06-12 16:01:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (13, 1, 'buscar una Configuracion de permiso', NULL, 'configuracions/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:08:08', '2009-06-12 16:08:08', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (14, 1, 'Mostrar una Configuracion de permiso', NULL, 'configuracions/mostrar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:14:43', '2009-06-12 16:14:43', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (3, 1, 'Configuracion', NULL, 'configuracions', NULL, 'SI', NULL, NULL, '1', '1', 1, '1900-01-01 00:00:00', '2009-06-11 15:05:12', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (16, 1, 'Incluir Empleados', NULL, 'empleados/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:03:42', '2009-06-12 17:03:42', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (17, 1, 'Editar Empleados', NULL, 'empleados/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:04:11', '2009-06-12 17:04:11', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (18, 1, 'Eliminar Empleados', NULL, 'empleados/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:04:39', '2009-06-12 17:04:39', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (19, 1, 'Buscar un Empleado', NULL, 'empleados/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:05:04', '2009-06-12 17:05:04', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (20, 1, 'Consulta de Empleado', NULL, 'empleados/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:06:49', '2009-06-12 17:06:49', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (21, 1, 'Incluir Funcion', NULL, 'funciones/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:28:13', '2009-06-15 11:28:13', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (22, 1, 'Editar Funcion', NULL, 'funciones/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:28:38', '2009-06-15 11:28:38', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (23, 1, 'Eliminar Funciones', NULL, 'funciones/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:29:10', '2009-06-15 11:29:10', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (24, 1, 'Consulta de Funcion', NULL, 'funciones/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:29:33', '2009-06-15 11:29:33', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (25, 1, 'Buscar una Funcion', NULL, 'funciones/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:34:47', '2009-06-15 11:34:47', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (26, 1, 'Incluir Grupo', NULL, 'grupos/add', NULL, 'NO', NULL, NULL, NULL, '1', 1, '2009-06-15 11:42:15', '2009-06-15 11:42:15', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (27, 1, 'Editar Grupos', NULL, 'grupos/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:42:37', '2009-06-15 11:42:37', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (28, 1, 'Eliminar Grupo', NULL, 'grupos/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:43:01', '2009-06-15 11:43:01', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (29, 1, 'Buscar un Grupo', NULL, 'grupos/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:43:24', '2009-06-15 11:46:32', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (30, 1, 'Consulta de Grupo', NULL, 'grupos/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:44:24', '2009-06-15 11:45:53', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (31, 1, 'Incluir un Perfil', NULL, 'perfiles/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:04:34', '2009-06-15 17:04:34', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (32, 1, 'Editar un Perfil', NULL, 'perfiles/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:04:56', '2009-06-15 17:04:56', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (33, 1, 'Consulta de un Perfil', NULL, 'perfiles/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:05:21', '2009-06-15 17:05:21', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (34, 1, 'Eliminar un perfil', NULL, 'perfiles/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:05:47', '2009-06-15 17:05:47', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (35, 1, 'Buscar un Perfil', NULL, 'perfiles/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:06:14', '2009-06-15 17:06:14', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (36, 1, 'Incluir Usuarios', NULL, 'usuarios/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:45:53', '2009-06-17 09:45:53', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (37, 1, 'Editar Usuarios', NULL, 'usuarios/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:46:12', '2009-06-17 09:46:12', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (38, 1, 'Eliminar Usuarios', NULL, 'usuarios/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:49:27', '2009-06-17 09:49:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (39, 1, 'Consulta de Usuario', NULL, 'usuarios/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:49:49', '2009-06-17 09:49:49', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (40, 1, 'Buscar un usuario', NULL, 'usuarios/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:51:03', '2009-06-17 09:51:03', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (41, 1, 'Actualizar Configuracion', NULL, 'configuracions/actualizar', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2012-05-05 13:41:21', '2012-05-05 13:41:21', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (45, 1, 'Ver Modulo', NULL, 'modulos/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:16:09', '2016-01-02 17:16:09', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (47, 2, 'Oficinas', NULL, 'oficinas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:56:52', '2016-01-03 18:56:52', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (51, 2, 'Eliminar Oficina', NULL, 'oficinas/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:59:59', '2016-01-03 18:59:59', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (7, 2, 'Empleados', NULL, 'empleados', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-06-03 10:59:05', '2016-01-11 18:30:05', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (42, 1, 'Modulos', NULL, 'modulos', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:07:13', '2016-01-02 17:07:13', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (46, 1, 'Eliminar Modulo', NULL, 'modulos/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:16:42', '2016-01-02 17:16:42', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (48, 2, 'Incluir Oficinas', NULL, 'oficinas/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:57:27', '2016-01-03 18:58:26', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (52, 2, 'Buscar Hijos de Oficinas (Ajax)', NULL, 'oficinas/buscarhijos', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 19:51:05', '2016-01-03 19:51:05', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (1, 1, 'Usuarios', '1', 'usuarios', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '1900-01-01 00:00:00', '2011-03-28 16:23:35', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (43, 1, 'Incluir Modulo', NULL, 'modulos/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:15:14', '2016-01-02 17:15:14', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (49, 2, 'Editar Oficina', NULL, 'oficinas/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:58:17', '2016-01-03 18:58:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (44, 1, 'Editar Modulo', NULL, 'modulos/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:15:40', '2016-01-02 17:15:40', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (50, 2, 'Ver Oficina', NULL, 'oficinas/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:59:17', '2016-01-03 18:59:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (54, 2, 'Empresas', NULL, 'empresas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:18:53', '2016-07-13 19:18:53', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (55, 2, 'Incluir Empresas', NULL, 'empresas/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:19:50', '2016-07-13 19:19:50', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (56, 2, 'Editar Empresas', NULL, 'empresas/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:20:17', '2016-07-13 19:20:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (57, 2, 'Visualizar Empresas', NULL, 'empresas/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:20:41', '2016-07-13 19:20:41', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (58, 2, 'Eliminar empresas', NULL, 'empresas/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:25:10', '2016-07-13 19:25:10', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (59, 5, 'Ventas', NULL, 'reportes/ventas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-07-16 07:15:22', '2016-07-16 07:15:22', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (60, 5, 'Buscar Ventas (Ajax)', NULL, 'reportes/buscarventas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-16 07:15:56', '2016-07-16 07:15:56', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (61, 5, 'Compras', NULL, 'reportes/compras', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-07-21 19:02:50', '2016-07-21 19:02:50', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (62, 5, 'Buscar Compras (Ajax)', NULL, 'reportes/buscarcompras', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-21 19:03:08', '2016-07-21 19:03:08', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (63, 4, 'Incluir Retenci&oacute;n', NULL, 'retenciones/add', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:48:36', '2016-08-16 18:50:05', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (64, 4, 'Buscar Facturas (Ajax)', NULL, 'retenciones/buscarfacturas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:50:43', '2016-08-16 18:50:43', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (65, 4, 'Calcular Retencion Compras (Ajax)', NULL, 'retenciones/calcularretencion', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:51:17', '2016-08-16 18:51:17', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (66, 4, 'Guardar Retenci&oacute;n', NULL, 'retenciones/incluir', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:51:57', '2016-08-16 18:51:57', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (68, 4, 'Tr&aacute;nsito', NULL, 'retenciones/transito', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-08-17 11:38:52', '2016-08-17 11:38:52', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (69, 4, 'Cambiar Retencion de Transito a Normal', NULL, 'retenciones/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-17 11:39:27', '2016-08-17 11:39:27', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (70, 4, 'Imprimir Retenci&oacute;n', NULL, 'retenciones/viewpdf', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-18 15:21:04', '2016-08-18 15:21:04', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (71, 4, 'Imprimir Reportes Compras', NULL, 'reportes/viewpdf', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-24 14:49:06', '2016-08-25 09:22:48', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (72, 5, 'Imprimir Reportes Ventas', NULL, 'reportes/viewpdfventas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-25 12:05:58', '2016-08-25 12:05:58', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (73, 5, 'Clientes', NULL, 'reportes/clientes', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-09-26 15:33:57', '2016-09-26 15:33:57', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (74, 5, 'Vendedores', NULL, 'reportes/vendedores', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-09-26 15:40:24', '2016-09-26 15:40:24', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (75, 5, 'Inventario', NULL, 'reportes/inventario', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-10-03 16:17:34', '2016-10-03 16:17:34', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (76, 5, 'Buscar Inventario (Ajax)', NULL, 'reportes/buscarinventario', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-10-03 16:18:22', '2016-10-03 16:18:22', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (77, 5, 'Imprimir Inventario', NULL, 'reportes/viewpdfinv', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-10-03 16:19:05', '2016-10-03 16:19:05', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (78, 5, 'Proveedores', NULL, 'reportes/proveedores', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-10-23 16:56:33', '2016-10-23 16:56:33', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (79, 5, 'Buscar Proveedores Compras (Ajax)', NULL, 'reportes/buscarproveedores', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-10-23 16:57:06', '2016-10-23 16:57:06', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (80, 5, 'Buscar Vendedores (Ajax)', NULL, 'reportes/buscarvendedores', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-10-25 16:03:58', '2016-10-25 16:03:58', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (67, 4, 'Normales', NULL, 'retenciones/index', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:57:31', '2016-11-16 15:16:43', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (81, 4, 'Buscar Retenciones (Ajax)', NULL, 'retenciones/buscarretenciones', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-11-16 15:18:34', '2016-11-16 15:19:30', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (82, 5, 'Impuestos', NULL, 'reportes/impuestos', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-12-07 18:21:06', '2016-12-07 18:21:06', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (83, 5, 'Buscar Impuestos (Ajax)', NULL, 'reportes/buscarimpuestos', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-12-07 18:21:33', '2016-12-07 18:21:33', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (84, 5, 'Imprimir Reportes Impuestos', NULL, 'reportes/viewpdfimpuestos', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-12-11 17:36:34', '2016-12-11 17:36:34', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (86, 20, 'Buscar Ventas Fiscales (Ajax)', NULL, 'ventas/buscarventas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 10:58:36', '2017-02-08 10:58:36', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (87, 20, 'Buscar Maquinas Fiscales (Ajax)', NULL, 'ventas/buscarmaquinas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 14:24:32', '2017-02-08 14:24:32', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (88, 20, 'Asociar Ventas a Maquina Fiscal', NULL, 'ventas/asociar', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 15:42:51', '2017-02-08 15:42:51', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (85, 20, '1. Asociar Ventas Z', NULL, 'ventas/ventasz', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-08 10:51:46', '2017-02-08 16:21:36', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (89, 20, '2. Cerrar Ventas Z', NULL, 'ventas/ventaszcerrar', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-08 16:22:59', '2017-02-08 16:22:59', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (90, 20, 'Cerrar Ventas Z', NULL, 'ventas/cerrar', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 17:31:38', '2017-02-08 17:31:38', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (91, 20, '3. Ventas Z Cerradas', NULL, 'ventas/ventaszreportes', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-08 18:20:18', '2017-02-08 18:20:18', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (92, 20, '4. Abrir Ventas Z ', NULL, 'ventas/ventaszdevolver', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-08 18:35:50', '2017-02-08 18:35:50', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (93, 20, 'Desasociar Ventas Z', NULL, 'ventas/devolver', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 21:37:23', '2017-02-08 21:37:23', 5);


--
-- TOC entry 1825 (class 2606 OID 332022)
-- Dependencies: 146 146
-- Name: funcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY funcion
    ADD CONSTRAINT funcion_pkey PRIMARY KEY (id);


-- Completed on 2017-02-08 21:51:20 VET

--
-- PostgreSQL database dump complete
--

