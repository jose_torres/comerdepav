﻿select fecha,sum(Contado) as contado, sum(MontoNetoCredito) as MontoNetoCredito,sum(MontoIvaCredito) as MontoIvaCredito, 
sum(MontoTotalCredito) as MontoTotalCredito, sum(Costo) as costo, sum(Cobros) as cobros, sum(CobrosDirectos) as CobrosDirectos,
sum(NotaCredito) as notacredito
from
(
select Venta.fecha,
sum(CASE WHEN Venta.estatus::text = 'P'::text or substring(Venta.numerofactura from 1 for 1)='B' THEN Venta.montobruto ELSE 0::real END) as Contado, 
sum(CASE WHEN Venta.estatus::text = 'A'::text or substring(Venta.numerofactura from 1 for 1)='A' THEN Venta.baseimp2 + Venta.montoexento ELSE 0::real END) as MontoNetoCredito, 
sum(CASE WHEN Venta.estatus::text = 'A'::text or substring(Venta.numerofactura from 1 for 1)='A' THEN Venta.ivaimp2 ELSE 0::real END) as MontoIvaCredito, 
sum(CASE WHEN Venta.estatus::text = 'A'::text or substring(Venta.numerofactura from 1 for 1)='A' THEN Venta.baseimp2 + Venta.montoexento + Venta.ivaimp2 ELSE 0::real END) as MontoTotalCredito, 
sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo, 
0 as Cobros, 
COALESCE(sum(Ventapago.monto),0) as CobrosDirectos, 
0 as NotaCredito from ventas as Venta 
inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa and Ventapago.tipopago<>'CREDITO') 
left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc)  
group by Venta.fecha 
union all
select Ccpag.fecha,
0::Real as Contado, 
0::Real as MontoNetoCredito, 
0::Real as MontoIvaCredito, 
0::Real as MontoTotalCredito, 
0::Real as Costo, 
COALESCE(sum(Ccpag.monto),0) as Cobros, 
0 as CobrosDirectos, 
0 as NotaCredito from ventas as Venta 
inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa and Ventapago.tipopago<>'CREDITO') 
left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc ) 
group by Ccpag.fecha
union all
select NCventas.fecha,
0::Real as Contado, 
0::Real as MontoNetoCredito, 
0::Real as MontoIvaCredito, 
0::Real as MontoTotalCredito, 
0::Real as Costo, 
0::Real as Cobros, 
0::Real as CobrosDirectos, 
COALESCE(sum(NCventas.montobruto),0) as NotaCredito from ventas as Venta 
inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa) 
left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura) 
left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa and Ventapago.tipopago<>'CREDITO') 
left join cuentasporcobrar Cxc on (Cxc.codmovimiento=Venta.codventa) 
left join ccpagadas Ccpag on (Cxc.codmovimiento = Ccpag.codmovimientocc and 
Cxc.tipomovimiento = Ccpag.tipomovimientocc and Cxc.codsucursal = Ccpag.codsucursalcc ) 
group by NCventas.fecha
) RESUMEN_VENTA
group by RESUMEN_VENTA.fecha 
order by RESUMEN_VENTA.fecha 