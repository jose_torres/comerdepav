ALTER TABLE condevolucionventa ADD COLUMN conciliado text DEFAULT 'NO';

ALTER TABLE condevolucionventa
   ALTER COLUMN coddevolucion DROP NOT NULL;
ALTER TABLE condevolucionventa
   ALTER COLUMN codsucursal DROP NOT NULL;
ALTER TABLE condevolucionventa
   ALTER COLUMN codventa DROP NOT NULL;
ALTER TABLE condevolucionventa
   ALTER COLUMN codsucursalventa DROP NOT NULL;
ALTER TABLE condevolucionventa
   ALTER COLUMN fecha DROP NOT NULL;

CREATE TABLE concargos
(
  id bigserial,
  codcargo integer NOT NULL,
  codsucursal integer NOT NULL,
  descripcion text NOT NULL,
  fecha date NOT NULL DEFAULT ('now'::text)::date,
  autorizado character varying(50) NOT NULL,
  responsable character varying(50) NOT NULL,
  hora time(6) without time zone NOT NULL DEFAULT ('now'::text)::time with time zone,
  estatus character varying(1) NOT NULL DEFAULT 'A'::character varying
);

CREATE TABLE conproductoscargos
(
  id bigserial,
  codcargo integer NOT NULL,
  codsucursal integer NOT NULL,
  coddeposito integer NOT NULL,
  codproducto integer NOT NULL,
  totalcosto real NOT NULL,
  cantidad real NOT NULL,
  unidad character varying(20) NOT NULL,
  factorconversion integer NOT NULL,
  costounitario real
);

CREATE TABLE condescargos
(
  id bigserial,	
  coddescargo integer NOT NULL,
  codsucursal integer NOT NULL,
  descripcion text NOT NULL,
  fecha date NOT NULL DEFAULT ('now'::text)::date,
  autorizado character varying(50) NOT NULL,
  responsable character varying(50) NOT NULL,
  usointerno boolean NOT NULL,
  hora time(6) without time zone NOT NULL DEFAULT ('now'::text)::time with time zone,
  estatus character varying(1) NOT NULL DEFAULT 'A'::character varying
);

CREATE TABLE conproductosdescargos
(
  id bigserial,
  coddescargo integer NOT NULL,
  codsucursal integer NOT NULL,
  coddeposito integer NOT NULL,
  codproducto integer NOT NULL,
  totalcosto real NOT NULL,
  cantidad real NOT NULL,
  unidad character varying(20) NOT NULL,
  factorconversion integer NOT NULL,
  costounitario real
);
