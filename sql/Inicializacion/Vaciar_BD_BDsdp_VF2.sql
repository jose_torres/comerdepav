--01.- Vaciar Tabla accesso
truncate table accesso;
--02.- iniciar Contador de Accesso
--03.- Vaciar Tabla bancodeposito
truncate table bancodeposito;
--04.- iniciar Contador de bancodeposito
--05.- Vaciar Tabla cargos
truncate table productoscargos;
truncate table cargos;
--06.- iniciar Contador de cargos

--07.- Vaciar Tabla ccabonopago
truncate table ccabonopago;
--08.- iniciar Contador de ccabonopago
--09.- Vaciar Tabla ccabono
truncate table ccabono;
--10.- iniciar Contador de ccabono

--09.- Vaciar Tabla ccfactura
truncate table ccfactura;
--10.- iniciar Contador de ccfactura

--11.- Vaciar Tabla ccnotacredito
truncate table ccnotacredito;
--12.- iniciar Contador de ccfactura

--11.- Vaciar Tabla ccnotadebito
truncate table ccnotadebito;
--11.- Vaciar Tabla ccpagadas
truncate table ccpagadasiva;
truncate table ccpagadas;
-----------------Compras
truncate table retenciones_compras;
truncate table comprasesperaproductos;
truncate table comprasespera;
truncate table compraspagos;
truncate table comprasproductos;
truncate table compras;
truncate table conciliacioncd;
------------------------------
truncate table cpabonopago;
truncate table cpabono;
truncate table cpfactura;

truncate table notacredito_compras;
truncate table notadebito_compras;
truncate table cpnotacredito;

truncate table cpnotadebito;

truncate table cppagadasiva;
truncate table cppagadas;

truncate table movimientoivacc;
truncate table movimientoivacp;

truncate table cuentasporcobrar;
truncate table cuentasporpagar;
---------- Descargos
truncate table traslados;
truncate table productosdescargos;
truncate table descargos;
truncate table despachos;
---------------devolucion Compra
truncate table devolucioncomprapago;
truncate table devolucioncompraproductos;
truncate table devolucioncompra;
---------------devolucion Venta
truncate table devolucionventacreditopagos;
truncate table devolucionventapago;
truncate table devolucionventaproductos;
truncate table devolucionventa;

------------ Historial
truncate table h_producto;
truncate table h_cuentasporpagar;
truncate table h_cuentasporcobrar;
truncate table h_ajuste_sucursal;
truncate table h_ajuste_hproducto;
truncate table h_ajuste;
truncate table productosdepositos_h;
---------inventario
truncate table inventariopedidos;
truncate table inventarioproductos;
truncate table inventario;
---------Nota de Entrega
truncate table nota_entregaproductos;
truncate table nota_entrega;

---------Caja
truncate table succheques;
truncate table sucdepositos;
truncate table sucgastos;
truncate table suctarjetas;
truncate table suctrans;
truncate table cuadrediarios;
truncate table movbancarios;
---------Ventas
truncate table retenciones_ventas_detalles;
truncate table retenciones_ventas;
truncate table retencionespendientes;
truncate table notacredito_ventas;
truncate table ventaesperaproductos;
truncate table ventaespera;
truncate table ventasproductos;
truncate table ventaspagos;
truncate table ventas;
UPDATE numerodocumento SET numero = 1;
UPDATE numerofactura SET numero = 1;
---------Pedidos
truncate table pedidoproductos;
truncate table pedido;
---------Planificacion
truncate table planificaciondecobranzasdetalles;
truncate table planificaciondecobranzas;
truncate table planificaciondepagosdetalles;
truncate table planificaciondepagos;
--------Presupuestos
truncate table presupuestoproductos;
truncate table presupuesto;

--------------Tablas Relacionados a Productos

truncate table departamentos;
truncate table productosdepositos;
truncate table unidades;
truncate table z_cambios;
truncate table z_producto;
truncate table z_productosdepositos;
truncate table z_unidad;

truncate table producto;
---------------------------------------------
