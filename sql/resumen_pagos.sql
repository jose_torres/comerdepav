﻿select PAGOS_COMPRAS.codcompra,PAGOS_COMPRAS.nrodocumento, PAGOS_COMPRAS.codproveedor, PAGOS_COMPRAS.rif,PAGOS_COMPRAS.descripcion, PAGOS_COMPRAS.fecha, PAGOS_COMPRAS.baseimp2,PAGOS_COMPRAS.ivaimp2, PAGOS_COMPRAS.retencion,  PAGOS_COMPRAS.femision, PAGOS_COMPRAS.tipomovimiento, PAGOS_COMPRAS.MontoPago, PAGOS_COMPRAS.estatus,PAGOS_COMPRAS.nrofactura from (
			select VT.codcompra,VT.nrodocumento, VT.codproveedor,CT.rif,CT.descripcion, VT.femision as Fecha, VT.baseimp2,VT.ivaimp2, VT.retencion,VT.femision,'1COMPRAS'::text AS tipomovimiento, 0 as MontoPago, VT.estatus,VT.nrodocumento as nrofactura 
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			union all
			select VT.codcompra,CAP.numero as nrodocumento, VT.codproveedor,CT.rif,CAP.banco as descripcion, 
			CASE WHEN upper(CAP.tipopago)='RETENCION' THEN CAP.fecharetencion 
			WHEN upper(CAP.tipopago)='CAJA' THEN VT.femision  ELSE CCP.fecha 
			END as Fecha,			 VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'2PAGOS-'||upper(CAP.tipopago) AS tipomovimiento, 
			 CAP.Monto as MontoPago, VT.estatus,VT.nrodocumento as nrofactura
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cppagadas  CCP on (CCP.codmovimientocp=VT.codcompra)
			inner join cpabonopago CAP on (CAP.codcpabono=CCP.codmovimientopago)
			union all
			select VT.codcompra, cast(DC.coddevolucioncompra as text) as nrodocumento, VT.codproveedor,CT.rif,
			'DEVOLUCION EN COMPRA' as descripcion, 
			DC.fdevolucion as Fecha,
			 VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'3'||'DEVOLUCION'::text AS tipomovimiento,
			  DC.montobruto as MontoPago, VT.estatus,VT.nrodocumento as nrofactura
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join devolucioncompra DC on (DC.codcompra=VT.codcompra)
			union all
			select VT.codcompra, cast(NC.codcpnotacredito as text) as nrodocumento, VT.codproveedor,CT.rif,
			NC.concepto as descripcion, 
			NC.fecha as Fecha,
			 VT.baseimp2,VT.ivaimp2,VT.retencion,VT.femision,'4'||'NOTA DE CREDITO'::text AS tipomovimiento,
			  NC.monto*-1 as MontoPago, VT.estatus,VT.nrodocumento as nrofactura
			from compras VT 
			inner join proveedores CT on (CT.codproveedor=VT.codproveedor)
			inner join cpnotacredito NC on (NC.nrodocumento=VT.nrodocumento and NC.codproveedor=VT.codproveedor)
			) PAGOS_COMPRAS
			where 1=1
ORDER BY PAGOS_COMPRAS.fecha,PAGOS_COMPRAS.tipomovimiento;