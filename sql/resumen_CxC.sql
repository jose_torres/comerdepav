﻿select Venta.fecha, Venta.numerofactura,
sum(Venta.montobruto) as Monto,
sum(Ventaproducto.cantidad*Ventaproducto.costopromedio) as Costo,
COALESCE(sum(Ventapago.monto),0) as Cobros, 
COALESCE(sum(NCventas.montobruto),0) as NotaCredito

from ventas as Venta
inner join ventasproductos Ventaproducto on (Ventaproducto.codventa=Venta.codventa)
inner join cuentasporcobrar CXC on (CXC.codmovimiento=Venta.codventa)
left join notacredito_ventas NCventas on (NCventas.facturaafecta=Venta.numerofactura)
left join ventaspagos Ventapago on (Ventapago.codventa=Venta.codventa)
group by Venta.fecha,Venta.numerofactura
order by Venta.fecha