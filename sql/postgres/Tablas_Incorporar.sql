--
-- PostgreSQL database dump
--

-- Started on 2017-03-20 14:18:08 VET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 421 (class 1259 OID 335456)
-- Dependencies: 2712 2713 2714 2715 6
-- Name: cuadrediarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cuadrediarios (
    id bigint NOT NULL,
    nro integer NOT NULL,
    fecha date DEFAULT '1900-01-01'::date,
    estatus integer DEFAULT 0 NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone
);


ALTER TABLE public.cuadrediarios OWNER TO postgres;

--
-- TOC entry 420 (class 1259 OID 335454)
-- Dependencies: 421 6
-- Name: cuadrediarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cuadrediarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cuadrediarios_id_seq OWNER TO postgres;

--
-- TOC entry 2749 (class 0 OID 0)
-- Dependencies: 420
-- Name: cuadrediarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cuadrediarios_id_seq OWNED BY cuadrediarios.id;


--
-- TOC entry 2750 (class 0 OID 0)
-- Dependencies: 420
-- Name: cuadrediarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cuadrediarios_id_seq', 1, true);


--
-- TOC entry 429 (class 1259 OID 335747)
-- Dependencies: 2733 2734 6
-- Name: documentotipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documentotipo (
    id bigint NOT NULL,
    descorta text NOT NULL,
    descripcion text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    accion text
);


ALTER TABLE public.documentotipo OWNER TO postgres;

--
-- TOC entry 428 (class 1259 OID 335745)
-- Dependencies: 429 6
-- Name: documentotipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE documentotipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.documentotipo_id_seq OWNER TO postgres;

--
-- TOC entry 2751 (class 0 OID 0)
-- Dependencies: 428
-- Name: documentotipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE documentotipo_id_seq OWNED BY documentotipo.id;


--
-- TOC entry 2752 (class 0 OID 0)
-- Dependencies: 428
-- Name: documentotipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('documentotipo_id_seq', 1, false);


--
-- TOC entry 427 (class 1259 OID 335731)
-- Dependencies: 2727 2728 2729 2730 2731 6
-- Name: movbancarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE movbancarios (
    id bigint NOT NULL,
    banco_id integer,
    cuentasbancaria_id integer,
    codsucursal bigint DEFAULT 0 NOT NULL,
    nrodocumento character varying(30),
    nrocomprobante character varying(15),
    fecha date NOT NULL,
    cedbeneficiario character varying(15),
    nombeneficiario character(250),
    monto double precision,
    saldo double precision,
    observacion character varying(150),
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    conciliado character varying(2),
    documentotipo_id integer,
    chequeo boolean,
    fconciliacion date,
    impreanombrede character varying(250),
    leyenda1 character varying(50),
    leyenda2 character varying(50),
    docasociado character varying(30),
    montoref double precision DEFAULT 0,
    cuadrediario_id integer DEFAULT 0,
    tipodeposito text
);


ALTER TABLE public.movbancarios OWNER TO postgres;

--
-- TOC entry 426 (class 1259 OID 335729)
-- Dependencies: 6 427
-- Name: movbancarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE movbancarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.movbancarios_id_seq OWNER TO postgres;

--
-- TOC entry 2753 (class 0 OID 0)
-- Dependencies: 426
-- Name: movbancarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE movbancarios_id_seq OWNED BY movbancarios.id;


--
-- TOC entry 2754 (class 0 OID 0)
-- Dependencies: 426
-- Name: movbancarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('movbancarios_id_seq', 4, true);


--
-- TOC entry 425 (class 1259 OID 335715)
-- Dependencies: 2717 2718 2719 2720 2721 2722 2723 2724 2725 6
-- Name: sucdepositos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sucdepositos (
    id bigint NOT NULL,
    movbancario_id bigint DEFAULT 0 NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    monto double precision DEFAULT 0.00 NOT NULL,
    fecha date,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    caja_id bigint DEFAULT 0 NOT NULL,
    cuadrediario_id bigint DEFAULT 0 NOT NULL,
    valor double precision DEFAULT 0 NOT NULL,
    cantidad double precision DEFAULT 0
);


ALTER TABLE public.sucdepositos OWNER TO postgres;

--
-- TOC entry 424 (class 1259 OID 335713)
-- Dependencies: 425 6
-- Name: sucdepositos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sucdepositos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.sucdepositos_id_seq OWNER TO postgres;

--
-- TOC entry 2755 (class 0 OID 0)
-- Dependencies: 424
-- Name: sucdepositos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sucdepositos_id_seq OWNED BY sucdepositos.id;


--
-- TOC entry 2756 (class 0 OID 0)
-- Dependencies: 424
-- Name: sucdepositos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sucdepositos_id_seq', 40, true);


--
-- TOC entry 2711 (class 2604 OID 335459)
-- Dependencies: 420 421 421
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cuadrediarios ALTER COLUMN id SET DEFAULT nextval('cuadrediarios_id_seq'::regclass);


--
-- TOC entry 2732 (class 2604 OID 335750)
-- Dependencies: 429 428 429
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documentotipo ALTER COLUMN id SET DEFAULT nextval('documentotipo_id_seq'::regclass);


--
-- TOC entry 2726 (class 2604 OID 335734)
-- Dependencies: 427 426 427
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movbancarios ALTER COLUMN id SET DEFAULT nextval('movbancarios_id_seq'::regclass);


--
-- TOC entry 2716 (class 2604 OID 335718)
-- Dependencies: 425 424 425
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sucdepositos ALTER COLUMN id SET DEFAULT nextval('sucdepositos_id_seq'::regclass);


--
-- TOC entry 2743 (class 0 OID 335456)
-- Dependencies: 421
-- Data for Name: cuadrediarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO cuadrediarios (id, nro, fecha, estatus, created, modified) VALUES (1, 1, '2016-12-14', 1, '2017-03-19 16:57:10', '2017-03-19 16:57:10');


--
-- TOC entry 2746 (class 0 OID 335747)
-- Dependencies: 429
-- Data for Name: documentotipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (1, 'DP', 'DEPOSITO', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (2, 'NC', 'NOTA DE CREDITO', '2011-03-17 08:14:59', '2011-03-17 08:14:59', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (3, 'CH', 'CHEQUE', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'DISMINUYE');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (4, 'ND', 'NOTA DE DEBITO', '2011-03-21 08:40:11', '2011-03-21 17:03:02', 'DISMINUYE');


--
-- TOC entry 2745 (class 0 OID 335731)
-- Dependencies: 427
-- Data for Name: movbancarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO movbancarios (id, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito) VALUES (2, 8, 3, 4, '', NULL, '2016-12-14', NULL, NULL, 132500, NULL, ' Deposito de la Sucursal nro:4 a la Fecha:2016-12-14', '2017-03-20 13:47:51', '2017-03-20 13:47:51', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 'EFECTIVO');
INSERT INTO movbancarios (id, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito) VALUES (3, 6, 4, 4, '', NULL, '2016-12-14', NULL, NULL, 113500, NULL, ' Deposito de la Sucursal nro:4 a la Fecha:2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 'EFECTIVO');


--
-- TOC entry 2744 (class 0 OID 335715)
-- Dependencies: 425
-- Data for Name: sucdepositos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (2, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 50, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (3, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 100, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (4, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 2, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (5, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 5, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (6, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 10, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (7, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 20, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (8, 2, 4, 12500, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 50, 250);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (9, 2, 4, 10000, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 100, 100);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (10, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 500, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (11, 2, 4, 10000, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 1000, 10);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (12, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 2000, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (13, 2, 4, 0, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 5000, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (14, 2, 4, 100000, '2016-12-14', '2017-03-20 13:47:52', '2017-03-20 13:47:52', 1, 1, 10000, 10);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (15, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 50, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (16, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 100, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (17, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 2, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (18, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 5, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (19, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 10, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (20, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 20, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (21, 3, 4, 1000, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 50, 20);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (22, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 100, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (23, 3, 4, 12500, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 500, 25);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (24, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 1000, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (25, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 2000, 0);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (26, 3, 4, 100000, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 5000, 20);
INSERT INTO sucdepositos (id, movbancario_id, codsucursal, monto, fecha, created, modified, caja_id, cuadrediario_id, valor, cantidad) VALUES (27, 3, 4, 0, '2016-12-14', '2017-03-20 14:09:45', '2017-03-20 14:09:45', 1, 1, 10000, 0);


--
-- TOC entry 2736 (class 2606 OID 335465)
-- Dependencies: 421 421
-- Name: cuadrediarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cuadrediarios
    ADD CONSTRAINT cuadrediarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2742 (class 2606 OID 335757)
-- Dependencies: 429 429
-- Name: documentotipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY documentotipo
    ADD CONSTRAINT documentotipo_pkey PRIMARY KEY (id);


--
-- TOC entry 2740 (class 2606 OID 335744)
-- Dependencies: 427 427
-- Name: movbancarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY movbancarios
    ADD CONSTRAINT movbancarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2738 (class 2606 OID 335728)
-- Dependencies: 425 425
-- Name: sucdepositos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sucdepositos
    ADD CONSTRAINT sucdepositos_pkey PRIMARY KEY (id);


-- Completed on 2017-03-20 14:18:08 VET

--
-- PostgreSQL database dump complete
--

