--
-- PostgreSQL database dump
--

-- Started on 2017-03-21 08:16:02 VET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 423 (class 1259 OID 335679)
-- Dependencies: 2712 2713 2714 6
-- Name: dinerodenominaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dinerodenominaciones (
    id bigint NOT NULL,
    denominacion text NOT NULL,
    valor double precision,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    fin text DEFAULT 'DINERO'::text NOT NULL
);


ALTER TABLE public.dinerodenominaciones OWNER TO postgres;

--
-- TOC entry 422 (class 1259 OID 335677)
-- Dependencies: 6 423
-- Name: dinerodenominaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dinerodenominaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.dinerodenominaciones_id_seq OWNER TO postgres;

--
-- TOC entry 2720 (class 0 OID 0)
-- Dependencies: 422
-- Name: dinerodenominaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dinerodenominaciones_id_seq OWNED BY dinerodenominaciones.id;


--
-- TOC entry 2721 (class 0 OID 0)
-- Dependencies: 422
-- Name: dinerodenominaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dinerodenominaciones_id_seq', 20, true);


--
-- TOC entry 2711 (class 2604 OID 335689)
-- Dependencies: 423 422 423
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dinerodenominaciones ALTER COLUMN id SET DEFAULT nextval('dinerodenominaciones_id_seq'::regclass);


--
-- TOC entry 2717 (class 0 OID 335679)
-- Dependencies: 423
-- Data for Name: dinerodenominaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (7, '2', 2, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (8, '5', 5, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (9, '10', 10, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (10, '20', 20, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (11, '50', 50, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (12, '100', 100, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (13, '500', 500, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (14, '1000', 1000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (15, '2000', 2000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (16, '5000', 5000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (17, '10000', 10000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (18, '20000', 20000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (19, '50', 50, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'MONEDA');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (1, '0,05', 0.050000000000000003, '1900-01-01 00:00:00', '2012-05-03 11:55:07', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (2, '0,10', 0.10000000000000001, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (3, '0,125', 0.125, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (4, '0,2', 0.20000000000000001, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (5, '0,5', 0.5, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (6, '1', 1, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (20, '100', 100, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'MONEDA');


--
-- TOC entry 2716 (class 2606 OID 335691)
-- Dependencies: 423 423
-- Name: dinerodenominaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dinerodenominaciones
    ADD CONSTRAINT dinerodenominaciones_pkey PRIMARY KEY (id);


-- Completed on 2017-03-21 08:16:02 VET

--
-- PostgreSQL database dump complete
--

