--
-- PostgreSQL database dump
--

-- Started on 2016-07-16 10:39:53 VET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 140 (class 1259 OID 327924)
-- Dependencies: 1828 1829 1830 6
-- Name: configuracion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE configuracion (
    id integer NOT NULL,
    perfil_id integer NOT NULL,
    funcion_id integer NOT NULL,
    incluir text,
    modificar text,
    consultar text,
    eliminar text,
    status text DEFAULT '1'::text NOT NULL,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.configuracion OWNER TO postgres;

--
-- TOC entry 141 (class 1259 OID 327933)
-- Dependencies: 140 6
-- Name: configuracion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE configuracion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.configuracion_id_seq OWNER TO postgres;

--
-- TOC entry 1897 (class 0 OID 0)
-- Dependencies: 141
-- Name: configuracion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE configuracion_id_seq OWNED BY configuracion.id;


--
-- TOC entry 1898 (class 0 OID 0)
-- Dependencies: 141
-- Name: configuracion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('configuracion_id_seq', 286, true);


--
-- TOC entry 142 (class 1259 OID 327935)
-- Dependencies: 1832 1833 1834 1835 6
-- Name: empleados; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE empleados (
    id integer NOT NULL,
    cedula integer,
    nombre text,
    tipotrab text,
    codestructura integer DEFAULT 0 NOT NULL,
    coescala text,
    conivel text,
    codcategoria text,
    ded text,
    nivel integer,
    correoelectronico text,
    catpersonale_id text,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    cargo_id integer,
    oficina_id integer DEFAULT 0
);


ALTER TABLE public.empleados OWNER TO postgres;

--
-- TOC entry 143 (class 1259 OID 327945)
-- Dependencies: 6 142
-- Name: empleados_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE empleados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.empleados_id_seq OWNER TO postgres;

--
-- TOC entry 1899 (class 0 OID 0)
-- Dependencies: 143
-- Name: empleados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE empleados_id_seq OWNED BY empleados.id;


--
-- TOC entry 1900 (class 0 OID 0)
-- Dependencies: 143
-- Name: empleados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('empleados_id_seq', 7, true);


--
-- TOC entry 144 (class 1259 OID 327947)
-- Dependencies: 1837 1838 6
-- Name: empresas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE empresas (
    id integer NOT NULL,
    rif text,
    nombre text,
    descripcion text,
    logo_izquierdo text,
    logo_derecho text,
    telefono text,
    status text,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.empresas OWNER TO postgres;

--
-- TOC entry 145 (class 1259 OID 327955)
-- Dependencies: 6 144
-- Name: empresas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE empresas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.empresas_id_seq OWNER TO postgres;

--
-- TOC entry 1901 (class 0 OID 0)
-- Dependencies: 145
-- Name: empresas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE empresas_id_seq OWNED BY empresas.id;


--
-- TOC entry 1902 (class 0 OID 0)
-- Dependencies: 145
-- Name: empresas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('empresas_id_seq', 1, true);


--
-- TOC entry 146 (class 1259 OID 327957)
-- Dependencies: 1840 1841 1842 1843 6
-- Name: funcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE funcion (
    id integer NOT NULL,
    grupo_id integer NOT NULL,
    nombre text,
    clave text,
    direccion text,
    nombreimagen text,
    visible text,
    titulopanel text,
    imagenpanel text,
    status text,
    panel text DEFAULT 0,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modulo_id bigint DEFAULT 1 NOT NULL
);


ALTER TABLE public.funcion OWNER TO postgres;

--
-- TOC entry 147 (class 1259 OID 327967)
-- Dependencies: 146 6
-- Name: funcion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE funcion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.funcion_id_seq OWNER TO postgres;

--
-- TOC entry 1903 (class 0 OID 0)
-- Dependencies: 147
-- Name: funcion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE funcion_id_seq OWNED BY funcion.id;


--
-- TOC entry 1904 (class 0 OID 0)
-- Dependencies: 147
-- Name: funcion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('funcion_id_seq', 60, true);


--
-- TOC entry 148 (class 1259 OID 327969)
-- Dependencies: 1845 1846 6
-- Name: grupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE grupo (
    id integer NOT NULL,
    nombre text,
    status text,
    usuario_id integer,
    imagen text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.grupo OWNER TO postgres;

--
-- TOC entry 149 (class 1259 OID 327977)
-- Dependencies: 6 148
-- Name: grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE grupo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.grupo_id_seq OWNER TO postgres;

--
-- TOC entry 1905 (class 0 OID 0)
-- Dependencies: 149
-- Name: grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE grupo_id_seq OWNED BY grupo.id;


--
-- TOC entry 1906 (class 0 OID 0)
-- Dependencies: 149
-- Name: grupo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('grupo_id_seq', 19, true);


--
-- TOC entry 150 (class 1259 OID 327979)
-- Dependencies: 1848 1849 6
-- Name: modulos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE modulos (
    id integer NOT NULL,
    nombre text,
    status text,
    imagen text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.modulos OWNER TO postgres;

--
-- TOC entry 151 (class 1259 OID 327987)
-- Dependencies: 6 150
-- Name: modulos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE modulos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.modulos_id_seq OWNER TO postgres;

--
-- TOC entry 1907 (class 0 OID 0)
-- Dependencies: 151
-- Name: modulos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE modulos_id_seq OWNED BY modulos.id;


--
-- TOC entry 1908 (class 0 OID 0)
-- Dependencies: 151
-- Name: modulos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('modulos_id_seq', 4, true);


--
-- TOC entry 152 (class 1259 OID 327989)
-- Dependencies: 1851 1852 1853 1854 6
-- Name: oficinas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oficinas (
    id integer NOT NULL,
    parent_id integer DEFAULT 0,
    descripcion text,
    familia integer DEFAULT 0,
    status text,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    siglas text,
    lft bigint,
    rght bigint
);


ALTER TABLE public.oficinas OWNER TO postgres;

--
-- TOC entry 153 (class 1259 OID 327999)
-- Dependencies: 152 6
-- Name: oficinas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE oficinas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.oficinas_id_seq OWNER TO postgres;

--
-- TOC entry 1909 (class 0 OID 0)
-- Dependencies: 153
-- Name: oficinas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE oficinas_id_seq OWNED BY oficinas.id;


--
-- TOC entry 1910 (class 0 OID 0)
-- Dependencies: 153
-- Name: oficinas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('oficinas_id_seq', 12, true);


--
-- TOC entry 154 (class 1259 OID 328001)
-- Dependencies: 1856 1857 1858 1859 6
-- Name: perfil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE perfil (
    id integer NOT NULL,
    id_padre integer DEFAULT 0,
    descripcion text,
    familia integer DEFAULT 0,
    status text,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.perfil OWNER TO postgres;

--
-- TOC entry 155 (class 1259 OID 328011)
-- Dependencies: 6 154
-- Name: perfil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE perfil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.perfil_id_seq OWNER TO postgres;

--
-- TOC entry 1911 (class 0 OID 0)
-- Dependencies: 155
-- Name: perfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE perfil_id_seq OWNED BY perfil.id;


--
-- TOC entry 1912 (class 0 OID 0)
-- Dependencies: 155
-- Name: perfil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('perfil_id_seq', 9, true);


--
-- TOC entry 156 (class 1259 OID 328013)
-- Dependencies: 1861 1862 1863 6
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    id integer NOT NULL,
    empleado_id integer NOT NULL,
    perfil_id integer NOT NULL,
    usuario text,
    clave text,
    status text,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    oficina_id bigint DEFAULT 1 NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 157 (class 1259 OID 328022)
-- Dependencies: 156 6
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- TOC entry 1913 (class 0 OID 0)
-- Dependencies: 157
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_id_seq OWNED BY usuario.id;


--
-- TOC entry 1914 (class 0 OID 0)
-- Dependencies: 157
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_id_seq', 6, true);


--
-- TOC entry 1831 (class 2604 OID 328024)
-- Dependencies: 141 140
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY configuracion ALTER COLUMN id SET DEFAULT nextval('configuracion_id_seq'::regclass);


--
-- TOC entry 1836 (class 2604 OID 328025)
-- Dependencies: 143 142
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleados ALTER COLUMN id SET DEFAULT nextval('empleados_id_seq'::regclass);


--
-- TOC entry 1839 (class 2604 OID 328026)
-- Dependencies: 145 144
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empresas ALTER COLUMN id SET DEFAULT nextval('empresas_id_seq'::regclass);


--
-- TOC entry 1844 (class 2604 OID 328027)
-- Dependencies: 147 146
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY funcion ALTER COLUMN id SET DEFAULT nextval('funcion_id_seq'::regclass);


--
-- TOC entry 1847 (class 2604 OID 328028)
-- Dependencies: 149 148
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY grupo ALTER COLUMN id SET DEFAULT nextval('grupo_id_seq'::regclass);


--
-- TOC entry 1850 (class 2604 OID 328029)
-- Dependencies: 151 150
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulos ALTER COLUMN id SET DEFAULT nextval('modulos_id_seq'::regclass);


--
-- TOC entry 1855 (class 2604 OID 328030)
-- Dependencies: 153 152
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oficinas ALTER COLUMN id SET DEFAULT nextval('oficinas_id_seq'::regclass);


--
-- TOC entry 1860 (class 2604 OID 328031)
-- Dependencies: 155 154
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil ALTER COLUMN id SET DEFAULT nextval('perfil_id_seq'::regclass);


--
-- TOC entry 1864 (class 2604 OID 328032)
-- Dependencies: 157 156
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN id SET DEFAULT nextval('usuario_id_seq'::regclass);


--
-- TOC entry 1883 (class 0 OID 327924)
-- Dependencies: 140
-- Data for Name: configuracion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (117, 2, 9, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:00', '2011-01-30 20:47:00');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (118, 2, 36, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:05', '2011-01-30 20:47:05');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (119, 2, 37, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:09', '2011-01-30 20:47:09');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (120, 2, 38, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:16', '2011-01-30 20:47:16');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (121, 2, 39, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:20', '2011-01-30 20:47:20');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (122, 2, 40, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:24', '2011-01-30 20:47:24');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (123, 2, 2, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:31', '2011-01-30 20:47:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (124, 2, 31, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:36', '2011-01-30 20:47:36');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (125, 2, 32, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:51', '2011-01-30 20:47:51');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (126, 2, 33, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:55', '2011-01-30 20:47:55');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (127, 2, 34, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:02', '2011-01-30 20:48:02');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (128, 2, 35, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:06', '2011-01-30 20:48:06');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (129, 2, 3, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:19', '2011-01-30 20:48:19');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (130, 2, 10, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:25', '2011-01-30 20:48:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (131, 2, 11, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:29', '2011-01-30 20:48:29');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (132, 2, 12, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:35', '2011-01-30 20:48:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (133, 2, 13, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:42', '2011-01-30 20:48:42');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (134, 2, 14, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:48', '2011-01-30 20:48:48');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (135, 2, 7, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:14', '2011-01-30 20:49:14');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (136, 2, 16, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:18', '2011-01-30 20:49:18');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (137, 2, 17, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:22', '2011-01-30 20:49:22');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (138, 2, 18, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:26', '2011-01-30 20:49:26');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (139, 2, 19, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:30', '2011-01-30 20:49:30');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (140, 2, 20, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:34', '2011-01-30 20:49:34');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (141, 2, 41, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:40', '2011-01-30 20:49:40');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (9, 1, 15, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-01-02 11:33:26');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (232, 3, 3, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (233, 3, 41, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (234, 3, 13, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (235, 3, 11, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (236, 3, 12, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (237, 3, 10, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (238, 3, 14, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (239, 3, 7, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (240, 3, 16, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (241, 3, 19, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (242, 3, 18, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (243, 3, 17, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (244, 3, 20, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (245, 3, 4, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (246, 3, 21, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (247, 3, 25, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (248, 3, 23, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (249, 3, 22, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (250, 3, 24, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (251, 3, 5, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (252, 3, 26, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (253, 3, 29, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (254, 3, 28, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (255, 3, 27, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (256, 3, 30, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (257, 3, 42, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (258, 3, 43, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (259, 3, 46, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (260, 3, 44, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (261, 3, 45, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (262, 3, 47, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (263, 3, 48, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (264, 3, 52, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (265, 3, 51, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (266, 3, 49, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (267, 3, 50, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (268, 3, 2, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (269, 3, 31, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (270, 3, 35, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (271, 3, 34, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (272, 3, 32, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (273, 3, 33, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (274, 3, 1, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (275, 3, 36, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (276, 3, 40, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (220, 3, 9, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 22:46:41', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (277, 3, 38, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:33');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (278, 3, 37, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:33');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (279, 3, 39, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:33');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (2, 1, 3, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (39, 1, 41, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:17:15', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (7, 1, 13, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (5, 1, 11, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (6, 1, 12, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (4, 1, 10, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (8, 1, 14, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (33, 1, 7, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:55', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (34, 1, 16, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:00', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (37, 1, 19, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:20', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (36, 1, 18, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:15', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (35, 1, 17, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:06', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (38, 1, 20, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:26', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (280, 1, 54, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:19:08', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (281, 1, 55, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:26:01', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (282, 1, 58, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:26:01', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (283, 1, 56, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:26:01', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (284, 1, 57, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:26:01', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (21, 1, 4, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:44', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (22, 1, 21, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:48', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (26, 1, 25, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:08', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (24, 1, 23, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:58', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (23, 1, 22, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:52', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (25, 1, 24, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:02', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (27, 1, 5, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:18', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (28, 1, 26, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:23', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (31, 1, 29, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:46', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (30, 1, 28, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:37', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (29, 1, 27, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:31', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (32, 1, 30, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:50', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (221, 1, 42, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:07:22', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (222, 1, 43, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:17:09', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (223, 1, 46, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:17:09', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (224, 1, 44, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:17:09', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (225, 1, 45, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:17:09', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (226, 1, 47, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (227, 1, 48, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (231, 1, 52, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:51:14', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (228, 1, 51, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (229, 1, 49, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (230, 1, 50, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (10, 1, 2, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-15 22:58:33', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (11, 1, 31, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:17:49', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (15, 1, 35, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:18:07', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (14, 1, 34, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:18:03', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (12, 1, 32, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:17:53', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (13, 1, 33, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:17:58', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (285, 1, 60, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-16 07:16:10', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (286, 1, 59, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-16 07:16:10', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (1, 1, 1, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (17, 1, 36, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:37:07', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (20, 1, 40, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:24', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (16, 1, 9, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:34:50', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (18, 1, 38, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:13', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (218, 1, 37, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 22:30:42', '2016-07-16 07:16:10');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (19, 1, 39, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:20', '2016-07-16 07:16:10');


--
-- TOC entry 1884 (class 0 OID 327935)
-- Dependencies: 142
-- Data for Name: empleados; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO empleados (id, cedula, nombre, tipotrab, codestructura, coescala, conivel, codcategoria, ded, nivel, correoelectronico, catpersonale_id, created, modified, cargo_id, oficina_id) VALUES (3, 1234, 'Giusseppe De Palma', 'ADM', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-01-30 19:02:25', '2016-07-14 07:13:15', NULL, 1);
INSERT INTO empleados (id, cedula, nombre, tipotrab, codestructura, coescala, conivel, codcategoria, ded, nivel, correoelectronico, catpersonale_id, created, modified, cargo_id, oficina_id) VALUES (5, 90001, 'LEIBAN ALBERTY RIVERO COLMENAREZ', 'ADM', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-02-14 11:45:56', '2016-07-16 07:14:13', NULL, 10);


--
-- TOC entry 1885 (class 0 OID 327947)
-- Dependencies: 144
-- Data for Name: empresas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO empresas (id, rif, nombre, descripcion, logo_izquierdo, logo_derecho, telefono, status, created, modified) VALUES (1, 'J308202320', 'COMERDEPA LA 21 C.A.', 'CARRERA 22 ENTRE 26 Y 27 CASA NRO 26-51', '', '', '0251-2321253', NULL, '2016-07-13 19:29:13', '2016-07-13 19:29:13');


--
-- TOC entry 1886 (class 0 OID 327957)
-- Dependencies: 146
-- Data for Name: funcion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (2, 1, 'Perfiles', NULL, 'perfiles', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '1900-01-01 00:00:00', '2009-06-11 15:05:07', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (10, 1, 'Grabar una Configuracion', NULL, 'configuracions/grabar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:00:03', '2009-06-12 16:00:03', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (4, 1, 'Funcion', NULL, 'funciones', NULL, 'SI', NULL, NULL, '1', '1', 1, '1900-01-01 00:00:00', '2009-06-11 15:05:18', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (5, 1, 'Grupos', NULL, 'grupos', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-03-11 15:42:24', '2009-03-11 15:42:24', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (11, 1, 'Cambiar una Configuracion de permiso', NULL, 'configuracions/cambiar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:00:44', '2009-06-12 16:00:44', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (9, 1, 'Cambio de Contrase&ntilde;a', NULL, 'usuarios/cambiopassword', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-06-09 16:51:27', '2009-06-09 16:51:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (12, 1, 'Eliminar una Configuracion de permiso', NULL, 'configuracions/eliminar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:01:27', '2009-06-12 16:01:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (13, 1, 'buscar una Configuracion de permiso', NULL, 'configuracions/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:08:08', '2009-06-12 16:08:08', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (14, 1, 'Mostrar una Configuracion de permiso', NULL, 'configuracions/mostrar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:14:43', '2009-06-12 16:14:43', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (3, 1, 'Configuracion', NULL, 'configuracions', NULL, 'SI', NULL, NULL, '1', '1', 1, '1900-01-01 00:00:00', '2009-06-11 15:05:12', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (16, 1, 'Incluir Empleados', NULL, 'empleados/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:03:42', '2009-06-12 17:03:42', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (17, 1, 'Editar Empleados', NULL, 'empleados/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:04:11', '2009-06-12 17:04:11', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (18, 1, 'Eliminar Empleados', NULL, 'empleados/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:04:39', '2009-06-12 17:04:39', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (19, 1, 'Buscar un Empleado', NULL, 'empleados/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:05:04', '2009-06-12 17:05:04', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (20, 1, 'Consulta de Empleado', NULL, 'empleados/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:06:49', '2009-06-12 17:06:49', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (21, 1, 'Incluir Funcion', NULL, 'funciones/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:28:13', '2009-06-15 11:28:13', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (22, 1, 'Editar Funcion', NULL, 'funciones/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:28:38', '2009-06-15 11:28:38', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (23, 1, 'Eliminar Funciones', NULL, 'funciones/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:29:10', '2009-06-15 11:29:10', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (24, 1, 'Consulta de Funcion', NULL, 'funciones/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:29:33', '2009-06-15 11:29:33', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (25, 1, 'Buscar una Funcion', NULL, 'funciones/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:34:47', '2009-06-15 11:34:47', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (26, 1, 'Incluir Grupo', NULL, 'grupos/add', NULL, 'NO', NULL, NULL, NULL, '1', 1, '2009-06-15 11:42:15', '2009-06-15 11:42:15', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (27, 1, 'Editar Grupos', NULL, 'grupos/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:42:37', '2009-06-15 11:42:37', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (28, 1, 'Eliminar Grupo', NULL, 'grupos/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:43:01', '2009-06-15 11:43:01', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (29, 1, 'Buscar un Grupo', NULL, 'grupos/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:43:24', '2009-06-15 11:46:32', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (30, 1, 'Consulta de Grupo', NULL, 'grupos/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:44:24', '2009-06-15 11:45:53', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (31, 1, 'Incluir un Perfil', NULL, 'perfiles/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:04:34', '2009-06-15 17:04:34', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (32, 1, 'Editar un Perfil', NULL, 'perfiles/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:04:56', '2009-06-15 17:04:56', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (33, 1, 'Consulta de un Perfil', NULL, 'perfiles/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:05:21', '2009-06-15 17:05:21', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (34, 1, 'Eliminar un perfil', NULL, 'perfiles/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:05:47', '2009-06-15 17:05:47', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (35, 1, 'Buscar un Perfil', NULL, 'perfiles/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:06:14', '2009-06-15 17:06:14', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (36, 1, 'Incluir Usuarios', NULL, 'usuarios/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:45:53', '2009-06-17 09:45:53', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (37, 1, 'Editar Usuarios', NULL, 'usuarios/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:46:12', '2009-06-17 09:46:12', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (38, 1, 'Eliminar Usuarios', NULL, 'usuarios/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:49:27', '2009-06-17 09:49:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (39, 1, 'Consulta de Usuario', NULL, 'usuarios/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:49:49', '2009-06-17 09:49:49', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (40, 1, 'Buscar un usuario', NULL, 'usuarios/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:51:03', '2009-06-17 09:51:03', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (41, 1, 'Actualizar Configuracion', NULL, 'configuracions/actualizar', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2012-05-05 13:41:21', '2012-05-05 13:41:21', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (45, 1, 'Ver Modulo', NULL, 'modulos/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:16:09', '2016-01-02 17:16:09', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (47, 2, 'Oficinas', NULL, 'oficinas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:56:52', '2016-01-03 18:56:52', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (51, 2, 'Eliminar Oficina', NULL, 'oficinas/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:59:59', '2016-01-03 18:59:59', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (7, 2, 'Empleados', NULL, 'empleados', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-06-03 10:59:05', '2016-01-11 18:30:05', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (42, 1, 'Modulos', NULL, 'modulos', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:07:13', '2016-01-02 17:07:13', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (46, 1, 'Eliminar Modulo', NULL, 'modulos/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:16:42', '2016-01-02 17:16:42', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (48, 2, 'Incluir Oficinas', NULL, 'oficinas/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:57:27', '2016-01-03 18:58:26', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (52, 2, 'Buscar Hijos de Oficinas (Ajax)', NULL, 'oficinas/buscarhijos', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 19:51:05', '2016-01-03 19:51:05', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (1, 1, 'Usuarios', '1', 'usuarios', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '1900-01-01 00:00:00', '2011-03-28 16:23:35', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (43, 1, 'Incluir Modulo', NULL, 'modulos/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:15:14', '2016-01-02 17:15:14', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (49, 2, 'Editar Oficina', NULL, 'oficinas/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:58:17', '2016-01-03 18:58:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (44, 1, 'Editar Modulo', NULL, 'modulos/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:15:40', '2016-01-02 17:15:40', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (50, 2, 'Ver Oficina', NULL, 'oficinas/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:59:17', '2016-01-03 18:59:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (54, 2, 'Empresas', NULL, 'empresas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:18:53', '2016-07-13 19:18:53', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (55, 2, 'Incluir Empresas', NULL, 'empresas/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:19:50', '2016-07-13 19:19:50', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (56, 2, 'Editar Empresas', NULL, 'empresas/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:20:17', '2016-07-13 19:20:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (57, 2, 'Visualizar Empresas', NULL, 'empresas/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:20:41', '2016-07-13 19:20:41', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (58, 2, 'Eliminar empresas', NULL, 'empresas/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:25:10', '2016-07-13 19:25:10', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (59, 5, 'Ventas', NULL, 'reportes/ventas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-07-16 07:15:22', '2016-07-16 07:15:22', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (60, 5, 'Buscar Ventas (Ajax)', NULL, 'reportes/buscarventas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-16 07:15:56', '2016-07-16 07:15:56', 2);


--
-- TOC entry 1887 (class 0 OID 327969)
-- Dependencies: 148
-- Data for Name: grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (4, 'Sistemas', NULL, NULL, 'applications-system.png', '2009-04-21 09:15:44', '2009-06-08 17:13:31');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (1, 'Seguridad', '1', 1, 'lock.png', '1900-01-01 00:00:00', '2011-09-24 21:37:46');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (2, 'Configuraciones', '1', 1, 'setting.png', '1900-01-01 00:00:00', '2011-09-24 21:45:45');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (3, 'Cajas', NULL, NULL, 'agt_business.png', '2009-03-24 14:30:30', '2011-09-24 21:59:19');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (10, 'Bancos', NULL, NULL, 'money.png', '2011-03-28 17:32:32', '2011-09-24 22:09:56');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (5, 'Reporte', NULL, NULL, 'internet-news-reader.png', '2009-06-08 11:19:18', '2012-04-12 13:59:23');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (8, 'Procesos', NULL, NULL, 'x-office-spreadsheet-template.png', '2011-03-28 17:31:38', '2014-10-29 15:45:32');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (12, 'Ctas_x_Cobrar', NULL, NULL, 'system-file-manager.png', '2015-07-11 11:09:30', '2015-07-11 11:09:30');


--
-- TOC entry 1888 (class 0 OID 327979)
-- Dependencies: 150
-- Data for Name: modulos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO modulos (id, nombre, status, imagen, created, modified) VALUES (1, 'Seguridad', '1', 'lock.png', '1900-01-01 00:00:00', '1900-01-01 00:00:00');
INSERT INTO modulos (id, nombre, status, imagen, created, modified) VALUES (3, 'Prueba', NULL, 'edit-copy2.png', '2016-01-03 16:09:17', '2016-01-03 16:09:17');
INSERT INTO modulos (id, nombre, status, imagen, created, modified) VALUES (2, 'Reportes', NULL, 'modulo_documentos.png', '2016-01-02 17:34:06', '2016-07-13 19:25:30');


--
-- TOC entry 1889 (class 0 OID 327989)
-- Dependencies: 152
-- Data for Name: oficinas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght) VALUES (10, 1, 'Administracion', 0, NULL, NULL, '2016-01-10 16:06:49', '2016-07-14 07:14:39', 'Admon', 0, 1);
INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght) VALUES (1, NULL, 'Gerencia', 0, NULL, NULL, '2016-01-03 19:51:25', '2016-07-14 07:14:11', 'GR', 4, 4);
INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght) VALUES (6, 2, 'Coordinacion Nacional de Producción de Operaciones', 0, NULL, NULL, '2016-01-06 00:50:03', '2016-01-06 01:20:26', 'CNPO', 4, 4);
INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght) VALUES (3, 1, 'Almacen', 0, NULL, NULL, '2016-01-06 00:44:19', '2016-07-14 07:16:23', 'Alm', 4, 10);
INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght) VALUES (12, 1, 'Detal', 0, NULL, NULL, '2016-07-14 07:16:49', '2016-07-14 07:16:49', 'Det', 2, 3);


--
-- TOC entry 1890 (class 0 OID 328001)
-- Dependencies: 154
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO perfil (id, id_padre, descripcion, familia, status, usuario_id, created, modified) VALUES (1, 0, 'Administrador', 0, '1', 0, '1900-01-01 00:00:00', '1900-01-01 00:00:00');
INSERT INTO perfil (id, id_padre, descripcion, familia, status, usuario_id, created, modified) VALUES (3, 0, 'Empleados', 3, NULL, NULL, '2009-06-08 17:29:08', '2009-06-08 17:29:08');
INSERT INTO perfil (id, id_padre, descripcion, familia, status, usuario_id, created, modified) VALUES (2, 0, 'Superusuario', 0, NULL, NULL, '2009-01-29 12:12:39', '2011-01-30 21:34:33');


--
-- TOC entry 1891 (class 0 OID 328013)
-- Dependencies: 156
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario (id, empleado_id, perfil_id, usuario, clave, status, usuario_id, created, modified, oficina_id) VALUES (4, 3, 1, 'gpalma', '20ec660b980da3aa5da69392833c69987a027302', NULL, NULL, '2011-02-14 11:46:19', '2016-07-14 07:17:53', 1);
INSERT INTO usuario (id, empleado_id, perfil_id, usuario, clave, status, usuario_id, created, modified, oficina_id) VALUES (5, 5, 1, 'lrivero', '6559ad050b5f66c9439bc36ad51f7965f65a4590', NULL, NULL, '2016-02-08 18:34:34', '2016-07-16 07:14:40', 1);


--
-- TOC entry 1866 (class 2606 OID 328034)
-- Dependencies: 140 140
-- Name: configuracion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY configuracion
    ADD CONSTRAINT configuracion_pkey PRIMARY KEY (id);


--
-- TOC entry 1868 (class 2606 OID 328036)
-- Dependencies: 142 142
-- Name: empleados_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY empleados
    ADD CONSTRAINT empleados_pkey PRIMARY KEY (id);


--
-- TOC entry 1870 (class 2606 OID 328038)
-- Dependencies: 144 144
-- Name: empresas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY empresas
    ADD CONSTRAINT empresas_pkey PRIMARY KEY (id);


--
-- TOC entry 1872 (class 2606 OID 328040)
-- Dependencies: 146 146
-- Name: funcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY funcion
    ADD CONSTRAINT funcion_pkey PRIMARY KEY (id);


--
-- TOC entry 1874 (class 2606 OID 328042)
-- Dependencies: 148 148
-- Name: grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY grupo
    ADD CONSTRAINT grupo_pkey PRIMARY KEY (id);


--
-- TOC entry 1876 (class 2606 OID 328044)
-- Dependencies: 150 150
-- Name: modulos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY modulos
    ADD CONSTRAINT modulos_pkey PRIMARY KEY (id);


--
-- TOC entry 1878 (class 2606 OID 328046)
-- Dependencies: 152 152
-- Name: oficina_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oficinas
    ADD CONSTRAINT oficina_pkey PRIMARY KEY (id);


--
-- TOC entry 1880 (class 2606 OID 328048)
-- Dependencies: 154 154
-- Name: perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id);


--
-- TOC entry 1882 (class 2606 OID 328050)
-- Dependencies: 156 156
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 1896 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-07-16 10:39:53 VET

--
-- PostgreSQL database dump complete
--

