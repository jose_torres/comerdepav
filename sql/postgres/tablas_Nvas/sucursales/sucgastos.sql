﻿CREATE TABLE sucgastos
(
  id bigserial NOT NULL ,  
  codsucursal bigint NOT NULL DEFAULT 0,
  monto monto numeric(150,2) NOT NULL DEFAULT 0.00,
  fecha date,  
  cuadrediario_id bigint NOT NULL DEFAULT 0,
  nro text,
  nombre text,
  motivo text,
  created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,  
  PRIMARY KEY (id)
);