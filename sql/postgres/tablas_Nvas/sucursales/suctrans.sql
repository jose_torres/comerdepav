﻿CREATE TABLE suctrans
(
  id bigserial NOT NULL,
  banco_id integer NOT NULL,
  cuadrediario_id integer NOT NULL,
  nro text,
  codventa bigint NOT NULL DEFAULT 0,
  codsucursal bigint NOT NULL DEFAULT 0,
  numerotransferencia text,
  fecha date NOT NULL,
  titular text,
  tipocc text,
  movbancario_id bigint NOT NULL DEFAULT 0,
  monto numeric(150,2) NOT NULL,
  created timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  modified timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,  
  PRIMARY KEY (id )
)