﻿CREATE TABLE sucdepositos
(
  id bigserial NOT NULL,
  movbancario_id bigint NOT NULL DEFAULT 0,
  codsucursal bigint NOT NULL DEFAULT 0,
  monto monto numeric(150,2) NOT NULL DEFAULT 0.00,
  fecha date,
  created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  caja_id bigint NOT NULL DEFAULT 0,
  cuadrediario_id bigint NOT NULL DEFAULT 0,
  valor monto numeric(150,2) NOT NULL DEFAULT 0,
  PRIMARY KEY (id )
);

CREATE TABLE movbancarios
(
  id bigserial NOT NULL,
  banco_id integer,
  cuentabancaria_id integer,
  codsucursal bigint NOT NULL DEFAULT 0,
  nrodocumento character varying(30) NOT NULL,
  nrocomprobante character varying(15),
  fecha date NOT NULL,
  cedbeneficiario character varying(15),
  nombeneficiario character(250),
  monto monto numeric(150,2),
  saldo monto numeric(150,2),
  observacion character varying(150),
  created timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  modified timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  conciliado character varying(2),
  documentotipo_id integer,
  chequeo boolean,
  fconciliacion date,
  impreanombrede character varying(250),
  leyenda1 character varying(50),
  leyenda2 character varying(50),
  docasociado character varying(30),
  montoref monto numeric(150,2) DEFAULT 0,
  cuadrediario_id integer DEFAULT 0,
  PRIMARY KEY (id )
);

CREATE TABLE documentotipo
(
  id bigserial NOT NULL,
  descorta text NOT NULL,
  descripcion text NOT NULL,
  created timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  modified timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  accion text,
  PRIMARY KEY (id )
);
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (1, 'DP', 'DEPOSITO', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (2, 'NC', 'NOTA DE CREDITO', '2011-03-17 08:14:59', '2011-03-17 08:14:59', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (3, 'CH', 'CHEQUE', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'DISMINUYE');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (4, 'ND', 'NOTA DE DEBITO', '2011-03-21 08:40:11', '2011-03-21 17:03:02', 'DISMINUYE');