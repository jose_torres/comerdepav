--
-- PostgreSQL database dump
--

-- Started on 2017-03-13 17:42:42 VET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 190 (class 1259 OID 335072)
-- Dependencies: 2036 2037 6
-- Name: documentotipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documentotipo (
    id bigint NOT NULL,
    descorta text NOT NULL,
    descripcion text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    accion text
);


ALTER TABLE public.documentotipo OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 335080)
-- Dependencies: 190 6
-- Name: documentotipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE documentotipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.documentotipo_id_seq OWNER TO postgres;

--
-- TOC entry 2044 (class 0 OID 0)
-- Dependencies: 191
-- Name: documentotipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE documentotipo_id_seq OWNED BY documentotipo.id;


--
-- TOC entry 2045 (class 0 OID 0)
-- Dependencies: 191
-- Name: documentotipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('documentotipo_id_seq', 11, true);


--
-- TOC entry 2038 (class 2604 OID 335326)
-- Dependencies: 191 190
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documentotipo ALTER COLUMN id SET DEFAULT nextval('documentotipo_id_seq'::regclass);


--
-- TOC entry 2041 (class 0 OID 335072)
-- Dependencies: 190
-- Data for Name: documentotipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (4, 'CH', 'CHEQUE', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'DISMINUYE');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (1, 'DP', 'DEPOSITO', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (2, 'NC', 'NOTA DE CREDITO', '2011-03-17 08:14:59', '2011-03-17 08:14:59', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (5, 'ND', 'NOTA DE DEBITO', '2011-03-21 08:40:11', '2011-03-21 17:03:02', 'DISMINUYE');


--
-- TOC entry 2040 (class 2606 OID 335398)
-- Dependencies: 190 190
-- Name: documentotipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY documentotipo
    ADD CONSTRAINT documentotipo_pkey PRIMARY KEY (id);


-- Completed on 2017-03-13 17:42:43 VET

--
-- PostgreSQL database dump complete
--

