﻿CREATE TABLE suctarjetas
(
  id bigserial NOT NULL,
  tarjetastipo_id integer DEFAULT 0,
  cuadrediario_id integer NOT NULL,
  maquina_id integer NOT NULL,
  fecha date NOT NULL,
  cantidad integer,
  monto numeric(150,2) NOT NULL,
  nro character varying(20),
  banco_id integer NOT NULL DEFAULT 0,
  empresacupon_id integer DEFAULT 0,
  movbancario_id integer DEFAULT 0,
  comision double precision DEFAULT 0,
  iva double precision DEFAULT 0,
  created timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  modified timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,  
  PRIMARY KEY (id )
)