﻿CREATE TABLE cuadrediarios (
    id bigserial NOT NULL,
    nro integer NOT NULL,
    fecha date DEFAULT '1900-01-01'::date,
    estatus integer DEFAULT 0 NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
     PRIMARY KEY (id )
);


ALTER TABLE public.cuadrediarios OWNER TO postgres;

COMMENT ON COLUMN cuadrediarios.estatus IS '1->Abierto
2->Cerrado
3->Transferido
4->Conciliado';

CREATE TABLE documentotipo (
    id bigserial NOT NULL,
    descorta text NOT NULL,
    descripcion text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    accion text,
     PRIMARY KEY (id )
);


CREATE TABLE movbancarios (
    id bigserial NOT NULL,
    banco_id integer,
    cuentasbancaria_id integer,
    codsucursal bigint DEFAULT 0 NOT NULL,
    nrodocumento character varying(30),
    nrocomprobante character varying(15),
    fecha date NOT NULL,
    cedbeneficiario character varying(15),
    nombeneficiario character(250),
    monto monto numeric(150,2),
    saldo monto numeric(150,2),
    observacion character varying(150),
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    conciliado character varying(2),
    documentotipo_id integer,
    chequeo boolean,
    fconciliacion date,
    impreanombrede character varying(250),
    leyenda1 character varying(50),
    leyenda2 character varying(50),
    docasociado character varying(30),
    montoref monto numeric(150,2) DEFAULT 0,
    cuadrediario_id integer DEFAULT 0,
    tipodeposito text,
     PRIMARY KEY (id )
);

CREATE TABLE sucdepositos (
    id bigserial NOT NULL,
    movbancario_id bigint DEFAULT 0 NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    monto monto numeric(150,2) DEFAULT 0.00 NOT NULL,
    fecha date,
    caja_id bigint DEFAULT 0 NOT NULL,
    cuadrediario_id bigint DEFAULT 0 NOT NULL,
    valor monto numeric(150,2) DEFAULT 0 NOT NULL,
    cantidad monto numeric(150,2) DEFAULT 0,    
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
     PRIMARY KEY (id )
);

--
-- TOC entry 2746 (class 0 OID 335747)
-- Dependencies: 429
-- Data for Name: documentotipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (1, 'DP', 'DEPOSITO', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (2, 'NC', 'NOTA DE CREDITO', '2011-03-17 08:14:59', '2011-03-17 08:14:59', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (3, 'CH', 'CHEQUE', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'DISMINUYE');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (4, 'ND', 'NOTA DE DEBITO', '2011-03-21 08:40:11', '2011-03-21 17:03:02', 'DISMINUYE');
