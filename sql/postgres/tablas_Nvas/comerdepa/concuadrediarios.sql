﻿CREATE TABLE concuadrediarios (
    id bigserial NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    nro integer NOT NULL,
    fecha date DEFAULT '1900-01-01'::date,
    estatus integer DEFAULT 0 NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    PRIMARY KEY (id )
);
