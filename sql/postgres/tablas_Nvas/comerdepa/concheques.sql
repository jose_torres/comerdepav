﻿--DROP TABLE succheques;

CREATE TABLE concheques
(
  id bigserial NOT NULL,
  id_sucursal bigint DEFAULT 0 NOT NULL, 
  banco_id integer NOT NULL,
  cuadrediario_id integer NOT NULL,
  codsucursal integer NOT NULL,
  codventa integer,
  nro integer,
  fecha date NOT NULL,
  monto numeric(150,2) NOT NULL,
  titular text,
  nrocta text,
  movbancario_id bigint NOT NULL DEFAULT 0,
  created timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  modified timestamp without time zone NOT NULL DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  PRIMARY KEY (id )
)
