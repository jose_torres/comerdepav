--01.- Crear la tabla consolidada de Ventas
CREATE TABLE conventas
(
  id bigserial NOT NULL,
  codventa bigint NOT NULL,
  codsucursal integer NOT NULL,
  numerofactura character varying(20) NOT NULL,
  fecha date NOT NULL DEFAULT ('now'::text)::date,
  codcliente integer NOT NULL,
  codvendedor integer NOT NULL, -- El codvendedor resulta ser el codusuario de la tabla usuario, como la clave primaria del usuario es codusuario y codsucursal, se usa el codsucursal de la venta, pues la venta y el usuario se supone que es de la misma sucursal
  codclientesucursal integer NOT NULL,
  hora time(6) without time zone NOT NULL DEFAULT ('now'::text)::time with time zone,
  coddeposito integer,
  codusuarioconectado integer,
  recibido numeric(150,2),
  estatus character varying(1) NOT NULL DEFAULT 'P'::character varying,
  montobruto numeric(150,2) NOT NULL,
  montoexento numeric(150,2) NOT NULL,
  baseimp1 numeric(150,2) NOT NULL,
  baseimp2 numeric(150,2) NOT NULL,
  baseimp3 numeric(150,2) NOT NULL,
  ivaimp1 numeric(150,2) NOT NULL,
  ivaimp2 numeric(150,2) NOT NULL,
  ivaimp3 numeric(150,2) NOT NULL,
  porimp1 numeric(150,2) NOT NULL,
  porimp2 numeric(150,2) NOT NULL,
  porimp3 numeric(150,2) NOT NULL,
  retencion numeric(150,2),
  nrocomprobantefiscal integer,
  vf text DEFAULT 'NO'::text,
  codmaquina bigint DEFAULT 0,
  memoria bigint DEFAULT 0,
  estatusz text DEFAULT 'Abierto'::text,
  usuariow text DEFAULT 'admin'::text,
  correlativo bigint DEFAULT 0,
  PRIMARY KEY (id )
);
--02.- Crear la tabla consolidada de Ventasproductos
CREATE TABLE conventasproductos
(
  id bigserial NOT NULL,
  codventa integer NOT NULL,
  codsucursal integer NOT NULL,
  codproducto integer NOT NULL,
  cantidad numeric(150,2) NOT NULL,
  precio numeric(150,2) NOT NULL,
  unidad character varying(20) NOT NULL,
  iva numeric(150,2) NOT NULL,
  factorconversion integer NOT NULL,
  costopromedio numeric(150,2) NOT NULL, -- El costo unitario del producto al momento de la venta, para saber las utilidades...
  indice smallint NOT NULL,
  unidad2 character varying(20), -- Es la unidad distinta a la de la venta, si se vendio en unidad alterna, esta seria la unidad principal
  factorconversion2 numeric(150,2), -- es el factor de conversion del producto en el momento de la venta, cuando se vende en unidad alterna, es igual a la columna factorconversion
  PRIMARY KEY (id)
);
--03.- Crear la tabla consolidada de ventaspagos
CREATE TABLE conventaspagos
(
  id bigserial NOT NULL,
  codventa integer NOT NULL,
  codsucursal integer NOT NULL,
  tipopago character varying(20) NOT NULL, -- CREDITO, EFECTIVO, TDEBITO, CHEQUE, TRANSFER, TCREDITO, TICKET
  monto numeric(150,2) NOT NULL,
  banco character varying(50),
  nrocuenta character varying(50),
  numerotransferencia character varying(50),
  cuenta character varying(50),
  numerocheque character(50),
  montorecibido numeric(150,2),
  codpuntoventa integer,
 PRIMARY KEY (id )
);
--04.- Crear la tabla consolidada devolucionventa
CREATE TABLE condevolucionventa
(
  id bigserial NOT NULL,
  coddevolucion bigint NOT NULL,
  codsucursal integer NOT NULL,
  codventa integer NOT NULL,
  codsucursalventa integer NOT NULL,
  fecha timestamp(6) without time zone NOT NULL DEFAULT now(),
  codusuario integer,
  codusuariosucursal integer,
  coddeposito integer,
  codccnotacredito integer,
  codccnotacreditosucursal integer,
  devoluciontotal boolean,
  montobruto numeric(150,2),
  montoexento numeric(150,2),
  baseimp1 numeric(150,2),
  baseimp2 numeric(150,2),
  baseimp3 numeric(150,2),
  ivaimp1 real,
  ivaimp2 real,
  ivaimp3 real,
  porimp1 numeric(150,2),
  porimp2 numeric(150,2),
  porimp3 numeric(150,2),
  retencion numeric(150,2) DEFAULT 0,
  observacion text,
  PRIMARY KEY (id )
);
--05.- Crear la tabla consolidada devolucionventaproductos
CREATE TABLE condevolucionventaproductos
(
  id bigserial NOT NULL,
  coddevolucion integer NOT NULL,
  codsucursal integer NOT NULL,
  codproducto integer NOT NULL,
  cantidad numeric(150,2) NOT NULL,
  indice smallint NOT NULL,
  unidad character varying(20), -- unidad en que se devolvió
  PRIMARY KEY (id )
);
--06.- Crear la tabla consolidada devolucionventacreditopagos
CREATE TABLE condevolucionventacreditopagos
(
 id bigserial NOT NULL,
  coddevolucionventa integer NOT NULL,
  codsucursal integer NOT NULL,
  tipopago character varying(10) NOT NULL,
  monto numeric(150,2) NOT NULL,
  fecha timestamp(6) without time zone NOT NULL DEFAULT now(),
  PRIMARY KEY (id )
);
--07.- Crear la tabla consolidada devolucionventapago
CREATE TABLE condevolucionventapago
(
  id bigserial NOT NULL,	
  coddevolucion integer NOT NULL,
  codsucursal integer NOT NULL,
  tipopago character varying(10) NOT NULL,
  monto numeric(150,2) NOT NULL,
  PRIMARY KEY (id)
);
--08.- Crear la tabla consolidada Clientes
CREATE TABLE conclientes
(
  id bigserial NOT NULL,
  codcliente bigint NOT NULL,
  descripcion text NOT NULL,
  rif character varying(15) NOT NULL,
  clase character varying(10),
  representante text,
  direccion text NOT NULL,
  direccion2 text,
  telefonos character varying(30),
  email text,
  numerodefax character varying(20),
  zona character varying(2),
  codvendedor integer, -- 'El codvendedor resulta ser el codusuario de la tabla usuario, como la clave primaria del usuario es codusuario y codsucursal, se usa el codsucursal del cliente, pues la cliente y el usuario se supone que es de la misma sucursal';
  tipodeprecio character varying(1) NOT NULL DEFAULT '2'::character varying,
  limitecredito numeric(150,2) NOT NULL DEFAULT 0,
  diasdecredito integer NOT NULL DEFAULT 0,
  diastolerancia integer,
  fechadeinicio date NOT NULL DEFAULT now(),
  observaciones text,
  tipodecliente character varying(1) NOT NULL DEFAULT '0'::character varying, -- 0 Si es ordinario (Que paga el IVA), 1 si es especial (Que retiene el 75% del IVA)
  interesdemora character varying(1),
  convenioprecio character varying(2) DEFAULT 0,
  estatus boolean NOT NULL DEFAULT true,
  porcdescuento numeric(10,2),
  credito boolean NOT NULL DEFAULT false,
  codsucursal integer NOT NULL,
  contribuyentepatente boolean NOT NULL DEFAULT false,
  porcentajepatente numeric(10,2),
  modificado integer NOT NULL DEFAULT 0,
  validado boolean NOT NULL DEFAULT false, -- True si ha sido validado por la página del SENIAT para RIF o CNE para cédulas de identidad
  diacobro character varying(9),
  diaventa character varying(9),
  fechaultimaventa date,
  nuevo boolean NOT NULL DEFAULT true,
  actualizado1 character varying(1) NOT NULL DEFAULT 0,
  actualizado2 character varying(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (id)
);
--09.- Crear la tabla consolidada retenciones_ventas
CREATE TABLE conretenciones_ventas
(
  id bigserial NOT NULL,
  codsucursal integer NOT NULL,	
  codretencion bigint NOT NULL,
  numero character varying(18),
  fechaemision date NOT NULL DEFAULT ('now'::text)::date,
  horaemision time(6) without time zone NOT NULL DEFAULT ('now'::text)::time with time zone,
  montototal numeric(150,2) NOT NULL,
  estatus character varying(1) NOT NULL DEFAULT 'A'::character varying,
  PRIMARY KEY (id)
);
--10.- Crear la tabla consolidada retenciones_ventas_detalles
CREATE TABLE conretenciones_ventas_detalles
(
  id bigserial NOT NULL,
  codretencion integer NOT NULL,
  numero character varying(18) NOT NULL,
  codcliente integer,
  codsucursalcliente integer,
  codmovimiento integer,
  codsucursal integer,
  documento character varying(15),
  tipodocumento character varying(15),
  montoretenido numeric(150,2),
  mesafecta character varying(2),
  anioafecta character varying(4),
  PRIMARY KEY (id)
)
;
