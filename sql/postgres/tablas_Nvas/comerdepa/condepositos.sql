﻿CREATE TABLE condepositos (
    id bigserial NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    movbancario_id bigint DEFAULT 0 NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    monto numeric(150,2) DEFAULT 0.00 NOT NULL,
    fecha date,
    caja_id bigint DEFAULT 0 NOT NULL,
    cuadrediario_id bigint DEFAULT 0 NOT NULL,
    valor numeric(150,2) DEFAULT 0 NOT NULL,
    cantidad numeric(150,2) DEFAULT 0,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
     PRIMARY KEY (id )
);
