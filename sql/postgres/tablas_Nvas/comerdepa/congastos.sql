﻿CREATE TABLE congastos
(
  id bigserial NOT NULL ,
  id_sucursal bigint DEFAULT 0 NOT NULL,    
  codsucursal bigint NOT NULL DEFAULT 0,
  monto numeric(150,2) NOT NULL DEFAULT 0.00,
  fecha date,  
  cuadrediario_id bigint NOT NULL DEFAULT 0,
  nro text,
  nombre text,
  motivo text,
  created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
  modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,  
  PRIMARY KEY (id)
);
