--
-- PostgreSQL database dump
--

-- Started on 2017-05-12 00:28:54 VET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- TOC entry 578 (class 1247 OID 340149)
-- Dependencies: 6 170
-- Name: dblink_pkey_results; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE dblink_pkey_results AS (
	"position" integer,
	colname text
);


ALTER TYPE public.dblink_pkey_results OWNER TO postgres;

--
-- TOC entry 203 (class 1255 OID 340139)
-- Dependencies: 6
-- Name: dblink(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink(text, text) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_record';


ALTER FUNCTION public.dblink(text, text) OWNER TO postgres;

--
-- TOC entry 204 (class 1255 OID 340140)
-- Dependencies: 6
-- Name: dblink(text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink(text, text, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_record';


ALTER FUNCTION public.dblink(text, text, boolean) OWNER TO postgres;

--
-- TOC entry 205 (class 1255 OID 340141)
-- Dependencies: 6
-- Name: dblink(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink(text) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_record';


ALTER FUNCTION public.dblink(text) OWNER TO postgres;

--
-- TOC entry 206 (class 1255 OID 340142)
-- Dependencies: 6
-- Name: dblink(text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink(text, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_record';


ALTER FUNCTION public.dblink(text, boolean) OWNER TO postgres;

--
-- TOC entry 213 (class 1255 OID 340152)
-- Dependencies: 6
-- Name: dblink_build_sql_delete(text, int2vector, integer, text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_build_sql_delete(text, int2vector, integer, text[]) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_build_sql_delete';


ALTER FUNCTION public.dblink_build_sql_delete(text, int2vector, integer, text[]) OWNER TO postgres;

--
-- TOC entry 212 (class 1255 OID 340151)
-- Dependencies: 6
-- Name: dblink_build_sql_insert(text, int2vector, integer, text[], text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_build_sql_insert(text, int2vector, integer, text[], text[]) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_build_sql_insert';


ALTER FUNCTION public.dblink_build_sql_insert(text, int2vector, integer, text[], text[]) OWNER TO postgres;

--
-- TOC entry 214 (class 1255 OID 340153)
-- Dependencies: 6
-- Name: dblink_build_sql_update(text, int2vector, integer, text[], text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_build_sql_update(text, int2vector, integer, text[], text[]) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_build_sql_update';


ALTER FUNCTION public.dblink_build_sql_update(text, int2vector, integer, text[], text[]) OWNER TO postgres;

--
-- TOC entry 221 (class 1255 OID 340160)
-- Dependencies: 6
-- Name: dblink_cancel_query(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_cancel_query(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_cancel_query';


ALTER FUNCTION public.dblink_cancel_query(text) OWNER TO postgres;

--
-- TOC entry 199 (class 1255 OID 340135)
-- Dependencies: 6
-- Name: dblink_close(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_close(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_close';


ALTER FUNCTION public.dblink_close(text) OWNER TO postgres;

--
-- TOC entry 200 (class 1255 OID 340136)
-- Dependencies: 6
-- Name: dblink_close(text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_close(text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_close';


ALTER FUNCTION public.dblink_close(text, boolean) OWNER TO postgres;

--
-- TOC entry 201 (class 1255 OID 340137)
-- Dependencies: 6
-- Name: dblink_close(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_close(text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_close';


ALTER FUNCTION public.dblink_close(text, text) OWNER TO postgres;

--
-- TOC entry 202 (class 1255 OID 340138)
-- Dependencies: 6
-- Name: dblink_close(text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_close(text, text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_close';


ALTER FUNCTION public.dblink_close(text, text, boolean) OWNER TO postgres;

--
-- TOC entry 185 (class 1255 OID 340121)
-- Dependencies: 6
-- Name: dblink_connect(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_connect(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_connect';


ALTER FUNCTION public.dblink_connect(text) OWNER TO postgres;

--
-- TOC entry 186 (class 1255 OID 340122)
-- Dependencies: 6
-- Name: dblink_connect(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_connect(text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_connect';


ALTER FUNCTION public.dblink_connect(text, text) OWNER TO postgres;

--
-- TOC entry 187 (class 1255 OID 340123)
-- Dependencies: 6
-- Name: dblink_connect_u(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_connect_u(text) RETURNS text
    LANGUAGE c STRICT SECURITY DEFINER
    AS '$libdir/dblink', 'dblink_connect';


ALTER FUNCTION public.dblink_connect_u(text) OWNER TO postgres;

--
-- TOC entry 188 (class 1255 OID 340124)
-- Dependencies: 6
-- Name: dblink_connect_u(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_connect_u(text, text) RETURNS text
    LANGUAGE c STRICT SECURITY DEFINER
    AS '$libdir/dblink', 'dblink_connect';


ALTER FUNCTION public.dblink_connect_u(text, text) OWNER TO postgres;

--
-- TOC entry 215 (class 1255 OID 340154)
-- Dependencies: 6
-- Name: dblink_current_query(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_current_query() RETURNS text
    LANGUAGE c
    AS '$libdir/dblink', 'dblink_current_query';


ALTER FUNCTION public.dblink_current_query() OWNER TO postgres;

--
-- TOC entry 189 (class 1255 OID 340125)
-- Dependencies: 6
-- Name: dblink_disconnect(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_disconnect() RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_disconnect';


ALTER FUNCTION public.dblink_disconnect() OWNER TO postgres;

--
-- TOC entry 190 (class 1255 OID 340126)
-- Dependencies: 6
-- Name: dblink_disconnect(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_disconnect(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_disconnect';


ALTER FUNCTION public.dblink_disconnect(text) OWNER TO postgres;

--
-- TOC entry 222 (class 1255 OID 340161)
-- Dependencies: 6
-- Name: dblink_error_message(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_error_message(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_error_message';


ALTER FUNCTION public.dblink_error_message(text) OWNER TO postgres;

--
-- TOC entry 207 (class 1255 OID 340143)
-- Dependencies: 6
-- Name: dblink_exec(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_exec(text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_exec';


ALTER FUNCTION public.dblink_exec(text, text) OWNER TO postgres;

--
-- TOC entry 208 (class 1255 OID 340144)
-- Dependencies: 6
-- Name: dblink_exec(text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_exec(text, text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_exec';


ALTER FUNCTION public.dblink_exec(text, text, boolean) OWNER TO postgres;

--
-- TOC entry 209 (class 1255 OID 340145)
-- Dependencies: 6
-- Name: dblink_exec(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_exec(text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_exec';


ALTER FUNCTION public.dblink_exec(text) OWNER TO postgres;

--
-- TOC entry 210 (class 1255 OID 340146)
-- Dependencies: 6
-- Name: dblink_exec(text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_exec(text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_exec';


ALTER FUNCTION public.dblink_exec(text, boolean) OWNER TO postgres;

--
-- TOC entry 195 (class 1255 OID 340131)
-- Dependencies: 6
-- Name: dblink_fetch(text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_fetch(text, integer) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_fetch';


ALTER FUNCTION public.dblink_fetch(text, integer) OWNER TO postgres;

--
-- TOC entry 196 (class 1255 OID 340132)
-- Dependencies: 6
-- Name: dblink_fetch(text, integer, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_fetch(text, integer, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_fetch';


ALTER FUNCTION public.dblink_fetch(text, integer, boolean) OWNER TO postgres;

--
-- TOC entry 197 (class 1255 OID 340133)
-- Dependencies: 6
-- Name: dblink_fetch(text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_fetch(text, text, integer) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_fetch';


ALTER FUNCTION public.dblink_fetch(text, text, integer) OWNER TO postgres;

--
-- TOC entry 198 (class 1255 OID 340134)
-- Dependencies: 6
-- Name: dblink_fetch(text, text, integer, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_fetch(text, text, integer, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_fetch';


ALTER FUNCTION public.dblink_fetch(text, text, integer, boolean) OWNER TO postgres;

--
-- TOC entry 220 (class 1255 OID 340159)
-- Dependencies: 6
-- Name: dblink_get_connections(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_get_connections() RETURNS text[]
    LANGUAGE c
    AS '$libdir/dblink', 'dblink_get_connections';


ALTER FUNCTION public.dblink_get_connections() OWNER TO postgres;

--
-- TOC entry 211 (class 1255 OID 340150)
-- Dependencies: 578 6
-- Name: dblink_get_pkey(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_get_pkey(text) RETURNS SETOF dblink_pkey_results
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_get_pkey';


ALTER FUNCTION public.dblink_get_pkey(text) OWNER TO postgres;

--
-- TOC entry 218 (class 1255 OID 340157)
-- Dependencies: 6
-- Name: dblink_get_result(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_get_result(text) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_get_result';


ALTER FUNCTION public.dblink_get_result(text) OWNER TO postgres;

--
-- TOC entry 219 (class 1255 OID 340158)
-- Dependencies: 6
-- Name: dblink_get_result(text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_get_result(text, boolean) RETURNS SETOF record
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_get_result';


ALTER FUNCTION public.dblink_get_result(text, boolean) OWNER TO postgres;

--
-- TOC entry 217 (class 1255 OID 340156)
-- Dependencies: 6
-- Name: dblink_is_busy(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_is_busy(text) RETURNS integer
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_is_busy';


ALTER FUNCTION public.dblink_is_busy(text) OWNER TO postgres;

--
-- TOC entry 191 (class 1255 OID 340127)
-- Dependencies: 6
-- Name: dblink_open(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_open(text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_open';


ALTER FUNCTION public.dblink_open(text, text) OWNER TO postgres;

--
-- TOC entry 192 (class 1255 OID 340128)
-- Dependencies: 6
-- Name: dblink_open(text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_open(text, text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_open';


ALTER FUNCTION public.dblink_open(text, text, boolean) OWNER TO postgres;

--
-- TOC entry 193 (class 1255 OID 340129)
-- Dependencies: 6
-- Name: dblink_open(text, text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_open(text, text, text) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_open';


ALTER FUNCTION public.dblink_open(text, text, text) OWNER TO postgres;

--
-- TOC entry 194 (class 1255 OID 340130)
-- Dependencies: 6
-- Name: dblink_open(text, text, text, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_open(text, text, text, boolean) RETURNS text
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_open';


ALTER FUNCTION public.dblink_open(text, text, text, boolean) OWNER TO postgres;

--
-- TOC entry 216 (class 1255 OID 340155)
-- Dependencies: 6
-- Name: dblink_send_query(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION dblink_send_query(text, text) RETURNS integer
    LANGUAGE c STRICT
    AS '$libdir/dblink', 'dblink_send_query';


ALTER FUNCTION public.dblink_send_query(text, text) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 161 (class 1259 OID 340030)
-- Dependencies: 1962 1963 1964 1965 6
-- Name: concheques; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE concheques (
    id bigint NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    banco_id integer NOT NULL,
    cuadrediario_id integer NOT NULL,
    codsucursal integer NOT NULL,
    codventa integer,
    nro integer,
    fecha date NOT NULL,
    monto numeric(150,2) NOT NULL,
    titular text,
    nrocta text,
    movbancario_id bigint DEFAULT 0 NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.concheques OWNER TO postgres;

--
-- TOC entry 160 (class 1259 OID 340028)
-- Dependencies: 161 6
-- Name: concheques_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE concheques_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.concheques_id_seq OWNER TO postgres;

--
-- TOC entry 2063 (class 0 OID 0)
-- Dependencies: 160
-- Name: concheques_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE concheques_id_seq OWNED BY concheques.id;


--
-- TOC entry 2064 (class 0 OID 0)
-- Dependencies: 160
-- Name: concheques_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('concheques_id_seq', 1, false);


--
-- TOC entry 159 (class 1259 OID 340000)
-- Dependencies: 1956 1957 1958 1959 1960 6
-- Name: concuadrediarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE concuadrediarios (
    id bigint NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    nro integer NOT NULL,
    fecha date DEFAULT '1900-01-01'::date,
    estatus integer DEFAULT 0 NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    codsucursal bigint
);


ALTER TABLE public.concuadrediarios OWNER TO postgres;

--
-- TOC entry 158 (class 1259 OID 339998)
-- Dependencies: 6 159
-- Name: concuadrediarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE concuadrediarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.concuadrediarios_id_seq OWNER TO postgres;

--
-- TOC entry 2065 (class 0 OID 0)
-- Dependencies: 158
-- Name: concuadrediarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE concuadrediarios_id_seq OWNED BY concuadrediarios.id;


--
-- TOC entry 2066 (class 0 OID 0)
-- Dependencies: 158
-- Name: concuadrediarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('concuadrediarios_id_seq', 1, true);


--
-- TOC entry 163 (class 1259 OID 340045)
-- Dependencies: 1967 1968 1969 1970 1971 1972 1973 1974 1975 1976 6
-- Name: condepositos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE condepositos (
    id bigint NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    movbancario_id bigint DEFAULT 0 NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    monto numeric(150,2) DEFAULT 0.00 NOT NULL,
    fecha date,
    caja_id bigint DEFAULT 0 NOT NULL,
    cuadrediario_id bigint DEFAULT 0 NOT NULL,
    valor numeric(150,2) DEFAULT 0 NOT NULL,
    cantidad numeric(150,2) DEFAULT 0,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone
);


ALTER TABLE public.condepositos OWNER TO postgres;

--
-- TOC entry 162 (class 1259 OID 340043)
-- Dependencies: 6 163
-- Name: condepositos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE condepositos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.condepositos_id_seq OWNER TO postgres;

--
-- TOC entry 2067 (class 0 OID 0)
-- Dependencies: 162
-- Name: condepositos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE condepositos_id_seq OWNED BY condepositos.id;


--
-- TOC entry 2068 (class 0 OID 0)
-- Dependencies: 162
-- Name: condepositos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('condepositos_id_seq', 1, false);


--
-- TOC entry 140 (class 1259 OID 331905)
-- Dependencies: 1916 1917 1918 6
-- Name: configuracion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE configuracion (
    id integer NOT NULL,
    perfil_id integer NOT NULL,
    funcion_id integer NOT NULL,
    incluir text,
    modificar text,
    consultar text,
    eliminar text,
    status text DEFAULT '1'::text NOT NULL,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.configuracion OWNER TO postgres;

--
-- TOC entry 141 (class 1259 OID 331914)
-- Dependencies: 6 140
-- Name: configuracion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE configuracion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.configuracion_id_seq OWNER TO postgres;

--
-- TOC entry 2069 (class 0 OID 0)
-- Dependencies: 141
-- Name: configuracion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE configuracion_id_seq OWNED BY configuracion.id;


--
-- TOC entry 2070 (class 0 OID 0)
-- Dependencies: 141
-- Name: configuracion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('configuracion_id_seq', 490, true);


--
-- TOC entry 165 (class 1259 OID 340063)
-- Dependencies: 1978 1979 1980 1981 1982 1983 6
-- Name: congastos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE congastos (
    id bigint NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    monto numeric(150,2) DEFAULT 0.00 NOT NULL,
    fecha date,
    cuadrediario_id bigint DEFAULT 0 NOT NULL,
    nro text,
    nombre text,
    motivo text,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone
);


ALTER TABLE public.congastos OWNER TO postgres;

--
-- TOC entry 164 (class 1259 OID 340061)
-- Dependencies: 6 165
-- Name: congastos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE congastos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.congastos_id_seq OWNER TO postgres;

--
-- TOC entry 2071 (class 0 OID 0)
-- Dependencies: 164
-- Name: congastos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE congastos_id_seq OWNED BY congastos.id;


--
-- TOC entry 2072 (class 0 OID 0)
-- Dependencies: 164
-- Name: congastos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('congastos_id_seq', 6, true);


--
-- TOC entry 172 (class 1259 OID 340165)
-- Dependencies: 2002 2003 2004 2005 2006 2007 6
-- Name: conmovbancarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE conmovbancarios (
    id bigint NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    banco_id integer,
    cuentasbancaria_id integer,
    codsucursal bigint DEFAULT 0 NOT NULL,
    nrodocumento character varying(30),
    nrocomprobante character varying(15),
    fecha date NOT NULL,
    cedbeneficiario character varying(15),
    nombeneficiario character(250),
    monto numeric(150,2),
    saldo numeric(150,2),
    observacion character varying(150),
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    conciliado character varying(2),
    documentotipo_id integer,
    chequeo boolean,
    fconciliacion date,
    impreanombrede character varying(250),
    leyenda1 character varying(50),
    leyenda2 character varying(50),
    docasociado character varying(30),
    montoref numeric(150,2) DEFAULT 0,
    cuadrediario_id integer DEFAULT 0,
    tipodeposito text
);


ALTER TABLE public.conmovbancarios OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 340163)
-- Dependencies: 172 6
-- Name: conmovbancarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE conmovbancarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.conmovbancarios_id_seq OWNER TO postgres;

--
-- TOC entry 2073 (class 0 OID 0)
-- Dependencies: 171
-- Name: conmovbancarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE conmovbancarios_id_seq OWNED BY conmovbancarios.id;


--
-- TOC entry 2074 (class 0 OID 0)
-- Dependencies: 171
-- Name: conmovbancarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('conmovbancarios_id_seq', 16, true);


--
-- TOC entry 167 (class 1259 OID 340080)
-- Dependencies: 1985 1986 1987 1988 1989 1990 1991 1992 1993 6
-- Name: contarjetas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE contarjetas (
    id bigint NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    tarjetastipo_id integer DEFAULT 0,
    cuadrediario_id integer NOT NULL,
    maquina_id integer NOT NULL,
    fecha date NOT NULL,
    cantidad integer,
    monto numeric(150,2) NOT NULL,
    nro character varying(20),
    banco_id integer DEFAULT 0 NOT NULL,
    empresacupon_id integer DEFAULT 0,
    movbancario_id integer DEFAULT 0,
    comision double precision DEFAULT 0,
    iva double precision DEFAULT 0,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.contarjetas OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 340078)
-- Dependencies: 167 6
-- Name: contarjetas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contarjetas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.contarjetas_id_seq OWNER TO postgres;

--
-- TOC entry 2075 (class 0 OID 0)
-- Dependencies: 166
-- Name: contarjetas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contarjetas_id_seq OWNED BY contarjetas.id;


--
-- TOC entry 2076 (class 0 OID 0)
-- Dependencies: 166
-- Name: contarjetas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contarjetas_id_seq', 1, false);


--
-- TOC entry 169 (class 1259 OID 340098)
-- Dependencies: 1995 1996 1997 1998 1999 2000 6
-- Name: contrans; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE contrans (
    id bigint NOT NULL,
    id_sucursal bigint DEFAULT 0 NOT NULL,
    banco_id integer NOT NULL,
    cuadrediario_id integer NOT NULL,
    nro text,
    codventa bigint DEFAULT 0 NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    numerotransferencia text,
    fecha date NOT NULL,
    titular text,
    tipocc text,
    movbancario_id bigint DEFAULT 0 NOT NULL,
    monto numeric(150,2) NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.contrans OWNER TO postgres;

--
-- TOC entry 168 (class 1259 OID 340096)
-- Dependencies: 6 169
-- Name: contrans_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE contrans_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.contrans_id_seq OWNER TO postgres;

--
-- TOC entry 2077 (class 0 OID 0)
-- Dependencies: 168
-- Name: contrans_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE contrans_id_seq OWNED BY contrans.id;


--
-- TOC entry 2078 (class 0 OID 0)
-- Dependencies: 168
-- Name: contrans_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('contrans_id_seq', 1, false);


--
-- TOC entry 142 (class 1259 OID 331916)
-- Dependencies: 1920 1921 1922 1923 6
-- Name: empleados; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE empleados (
    id integer NOT NULL,
    cedula integer,
    nombre text,
    tipotrab text,
    codestructura integer DEFAULT 0 NOT NULL,
    coescala text,
    conivel text,
    codcategoria text,
    ded text,
    nivel integer,
    correoelectronico text,
    catpersonale_id text,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    cargo_id integer,
    oficina_id integer DEFAULT 0
);


ALTER TABLE public.empleados OWNER TO postgres;

--
-- TOC entry 143 (class 1259 OID 331926)
-- Dependencies: 142 6
-- Name: empleados_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE empleados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.empleados_id_seq OWNER TO postgres;

--
-- TOC entry 2079 (class 0 OID 0)
-- Dependencies: 143
-- Name: empleados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE empleados_id_seq OWNED BY empleados.id;


--
-- TOC entry 2080 (class 0 OID 0)
-- Dependencies: 143
-- Name: empleados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('empleados_id_seq', 11, true);


--
-- TOC entry 144 (class 1259 OID 331928)
-- Dependencies: 1925 1926 1928 6
-- Name: empresas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE empresas (
    id integer NOT NULL,
    rif text,
    nombre text,
    descripcion text,
    logo_izquierdo text,
    logo_derecho text,
    telefono text,
    status text,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    direccion text,
    bd text,
    tipoventa text DEFAULT 'DETAL'::text
);


ALTER TABLE public.empresas OWNER TO postgres;

--
-- TOC entry 145 (class 1259 OID 331936)
-- Dependencies: 6 144
-- Name: empresas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE empresas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.empresas_id_seq OWNER TO postgres;

--
-- TOC entry 2081 (class 0 OID 0)
-- Dependencies: 145
-- Name: empresas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE empresas_id_seq OWNED BY empresas.id;


--
-- TOC entry 2082 (class 0 OID 0)
-- Dependencies: 145
-- Name: empresas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('empresas_id_seq', 3, true);


--
-- TOC entry 146 (class 1259 OID 331938)
-- Dependencies: 1929 1930 1931 1932 6
-- Name: funcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE funcion (
    id integer NOT NULL,
    grupo_id integer NOT NULL,
    nombre text,
    clave text,
    direccion text,
    nombreimagen text,
    visible text,
    titulopanel text,
    imagenpanel text,
    status text,
    panel text DEFAULT 0,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modulo_id bigint DEFAULT 1 NOT NULL
);


ALTER TABLE public.funcion OWNER TO postgres;

--
-- TOC entry 147 (class 1259 OID 331948)
-- Dependencies: 6 146
-- Name: funcion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE funcion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.funcion_id_seq OWNER TO postgres;

--
-- TOC entry 2083 (class 0 OID 0)
-- Dependencies: 147
-- Name: funcion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE funcion_id_seq OWNED BY funcion.id;


--
-- TOC entry 2084 (class 0 OID 0)
-- Dependencies: 147
-- Name: funcion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('funcion_id_seq', 111, true);


--
-- TOC entry 148 (class 1259 OID 331950)
-- Dependencies: 1934 1935 6
-- Name: grupo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE grupo (
    id integer NOT NULL,
    nombre text,
    status text,
    usuario_id integer,
    imagen text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.grupo OWNER TO postgres;

--
-- TOC entry 149 (class 1259 OID 331958)
-- Dependencies: 6 148
-- Name: grupo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE grupo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.grupo_id_seq OWNER TO postgres;

--
-- TOC entry 2085 (class 0 OID 0)
-- Dependencies: 149
-- Name: grupo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE grupo_id_seq OWNED BY grupo.id;


--
-- TOC entry 2086 (class 0 OID 0)
-- Dependencies: 149
-- Name: grupo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('grupo_id_seq', 20, true);


--
-- TOC entry 150 (class 1259 OID 331960)
-- Dependencies: 1937 1938 6
-- Name: modulos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE modulos (
    id integer NOT NULL,
    nombre text,
    status text,
    imagen text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.modulos OWNER TO postgres;

--
-- TOC entry 151 (class 1259 OID 331968)
-- Dependencies: 6 150
-- Name: modulos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE modulos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.modulos_id_seq OWNER TO postgres;

--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 151
-- Name: modulos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE modulos_id_seq OWNED BY modulos.id;


--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 151
-- Name: modulos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('modulos_id_seq', 5, true);


--
-- TOC entry 152 (class 1259 OID 331970)
-- Dependencies: 1940 1941 1942 1943 1944 6
-- Name: oficinas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE oficinas (
    id integer NOT NULL,
    parent_id integer DEFAULT 0,
    descripcion text,
    familia integer DEFAULT 0,
    status text,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    siglas text,
    lft bigint,
    rght bigint,
    empresa_id bigint DEFAULT 1
);


ALTER TABLE public.oficinas OWNER TO postgres;

--
-- TOC entry 153 (class 1259 OID 331981)
-- Dependencies: 6 152
-- Name: oficinas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE oficinas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.oficinas_id_seq OWNER TO postgres;

--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 153
-- Name: oficinas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE oficinas_id_seq OWNED BY oficinas.id;


--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 153
-- Name: oficinas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('oficinas_id_seq', 12, true);


--
-- TOC entry 154 (class 1259 OID 331983)
-- Dependencies: 1946 1947 1948 1949 6
-- Name: perfil; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE perfil (
    id integer NOT NULL,
    id_padre integer DEFAULT 0,
    descripcion text,
    familia integer DEFAULT 0,
    status text,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL
);


ALTER TABLE public.perfil OWNER TO postgres;

--
-- TOC entry 155 (class 1259 OID 331993)
-- Dependencies: 6 154
-- Name: perfil_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE perfil_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.perfil_id_seq OWNER TO postgres;

--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 155
-- Name: perfil_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE perfil_id_seq OWNED BY perfil.id;


--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 155
-- Name: perfil_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('perfil_id_seq', 11, true);


--
-- TOC entry 156 (class 1259 OID 331995)
-- Dependencies: 1951 1952 1953 6
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuario (
    id integer NOT NULL,
    empleado_id integer NOT NULL,
    perfil_id integer NOT NULL,
    usuario text,
    clave text,
    status text,
    usuario_id integer,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    oficina_id bigint DEFAULT 1 NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 157 (class 1259 OID 332004)
-- Dependencies: 6 156
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 157
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuario_id_seq OWNED BY usuario.id;


--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 157
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('usuario_id_seq', 10, true);


--
-- TOC entry 1961 (class 2604 OID 340033)
-- Dependencies: 160 161 161
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY concheques ALTER COLUMN id SET DEFAULT nextval('concheques_id_seq'::regclass);


--
-- TOC entry 1955 (class 2604 OID 340003)
-- Dependencies: 158 159 159
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY concuadrediarios ALTER COLUMN id SET DEFAULT nextval('concuadrediarios_id_seq'::regclass);


--
-- TOC entry 1966 (class 2604 OID 340048)
-- Dependencies: 162 163 163
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY condepositos ALTER COLUMN id SET DEFAULT nextval('condepositos_id_seq'::regclass);


--
-- TOC entry 1919 (class 2604 OID 332006)
-- Dependencies: 141 140
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY configuracion ALTER COLUMN id SET DEFAULT nextval('configuracion_id_seq'::regclass);


--
-- TOC entry 1977 (class 2604 OID 340066)
-- Dependencies: 165 164 165
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY congastos ALTER COLUMN id SET DEFAULT nextval('congastos_id_seq'::regclass);


--
-- TOC entry 2001 (class 2604 OID 340168)
-- Dependencies: 172 171 172
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY conmovbancarios ALTER COLUMN id SET DEFAULT nextval('conmovbancarios_id_seq'::regclass);


--
-- TOC entry 1984 (class 2604 OID 340083)
-- Dependencies: 167 166 167
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contarjetas ALTER COLUMN id SET DEFAULT nextval('contarjetas_id_seq'::regclass);


--
-- TOC entry 1994 (class 2604 OID 340101)
-- Dependencies: 168 169 169
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY contrans ALTER COLUMN id SET DEFAULT nextval('contrans_id_seq'::regclass);


--
-- TOC entry 1924 (class 2604 OID 332007)
-- Dependencies: 143 142
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empleados ALTER COLUMN id SET DEFAULT nextval('empleados_id_seq'::regclass);


--
-- TOC entry 1927 (class 2604 OID 332008)
-- Dependencies: 145 144
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY empresas ALTER COLUMN id SET DEFAULT nextval('empresas_id_seq'::regclass);


--
-- TOC entry 1933 (class 2604 OID 332009)
-- Dependencies: 147 146
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY funcion ALTER COLUMN id SET DEFAULT nextval('funcion_id_seq'::regclass);


--
-- TOC entry 1936 (class 2604 OID 332010)
-- Dependencies: 149 148
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY grupo ALTER COLUMN id SET DEFAULT nextval('grupo_id_seq'::regclass);


--
-- TOC entry 1939 (class 2604 OID 332011)
-- Dependencies: 151 150
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY modulos ALTER COLUMN id SET DEFAULT nextval('modulos_id_seq'::regclass);


--
-- TOC entry 1945 (class 2604 OID 332012)
-- Dependencies: 153 152
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY oficinas ALTER COLUMN id SET DEFAULT nextval('oficinas_id_seq'::regclass);


--
-- TOC entry 1950 (class 2604 OID 332013)
-- Dependencies: 155 154
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY perfil ALTER COLUMN id SET DEFAULT nextval('perfil_id_seq'::regclass);


--
-- TOC entry 1954 (class 2604 OID 332014)
-- Dependencies: 157 156
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuario ALTER COLUMN id SET DEFAULT nextval('usuario_id_seq'::regclass);


--
-- TOC entry 2050 (class 0 OID 340030)
-- Dependencies: 161
-- Data for Name: concheques; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2049 (class 0 OID 340000)
-- Dependencies: 159
-- Data for Name: concuadrediarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO concuadrediarios (id, id_sucursal, nro, fecha, estatus, created, modified, codsucursal) VALUES (1, 20, 1, '2016-12-15', 2, '2017-04-30 13:15:41', '2017-04-30 13:18:51', 4);


--
-- TOC entry 2051 (class 0 OID 340045)
-- Dependencies: 163
-- Data for Name: condepositos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2040 (class 0 OID 331905)
-- Dependencies: 140
-- Data for Name: configuracion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (117, 2, 9, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:00', '2011-01-30 20:47:00');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (118, 2, 36, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:05', '2011-01-30 20:47:05');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (119, 2, 37, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:09', '2011-01-30 20:47:09');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (120, 2, 38, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:16', '2011-01-30 20:47:16');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (121, 2, 39, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:20', '2011-01-30 20:47:20');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (122, 2, 40, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:24', '2011-01-30 20:47:24');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (123, 2, 2, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:31', '2011-01-30 20:47:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (124, 2, 31, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:36', '2011-01-30 20:47:36');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (125, 2, 32, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:51', '2011-01-30 20:47:51');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (126, 2, 33, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:47:55', '2011-01-30 20:47:55');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (127, 2, 34, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:02', '2011-01-30 20:48:02');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (128, 2, 35, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:06', '2011-01-30 20:48:06');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (129, 2, 3, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:19', '2011-01-30 20:48:19');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (130, 2, 10, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:25', '2011-01-30 20:48:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (131, 2, 11, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:29', '2011-01-30 20:48:29');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (132, 2, 12, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:35', '2011-01-30 20:48:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (133, 2, 13, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:42', '2011-01-30 20:48:42');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (134, 2, 14, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:48:48', '2011-01-30 20:48:48');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (135, 2, 7, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:14', '2011-01-30 20:49:14');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (136, 2, 16, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:18', '2011-01-30 20:49:18');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (137, 2, 17, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:22', '2011-01-30 20:49:22');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (138, 2, 18, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:26', '2011-01-30 20:49:26');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (139, 2, 19, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:30', '2011-01-30 20:49:30');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (140, 2, 20, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:34', '2011-01-30 20:49:34');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (141, 2, 41, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 20:49:40', '2011-01-30 20:49:40');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (9, 1, 15, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2016-01-02 11:33:26');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (232, 3, 3, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (233, 3, 41, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (234, 3, 13, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (235, 3, 11, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (236, 3, 12, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (237, 3, 10, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (238, 3, 14, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (239, 3, 7, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (240, 3, 16, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (241, 3, 19, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (242, 3, 18, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (243, 3, 17, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (244, 3, 20, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (245, 3, 4, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (246, 3, 21, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (247, 3, 25, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (248, 3, 23, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (249, 3, 22, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (250, 3, 24, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (251, 3, 5, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (252, 3, 26, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (253, 3, 29, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (254, 3, 28, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (255, 3, 27, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (256, 3, 30, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (257, 3, 42, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (258, 3, 43, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (259, 3, 46, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (260, 3, 44, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (261, 3, 45, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (262, 3, 47, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (263, 3, 48, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (264, 3, 52, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (265, 3, 51, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (266, 3, 49, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (267, 3, 50, NULL, NULL, NULL, NULL, '1', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (268, 3, 2, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (269, 3, 31, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (270, 3, 35, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (271, 3, 34, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (272, 3, 32, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (273, 3, 33, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (274, 3, 1, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (275, 3, 36, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (276, 3, 40, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (220, 3, 9, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 22:46:41', '2016-02-09 20:10:32');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (277, 3, 38, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:33');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (278, 3, 37, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:33');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (279, 3, 39, NULL, NULL, NULL, NULL, '0', NULL, '2016-02-09 20:10:08', '2016-02-09 20:10:33');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (343, 10, 34, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (344, 10, 32, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (345, 10, 33, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (346, 10, 62, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (431, 11, 9, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (432, 11, 38, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (433, 11, 37, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (434, 11, 39, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (443, 10, 76, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-27 16:19:50', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (444, 10, 79, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-27 16:19:50', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (445, 10, 80, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-27 16:19:50', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (347, 10, 60, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (446, 10, 73, NULL, NULL, NULL, NULL, '0', NULL, '2016-10-27 16:19:50', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (348, 10, 61, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (447, 10, 75, NULL, NULL, NULL, NULL, '0', NULL, '2016-10-27 16:19:50', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (448, 10, 78, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-27 16:19:50', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (449, 10, 74, NULL, NULL, NULL, NULL, '0', NULL, '2016-10-27 16:19:50', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (349, 10, 59, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (353, 10, 63, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (354, 10, 64, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (355, 10, 65, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (356, 10, 69, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (352, 10, 67, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (410, 11, 35, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (411, 11, 34, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (412, 11, 32, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (413, 11, 33, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (414, 11, 62, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (451, 11, 76, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-27 16:21:35', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (452, 11, 79, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-27 16:21:35', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (453, 11, 80, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-27 16:21:35', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (415, 11, 60, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (454, 11, 73, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-27 16:21:35', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (299, 10, 3, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (300, 10, 41, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (301, 10, 13, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (302, 10, 11, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (303, 10, 12, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (304, 10, 10, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (305, 10, 14, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (306, 10, 7, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (307, 10, 16, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (308, 10, 19, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (309, 10, 18, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (310, 10, 17, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (311, 10, 20, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (312, 10, 54, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (313, 10, 55, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (314, 10, 58, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (315, 10, 56, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (316, 10, 57, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (317, 10, 4, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (318, 10, 21, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (319, 10, 25, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (320, 10, 23, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (416, 11, 61, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (455, 11, 75, NULL, NULL, NULL, NULL, '0', NULL, '2016-10-27 16:21:35', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (456, 11, 78, NULL, NULL, NULL, NULL, '0', NULL, '2016-10-27 16:21:35', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (457, 11, 74, NULL, NULL, NULL, NULL, '0', NULL, '2016-10-27 16:21:35', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (417, 11, 59, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (418, 11, 71, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (458, 11, 77, NULL, NULL, NULL, NULL, '0', NULL, '2016-10-27 16:21:35', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (419, 11, 72, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (420, 11, 67, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (321, 10, 22, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (322, 10, 24, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (323, 10, 5, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (324, 10, 26, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (325, 10, 29, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (326, 10, 28, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (327, 10, 27, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (328, 10, 30, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (329, 10, 42, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (330, 10, 43, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (331, 10, 46, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (332, 10, 44, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (333, 10, 45, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (334, 10, 47, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (335, 10, 48, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (336, 10, 52, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (337, 10, 51, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (338, 10, 49, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (339, 10, 50, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (340, 10, 2, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (341, 10, 31, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (342, 10, 35, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (350, 10, 71, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (450, 10, 77, NULL, NULL, NULL, NULL, '0', NULL, '2016-10-27 16:19:50', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (351, 10, 72, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (460, 10, 81, NULL, NULL, NULL, NULL, '1', NULL, '2016-11-16 15:21:25', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (357, 10, 66, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (358, 10, 68, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (359, 10, 70, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (360, 10, 1, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (361, 10, 36, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (362, 10, 40, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (363, 10, 9, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (364, 10, 38, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (365, 10, 37, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (366, 10, 39, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 15:04:40', '2016-11-16 15:21:25');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (367, 11, 3, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (368, 11, 41, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (369, 11, 13, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (370, 11, 11, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (371, 11, 12, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (372, 11, 10, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (373, 11, 14, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (374, 11, 7, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (375, 11, 16, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (376, 11, 19, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (377, 11, 18, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (378, 11, 17, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (379, 11, 20, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (380, 11, 54, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (381, 11, 55, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (382, 11, 58, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (383, 11, 56, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (384, 11, 57, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (385, 11, 4, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (386, 11, 21, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (387, 11, 25, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (388, 11, 23, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (389, 11, 22, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (390, 11, 24, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (391, 11, 5, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (392, 11, 26, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (393, 11, 29, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (394, 11, 28, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (395, 11, 27, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (396, 11, 30, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (397, 11, 42, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (398, 11, 43, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (399, 11, 46, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (400, 11, 44, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (401, 11, 45, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (402, 11, 47, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (403, 11, 48, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (404, 11, 52, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (405, 11, 51, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (406, 11, 49, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (407, 11, 50, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (408, 11, 2, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (409, 11, 31, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (421, 11, 63, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (422, 11, 64, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (423, 11, 65, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (424, 11, 69, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (425, 11, 66, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (426, 11, 68, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (427, 11, 70, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (428, 11, 1, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (429, 11, 36, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (430, 11, 40, NULL, NULL, NULL, NULL, '0', NULL, '2016-08-30 16:17:27', '2016-10-27 16:22:35');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (2, 1, 3, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (39, 1, 41, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:17:15', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (7, 1, 13, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (5, 1, 11, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (6, 1, 12, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (4, 1, 10, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (8, 1, 14, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (33, 1, 7, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:55', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (34, 1, 16, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:00', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (37, 1, 19, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:20', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (36, 1, 18, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:15', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (35, 1, 17, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:06', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (38, 1, 20, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 21:00:26', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (280, 1, 54, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:19:08', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (281, 1, 55, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:26:01', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (282, 1, 58, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:26:01', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (283, 1, 56, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:26:01', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (284, 1, 57, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-13 19:26:01', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (21, 1, 4, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:44', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (22, 1, 21, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:48', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (26, 1, 25, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:08', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (24, 1, 23, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:58', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (23, 1, 22, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:52', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (25, 1, 24, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:02', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (27, 1, 5, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:18', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (28, 1, 26, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:23', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (31, 1, 29, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:46', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (30, 1, 28, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:37', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (29, 1, 27, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:31', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (32, 1, 30, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:59:50', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (221, 1, 42, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:07:22', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (222, 1, 43, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:17:09', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (223, 1, 46, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:17:09', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (224, 1, 44, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:17:09', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (225, 1, 45, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-02 17:17:09', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (226, 1, 47, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (227, 1, 48, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (231, 1, 52, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:51:14', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (228, 1, 51, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (229, 1, 49, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (230, 1, 50, NULL, NULL, NULL, NULL, '1', NULL, '2016-01-03 19:00:16', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (10, 1, 2, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-15 22:58:33', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (11, 1, 31, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:17:49', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (15, 1, 35, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:18:07', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (14, 1, 34, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:18:03', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (12, 1, 32, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:17:53', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (13, 1, 33, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:17:58', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (287, 1, 62, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-21 19:03:27', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (461, 1, 83, NULL, NULL, NULL, NULL, '1', NULL, '2016-12-07 18:22:19', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (437, 1, 76, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-03 16:19:27', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (440, 1, 79, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-23 16:58:18', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (442, 1, 80, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-25 16:04:13', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (489, 1, 110, NULL, NULL, NULL, NULL, '1', NULL, '2017-05-11 23:42:48', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (490, 1, 111, NULL, NULL, NULL, NULL, '1', NULL, '2017-05-12 00:21:31', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (487, 1, 108, NULL, NULL, NULL, NULL, '0', NULL, '2017-05-09 19:18:58', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (482, 1, 103, NULL, NULL, NULL, NULL, '1', NULL, '2017-04-30 16:24:51', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (483, 1, 104, NULL, NULL, NULL, NULL, '1', NULL, '2017-04-30 17:17:44', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (484, 1, 105, NULL, NULL, NULL, NULL, '1', NULL, '2017-05-01 13:28:45', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (285, 1, 60, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-16 07:16:10', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (435, 1, 73, NULL, NULL, NULL, NULL, '1', NULL, '2016-09-26 15:34:54', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (288, 1, 61, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-21 19:03:27', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (462, 1, 82, NULL, NULL, NULL, NULL, '1', NULL, '2016-12-07 18:22:19', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (438, 1, 75, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-03 16:19:27', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (441, 1, 78, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-23 16:58:18', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (436, 1, 74, NULL, NULL, NULL, NULL, '1', NULL, '2016-09-26 15:41:46', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (286, 1, 59, NULL, NULL, NULL, NULL, '1', NULL, '2016-07-16 07:16:10', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (297, 1, 71, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-24 14:49:18', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (463, 1, 84, NULL, NULL, NULL, NULL, '1', NULL, '2016-12-11 17:36:53', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (439, 1, 77, NULL, NULL, NULL, NULL, '1', NULL, '2016-10-03 16:19:27', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (298, 1, 72, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-25 12:06:26', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (290, 1, 63, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-16 18:58:33', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (291, 1, 64, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-16 18:58:33', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (459, 1, 81, NULL, NULL, NULL, NULL, '1', NULL, '2016-11-16 15:19:46', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (292, 1, 65, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-16 18:58:33', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (294, 1, 69, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-17 11:39:47', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (293, 1, 66, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-16 18:58:33', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (289, 1, 67, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-16 18:58:33', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (295, 1, 68, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-17 11:39:47', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (296, 1, 70, NULL, NULL, NULL, NULL, '1', NULL, '2016-08-18 15:21:19', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (1, 1, 1, NULL, NULL, NULL, NULL, '1', 1, '1900-01-01 00:00:00', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (17, 1, 36, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:37:07', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (20, 1, 40, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:24', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (16, 1, 9, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-16 09:34:50', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (18, 1, 38, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:13', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (218, 1, 37, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-30 22:30:42', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (19, 1, 39, 'SI', 'SI', 'SI', 'SI', '1', NULL, '2011-01-20 20:58:20', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (474, 1, 95, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-12 20:22:40', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (467, 1, 88, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 15:44:07', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (479, 1, 100, NULL, NULL, NULL, NULL, '1', NULL, '2017-04-17 17:15:55', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (476, 1, 97, NULL, NULL, NULL, NULL, '1', NULL, '2017-03-14 17:35:26', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (466, 1, 87, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 14:24:44', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (480, 1, 101, NULL, NULL, NULL, NULL, '1', NULL, '2017-04-19 19:38:26', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (481, 1, 102, NULL, NULL, NULL, NULL, '1', NULL, '2017-04-24 11:12:08', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (465, 1, 86, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 10:58:50', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (469, 1, 90, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 17:31:57', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (475, 1, 96, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-15 06:15:26', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (477, 1, 98, NULL, NULL, NULL, NULL, '1', NULL, '2017-03-14 19:04:43', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (472, 1, 93, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 21:38:02', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (478, 1, 99, NULL, NULL, NULL, NULL, '1', NULL, '2017-03-21 21:06:40', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (464, 1, 85, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 10:52:01', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (468, 1, 89, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 16:23:10', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (471, 1, 92, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 21:20:50', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (473, 1, 94, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-12 20:03:13', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (488, 1, 109, NULL, NULL, NULL, NULL, '1', NULL, '2017-05-10 21:23:58', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (470, 1, 91, NULL, NULL, NULL, NULL, '1', NULL, '2017-02-08 18:20:46', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (486, 1, 107, NULL, NULL, NULL, NULL, '1', NULL, '2017-05-09 19:18:58', '2017-05-12 00:21:31');
INSERT INTO configuracion (id, perfil_id, funcion_id, incluir, modificar, consultar, eliminar, status, usuario_id, created, modified) VALUES (485, 1, 106, NULL, NULL, NULL, NULL, '1', NULL, '2017-05-09 18:11:04', '2017-05-12 00:21:31');


--
-- TOC entry 2052 (class 0 OID 340063)
-- Dependencies: 165
-- Data for Name: congastos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO congastos (id, id_sucursal, codsucursal, monto, fecha, cuadrediario_id, nro, nombre, motivo, created, modified) VALUES (6, 7, 4, 1960.00, '2016-12-15', 20, NULL, 'Ramón Perez', 'Agua de botellon', '2017-04-30 13:17:09', '2017-04-30 13:17:09');


--
-- TOC entry 2055 (class 0 OID 340165)
-- Dependencies: 172
-- Data for Name: conmovbancarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO conmovbancarios (id, id_sucursal, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito) VALUES (15, 26, 8, 3, 4, '456', NULL, '2016-12-15', NULL, NULL, 202349.98, NULL, ' Deposito de la Sucursal nro:4 a la Fecha:2016-12-15', '2017-04-30 13:16:25', '2017-04-30 13:16:25', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 20, 'DEBITO');
INSERT INTO conmovbancarios (id, id_sucursal, banco_id, cuentasbancaria_id, codsucursal, nrodocumento, nrocomprobante, fecha, cedbeneficiario, nombeneficiario, monto, saldo, observacion, created, modified, conciliado, documentotipo_id, chequeo, fconciliacion, impreanombrede, leyenda1, leyenda2, docasociado, montoref, cuadrediario_id, tipodeposito) VALUES (16, 27, 16, 1, 4, 'ty6788767', NULL, '2016-12-15', NULL, NULL, 22000.00, NULL, ' Deposito de la Sucursal nro:4 a la Fecha:2016-12-15', '2017-04-30 13:18:33', '2017-04-30 13:18:33', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, 20, 'EFECTIVO');


--
-- TOC entry 2053 (class 0 OID 340080)
-- Dependencies: 167
-- Data for Name: contarjetas; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2054 (class 0 OID 340098)
-- Dependencies: 169
-- Data for Name: contrans; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2041 (class 0 OID 331916)
-- Dependencies: 142
-- Data for Name: empleados; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO empleados (id, cedula, nombre, tipotrab, codestructura, coescala, conivel, codcategoria, ded, nivel, correoelectronico, catpersonale_id, created, modified, cargo_id, oficina_id) VALUES (3, 1234, 'Giusseppe De Palma', 'ADM', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-01-30 19:02:25', '2016-07-14 07:13:15', NULL, 1);
INSERT INTO empleados (id, cedula, nombre, tipotrab, codestructura, coescala, conivel, codcategoria, ded, nivel, correoelectronico, catpersonale_id, created, modified, cargo_id, oficina_id) VALUES (5, 90001, 'LEIBAN ALBERTY RIVERO COLMENAREZ', 'ADM', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2011-02-14 11:45:56', '2016-07-16 07:14:13', NULL, 10);
INSERT INTO empleados (id, cedula, nombre, tipotrab, codestructura, coescala, conivel, codcategoria, ded, nivel, correoelectronico, catpersonale_id, created, modified, cargo_id, oficina_id) VALUES (8, 90002, 'Pedro Graterol', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-30 14:43:23', '2016-08-30 16:20:24', NULL, 1);
INSERT INTO empleados (id, cedula, nombre, tipotrab, codestructura, coescala, conivel, codcategoria, ded, nivel, correoelectronico, catpersonale_id, created, modified, cargo_id, oficina_id) VALUES (9, 90003, 'Dember Dalis', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-30 15:03:34', '2016-08-30 16:20:30', NULL, 10);
INSERT INTO empleados (id, cedula, nombre, tipotrab, codestructura, coescala, conivel, codcategoria, ded, nivel, correoelectronico, catpersonale_id, created, modified, cargo_id, oficina_id) VALUES (10, 90004, 'Roraima Ruiz', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-08-30 16:20:56', '2016-08-30 16:20:56', NULL, 10);
INSERT INTO empleados (id, cedula, nombre, tipotrab, codestructura, coescala, conivel, codcategoria, ded, nivel, correoelectronico, catpersonale_id, created, modified, cargo_id, oficina_id) VALUES (11, 90005, 'DAYANA CORDEO}RO', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-09-23 15:30:15', '2016-09-23 15:30:15', NULL, 10);


--
-- TOC entry 2042 (class 0 OID 331928)
-- Dependencies: 144
-- Data for Name: empresas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO empresas (id, rif, nombre, descripcion, logo_izquierdo, logo_derecho, telefono, status, created, modified, direccion, bd, tipoventa) VALUES (3, 'J-30820232-0', 'COMERDEPA LA 21 DETAL C.A.', 'Ventas al Detal', 'UEVbf_logo_li.jpg', '', '0251-2321253', NULL, '2016-09-05 18:08:51', '2016-09-05 18:08:51', 'CARRERA 22 ENTRE 26 Y 27 CASA NRO 26-51', 'comerdepa_3', 'DETAL');
INSERT INTO empresas (id, rif, nombre, descripcion, logo_izquierdo, logo_derecho, telefono, status, created, modified, direccion, bd, tipoventa) VALUES (1, 'J-30820232-0', 'COMERDEPA LA 21 C.A.', 'CARRERA 22 ENTRE 26 Y 27 CASA NRO 26-51', 'logo.jpg', 'logo.jpg', '0251-2321253', NULL, '2016-07-13 19:29:13', '2017-05-09 18:01:34', 'Carrera 22 entre calles 26 y 27 Barquisimeto Edo. Lara', 'comerdepa', 'MAYOR');
INSERT INTO empresas (id, rif, nombre, descripcion, logo_izquierdo, logo_derecho, telefono, status, created, modified, direccion, bd, tipoventa) VALUES (2, 'J-29866535-1', 'SUPERDEPA C.A.', '', '3r4ry_superdepa1_li.jpg', '', '0251-2321253', NULL, '2016-09-05 18:07:21', '2017-05-09 18:03:50', 'CARRERA 22 ENTRE 26 Y 27 CASA NRO 26-51', 'superdepa', 'MAYOR');


--
-- TOC entry 2043 (class 0 OID 331938)
-- Dependencies: 146
-- Data for Name: funcion; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (2, 1, 'Perfiles', NULL, 'perfiles', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '1900-01-01 00:00:00', '2009-06-11 15:05:07', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (10, 1, 'Grabar una Configuracion', NULL, 'configuracions/grabar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:00:03', '2009-06-12 16:00:03', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (4, 1, 'Funcion', NULL, 'funciones', NULL, 'SI', NULL, NULL, '1', '1', 1, '1900-01-01 00:00:00', '2009-06-11 15:05:18', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (5, 1, 'Grupos', NULL, 'grupos', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-03-11 15:42:24', '2009-03-11 15:42:24', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (11, 1, 'Cambiar una Configuracion de permiso', NULL, 'configuracions/cambiar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:00:44', '2009-06-12 16:00:44', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (9, 1, 'Cambio de Contrase&ntilde;a', NULL, 'usuarios/cambiopassword', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-06-09 16:51:27', '2009-06-09 16:51:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (12, 1, 'Eliminar una Configuracion de permiso', NULL, 'configuracions/eliminar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:01:27', '2009-06-12 16:01:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (13, 1, 'buscar una Configuracion de permiso', NULL, 'configuracions/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:08:08', '2009-06-12 16:08:08', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (14, 1, 'Mostrar una Configuracion de permiso', NULL, 'configuracions/mostrar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 16:14:43', '2009-06-12 16:14:43', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (3, 1, 'Configuracion', NULL, 'configuracions', NULL, 'SI', NULL, NULL, '1', '1', 1, '1900-01-01 00:00:00', '2009-06-11 15:05:12', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (16, 1, 'Incluir Empleados', NULL, 'empleados/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:03:42', '2009-06-12 17:03:42', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (17, 1, 'Editar Empleados', NULL, 'empleados/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:04:11', '2009-06-12 17:04:11', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (18, 1, 'Eliminar Empleados', NULL, 'empleados/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:04:39', '2009-06-12 17:04:39', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (19, 1, 'Buscar un Empleado', NULL, 'empleados/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:05:04', '2009-06-12 17:05:04', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (20, 1, 'Consulta de Empleado', NULL, 'empleados/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-12 17:06:49', '2009-06-12 17:06:49', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (21, 1, 'Incluir Funcion', NULL, 'funciones/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:28:13', '2009-06-15 11:28:13', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (22, 1, 'Editar Funcion', NULL, 'funciones/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:28:38', '2009-06-15 11:28:38', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (23, 1, 'Eliminar Funciones', NULL, 'funciones/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:29:10', '2009-06-15 11:29:10', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (24, 1, 'Consulta de Funcion', NULL, 'funciones/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:29:33', '2009-06-15 11:29:33', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (25, 1, 'Buscar una Funcion', NULL, 'funciones/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:34:47', '2009-06-15 11:34:47', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (26, 1, 'Incluir Grupo', NULL, 'grupos/add', NULL, 'NO', NULL, NULL, NULL, '1', 1, '2009-06-15 11:42:15', '2009-06-15 11:42:15', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (27, 1, 'Editar Grupos', NULL, 'grupos/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:42:37', '2009-06-15 11:42:37', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (28, 1, 'Eliminar Grupo', NULL, 'grupos/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:43:01', '2009-06-15 11:43:01', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (29, 1, 'Buscar un Grupo', NULL, 'grupos/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:43:24', '2009-06-15 11:46:32', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (30, 1, 'Consulta de Grupo', NULL, 'grupos/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 11:44:24', '2009-06-15 11:45:53', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (31, 1, 'Incluir un Perfil', NULL, 'perfiles/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:04:34', '2009-06-15 17:04:34', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (32, 1, 'Editar un Perfil', NULL, 'perfiles/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:04:56', '2009-06-15 17:04:56', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (33, 1, 'Consulta de un Perfil', NULL, 'perfiles/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:05:21', '2009-06-15 17:05:21', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (34, 1, 'Eliminar un perfil', NULL, 'perfiles/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:05:47', '2009-06-15 17:05:47', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (35, 1, 'Buscar un Perfil', NULL, 'perfiles/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-15 17:06:14', '2009-06-15 17:06:14', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (36, 1, 'Incluir Usuarios', NULL, 'usuarios/add', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:45:53', '2009-06-17 09:45:53', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (37, 1, 'Editar Usuarios', NULL, 'usuarios/edit', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:46:12', '2009-06-17 09:46:12', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (38, 1, 'Eliminar Usuarios', NULL, 'usuarios/delete', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:49:27', '2009-06-17 09:49:27', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (39, 1, 'Consulta de Usuario', NULL, 'usuarios/view', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:49:49', '2009-06-17 09:49:49', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (40, 1, 'Buscar un usuario', NULL, 'usuarios/buscar', NULL, 'NO', NULL, NULL, NULL, '1', NULL, '2009-06-17 09:51:03', '2009-06-17 09:51:03', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (41, 1, 'Actualizar Configuracion', NULL, 'configuracions/actualizar', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2012-05-05 13:41:21', '2012-05-05 13:41:21', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (45, 1, 'Ver Modulo', NULL, 'modulos/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:16:09', '2016-01-02 17:16:09', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (47, 2, 'Oficinas', NULL, 'oficinas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:56:52', '2016-01-03 18:56:52', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (51, 2, 'Eliminar Oficina', NULL, 'oficinas/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:59:59', '2016-01-03 18:59:59', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (7, 2, 'Empleados', NULL, 'empleados', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '2009-06-03 10:59:05', '2016-01-11 18:30:05', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (42, 1, 'Modulos', NULL, 'modulos', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:07:13', '2016-01-02 17:07:13', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (46, 1, 'Eliminar Modulo', NULL, 'modulos/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:16:42', '2016-01-02 17:16:42', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (48, 2, 'Incluir Oficinas', NULL, 'oficinas/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:57:27', '2016-01-03 18:58:26', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (52, 2, 'Buscar Hijos de Oficinas (Ajax)', NULL, 'oficinas/buscarhijos', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 19:51:05', '2016-01-03 19:51:05', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (1, 1, 'Usuarios', '1', 'usuarios', NULL, 'SI', NULL, NULL, NULL, '1', NULL, '1900-01-01 00:00:00', '2011-03-28 16:23:35', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (43, 1, 'Incluir Modulo', NULL, 'modulos/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:15:14', '2016-01-02 17:15:14', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (49, 2, 'Editar Oficina', NULL, 'oficinas/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:58:17', '2016-01-03 18:58:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (44, 1, 'Editar Modulo', NULL, 'modulos/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-02 17:15:40', '2016-01-02 17:15:40', 1);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (50, 2, 'Ver Oficina', NULL, 'oficinas/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-01-03 18:59:17', '2016-01-03 18:59:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (54, 2, 'Empresas', NULL, 'empresas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:18:53', '2016-07-13 19:18:53', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (55, 2, 'Incluir Empresas', NULL, 'empresas/add', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:19:50', '2016-07-13 19:19:50', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (56, 2, 'Editar Empresas', NULL, 'empresas/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:20:17', '2016-07-13 19:20:17', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (57, 2, 'Visualizar Empresas', NULL, 'empresas/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:20:41', '2016-07-13 19:20:41', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (58, 2, 'Eliminar empresas', NULL, 'empresas/delete', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-13 19:25:10', '2016-07-13 19:25:10', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (59, 5, 'Ventas', NULL, 'reportes/ventas', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-07-16 07:15:22', '2016-07-16 07:15:22', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (60, 5, 'Buscar Ventas (Ajax)', NULL, 'reportes/buscarventas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-16 07:15:56', '2016-07-16 07:15:56', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (61, 5, 'Compras', NULL, 'reportes/compras', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-07-21 19:02:50', '2016-07-21 19:02:50', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (62, 5, 'Buscar Compras (Ajax)', NULL, 'reportes/buscarcompras', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-07-21 19:03:08', '2016-07-21 19:03:08', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (63, 4, 'Incluir Retenci&oacute;n', NULL, 'retenciones/add', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:48:36', '2016-08-16 18:50:05', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (64, 4, 'Buscar Facturas (Ajax)', NULL, 'retenciones/buscarfacturas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:50:43', '2016-08-16 18:50:43', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (65, 4, 'Calcular Retencion Compras (Ajax)', NULL, 'retenciones/calcularretencion', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:51:17', '2016-08-16 18:51:17', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (66, 4, 'Guardar Retenci&oacute;n', NULL, 'retenciones/incluir', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:51:57', '2016-08-16 18:51:57', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (68, 4, 'Tr&aacute;nsito', NULL, 'retenciones/transito', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-08-17 11:38:52', '2016-08-17 11:38:52', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (69, 4, 'Cambiar Retencion de Transito a Normal', NULL, 'retenciones/edit', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-17 11:39:27', '2016-08-17 11:39:27', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (70, 4, 'Imprimir Retenci&oacute;n', NULL, 'retenciones/viewpdf', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-18 15:21:04', '2016-08-18 15:21:04', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (71, 4, 'Imprimir Reportes Compras', NULL, 'reportes/viewpdf', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-24 14:49:06', '2016-08-25 09:22:48', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (72, 5, 'Imprimir Reportes Ventas', NULL, 'reportes/viewpdfventas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-08-25 12:05:58', '2016-08-25 12:05:58', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (73, 5, 'Clientes', NULL, 'reportes/clientes', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-09-26 15:33:57', '2016-09-26 15:33:57', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (74, 5, 'Vendedores', NULL, 'reportes/vendedores', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-09-26 15:40:24', '2016-09-26 15:40:24', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (75, 5, 'Inventario', NULL, 'reportes/inventario', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-10-03 16:17:34', '2016-10-03 16:17:34', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (76, 5, 'Buscar Inventario (Ajax)', NULL, 'reportes/buscarinventario', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-10-03 16:18:22', '2016-10-03 16:18:22', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (77, 5, 'Imprimir Inventario', NULL, 'reportes/viewpdfinv', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-10-03 16:19:05', '2016-10-03 16:19:05', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (78, 5, 'Proveedores', NULL, 'reportes/proveedores', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-10-23 16:56:33', '2016-10-23 16:56:33', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (79, 5, 'Buscar Proveedores Compras (Ajax)', NULL, 'reportes/buscarproveedores', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-10-23 16:57:06', '2016-10-23 16:57:06', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (80, 5, 'Buscar Vendedores (Ajax)', NULL, 'reportes/buscarvendedores', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-10-25 16:03:58', '2016-10-25 16:03:58', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (67, 4, 'Normales', NULL, 'retenciones/index', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-08-16 18:57:31', '2016-11-16 15:16:43', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (81, 4, 'Buscar Retenciones (Ajax)', NULL, 'retenciones/buscarretenciones', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-11-16 15:18:34', '2016-11-16 15:19:30', 3);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (82, 5, 'Impuestos', NULL, 'reportes/impuestos', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2016-12-07 18:21:06', '2016-12-07 18:21:06', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (83, 5, 'Buscar Impuestos (Ajax)', NULL, 'reportes/buscarimpuestos', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-12-07 18:21:33', '2016-12-07 18:21:33', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (84, 5, 'Imprimir Reportes Impuestos', NULL, 'reportes/viewpdfimpuestos', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2016-12-11 17:36:34', '2016-12-11 17:36:34', 2);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (86, 20, 'Buscar Ventas Fiscales (Ajax)', NULL, 'ventas/buscarventas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 10:58:36', '2017-02-08 10:58:36', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (87, 20, 'Buscar Maquinas Fiscales (Ajax)', NULL, 'ventas/buscarmaquinas', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 14:24:32', '2017-02-08 14:24:32', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (88, 20, 'Asociar Ventas a Maquina Fiscal', NULL, 'ventas/asociar', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 15:42:51', '2017-02-08 15:42:51', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (85, 20, '1. Asociar Ventas Z', NULL, 'ventas/ventasz', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-08 10:51:46', '2017-02-08 16:21:36', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (89, 20, '2. Cerrar Ventas Z', NULL, 'ventas/ventaszcerrar', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-08 16:22:59', '2017-02-08 16:22:59', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (90, 20, 'Cerrar Ventas Z', NULL, 'ventas/cerrar', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 17:31:38', '2017-02-08 17:31:38', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (91, 20, '3. Ventas Z Cerradas', NULL, 'ventas/ventaszreportes', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-08 18:20:18', '2017-02-08 18:20:18', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (92, 20, '4. Abrir Ventas Z ', NULL, 'ventas/ventaszdevolver', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-08 18:35:50', '2017-02-08 18:35:50', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (93, 20, 'Desasociar Ventas Z', NULL, 'ventas/devolver', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-08 21:37:23', '2017-02-08 21:37:23', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (94, 20, '5. Ventas No Fiscales', NULL, 'ventas/ventasznf', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-12 20:02:33', '2017-02-12 20:02:33', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (95, 20, 'Abrir Ventas Z No Fiscales', NULL, 'ventas/abrir', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-02-12 20:22:19', '2017-02-12 20:22:19', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (97, 20, 'Buscar Cuenta Bancaria (Ajax)', NULL, 'ventas/buscarctabanco', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-03-14 17:35:06', '2017-03-14 17:35:06', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (98, 20, 'Depositar (Ajax)', NULL, 'ventas/depositar', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-03-14 19:04:22', '2017-03-14 19:04:22', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (99, 20, 'Eliminar Deposito (Ajax)', NULL, 'ventas/eliminardeposito', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-03-21 21:06:12', '2017-03-21 21:06:12', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (100, 20, 'Buscar Cheque por Depositar (Ajax)', NULL, 'ventas/buscarchequesxdep', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-04-17 17:15:35', '2017-04-17 17:15:35', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (101, 20, 'Buscar Pagos en Puntos de Ventas (Ajax)', NULL, 'ventas/buscarpdvxdep', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-04-19 19:38:07', '2017-04-19 19:38:07', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (102, 20, 'Buscar Transferencia en Cuadre (Ajax)', NULL, 'ventas/buscartransferencia', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-04-24 11:10:32', '2017-04-24 11:11:09', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (104, 3, 'Resumen de Cuadre', NULL, 'cuadrediarios/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-04-30 17:17:20', '2017-04-30 17:17:20', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (105, 3, 'Imprimir Cuadre Diario', NULL, 'cuadrediarios/viewpdf', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-05-01 13:28:25', '2017-05-01 13:28:25', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (108, 3, 'Imprimir Cuadre Diario Consolidado', NULL, 'concuadrediarios/viewpdf', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-05-09 19:18:34', '2017-05-09 19:18:34', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (96, 3, '1. Cierre de Caja', NULL, 'ventas/cierre', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-02-15 06:15:05', '2017-05-09 19:19:59', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (103, 3, '2. Listado de Cuadres', NULL, 'cuadrediarios/index', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-04-30 16:24:22', '2017-05-09 19:20:21', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (107, 3, '4. Cuadres Transferidos', NULL, 'concuadrediarios/index', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-05-09 19:17:32', '2017-05-09 19:21:55', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (106, 3, '3. Transferir Cuadres', NULL, 'concuadrediarios/transferir', NULL, 'SI', NULL, NULL, NULL, '0', NULL, '2017-05-09 18:10:45', '2017-05-09 19:29:30', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (109, 3, 'Buscar Cierre a Sucursal (Ajax)', NULL, 'concuadrediarios/buscarcierre', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-05-10 21:23:37', '2017-05-10 21:23:37', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (110, 3, 'Transferir Cuadres a Principal (Ajax)', NULL, 'concuadrediarios/transferirprincipal', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-05-11 23:42:27', '2017-05-11 23:42:27', 5);
INSERT INTO funcion (id, grupo_id, nombre, clave, direccion, nombreimagen, visible, titulopanel, imagenpanel, status, panel, usuario_id, created, modified, modulo_id) VALUES (111, 3, 'Ver Cuadres Transferidos', NULL, 'concuadrediarios/view', NULL, 'NO', NULL, NULL, NULL, '0', NULL, '2017-05-12 00:21:09', '2017-05-12 00:21:09', 5);


--
-- TOC entry 2044 (class 0 OID 331950)
-- Dependencies: 148
-- Data for Name: grupo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (1, 'Seguridad', '1', 1, 'lock.png', '1900-01-01 00:00:00', '2011-09-24 21:37:46');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (2, 'Configuraciones', '1', 1, 'setting.png', '1900-01-01 00:00:00', '2011-09-24 21:45:45');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (3, 'Cajas', NULL, NULL, 'agt_business.png', '2009-03-24 14:30:30', '2011-09-24 21:59:19');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (10, 'Bancos', NULL, NULL, 'money.png', '2011-03-28 17:32:32', '2011-09-24 22:09:56');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (5, 'Reporte', NULL, NULL, 'internet-news-reader.png', '2009-06-08 11:19:18', '2012-04-12 13:59:23');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (8, 'Procesos', NULL, NULL, 'x-office-spreadsheet-template.png', '2011-03-28 17:31:38', '2014-10-29 15:45:32');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (12, 'Ctas_x_Cobrar', NULL, NULL, 'system-file-manager.png', '2015-07-11 11:09:30', '2015-07-11 11:09:30');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (4, 'Retenciones', NULL, NULL, 'x-office-document.png', '2009-04-21 09:15:44', '2016-08-16 18:49:41');
INSERT INTO grupo (id, nombre, status, usuario_id, imagen, created, modified) VALUES (20, 'Ventas', NULL, NULL, 'x-office-spreadsheet.png', '2017-02-08 10:51:23', '2017-02-08 10:51:23');


--
-- TOC entry 2045 (class 0 OID 331960)
-- Dependencies: 150
-- Data for Name: modulos; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO modulos (id, nombre, status, imagen, created, modified) VALUES (1, 'Seguridad', '1', 'lock.png', '1900-01-01 00:00:00', '1900-01-01 00:00:00');
INSERT INTO modulos (id, nombre, status, imagen, created, modified) VALUES (2, 'Reportes', NULL, 'modulo_documentos.png', '2016-01-02 17:34:06', '2016-07-13 19:25:30');
INSERT INTO modulos (id, nombre, status, imagen, created, modified) VALUES (3, 'Retenciones', NULL, 'edit-copy2.png', '2016-01-03 16:09:17', '2016-08-16 18:57:55');
INSERT INTO modulos (id, nombre, status, imagen, created, modified) VALUES (5, 'Ventas', NULL, 'x-office-spreadsheet.png', '2017-02-08 10:50:13', '2017-02-08 10:50:13');


--
-- TOC entry 2046 (class 0 OID 331970)
-- Dependencies: 152
-- Data for Name: oficinas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght, empresa_id) VALUES (10, 1, 'Administracion', 0, NULL, NULL, '2016-01-10 16:06:49', '2016-07-14 07:14:39', 'Admon', 0, 1, 1);
INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght, empresa_id) VALUES (1, NULL, 'Gerencia', 0, NULL, NULL, '2016-01-03 19:51:25', '2016-07-14 07:14:11', 'GR', 4, 4, 1);
INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght, empresa_id) VALUES (6, 2, 'Coordinacion Nacional de Producción de Operaciones', 0, NULL, NULL, '2016-01-06 00:50:03', '2016-01-06 01:20:26', 'CNPO', 4, 4, 1);
INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght, empresa_id) VALUES (3, 1, 'Almacen', 0, NULL, NULL, '2016-01-06 00:44:19', '2016-07-14 07:16:23', 'Alm', 4, 10, 1);
INSERT INTO oficinas (id, parent_id, descripcion, familia, status, usuario_id, created, modified, siglas, lft, rght, empresa_id) VALUES (12, 1, 'Detal', 0, NULL, NULL, '2016-07-14 07:16:49', '2016-07-14 07:16:49', 'Det', 2, 3, 1);


--
-- TOC entry 2047 (class 0 OID 331983)
-- Dependencies: 154
-- Data for Name: perfil; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO perfil (id, id_padre, descripcion, familia, status, usuario_id, created, modified) VALUES (1, 0, 'Administrador', 0, '1', 0, '1900-01-01 00:00:00', '1900-01-01 00:00:00');
INSERT INTO perfil (id, id_padre, descripcion, familia, status, usuario_id, created, modified) VALUES (3, 0, 'Empleados', 3, NULL, NULL, '2009-06-08 17:29:08', '2009-06-08 17:29:08');
INSERT INTO perfil (id, id_padre, descripcion, familia, status, usuario_id, created, modified) VALUES (2, 0, 'Superusuario', 0, NULL, NULL, '2009-01-29 12:12:39', '2011-01-30 21:34:33');
INSERT INTO perfil (id, id_padre, descripcion, familia, status, usuario_id, created, modified) VALUES (10, NULL, 'Cuenta por Pagar', 10, NULL, NULL, '2016-08-30 15:03:58', '2016-08-30 16:18:30');
INSERT INTO perfil (id, id_padre, descripcion, familia, status, usuario_id, created, modified) VALUES (11, NULL, 'Cuenta por Cobrar', 11, NULL, NULL, '2016-08-30 16:15:28', '2016-08-30 16:18:38');


--
-- TOC entry 2048 (class 0 OID 331995)
-- Dependencies: 156
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO usuario (id, empleado_id, perfil_id, usuario, clave, status, usuario_id, created, modified, oficina_id) VALUES (4, 3, 1, 'gpalma', '20ec660b980da3aa5da69392833c69987a027302', NULL, NULL, '2011-02-14 11:46:19', '2016-07-14 07:17:53', 1);
INSERT INTO usuario (id, empleado_id, perfil_id, usuario, clave, status, usuario_id, created, modified, oficina_id) VALUES (5, 5, 1, 'lrivero', '6559ad050b5f66c9439bc36ad51f7965f65a4590', NULL, NULL, '2016-02-08 18:34:34', '2016-07-16 07:14:40', 1);
INSERT INTO usuario (id, empleado_id, perfil_id, usuario, clave, status, usuario_id, created, modified, oficina_id) VALUES (7, 8, 1, 'pgraterol', '05d03af78f5e84ad5eceab408e9b3ca556819da0', NULL, NULL, '2016-08-30 14:43:53', '2016-08-30 14:43:53', 1);
INSERT INTO usuario (id, empleado_id, perfil_id, usuario, clave, status, usuario_id, created, modified, oficina_id) VALUES (8, 9, 10, 'dember', 'b811df902e03c44122aeab2a020aa1c09f94f185', NULL, NULL, '2016-08-30 15:05:08', '2016-08-30 15:05:08', 1);
INSERT INTO usuario (id, empleado_id, perfil_id, usuario, clave, status, usuario_id, created, modified, oficina_id) VALUES (9, 10, 11, 'rr', 'd2df16bee9d37212d9dc0a9709c3a8579fedcfff', NULL, NULL, '2016-08-30 16:21:23', '2016-08-30 16:21:23', 1);
INSERT INTO usuario (id, empleado_id, perfil_id, usuario, clave, status, usuario_id, created, modified, oficina_id) VALUES (10, 11, 11, 'dayana', '8c546c2cd303aac846fb0d4480e5864b569a1b4a', NULL, NULL, '2016-10-26 16:42:00', '2016-10-26 16:42:00', 1);


--
-- TOC entry 2029 (class 2606 OID 340042)
-- Dependencies: 161 161
-- Name: concheques_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY concheques
    ADD CONSTRAINT concheques_pkey PRIMARY KEY (id);


--
-- TOC entry 2027 (class 2606 OID 340010)
-- Dependencies: 159 159
-- Name: concuadrediarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY concuadrediarios
    ADD CONSTRAINT concuadrediarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2031 (class 2606 OID 340060)
-- Dependencies: 163 163
-- Name: condepositos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY condepositos
    ADD CONSTRAINT condepositos_pkey PRIMARY KEY (id);


--
-- TOC entry 2009 (class 2606 OID 332016)
-- Dependencies: 140 140
-- Name: configuracion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY configuracion
    ADD CONSTRAINT configuracion_pkey PRIMARY KEY (id);


--
-- TOC entry 2033 (class 2606 OID 340077)
-- Dependencies: 165 165
-- Name: congastos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY congastos
    ADD CONSTRAINT congastos_pkey PRIMARY KEY (id);


--
-- TOC entry 2039 (class 2606 OID 340179)
-- Dependencies: 172 172
-- Name: conmovbancarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY conmovbancarios
    ADD CONSTRAINT conmovbancarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2035 (class 2606 OID 340094)
-- Dependencies: 167 167
-- Name: contarjetas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY contarjetas
    ADD CONSTRAINT contarjetas_pkey PRIMARY KEY (id);


--
-- TOC entry 2037 (class 2606 OID 340112)
-- Dependencies: 169 169
-- Name: contrans_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY contrans
    ADD CONSTRAINT contrans_pkey PRIMARY KEY (id);


--
-- TOC entry 2011 (class 2606 OID 332018)
-- Dependencies: 142 142
-- Name: empleados_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY empleados
    ADD CONSTRAINT empleados_pkey PRIMARY KEY (id);


--
-- TOC entry 2013 (class 2606 OID 332020)
-- Dependencies: 144 144
-- Name: empresas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY empresas
    ADD CONSTRAINT empresas_pkey PRIMARY KEY (id);


--
-- TOC entry 2015 (class 2606 OID 332022)
-- Dependencies: 146 146
-- Name: funcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY funcion
    ADD CONSTRAINT funcion_pkey PRIMARY KEY (id);


--
-- TOC entry 2017 (class 2606 OID 332024)
-- Dependencies: 148 148
-- Name: grupo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY grupo
    ADD CONSTRAINT grupo_pkey PRIMARY KEY (id);


--
-- TOC entry 2019 (class 2606 OID 332026)
-- Dependencies: 150 150
-- Name: modulos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY modulos
    ADD CONSTRAINT modulos_pkey PRIMARY KEY (id);


--
-- TOC entry 2021 (class 2606 OID 332028)
-- Dependencies: 152 152
-- Name: oficina_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY oficinas
    ADD CONSTRAINT oficina_pkey PRIMARY KEY (id);


--
-- TOC entry 2023 (class 2606 OID 332030)
-- Dependencies: 154 154
-- Name: perfil_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY perfil
    ADD CONSTRAINT perfil_pkey PRIMARY KEY (id);


--
-- TOC entry 2025 (class 2606 OID 332032)
-- Dependencies: 156 156
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2060 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- TOC entry 2061 (class 0 OID 0)
-- Dependencies: 187
-- Name: dblink_connect_u(text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION dblink_connect_u(text) FROM PUBLIC;
REVOKE ALL ON FUNCTION dblink_connect_u(text) FROM postgres;
GRANT ALL ON FUNCTION dblink_connect_u(text) TO postgres;


--
-- TOC entry 2062 (class 0 OID 0)
-- Dependencies: 188
-- Name: dblink_connect_u(text, text); Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON FUNCTION dblink_connect_u(text, text) FROM PUBLIC;
REVOKE ALL ON FUNCTION dblink_connect_u(text, text) FROM postgres;
GRANT ALL ON FUNCTION dblink_connect_u(text, text) TO postgres;


-- Completed on 2017-05-12 00:28:55 VET

--
-- PostgreSQL database dump complete
--

