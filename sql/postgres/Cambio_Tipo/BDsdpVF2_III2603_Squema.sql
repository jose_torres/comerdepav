--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.5
-- Dumped by pg_dump version 9.3.1
-- Started on 2017-03-26 16:03:13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 462 (class 3079 OID 12670)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 4327 (class 0 OID 0)
-- Dependencies: 462
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 475 (class 1255 OID 97511)
-- Name: crearcargo(bigint, text, character varying, character varying, date, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearcargo(pcodigo bigint, pdescripcion text, pautorizado character varying, presponsable character varying, pfecha date, pcodsucursal bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	ultimovalor int;
BEGIN
	INSERT INTO cargos 
            (codcargo, descripcion, autorizado, responsable, fecha, codsucursal) 
             VALUES (pcodigo, pdescripcion,pautorizado, presponsable, pfecha, pcodsucursal) ;
	RETURN pcodigo;
END;
$$;


ALTER FUNCTION public.crearcargo(pcodigo bigint, pdescripcion text, pautorizado character varying, presponsable character varying, pfecha date, pcodsucursal bigint) OWNER TO postgres;

--
-- TOC entry 476 (class 1255 OID 97512)
-- Name: crearccabono(bigint, character varying, character varying, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearccabono(pcodccabono bigint, pnrodocumento character varying, pconcepto character varying, pfecha date) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN

	INSERT INTO ccabono(codccabono,nrodocumento,concepto,fecha) 
         VALUES (pcodccabono,pnrodocumento,pconcepto,pfecha) ;
	RETURN pcodccabono;
END;
$$;


ALTER FUNCTION public.crearccabono(pcodccabono bigint, pnrodocumento character varying, pconcepto character varying, pfecha date) OWNER TO postgres;

--
-- TOC entry 477 (class 1255 OID 97513)
-- Name: crearccnotacredito(bigint, bigint, character varying, character varying, date, bigint, bigint, real); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearccnotacredito(pcodccnotacredito bigint, pcodsucursal bigint, pnrodocumento character varying, pconcepto character varying, pfecha date, pcodcliente bigint, pcodclientesucursal bigint, pmonto real) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN

	INSERT INTO ccnotacredito(codccnotacredito,codsucursal,nrodocumento,concepto,fecha,codcliente,codclientesucursal,monto) 
         VALUES (pcodccnotacredito,pcodsucursal,pnrodocumento,pconcepto,pfecha,pcodcliente,pcodclientesucursal,pmonto);
	RETURN pcodccnotacredito;
END;
$$;


ALTER FUNCTION public.crearccnotacredito(pcodccnotacredito bigint, pcodsucursal bigint, pnrodocumento character varying, pconcepto character varying, pfecha date, pcodcliente bigint, pcodclientesucursal bigint, pmonto real) OWNER TO postgres;

--
-- TOC entry 478 (class 1255 OID 97514)
-- Name: crearcpabono(bigint, character varying, character varying, date); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearcpabono(pcodcpabono bigint, pnrodocumento character varying, pconcepto character varying, pfecha date) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN

	INSERT INTO cpabono(codcpabono,nrodocumento,concepto,fecha) 
         VALUES (pcodcpabono,pnrodocumento,pconcepto,pfecha) ;
	RETURN pcodcpabono;
END;
$$;


ALTER FUNCTION public.crearcpabono(pcodcpabono bigint, pnrodocumento character varying, pconcepto character varying, pfecha date) OWNER TO postgres;

--
-- TOC entry 479 (class 1255 OID 97515)
-- Name: crearcpfactura(bigint, character varying, character varying, date, integer, date, double precision, character varying, integer, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearcpfactura(pcodcpfactura bigint, pnrodocumento character varying, pconcepto character varying, pfecha date, pcodproveedor integer, pfechavencimiento date, pmonto double precision, pnrocontrol character varying, pcodsucursal integer, pbase1 double precision, pbase2 double precision, pbase3 double precision, piva1 double precision, piva2 double precision, piva3 double precision, ppor1 double precision, ppor2 double precision, ppor3 double precision, pmontoexento double precision) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN

	INSERT INTO cpfactura(codcpfactura,nrodocumento,concepto,fecha,codproveedor,fechavencimiento,monto,nrocontrol,codsucursal,baseimp1,baseimp2,baseimp3,ivaimp1,ivaimp2,ivaimp3,porimp1,porimp2,porimp3,montoexento) 
         VALUES (pcodcpfactura,pnrodocumento,pconcepto,pfecha,pcodproveedor,pfechavencimiento,pmonto,pnrocontrol,pcodsucursal,pbase1,pbase2,pbase3,piva1,piva2,piva3,ppor1,ppor2,ppor3,pmontoexento) ;
	RETURN pcodcpfactura;
END;
$$;


ALTER FUNCTION public.crearcpfactura(pcodcpfactura bigint, pnrodocumento character varying, pconcepto character varying, pfecha date, pcodproveedor integer, pfechavencimiento date, pmonto double precision, pnrocontrol character varying, pcodsucursal integer, pbase1 double precision, pbase2 double precision, pbase3 double precision, piva1 double precision, piva2 double precision, piva3 double precision, ppor1 double precision, ppor2 double precision, ppor3 double precision, pmontoexento double precision) OWNER TO postgres;

--
-- TOC entry 480 (class 1255 OID 97516)
-- Name: crearcpnotacredito(bigint, character varying, character varying, date, integer, date, double precision, integer, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearcpnotacredito(pcodcpnotacredito bigint, pnrodocumento character varying, pconcepto character varying, pfecha date, pcodproveedor integer, pfechavencimiento date, pmonto double precision, pcodsucursal integer, pnrocontrol character varying, pfacturaafecta character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN

	INSERT INTO cpnotacredito(codcpnotacredito,nrodocumento,concepto,fecha,codproveedor,fechavencimiento,monto,codsucursal,nrocontrol,facturaafecta) 
         VALUES (pcodcpnotacredito,pnrodocumento,pconcepto,pfecha,pcodproveedor,pfechavencimiento,pmonto,pcodsucursal,pnrocontrol,pfacturaafecta) ;
	RETURN pcodcpnotacredito;
END;
$$;


ALTER FUNCTION public.crearcpnotacredito(pcodcpnotacredito bigint, pnrodocumento character varying, pconcepto character varying, pfecha date, pcodproveedor integer, pfechavencimiento date, pmonto double precision, pcodsucursal integer, pnrocontrol character varying, pfacturaafecta character varying) OWNER TO postgres;

--
-- TOC entry 481 (class 1255 OID 97517)
-- Name: crearcpnotadebito(bigint, character varying, character varying, date, integer, double precision, integer, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearcpnotadebito(pcodcpnotadebito bigint, pnrodocumento character varying, pconcepto character varying, pfecha date, pproveedor integer, pmonto double precision, psucursal integer, pfacturaafecta character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
BEGIN

	INSERT INTO cpnotadebito(codcpnotadebito,nrodocumento,concepto,fecha,codproveedor,monto,codsucursal,facturaafecta) 
         VALUES (pcodcpnotadebito,pnrodocumento,pconcepto,pfecha,pproveedor,pmonto,psucursal,pfacturaafecta) ;
	RETURN pcodcpnotadebito;
END;
$$;


ALTER FUNCTION public.crearcpnotadebito(pcodcpnotadebito bigint, pnrodocumento character varying, pconcepto character varying, pfecha date, pproveedor integer, pmonto double precision, psucursal integer, pfacturaafecta character varying) OWNER TO postgres;

--
-- TOC entry 482 (class 1255 OID 97518)
-- Name: crearcuentasporpagarpagos(bigint, date, character varying, character varying); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearcuentasporpagarpagos(pcodpago bigint, pfecha date, pconcepto character varying, pnrodocumento character varying) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE


BEGIN

	INSERT INTO cuentasporpagarpagos(codpago,fecha,concepto,nrodocumento) 
         VALUES (pcodpago,pfecha,pconcepto,pnrodocumento) ;


	RETURN pcodpago;

END;

$$;


ALTER FUNCTION public.crearcuentasporpagarpagos(pcodpago bigint, pfecha date, pconcepto character varying, pnrodocumento character varying) OWNER TO postgres;

--
-- TOC entry 483 (class 1255 OID 97519)
-- Name: creardescargo(bigint, text, character varying, character varying, date, boolean, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION creardescargo(pcodigo bigint, pdescripcion text, pautorizado character varying, presponsable character varying, pfecha date, pusointerno boolean, pcodsucursal bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	ultimovalor int;
BEGIN
	INSERT INTO descargos 
            (coddescargo, descripcion, autorizado, responsable, fecha, usointerno, codsucursal) 
             VALUES (pcodigo, pdescripcion,pautorizado, presponsable, pfecha, pusointerno, pcodsucursal) ;
	RETURN pcodigo;
END;
$$;


ALTER FUNCTION public.creardescargo(pcodigo bigint, pdescripcion text, pautorizado character varying, presponsable character varying, pfecha date, pusointerno boolean, pcodsucursal bigint) OWNER TO postgres;

--
-- TOC entry 484 (class 1255 OID 97520)
-- Name: creartraslado(bigint, bigint, bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION creartraslado(pcodcargo bigint, pcoddescargo bigint, pcodsucursalcargo bigint, pcodsucursaldescargo bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	ultimovalor int;
BEGIN
	INSERT INTO traslados 
            (codcargo, coddescargo, codsucursalcargo, codsucursaldescargo)
             VALUES (pcodcargo, pcoddescargo, pcodsucursalcargo, pcodsucursaldescargo) ;
	RETURN pcodcargo;
END;
$$;


ALTER FUNCTION public.creartraslado(pcodcargo bigint, pcoddescargo bigint, pcodsucursalcargo bigint, pcodsucursaldescargo bigint) OWNER TO postgres;

--
-- TOC entry 485 (class 1255 OID 97521)
-- Name: crearventa(integer, integer, integer, date, integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION crearventa(pcodventa integer, pcodsucursal integer, pcodnumerofactura integer, pfecha date, pcodvendedor integer, pcodcliente integer, pcodclientesucursal integer) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	ultimovalor int;
	numer int4;
	long int4;
	pref varchar(2);
BEGIN
	--El último número al que se le va a 
	SELECT numero INTO numer FROM numerofactura WHERE codnumerofactura = pcodnumerofactura;
	SELECT prefijo INTO pref FROM numerofactura WHERE codnumerofactura = pcodnumerofactura;
	SELECT long INTO long FROM numerofactura WHERE codnumerofactura = pcodnumerofactura;
	numer := numer + 1;
	INSERT INTO ventas (codventas, codsucursal, numerofactura, fecha, codcliente, codvendedor, codclientesucursal) VALUES 
	(pcodventa,pcodsucursal,pref || LPAD(numer::text, long, '0'),pfecha,pcodcliente,pcodvendedor,pcodclientesucursal);
	RETURN pcodventa;
END;
$$;


ALTER FUNCTION public.crearventa(pcodventa integer, pcodsucursal integer, pcodnumerofactura integer, pfecha date, pcodvendedor integer, pcodcliente integer, pcodclientesucursal integer) OWNER TO postgres;

--
-- TOC entry 486 (class 1255 OID 97522)
-- Name: funcioncambiocambiosucursal(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncambiocambiosucursal() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 x bigint;
 registro RECORD;
BEGIN
 --Si es cargo genera cambio solo a la sucursal principal
 IF NEW.tabla = 'cargos' OR NEW.tabla = 'descargos' THEN
  --Sólo las sucursales secundarias registrarán cargos en cambios asi que no pregunto si es sucursal secundaria
  SELECT sucursalprincipal INTO x FROM configuracion;
  INSERT INTO cambiossucursal VALUES (NEW.codcambio, x);
 ELSE
  FOR registro IN SELECT * FROM sucursal WHERE codsucursal NOT IN (SELECT sucursal FROM configuracion) LOOP
   INSERT INTO cambiossucursal VALUES (NEW.codcambio, registro.codsucursal);
  END LOOP;
 END IF;
 RETURN NULL;
END;
$$;


ALTER FUNCTION public.funcioncambiocambiosucursal() OWNER TO postgres;

--
-- TOC entry 487 (class 1255 OID 97523)
-- Name: funcioncambiocargo(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncambiocargo() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 x bigint;
BEGIN
 --Si el cargo lo hace cualquier sucursal menos la principal
 SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal=sucursalprincipal;
 IF x = 0 THEN --Si es la sucursal
  --Guardo en la tabla z_cargos
  SELECT COUNT(*) INTO x FROM z_cambios WHERE codcambio = NEW.codcargo and tabla = 'cargos' and codsucursal = (SELECT sucursalprincipal FROM configuracion);
  IF x = 0 THEN --Si no esta aun en la tabla z_cargos
   INSERT INTO z_cambios (codcambio, tabla, codsucursal) VALUES (NEW.codcargo,'cargos',(SELECT sucursalprincipal FROM configuracion));
  END IF; --Si no esta aún en la tabla z_cargos
 END IF;--Si es la sucursal
 RETURN NULL; 
END;
$$;


ALTER FUNCTION public.funcioncambiocargo() OWNER TO postgres;

--
-- TOC entry 488 (class 1255 OID 97524)
-- Name: funcioncambiocliente(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncambiocliente() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 x bigint;
 registro RECORD;
BEGIN
 
 IF NEW.modificado = 0 THEN --Debe ir a z_cambios
  SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal=sucursalprincipal;
  IF x = 0 THEN --Si es la sucursal genera cambio solo a la principal
   INSERT INTO z_cambios (codcambio, tabla, codsucursal) VALUES (NEW.codcliente, 'Clientes'||NEW.codsucursal::text,(SELECT sucursalprincipal FROM configuracion));
  ELSE --Si es la principal genera cambio a las demas sucursales 
   FOR registro IN SELECT * FROM sucursal INNER JOIN configuracion ON sucursal.codsucursal <> configuracion.sucursalprincipal WHERE estatus = true LOOP
    INSERT INTO z_cambios (codcambio, tabla, codsucursal) VALUES (NEW.codcliente, 'Clientes'||NEW.codsucursal::text,registro.codsucursal);
   END LOOP;
  END IF;--Si es la sucursal
 ELSE --Si modificado <> 0
  SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal=sucursalprincipal;
  IF x > 0 THEN --Si es la principal genera cambio a las demas sucursales distinto al NEW.modificado
   FOR registro IN SELECT * FROM sucursal S INNER JOIN configuracion C ON S.codsucursal <> C.sucursalprincipal WHERE estatus = true AND S.codsucursal <> NEW.modificado LOOP
    INSERT INTO z_cambios (codcambio, tabla, codsucursal) VALUES (NEW.codcliente, 'Clientes'||NEW.codsucursal::text,registro.codsucursal);
   END LOOP;
  END IF;--Si es la sucursal
 END IF;
 RETURN NULL; 
END;
$$;


ALTER FUNCTION public.funcioncambiocliente() OWNER TO postgres;

--
-- TOC entry 489 (class 1255 OID 97525)
-- Name: funcioncambiodeposito(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncambiodeposito() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    

DECLARE
 x bigint;
 y boolean;
 ultimovalor int4;
 registro RECORD; 

BEGIN
 y = false;
 SELECT count(*) INTO x FROM depositos WHERE coddeposito = NEW.coddeposito AND codsucursal IN (SELECT sucursal FROM configuracion );
 IF x = 0 THEN
  SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal = sucursalprincipal;
  IF x <> 0 THEN
   y = true;
  END IF;
 ELSE
  y = true;
 END IF;

IF y THEN --Si Y verdadero entonces o es un deposito de la sucursal, o es la sucursal principal
 --Registro el cambio en la tabla z!
 SELECT COUNT(*) INTO x FROM z_productosdepositos WHERE coddeposito = NEW.coddeposito AND codproducto = NEW.codproducto;
 IF x = 0 THEN
  INSERT INTO z_productosdepositos (coddeposito, codproducto, cantidad) VALUES (NEW.coddeposito, NEW.codproducto, NEW.cantidad);
  SELECT last_value INTO ultimovalor FROM z_productosdepositos_z_codproductosdepositos_seq;
 ELSE
  UPDATE z_productosdepositos SET cantidad = NEW.cantidad WHERE coddeposito = NEW.coddeposito AND codproducto = NEW.codproducto;
  SELECT z_codproductosdepositos INTO ultimovalor FROM z_productosdepositos WHERE coddeposito = NEW.coddeposito AND codproducto = NEW.codproducto;
 END IF;
 SELECT COUNT(*) INTO x FROM configuracion WHERE  sucursalprincipal = sucursal;
 IF x = 0 THEN  --Si es sucursal 
  SELECT COUNT(*) INTO x FROM depositos WHERE coddeposito = NEW.coddeposito AND codsucursal = (SELECT sucursal FROM configuracion);
  IF x = 1 THEN --Si el depósito es propio de la sucursal entonces
   --Generar cambios solo para la principal
   SELECT COUNT(*) INTO x FROM z_cambios WHERE codcambio = ultimovalor AND tabla = 'z_productosdepositos' AND codsucursal = (SELECT sucursalprincipal FROM configuracion);
   IF x = 0 THEN -- SI el cambio noe sta registrado aun para la principal
    INSERT INTO z_cambios (codcambio, tabla, codsucursal) VALUES (ultimovalor,'z_productosdepositos',(SELECT sucursalprincipal FROM configuracion));
   END IF; --Si el cambio no esta para la principal aun
  END IF; --Si el depósito es propio
 ELSE -- Si es la principal
  --Generar cambios para todas las sucursales menos la dueña del depósito, en caso de que no pertenezca a la sucursal principal
   FOR registro IN SELECT * FROM sucursal WHERE estatus = true AND NOT codsucursal = (SELECT sucursalprincipal FROM configuracion) AND NOT codsucursal = (SELECT codsucursal FROM depositos WHERE coddeposito = NEW.coddeposito) LOOP
    SELECT COUNT(*) INTO x FROM z_cambios WHERE codcambio = ultimovalor AND tabla = 'z_productosdepositos' AND codsucursal = registro.codsucursal;
    IF x = 0 THEN
     INSERT INTO z_cambios (codcambio, tabla, codsucursal) VALUES (ultimovalor,'z_productosdepositos',registro.codsucursal);
    END IF;
   END LOOP;
 END IF; -- Si es de sucursal
 RETURN NULL; 
ELSE
 RETURN NULL;
END IF; --Si Y es verdadero
END;

$$;


ALTER FUNCTION public.funcioncambiodeposito() OWNER TO postgres;

--
-- TOC entry 490 (class 1255 OID 97526)
-- Name: funcioncambiodescargo(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncambiodescargo() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 x bigint;
BEGIN
 --Si el descargo lo hace cualquier sucursal menos la principal
 SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal=sucursalprincipal;
 IF x = 0 THEN --Si es la sucursal
  --Guardo en la tabla z_cambios como descargo
  SELECT COUNT(*) INTO x FROM z_cambios WHERE codcambio = NEW.coddescargo and tabla = 'descargos' and codsucursal = (SELECT sucursalprincipal FROM configuracion);
  IF x = 0 THEN --Si no esta aun en la tabla z_cambios
   INSERT INTO z_cambios (codcambio, tabla, codsucursal) VALUES (NEW.coddescargo,'descargos',(SELECT sucursalprincipal FROM configuracion));
  END IF; --Si no esta aún en la tabla z_cambios
 END IF;--Si es la sucursal
 RETURN NULL; 
END;
$$;


ALTER FUNCTION public.funcioncambiodescargo() OWNER TO postgres;

--
-- TOC entry 491 (class 1255 OID 97527)
-- Name: funcioncambioiva(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncambioiva() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
 sql text;
BEGIN
 sql = text('''');
 IF tg_op = 'UPDATE' THEN -- SI ES UPDATE
  
 ELSE -- SI ES INSERT
  INSERT INTO c_iva (codiva, porcentaje, descripcion) VALUES (NEW.codiva, NEW.porcentaje, NEW.descripcion);
 END IF;
 RETURN NULL; 
END;$$;


ALTER FUNCTION public.funcioncambioiva() OWNER TO postgres;

--
-- TOC entry 492 (class 1255 OID 97528)
-- Name: funcioncambioproducto(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncambioproducto() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
 x bigint; 
 ultimovalor int4; 
 registro RECORD; 
BEGIN 
 --Solo ejecutará este código si es la sucursal
 SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal=1;
 If x = 1 Then
  select count(*) into x from z_producto where codproducto = NEW.codproducto; 
  If x = 0 Then 
   INSERT INTO z_producto (codproducto) VALUES (NEW.codproducto); 
  END IF;
  FOR registro IN SELECT * FROM sucursal WHERE estatus = true AND NOT codsucursal = (SELECT sucursalprincipal FROM configuracion) AND codsucursal NOT IN (SELECT codsucursal FROM z_cambios WHERE codcambio = NEW.codproducto AND tabla = 'z_producto') LOOP
   INSERT INTO z_cambios (codcambio, tabla, codsucursal) VALUES (NEW.codproducto,'z_producto',registro.codsucursal);
  END LOOP;
IF tg_op = 'UPDATE' THEN --Si es update, pasa a cambio solo si el nuevo valor es distinto, sino no es cambio 


 
 IF (NEW.numero <> OLD.numero) OR   (NEW.numero is not null and OLD.numero is null)   THEN 
  UPDATE z_producto SET numero=NEW.numero WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.numero <> OLD.numero THEN 
 
 IF (NEW.factorconversion <> OLD.factorconversion) OR   (NEW.factorconversion is not null and OLD.factorconversion is null)   THEN 
  UPDATE z_producto SET factorconversion=NEW.factorconversion WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.factorconversion <> OLD.factorconversion THEN 
 
 IF (NEW.stockmaximo <> OLD.stockmaximo) OR   (NEW.stockmaximo is not null and OLD.stockmaximo is null)   THEN 
  UPDATE z_producto SET stockmaximo=NEW.stockmaximo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.stockmaximo <> OLD.stockmaximo THEN 
 
 IF (NEW.stockminimo <> OLD.stockminimo) OR   (NEW.stockminimo is not null and OLD.stockminimo is null)   THEN 
  UPDATE z_producto SET stockminimo=NEW.stockminimo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.stockminimo <> OLD.stockminimo THEN 
 
 IF (NEW.codiva <> OLD.codiva) OR   (NEW.codiva is not null and OLD.codiva is null)   THEN 
  UPDATE z_producto SET codiva=NEW.codiva WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codiva <> OLD.codiva THEN 
 
 IF (NEW.codmarca <> OLD.codmarca) OR   (NEW.codmarca is not null and OLD.codmarca is null)   THEN 
  UPDATE z_producto SET codmarca=NEW.codmarca WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codmarca <> OLD.codmarca THEN 
 
 IF (NEW.coddepartamento <> OLD.coddepartamento) OR   (NEW.coddepartamento is not null and OLD.coddepartamento is null)   THEN 
  UPDATE z_producto SET coddepartamento=NEW.coddepartamento WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.coddepartamento <> OLD.coddepartamento THEN 
 
 IF (NEW.codproducto <> OLD.codproducto) OR   (NEW.codproducto is not null and OLD.codproducto is null)   THEN 
  UPDATE z_producto SET codproducto=NEW.codproducto WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codproducto <> OLD.codproducto THEN 
 
 IF (NEW.texto <> OLD.texto) OR   (NEW.texto is not null and OLD.texto is null)   THEN 
  UPDATE z_producto SET texto=NEW.texto WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.texto <> OLD.texto THEN 
 
 IF (NEW.foto <> OLD.foto) OR   (NEW.foto is not null and OLD.foto is null)   THEN 
  UPDATE z_producto SET foto=NEW.foto WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.foto <> OLD.foto THEN 
 
 IF (NEW.nombre <> OLD.nombre) OR   (NEW.nombre is not null and OLD.nombre is null)   THEN 
  UPDATE z_producto SET nombre=NEW.nombre WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.nombre <> OLD.nombre THEN 
 
 IF (NEW.preciodetal1 <> OLD.preciodetal1) OR   (NEW.preciodetal1 is not null and OLD.preciodetal1 is null)   THEN 
  UPDATE z_producto SET preciodetal1=NEW.preciodetal1 WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciodetal1 <> OLD.preciodetal1 THEN 
 
 IF (NEW.preciomayor1 <> OLD.preciomayor1) OR   (NEW.preciomayor1 is not null and OLD.preciomayor1 is null)   THEN 
  UPDATE z_producto SET preciomayor1=NEW.preciomayor1 WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciomayor1 <> OLD.preciomayor1 THEN 
 
 IF (NEW.preciominimo1 <> OLD.preciominimo1) OR   (NEW.preciominimo1 is not null and OLD.preciominimo1 is null)   THEN 
  UPDATE z_producto SET preciominimo1=NEW.preciominimo1 WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciominimo1 <> OLD.preciominimo1 THEN 
 
 IF (NEW.float <> OLD.float) OR   (NEW.float is not null and OLD.float is null)   THEN 
  UPDATE z_producto SET float=NEW.float WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.float <> OLD.float THEN 
 
 IF (NEW.preciodetal <> OLD.preciodetal) OR   (NEW.preciodetal is not null and OLD.preciodetal is null)   THEN 
  UPDATE z_producto SET preciodetal=NEW.preciodetal WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciodetal <> OLD.preciodetal THEN 
 
 IF (NEW.preciomayor <> OLD.preciomayor) OR   (NEW.preciomayor is not null and OLD.preciomayor is null)   THEN 
  UPDATE z_producto SET preciomayor=NEW.preciomayor WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciomayor <> OLD.preciomayor THEN 
 
 IF (NEW.preciominimo <> OLD.preciominimo) OR   (NEW.preciominimo is not null and OLD.preciominimo is null)   THEN 
  UPDATE z_producto SET preciominimo=NEW.preciominimo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciominimo <> OLD.preciominimo THEN 
 
 IF (NEW.costoanterior <> OLD.costoanterior) OR   (NEW.costoanterior is not null and OLD.costoanterior is null)   THEN 
  UPDATE z_producto SET costoanterior=NEW.costoanterior WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.costoanterior <> OLD.costoanterior THEN 
 
 IF (NEW.costopromedio <> OLD.costopromedio) OR   (NEW.costopromedio is not null and OLD.costopromedio is null)   THEN 
  UPDATE z_producto SET costopromedio=NEW.costopromedio WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.costopromedio <> OLD.costopromedio THEN 
 
 IF (NEW.costoactual <> OLD.costoactual) OR   (NEW.costoactual is not null and OLD.costoactual is null)   THEN 
  UPDATE z_producto SET costoactual=NEW.costoactual WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.costoactual <> OLD.costoactual THEN 
 
 IF (NEW.varchar <> OLD.varchar) OR   (NEW.varchar is not null and OLD.varchar is null)   THEN 
  UPDATE z_producto SET varchar=NEW.varchar WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.varchar <> OLD.varchar THEN 
 
 IF (NEW.unidadalterna <> OLD.unidadalterna) OR   (NEW.unidadalterna is not null and OLD.unidadalterna is null)   THEN 
  UPDATE z_producto SET unidadalterna=NEW.unidadalterna WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.unidadalterna <> OLD.unidadalterna THEN 
 
 IF (NEW.unidadprincipal <> OLD.unidadprincipal) OR   (NEW.unidadprincipal is not null and OLD.unidadprincipal is null)   THEN 
  UPDATE z_producto SET unidadprincipal=NEW.unidadprincipal WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.unidadprincipal <> OLD.unidadprincipal THEN 
 
 IF (NEW.estado <> OLD.estado) OR   (NEW.estado is not null and OLD.estado is null)   THEN 
  UPDATE z_producto SET estado=NEW.estado WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.estado <> OLD.estado THEN 
 
 IF (NEW.codbarra <> OLD.codbarra) OR   (NEW.codbarra is not null and OLD.codbarra is null)   THEN 
  UPDATE z_producto SET codbarra=NEW.codbarra WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codbarra <> OLD.codbarra THEN 
 
 IF (NEW.referencia <> OLD.referencia) OR   (NEW.referencia is not null and OLD.referencia is null)   THEN 
  UPDATE z_producto SET referencia=NEW.referencia WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.referencia <> OLD.referencia THEN 
 
 IF (NEW.modelo <> OLD.modelo) OR   (NEW.modelo is not null and OLD.modelo is null)   THEN 
  UPDATE z_producto SET modelo=NEW.modelo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.modelo <> OLD.modelo THEN 
 
 IF (NEW.codigo <> OLD.codigo) OR   (NEW.codigo is not null and OLD.codigo is null)   THEN 
  UPDATE z_producto SET codigo=NEW.codigo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codigo <> OLD.codigo THEN 
 
 IF (NEW.fecha <> OLD.fecha) OR   (NEW.fecha is not null and OLD.fecha is null)   THEN 
  UPDATE z_producto SET fecha=NEW.fecha WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.fecha <> OLD.fecha THEN 

ELSE --Si es INSERT lo incluyo directamente como cambio 


 UPDATE z_producto SET numero=NEW.numero WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET factorconversion=NEW.factorconversion WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET stockmaximo=NEW.stockmaximo WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET stockminimo=NEW.stockminimo WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET codiva=NEW.codiva WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET codmarca=NEW.codmarca WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET coddepartamento=NEW.coddepartamento WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET codproducto=NEW.codproducto WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET texto=NEW.texto WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET foto=NEW.foto WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET nombre=NEW.nombre WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET preciodetal1=NEW.preciodetal1 WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET preciomayor1=NEW.preciomayor1 WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET preciominimo1=NEW.preciominimo1 WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET float=NEW.float WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET preciodetal=NEW.preciodetal WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET preciomayor=NEW.preciomayor WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET preciominimo=NEW.preciominimo WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET costoanterior=NEW.costoanterior WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET costopromedio=NEW.costopromedio WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET costoactual=NEW.costoactual WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET varchar=NEW.varchar WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET unidadalterna=NEW.unidadalterna WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET unidadprincipal=NEW.unidadprincipal WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET estado=NEW.estado WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET codbarra=NEW.codbarra WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET referencia=NEW.referencia WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET modelo=NEW.modelo WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET codigo=NEW.codigo WHERE codproducto=NEW.codproducto;
 UPDATE z_producto SET fecha=NEW.fecha WHERE codproducto=NEW.codproducto;

END IF; --IF tg_op = 'UPDATE' THEN 
END IF; --IF x = 0 THEN
RETURN NULL; 
END; 
$$;


ALTER FUNCTION public.funcioncambioproducto() OWNER TO postgres;

--
-- TOC entry 493 (class 1255 OID 97530)
-- Name: funcioncargardeposito(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncargardeposito() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
BEGIN
 --Si el deposito es de la sucursal suma..
 SELECT coddeposito INTO tem FROM depositos WHERE coddeposito= NEW.coddeposito AND codsucursal IN (SELECT sucursal FROM configuracion);
 IF FOUND THEN
  --Si el producto ya existe en el depósito
  SELECT coddeposito INTO tem FROM productosdepositos WHERE coddeposito= NEW.coddeposito and codproducto= NEW.codproducto;
  IF FOUND THEN
   UPDATE productosdepositos  
    SET cantidad = cantidad + CAST(NEW.cantidad AS FLOAT4) /CAST(NEW.factorconversion AS FLOAT4)
    WHERE coddeposito= NEW.coddeposito and codproducto= NEW.codproducto;
  ELSE
   INSERT INTO productosdepositos (coddeposito, cantidad, codproducto) VALUES
   (NEW.coddeposito,CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4), NEW.codproducto);
  END IF;
 END IF;
 RETURN NULL; 
END;$$;


ALTER FUNCTION public.funcioncargardeposito() OWNER TO postgres;

--
-- TOC entry 494 (class 1255 OID 97531)
-- Name: funcioncomprarproducto(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioncomprarproducto() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
 depo int4;
BEGIN
 IF tg_op = 'INSERT' THEN 
  SELECT coddeposito INTO depo FROM compras WHERE codcompra = NEW.codcompra;
  --Si el producto ya existe en el depósito
  SELECT coddeposito INTO tem FROM productosdepositos WHERE codproducto= NEW.codproducto AND coddeposito = depo;
  IF FOUND THEN
   UPDATE productosdepositos  
    SET cantidad = cantidad + CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4)
    WHERE coddeposito= tem and codproducto= NEW.codproducto;
  ELSE
   SELECT coddeposito INTO tem FROM compras WHERE codcompra = NEW.codcompra;
   INSERT INTO productosdepositos (coddeposito,cantidad,codproducto) VALUES
   (depo, CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4), NEW.codproducto);
  END IF;
 ELSE --Es un DELETE
  SELECT coddeposito INTO tem FROM compras WHERE codcompra = OLD.codcompra;
  UPDATE productosdepositos  
    SET cantidad = cantidad - CAST(OLD.cantidad AS FLOAT4) / CAST(OLD.factorconversion AS FLOAT4)
    WHERE coddeposito= tem and codproducto= OLD.codproducto;
 END IF;
 RETURN NULL; 
END;$$;


ALTER FUNCTION public.funcioncomprarproducto() OWNER TO postgres;

--
-- TOC entry 495 (class 1255 OID 97532)
-- Name: funciondescargardeposito(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funciondescargardeposito() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
BEGIN
 --Si el deposito es de la sucursal resta...
 SELECT coddeposito INTO tem FROM depositos WHERE coddeposito= NEW.coddeposito AND codsucursal IN (SELECT sucursal FROM configuracion);
 IF FOUND THEN
  --Si el producto ya existe en el depósito
  SELECT coddeposito INTO tem FROM productosdepositos WHERE coddeposito= NEW.coddeposito and codproducto= NEW.codproducto;
  IF FOUND THEN
   UPDATE productosdepositos 
    SET cantidad = cantidad - CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4)
    WHERE coddeposito= NEW.coddeposito and codproducto= NEW.codproducto;
  ELSE
   INSERT INTO productosdepositos (coddeposito, cantidad, codproducto)VALUES
    (NEW.coddeposito, -1 * CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4), NEW.codproducto); 
  END IF;
 END IF;
 RETURN NULL; 	
END;
$$;


ALTER FUNCTION public.funciondescargardeposito() OWNER TO postgres;

--
-- TOC entry 496 (class 1255 OID 97533)
-- Name: funciondevolverproductocompra(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funciondevolverproductocompra() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
BEGIN
 --Si el producto ya existe en el depósito
 SELECT coddeposito INTO tem FROM productosdepositos WHERE codproducto= NEW.codproducto AND coddeposito = (SELECT coddeposito FROM devolucioncompra WHERE coddevolucioncompra = NEW.coddevolucioncompra);
 IF FOUND THEN
  UPDATE productosdepositos  
   SET cantidad = cantidad - CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4)
   WHERE coddeposito= tem and codproducto= NEW.codproducto;
 ELSE
  SELECT coddeposito INTO tem FROM devolucioncompra WHERE coddevolucioncompra = NEW.coddevolucioncompra;
  INSERT INTO productosdepositos (coddeposito, cantidad, codproducto)VALUES
  (tem, -1*CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4), NEW.codproducto);
 END IF;
 RETURN NULL; 
END;$$;


ALTER FUNCTION public.funciondevolverproductocompra() OWNER TO postgres;

--
-- TOC entry 497 (class 1255 OID 97534)
-- Name: funciondevolverproductoventa(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funciondevolverproductoventa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
 cant float4;
 factor int4;
BEGIN
 --Me traigo el factor de conversión de ventasproductos
 SELECT V.factorconversion INTO factor
	FROM ventasproductos V
	INNER JOIN
		(SELECT codventa, codsucursalventa FROM devolucionventa WHERE coddevolucion = NEW.coddevolucion AND codsucursal = NEW.codsucursal) AS A
	ON V.codventa = A.codventa AND V.codsucursal = A.codsucursalventa
	WHERE V.codproducto = NEW.codproducto;
 --Si el producto ya existe en el depósito
 SELECT coddeposito INTO tem FROM productosdepositos WHERE codproducto= NEW.codproducto AND coddeposito = (SELECT coddeposito FROM devolucionventa WHERE coddevolucion = NEW.coddevolucion AND codsucursal = NEW.codsucursal);
 IF FOUND THEN
  UPDATE productosdepositos  
   SET cantidad = cantidad + CAST(NEW.cantidad AS FLOAT4) / CAST(factor AS FLOAT4)
   WHERE coddeposito= tem and codproducto= NEW.codproducto;
 ELSE
  SELECT coddeposito INTO tem FROM devolucionventa WHERE coddevolucion = NEW.coddevolucion AND codsucursal = NEW.codsucursal;
  INSERT INTO productosdepositos (coddeposito, cantidad, codproducto)VALUES
  (tem, CAST(NEW.cantidad AS FLOAT4) / CAST(factor AS FLOAT4), NEW.codproducto);
 END IF;
 RETURN NULL; 
END;$$;


ALTER FUNCTION public.funciondevolverproductoventa() OWNER TO postgres;

--
-- TOC entry 498 (class 1255 OID 97535)
-- Name: funcioninsertarz_cambios(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcioninsertarz_cambios() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 x bigint;
BEGIN
 SELECT COUNT(*) INTO x FROM z_cambios WHERE codcambio = NEW.codcambio AND tabla = NEW.tabla AND codsucursal = NEW.codsucursal;
 IF x = 0 THEN
  RETURN NEW;
 ELSE
  RETURN NULL;
 END IF;
END;
$$;


ALTER FUNCTION public.funcioninsertarz_cambios() OWNER TO postgres;

--
-- TOC entry 499 (class 1255 OID 97536)
-- Name: funcionnotaentregaproductos(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcionnotaentregaproductos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
 depo int4;
 estatus char;
BEGIN
 IF tg_op = 'INSERT' THEN 
  SELECT coddeposito INTO depo FROM nota_entrega WHERE codnotaentrega = NEW.codnotaentrega;
  --Si el producto ya existe en el depósito
  SELECT coddeposito INTO tem FROM productosdepositos WHERE codproducto= NEW.codproducto AND coddeposito = depo;
  IF FOUND THEN
   UPDATE productosdepositos  
    SET cantidad = cantidad - CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4)
    WHERE coddeposito= tem and codproducto= NEW.codproducto;
  ELSE
   SELECT coddeposito INTO tem FROM nota_entrega WHERE codnotaentrega = NEW.codnotaentrega;
   INSERT INTO productosdepositos (coddeposito,cantidad,codproducto) VALUES
   (depo, -1*CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4), NEW.codproducto);
  END IF;
 ELSE 
   IF tg_op = 'UPDATE' THEN
     SELECT estatus INTO estatus FROM nota_entrega WHERE codnotaentrega = OLD.codnotaentrega;
     IF estatus = 'E' THEN 
       SELECT coddeposito INTO tem FROM nota_entrega WHERE codnotaentrega = OLD.codnotaentrega;
       UPDATE productosdepositos  
       SET cantidad = cantidad + CAST(OLD.cantidad AS FLOAT4) / CAST(OLD.factorconversion AS FLOAT4)
       WHERE coddeposito= tem and codproducto= OLD.codproducto;
     END IF;
   ELSE --Es un DELETE
     SELECT coddeposito INTO tem FROM nota_entrega WHERE codnotaentrega = OLD.codnotaentrega;
     UPDATE productosdepositos  
     SET cantidad = cantidad + CAST(OLD.cantidad AS FLOAT4) / CAST(OLD.factorconversion AS FLOAT4)
     WHERE coddeposito= tem and codproducto= OLD.codproducto;
   END IF;
 END IF;
 RETURN NULL; 
END;$$;


ALTER FUNCTION public.funcionnotaentregaproductos() OWNER TO postgres;

--
-- TOC entry 500 (class 1255 OID 97537)
-- Name: funcionnotaentregaventa(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcionnotaentregaventa() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
 depo int4;
 est char;
 registro RECORD;
BEGIN
   SELECT estatus INTO est FROM nota_entrega WHERE codnotaentrega = OLD.codnotaentrega;
   IF est = 'E' THEN
     SELECT coddeposito INTO tem FROM nota_entrega WHERE codnotaentrega = OLD.codnotaentrega;
     FOR registro IN SELECT * FROM nota_entregaproductos WHERE codnotaentrega = OLD.codnotaentrega LOOP
       UPDATE productosdepositos
       SET cantidad = cantidad + CAST(registro.cantidad AS FLOAT4) / CAST(registro.factorconversion AS FLOAT4)
       WHERE coddeposito= tem and codproducto= registro.codproducto;
     END LOOP;     
   END IF;
 RETURN NULL; 
END;$$;


ALTER FUNCTION public.funcionnotaentregaventa() OWNER TO postgres;

--
-- TOC entry 501 (class 1255 OID 97538)
-- Name: funcionvenderproducto(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcionvenderproducto() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 tem int4;
 depo int4;
BEGIN
 IF tg_op = 'INSERT' THEN 
  SELECT coddeposito INTO depo FROM ventas WHERE codventa = NEW.codventa;
  --Si el producto ya existe en el depósito
  SELECT coddeposito INTO tem FROM productosdepositos WHERE codproducto= NEW.codproducto AND coddeposito = depo;
  IF FOUND THEN
   UPDATE productosdepositos  
    SET cantidad = cantidad - CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4)
    WHERE coddeposito= tem and codproducto= NEW.codproducto;
  ELSE
   SELECT coddeposito INTO tem FROM ventas WHERE codventa = NEW.codventa;
   INSERT INTO productosdepositos (coddeposito,cantidad,codproducto) VALUES
   (depo, -1*CAST(NEW.cantidad AS FLOAT4) / CAST(NEW.factorconversion AS FLOAT4), NEW.codproducto);
  END IF;
 ELSE --Es un DELETE
  SELECT coddeposito INTO tem FROM ventas WHERE codventa = OLD.codventa;
  UPDATE productosdepositos  
    SET cantidad = cantidad + CAST(OLD.cantidad AS FLOAT4) / CAST(OLD.factorconversion AS FLOAT4)
    WHERE coddeposito= tem and codproducto= OLD.codproducto;
 END IF;
 RETURN NULL; 
END;$$;


ALTER FUNCTION public.funcionvenderproducto() OWNER TO postgres;

--
-- TOC entry 502 (class 1255 OID 97539)
-- Name: funcionz_cambios(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcionz_cambios() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 x bigint;
BEGIN
IF NEW.tabla = 'cargos' THEn
 SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal = sucursalprincipal;
 IF x = 0 THEN --Si es una sucursal
  RETURN NEW;
 ELSE --SI es la principal
  SELECT COUNT(*) INTO x FROM cargos WHERE codcargo = NEW.codcargo AND codsucursal = NEW.codsucursal;
  IF x = 0 THEN --Si el cargo no existe
   INSERT INTO cargos (codcargo, codsucursal, descripcion, fecha, autorizado, responsable) VALUES (NEW.codcargo, NEW.codsucursal, NEW.descripcion, NEW.fecha, NEW.autorizado, NEW.responsable);
  ELSE --SI el cargo existe
   UPDATE cargos SET descripcion = NEW.descripcion, fecha = NEW.fecha, autorizado = NEW.autorizado, responsable = NEW.responsable WHERE codcargo = NEW.codcargo AND codsucursal = NEW.codsucursal;
  END IF; --Si el cargo no existe
  RETURN NULL;
 END IF; --Si es una sucursal
END IF; --Si NEW.tabla = 'cargos'
END;
$$;


ALTER FUNCTION public.funcionz_cambios() OWNER TO postgres;

--
-- TOC entry 503 (class 1255 OID 97540)
-- Name: funcionz_cargos(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcionz_cargos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$    
DECLARE
 x bigint;
BEGIN
 SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal = sucursalprincipal;
 IF x = 0 THEN --Si es una sucursal
  RETURN NEW;
 ELSE --SI es la principal
  SELECT COUNT(*) INTO x FROM cargos WHERE codcargo = NEW.codcargo AND codsucursal = NEW.codsucursal;
  IF x = 0 THEN --Si el cargo no existe
   INSERT INTO cargos (codcargo, codsucursal, descripcion, fecha, autorizado, responsable) VALUES (NEW.codcargo, NEW.codsucursal, NEW.descripcion, NEW.fecha, NEW.autorizado, NEW.responsable);
  ELSE --SI el cargo existe
   UPDATE cargos SET descripcion = NEW.descripcion, fecha = NEW.fecha, autorizado = NEW.autorizado, responsable = NEW.responsable WHERE codcargo = NEW.codcargo AND codsucursal = NEW.codsucursal;
  END IF; --Si el cargo no existe
  RETURN NULL;
 END IF; --Si es una sucursal
END;
$$;


ALTER FUNCTION public.funcionz_cargos() OWNER TO postgres;

--
-- TOC entry 504 (class 1255 OID 97541)
-- Name: funcionz_producto(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcionz_producto() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
DECLARE 
x bigint; 
ultimovalor int4; 
BEGIN 
 --Solo ejecuta este código si no es la sucursal
SELECT COUNT(*) INTO x FROM configuracion WHERE sucursal=sucursalprincipal;
If x = 0 THEN --AND NEW.z_estatus = false Then
select count(*) into x from producto where codproducto = NEW.codproducto; 
If x = 0 Then --Hay que insertar

INSERT INTO producto (numero,factorconversion,stockmaximo,stockminimo,codiva,codmarca,coddepartamento,codproducto,texto,foto,nombre,preciodetal1,preciomayor1,preciominimo1,float,preciodetal,preciomayor,preciominimo,costoanterior,costopromedio,costoactual,varchar,unidadalterna,unidadprincipal,estado,codbarra,referencia,modelo,codigo,fecha) 
VALUES(NEW.numero, NEW.factorconversion, NEW.stockmaximo, NEW.stockminimo, NEW.codiva, NEW.codmarca, NEW.coddepartamento, NEW.codproducto, NEW.texto, NEW.foto, NEW.nombre, NEW.preciodetal1, NEW.preciomayor1, NEW.preciominimo1, NEW.float, NEW.preciodetal, NEW.preciomayor, NEW.preciominimo, NEW.costoanterior, NEW.costopromedio, NEW.costoactual, NEW.varchar, NEW.unidadalterna, NEW.unidadprincipal, NEW.estado, NEW.codbarra, NEW.referencia, NEW.modelo, NEW.codigo, NEW.fecha); 

ELSE
 
 IF (NEW.numero is not null) THEN 
  UPDATE z_producto SET numero=NEW.numero WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.numero <> OLD.numero THEN 
 
 IF (NEW.factorconversion is not null) THEN 
  UPDATE producto SET factorconversion=NEW.factorconversion WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.factorconversion <> OLD.factorconversion THEN 
 
 IF (NEW.stockmaximo is not null) THEN 
  UPDATE producto SET stockmaximo=NEW.stockmaximo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.stockmaximo <> OLD.stockmaximo THEN 
 
 IF (NEW.stockminimo is not null) THEN 
  UPDATE producto SET stockminimo=NEW.stockminimo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.stockminimo <> OLD.stockminimo THEN 
 
 IF (NEW.codiva is not null) THEN 
  UPDATE producto SET codiva=NEW.codiva WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codiva <> OLD.codiva THEN 
 
 IF (NEW.codmarca is not null) THEN 
  UPDATE producto SET codmarca=NEW.codmarca WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codmarca <> OLD.codmarca THEN 
 
 IF (NEW.coddepartamento is not null) THEN 
  UPDATE producto SET coddepartamento=NEW.coddepartamento WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.coddepartamento <> OLD.coddepartamento THEN 
 
 IF (NEW.codproducto is not null) THEN 
  UPDATE producto SET codproducto=NEW.codproducto WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codproducto <> OLD.codproducto THEN 
 
 IF (NEW.texto is not null) THEN 
  UPDATE producto SET texto=NEW.texto WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.texto <> OLD.texto THEN 
 
 IF (NEW.foto is not null) THEN 
  UPDATE producto SET foto=NEW.foto WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.foto <> OLD.foto THEN 
 
 IF (NEW.nombre is not null) THEN 
  UPDATE producto SET nombre=NEW.nombre WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.nombre <> OLD.nombre THEN 
 
 IF (NEW.preciodetal1 is not null) THEN 
  UPDATE producto SET preciodetal1=NEW.preciodetal1 WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciodetal1 <> OLD.preciodetal1 THEN 
 
 IF (NEW.preciomayor1 is not null) THEN 
  UPDATE producto SET preciomayor1=NEW.preciomayor1 WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciomayor1 <> OLD.preciomayor1 THEN 
 
 IF (NEW.preciominimo1 is not null) THEN 
  UPDATE producto SET preciominimo1=NEW.preciominimo1 WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciominimo1 <> OLD.preciominimo1 THEN 
 
 IF (NEW.float is not null) THEN 
  UPDATE producto SET float=NEW.float WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.float <> OLD.float THEN 
 
 IF (NEW.preciodetal is not null) THEN 
  UPDATE producto SET preciodetal=NEW.preciodetal WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciodetal <> OLD.preciodetal THEN 
 
 IF (NEW.preciomayor is not null) THEN 
  UPDATE producto SET preciomayor=NEW.preciomayor WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciomayor <> OLD.preciomayor THEN 
 
 IF (NEW.preciominimo is not null) THEN 
  UPDATE producto SET preciominimo=NEW.preciominimo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.preciominimo <> OLD.preciominimo THEN 
 
 IF (NEW.costoanterior is not null) THEN 
  UPDATE producto SET costoanterior=NEW.costoanterior WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.costoanterior <> OLD.costoanterior THEN 
 
 IF (NEW.costopromedio is not null) THEN 
  UPDATE producto SET costopromedio=NEW.costopromedio WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.costopromedio <> OLD.costopromedio THEN 
 
 IF (NEW.costoactual is not null) THEN 
  UPDATE producto SET costoactual=NEW.costoactual WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.costoactual <> OLD.costoactual THEN 
 
 IF (NEW.varchar is not null) THEN 
  UPDATE producto SET varchar=NEW.varchar WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.varchar <> OLD.varchar THEN 
 
 IF (NEW.unidadalterna is not null) THEN 
  UPDATE producto SET unidadalterna=NEW.unidadalterna WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.unidadalterna <> OLD.unidadalterna THEN 
 
 IF (NEW.unidadprincipal is not null) THEN 
  UPDATE producto SET unidadprincipal=NEW.unidadprincipal WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.unidadprincipal <> OLD.unidadprincipal THEN 
 
 IF (NEW.estado is not null) THEN 
  UPDATE producto SET estado=NEW.estado WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.estado <> OLD.estado THEN 
 
 IF (NEW.codbarra is not null) THEN 
  UPDATE producto SET codbarra=NEW.codbarra WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codbarra <> OLD.codbarra THEN 
 
 IF (NEW.referencia is not null) THEN 
  UPDATE producto SET referencia=NEW.referencia WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.referencia <> OLD.referencia THEN 
 
 IF (NEW.modelo is not null) THEN 
  UPDATE producto SET modelo=NEW.modelo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.modelo <> OLD.modelo THEN 
 
 IF (NEW.codigo is not null) THEN 
  UPDATE producto SET codigo=NEW.codigo WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.codigo <> OLD.codigo THEN 
 
 IF (NEW.fecha is not null) THEN 
  UPDATE producto SET fecha=NEW.fecha WHERE codproducto=NEW.codproducto;
 END IF; --IF NEW.fecha <> OLD.fecha THEN 


END IF; --IF tg_op =0
RETURN NULL; 
ELSE --IF x = 0 de la sucursal
RETURN NEW;
END IF; --IF x = 0 de la sucursal
END; 
$$;


ALTER FUNCTION public.funcionz_producto() OWNER TO postgres;

--
-- TOC entry 505 (class 1255 OID 97542)
-- Name: funcionz_productosdepositos(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION funcionz_productosdepositos() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

DECLARE 

x bigint; 

ultimovalor int4; 

BEGIN 

 IF NEW.z_estatus = true THEN

  SELECT COUNT(*) INTO x FROM productosdepositos WHERE coddeposito = NEW.coddeposito AND codproducto = NEW.codproducto;

  IF x = 0 THEN --Si no esta ese producto en ese deposito

   INSERT INTO productosdepositos (coddeposito, codproducto, cantidad) VALUES (NEW.coddeposito, NEW.codproducto, NEW.cantidad);

  ELSE -- SI ya esta el producto con ese depósito

   UPDATE productosdepositos SET cantidad = NEW.cantidad WHERE coddeposito = NEW.coddeposito AND codproducto = NEW.codproducto;

  END IF;

  RETURN NULL;

 ELSE

  RETURN NEW;

 END IF;

END; $$;


ALTER FUNCTION public.funcionz_productosdepositos() OWNER TO postgres;

--
-- TOC entry 506 (class 1255 OID 97543)
-- Name: guardarexistencias(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION guardarexistencias() RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	registro RECORD;
	cont int4;
BEGIN
	cont = 0;
	FOR registro IN SELECT A.coddeposito, A.codproducto, A.cantidad, A.fecha, B.cantidad AS cantidad2, B.fecha AS fecha2 FROM ( SELECT *, now() AS fecha FROM productosdepositos ) A LEFT JOIN ( SELECT DISTINCT ON (coddeposito, codproducto) coddeposito, codproducto, cantidad, fecha FROM productosdepositos_h ORDER BY coddeposito, codproducto, fecha DESC ) B ON A.coddeposito = B.coddeposito AND A.codproducto = B.codproducto LOOP
		IF registro.fecha2 IS NULL THEN
			INSERT INTO productosdepositos_h (coddeposito, codproducto, cantidad, fecha) VALUES (registro.coddeposito, registro.codproducto, registro.cantidad, registro.fecha);
			cont = cont+1;
		ELSE 	
			IF registro.fecha::date > registro.fecha2::date AND registro.cantidad <> registro.cantidad2 THEN
				--UPDATE productosdepositos_h SET cantidad = registro.cantidad, fecha =  registro.fecha WHERE coddeposito = registro.coddeposito AND codproducto = registro.codproducto;
				INSERT INTO productosdepositos_h (coddeposito, codproducto, cantidad, fecha) VALUES (registro.coddeposito, registro.codproducto, registro.cantidad, registro.fecha);
				cont = cont+1;
			END IF;
		END IF;
	END LOOP;
	RETURN cont;
END;
$$;


ALTER FUNCTION public.guardarexistencias() OWNER TO postgres;

--
-- TOC entry 507 (class 1255 OID 97544)
-- Name: incrementarfactura(bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION incrementarfactura(pcodnumerofactura bigint, pcodsucursal bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	ultimovalor int;
	numeroinc int;
BEGIN
	SELECT numero INTO ultimovalor FROM numerofactura WHERE codnumerofactura = pcodnumerofactura and codsucursal = pcodsucursal;
	IF FOUND THEN
		numeroinc = ultimovalor+1;
		update numerofactura set numero = numeroinc where codnumerofactura = pcodnumerofactura and codsucursal = pcodsucursal;
	ELSE
		numeroinc = 0;
	END IF;
	RETURN numeroinc;
END;
$$;


ALTER FUNCTION public.incrementarfactura(pcodnumerofactura bigint, pcodsucursal bigint) OWNER TO postgres;

--
-- TOC entry 508 (class 1255 OID 97545)
-- Name: incrementarnumerodocumento(text, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION incrementarnumerodocumento(ptipodocumento text, pcodsucursal bigint) RETURNS integer
    LANGUAGE plpgsql
    AS $$DECLARE
	ultimovalor int;
	numeroinc int;
BEGIN
	SELECT numero INTO ultimovalor FROM numerodocumento WHERE tipodocumento = ptipodocumento and codsucursal = pcodsucursal order by numero desc;
	IF FOUND THEN
		numeroinc = ultimovalor+1;
		update numerodocumento set numero = numeroinc where tipodocumento = ptipodocumento and codsucursal = pcodsucursal;
	ELSE
		numeroinc = 0;
	END IF;
	RETURN numeroinc;
END;
$$;


ALTER FUNCTION public.incrementarnumerodocumento(ptipodocumento text, pcodsucursal bigint) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 170 (class 1259 OID 97546)
-- Name: aaaa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE aaaa (
    count bigint
);


ALTER TABLE public.aaaa OWNER TO postgres;

--
-- TOC entry 171 (class 1259 OID 97549)
-- Name: acceso; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE acceso (
    codusuario integer NOT NULL,
    fecha date NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    accion character varying(120) NOT NULL,
    usuarioclave character varying(50),
    estacion character varying(20) NOT NULL
);


ALTER TABLE public.acceso OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 97553)
-- Name: banco_coddeposito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE banco_coddeposito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.banco_coddeposito_seq OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 97555)
-- Name: bancodeposito; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bancodeposito (
    coddepbanco integer DEFAULT nextval('banco_coddeposito_seq'::regclass) NOT NULL,
    concepto character varying(120) NOT NULL,
    fechadeposito date,
    fecha date,
    numero character varying(10),
    nrodocumento integer,
    monto real NOT NULL,
    codbanco character varying(5) NOT NULL,
    codcuenta character varying(30) NOT NULL
);


ALTER TABLE public.bancodeposito OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 97559)
-- Name: bancos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE bancos (
    codbanco integer NOT NULL,
    nombre text,
    abreviatura text,
    imagen text,
    estatus boolean
);


ALTER TABLE public.bancos OWNER TO postgres;

--
-- TOC entry 175 (class 1259 OID 97565)
-- Name: bancos_codbanco_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bancos_codbanco_seq
    START WITH 22
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bancos_codbanco_seq OWNER TO postgres;

--
-- TOC entry 4328 (class 0 OID 0)
-- Dependencies: 175
-- Name: bancos_codbanco_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bancos_codbanco_seq OWNED BY bancos.codbanco;


--
-- TOC entry 176 (class 1259 OID 97567)
-- Name: cambios_codcambio_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cambios_codcambio_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.cambios_codcambio_seq OWNER TO postgres;

--
-- TOC entry 177 (class 1259 OID 97569)
-- Name: cargos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cargos (
    codcargo integer NOT NULL,
    codsucursal integer NOT NULL,
    descripcion text NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    autorizado character varying(50) NOT NULL,
    responsable character varying(50) NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.cargos OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 97578)
-- Name: cargos_codcargo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cargos_codcargo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cargos_codcargo_seq OWNER TO postgres;

--
-- TOC entry 179 (class 1259 OID 97580)
-- Name: ccabono_codccabono_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ccabono_codccabono_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.ccabono_codccabono_seq OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 97582)
-- Name: ccabono; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ccabono (
    codccabono integer DEFAULT nextval('ccabono_codccabono_seq'::regclass) NOT NULL,
    nrodocumento character varying(15),
    concepto text NOT NULL,
    fecha date NOT NULL
);


ALTER TABLE public.ccabono OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 97589)
-- Name: ccabonopago; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ccabonopago (
    codccabono integer NOT NULL,
    tipopago character varying(20) NOT NULL,
    monto real NOT NULL,
    banco character varying(50),
    numero character varying(20),
    punto character varying(50),
    cuenta character varying(50),
    fecharetencion date,
    mesretencion character varying(20),
    anioretencion character varying(4)
);


ALTER TABLE public.ccabonopago OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 97592)
-- Name: ccfactura_codccfactura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ccfactura_codccfactura_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.ccfactura_codccfactura_seq OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 97594)
-- Name: ccfactura; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ccfactura (
    codccfactura integer DEFAULT nextval('ccfactura_codccfactura_seq'::regclass) NOT NULL,
    nrodocumento character varying(15) NOT NULL,
    concepto text NOT NULL,
    fecha date NOT NULL,
    codcliente integer NOT NULL,
    codclientesucursal integer NOT NULL,
    fechavencimiento date NOT NULL,
    monto real NOT NULL,
    codsucursal integer NOT NULL,
    baseimp1 real NOT NULL,
    baseimp2 real NOT NULL,
    baseimp3 real NOT NULL,
    ivaimp1 real NOT NULL,
    ivaimp2 real NOT NULL,
    ivaimp3 real NOT NULL,
    porimp1 real NOT NULL,
    porimp2 real NOT NULL,
    porimp3 real NOT NULL,
    montoexento real NOT NULL,
    codvendedor integer NOT NULL
);


ALTER TABLE public.ccfactura OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 97601)
-- Name: ccnotacredito_codccnotacredito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ccnotacredito_codccnotacredito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.ccnotacredito_codccnotacredito_seq OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 97603)
-- Name: ccnotacredito; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ccnotacredito (
    codccnotacredito integer DEFAULT nextval('ccnotacredito_codccnotacredito_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    concepto text NOT NULL,
    nrodocumento character varying(15),
    codcliente integer,
    codclientesucursal integer,
    fecha date NOT NULL,
    monto real,
    saldo real DEFAULT (0)::real,
    facturaafecta character varying(15)
);


ALTER TABLE public.ccnotacredito OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 97611)
-- Name: ccnotacreditocompra_codccnotacredito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ccnotacreditocompra_codccnotacredito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.ccnotacreditocompra_codccnotacredito_seq OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 97613)
-- Name: ccnotadebito; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ccnotadebito (
    codccnotadebito integer NOT NULL,
    codsucursal integer NOT NULL,
    nrodocumento character varying(15) NOT NULL,
    concepto text NOT NULL,
    fecha date NOT NULL,
    codcliente integer NOT NULL,
    codclientesucursal integer NOT NULL,
    fechavencimiento date NOT NULL,
    monto real NOT NULL
);


ALTER TABLE public.ccnotadebito OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 97619)
-- Name: ccnotadebito_codccnotadebito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ccnotadebito_codccnotadebito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ccnotadebito_codccnotadebito_seq OWNER TO postgres;

--
-- TOC entry 4329 (class 0 OID 0)
-- Dependencies: 188
-- Name: ccnotadebito_codccnotadebito_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ccnotadebito_codccnotadebito_seq OWNED BY ccnotadebito.codccnotadebito;


--
-- TOC entry 189 (class 1259 OID 97621)
-- Name: ccpagadas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ccpagadas (
    codmovimientocc integer NOT NULL,
    tipomovimientocc character varying(20) NOT NULL,
    codmovimientopago integer NOT NULL,
    tipomovimientopago character varying(20) NOT NULL,
    monto real NOT NULL,
    iva real,
    codsucursalcc integer NOT NULL,
    codsucursalpago integer,
    fecha date NOT NULL
);


ALTER TABLE public.ccpagadas OWNER TO postgres;

--
-- TOC entry 4330 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN ccpagadas.tipomovimientocc; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ccpagadas.tipomovimientocc IS 'VENTAS, ND, FA, NC';


--
-- TOC entry 4331 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN ccpagadas.tipomovimientopago; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ccpagadas.tipomovimientopago IS 'ND, PG';


--
-- TOC entry 4332 (class 0 OID 0)
-- Dependencies: 189
-- Name: COLUMN ccpagadas.codsucursalcc; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ccpagadas.codsucursalcc IS '-- codsucursal del tipomovimiento, porque ventas tiene clave primaria: codventa y codsucursal';


--
-- TOC entry 190 (class 1259 OID 97624)
-- Name: ccpagadasiva; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ccpagadasiva (
    codmovimientocc integer NOT NULL,
    tipomovimientocc character varying(20) NOT NULL,
    codmovimientopago integer NOT NULL,
    tipomovimientopago character varying(20) NOT NULL,
    poriva real NOT NULL,
    baseiva real NOT NULL,
    codsucursalcc integer NOT NULL,
    codsucursalpago integer NOT NULL
);


ALTER TABLE public.ccpagadasiva OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 97627)
-- Name: cheque; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cheque (
    codcheque integer NOT NULL,
    nrocuenta text NOT NULL,
    nrotalon text,
    nrocheque text NOT NULL,
    codbeneficiario integer NOT NULL,
    monto real NOT NULL,
    femision date NOT NULL,
    fcobro date,
    concepto character varying(120) NOT NULL,
    estatus character varying(1) DEFAULT 'A'::character varying
);


ALTER TABLE public.cheque OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 97634)
-- Name: choferes_codchofer_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE choferes_codchofer_seq
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.choferes_codchofer_seq OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 97636)
-- Name: choferes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE choferes (
    codchofer integer DEFAULT nextval('choferes_codchofer_seq'::regclass) NOT NULL,
    nombre character varying(70) NOT NULL,
    vehiculoasignado character varying(10) NOT NULL,
    placas character varying(10),
    fecha date NOT NULL
);


ALTER TABLE public.choferes OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 97640)
-- Name: clientes_codcliente_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE clientes_codcliente_seq
    START WITH 4978
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.clientes_codcliente_seq OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 97642)
-- Name: clientes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE clientes (
    codcliente integer DEFAULT nextval('clientes_codcliente_seq'::regclass) NOT NULL,
    descripcion text NOT NULL,
    rif character varying(15) NOT NULL,
    clase character varying(10),
    representante text,
    direccion text NOT NULL,
    direccion2 text,
    telefonos character varying(30),
    email text,
    numerodefax character varying(20),
    zona character varying(2),
    codvendedor integer,
    tipodeprecio character varying(1) DEFAULT '2'::character varying NOT NULL,
    limitecredito real DEFAULT 0 NOT NULL,
    diasdecredito integer DEFAULT 0 NOT NULL,
    diastolerancia integer,
    fechadeinicio date DEFAULT now() NOT NULL,
    observaciones text,
    tipodecliente character varying(1) DEFAULT '0'::character varying NOT NULL,
    interesdemora character varying(1),
    convenioprecio character varying(2) DEFAULT 0,
    estatus boolean DEFAULT true NOT NULL,
    porcdescuento real,
    credito boolean DEFAULT false NOT NULL,
    codsucursal integer NOT NULL,
    contribuyentepatente boolean DEFAULT false NOT NULL,
    porcentajepatente real,
    modificado integer DEFAULT 0 NOT NULL,
    validado boolean DEFAULT false NOT NULL,
    diacobro character varying(9),
    diaventa character varying(9),
    fechaultimaventa date,
    nuevo boolean DEFAULT true NOT NULL,
    actualizado1 character varying(1) DEFAULT 0 NOT NULL,
    actualizado2 character varying(1) DEFAULT 0 NOT NULL
);


ALTER TABLE public.clientes OWNER TO postgres;

--
-- TOC entry 4333 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN clientes.codvendedor; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.codvendedor IS '''El codvendedor resulta ser el codusuario de la tabla usuario, como la clave primaria del usuario es codusuario y codsucursal, se usa el codsucursal del cliente, pues la cliente y el usuario se supone que es de la misma sucursal'';';


--
-- TOC entry 4334 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN clientes.tipodecliente; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.tipodecliente IS '0 Si es ordinario (Que paga el IVA), 1 si es especial (Que retiene el 75% del IVA)';


--
-- TOC entry 4335 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN clientes.validado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.validado IS 'True si ha sido validado por la página del SENIAT para RIF o CNE para cédulas de identidad';


--
-- TOC entry 196 (class 1259 OID 97663)
-- Name: codigosreportes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE codigosreportes (
    codigo character varying(4) NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.codigosreportes OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 97669)
-- Name: comision_niveldeprecios_vendedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comision_niveldeprecios_vendedor (
    codvendedor integer NOT NULL,
    codsucursalvendedor integer NOT NULL,
    porcpreciodebajodelminimo real NOT NULL,
    porcpreciominimo real NOT NULL,
    porcpreciomayor real NOT NULL,
    porcpreciodetal real NOT NULL,
    porccobrodebajodelminimo real NOT NULL,
    porccobropreciominimo real NOT NULL,
    porccobropreciomayor real NOT NULL,
    porccobropreciodetal real NOT NULL,
    aplicamontobruto boolean DEFAULT true
);


ALTER TABLE public.comision_niveldeprecios_vendedor OWNER TO postgres;

--
-- TOC entry 4336 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN comision_niveldeprecios_vendedor.aplicamontobruto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_niveldeprecios_vendedor.aplicamontobruto IS 'Las Comisiones se pueden aplicar sobre el monto bruto o sobre la base imponible de la Factura.';


--
-- TOC entry 198 (class 1259 OID 97673)
-- Name: comision_tabladecobros_vendedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comision_tabladecobros_vendedor (
    codvendedor integer NOT NULL,
    codsucursalvendedor integer NOT NULL,
    porc real NOT NULL,
    desde real NOT NULL,
    hasta real NOT NULL,
    aplicamontobruto boolean DEFAULT true
);


ALTER TABLE public.comision_tabladecobros_vendedor OWNER TO postgres;

--
-- TOC entry 4337 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN comision_tabladecobros_vendedor.desde; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladecobros_vendedor.desde IS 'Tipo Moneda';


--
-- TOC entry 4338 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN comision_tabladecobros_vendedor.hasta; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladecobros_vendedor.hasta IS 'Tipo Moneda';


--
-- TOC entry 4339 (class 0 OID 0)
-- Dependencies: 198
-- Name: COLUMN comision_tabladecobros_vendedor.aplicamontobruto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladecobros_vendedor.aplicamontobruto IS 'Las Comisiones se pueden aplicar sobre el monto bruto o sobre la base imponible de la Factura.';


--
-- TOC entry 199 (class 1259 OID 97677)
-- Name: comision_tabladeutilidad_vendedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comision_tabladeutilidad_vendedor (
    codvendedor integer NOT NULL,
    codsucursalvendedor integer NOT NULL,
    porc real NOT NULL,
    desde real NOT NULL,
    hasta real NOT NULL,
    aplicamontobruto boolean DEFAULT true
);


ALTER TABLE public.comision_tabladeutilidad_vendedor OWNER TO postgres;

--
-- TOC entry 4340 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN comision_tabladeutilidad_vendedor.desde; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladeutilidad_vendedor.desde IS 'Tipo Moneda';


--
-- TOC entry 4341 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN comision_tabladeutilidad_vendedor.hasta; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladeutilidad_vendedor.hasta IS 'Tipo Moneda';


--
-- TOC entry 4342 (class 0 OID 0)
-- Dependencies: 199
-- Name: COLUMN comision_tabladeutilidad_vendedor.aplicamontobruto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladeutilidad_vendedor.aplicamontobruto IS 'Las Comisiones se pueden aplicar sobre el monto bruto o sobre la base imponible de la Factura.';


--
-- TOC entry 200 (class 1259 OID 97681)
-- Name: comision_tabladeventas_vendedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comision_tabladeventas_vendedor (
    codvendedor integer NOT NULL,
    codsucursalvendedor integer NOT NULL,
    porc real NOT NULL,
    desde real NOT NULL,
    hasta real NOT NULL,
    aplicamontobruto boolean DEFAULT true
);


ALTER TABLE public.comision_tabladeventas_vendedor OWNER TO postgres;

--
-- TOC entry 4343 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN comision_tabladeventas_vendedor.desde; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladeventas_vendedor.desde IS 'Tipo Moneda';


--
-- TOC entry 4344 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN comision_tabladeventas_vendedor.hasta; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladeventas_vendedor.hasta IS 'Tipo Moneda';


--
-- TOC entry 4345 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN comision_tabladeventas_vendedor.aplicamontobruto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladeventas_vendedor.aplicamontobruto IS 'Las Comisiones se pueden aplicar sobre el monto bruto o sobre la bas e imponible de la Factura.';


--
-- TOC entry 201 (class 1259 OID 97685)
-- Name: comision_tabladiatoleranciaconcobros_vendedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comision_tabladiatoleranciaconcobros_vendedor (
    codvendedor integer NOT NULL,
    codsucursalvendedor integer NOT NULL,
    porc real NOT NULL,
    desdemonto real NOT NULL,
    hastamonto real NOT NULL,
    desdedia real NOT NULL,
    hastadia real NOT NULL,
    aplicamontobruto boolean DEFAULT true
);


ALTER TABLE public.comision_tabladiatoleranciaconcobros_vendedor OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 97689)
-- Name: comision_tabladiatoleranciaparacobros_vendedor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comision_tabladiatoleranciaparacobros_vendedor (
    codvendedor integer NOT NULL,
    codsucursalvendedor integer NOT NULL,
    porc real NOT NULL,
    desde real NOT NULL,
    hasta real NOT NULL,
    aplicamontobruto boolean DEFAULT true
);


ALTER TABLE public.comision_tabladiatoleranciaparacobros_vendedor OWNER TO postgres;

--
-- TOC entry 4346 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN comision_tabladiatoleranciaparacobros_vendedor.desde; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladiatoleranciaparacobros_vendedor.desde IS 'Tipo Fecha';


--
-- TOC entry 4347 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN comision_tabladiatoleranciaparacobros_vendedor.hasta; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladiatoleranciaparacobros_vendedor.hasta IS 'Tipo Fecha';


--
-- TOC entry 4348 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN comision_tabladiatoleranciaparacobros_vendedor.aplicamontobruto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN comision_tabladiatoleranciaparacobros_vendedor.aplicamontobruto IS 'Las Comisiones se pueden aplicar sobre el monto bruto o sobre la base imponible de la Factura.';


--
-- TOC entry 203 (class 1259 OID 97693)
-- Name: compras_codcompra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE compras_codcompra_seq
    START WITH 18
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compras_codcompra_seq OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 97695)
-- Name: compras; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE compras (
    codcompra integer DEFAULT nextval('compras_codcompra_seq'::regclass) NOT NULL,
    codproveedor integer NOT NULL,
    coddeposito integer NOT NULL,
    nrodocumento character varying(15) NOT NULL,
    nrocontrol character varying(15),
    femision date NOT NULL,
    fregistro date DEFAULT ('now'::text)::date NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    descuento real,
    pordescuento real,
    aplicadescuento boolean,
    fentrega date NOT NULL,
    montobruto real,
    montoexento real,
    baseimp1 real,
    baseimp2 real,
    baseimp3 real,
    ivaimp1 real,
    ivaimp2 real,
    ivaimp3 real,
    porimp1 real,
    porimp2 real,
    porimp3 real,
    retencion real DEFAULT (0)::real,
    codsucursal integer DEFAULT 1 NOT NULL,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.compras OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 97704)
-- Name: comprasespera_codcompraespera_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE comprasespera_codcompraespera_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.comprasespera_codcompraespera_seq OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 97706)
-- Name: comprasespera; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comprasespera (
    codcompraespera integer DEFAULT nextval('comprasespera_codcompraespera_seq'::regclass) NOT NULL,
    codproveedor integer NOT NULL,
    coddeposito integer NOT NULL,
    nrodocumento character varying(15) NOT NULL,
    nrocontrol character varying(15),
    femision date NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    fentrega date NOT NULL
);


ALTER TABLE public.comprasespera OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 97711)
-- Name: comprasesperaproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comprasesperaproductos (
    codcompraespera integer NOT NULL,
    codproducto integer NOT NULL,
    costo real NOT NULL,
    unidad character varying(20) NOT NULL,
    iva real NOT NULL,
    cantidad real NOT NULL,
    factorconversion real NOT NULL,
    indice smallint NOT NULL
);


ALTER TABLE public.comprasesperaproductos OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 97714)
-- Name: compraspagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE compraspagos (
    codcompra integer NOT NULL,
    tipopago character varying(20) NOT NULL,
    monto real NOT NULL,
    codsucursal integer NOT NULL
);


ALTER TABLE public.compraspagos OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 97717)
-- Name: comprasproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE comprasproductos (
    codcompra integer NOT NULL,
    codproducto integer NOT NULL,
    costo real NOT NULL,
    unidad character varying(20) NOT NULL,
    iva real NOT NULL,
    cantidad real NOT NULL,
    factorconversion real NOT NULL,
    costodescuento real,
    indice smallint NOT NULL,
    costoanterior real
);


ALTER TABLE public.comprasproductos OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 97720)
-- Name: conceptos_codconcepto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE conceptos_codconcepto_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.conceptos_codconcepto_seq OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 97722)
-- Name: conceptos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE conceptos (
    codconcepto integer DEFAULT nextval('conceptos_codconcepto_seq'::regclass) NOT NULL,
    descripcion character varying(70) NOT NULL
);


ALTER TABLE public.conceptos OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 97726)
-- Name: conciliacioncd; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE conciliacioncd (
    codmovimiento integer NOT NULL,
    tipomovimiento character varying NOT NULL,
    descripcion character varying NOT NULL,
    fechaconciliacion date DEFAULT '2013-07-17'::date NOT NULL,
    codsucursal integer NOT NULL,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL,
    control character varying(50)
);


ALTER TABLE public.conciliacioncd OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 97734)
-- Name: configuracion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE configuracion (
    sucursal integer NOT NULL,
    sucursalprincipal integer,
    porcentajeretencion real,
    agenteretencion boolean DEFAULT true NOT NULL,
    nombreempresa text,
    direccion text,
    telefonos text,
    rif text,
    pais text,
    estado text,
    ciudad text,
    pfacturacredito integer,
    actprecios character varying(50),
    diascredito integer DEFAULT 15 NOT NULL,
    correo character varying,
    mesfiscal integer
);


ALTER TABLE public.configuracion OWNER TO postgres;

--
-- TOC entry 4349 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN configuracion.agenteretencion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN configuracion.agenteretencion IS 'True si la empresa es agente de retención, False lo contrario
';


--
-- TOC entry 214 (class 1259 OID 97742)
-- Name: configurartabla; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE configurartabla (
    tabla character varying(20) NOT NULL,
    campo character varying(20) NOT NULL,
    nombre character varying(20),
    indice integer,
    busqueda character varying(20),
    activo boolean,
    tipo character varying(20),
    longitud character varying(2) DEFAULT 0,
    modificable boolean DEFAULT true
);


ALTER TABLE public.configurartabla OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 97747)
-- Name: correo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE correo (
    pass character varying(50) NOT NULL,
    nombre character varying,
    tipo character varying(50)
);


ALTER TABLE public.correo OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 97753)
-- Name: cpabono; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cpabono (
    codcpabono integer NOT NULL,
    nrodocumento character varying(15),
    concepto text NOT NULL,
    fecha date NOT NULL
);


ALTER TABLE public.cpabono OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 97759)
-- Name: cpabono_codcpabono_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cpabono_codcpabono_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cpabono_codcpabono_seq OWNER TO postgres;

--
-- TOC entry 4350 (class 0 OID 0)
-- Dependencies: 217
-- Name: cpabono_codcpabono_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cpabono_codcpabono_seq OWNED BY cpabono.codcpabono;


--
-- TOC entry 218 (class 1259 OID 97761)
-- Name: cpabonopago; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cpabonopago (
    codcpabono integer NOT NULL,
    tipopago character varying(20) NOT NULL,
    monto real NOT NULL,
    banco character varying(50),
    numero character varying(20),
    cuenta character varying(50),
    fecharetencion date,
    fechacobro date,
    mesretencion character varying(20),
    anioretencion character varying(4)
);


ALTER TABLE public.cpabonopago OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 97764)
-- Name: cpfactura; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cpfactura (
    codcpfactura integer NOT NULL,
    nrodocumento character varying(15) NOT NULL,
    concepto text NOT NULL,
    fecha date NOT NULL,
    codproveedor integer NOT NULL,
    fechavencimiento date NOT NULL,
    monto real NOT NULL,
    nrocontrol character varying(20),
    codsucursal integer NOT NULL,
    baseimp1 real NOT NULL,
    baseimp2 real NOT NULL,
    baseimp3 real NOT NULL,
    ivaimp1 real NOT NULL,
    ivaimp2 real NOT NULL,
    ivaimp3 real NOT NULL,
    porimp1 real NOT NULL,
    porimp2 real NOT NULL,
    porimp3 real NOT NULL,
    montoexento real NOT NULL,
    afectalibro boolean DEFAULT false NOT NULL,
    gasto boolean DEFAULT false NOT NULL
);


ALTER TABLE public.cpfactura OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 97772)
-- Name: cpfactura_codcpfactura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cpfactura_codcpfactura_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cpfactura_codcpfactura_seq OWNER TO postgres;

--
-- TOC entry 4351 (class 0 OID 0)
-- Dependencies: 220
-- Name: cpfactura_codcpfactura_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cpfactura_codcpfactura_seq OWNED BY cpfactura.codcpfactura;


--
-- TOC entry 221 (class 1259 OID 97774)
-- Name: cpnotacredito_codcpnotacredito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cpnotacredito_codcpnotacredito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cpnotacredito_codcpnotacredito_seq OWNER TO postgres;

--
-- TOC entry 222 (class 1259 OID 97776)
-- Name: cpnotacredito; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cpnotacredito (
    codcpnotacredito integer DEFAULT nextval('cpnotacredito_codcpnotacredito_seq'::regclass) NOT NULL,
    nrodocumento character varying(15),
    concepto text NOT NULL,
    fecha date NOT NULL,
    codproveedor integer NOT NULL,
    fechavencimiento date NOT NULL,
    monto real NOT NULL,
    codsucursal integer NOT NULL,
    nrocontrol character varying(15),
    facturaafecta character varying(15) NOT NULL,
    fechapago date,
    saldo real DEFAULT (0)::real NOT NULL,
    afectatotal boolean DEFAULT true NOT NULL
);


ALTER TABLE public.cpnotacredito OWNER TO postgres;

--
-- TOC entry 223 (class 1259 OID 97785)
-- Name: cpnotadebito; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cpnotadebito (
    codcpnotadebito integer NOT NULL,
    nrodocumento character varying(15),
    concepto text NOT NULL,
    fecha date NOT NULL,
    nrocontrol character varying(15) NOT NULL,
    codproveedor integer NOT NULL,
    codsucursal integer NOT NULL,
    facturaafecta character varying(15) NOT NULL,
    monto real NOT NULL,
    fechapago date
);


ALTER TABLE public.cpnotadebito OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 97791)
-- Name: cpnotadebito_codcpnotadebito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cpnotadebito_codcpnotadebito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cpnotadebito_codcpnotadebito_seq OWNER TO postgres;

--
-- TOC entry 4352 (class 0 OID 0)
-- Dependencies: 224
-- Name: cpnotadebito_codcpnotadebito_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cpnotadebito_codcpnotadebito_seq OWNED BY cpnotadebito.codcpnotadebito;


--
-- TOC entry 225 (class 1259 OID 97793)
-- Name: cppagadas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cppagadas (
    codmovimientocp integer NOT NULL,
    tipomovimientocp character varying(20) NOT NULL,
    codmovimientopago integer NOT NULL,
    tipomovimientopago character varying(20) NOT NULL,
    monto real NOT NULL,
    iva real,
    codsucursalcp integer NOT NULL,
    fecha date NOT NULL,
    codsucursalpago integer
);


ALTER TABLE public.cppagadas OWNER TO postgres;

--
-- TOC entry 4353 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN cppagadas.tipomovimientocp; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cppagadas.tipomovimientocp IS 'COMPRAS, ND, FA, NC';


--
-- TOC entry 4354 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN cppagadas.tipomovimientopago; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cppagadas.tipomovimientopago IS 'ND, PG';


--
-- TOC entry 226 (class 1259 OID 97796)
-- Name: cppagadasiva; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cppagadasiva (
    codmovimientocp integer NOT NULL,
    tipomovimientocp character varying(20) NOT NULL,
    codmovimientopago integer NOT NULL,
    tipomovimientopago character varying(20) NOT NULL,
    poriva real NOT NULL,
    baseiva real NOT NULL,
    codsucursalcp integer NOT NULL,
    codsucursalpago integer NOT NULL
);


ALTER TABLE public.cppagadasiva OWNER TO postgres;

--
-- TOC entry 4355 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN cppagadasiva.tipomovimientocp; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cppagadasiva.tipomovimientocp IS 'COMPRAS, FA, NC
';


--
-- TOC entry 4356 (class 0 OID 0)
-- Dependencies: 226
-- Name: COLUMN cppagadasiva.tipomovimientopago; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cppagadasiva.tipomovimientopago IS 'ND, PG';


--
-- TOC entry 450 (class 1259 OID 133560)
-- Name: cuadrediarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cuadrediarios (
    id bigint NOT NULL,
    nro integer NOT NULL,
    fecha date DEFAULT '1900-01-01'::date,
    estatus integer DEFAULT 0 NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone
);


ALTER TABLE public.cuadrediarios OWNER TO postgres;

--
-- TOC entry 451 (class 1259 OID 133567)
-- Name: cuadrediarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cuadrediarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuadrediarios_id_seq OWNER TO postgres;

--
-- TOC entry 4357 (class 0 OID 0)
-- Dependencies: 451
-- Name: cuadrediarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cuadrediarios_id_seq OWNED BY cuadrediarios.id;


--
-- TOC entry 227 (class 1259 OID 97799)
-- Name: cuentasbancarias; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cuentasbancarias (
    codcuenta integer NOT NULL,
    codbanco integer,
    numerocuenta text,
    fechacreacion date,
    codtitular integer,
    punto boolean DEFAULT false
);


ALTER TABLE public.cuentasbancarias OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 97806)
-- Name: cuentasbancarias_codcuenta_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cuentasbancarias_codcuenta_seq
    START WITH 16
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuentasbancarias_codcuenta_seq OWNER TO postgres;

--
-- TOC entry 4358 (class 0 OID 0)
-- Dependencies: 228
-- Name: cuentasbancarias_codcuenta_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cuentasbancarias_codcuenta_seq OWNED BY cuentasbancarias.codcuenta;


--
-- TOC entry 229 (class 1259 OID 97808)
-- Name: cuentasporcobrar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cuentasporcobrar (
    codmovimiento integer NOT NULL,
    tipomovimiento character varying(20) NOT NULL,
    monto real NOT NULL,
    fvencimiento date NOT NULL,
    concepto text NOT NULL,
    codsucursal integer NOT NULL
);


ALTER TABLE public.cuentasporcobrar OWNER TO postgres;

--
-- TOC entry 4359 (class 0 OID 0)
-- Dependencies: 229
-- Name: COLUMN cuentasporcobrar.tipomovimiento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cuentasporcobrar.tipomovimiento IS 'VENTAS, ND, FA, NC';


--
-- TOC entry 230 (class 1259 OID 97814)
-- Name: cuentasporpagar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cuentasporpagar (
    codmovimiento integer NOT NULL,
    tipomovimiento character varying(20) NOT NULL,
    monto real NOT NULL,
    fvencimiento date NOT NULL,
    concepto text NOT NULL,
    iva real,
    codsucursal integer NOT NULL
);


ALTER TABLE public.cuentasporpagar OWNER TO postgres;

--
-- TOC entry 4360 (class 0 OID 0)
-- Dependencies: 230
-- Name: COLUMN cuentasporpagar.tipomovimiento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cuentasporpagar.tipomovimiento IS 'COMPRAS, ND, FAC, NC';


--
-- TOC entry 231 (class 1259 OID 97820)
-- Name: cuentasporpagarpagos_codpago_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cuentasporpagarpagos_codpago_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cuentasporpagarpagos_codpago_seq OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 97822)
-- Name: departamentos_coddepartamento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE departamentos_coddepartamento_seq
    START WITH 55
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.departamentos_coddepartamento_seq OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 97824)
-- Name: departamentos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE departamentos (
    coddepartamento integer DEFAULT nextval('departamentos_coddepartamento_seq'::regclass) NOT NULL,
    codigo character varying(2) NOT NULL,
    descripcion text NOT NULL,
    clase character varying(10),
    compuesto character varying(1),
    seriales character varying(1),
    comisiones character varying(1),
    estatus boolean DEFAULT true NOT NULL,
    despacho character varying(15)
);


ALTER TABLE public.departamentos OWNER TO postgres;

--
-- TOC entry 234 (class 1259 OID 97832)
-- Name: depositos_coddeposito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE depositos_coddeposito_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.depositos_coddeposito_seq OWNER TO postgres;

--
-- TOC entry 235 (class 1259 OID 97834)
-- Name: depositos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE depositos (
    coddeposito integer DEFAULT nextval('depositos_coddeposito_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    descripcion text NOT NULL,
    clase character varying(10),
    responsable text,
    estatus boolean DEFAULT true NOT NULL
);


ALTER TABLE public.depositos OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 97842)
-- Name: descargos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE descargos (
    coddescargo integer NOT NULL,
    codsucursal integer NOT NULL,
    descripcion text NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    autorizado character varying(50) NOT NULL,
    responsable character varying(50) NOT NULL,
    usointerno boolean NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.descargos OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 97851)
-- Name: descargos_coddescargo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE descargos_coddescargo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.descargos_coddescargo_seq OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 97853)
-- Name: despachos_coddespacho_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE despachos_coddespacho_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.despachos_coddespacho_seq OWNER TO postgres;

--
-- TOC entry 239 (class 1259 OID 97855)
-- Name: despachos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE despachos (
    coddespacho integer DEFAULT nextval('despachos_coddespacho_seq'::regclass) NOT NULL,
    nrodocumento character varying(20) NOT NULL,
    codchofer integer NOT NULL,
    fechaemision date NOT NULL,
    cliente character varying(70) NOT NULL,
    monto real NOT NULL,
    turno character varying(1) DEFAULT 'M'::character varying NOT NULL,
    tipo character varying(5) NOT NULL,
    fechadespacho date NOT NULL
);


ALTER TABLE public.despachos OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 97860)
-- Name: devolucionz_codevolucionz_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE devolucionz_codevolucionz_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.devolucionz_codevolucionz_seq OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 97862)
-- Name: devolucion_z; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devolucion_z (
    codevolucionz integer DEFAULT nextval('devolucionz_codevolucionz_seq'::regclass),
    codmovimientoz integer,
    factura integer,
    fecha date,
    base12 real,
    iva12 real,
    base8 real,
    iva8 real,
    base22 real,
    iva22 real,
    exento real,
    totaldevolucion real,
    memoria integer
);


ALTER TABLE public.devolucion_z OWNER TO postgres;

--
-- TOC entry 242 (class 1259 OID 97866)
-- Name: devolucioncompra_coddevolucioncompra_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE devolucioncompra_coddevolucioncompra_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.devolucioncompra_coddevolucioncompra_seq OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 97868)
-- Name: devolucioncompra; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devolucioncompra (
    coddevolucioncompra integer DEFAULT nextval('devolucioncompra_coddevolucioncompra_seq'::regclass) NOT NULL,
    codcompra integer NOT NULL,
    coddeposito integer NOT NULL,
    fdevolucion date DEFAULT ('now'::text)::date NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    codsucursal integer NOT NULL,
    devoluciontotal boolean NOT NULL,
    montobruto real NOT NULL,
    montoexento real NOT NULL,
    baseimp1 real NOT NULL,
    baseimp2 real NOT NULL,
    baseimp3 real NOT NULL,
    ivaimp1 real NOT NULL,
    ivaimp2 real NOT NULL,
    ivaimp3 real NOT NULL,
    porimp1 real NOT NULL,
    porimp2 real NOT NULL,
    porimp3 real NOT NULL,
    codusuario integer NOT NULL,
    retencion real DEFAULT (0)::real NOT NULL
);


ALTER TABLE public.devolucioncompra OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 97875)
-- Name: devolucioncomprapago; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devolucioncomprapago (
    coddevolucioncompra integer NOT NULL,
    codsucursal integer NOT NULL,
    tipopago character varying(10) NOT NULL,
    monto real NOT NULL
);


ALTER TABLE public.devolucioncomprapago OWNER TO postgres;

--
-- TOC entry 245 (class 1259 OID 97878)
-- Name: devolucioncompraproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devolucioncompraproductos (
    coddevolucioncompra integer NOT NULL,
    codproducto integer NOT NULL,
    unidad character varying(20),
    cantidad real NOT NULL,
    factorconversion real NOT NULL,
    costo real NOT NULL,
    iva real NOT NULL,
    indice smallint NOT NULL
);


ALTER TABLE public.devolucioncompraproductos OWNER TO postgres;

--
-- TOC entry 246 (class 1259 OID 97881)
-- Name: devolucionventa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devolucionventa (
    coddevolucion integer NOT NULL,
    codsucursal integer NOT NULL,
    codventa integer NOT NULL,
    codsucursalventa integer NOT NULL,
    fecha timestamp(6) without time zone DEFAULT now() NOT NULL,
    codusuario integer,
    codusuariosucursal integer,
    coddeposito integer,
    codccnotacredito integer,
    codccnotacreditosucursal integer,
    devoluciontotal boolean,
    montobruto real,
    montoexento real,
    baseimp1 real,
    baseimp2 real,
    baseimp3 real,
    ivaimp1 real,
    ivaimp2 real,
    ivaimp3 real,
    porimp1 real,
    porimp2 real,
    porimp3 real,
    retencion real DEFAULT 0,
    observacion text
);


ALTER TABLE public.devolucionventa OWNER TO postgres;

--
-- TOC entry 247 (class 1259 OID 97889)
-- Name: devolucionventa_coddevolucion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE devolucionventa_coddevolucion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.devolucionventa_coddevolucion_seq OWNER TO postgres;

--
-- TOC entry 4361 (class 0 OID 0)
-- Dependencies: 247
-- Name: devolucionventa_coddevolucion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE devolucionventa_coddevolucion_seq OWNED BY devolucionventa.coddevolucion;


--
-- TOC entry 248 (class 1259 OID 97891)
-- Name: devolucionventacreditopagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devolucionventacreditopagos (
    coddevolucionventa integer NOT NULL,
    codsucursal integer NOT NULL,
    tipopago character varying(10) NOT NULL,
    monto real NOT NULL,
    fecha timestamp(6) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.devolucionventacreditopagos OWNER TO postgres;

--
-- TOC entry 249 (class 1259 OID 97895)
-- Name: devolucionventapago; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devolucionventapago (
    coddevolucion integer NOT NULL,
    codsucursal integer NOT NULL,
    tipopago character varying(10) NOT NULL,
    monto real NOT NULL
);


ALTER TABLE public.devolucionventapago OWNER TO postgres;

--
-- TOC entry 250 (class 1259 OID 97898)
-- Name: devolucionventaproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE devolucionventaproductos (
    coddevolucion integer NOT NULL,
    codsucursal integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL,
    indice smallint NOT NULL,
    unidad character varying(20)
);


ALTER TABLE public.devolucionventaproductos OWNER TO postgres;

--
-- TOC entry 4362 (class 0 OID 0)
-- Dependencies: 250
-- Name: COLUMN devolucionventaproductos.unidad; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN devolucionventaproductos.unidad IS 'unidad en que se devolvió';


--
-- TOC entry 459 (class 1259 OID 133714)
-- Name: dinerodenominaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dinerodenominaciones (
    id bigint NOT NULL,
    denominacion text NOT NULL,
    valor double precision,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    fin text DEFAULT 'DINERO'::text NOT NULL
);


ALTER TABLE public.dinerodenominaciones OWNER TO postgres;

--
-- TOC entry 458 (class 1259 OID 133712)
-- Name: dinerodenominaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dinerodenominaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dinerodenominaciones_id_seq OWNER TO postgres;

--
-- TOC entry 4363 (class 0 OID 0)
-- Dependencies: 458
-- Name: dinerodenominaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dinerodenominaciones_id_seq OWNED BY dinerodenominaciones.id;


--
-- TOC entry 251 (class 1259 OID 97901)
-- Name: documentosretenciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documentosretenciones (
    codretencion integer NOT NULL,
    codmovimiento integer NOT NULL,
    tipomovimiento character varying(20) NOT NULL,
    montoretenido real,
    poriva real NOT NULL,
    baseiva real,
    ivaretenido real
);


ALTER TABLE public.documentosretenciones OWNER TO postgres;

--
-- TOC entry 4364 (class 0 OID 0)
-- Dependencies: 251
-- Name: COLUMN documentosretenciones.tipomovimiento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN documentosretenciones.tipomovimiento IS 'COMPRAS, ND, FAC, NC';


--
-- TOC entry 452 (class 1259 OID 133569)
-- Name: documentotipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documentotipo (
    id bigint NOT NULL,
    descorta text NOT NULL,
    descripcion text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    accion text
);


ALTER TABLE public.documentotipo OWNER TO postgres;

--
-- TOC entry 453 (class 1259 OID 133577)
-- Name: documentotipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE documentotipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documentotipo_id_seq OWNER TO postgres;

--
-- TOC entry 4365 (class 0 OID 0)
-- Dependencies: 453
-- Name: documentotipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE documentotipo_id_seq OWNED BY documentotipo.id;


--
-- TOC entry 252 (class 1259 OID 97904)
-- Name: estaciones_codestacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE estaciones_codestacion_seq
    START WITH 15
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estaciones_codestacion_seq OWNER TO postgres;

--
-- TOC entry 253 (class 1259 OID 97906)
-- Name: estaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE estaciones (
    codestacion integer DEFAULT nextval('estaciones_codestacion_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    nombre character varying(50) NOT NULL,
    descripcion text NOT NULL,
    codnumerofactura integer,
    coddeposito integer,
    tipodeprecioventa smallint DEFAULT 2 NOT NULL,
    estatus boolean DEFAULT true NOT NULL,
    puntoventa boolean DEFAULT true NOT NULL,
    ventacredito boolean DEFAULT false NOT NULL,
    venderexistencianegativa boolean DEFAULT true NOT NULL,
    tipopago character varying(1) DEFAULT 'D'::character varying NOT NULL
);


ALTER TABLE public.estaciones OWNER TO postgres;

--
-- TOC entry 4366 (class 0 OID 0)
-- Dependencies: 253
-- Name: COLUMN estaciones.tipodeprecioventa; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN estaciones.tipodeprecioventa IS '0 - Mínimo
1 - Mayor
2 - Detal';


--
-- TOC entry 4367 (class 0 OID 0)
-- Dependencies: 253
-- Name: COLUMN estaciones.tipopago; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN estaciones.tipopago IS 'D = Directo
S = Separado';


--
-- TOC entry 254 (class 1259 OID 97919)
-- Name: formatos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE formatos (
    codformato integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.formatos OWNER TO postgres;

--
-- TOC entry 255 (class 1259 OID 97925)
-- Name: formatos_codformato_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE formatos_codformato_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formatos_codformato_seq OWNER TO postgres;

--
-- TOC entry 4368 (class 0 OID 0)
-- Dependencies: 255
-- Name: formatos_codformato_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE formatos_codformato_seq OWNED BY formatos.codformato;


--
-- TOC entry 256 (class 1259 OID 97927)
-- Name: formatosestaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE formatosestaciones (
    codformato integer NOT NULL,
    codestacion integer NOT NULL,
    impresora text,
    automatico boolean DEFAULT false NOT NULL,
    copias smallint NOT NULL,
    codestacionsucursal integer NOT NULL,
    texto text,
    papelorientacion smallint DEFAULT 1,
    papelfuente smallint DEFAULT 1
);


ALTER TABLE public.formatosestaciones OWNER TO postgres;

--
-- TOC entry 4369 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN formatosestaciones.papelorientacion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN formatosestaciones.papelorientacion IS '1 : Horizontal
2 : Vertical
';


--
-- TOC entry 4370 (class 0 OID 0)
-- Dependencies: 256
-- Name: COLUMN formatosestaciones.papelfuente; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN formatosestaciones.papelfuente IS '1 : Default
2 : Cassette
';


--
-- TOC entry 257 (class 1259 OID 97936)
-- Name: formulario_codformulario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE formulario_codformulario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formulario_codformulario_seq OWNER TO postgres;

--
-- TOC entry 258 (class 1259 OID 97938)
-- Name: formularios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE formularios (
    codformulario integer NOT NULL,
    nombreformulario character varying(50) NOT NULL,
    descripcionformulario character varying(50) NOT NULL,
    nombremenu character varying(50) NOT NULL
);


ALTER TABLE public.formularios OWNER TO postgres;

--
-- TOC entry 259 (class 1259 OID 97941)
-- Name: funcionesformularios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE funcionesformularios (
    codfuncionformulario integer NOT NULL,
    componente character varying(50) NOT NULL,
    descripcioncomponente text NOT NULL,
    index character varying(2),
    codformulario integer
);


ALTER TABLE public.funcionesformularios OWNER TO postgres;

--
-- TOC entry 260 (class 1259 OID 97947)
-- Name: funcionesformularios_codfuncionformulario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE funcionesformularios_codfuncionformulario_seq
    START WITH 325
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.funcionesformularios_codfuncionformulario_seq OWNER TO postgres;

--
-- TOC entry 4371 (class 0 OID 0)
-- Dependencies: 260
-- Name: funcionesformularios_codfuncionformulario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE funcionesformularios_codfuncionformulario_seq OWNED BY funcionesformularios.codfuncionformulario;


--
-- TOC entry 261 (class 1259 OID 97949)
-- Name: z_cambioprecios_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE z_cambioprecios_seq
    START WITH 10
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.z_cambioprecios_seq OWNER TO postgres;

--
-- TOC entry 262 (class 1259 OID 97951)
-- Name: h_ajuste; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE h_ajuste (
    nrocambio integer DEFAULT nextval('z_cambioprecios_seq'::regclass) NOT NULL,
    codajuste integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codusuario integer NOT NULL,
    estatus character varying(2) NOT NULL,
    hora time without time zone NOT NULL
);


ALTER TABLE public.h_ajuste OWNER TO postgres;

--
-- TOC entry 263 (class 1259 OID 97956)
-- Name: h_ajuste_hproducto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE h_ajuste_hproducto (
    codajuste integer NOT NULL,
    codproducto integer NOT NULL,
    codsucursal integer NOT NULL,
    estatus character varying(2) NOT NULL
);


ALTER TABLE public.h_ajuste_hproducto OWNER TO postgres;

--
-- TOC entry 264 (class 1259 OID 97959)
-- Name: h_ajuste_sucursal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE h_ajuste_sucursal (
    codsucursal integer NOT NULL,
    estatus character varying(2) NOT NULL,
    codajuste integer NOT NULL
);


ALTER TABLE public.h_ajuste_sucursal OWNER TO postgres;

--
-- TOC entry 265 (class 1259 OID 97962)
-- Name: hcuentasxcobrar_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hcuentasxcobrar_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.hcuentasxcobrar_codigo_seq OWNER TO postgres;

--
-- TOC entry 266 (class 1259 OID 97964)
-- Name: h_cuentasporcobrar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE h_cuentasporcobrar (
    codmovimiento integer NOT NULL,
    tipomovimiento character varying(20) NOT NULL,
    monto real NOT NULL,
    fvencimiento date NOT NULL,
    concepto text NOT NULL,
    iva real,
    codsucursal integer NOT NULL,
    fechacobro date DEFAULT ('now'::text)::date,
    codigo integer DEFAULT nextval('hcuentasxcobrar_codigo_seq'::regclass) NOT NULL,
    codvendedor integer
);


ALTER TABLE public.h_cuentasporcobrar OWNER TO postgres;

--
-- TOC entry 267 (class 1259 OID 97972)
-- Name: hcuentasxpagar_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE hcuentasxpagar_codigo_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.hcuentasxpagar_codigo_seq OWNER TO postgres;

--
-- TOC entry 268 (class 1259 OID 97974)
-- Name: h_cuentasporpagar; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE h_cuentasporpagar (
    codmovimiento integer NOT NULL,
    tipomovimiento character varying(20) NOT NULL,
    monto real NOT NULL,
    fvencimiento date NOT NULL,
    fcancelacion date NOT NULL,
    concepto text NOT NULL,
    iva real,
    codsucursal integer NOT NULL,
    codigo integer DEFAULT nextval('hcuentasxpagar_codigo_seq'::regclass) NOT NULL
);


ALTER TABLE public.h_cuentasporpagar OWNER TO postgres;

--
-- TOC entry 4372 (class 0 OID 0)
-- Dependencies: 268
-- Name: COLUMN h_cuentasporpagar.tipomovimiento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN h_cuentasporpagar.tipomovimiento IS 'COMPRAS, ND, FAC, NC';


--
-- TOC entry 269 (class 1259 OID 97981)
-- Name: h_producto_codajuste_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE h_producto_codajuste_seq
    START WITH 14
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.h_producto_codajuste_seq OWNER TO postgres;

--
-- TOC entry 270 (class 1259 OID 97983)
-- Name: h_producto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE h_producto (
    codproducto integer NOT NULL,
    codigo character varying(8) NOT NULL,
    nombre text NOT NULL,
    coddepartamento integer,
    unidadprincipal character varying(20),
    unidadalterna character varying(20),
    factorconversion integer,
    costonuevo real,
    costoanterior real,
    preciominimonuevo real,
    preciomayornuevo real,
    preciodetalnuevo real,
    preciominimoanterior real,
    preciomayoranterior real,
    preciodetalanterior real,
    fecha date NOT NULL,
    codajuste integer DEFAULT nextval('h_producto_codajuste_seq'::regclass) NOT NULL,
    tipoajuste character varying(1),
    hora time(6) without time zone NOT NULL,
    codusuario integer
);


ALTER TABLE public.h_producto OWNER TO postgres;

--
-- TOC entry 271 (class 1259 OID 97990)
-- Name: inventario_codinventario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inventario_codinventario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inventario_codinventario_seq OWNER TO postgres;

--
-- TOC entry 272 (class 1259 OID 97992)
-- Name: inventario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE inventario (
    codinventario integer DEFAULT nextval('inventario_codinventario_seq'::regclass) NOT NULL,
    nroinventario integer NOT NULL,
    codsucursal integer NOT NULL,
    coddepartamento integer NOT NULL,
    fechainicio date NOT NULL,
    fechacierre date,
    estatus character varying(1) DEFAULT 'A'::character varying
);


ALTER TABLE public.inventario OWNER TO postgres;

--
-- TOC entry 273 (class 1259 OID 97997)
-- Name: inventariopedidos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE inventariopedidos (
    codpedido integer NOT NULL,
    coddpto integer NOT NULL,
    codsucursal integer NOT NULL,
    codproducto character varying NOT NULL,
    cantidad real NOT NULL,
    cantidadaprobada real,
    fechapedido date NOT NULL,
    fechaaprobacion date,
    diadpto character varying,
    codigo character varying,
    estatus character varying(1) DEFAULT 'A'::character varying,
    coddeposito integer NOT NULL,
    existppal real,
    existsuc real
);


ALTER TABLE public.inventariopedidos OWNER TO postgres;

--
-- TOC entry 274 (class 1259 OID 98004)
-- Name: inventariopedidos_codpedido_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE inventariopedidos_codpedido_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.inventariopedidos_codpedido_seq OWNER TO postgres;

--
-- TOC entry 275 (class 1259 OID 98006)
-- Name: inventarioproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE inventarioproductos (
    codinventario integer NOT NULL,
    codproducto character varying NOT NULL,
    descripcion character varying NOT NULL,
    existenciat real,
    existenciaf real,
    diferencia real,
    costoactual real
);


ALTER TABLE public.inventarioproductos OWNER TO postgres;

--
-- TOC entry 445 (class 1259 OID 131797)
-- Name: iva; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE iva (
    codiva bigint NOT NULL,
    porcentaje real NOT NULL,
    descripcion character varying(20) NOT NULL,
    estatus text,
    montocomparar real,
    simbolo text,
    tipocliente text
);


ALTER TABLE public.iva OWNER TO postgres;

--
-- TOC entry 444 (class 1259 OID 131795)
-- Name: iva_codiva_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE iva_codiva_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.iva_codiva_seq OWNER TO postgres;

--
-- TOC entry 4373 (class 0 OID 0)
-- Dependencies: 444
-- Name: iva_codiva_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE iva_codiva_seq OWNED BY iva.codiva;


--
-- TOC entry 276 (class 1259 OID 98017)
-- Name: maquinas_codmaquina_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE maquinas_codmaquina_seq
    START WITH 8
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.maquinas_codmaquina_seq OWNER TO postgres;

--
-- TOC entry 277 (class 1259 OID 98019)
-- Name: maquinas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE maquinas (
    codmaquina integer DEFAULT nextval('maquinas_codmaquina_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    marca character varying,
    modelo character varying,
    serial character varying,
    descripcion character varying,
    memoria integer,
    estatus boolean,
    correlativo bigint DEFAULT 0,
    inicial text DEFAULT 'Z'::text
);


ALTER TABLE public.maquinas OWNER TO postgres;

--
-- TOC entry 278 (class 1259 OID 98026)
-- Name: marca_codmarca_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE marca_codmarca_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.marca_codmarca_seq OWNER TO postgres;

--
-- TOC entry 279 (class 1259 OID 98028)
-- Name: marca; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE marca (
    codmarca integer DEFAULT nextval('marca_codmarca_seq'::regclass) NOT NULL,
    descripcion character varying(30) NOT NULL,
    estatus boolean DEFAULT true NOT NULL
);


ALTER TABLE public.marca OWNER TO postgres;

--
-- TOC entry 454 (class 1259 OID 133579)
-- Name: movbancarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE movbancarios (
    id bigint NOT NULL,
    banco_id integer,
    cuentasbancaria_id integer,
    codsucursal bigint DEFAULT 0 NOT NULL,
    nrodocumento character varying(30),
    nrocomprobante character varying(15),
    fecha date NOT NULL,
    cedbeneficiario character varying(15),
    nombeneficiario character(250),
    monto double precision,
    saldo double precision,
    observacion character varying(150),
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    conciliado character varying(2),
    documentotipo_id integer,
    chequeo boolean,
    fconciliacion date,
    impreanombrede character varying(250),
    leyenda1 character varying(50),
    leyenda2 character varying(50),
    docasociado character varying(30),
    montoref double precision DEFAULT 0,
    cuadrediario_id integer DEFAULT 0,
    tipodeposito text
);


ALTER TABLE public.movbancarios OWNER TO postgres;

--
-- TOC entry 455 (class 1259 OID 133590)
-- Name: movbancarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE movbancarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movbancarios_id_seq OWNER TO postgres;

--
-- TOC entry 4374 (class 0 OID 0)
-- Dependencies: 455
-- Name: movbancarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE movbancarios_id_seq OWNED BY movbancarios.id;


--
-- TOC entry 280 (class 1259 OID 98033)
-- Name: movimientoz_codmovimiento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE movimientoz_codmovimiento_seq
    START WITH 4
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movimientoz_codmovimiento_seq OWNER TO postgres;

--
-- TOC entry 281 (class 1259 OID 98035)
-- Name: movimiento_z; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE movimiento_z (
    codmovimiento integer DEFAULT nextval('movimientoz_codmovimiento_seq'::regclass) NOT NULL,
    ultimafactura character varying,
    fecha date,
    memoria integer,
    base12 real,
    iva12 real,
    base8 real,
    iva8 real,
    base22 real,
    iva22 real,
    excento real,
    totalventa real,
    codmaquina integer,
    estatus boolean
);


ALTER TABLE public.movimiento_z OWNER TO postgres;

--
-- TOC entry 282 (class 1259 OID 98042)
-- Name: movimientoivacc; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE movimientoivacc (
    codmovimiento integer NOT NULL,
    codsucursal integer NOT NULL,
    tipomovimiento character varying(20) NOT NULL,
    poriva real NOT NULL,
    baseiva real NOT NULL
);


ALTER TABLE public.movimientoivacc OWNER TO postgres;

--
-- TOC entry 283 (class 1259 OID 98045)
-- Name: movimientoivacp; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE movimientoivacp (
    codmovimiento integer NOT NULL,
    tipomovimiento character varying(20) NOT NULL,
    poriva real NOT NULL,
    baseiva real NOT NULL
);


ALTER TABLE public.movimientoivacp OWNER TO postgres;

--
-- TOC entry 284 (class 1259 OID 98048)
-- Name: nccompras_codnotacredito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE nccompras_codnotacredito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nccompras_codnotacredito_seq OWNER TO postgres;

--
-- TOC entry 285 (class 1259 OID 98050)
-- Name: ncventas_codnotacredito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ncventas_codnotacredito_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ncventas_codnotacredito_seq OWNER TO postgres;

--
-- TOC entry 286 (class 1259 OID 98052)
-- Name: numeronotaentrega_codnotaentrega_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE numeronotaentrega_codnotaentrega_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.numeronotaentrega_codnotaentrega_seq OWNER TO postgres;

--
-- TOC entry 287 (class 1259 OID 98054)
-- Name: nota_entrega; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nota_entrega (
    codnotaentrega integer DEFAULT nextval('numeronotaentrega_codnotaentrega_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codcliente integer NOT NULL,
    codvendedor integer NOT NULL,
    codclientesucursal integer NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    coddeposito integer,
    codusuarioconectado integer,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.nota_entrega OWNER TO postgres;

--
-- TOC entry 288 (class 1259 OID 98061)
-- Name: nota_entregaproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE nota_entregaproductos (
    codnotaentrega integer NOT NULL,
    codsucursal integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL,
    precio real NOT NULL,
    unidad character varying(20) NOT NULL,
    iva real NOT NULL,
    factorconversion integer NOT NULL,
    costopromedio real NOT NULL,
    indice smallint NOT NULL,
    unidad2 character varying(20),
    factorconversion2 real,
    tipoprecio character(1)
);


ALTER TABLE public.nota_entregaproductos OWNER TO postgres;

--
-- TOC entry 289 (class 1259 OID 98064)
-- Name: notacredito_compras; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notacredito_compras (
    codnotacredito integer DEFAULT nextval('nccompras_codnotacredito_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    facturaafecta character varying(20) NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codproveedor integer NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    codusuarioconectado integer,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL,
    montobruto real NOT NULL,
    montoexento real NOT NULL,
    baseimp1 real NOT NULL,
    baseimp2 real NOT NULL,
    baseimp3 real NOT NULL,
    ivaimp1 real NOT NULL,
    ivaimp2 real NOT NULL,
    ivaimp3 real NOT NULL,
    porimp1 real NOT NULL,
    porimp2 real NOT NULL,
    porimp3 real NOT NULL,
    retencion real,
    codvendedorcliente integer
);


ALTER TABLE public.notacredito_compras OWNER TO postgres;

--
-- TOC entry 290 (class 1259 OID 98071)
-- Name: notacredito_ventas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notacredito_ventas (
    codnotacredito integer DEFAULT nextval('ncventas_codnotacredito_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    facturaafecta character varying(20) NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codcliente integer NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    codusuarioconectado integer,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL,
    montobruto real NOT NULL,
    montoexento real NOT NULL,
    baseimp1 real NOT NULL,
    baseimp2 real NOT NULL,
    baseimp3 real NOT NULL,
    ivaimp1 real NOT NULL,
    ivaimp2 real NOT NULL,
    ivaimp3 real NOT NULL,
    porimp1 real NOT NULL,
    porimp2 real NOT NULL,
    porimp3 real NOT NULL,
    retencion real,
    codvendedorcliente integer
);


ALTER TABLE public.notacredito_ventas OWNER TO postgres;

--
-- TOC entry 291 (class 1259 OID 98078)
-- Name: notadebito_compras; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE notadebito_compras (
    codnotadebito integer NOT NULL,
    codsucursal integer NOT NULL,
    facturaafecta character varying(20) NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codproveedor integer NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    codusuarioconectado integer,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL,
    montobruto real NOT NULL,
    montoexento real NOT NULL,
    baseimp1 real NOT NULL,
    baseimp2 real NOT NULL,
    baseimp3 real NOT NULL,
    ivaimp1 real NOT NULL,
    ivaimp2 real NOT NULL,
    ivaimp3 real NOT NULL,
    porimp1 real NOT NULL,
    porimp2 real NOT NULL,
    porimp3 real NOT NULL,
    retencion real
);


ALTER TABLE public.notadebito_compras OWNER TO postgres;

--
-- TOC entry 292 (class 1259 OID 98084)
-- Name: numerodocumento; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE numerodocumento (
    codnumerodocumento integer NOT NULL,
    codsucursal integer NOT NULL,
    numero integer NOT NULL,
    tipodocumento text NOT NULL,
    descripcion text,
    longitud integer DEFAULT 8 NOT NULL
);


ALTER TABLE public.numerodocumento OWNER TO postgres;

--
-- TOC entry 293 (class 1259 OID 98091)
-- Name: numerodocumento_codnumerodocumento_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE numerodocumento_codnumerodocumento_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.numerodocumento_codnumerodocumento_seq OWNER TO postgres;

--
-- TOC entry 4375 (class 0 OID 0)
-- Dependencies: 293
-- Name: numerodocumento_codnumerodocumento_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE numerodocumento_codnumerodocumento_seq OWNED BY numerodocumento.codnumerodocumento;


--
-- TOC entry 294 (class 1259 OID 98093)
-- Name: numerofactura_codnumerofactura_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE numerofactura_codnumerofactura_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.numerofactura_codnumerofactura_seq OWNER TO postgres;

--
-- TOC entry 295 (class 1259 OID 98095)
-- Name: numerofactura; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE numerofactura (
    codnumerofactura integer DEFAULT nextval('numerofactura_codnumerofactura_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    numero integer NOT NULL,
    prefijo character varying(2) NOT NULL,
    descripcion text NOT NULL,
    longitud integer DEFAULT 8 NOT NULL
);


ALTER TABLE public.numerofactura OWNER TO postgres;

--
-- TOC entry 296 (class 1259 OID 98103)
-- Name: origendatos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE origendatos (
    codorigendatos integer NOT NULL,
    descripcion character varying(20) NOT NULL
);


ALTER TABLE public.origendatos OWNER TO postgres;

--
-- TOC entry 297 (class 1259 OID 98106)
-- Name: origendatos_codorigendatos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE origendatos_codorigendatos_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.origendatos_codorigendatos_seq OWNER TO postgres;

--
-- TOC entry 4376 (class 0 OID 0)
-- Dependencies: 297
-- Name: origendatos_codorigendatos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE origendatos_codorigendatos_seq OWNED BY origendatos.codorigendatos;


--
-- TOC entry 298 (class 1259 OID 98108)
-- Name: origendatosvariables; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE origendatosvariables (
    codvariable integer NOT NULL,
    codorigendatos integer NOT NULL,
    descripcion text NOT NULL
);


ALTER TABLE public.origendatosvariables OWNER TO postgres;

--
-- TOC entry 299 (class 1259 OID 98114)
-- Name: origendatosvariables_codvariable_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE origendatosvariables_codvariable_seq
    START WITH 23
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.origendatosvariables_codvariable_seq OWNER TO postgres;

--
-- TOC entry 4377 (class 0 OID 0)
-- Dependencies: 299
-- Name: origendatosvariables_codvariable_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE origendatosvariables_codvariable_seq OWNED BY origendatosvariables.codvariable;


--
-- TOC entry 300 (class 1259 OID 98116)
-- Name: pedido; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pedido (
    codpedido integer NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codcliente integer NOT NULL,
    codclientesucursal integer NOT NULL,
    coddeposito integer NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    codsucursal integer NOT NULL,
    fechavencimiento date,
    codusuarioconectado integer,
    codvendedor integer,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.pedido OWNER TO postgres;

--
-- TOC entry 301 (class 1259 OID 98122)
-- Name: pedido_codpedido_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pedido_codpedido_seq
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pedido_codpedido_seq OWNER TO postgres;

--
-- TOC entry 4378 (class 0 OID 0)
-- Dependencies: 301
-- Name: pedido_codpedido_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pedido_codpedido_seq OWNED BY pedido.codpedido;


--
-- TOC entry 302 (class 1259 OID 98124)
-- Name: pedidoproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pedidoproductos (
    codpedido integer NOT NULL,
    codsucursal integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL,
    precio real NOT NULL,
    unidad character varying(20) NOT NULL,
    iva real NOT NULL,
    factorconversion integer NOT NULL,
    costopromedio real NOT NULL,
    indice smallint NOT NULL,
    unidad2 character varying(20),
    factorconversion2 real,
    tipoprecio character(1)
);


ALTER TABLE public.pedidoproductos OWNER TO postgres;

--
-- TOC entry 303 (class 1259 OID 98127)
-- Name: planificaciondecobranzas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE planificaciondecobranzas (
    codplanificacion integer NOT NULL,
    codsucursal integer NOT NULL,
    codvendedor integer NOT NULL,
    fechacreacion date DEFAULT ('now'::text)::date NOT NULL,
    horacreacion time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    estatus character(1),
    fechahasta date
);


ALTER TABLE public.planificaciondecobranzas OWNER TO postgres;

--
-- TOC entry 4379 (class 0 OID 0)
-- Dependencies: 303
-- Name: COLUMN planificaciondecobranzas.estatus; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN planificaciondecobranzas.estatus IS 'A: Activa, F: Finalizada';


--
-- TOC entry 304 (class 1259 OID 98132)
-- Name: planificaciondecobranzas_codplanificacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE planificaciondecobranzas_codplanificacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.planificaciondecobranzas_codplanificacion_seq OWNER TO postgres;

--
-- TOC entry 305 (class 1259 OID 98134)
-- Name: planificaciondecobranzasdetalles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE planificaciondecobranzasdetalles (
    codplanificacion integer NOT NULL,
    codsucursal integer NOT NULL,
    codvendedor integer NOT NULL,
    codcliente integer NOT NULL,
    codsucursalcliente integer NOT NULL,
    documento character varying(50),
    codventa integer NOT NULL,
    fechavencimiento date,
    dias integer,
    saldo real,
    fechacobro date,
    estatus character(1)
);


ALTER TABLE public.planificaciondecobranzasdetalles OWNER TO postgres;

--
-- TOC entry 4380 (class 0 OID 0)
-- Dependencies: 305
-- Name: COLUMN planificaciondecobranzasdetalles.estatus; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN planificaciondecobranzasdetalles.estatus IS 'P: Pendiente, C: Cobrada';


--
-- TOC entry 306 (class 1259 OID 98137)
-- Name: planificaciondepagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE planificaciondepagos (
    codplanificacion integer NOT NULL,
    codsucursal integer NOT NULL,
    fechacreacion date DEFAULT ('now'::text)::date NOT NULL,
    horacreacion time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    estatus character(1),
    fechahasta date,
    procesadopor text
);


ALTER TABLE public.planificaciondepagos OWNER TO postgres;

--
-- TOC entry 307 (class 1259 OID 98145)
-- Name: planificaciondepagos_codplanificacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE planificaciondepagos_codplanificacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.planificaciondepagos_codplanificacion_seq OWNER TO postgres;

--
-- TOC entry 4381 (class 0 OID 0)
-- Dependencies: 307
-- Name: planificaciondepagos_codplanificacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE planificaciondepagos_codplanificacion_seq OWNED BY planificaciondepagos.codplanificacion;


--
-- TOC entry 308 (class 1259 OID 98147)
-- Name: planificaciondepagosdetalles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE planificaciondepagosdetalles (
    codplanificacion integer NOT NULL,
    codsucursal integer NOT NULL,
    codproveedor integer NOT NULL,
    codsucursalproveedor integer NOT NULL,
    documento character varying(50),
    codcompra integer NOT NULL,
    fechavencimiento date,
    dias integer,
    saldo real,
    fechacobro date,
    estatus character(1)
);


ALTER TABLE public.planificaciondepagosdetalles OWNER TO postgres;

--
-- TOC entry 4382 (class 0 OID 0)
-- Dependencies: 308
-- Name: COLUMN planificaciondepagosdetalles.estatus; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN planificaciondepagosdetalles.estatus IS 'P: Pendiente C: Cancelada';


--
-- TOC entry 309 (class 1259 OID 98150)
-- Name: planificaciondepagosdetalles_codplanificacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE planificaciondepagosdetalles_codplanificacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.planificaciondepagosdetalles_codplanificacion_seq OWNER TO postgres;

--
-- TOC entry 4383 (class 0 OID 0)
-- Dependencies: 309
-- Name: planificaciondepagosdetalles_codplanificacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE planificaciondepagosdetalles_codplanificacion_seq OWNED BY planificaciondepagosdetalles.codplanificacion;


--
-- TOC entry 310 (class 1259 OID 98152)
-- Name: presupuesto_codpresupuesto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE presupuesto_codpresupuesto_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.presupuesto_codpresupuesto_seq OWNER TO postgres;

--
-- TOC entry 311 (class 1259 OID 98154)
-- Name: presupuesto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE presupuesto (
    codpresupuesto integer DEFAULT nextval('presupuesto_codpresupuesto_seq'::regclass) NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codcliente integer NOT NULL,
    codclientesucursal integer NOT NULL,
    coddeposito integer NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    email character varying(100),
    codsucursal integer NOT NULL,
    condicionenviarcorreo boolean,
    fechavencimiento date,
    atencion character varying(100),
    observaciones character varying(150),
    codusuarioconectado integer,
    codvendedor integer,
    atentamente text,
    validopor text,
    despacho text,
    cheque text,
    condicion text,
    telefonosvend text,
    empresa character varying(1) DEFAULT 'S'::character varying NOT NULL
);


ALTER TABLE public.presupuesto OWNER TO postgres;

--
-- TOC entry 4384 (class 0 OID 0)
-- Dependencies: 311
-- Name: COLUMN presupuesto.email; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN presupuesto.email IS 'Maximo 3 correos...Separados por ";"';


--
-- TOC entry 4385 (class 0 OID 0)
-- Dependencies: 311
-- Name: COLUMN presupuesto.codsucursal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN presupuesto.codsucursal IS 'sucursalActivo';


--
-- TOC entry 312 (class 1259 OID 98164)
-- Name: presupuestoproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE presupuestoproductos (
    codpresupuesto integer NOT NULL,
    codsucursal integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL,
    precio real NOT NULL,
    unidad character varying(20) NOT NULL,
    iva real NOT NULL,
    factorconversion integer NOT NULL,
    costopromedio real NOT NULL,
    indice smallint NOT NULL,
    unidad2 character varying(20),
    factorconversion2 real,
    tipoprecio character(1)
);


ALTER TABLE public.presupuestoproductos OWNER TO postgres;

--
-- TOC entry 313 (class 1259 OID 98167)
-- Name: producto_codproducto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE producto_codproducto_seq
    START WITH 1134
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.producto_codproducto_seq OWNER TO postgres;

--
-- TOC entry 314 (class 1259 OID 98169)
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE producto (
    codproducto integer DEFAULT nextval('producto_codproducto_seq'::regclass) NOT NULL,
    codigo character varying(8) NOT NULL,
    nombre text NOT NULL,
    coddepartamento integer NOT NULL,
    codmarca integer NOT NULL,
    modelo character varying(20),
    referencia character varying(15),
    codbarra character varying(14),
    codiva integer NOT NULL,
    estado character varying(10) DEFAULT 'ACTIVO'::character varying NOT NULL,
    stockminimo integer,
    stockmaximo integer,
    foto text,
    unidadprincipal character varying(20) NOT NULL,
    unidadalterna character varying(20) NOT NULL,
    factorconversion integer NOT NULL,
    costoactual real NOT NULL,
    costopromedio real NOT NULL,
    costoanterior real NOT NULL,
    preciominimo real NOT NULL,
    preciomayor real NOT NULL,
    preciodetal real NOT NULL,
    numero integer,
    fecha date,
    "varchar" character varying(20),
    texto text,
    "float" real,
    preciominimo1 real,
    preciomayor1 real,
    preciodetal1 real,
    minimoventap real DEFAULT 0.01 NOT NULL,
    minimoventaa real DEFAULT 0.01 NOT NULL
);


ALTER TABLE public.producto OWNER TO postgres;

--
-- TOC entry 315 (class 1259 OID 98179)
-- Name: productoscargos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productoscargos (
    codcargo integer NOT NULL,
    codsucursal integer NOT NULL,
    coddeposito integer NOT NULL,
    codproducto integer NOT NULL,
    totalcosto real NOT NULL,
    cantidad real NOT NULL,
    unidad character varying(20) NOT NULL,
    factorconversion integer NOT NULL,
    costounitario real
);


ALTER TABLE public.productoscargos OWNER TO postgres;

--
-- TOC entry 316 (class 1259 OID 98182)
-- Name: productosdepositos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productosdepositos (
    coddeposito integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL
);


ALTER TABLE public.productosdepositos OWNER TO postgres;

--
-- TOC entry 317 (class 1259 OID 98185)
-- Name: productosdepositos_h; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productosdepositos_h (
    coddeposito integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL,
    fecha timestamp(6) without time zone DEFAULT now() NOT NULL,
    automatico boolean DEFAULT true NOT NULL
);


ALTER TABLE public.productosdepositos_h OWNER TO postgres;

--
-- TOC entry 318 (class 1259 OID 98190)
-- Name: productosdescargos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productosdescargos (
    coddescargo integer NOT NULL,
    codsucursal integer NOT NULL,
    coddeposito integer NOT NULL,
    codproducto integer NOT NULL,
    totalcosto real NOT NULL,
    cantidad real NOT NULL,
    unidad character varying(20) NOT NULL,
    factorconversion integer NOT NULL,
    costounitario real
);


ALTER TABLE public.productosdescargos OWNER TO postgres;

--
-- TOC entry 319 (class 1259 OID 98193)
-- Name: proveedores_codproveedor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE proveedores_codproveedor_seq
    START WITH 240
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proveedores_codproveedor_seq OWNER TO postgres;

--
-- TOC entry 320 (class 1259 OID 98195)
-- Name: proveedores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE proveedores (
    codproveedor integer DEFAULT nextval('proveedores_codproveedor_seq'::regclass) NOT NULL,
    descripcion text NOT NULL,
    rif character varying(15) NOT NULL,
    clase character varying(10),
    representante text,
    direccion text DEFAULT 'Ninguno'::text NOT NULL,
    direccion2 text,
    telefonos character varying(30) DEFAULT 'Ninguno'::character varying NOT NULL,
    email text,
    numerodefax character varying(20),
    diasdecredito integer DEFAULT 0 NOT NULL,
    limitedecredito real DEFAULT 0 NOT NULL,
    formadepago character varying(30) DEFAULT 'Crédito'::character varying NOT NULL,
    porcderetencion integer,
    fechadeinicio date DEFAULT now() NOT NULL,
    observaciones text,
    tipoproveedor character varying(15) DEFAULT 'Nacional'::character varying NOT NULL,
    estatus boolean DEFAULT true NOT NULL
);


ALTER TABLE public.proveedores OWNER TO postgres;

--
-- TOC entry 321 (class 1259 OID 98210)
-- Name: puntodeventa; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE puntodeventa (
    codpuntoventa bigint NOT NULL,
    serial text,
    nrodecuenta text NOT NULL,
    codbanco integer,
    modelo text,
    marca text
);


ALTER TABLE public.puntodeventa OWNER TO postgres;

--
-- TOC entry 322 (class 1259 OID 98216)
-- Name: puntodeventa_codpunto_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE puntodeventa_codpunto_seq
    START WITH 12
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.puntodeventa_codpunto_seq OWNER TO postgres;

--
-- TOC entry 323 (class 1259 OID 98218)
-- Name: puntodeventa_codpuntoventa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE puntodeventa_codpuntoventa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.puntodeventa_codpuntoventa_seq OWNER TO postgres;

--
-- TOC entry 4386 (class 0 OID 0)
-- Dependencies: 323
-- Name: puntodeventa_codpuntoventa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE puntodeventa_codpuntoventa_seq OWNED BY puntodeventa.codpuntoventa;


--
-- TOC entry 324 (class 1259 OID 98220)
-- Name: rep_despachos; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rep_despachos AS
 SELECT dp.nrodocumento AS nro,
    dp.codchofer,
    ch.nombre,
    ch.vehiculoasignado,
    ((((date_part('day'::text, dp.fechaemision) || '-'::text) || date_part('month'::text, dp.fechaemision)) || '-'::text) || date_part('year'::text, dp.fechaemision)) AS fecha,
    dp.cliente,
    dp.monto,
    ((((date_part('day'::text, dp.fechadespacho) || '-'::text) || date_part('month'::text, dp.fechadespacho)) || '-'::text) || date_part('year'::text, dp.fechadespacho)) AS fecha_despacho,
    dp.turno,
    dp.tipo,
    date_part('month'::text, dp.fechadespacho) AS mes_despacho,
    date_part('year'::text, dp.fechadespacho) AS ano_despacho,
    dp.fechaemision,
    dp.fechadespacho AS f_despacho
   FROM (despachos dp
     LEFT JOIN choferes ch ON ((ch.codchofer = dp.codchofer)))
  ORDER BY dp.codchofer, dp.fechaemision;


ALTER TABLE public.rep_despachos OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 98225)
-- Name: rep_inv_existencia; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rep_inv_existencia AS
 SELECT (p.codigo)::text AS codproducto,
    p.nombre,
    dep.codigo AS dpto,
    dep.descripcion AS descdpto,
    d.descripcion AS descripciondepto,
    (pd.cantidad)::text AS cantidad
   FROM (((productosdepositos pd
     JOIN producto p ON ((p.codproducto = pd.codproducto)))
     JOIN departamentos dep ON ((dep.coddepartamento = p.coddepartamento)))
     JOIN depositos d ON ((d.coddeposito = pd.coddeposito)));


ALTER TABLE public.rep_inv_existencia OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 98230)
-- Name: rep_inventariofisico; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rep_inventariofisico AS
 SELECT p.codigo,
    p.nombre,
    pd.cantidad,
    p.unidadprincipal,
    p.unidadalterna,
    p.factorconversion,
    d.codigo AS cod_departamento,
    d.descripcion AS departamento,
    pd.coddeposito AS cod_deposito,
    dp.descripcion AS depo_descripcion,
    p.costoactual,
    p.costopromedio
   FROM (((productosdepositos pd
     JOIN producto p ON ((p.codproducto = pd.codproducto)))
     JOIN departamentos d ON ((p.coddepartamento = d.coddepartamento)))
     JOIN depositos dp ON ((dp.coddeposito = pd.coddeposito)));


ALTER TABLE public.rep_inventariofisico OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 98235)
-- Name: resumendecomprascreditosindevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendecomprascreditosindevolucion AS
 SELECT c.codcompra,
    ((((c.femision)::text || ' '::text) || (c.hora)::text))::timestamp without time zone AS fecha,
    cp.totalcosto,
    cp.totalcostodescuento,
    cp.subtotaliva,
    cp.subtotalivadescuento,
    cp.total,
    cp.totaldescuento,
    pag.credito,
    c.descuento
   FROM (( SELECT compraspagos.codcompra,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CREDITO'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS credito
           FROM compraspagos
          GROUP BY compraspagos.codcompra) pag
     JOIN (( SELECT cp_1.codcompra,
            COALESCE(sum((cp_1.costo * cp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((cp_1.costodescuento * cp_1.cantidad)), (0)::real) AS totalcostodescuento,
            COALESCE(sum(((cp_1.costo * cp_1.cantidad) * ((1)::real + (cp_1.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((cp_1.costodescuento * cp_1.cantidad) * ((1)::real + (cp_1.iva / (100)::real)))), (0)::real) AS totaldescuento,
            COALESCE(sum(((cp_1.costo * cp_1.cantidad) * (cp_1.iva / (100)::real))), (0)::real) AS subtotaliva,
            COALESCE(sum(((cp_1.costodescuento * cp_1.cantidad) * (cp_1.iva / (100)::real))), (0)::real) AS subtotalivadescuento
           FROM comprasproductos cp_1
          GROUP BY cp_1.codcompra) cp
     JOIN compras c ON ((c.codcompra = cp.codcompra))) ON ((pag.codcompra = c.codcompra)));


ALTER TABLE public.resumendecomprascreditosindevolucion OWNER TO postgres;

--
-- TOC entry 328 (class 1259 OID 98240)
-- Name: resumendedevolucionescompracreditodiario; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendedevolucionescompracreditodiario AS
 SELECT d.coddevolucioncompra,
    d.codsucursal,
    d.codcompra,
    d.fdevolucion,
    d.totalcosto,
    d.total,
    d.subtotaliva,
    d.totalcostodescuento,
    d.totaldescuento,
    d.subtotalivadescuento,
    pag.credito
   FROM (( SELECT devolucioncomprapago.coddevolucioncompra,
            devolucioncomprapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'CREDITO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS credito
           FROM devolucioncomprapago
          GROUP BY devolucioncomprapago.coddevolucioncompra, devolucioncomprapago.codsucursal) pag
     JOIN ( SELECT d_1.coddevolucioncompra,
            d_1.codsucursal,
            d_1.codcompra,
            d_1.fdevolucion,
            COALESCE(sum((cp.costo * dp.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum(((cp.costo * dp.cantidad) * (cp.iva / (100)::real))), (0)::real) AS subtotaliva,
            COALESCE(sum(((cp.costo * dp.cantidad) * ((1)::real + (cp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum((cp.costodescuento * dp.cantidad)), (0)::real) AS totalcostodescuento,
            COALESCE(sum(((cp.costodescuento * dp.cantidad) * (cp.iva / (100)::real))), (0)::real) AS subtotalivadescuento,
            COALESCE(sum(((cp.costodescuento * dp.cantidad) * ((1)::real + (cp.iva / (100)::real)))), (0)::real) AS totaldescuento
           FROM (comprasproductos cp
             JOIN (devolucioncompra d_1
             JOIN devolucioncompraproductos dp ON ((dp.coddevolucioncompra = d_1.coddevolucioncompra))) ON (((d_1.codcompra = cp.codcompra) AND (cp.indice = dp.indice))))
          GROUP BY d_1.coddevolucioncompra, d_1.codsucursal, d_1.codcompra, d_1.fdevolucion) d ON (((pag.coddevolucioncompra = d.coddevolucioncompra) AND (d.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendedevolucionescompracreditodiario OWNER TO postgres;

--
-- TOC entry 329 (class 1259 OID 98245)
-- Name: ventasproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ventasproductos (
    codventa integer NOT NULL,
    codsucursal integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL,
    precio real NOT NULL,
    unidad character varying(20) NOT NULL,
    iva real NOT NULL,
    factorconversion integer NOT NULL,
    costopromedio real NOT NULL,
    indice smallint NOT NULL,
    unidad2 character varying(20),
    factorconversion2 real
);


ALTER TABLE public.ventasproductos OWNER TO postgres;

--
-- TOC entry 4387 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ventasproductos.costopromedio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ventasproductos.costopromedio IS 'El costo unitario del producto al momento de la venta, para saber las utilidades...';


--
-- TOC entry 4388 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ventasproductos.unidad2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ventasproductos.unidad2 IS 'Es la unidad distinta a la de la venta, si se vendio en unidad alterna, esta seria la unidad principal
';


--
-- TOC entry 4389 (class 0 OID 0)
-- Dependencies: 329
-- Name: COLUMN ventasproductos.factorconversion2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ventasproductos.factorconversion2 IS 'es el factor de conversion del producto en el momento de la venta, cuando se vende en unidad alterna, es igual a la columna factorconversion';


--
-- TOC entry 330 (class 1259 OID 98248)
-- Name: resumendedevolucionescreditodiario; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendedevolucionescreditodiario AS
 SELECT d.coddevolucion,
    d.codsucursal,
    d.codventa,
    d.codsucursalventa,
    d.fecha,
    d.totalcosto,
    d.subtotal,
    d.total,
    d.subtotaliva,
    pag.credito
   FROM (( SELECT devolucionventapago.coddevolucion,
            devolucionventapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CREDITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS credito
           FROM devolucionventapago
          GROUP BY devolucionventapago.coddevolucion, devolucionventapago.codsucursal) pag
     JOIN ( SELECT d_1.coddevolucion,
            d_1.codsucursal,
            d_1.codventa,
            d_1.codsucursalventa,
            d_1.fecha,
            COALESCE(sum((vp.costopromedio * dp.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp.precio * dp.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp.precio * dp.cantidad) * ((1)::real + (vp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp.precio * dp.cantidad) * (vp.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM (ventasproductos vp
             JOIN (devolucionventa d_1
             JOIN devolucionventaproductos dp ON (((dp.coddevolucion = d_1.coddevolucion) AND (d_1.codsucursal = dp.codsucursal)))) ON ((((d_1.codventa = vp.codventa) AND (d_1.codsucursalventa = vp.codsucursal)) AND (vp.indice = dp.indice))))
          GROUP BY d_1.coddevolucion, d_1.codsucursal, d_1.codventa, d_1.codsucursalventa, d_1.fecha) d ON (((pag.coddevolucion = d.coddevolucion) AND (d.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendedevolucionescreditodiario OWNER TO postgres;

--
-- TOC entry 331 (class 1259 OID 98253)
-- Name: resumendedevolucionesdiario; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendedevolucionesdiario AS
 SELECT d.coddevolucion,
    d.codsucursal,
    d.codventa,
    d.codsucursalventa,
    d.fecha,
    d.totalcosto,
    d.subtotal,
    d.total,
    d.subtotaliva,
    pag.credito,
    (((pag.efectivo + pag.cheque) + pag.transfer) + pag.debito) AS contado,
    pag.efectivo,
    pag.cheque,
    pag.transfer,
    pag.debito
   FROM (( SELECT devolucionventapago.coddevolucion,
            devolucionventapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CREDITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'EFECTIVO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CHEQUE'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TRANSFER'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TDEBITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS debito
           FROM devolucionventapago
          GROUP BY devolucionventapago.coddevolucion, devolucionventapago.codsucursal) pag
     JOIN ( SELECT d_1.coddevolucion,
            d_1.codsucursal,
            d_1.codventa,
            d_1.codsucursalventa,
            d_1.fecha,
            COALESCE(sum((vp.costopromedio * dp.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp.precio * dp.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp.precio * dp.cantidad) * ((1)::real + (vp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp.precio * dp.cantidad) * (vp.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM (ventasproductos vp
             JOIN (devolucionventa d_1
             JOIN devolucionventaproductos dp ON (((dp.coddevolucion = d_1.coddevolucion) AND (d_1.codsucursal = dp.codsucursal)))) ON ((((d_1.codventa = vp.codventa) AND (d_1.codsucursalventa = vp.codsucursal)) AND (vp.indice = dp.indice))))
          GROUP BY d_1.coddevolucion, d_1.codsucursal, d_1.codventa, d_1.codsucursalventa, d_1.fecha) d ON (((pag.coddevolucion = d.coddevolucion) AND (d.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendedevolucionesdiario OWNER TO postgres;

--
-- TOC entry 332 (class 1259 OID 98258)
-- Name: ventas_codventa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ventas_codventa_seq
    START WITH 662
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ventas_codventa_seq OWNER TO postgres;

--
-- TOC entry 333 (class 1259 OID 98260)
-- Name: ventas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ventas (
    codventa integer DEFAULT nextval('ventas_codventa_seq'::regclass) NOT NULL,
    codsucursal integer NOT NULL,
    numerofactura character varying(20) NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codcliente integer NOT NULL,
    codvendedor integer NOT NULL,
    codclientesucursal integer NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    coddeposito integer,
    codusuarioconectado integer,
    recibido real,
    estatus character varying(1) DEFAULT 'P'::character varying NOT NULL,
    montobruto double precision NOT NULL,
    montoexento real NOT NULL,
    baseimp1 double precision NOT NULL,
    baseimp2 double precision NOT NULL,
    baseimp3 double precision NOT NULL,
    ivaimp1 double precision NOT NULL,
    ivaimp2 double precision NOT NULL,
    ivaimp3 double precision NOT NULL,
    porimp1 double precision NOT NULL,
    porimp2 double precision NOT NULL,
    porimp3 double precision NOT NULL,
    retencion double precision,
    nrocomprobantefiscal integer,
    vf text DEFAULT 'NO'::text,
    codmaquina bigint DEFAULT 0,
    memoria bigint DEFAULT 0,
    estatusz text DEFAULT 'Abierto'::text,
    usuariow text DEFAULT 'admin'::text,
    correlativo bigint DEFAULT 0
);


ALTER TABLE public.ventas OWNER TO postgres;

--
-- TOC entry 4390 (class 0 OID 0)
-- Dependencies: 333
-- Name: COLUMN ventas.codvendedor; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ventas.codvendedor IS 'El codvendedor resulta ser el codusuario de la tabla usuario, como la clave primaria del usuario es codusuario y codsucursal, se usa el codsucursal de la venta, pues la venta y el usuario se supone que es de la misma sucursal';


--
-- TOC entry 334 (class 1259 OID 98267)
-- Name: ventaspagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ventaspagos (
    codventa integer NOT NULL,
    codsucursal integer NOT NULL,
    tipopago character varying(20) NOT NULL,
    monto real NOT NULL,
    banco character varying(50),
    nrocuenta character varying(50),
    numerotransferencia character varying(50),
    cuenta character varying(50),
    numerocheque character(50),
    montorecibido real,
    codpuntoventa integer
);


ALTER TABLE public.ventaspagos OWNER TO postgres;

--
-- TOC entry 4391 (class 0 OID 0)
-- Dependencies: 334
-- Name: COLUMN ventaspagos.tipopago; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN ventaspagos.tipopago IS 'CREDITO, EFECTIVO, TDEBITO, CHEQUE, TRANSFER, TCREDITO, TICKET';


--
-- TOC entry 335 (class 1259 OID 98270)
-- Name: resumendeventascreditosindevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventascreditosindevolucion AS
 SELECT v.codventa,
    v.codsucursal,
    ((((v.fecha)::text || ' '::text) || (v.hora)::text))::timestamp without time zone AS fecha,
    vp.totalcosto,
    vp.subtotal,
    vp.total,
    vp.subtotaliva,
    pag.credito
   FROM (( SELECT ventaspagos.codventa,
            ventaspagos.codsucursal,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS credito
           FROM ventaspagos
          GROUP BY ventaspagos.codventa, ventaspagos.codsucursal) pag
     JOIN (( SELECT vp_1.codventa,
            vp_1.codsucursal,
            COALESCE(sum((vp_1.costopromedio * vp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp_1.precio * vp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * ((1)::real + (vp_1.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * (vp_1.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM ventasproductos vp_1
          GROUP BY vp_1.codventa, vp_1.codsucursal) vp
     JOIN ventas v ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal)))) ON (((pag.codventa = v.codventa) AND (v.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendeventascreditosindevolucion OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 98275)
-- Name: resumendeventasdiarias; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventasdiarias AS
 SELECT a.codventa,
    a.codsucursal,
    a.codproducto,
    a.comprado,
    a.devuelto,
    a.cantidad,
    a.costopromedio,
    a.precio,
    a.iva,
    a.fecha,
    (a.cantidad * a.costopromedio) AS totalcosto,
    (a.cantidad * a.precio) AS subtotal,
    ((a.cantidad * a.precio) * ((1)::double precision + (a.iva / (100)::double precision))) AS total,
    ((a.cantidad * a.precio) * (a.iva / (100)::double precision)) AS subtotaliva
   FROM ( SELECT vp.codventa,
            vp.codsucursal,
            vp.codproducto,
            vp.cantidad AS comprado,
            COALESCE(b.cantidad, (0)::real) AS devuelto,
            (vp.cantidad - COALESCE(b.cantidad, (0)::real)) AS cantidad,
            vp.costopromedio,
            vp.precio,
            vp.iva,
            v.fecha
           FROM (ventas v
             JOIN (ventasproductos vp
             LEFT JOIN ( SELECT v_1.codventa,
                    v_1.codsucursal,
                    dp.indice,
                    sum(dp.cantidad) AS cantidad
                   FROM (ventas v_1
                     LEFT JOIN (devolucionventa d
                     JOIN devolucionventaproductos dp ON (((d.coddevolucion = dp.coddevolucion) AND (d.codsucursal = dp.codsucursal)))) ON ((((d.codventa = v_1.codventa) AND (d.codsucursalventa = v_1.codsucursal)) AND (v_1.fecha = (d.fecha)::date))))
                  GROUP BY v_1.codventa, v_1.codsucursal, dp.indice) b ON ((((vp.codventa = b.codventa) AND (vp.codsucursal = b.codsucursal)) AND (vp.indice = b.indice)))) ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal))))) a;


ALTER TABLE public.resumendeventasdiarias OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 98280)
-- Name: resumendeventasdiariasporproducto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventasdiariasporproducto AS
 SELECT a.codventa,
    a.codsucursal,
    a.codproducto,
    a.comprado,
    a.devuelto,
    a.cantidad,
    a.costopromedio,
    a.precio,
    a.iva,
    a.fecha,
    (a.cantidad * a.costopromedio) AS totalcosto,
    (a.cantidad * a.precio) AS subtotal,
    ((a.cantidad * a.precio) * ((1)::double precision + (a.iva / (100)::double precision))) AS total,
    ((a.cantidad * a.precio) * (a.iva / (100)::double precision)) AS subtotaliva
   FROM ( SELECT vp.codventa,
            vp.codsucursal,
            vp.codproducto,
            vp.cantidad AS comprado,
            COALESCE(a_1.cantidad, (0)::real) AS devuelto,
            (vp.cantidad - COALESCE(a_1.cantidad, (0)::real)) AS cantidad,
            vp.costopromedio,
            vp.precio,
            vp.iva,
            v.fecha
           FROM (ventas v
             JOIN (ventasproductos vp
             LEFT JOIN ( SELECT v_1.codventa,
                    v_1.codsucursal,
                    dp.indice,
                    sum(dp.cantidad) AS cantidad
                   FROM (ventas v_1
                     LEFT JOIN (devolucionventa d
                     JOIN devolucionventaproductos dp ON (((d.coddevolucion = dp.coddevolucion) AND (d.codsucursal = dp.codsucursal)))) ON ((((d.codventa = v_1.codventa) AND (d.codsucursalventa = v_1.codsucursal)) AND (v_1.fecha = (d.fecha)::date))))
                  GROUP BY v_1.codventa, v_1.codsucursal, dp.indice) a_1 ON ((((vp.codventa = a_1.codventa) AND (vp.codsucursal = a_1.codsucursal)) AND (vp.indice = a_1.indice)))) ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal))))) a;


ALTER TABLE public.resumendeventasdiariasporproducto OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 98285)
-- Name: resumendeventasdiariasporventacondevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventasdiariasporventacondevolucion AS
 SELECT a.codventa,
    a.codsucursal,
    a.fecha,
    a.totalcosto,
    a.subtotal,
    a.total,
    a.subtotaliva,
    b.credito,
    b.contado,
        CASE a.total
            WHEN 0 THEN (0)::real
            ELSE ((a.subtotal * b.contado) / a.total)
        END AS basecontado,
        CASE a.total
            WHEN 0 THEN (0)::real
            ELSE ((a.subtotal * b.credito) / a.total)
        END AS basecredito
   FROM (( SELECT a_1.codventa,
            a_1.codsucursal,
            a_1.fecha,
            sum((a_1.cantidad * a_1.costopromedio)) AS totalcosto,
            sum((a_1.cantidad * a_1.precio)) AS subtotal,
            (sum(((a_1.cantidad * a_1.precio) * ((1)::double precision + (a_1.iva / (100)::double precision)))))::real AS total,
            (sum(((a_1.cantidad * a_1.precio) * (a_1.iva / (100)::double precision))))::real AS subtotaliva
           FROM ( SELECT vp.codventa,
                    vp.codsucursal,
                    vp.codproducto,
                    vp.cantidad AS comprado,
                    COALESCE(a_2.cantidad, (0)::real) AS devuelto,
                    (vp.cantidad - COALESCE(a_2.cantidad, (0)::real)) AS cantidad,
                    vp.costopromedio,
                    vp.precio,
                    vp.iva,
                    v.fecha
                   FROM (ventas v
                     JOIN (ventasproductos vp
                     LEFT JOIN ( SELECT v_1.codventa,
                            v_1.codsucursal,
                            dp.indice,
                            sum(dp.cantidad) AS cantidad
                           FROM (ventas v_1
                             LEFT JOIN (devolucionventa d
                             JOIN devolucionventaproductos dp ON (((d.coddevolucion = dp.coddevolucion) AND (d.codsucursal = dp.codsucursal)))) ON ((((d.codventa = v_1.codventa) AND (d.codsucursalventa = v_1.codsucursal)) AND (v_1.fecha = (d.fecha)::date))))
                          GROUP BY v_1.codventa, v_1.codsucursal, dp.indice) a_2 ON ((((vp.codventa = a_2.codventa) AND (vp.codsucursal = a_2.codsucursal)) AND (vp.indice = a_2.indice)))) ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal))))) a_1
          GROUP BY a_1.codventa, a_1.codsucursal, a_1.fecha) a
     JOIN ( SELECT c.codventa,
            c.codsucursal,
            sum(
                CASE
                    WHEN (c.tipopago = 'CREDITO'::text) THEN c.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN (c.tipopago <> 'CREDITO'::text) THEN c.monto
                    ELSE (0)::real
                END) AS contado
           FROM ( SELECT v.codventa,
                    v.codsucursal,
                        CASE
                            WHEN ((vp.tipopago)::text = 'CREDITO'::text) THEN 'CREDITO'::text
                            ELSE 'CONTADO'::text
                        END AS tipopago,
                    sum(vp.monto) AS monto
                   FROM (ventaspagos vp
                     JOIN ventas v ON (((vp.codventa = v.codventa) AND (vp.codsucursal = v.codsucursal))))
                  GROUP BY v.codventa, v.codsucursal,
                        CASE
                            WHEN ((vp.tipopago)::text = 'CREDITO'::text) THEN 'CREDITO'::text
                            ELSE 'CONTADO'::text
                        END
                UNION
                 SELECT v.codventa,
                    v.codsucursal,
                        CASE
                            WHEN ((dp.tipopago)::text = 'CREDITO'::text) THEN 'CREDITO'::text
                            ELSE 'CONTADO'::text
                        END AS tipopago,
                    sum((dp.monto * (- (1)::real))) AS monto
                   FROM (devolucionventapago dp
                     JOIN (devolucionventa d
                     JOIN ventas v ON ((((d.codventa = v.codventa) AND (d.codsucursalventa = v.codsucursal)) AND ((d.fecha)::date = v.fecha)))) ON (((dp.coddevolucion = d.coddevolucion) AND (dp.codsucursal = d.codsucursal))))
                  GROUP BY v.codventa, v.codsucursal,
                        CASE
                            WHEN ((dp.tipopago)::text = 'CREDITO'::text) THEN 'CREDITO'::text
                            ELSE 'CONTADO'::text
                        END) c
          GROUP BY c.codventa, c.codsucursal) b ON (((a.codventa = b.codventa) AND (a.codsucursal = b.codsucursal))));


ALTER TABLE public.resumendeventasdiariasporventacondevolucion OWNER TO postgres;

--
-- TOC entry 443 (class 1259 OID 115581)
-- Name: resumendeventasdiariasporventasindevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventasdiariasporventasindevolucion AS
 SELECT v.codventa,
    v.codsucursal,
    ((((v.fecha)::text || ' '::text) || (v.hora)::text))::timestamp without time zone AS fecha,
    vp.totalcosto,
    vp.subtotal,
    vp.total,
    vp.subtotaliva,
    (((((pag.efectivo + pag.tdebito) + pag.cheque) + pag.transfer) + pag.tcredito) + pag.ticket) AS contado,
    pag.credito,
    pag.efectivo,
    pag.tdebito,
    pag.cheque,
    pag.transfer,
    pag.tcredito,
    pag.ticket,
    v.retencion
   FROM (( SELECT ventaspagos.codventa,
            ventaspagos.codsucursal,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'EFECTIVO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'DEBITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS tdebito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CHEQUE'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TRANSFER'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TCREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS tcredito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TICKET'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS ticket
           FROM ventaspagos
          GROUP BY ventaspagos.codventa, ventaspagos.codsucursal) pag
     JOIN (( SELECT vp_1.codventa,
            vp_1.codsucursal,
            COALESCE(sum((vp_1.costopromedio * vp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp_1.precio * vp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * ((1)::real + (vp_1.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * (vp_1.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM ventasproductos vp_1
          GROUP BY vp_1.codventa, vp_1.codsucursal) vp
     JOIN ventas v ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal)))) ON (((pag.codventa = v.codventa) AND (v.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendeventasdiariasporventasindevolucion OWNER TO postgres;

--
-- TOC entry 339 (class 1259 OID 98295)
-- Name: retenciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE retenciones (
    codretencion integer NOT NULL,
    numero integer NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    codproveedor integer NOT NULL,
    rifproveedor character varying(15) NOT NULL,
    codsucursal integer NOT NULL,
    codmovimiento integer NOT NULL,
    nrodocumento character varying(15) NOT NULL,
    tipodocumento character varying(15) NOT NULL,
    montoretenido real NOT NULL
);


ALTER TABLE public.retenciones OWNER TO postgres;

--
-- TOC entry 340 (class 1259 OID 98300)
-- Name: retenciones_codretencion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE retenciones_codretencion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.retenciones_codretencion_seq OWNER TO postgres;

--
-- TOC entry 4392 (class 0 OID 0)
-- Dependencies: 340
-- Name: retenciones_codretencion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE retenciones_codretencion_seq OWNED BY retenciones.codretencion;


--
-- TOC entry 341 (class 1259 OID 98302)
-- Name: retenciones_compras_codretencion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE retenciones_compras_codretencion_seq
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.retenciones_compras_codretencion_seq OWNER TO postgres;

--
-- TOC entry 342 (class 1259 OID 98304)
-- Name: retenciones_compras; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE retenciones_compras (
    codretencion integer DEFAULT nextval('retenciones_compras_codretencion_seq'::regclass) NOT NULL,
    numero character varying(18),
    fechaemision date DEFAULT ('now'::text)::date NOT NULL,
    horaemision time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    montoiva real NOT NULL,
    codproveedor integer,
    codmovimiento integer,
    codsucursal integer,
    documento character varying(15),
    tipodocumento character varying(15),
    montoretenido real,
    mesafecta character varying(2),
    anioafecta character varying(4),
    tiporetencion character varying(50)
);


ALTER TABLE public.retenciones_compras OWNER TO postgres;

--
-- TOC entry 343 (class 1259 OID 98310)
-- Name: retenciones_ventas_codretencion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE retenciones_ventas_codretencion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.retenciones_ventas_codretencion_seq OWNER TO postgres;

--
-- TOC entry 344 (class 1259 OID 98312)
-- Name: retenciones_ventas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE retenciones_ventas (
    codretencion integer DEFAULT nextval('retenciones_ventas_codretencion_seq'::regclass) NOT NULL,
    numero character varying(18),
    fechaemision date DEFAULT ('now'::text)::date NOT NULL,
    horaemision time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL,
    montototal real NOT NULL,
    estatus character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.retenciones_ventas OWNER TO postgres;

--
-- TOC entry 345 (class 1259 OID 98319)
-- Name: retenciones_ventas_detalles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE retenciones_ventas_detalles (
    codretencion integer NOT NULL,
    numero character varying(18) NOT NULL,
    codcliente integer,
    codsucursalcliente integer,
    codmovimiento integer,
    codsucursal integer,
    documento character varying(15),
    tipodocumento character varying(15),
    montoretenido real,
    mesafecta character varying(2),
    anioafecta character varying(4)
);


ALTER TABLE public.retenciones_ventas_detalles OWNER TO postgres;

--
-- TOC entry 346 (class 1259 OID 98322)
-- Name: retenciones_ventas_numero_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE retenciones_ventas_numero_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.retenciones_ventas_numero_seq OWNER TO postgres;

--
-- TOC entry 347 (class 1259 OID 98324)
-- Name: retencionespendientes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE retencionespendientes (
    tipomovimiento character varying(20) NOT NULL,
    codmovimiento integer NOT NULL,
    monto real NOT NULL
);


ALTER TABLE public.retencionespendientes OWNER TO postgres;

--
-- TOC entry 4393 (class 0 OID 0)
-- Dependencies: 347
-- Name: COLUMN retencionespendientes.tipomovimiento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN retencionespendientes.tipomovimiento IS 'COMPRAS, ND, FAC, NC';


--
-- TOC entry 348 (class 1259 OID 98327)
-- Name: retnormal_codretnormal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE retnormal_codretnormal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.retnormal_codretnormal_seq OWNER TO postgres;

--
-- TOC entry 349 (class 1259 OID 98329)
-- Name: rettransito_codrettransito_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE rettransito_codrettransito_seq
    START WITH 2
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.rettransito_codrettransito_seq OWNER TO postgres;

--
-- TOC entry 456 (class 1259 OID 133592)
-- Name: sucdepositos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sucdepositos (
    id bigint NOT NULL,
    movbancario_id bigint DEFAULT 0 NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    monto double precision DEFAULT 0.00 NOT NULL,
    fecha date,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    caja_id bigint DEFAULT 0 NOT NULL,
    cuadrediario_id bigint DEFAULT 0 NOT NULL,
    valor double precision DEFAULT 0 NOT NULL,
    cantidad double precision DEFAULT 0
);


ALTER TABLE public.sucdepositos OWNER TO postgres;

--
-- TOC entry 457 (class 1259 OID 133604)
-- Name: sucdepositos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sucdepositos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sucdepositos_id_seq OWNER TO postgres;

--
-- TOC entry 4394 (class 0 OID 0)
-- Dependencies: 457
-- Name: sucdepositos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sucdepositos_id_seq OWNED BY sucdepositos.id;


--
-- TOC entry 461 (class 1259 OID 133766)
-- Name: sucgastos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sucgastos (
    id bigint NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    monto double precision DEFAULT 0.00 NOT NULL,
    fecha date,
    cuadrediario_id bigint DEFAULT 0 NOT NULL,
    nro text,
    nombre text,
    motivo text,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone
);


ALTER TABLE public.sucgastos OWNER TO postgres;

--
-- TOC entry 460 (class 1259 OID 133764)
-- Name: sucgastos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sucgastos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sucgastos_id_seq OWNER TO postgres;

--
-- TOC entry 4395 (class 0 OID 0)
-- Dependencies: 460
-- Name: sucgastos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sucgastos_id_seq OWNED BY sucgastos.id;


--
-- TOC entry 350 (class 1259 OID 98331)
-- Name: sucursal_codsucursal_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sucursal_codsucursal_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sucursal_codsucursal_seq OWNER TO postgres;

--
-- TOC entry 351 (class 1259 OID 98333)
-- Name: sucursal; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sucursal (
    codsucursal integer DEFAULT nextval('sucursal_codsucursal_seq'::regclass) NOT NULL,
    descripcion text NOT NULL,
    ip character varying(50) NOT NULL,
    direccion text NOT NULL,
    telefonos character varying(40),
    estatus boolean DEFAULT true NOT NULL,
    codcaja integer,
    bd text
);


ALTER TABLE public.sucursal OWNER TO postgres;

--
-- TOC entry 352 (class 1259 OID 98341)
-- Name: tipocomision; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipocomision (
    id bigint NOT NULL,
    nombre text,
    abreviatura text,
    estatus boolean DEFAULT true,
    c_porc boolean DEFAULT true,
    c_desdemonto boolean DEFAULT true,
    c_hastamonto boolean DEFAULT true,
    c_aplmontobruto boolean DEFAULT true,
    c_desdedia boolean DEFAULT true,
    c_hastadia boolean DEFAULT true
);


ALTER TABLE public.tipocomision OWNER TO postgres;

--
-- TOC entry 353 (class 1259 OID 98354)
-- Name: tipocomision_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipocomision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipocomision_id_seq OWNER TO postgres;

--
-- TOC entry 4396 (class 0 OID 0)
-- Dependencies: 353
-- Name: tipocomision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipocomision_id_seq OWNED BY tipocomision.id;


--
-- TOC entry 354 (class 1259 OID 98356)
-- Name: tipocomisiondef; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipocomisiondef (
    id bigint NOT NULL,
    nombre text,
    tipocomision_id bigint,
    estatus boolean DEFAULT true
);


ALTER TABLE public.tipocomisiondef OWNER TO postgres;

--
-- TOC entry 355 (class 1259 OID 98363)
-- Name: tipocomisiondef_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipocomisiondef_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipocomisiondef_id_seq OWNER TO postgres;

--
-- TOC entry 4397 (class 0 OID 0)
-- Dependencies: 355
-- Name: tipocomisiondef_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipocomisiondef_id_seq OWNED BY tipocomisiondef.id;


--
-- TOC entry 356 (class 1259 OID 98365)
-- Name: tipocomisiondef_tablacomision; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipocomisiondef_tablacomision (
    id bigint NOT NULL,
    tipocomisiondef_id integer NOT NULL,
    porc real NOT NULL,
    desdemonto real NOT NULL,
    hastamonto real NOT NULL,
    desdedia real NOT NULL,
    hastadia real NOT NULL,
    aplicamontobruto boolean DEFAULT true
);


ALTER TABLE public.tipocomisiondef_tablacomision OWNER TO postgres;

--
-- TOC entry 357 (class 1259 OID 98369)
-- Name: tipocomisiondef_tablacomision_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipocomisiondef_tablacomision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipocomisiondef_tablacomision_id_seq OWNER TO postgres;

--
-- TOC entry 4398 (class 0 OID 0)
-- Dependencies: 357
-- Name: tipocomisiondef_tablacomision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipocomisiondef_tablacomision_id_seq OWNED BY tipocomisiondef_tablacomision.id;


--
-- TOC entry 358 (class 1259 OID 98371)
-- Name: tipocomisiondef_vendedores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipocomisiondef_vendedores (
    id bigint NOT NULL,
    codvendedor bigint,
    codsucursal bigint,
    tipocomisiondef_id bigint
);


ALTER TABLE public.tipocomisiondef_vendedores OWNER TO postgres;

--
-- TOC entry 359 (class 1259 OID 98374)
-- Name: tipocomisiondef_vendedores_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipocomisiondef_vendedores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipocomisiondef_vendedores_id_seq OWNER TO postgres;

--
-- TOC entry 4399 (class 0 OID 0)
-- Dependencies: 359
-- Name: tipocomisiondef_vendedores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipocomisiondef_vendedores_id_seq OWNED BY tipocomisiondef_vendedores.id;


--
-- TOC entry 360 (class 1259 OID 98376)
-- Name: tipoexcepcion; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipoexcepcion (
    id bigint NOT NULL,
    nombre text,
    abreviatura text,
    estatus boolean DEFAULT true,
    c_porc boolean DEFAULT true,
    c_desdemonto boolean DEFAULT true,
    c_hastamonto boolean DEFAULT true,
    c_aplmontobruto boolean DEFAULT true,
    c_desdedia boolean DEFAULT true,
    c_hastadia boolean DEFAULT true
);


ALTER TABLE public.tipoexcepcion OWNER TO postgres;

--
-- TOC entry 361 (class 1259 OID 98389)
-- Name: tipoexcepcion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipoexcepcion_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipoexcepcion_id_seq OWNER TO postgres;

--
-- TOC entry 4400 (class 0 OID 0)
-- Dependencies: 361
-- Name: tipoexcepcion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipoexcepcion_id_seq OWNED BY tipoexcepcion.id;


--
-- TOC entry 362 (class 1259 OID 98391)
-- Name: tipoexcepciondef; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipoexcepciondef (
    id bigint NOT NULL,
    nombre text,
    tipoexcepcion_id bigint,
    estatus boolean DEFAULT true
);


ALTER TABLE public.tipoexcepciondef OWNER TO postgres;

--
-- TOC entry 363 (class 1259 OID 98398)
-- Name: tipoexcepciondef_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipoexcepciondef_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipoexcepciondef_id_seq OWNER TO postgres;

--
-- TOC entry 4401 (class 0 OID 0)
-- Dependencies: 363
-- Name: tipoexcepciondef_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipoexcepciondef_id_seq OWNED BY tipoexcepciondef.id;


--
-- TOC entry 364 (class 1259 OID 98400)
-- Name: tipoexcepciondef_tablacomision; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipoexcepciondef_tablacomision (
    id bigint NOT NULL,
    tipoexcepciondef_id integer NOT NULL,
    porc real NOT NULL,
    desdemonto real NOT NULL,
    hastamonto real NOT NULL,
    desdedia real NOT NULL,
    hastadia real NOT NULL,
    aplicamontobruto boolean DEFAULT true,
    codproducto bigint,
    coddepartamento bigint
);


ALTER TABLE public.tipoexcepciondef_tablacomision OWNER TO postgres;

--
-- TOC entry 365 (class 1259 OID 98404)
-- Name: tipoexcepciondef_tablacomision_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipoexcepciondef_tablacomision_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipoexcepciondef_tablacomision_id_seq OWNER TO postgres;

--
-- TOC entry 4402 (class 0 OID 0)
-- Dependencies: 365
-- Name: tipoexcepciondef_tablacomision_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipoexcepciondef_tablacomision_id_seq OWNED BY tipoexcepciondef_tablacomision.id;


--
-- TOC entry 366 (class 1259 OID 98406)
-- Name: tipoexcepciondef_vendedores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipoexcepciondef_vendedores (
    id bigint NOT NULL,
    codvendedor bigint,
    codsucursal bigint,
    tipoexcepciondef_id bigint
);


ALTER TABLE public.tipoexcepciondef_vendedores OWNER TO postgres;

--
-- TOC entry 367 (class 1259 OID 98409)
-- Name: tipoexcepciondef_vendedores_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipoexcepciondef_vendedores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipoexcepciondef_vendedores_id_seq OWNER TO postgres;

--
-- TOC entry 4403 (class 0 OID 0)
-- Dependencies: 367
-- Name: tipoexcepciondef_vendedores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipoexcepciondef_vendedores_id_seq OWNED BY tipoexcepciondef_vendedores.id;


--
-- TOC entry 368 (class 1259 OID 98411)
-- Name: tiposusuario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tiposusuario (
    codtipousuario integer NOT NULL,
    nombre character varying(50) NOT NULL
);


ALTER TABLE public.tiposusuario OWNER TO postgres;

--
-- TOC entry 369 (class 1259 OID 98414)
-- Name: tiposusuario_codtipousuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tiposusuario_codtipousuario_seq
    START WITH 13
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tiposusuario_codtipousuario_seq OWNER TO postgres;

--
-- TOC entry 4404 (class 0 OID 0)
-- Dependencies: 369
-- Name: tiposusuario_codtipousuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tiposusuario_codtipousuario_seq OWNED BY tiposusuario.codtipousuario;


--
-- TOC entry 370 (class 1259 OID 98416)
-- Name: tiposusuarioformulario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tiposusuarioformulario (
    codtipousuario integer NOT NULL,
    codformulario integer NOT NULL
);


ALTER TABLE public.tiposusuarioformulario OWNER TO postgres;

--
-- TOC entry 371 (class 1259 OID 98419)
-- Name: traslados_codtraslado_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE traslados_codtraslado_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.traslados_codtraslado_seq OWNER TO postgres;

--
-- TOC entry 372 (class 1259 OID 98421)
-- Name: traslados; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE traslados (
    codtraslado integer DEFAULT nextval('traslados_codtraslado_seq'::regclass) NOT NULL,
    codcargo integer NOT NULL,
    codsucursalcargo integer NOT NULL,
    coddescargo integer NOT NULL,
    codsucursaldescargo integer NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    controldestino character varying
);


ALTER TABLE public.traslados OWNER TO postgres;

--
-- TOC entry 373 (class 1259 OID 98429)
-- Name: unidades; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE unidades (
    unidad character varying(20) NOT NULL
);


ALTER TABLE public.unidades OWNER TO postgres;

--
-- TOC entry 374 (class 1259 OID 98432)
-- Name: usuarioclave; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuarioclave (
    codusuario integer NOT NULL,
    clave character varying(50) NOT NULL
);


ALTER TABLE public.usuarioclave OWNER TO postgres;

--
-- TOC entry 375 (class 1259 OID 98435)
-- Name: usuarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuarios (
    codusuario integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(50) NOT NULL,
    nombreusuario character varying(50) NOT NULL,
    claveusuario character varying(50) NOT NULL,
    estatus boolean DEFAULT true NOT NULL,
    telefonos character varying(30),
    direccion text,
    codsucursal integer NOT NULL,
    email text,
    codtipo integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.usuarios OWNER TO postgres;

--
-- TOC entry 376 (class 1259 OID 98443)
-- Name: usuarios_codusuario_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE usuarios_codusuario_seq
    START WITH 43
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuarios_codusuario_seq OWNER TO postgres;

--
-- TOC entry 4405 (class 0 OID 0)
-- Dependencies: 376
-- Name: usuarios_codusuario_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE usuarios_codusuario_seq OWNED BY usuarios.codusuario;


--
-- TOC entry 377 (class 1259 OID 98445)
-- Name: usuariosfuncionesformulario; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE usuariosfuncionesformulario (
    codusuario integer NOT NULL,
    codfuncionformulario integer NOT NULL,
    codsucursal integer NOT NULL
);


ALTER TABLE public.usuariosfuncionesformulario OWNER TO postgres;

--
-- TOC entry 378 (class 1259 OID 98448)
-- Name: v_ajuste; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ajuste AS
 SELECT (h_producto.codajuste)::text AS "Código_varchar",
    count(h_producto.codproducto) AS "TotalProductos_int",
    h_producto.fecha AS "FechaCreacion_date",
    (h_producto.codusuario)::text AS "CodUsuario_varchar"
   FROM h_producto
  GROUP BY h_producto.codajuste, h_producto.fecha, h_producto.codusuario;


ALTER TABLE public.v_ajuste OWNER TO postgres;

--
-- TOC entry 449 (class 1259 OID 133359)
-- Name: v_bancos; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_bancos AS
 SELECT bancos.codbanco AS "Código_int",
    bancos.nombre AS "Nombre_varchar",
    bancos.abreviatura AS "Abreviatura_text"
   FROM bancos
  WHERE (bancos.estatus = true);


ALTER TABLE public.v_bancos OWNER TO postgres;

--
-- TOC entry 379 (class 1259 OID 98452)
-- Name: v_cargo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_cargo AS
 SELECT cargos.codcargo AS "Código_int",
    cargos.descripcion AS "Descripción_text",
    cargos.autorizado AS "Autorizado_varchar",
    cargos.responsable AS "Responsable_varchar",
    to_char((cargos.fecha)::timestamp with time zone, 'DD/MM/YYYY'::text) AS "Fecha_date",
    sucursal.descripcion AS "Sucursal_text"
   FROM (cargos
     JOIN sucursal ON ((cargos.codsucursal = sucursal.codsucursal)));


ALTER TABLE public.v_cargo OWNER TO postgres;

--
-- TOC entry 380 (class 1259 OID 98456)
-- Name: vendedores_codvendedor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE vendedores_codvendedor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.vendedores_codvendedor_seq OWNER TO postgres;

--
-- TOC entry 381 (class 1259 OID 98458)
-- Name: vendedores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE vendedores (
    codvendedor integer DEFAULT nextval('vendedores_codvendedor_seq'::regclass) NOT NULL,
    descripcion text NOT NULL,
    clase character varying(10),
    nivelprecios2 character varying(1),
    tabladeventas2 character varying(1),
    tabladecobros2 character varying(1),
    tabladeutilidad2 character varying(1),
    diastolerancia2 character varying(1),
    estatus boolean DEFAULT true NOT NULL,
    codusuario integer,
    codsucursal integer NOT NULL,
    nivelprecios boolean,
    tabladeventas boolean,
    tabladecobros boolean,
    tabladeutilidad boolean,
    diastolerancia boolean,
    diastoleranciaxmontos boolean,
    telefonos text
);


ALTER TABLE public.vendedores OWNER TO postgres;

--
-- TOC entry 382 (class 1259 OID 98466)
-- Name: v_cliente; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_cliente AS
 SELECT (((clientes.codcliente)::text || '-'::text) || (clientes.codsucursal)::text) AS "Código_varchar",
    clientes.descripcion AS "Descripción_text",
    clientes.rif AS "RIF_varchar",
    clientes.clase AS "Clase_varchar",
    clientes.representante AS "Representante_text",
    clientes.direccion AS direccion_text,
    clientes.telefonos AS "Teléfonos_varchar",
    clientes.email AS "Email_text",
    clientes.numerodefax AS "Nro. Fax_varchar",
    clientes.zona AS "Zona_varchar",
    vendedores.descripcion AS "Vendedor_text",
    clientes.tipodeprecio AS "Tipo de Precio_varchar",
    clientes.limitecredito AS "Límite de Crédito_float",
    clientes.diasdecredito AS "Días de Crédito_int",
    clientes.diastolerancia AS "Días de Tolerancia_int",
    clientes.fechadeinicio AS "Fecha de Inicio_date",
    clientes.observaciones AS "Observaciones_text",
    clientes.tipodecliente AS "Tipo de Cliente_varchar",
    clientes.interesdemora AS "Interés de Mora_varchar",
    clientes.convenioprecio AS "Convenio de Precio_varchar",
    clientes.porcdescuento AS "% Descuento_float"
   FROM (clientes
     LEFT JOIN vendedores ON ((clientes.codvendedor = vendedores.codvendedor)))
  WHERE (clientes.estatus = true);


ALTER TABLE public.v_cliente OWNER TO postgres;

--
-- TOC entry 383 (class 1259 OID 98471)
-- Name: v_cliente_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_cliente_i AS
 SELECT (((clientes.codcliente)::text || '-'::text) || (clientes.codsucursal)::text) AS "Código_varchar",
    clientes.descripcion AS "Descripción_text",
    clientes.rif AS "RIF_varchar",
    clientes.clase AS "Clase_varchar",
    clientes.representante AS "Representante_text",
    clientes.direccion AS direccion_text,
    clientes.telefonos AS "Teléfonos_varchar",
    clientes.email AS "Email_text",
    clientes.numerodefax AS "Nro. Fax_varchar",
    clientes.zona AS "Zona_varchar",
    vendedores.descripcion AS "Vendedor_text",
    clientes.tipodeprecio AS "Tipo de Precio_varchar",
    clientes.limitecredito AS "Límite de Crédito_float",
    clientes.diasdecredito AS "Días de Crédito_int",
    clientes.diastolerancia AS "Días de Tolerancia_int",
    clientes.fechadeinicio AS "Fecha de Inicio_date",
    clientes.observaciones AS "Observaciones_text",
    clientes.tipodecliente AS "Tipo de Cliente_varchar",
    clientes.interesdemora AS "Interés de Mora_varchar",
    clientes.convenioprecio AS "Convenio de Precio_varchar",
    clientes.porcdescuento AS "% Descuento_float"
   FROM (clientes
     JOIN vendedores ON (((clientes.codvendedor = vendedores.codvendedor) AND (clientes.estatus = false))));


ALTER TABLE public.v_cliente_i OWNER TO postgres;

--
-- TOC entry 384 (class 1259 OID 98476)
-- Name: v_compra; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_compra AS
 SELECT compras.codcompra AS "Código_int",
    proveedores.rif AS "RIF_text",
    proveedores.descripcion AS "Proveedor_text",
    depositos.descripcion AS "Depósito_text",
    compras.nrodocumento AS "Documento_varchar",
    compras.nrocontrol AS "Control_varchar",
    compras.femision AS "Emisión_date",
    compras.fregistro AS "Registro_date"
   FROM ((compras
     JOIN proveedores ON ((compras.codproveedor = proveedores.codproveedor)))
     JOIN depositos ON ((depositos.coddeposito = compras.coddeposito)));


ALTER TABLE public.v_compra OWNER TO postgres;

--
-- TOC entry 385 (class 1259 OID 98481)
-- Name: v_compraespera; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_compraespera AS
 SELECT a.codcompraespera AS "Código_int",
    b.descripcion AS "Proveedor_varchar",
    a.nrodocumento AS "Documento_varchar",
    a.femision AS "Fecha_date",
    a.coddeposito AS "Depósito_int"
   FROM (comprasespera a
     JOIN proveedores b ON ((a.codproveedor = b.codproveedor)));


ALTER TABLE public.v_compraespera OWNER TO postgres;

--
-- TOC entry 386 (class 1259 OID 98485)
-- Name: v_comprasfacturas_cxp; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_comprasfacturas_cxp AS
 SELECT a.codproveedor,
    a.descripcion,
    a.direccion,
    a.tipomovimiento,
    a.documento,
    b.fvencimiento,
    a.fecha,
    b.monto,
    a.codmovimiento
   FROM (( SELECT a_1.codcompra AS codmovimiento,
            'COMPRAS'::text AS tipomovimiento,
            a_1.nrodocumento AS documento,
            a_1.femision AS fecha,
            a_1.codproveedor,
            ( SELECT proveedores.descripcion
                   FROM proveedores
                  WHERE (proveedores.codproveedor = a_1.codproveedor)) AS descripcion,
            ( SELECT proveedores.direccion
                   FROM proveedores
                  WHERE (proveedores.codproveedor = a_1.codproveedor)) AS direccion
           FROM compras a_1
        UNION
         SELECT a_1.codcpfactura AS codmovimiento,
            'FA'::text AS tipomovimiento,
            a_1.nrodocumento AS documento,
            a_1.fecha,
            a_1.codproveedor,
            ( SELECT proveedores.descripcion
                   FROM proveedores
                  WHERE (proveedores.codproveedor = a_1.codproveedor)) AS descripcion,
            ( SELECT proveedores.direccion
                   FROM proveedores
                  WHERE (proveedores.codproveedor = a_1.codproveedor)) AS direccion
           FROM cpfactura a_1) a
     JOIN cuentasporpagar b ON (((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text))))
  ORDER BY a.descripcion, b.fvencimiento;


ALTER TABLE public.v_comprasfacturas_cxp OWNER TO postgres;

--
-- TOC entry 387 (class 1259 OID 98490)
-- Name: v_correo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_correo AS
 SELECT upper((correo.nombre)::text) AS "Nombre_varchar",
    correo.tipo AS "Tipo_varchar"
   FROM correo;


ALTER TABLE public.v_correo OWNER TO postgres;

--
-- TOC entry 388 (class 1259 OID 98494)
-- Name: v_departamento; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_departamento AS
 SELECT departamentos.codigo AS "Código_varchar",
    departamentos.descripcion AS "Descripción_text",
    departamentos.clase AS "Clase_varchar",
    departamentos.compuesto AS "Compuesto_varchar",
    departamentos.seriales AS "Seriales_varchar",
    departamentos.comisiones AS "Comisiones_varchar",
    departamentos.estatus AS "Estatus_bool"
   FROM departamentos
  WHERE (departamentos.estatus = true);


ALTER TABLE public.v_departamento OWNER TO postgres;

--
-- TOC entry 389 (class 1259 OID 98498)
-- Name: v_departamento_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_departamento_i AS
 SELECT departamentos.coddepartamento AS "Código_int",
    departamentos.descripcion AS "Descripción_text",
    departamentos.clase AS "Clase_varchar",
    departamentos.compuesto AS "Compuesto_varchar",
    departamentos.seriales AS "Seriales_varchar",
    departamentos.comisiones AS "Comisiones_varchar",
    departamentos.estatus AS "Estatus_bool"
   FROM departamentos
  WHERE (departamentos.estatus = false);


ALTER TABLE public.v_departamento_i OWNER TO postgres;

--
-- TOC entry 390 (class 1259 OID 98502)
-- Name: v_deposito; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_deposito AS
 SELECT depositos.coddeposito AS "Código_varchar",
    depositos.descripcion AS "Descripción_text",
    depositos.responsable AS "Responsable_text",
    sucursal.descripcion AS "Sucursal_text",
    depositos.clase AS "Clase_varchar"
   FROM (depositos
     JOIN sucursal ON (((depositos.codsucursal = sucursal.codsucursal) AND (depositos.estatus = true))));


ALTER TABLE public.v_deposito OWNER TO postgres;

--
-- TOC entry 391 (class 1259 OID 98506)
-- Name: v_deposito_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_deposito_i AS
 SELECT depositos.coddeposito AS "Código_varchar",
    depositos.descripcion AS "Descripción_text",
    depositos.responsable AS "Responsable_text",
    sucursal.descripcion AS "Sucursal_text",
    depositos.clase AS "Clase_varchar"
   FROM (depositos
     JOIN sucursal ON (((depositos.codsucursal = sucursal.codsucursal) AND (depositos.estatus = false))));


ALTER TABLE public.v_deposito_i OWNER TO postgres;

--
-- TOC entry 392 (class 1259 OID 98510)
-- Name: v_descargo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_descargo AS
 SELECT descargos.coddescargo AS "Código_int",
    descargos.descripcion AS "Descripción_text",
    descargos.autorizado AS "Autorizado_varchar",
    descargos.responsable AS "Responsable_varchar",
    to_char((descargos.fecha)::timestamp with time zone, 'DD/MM/YYYY'::text) AS "Fecha_date",
    sucursal.descripcion AS "Sucursal_text"
   FROM (descargos
     JOIN sucursal ON ((descargos.codsucursal = sucursal.codsucursal)));


ALTER TABLE public.v_descargo OWNER TO postgres;

--
-- TOC entry 393 (class 1259 OID 98514)
-- Name: v_devolucion_z; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_devolucion_z AS
 SELECT devolucion_z.codevolucionz AS "Códigodev_int",
    devolucion_z.codmovimientoz AS "Códigomov_int",
    devolucion_z.factura AS "Factura_int",
    devolucion_z.fecha AS "Fecha_date"
   FROM devolucion_z;


ALTER TABLE public.v_devolucion_z OWNER TO postgres;

--
-- TOC entry 394 (class 1259 OID 98518)
-- Name: v_devolucioncompra; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_devolucioncompra AS
 SELECT d.coddevolucioncompra AS "Código_varchar",
    d.codcompra AS "Compra_varchar",
    p.rif AS "RIF_text",
    p.descripcion AS "Descripcion_varchar",
    c.nrodocumento AS "Documento_varchar",
    d.fdevolucion AS "Fecha_date"
   FROM (devolucioncompra d
     JOIN (compras c
     JOIN proveedores p ON ((c.codproveedor = p.codproveedor))) ON ((d.codcompra = c.codcompra)))
  ORDER BY d.coddevolucioncompra;


ALTER TABLE public.v_devolucioncompra OWNER TO postgres;

--
-- TOC entry 395 (class 1259 OID 98523)
-- Name: v_devolucionventa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_devolucionventa AS
 SELECT (((d.coddevolucion)::text || '-'::text) || (d.codsucursal)::text) AS "Código_varchar",
    d.coddevolucion AS "Numero_varchar",
    v.codvendedor AS "Vendedor_varchar",
    (((v.codcliente)::text || '-'::text) || (v.codsucursal)::text) AS "Código Cliente_varchar",
    v.numerofactura AS "Factura_varchar",
    c.rif AS "Rif_varchar",
    c.descripcion AS "Descripción_varchar",
    d.fecha AS "Fecha_date",
    ( SELECT sum(devolucionventapago.monto) AS sum
           FROM devolucionventapago
          WHERE ((devolucionventapago.codsucursal = d.codsucursal) AND (devolucionventapago.coddevolucion = d.coddevolucion))) AS "Monto_double",
    d.coddeposito AS "Deposito_varchar",
    d.codventa AS "Venta_varchar",
    d.devoluciontotal AS "DevolucionTotal_bool"
   FROM (devolucionventa d
     JOIN (ventas v
     JOIN clientes c ON (((v.codcliente = c.codcliente) AND (v.codclientesucursal = c.codsucursal)))) ON (((d.codventa = v.codventa) AND (d.codsucursalventa = v.codsucursal))))
  ORDER BY d.coddevolucion;


ALTER TABLE public.v_devolucionventa OWNER TO postgres;

--
-- TOC entry 396 (class 1259 OID 98528)
-- Name: v_devolucionventa_2; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_devolucionventa_2 AS
 SELECT (((d.coddevolucion)::text || '-'::text) || (d.codsucursal)::text) AS "Código_varchar",
    d.coddevolucion AS "Numero_varchar",
    v.codvendedor AS "Vendedor_varchar",
    (((v.codcliente)::text || '-'::text) || (v.codsucursal)::text) AS "Código Cliente_varchar",
    v.numerofactura AS "Factura_varchar",
    c.rif AS "Rif_varchar",
    c.descripcion AS "Descripción_varchar",
    d.fecha AS "Fecha_date",
    ( SELECT sum(devolucionventapago.monto) AS sum
           FROM devolucionventapago
          WHERE ((devolucionventapago.codsucursal = d.codsucursal) AND (devolucionventapago.coddevolucion = d.coddevolucion))) AS "Monto_double",
    d.coddeposito AS "Deposito_varchar",
    d.codventa AS "Venta_varchar",
    d.devoluciontotal AS "DevolucionTotal_bool",
    0 AS ctid
   FROM (devolucionventa d
     JOIN (ventas v
     JOIN clientes c ON (((v.codcliente = c.codcliente) AND (v.codclientesucursal = c.codsucursal)))) ON (((d.codventa = v.codventa) AND (d.codsucursalventa = v.codsucursal))))
  ORDER BY d.coddevolucion;


ALTER TABLE public.v_devolucionventa_2 OWNER TO postgres;

--
-- TOC entry 397 (class 1259 OID 98533)
-- Name: v_estaciones; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_estaciones AS
 SELECT a.codestacion AS "Código_int",
    a.nombre AS "Nombre_text",
    a.descripcion AS "Descripción_varchar",
    (((b.codsucursal)::text || '-'::text) || b.descripcion) AS "Sucursal_varchar",
    a.codnumerofactura AS "Numeración_varchar"
   FROM (estaciones a
     JOIN sucursal b ON ((a.codsucursal = b.codsucursal)));


ALTER TABLE public.v_estaciones OWNER TO postgres;

--
-- TOC entry 398 (class 1259 OID 98537)
-- Name: v_estadodecuenta; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_estadodecuenta AS
 SELECT todo.tipomovimiento,
    todo.femision,
    todo.fvencimiento,
    todo.numero,
    todo.concepto,
    todo.debito,
    todo.credito,
    todo.fecha,
    todo.codproveedor
   FROM ( SELECT a.tipomovimiento,
            a.femision,
            b.fvencimiento,
            a.numero,
            b.concepto,
            0 AS debito,
            b.monto AS credito,
            a.fecha,
            a.codproveedor
           FROM (( SELECT a_1.codcompra AS codmovimiento,
                    'COMPRAS'::text AS tipomovimiento,
                    a_1.femision,
                    a_1.nrodocumento AS numero,
                    a_1.femision AS fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM compras a_1
                UNION
                 SELECT a_1.codcpfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.fecha,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpfactura a_1
                UNION
                 SELECT a_1.codcpnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.fecha,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpnotadebito a_1) a
             JOIN cuentasporpagar b ON ((((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text)) AND (a.codsucursal = b.codsucursal))))
        UNION
         SELECT a.tipomovimiento,
            a.femision,
            bh.fvencimiento,
            a.numero,
            bh.concepto,
            0 AS debito,
            bh.monto AS credito,
            a.fecha,
            a.codproveedor
           FROM (( SELECT a_1.codcompra AS codmovimiento,
                    'COMPRAS'::text AS tipomovimiento,
                    a_1.femision,
                    a_1.nrodocumento AS numero,
                    a_1.femision AS fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM compras a_1
                UNION
                 SELECT a_1.codcpfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.fecha,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpfactura a_1
                UNION
                 SELECT a_1.codcpnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.fecha,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpnotadebito a_1) a
             JOIN h_cuentasporpagar bh ON ((((a.codmovimiento = bh.codmovimiento) AND (a.tipomovimiento = (bh.tipomovimiento)::text)) AND (a.codsucursal = bh.codsucursal))))
        UNION
         SELECT cpp.tipomovimientopago AS tipomovimiento,
            pagos.fecha AS femision,
            NULL::date AS fvencimiento,
            pagos.numero,
            pagos.descripcion AS concepto,
            cpp.monto AS debito,
            0 AS credito,
            pagos.fecha,
            a.codproveedor
           FROM (( SELECT cpnotacredito.codcpnotacredito AS codmovimiento,
                    'NC'::text AS tipomovimiento,
                    cpnotacredito.fecha,
                    cpnotacredito.nrodocumento AS numero,
                    cpnotacredito.concepto AS descripcion
                   FROM cpnotacredito
                UNION
                 SELECT cpabono.codcpabono AS codmovimiento,
                    'PAG'::text AS tipomovimiento,
                    cpabono.fecha,
                    cpabono.nrodocumento AS numero,
                    cpabono.concepto AS descripcion
                   FROM cpabono
                UNION
                 SELECT cpabono.codcpabono AS codmovimiento,
                    'ABO'::text AS tipomovimiento,
                    cpabono.fecha,
                    cpabono.nrodocumento AS numero,
                    cpabono.concepto AS descripcion
                   FROM cpabono) pagos
             JOIN (cppagadas cpp
             JOIN (cuentasporpagar cxp
             JOIN ( SELECT a_1.codcompra AS codmovimiento,
                    'COMPRAS'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.femision,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM compras a_1
                UNION
                 SELECT a_1.codcpfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpfactura a_1
                UNION
                 SELECT a_1.codcpnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpnotadebito a_1) a ON ((((a.codmovimiento = cxp.codmovimiento) AND (a.tipomovimiento = (cxp.tipomovimiento)::text)) AND (a.codsucursal = cxp.codsucursal)))) ON (((((cpp.tipomovimientocp)::text = (cxp.tipomovimiento)::text) AND (cpp.codmovimientocp = cxp.codmovimiento)) AND (cxp.codsucursal = cpp.codsucursalcp)))) ON (((cpp.codmovimientopago = pagos.codmovimiento) AND ((cpp.tipomovimientopago)::text = pagos.tipomovimiento))))
        UNION
         SELECT cpp.tipomovimientopago AS tipomovimiento,
            pagos.fecha AS femision,
            NULL::date AS fvencimiento,
            pagos.numero,
            pagos.descripcion AS concepto,
            cpp.monto AS debito,
            0 AS credito,
            pagos.fecha,
            a.codproveedor
           FROM (( SELECT cpnotacredito.codcpnotacredito AS codmovimiento,
                    'NC'::text AS tipomovimiento,
                    cpnotacredito.fecha,
                    cpnotacredito.nrodocumento AS numero,
                    cpnotacredito.concepto AS descripcion
                   FROM cpnotacredito
                UNION
                 SELECT cpabono.codcpabono AS codmovimiento,
                    'PAG'::text AS tipomovimiento,
                    cpabono.fecha,
                    cpabono.nrodocumento AS numero,
                    cpabono.concepto AS descripcion
                   FROM cpabono
                UNION
                 SELECT cpabono.codcpabono AS codmovimiento,
                    'ABO'::text AS tipomovimiento,
                    cpabono.fecha,
                    cpabono.nrodocumento AS numero,
                    cpabono.concepto AS descripcion
                   FROM cpabono) pagos
             JOIN (cppagadas cpp
             JOIN (h_cuentasporpagar hcxp
             JOIN ( SELECT a_1.codcompra AS codmovimiento,
                    'COMPRAS'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.femision,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM compras a_1
                UNION
                 SELECT a_1.codcpfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpfactura a_1
                UNION
                 SELECT a_1.codcpnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpnotadebito a_1) a ON ((((a.codmovimiento = hcxp.codmovimiento) AND (a.tipomovimiento = (hcxp.tipomovimiento)::text)) AND (a.codsucursal = hcxp.codsucursal)))) ON (((((cpp.tipomovimientocp)::text = (hcxp.tipomovimiento)::text) AND (cpp.codmovimientocp = hcxp.codmovimiento)) AND (hcxp.codsucursal = cpp.codsucursalcp)))) ON (((cpp.codmovimientopago = pagos.codmovimiento) AND ((cpp.tipomovimientopago)::text = pagos.tipomovimiento))))) todo;


ALTER TABLE public.v_estadodecuenta OWNER TO postgres;

--
-- TOC entry 399 (class 1259 OID 98542)
-- Name: v_estadodecuenta_cxc; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_estadodecuenta_cxc AS
 SELECT todo.tipomovimiento,
    todo.fvencimiento,
    todo.numero,
    todo.concepto,
    todo.debito,
    todo.credito,
    todo.fecha,
    todo.codcliente,
    todo.codclientesucursal
   FROM ( SELECT a.tipomovimiento,
            b.fvencimiento,
            a.numero,
            b.concepto,
            0 AS debito,
            b.monto AS credito,
            a.fecha,
            a.codcliente,
            a.codclientesucursal
           FROM (( SELECT a_1.codventa AS codmovimiento,
                    'VENTAS'::text AS tipomovimiento,
                    a_1.numerofactura AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ventas a_1
                UNION
                 SELECT a_1.codccfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccfactura a_1
                UNION
                 SELECT a_1.codccnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccnotadebito a_1) a
             JOIN cuentasporcobrar b ON ((((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text)) AND (a.codsucursal = b.codsucursal))))
        UNION
         SELECT a.tipomovimiento,
            bh.fvencimiento,
            a.numero,
            bh.concepto,
            0 AS debito,
            bh.monto AS credito,
            a.fecha,
            a.codcliente,
            a.codclientesucursal
           FROM (( SELECT a_1.codventa AS codmovimiento,
                    'VENTAS'::text AS tipomovimiento,
                    a_1.numerofactura AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ventas a_1
                UNION
                 SELECT a_1.codccfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccfactura a_1
                UNION
                 SELECT a_1.codccnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccnotadebito a_1) a
             JOIN h_cuentasporcobrar bh ON ((((a.codmovimiento = bh.codmovimiento) AND (a.tipomovimiento = (bh.tipomovimiento)::text)) AND (a.codsucursal = bh.codsucursal))))
        UNION
         SELECT ccp.tipomovimientopago AS tipomovimiento,
            NULL::date AS fvencimiento,
            pagos.numero,
            pagos.descripcion AS concepto,
            ccp.monto AS debito,
            0 AS credito,
            pagos.fecha,
            a.codcliente,
            a.codclientesucursal
           FROM (( SELECT ccnotacredito.codccnotacredito AS codmovimiento,
                    'NC'::text AS tipomovimiento,
                    ccnotacredito.fecha,
                    ccnotacredito.nrodocumento AS numero,
                    ccnotacredito.concepto AS descripcion
                   FROM ccnotacredito
                UNION
                 SELECT ccabono.codccabono AS codmovimiento,
                    'PAG'::text AS tipomovimiento,
                    ccabono.fecha,
                    ccabono.nrodocumento AS numero,
                    ccabono.concepto AS descripcion
                   FROM ccabono
                UNION
                 SELECT ccabono.codccabono AS codmovimiento,
                    'ABO'::text AS tipomovimiento,
                    ccabono.fecha,
                    ccabono.nrodocumento AS numero,
                    ccabono.concepto AS descripcion
                   FROM ccabono) pagos
             JOIN (ccpagadas ccp
             JOIN (cuentasporcobrar cxc
             JOIN ( SELECT a_1.codventa AS codmovimiento,
                    'VENTAS'::text AS tipomovimiento,
                    a_1.numerofactura AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ventas a_1
                UNION
                 SELECT a_1.codccfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccfactura a_1
                UNION
                 SELECT a_1.codccnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccnotadebito a_1) a ON ((((a.codmovimiento = cxc.codmovimiento) AND (a.tipomovimiento = (cxc.tipomovimiento)::text)) AND (a.codsucursal = cxc.codsucursal)))) ON (((((ccp.tipomovimientocc)::text = (cxc.tipomovimiento)::text) AND (ccp.codmovimientocc = cxc.codmovimiento)) AND (cxc.codsucursal = ccp.codsucursalcc)))) ON (((ccp.codmovimientopago = pagos.codmovimiento) AND ((ccp.tipomovimientopago)::text = pagos.tipomovimiento))))
        UNION
         SELECT ccp.tipomovimientopago AS tipomovimiento,
            NULL::date AS fvencimiento,
            pagos.numero,
            pagos.descripcion AS concepto,
            sum(ccp.monto) AS debito,
            0 AS credito,
            pagos.fecha,
            a.codcliente,
            a.codclientesucursal
           FROM (( SELECT ccnotacredito.codccnotacredito AS codmovimiento,
                    'NC'::text AS tipomovimiento,
                    ccnotacredito.fecha,
                    ccnotacredito.nrodocumento AS numero,
                    ccnotacredito.concepto AS descripcion
                   FROM ccnotacredito
                UNION
                 SELECT ccabono.codccabono AS codmovimiento,
                    'PAG'::text AS tipomovimiento,
                    ccabono.fecha,
                    ccabono.nrodocumento AS numero,
                    ccabono.concepto AS descripcion
                   FROM ccabono
                UNION
                 SELECT ccabono.codccabono AS codmovimiento,
                    'ABO'::text AS tipomovimiento,
                    ccabono.fecha,
                    ccabono.nrodocumento AS numero,
                    ccabono.concepto AS descripcion
                   FROM ccabono) pagos
             JOIN (ccpagadas ccp
             JOIN (h_cuentasporcobrar hcxc
             JOIN ( SELECT a_1.codventa AS codmovimiento,
                    'VENTAS'::text AS tipomovimiento,
                    a_1.numerofactura AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ventas a_1
                UNION
                 SELECT a_1.codccfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccfactura a_1
                UNION
                 SELECT a_1.codccnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccnotadebito a_1) a ON ((((a.codmovimiento = hcxc.codmovimiento) AND (a.tipomovimiento = (hcxc.tipomovimiento)::text)) AND (a.codsucursal = hcxc.codsucursal)))) ON (((((ccp.tipomovimientocc)::text = (hcxc.tipomovimiento)::text) AND (ccp.codmovimientocc = hcxc.codmovimiento)) AND (hcxc.codsucursal = ccp.codsucursalcc)))) ON (((ccp.codmovimientopago = pagos.codmovimiento) AND ((ccp.tipomovimientopago)::text = pagos.tipomovimiento))))
          GROUP BY ccp.tipomovimientopago, pagos.numero, pagos.descripcion, pagos.fecha, a.codcliente, a.codclientesucursal) todo
  ORDER BY todo.numero;


ALTER TABLE public.v_estadodecuenta_cxc OWNER TO postgres;

--
-- TOC entry 400 (class 1259 OID 98547)
-- Name: v_formatoretencion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_formatoretencion AS
 SELECT c.femision AS fechafactura,
    c.nrodocumento AS numerofactura,
    c.nrocontrol AS numerocontrol,
    c.codcompra AS numeronotadebito,
    '' AS numeronotacredito,
    '1' AS tipotransaccion,
    '' AS numerofacturaafectada,
    ( SELECT sum((documentosretenciones.baseiva * ((1)::double precision + (documentosretenciones.poriva / (100)::double precision)))) AS sum
           FROM documentosretenciones
          WHERE ((documentosretenciones.codmovimiento = c.codcompra) AND ((documentosretenciones.tipomovimiento)::text = c.tipomovimiento))) AS totalincluyendoiva,
    COALESCE(( SELECT (documentosretenciones.baseiva * ((1)::double precision + (documentosretenciones.poriva / (100)::double precision)))
           FROM documentosretenciones
          WHERE (((documentosretenciones.codmovimiento = c.codcompra) AND ((documentosretenciones.tipomovimiento)::text = c.tipomovimiento)) AND (documentosretenciones.poriva = (0)::double precision))), (0)::double precision) AS comprassinderechoaiva,
    a.baseiva AS baseimponible,
    a.poriva AS porcentajeiva,
    (a.baseiva * (a.poriva / (100)::double precision)) AS iva,
    a.ivaretenido,
    b.codretencion
   FROM ((documentosretenciones a
     JOIN ( SELECT compras.femision,
            compras.nrodocumento,
            compras.nrocontrol,
            compras.codcompra,
            'COMPRAS'::text AS tipomovimiento
           FROM compras
        UNION
         SELECT cpnotadebito.fecha,
            cpnotadebito.nrodocumento,
            cpnotadebito.nrodocumento,
            cpnotadebito.codcpnotadebito,
            'ND'::text AS tipomovimiento
           FROM cpnotadebito) c ON ((((a.codmovimiento = c.codcompra) AND ((a.tipomovimiento)::text = c.tipomovimiento)) AND (a.poriva <> (0)::double precision))))
     JOIN retenciones b ON ((a.codretencion = b.codretencion)));


ALTER TABLE public.v_formatoretencion OWNER TO postgres;

--
-- TOC entry 401 (class 1259 OID 98552)
-- Name: v_historiadelproducto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historiadelproducto AS
 SELECT cg.descripcion,
    'CARGO'::text AS tipo,
    (cg.codcargo)::text AS numero,
    date_trunc('second'::text, ((((cg.fecha)::text || ' '::text) || (cg.hora)::text))::timestamp without time zone) AS fecha,
    pcg.coddeposito AS deposito,
    pcg.cantidad,
    pcg.unidad,
    (pcg.totalcosto / pcg.cantidad) AS costo,
    pcg.codproducto,
    pcg.factorconversion
   FROM (productoscargos pcg
     JOIN cargos cg ON (((pcg.codcargo = cg.codcargo) AND (pcg.codsucursal = cg.codsucursal))))
UNION
 SELECT dcg.descripcion,
    'DESCARGO'::text AS tipo,
    (dcg.coddescargo)::text AS numero,
    date_trunc('second'::text, ((((dcg.fecha)::text || ' '::text) || (dcg.hora)::text))::timestamp without time zone) AS fecha,
    pdcg.coddeposito AS deposito,
    (pdcg.cantidad * (- (1)::double precision)) AS cantidad,
    pdcg.unidad,
    (pdcg.totalcosto / pdcg.cantidad) AS costo,
    pdcg.codproducto,
    pdcg.factorconversion
   FROM (productosdescargos pdcg
     JOIN descargos dcg ON (((pdcg.coddescargo = dcg.coddescargo) AND (pdcg.codsucursal = dcg.codsucursal))))
UNION
 SELECT ('PROVEEDOR '::text || p.descripcion) AS descripcion,
    'COMPRA'::text AS tipo,
    c.nrodocumento AS numero,
    date_trunc('second'::text, ((((c.fregistro)::text || ' '::text) || (c.hora)::text))::timestamp without time zone) AS fecha,
    c.coddeposito AS deposito,
    cp.cantidad,
    cp.unidad,
    cp.costo,
    cp.codproducto,
    cp.factorconversion
   FROM (proveedores p
     JOIN (compras c
     JOIN comprasproductos cp ON ((c.codcompra = cp.codcompra))) ON ((p.codproveedor = c.codproveedor)))
UNION
 SELECT ('PROVEEDOR '::text || p.descripcion) AS descripcion,
    'DEVOLUCIÓN COMPRA'::text AS tipo,
    (d.coddevolucioncompra)::text AS numero,
    date_trunc('second'::text, ((((d.fdevolucion)::text || ' '::text) || (d.hora)::text))::timestamp without time zone) AS fecha,
    d.coddeposito AS deposito,
    (dp.cantidad * (- (1)::double precision)) AS cantidad,
    dp.unidad,
    dp.costo,
    dp.codproducto,
    dp.factorconversion
   FROM (devolucioncompraproductos dp
     JOIN (devolucioncompra d
     JOIN (compras c
     JOIN proveedores p ON ((c.codproveedor = p.codproveedor))) ON ((d.codcompra = c.codcompra))) ON ((dp.coddevolucioncompra = d.coddevolucioncompra)))
UNION
 SELECT ('CLIENTE '::text || p.descripcion) AS descripcion,
    'VENTA'::text AS tipo,
    c.numerofactura AS numero,
    date_trunc('second'::text, ((((c.fecha)::text || ' '::text) || (c.hora)::text))::timestamp without time zone) AS fecha,
    c.coddeposito AS deposito,
    (cp.cantidad * (- (1)::double precision)) AS cantidad,
    cp.unidad,
    cp.precio AS costo,
    cp.codproducto,
    cp.factorconversion
   FROM (clientes p
     JOIN (ventas c
     JOIN ventasproductos cp ON ((c.codventa = cp.codventa))) ON ((p.codcliente = c.codcliente)))
UNION
 SELECT ('CLIENTE '::text || c.descripcion) AS descripcion,
    'DEVOLUCIÓN VENTA'::text AS tipo,
    (d.coddevolucion)::text AS numero,
    date_trunc('second'::text, d.fecha) AS fecha,
    d.coddeposito AS deposito,
    dp.cantidad,
    vp.unidad,
    vp.precio AS costo,
    vp.codproducto,
    vp.factorconversion
   FROM (clientes c
     JOIN (ventas v
     JOIN (ventasproductos vp
     JOIN (devolucionventa d
     JOIN devolucionventaproductos dp ON (((d.coddevolucion = dp.coddevolucion) AND (d.codsucursal = dp.codsucursal)))) ON ((((vp.codventa = d.codventa) AND (vp.codsucursal = d.codsucursal)) AND (vp.codproducto = dp.codproducto)))) ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal)))) ON (((c.codcliente = v.codcliente) AND (c.codsucursal = v.codclientesucursal))))
UNION
 SELECT ('CLIENTE '::text || p.descripcion) AS descripcion,
    'NOTA ENTREGA'::text AS tipo,
    (c.codnotaentrega)::text AS numero,
    date_trunc('second'::text, ((((c.fecha)::text || ' '::text) || (c.hora)::text))::timestamp without time zone) AS fecha,
    c.coddeposito AS deposito,
    (cp.cantidad * (- (1)::double precision)) AS cantidad,
    cp.unidad,
    cp.precio AS costo,
    cp.codproducto,
    cp.factorconversion
   FROM (clientes p
     JOIN (nota_entrega c
     JOIN nota_entregaproductos cp ON ((c.codnotaentrega = cp.codnotaentrega))) ON ((p.codcliente = c.codcliente)));


ALTER TABLE public.v_historiadelproducto OWNER TO postgres;

--
-- TOC entry 402 (class 1259 OID 98557)
-- Name: v_historialcompras; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historialcompras AS
 SELECT c.codcompra,
    c.fregistro,
    c.nrodocumento,
    c.codproveedor,
    cp.totalcosto,
    cp.total,
    (((pag.caja + pag.cajachica) + pag.cheque) + pag.transaccion) AS contado,
    pag.credito
   FROM (( SELECT compraspagos.codcompra,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CAJA'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS caja,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CAJACHICA'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS cajachica,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CHEQUE'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'TRANSACCION'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS transaccion,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CREDITO'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS credito
           FROM compraspagos
          GROUP BY compraspagos.codcompra) pag
     JOIN (( SELECT cp_1.codcompra,
            COALESCE(sum((cp_1.costo * cp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum(((cp_1.costo * cp_1.cantidad) * ((1)::real + (cp_1.iva / (100)::real)))), (0)::real) AS total
           FROM comprasproductos cp_1
          GROUP BY cp_1.codcompra) cp
     JOIN compras c ON ((c.codcompra = cp.codcompra))) ON ((pag.codcompra = c.codcompra)));


ALTER TABLE public.v_historialcompras OWNER TO postgres;

--
-- TOC entry 403 (class 1259 OID 98562)
-- Name: v_historialdevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historialdevolucion AS
 SELECT dv.coddevolucion,
    dv.codsucursal,
    dv.fecha,
    dv.codventa,
    dp.codcliente,
    dp.total,
    (((((pag.efectivo + pag.tdebito) + pag.cheque) + pag.transfer) + pag.tcredito) + pag.ticket) AS contado,
    pag.credito
   FROM (( SELECT devolucionventapago.coddevolucion,
            devolucionventapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CREDITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'EFECTIVO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TDEBITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS tdebito,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CHEQUE'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TRANSFER'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TCREDITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS tcredito,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TICKET'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS ticket
           FROM devolucionventapago
          GROUP BY devolucionventapago.coddevolucion, devolucionventapago.codsucursal) pag
     JOIN (( SELECT dp_1.coddevolucion,
            dp_1.codsucursal,
            v.codcliente,
            COALESCE(sum((vp.costopromedio * dp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp.precio * dp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp.precio * dp_1.cantidad) * ((1)::real + (vp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp.precio * dp_1.cantidad) * (vp.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM (((devolucionventaproductos dp_1
             JOIN devolucionventa dv_1 ON ((dp_1.coddevolucion = dv_1.coddevolucion)))
             JOIN ventas v ON ((v.codventa = dv_1.codventa)))
             JOIN ventasproductos vp ON (((vp.codventa = dv_1.codventa) AND (dp_1.codproducto = vp.codproducto))))
          GROUP BY dp_1.coddevolucion, dp_1.codsucursal, v.codcliente
          ORDER BY dp_1.coddevolucion) dp
     JOIN devolucionventa dv ON (((dv.coddevolucion = dp.coddevolucion) AND (dv.codsucursal = dp.codsucursal)))) ON (((pag.coddevolucion = dv.coddevolucion) AND (dv.codsucursal = pag.codsucursal))));


ALTER TABLE public.v_historialdevolucion OWNER TO postgres;

--
-- TOC entry 404 (class 1259 OID 98567)
-- Name: v_historialdevolucioncompra; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historialdevolucioncompra AS
 SELECT dc.coddevolucioncompra,
    dc.codsucursal,
    dc.fdevolucion,
    dc.codcompra,
    dp.codproveedor,
    dp.total,
    (((((pag.efectivo + pag.tdebito) + pag.cheque) + pag.transfer) + pag.tcredito) + pag.ticket) AS contado,
    pag.credito
   FROM (( SELECT devolucioncomprapago.coddevolucioncompra,
            devolucioncomprapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'CREDITO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'EFECTIVO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'TDEBITO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS tdebito,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'CHEQUE'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'TRANSFER'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'TCREDITO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS tcredito,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'TICKET'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS ticket
           FROM devolucioncomprapago
          GROUP BY devolucioncomprapago.coddevolucioncompra, devolucioncomprapago.codsucursal) pag
     JOIN (( SELECT dp_1.coddevolucioncompra,
            c.codproveedor,
            COALESCE(sum((cp.costo * dp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((cp.costo * dp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((cp.costo * dp_1.cantidad) * ((1)::real + (cp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((cp.costo * dp_1.cantidad) * (cp.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM (((devolucioncompraproductos dp_1
             JOIN devolucioncompra dc_1 ON ((dp_1.coddevolucioncompra = dc_1.coddevolucioncompra)))
             JOIN compras c ON ((c.codcompra = dc_1.codcompra)))
             JOIN comprasproductos cp ON (((cp.codcompra = dc_1.codcompra) AND (dp_1.codproducto = cp.codproducto))))
          GROUP BY dp_1.coddevolucioncompra, c.codproveedor
          ORDER BY dp_1.coddevolucioncompra) dp
     JOIN devolucioncompra dc ON ((dc.coddevolucioncompra = dp.coddevolucioncompra))) ON (((pag.coddevolucioncompra = dc.coddevolucioncompra) AND (dc.codsucursal = pag.codsucursal))));


ALTER TABLE public.v_historialdevolucioncompra OWNER TO postgres;

--
-- TOC entry 405 (class 1259 OID 98572)
-- Name: v_historialventas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historialventas AS
 SELECT v.codventa,
    v.codsucursal,
    v.fecha,
    v.numerofactura,
    v.codcliente,
    vp.total,
    (((((pag.efectivo + pag.tdebito) + pag.cheque) + pag.transfer) + pag.tcredito) + pag.ticket) AS contado,
    pag.credito,
    v.estatus
   FROM (( SELECT ventaspagos.codventa,
            ventaspagos.codsucursal,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'EFECTIVO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TDEBITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS tdebito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CHEQUE'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TRANSFER'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TCREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS tcredito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TICKET'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS ticket
           FROM ventaspagos
          GROUP BY ventaspagos.codventa, ventaspagos.codsucursal) pag
     JOIN (( SELECT vp_1.codventa,
            vp_1.codsucursal,
            COALESCE(sum((vp_1.costopromedio * vp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp_1.precio * vp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * ((1)::real + (vp_1.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * (vp_1.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM ventasproductos vp_1
          GROUP BY vp_1.codventa, vp_1.codsucursal) vp
     JOIN ventas v ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal)))) ON (((pag.codventa = v.codventa) AND (v.codsucursal = pag.codsucursal))));


ALTER TABLE public.v_historialventas OWNER TO postgres;

--
-- TOC entry 448 (class 1259 OID 131816)
-- Name: v_iva; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_iva AS
 SELECT iva.codiva AS "Código_int",
    iva.porcentaje AS "Porcentaje_float",
    iva.descripcion AS "Descripción_varchar"
   FROM iva;


ALTER TABLE public.v_iva OWNER TO postgres;

--
-- TOC entry 406 (class 1259 OID 98581)
-- Name: v_maquinas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_maquinas AS
 SELECT maquinas.codmaquina AS "Código_int",
    maquinas.marca AS "Marca_varchar",
    maquinas.modelo AS "Modelo_text",
    maquinas.serial AS "Serial_text",
        CASE
            WHEN (maquinas.estatus = true) THEN 'ACTIVO'::text
            ELSE 'NO ACTIVO'::text
        END AS "Estatus_text"
   FROM maquinas;


ALTER TABLE public.v_maquinas OWNER TO postgres;

--
-- TOC entry 407 (class 1259 OID 98585)
-- Name: v_marca; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_marca AS
 SELECT marca.codmarca AS "Código_int",
    marca.descripcion AS "Descripción_varchar"
   FROM marca;


ALTER TABLE public.v_marca OWNER TO postgres;

--
-- TOC entry 408 (class 1259 OID 98589)
-- Name: v_movimientoz; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_movimientoz AS
 SELECT movimiento_z.codmovimiento AS "Código_int",
    movimiento_z.ultimafactura AS "UltimaFactura_int",
    movimiento_z.memoria AS "Memoria_int",
    movimiento_z.totalventa AS "Totalventa_real"
   FROM movimiento_z
  WHERE (movimiento_z.estatus = true);


ALTER TABLE public.v_movimientoz OWNER TO postgres;

--
-- TOC entry 409 (class 1259 OID 98593)
-- Name: v_notacredito_compras; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_notacredito_compras AS
 SELECT (((a.codnotacredito)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Proveedor_varchar",
    a.facturaafecta AS "Factura Afectada_varchar",
    a.fecha AS "Fecha_date",
    b.codproveedor AS "Código Proveedor_varchar",
    a.montobruto AS "Monto_Double"
   FROM (notacredito_compras a
     JOIN proveedores b ON ((a.codproveedor = b.codproveedor)));


ALTER TABLE public.v_notacredito_compras OWNER TO postgres;

--
-- TOC entry 410 (class 1259 OID 98598)
-- Name: v_notacredito_ventas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_notacredito_ventas AS
 SELECT (((a.codnotacredito)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.facturaafecta AS "Factura Afectada_varchar",
    a.fecha AS "Fecha_date",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.montobruto AS "Monto_Double"
   FROM (notacredito_ventas a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codsucursal = b.codsucursal))));


ALTER TABLE public.v_notacredito_ventas OWNER TO postgres;

--
-- TOC entry 411 (class 1259 OID 98603)
-- Name: v_notaentrega; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_notaentrega AS
 SELECT (((a.codnotaentrega)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.fecha AS "Fecha_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.codvendedor AS "Vendedor_varchar",
    'V' AS ctid_varchar
   FROM (nota_entrega a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))))
  WHERE ((a.estatus)::text <> 'E'::text);


ALTER TABLE public.v_notaentrega OWNER TO postgres;

--
-- TOC entry 412 (class 1259 OID 98608)
-- Name: v_pedido; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_pedido AS
 SELECT (((a.codpedido)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.fecha AS "FechaCreacion_date",
    a.fechavencimiento AS "FechaVencimiento_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar"
   FROM (pedido a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))))
  WHERE ((a.estatus)::text <> 'E'::text);


ALTER TABLE public.v_pedido OWNER TO postgres;

--
-- TOC entry 413 (class 1259 OID 98613)
-- Name: v_presupuesto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_presupuesto AS
 SELECT (((a.codpresupuesto)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.fecha AS "FechaCreacion_date",
    a.fechavencimiento AS "FechaVencimiento_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.empresa AS "Empresa_varchar"
   FROM (presupuesto a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))));


ALTER TABLE public.v_presupuesto OWNER TO postgres;

--
-- TOC entry 446 (class 1259 OID 131806)
-- Name: v_producto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_producto AS
 SELECT producto.codigo AS "Código_varchar",
    producto.nombre AS "Nombre_varchar",
    departamentos.descripcion AS "Departamento_text",
    marca.descripcion AS "Marca_varchar",
    producto.modelo AS "Modelo_varchar",
    producto.referencia AS "Referencia_varchar",
    (((iva.porcentaje)::text || '% '::text) || (iva.descripcion)::text) AS "Iva_varchar",
    producto.estado AS "Estado_varchar",
    producto.stockminimo AS "Stock Mínimo_varchar",
    producto.stockmaximo AS "Stock Máximo_varchar",
    producto.foto AS "Foto_varchar",
    producto.unidadprincipal AS "Unidad Principal_varchar",
    producto.unidadalterna AS "Unidad Alterna_varchar",
    producto.factorconversion AS "Factor de Conversion_varchar",
    producto.costoactual AS "Costo Actual_varchar",
    producto.costopromedio AS "Costo Promedio_varchar",
    producto.costoanterior AS "Costo Anterior_varchar",
    producto.preciominimo AS "Precio Mínimo_varchar",
    producto.preciomayor AS "Precio Mayor_varchar",
    producto.preciodetal AS "Precio Detal_varchar",
    producto.preciominimo1 AS "Prec. Min. con IVA_varchar",
    producto.preciomayor1 AS "Prec. May. con IVA_varchar",
    producto.preciodetal1 AS "Prec. Det. con IVA_varchar",
    producto.numero AS "Número_varchar",
    producto.fecha AS "Fecha_varchar",
    producto.texto AS "Texto_varchar",
    producto."varchar" AS "Varchar_varchar",
    producto."float" AS float_varchar,
    producto.codbarra AS "Código de Barras_varchar"
   FROM (((producto
     JOIN departamentos ON ((producto.coddepartamento = departamentos.coddepartamento)))
     JOIN iva ON ((producto.codiva = iva.codiva)))
     JOIN marca ON (((producto.codmarca = marca.codmarca) AND (NOT ((producto.estado)::text = 'INACTIVO'::text)))));


ALTER TABLE public.v_producto OWNER TO postgres;

--
-- TOC entry 447 (class 1259 OID 131811)
-- Name: v_producto_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_producto_i AS
 SELECT producto.codproducto AS "Código_int",
    producto.nombre AS "Nombre_int",
    departamentos.descripcion AS "Departamento_text",
    marca.descripcion AS "Marca_varchar",
    producto.modelo AS "Modelo_varchar",
    producto.referencia AS "Referencia_varchar",
    iva.descripcion AS "Iva_varchar",
    producto.estado AS "Estado_varchar",
    producto.stockminimo AS "Stock Mínimo_varchar",
    producto.stockmaximo AS "Stock Máximo_varchar",
    producto.foto AS "Foto_varchar",
    producto.unidadprincipal AS "Unidad Principal_varchar",
    producto.unidadalterna AS "Unidad Alterna_varchar",
    producto.factorconversion AS "Factor de Conversion_varchar",
    producto.costoactual AS "Costo Actual_varchar",
    producto.costopromedio AS "Costo Promedio_varchar",
    producto.costoanterior AS "Costo Anterior_varchar",
    producto.preciominimo AS "Precio Mínimo_varchar",
    producto.preciomayor AS "Precio Mayor_varchar",
    producto.preciodetal AS "Precio Detal_varchar",
    producto.preciominimo1 AS "Prec. Min. con IVA_varchar",
    producto.preciomayor1 AS "Prec. May. con IVA_varchar",
    producto.preciodetal1 AS "Prec. Det. con IVA_varchar",
    producto.numero AS "Número_varchar",
    producto.fecha AS "Fecha_varchar",
    producto.texto AS "Texto_varchar",
    producto."varchar" AS "Varchar_varchar",
    producto."float" AS float_varchar,
    producto.codbarra AS "Código de Barras_varchar"
   FROM (((producto
     JOIN departamentos ON ((producto.coddepartamento = departamentos.coddepartamento)))
     JOIN iva ON ((producto.codiva = iva.codiva)))
     JOIN marca ON (((producto.codmarca = marca.codmarca) AND ((producto.estado)::text = 'INACTIVO'::text))));


ALTER TABLE public.v_producto_i OWNER TO postgres;

--
-- TOC entry 414 (class 1259 OID 98628)
-- Name: v_proveedor; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_proveedor AS
 SELECT proveedores.codproveedor AS "Código_varchar",
    proveedores.descripcion AS "Descripción_text",
    proveedores.rif AS "RIF_varchar",
    proveedores.clase AS "Clase_varchar",
    proveedores.direccion AS "Dirección_text",
    proveedores.direccion2 AS "Dirección 2_text",
    proveedores.telefonos AS "Teléfonos_varchar",
    proveedores.email AS "E-mail_text",
    proveedores.numerodefax AS "Número de fax_varchar",
    proveedores.diasdecredito AS "Días de crédito_int",
    proveedores.limitedecredito AS "Límite de Crédito_float",
    proveedores.formadepago AS "Forma de pago_varchar",
    proveedores.porcderetencion AS "% de Retención_int",
    proveedores.fechadeinicio AS "Fecha de Inicio_date",
    proveedores.tipoproveedor AS "Tipo Proveedor_varchar"
   FROM proveedores
  WHERE (proveedores.estatus = true);


ALTER TABLE public.v_proveedor OWNER TO postgres;

--
-- TOC entry 415 (class 1259 OID 98632)
-- Name: v_proveedor_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_proveedor_i AS
 SELECT proveedores.codproveedor AS "Código_varchar",
    proveedores.descripcion AS "Descripción_text",
    proveedores.rif AS "RIF_varchar",
    proveedores.clase AS "Clase_varchar",
    proveedores.direccion AS "Dirección_text",
    proveedores.direccion2 AS "Dirección 2_text",
    proveedores.telefonos AS "Teléfonos_varchar",
    proveedores.email AS "E-mail_text",
    proveedores.numerodefax AS "Número de fax_varchar",
    proveedores.diasdecredito AS "Días de crédito_int",
    proveedores.limitedecredito AS "Límite de Crédito_float",
    proveedores.formadepago AS "Forma de pago_varchar",
    proveedores.porcderetencion AS "% de Retención_int",
    proveedores.fechadeinicio AS "Fecha de Inicio_date",
    proveedores.tipoproveedor AS "Tipo Proveedor_varchar"
   FROM proveedores
  WHERE (proveedores.estatus = false);


ALTER TABLE public.v_proveedor_i OWNER TO postgres;

--
-- TOC entry 416 (class 1259 OID 98636)
-- Name: v_puntodeventa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_puntodeventa AS
 SELECT puntodeventa.codpuntoventa AS "Código_int",
    bancos.nombre AS "Nombre_text",
    puntodeventa.serial AS "Serial_varchar",
    puntodeventa.nrodecuenta AS "Nrodecuenta_text",
    puntodeventa.marca AS "Marca_text",
    puntodeventa.modelo AS "Modelo_text"
   FROM puntodeventa,
    bancos
  WHERE (puntodeventa.codbanco = bancos.codbanco);


ALTER TABLE public.v_puntodeventa OWNER TO postgres;

--
-- TOC entry 417 (class 1259 OID 98640)
-- Name: v_rep_cxc_general; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_rep_cxc_general AS
 SELECT (((a.codcliente)::text || '-'::text) || (a.codsucursal)::text) AS codcliente,
    a.descripcion AS nombrecliente,
    a.direccion AS direccion1,
    a.direccion2,
    a.telefonos AS telefono,
    a.tipomovimiento,
    a.fecha,
    b.fvencimiento,
    '0' AS dias,
    a.documento AS numero,
    b.monto
   FROM (( SELECT a_1.codventa AS codmovimiento,
            'VENTAS'::text AS tipomovimiento,
            a_1.numerofactura AS documento,
            a_1.fecha,
            a_1.codcliente,
            a_1.codsucursal,
            a_1.codclientesucursal,
            ( SELECT clientes.descripcion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS descripcion,
            ( SELECT clientes.direccion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion,
            ( SELECT clientes.direccion2
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion2,
            ( SELECT clientes.telefonos
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS telefonos
           FROM ventas a_1
        UNION
         SELECT a_1.codccfactura AS codmovimiento,
            'FA'::text AS tipomovimiento,
            a_1.nrodocumento AS documento,
            a_1.fecha,
            a_1.codcliente,
            a_1.codsucursal,
            a_1.codclientesucursal,
            ( SELECT clientes.descripcion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS descripcion,
            ( SELECT clientes.direccion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion,
            ( SELECT clientes.direccion2
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion2,
            ( SELECT clientes.telefonos
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS telefonos
           FROM ccfactura a_1) a
     JOIN cuentasporcobrar b ON ((((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text)) AND (a.codsucursal = b.codsucursal))))
  ORDER BY a.descripcion;


ALTER TABLE public.v_rep_cxc_general OWNER TO postgres;

--
-- TOC entry 418 (class 1259 OID 98645)
-- Name: v_retencion_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_retencion_v AS
 SELECT retenciones."Código_varchar",
    retenciones."Cliente_varchar",
    retenciones."Factura_text",
    retenciones."Monto_float",
    retenciones."Fecha_varchar"
   FROM ( SELECT rv.codretencion AS "Código_varchar",
            c.descripcion AS "Cliente_varchar",
            rv.documento AS "Factura_text",
            rv.montoretenido AS "Monto_float",
            r.fechaemision AS "Fecha_varchar"
           FROM ((retenciones_ventas_detalles rv
             JOIN retenciones_ventas r ON (((r.codretencion = rv.codretencion) AND ((r.estatus)::text <> 'E'::text))))
             JOIN clientes c ON ((c.codcliente = rv.codcliente)))
          ORDER BY r.fechaemision, rv.documento) retenciones
  ORDER BY retenciones."Cliente_varchar";


ALTER TABLE public.v_retencion_v OWNER TO postgres;

--
-- TOC entry 419 (class 1259 OID 98650)
-- Name: v_sucursal; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_sucursal AS
 SELECT sucursal.codsucursal AS "Código_int",
    sucursal.descripcion AS "Descripción_text",
    sucursal.ip AS "IP_varchar",
    sucursal.direccion AS "Dirección_text",
    sucursal.telefonos AS "Teléfonos_varchar"
   FROM sucursal
  WHERE (sucursal.estatus = true);


ALTER TABLE public.v_sucursal OWNER TO postgres;

--
-- TOC entry 420 (class 1259 OID 98654)
-- Name: v_sucursal_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_sucursal_i AS
 SELECT sucursal.codsucursal AS "Código_int",
    sucursal.descripcion AS "Descripción_text",
    sucursal.ip AS "IP_varchar",
    sucursal.direccion AS "Dirección_text",
    sucursal.telefonos AS "Teléfonos_varchar"
   FROM sucursal
  WHERE (sucursal.estatus = false);


ALTER TABLE public.v_sucursal_i OWNER TO postgres;

--
-- TOC entry 421 (class 1259 OID 98658)
-- Name: v_tipocomision; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_tipocomision AS
 SELECT tipocomision.id AS "Código_int",
    tipocomision.nombre AS "Nombre_varchar",
    tipocomision.abreviatura AS "Abreviatura_text",
    tipocomision.estatus AS "Estatus_bool"
   FROM tipocomision;


ALTER TABLE public.v_tipocomision OWNER TO postgres;

--
-- TOC entry 422 (class 1259 OID 98662)
-- Name: v_tipocomisiondef; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_tipocomisiondef AS
 SELECT tipocomisiondef.id AS "Código_int",
    tipocomisiondef.nombre AS "Nombre_varchar",
    tipocomisiondef.estatus AS "Estatus_bool",
    tipocomision.nombre AS "Comision_varchar"
   FROM (tipocomisiondef
     JOIN tipocomision ON ((tipocomision.id = tipocomisiondef.tipocomision_id)));


ALTER TABLE public.v_tipocomisiondef OWNER TO postgres;

--
-- TOC entry 423 (class 1259 OID 98666)
-- Name: v_tipoexcepcion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_tipoexcepcion AS
 SELECT tipoexcepcion.id AS "Código_int",
    tipoexcepcion.nombre AS "Nombre_varchar",
    tipoexcepcion.abreviatura AS "Abreviatura_text",
    tipoexcepcion.estatus AS "Estatus_bool"
   FROM tipoexcepcion;


ALTER TABLE public.v_tipoexcepcion OWNER TO postgres;

--
-- TOC entry 424 (class 1259 OID 98670)
-- Name: v_tipoexcepciondef; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_tipoexcepciondef AS
 SELECT tipoexcepciondef.id AS "Código_int",
    tipoexcepciondef.nombre AS "Nombre_varchar",
    tipoexcepciondef.estatus AS "Estatus_bool",
    tipoexcepcion.nombre AS "Excepcion_varchar"
   FROM (tipoexcepciondef
     JOIN tipoexcepcion ON ((tipoexcepcion.id = tipoexcepciondef.tipoexcepcion_id)));


ALTER TABLE public.v_tipoexcepciondef OWNER TO postgres;

--
-- TOC entry 425 (class 1259 OID 98674)
-- Name: v_vendedor; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_vendedor AS
 SELECT (((vendedores.codvendedor)::text || '-'::text) || (vendedores.codsucursal)::text) AS "Código_varchar",
    vendedores.descripcion AS "Descripción_text",
    vendedores.clase AS "Clase_varchar",
    vendedores.nivelprecios2 AS "Nivel de Precios_varchar",
    vendedores.tabladeventas2 AS "Tabla de Ventas_varchar",
    vendedores.tabladecobros2 AS "Tabla de Cobros_varchar",
    vendedores.tabladeutilidad2 AS "Tabla de Utilidad_varchar",
    vendedores.diastolerancia2 AS "Días de Tolerancia_varchar"
   FROM vendedores
  WHERE ((vendedores.codsucursal = ( SELECT configuracion.sucursal
           FROM configuracion)) AND (vendedores.estatus = true));


ALTER TABLE public.v_vendedor OWNER TO postgres;

--
-- TOC entry 426 (class 1259 OID 98679)
-- Name: v_vendedor2; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_vendedor2 AS
 SELECT vendedores.codvendedor AS "Código_int",
    vendedores.descripcion AS "Descripción_text",
    vendedores.clase AS "Clase_varchar",
    vendedores.nivelprecios2 AS "Nivel de Precios_varchar",
    vendedores.tabladeventas2 AS "Tabla de Ventas_varchar",
    vendedores.tabladecobros2 AS "Tabla de Cobros_varchar",
    vendedores.tabladeutilidad2 AS "Tabla de Utilidad_varchar",
    vendedores.diastolerancia2 AS "Días de Tolerancia_varchar"
   FROM vendedores
  WHERE ((vendedores.codsucursal = ( SELECT configuracion.sucursal
           FROM configuracion)) AND (vendedores.estatus = true));


ALTER TABLE public.v_vendedor2 OWNER TO postgres;

--
-- TOC entry 427 (class 1259 OID 98683)
-- Name: v_vendedor_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_vendedor_i AS
 SELECT vendedores.codvendedor AS "Código_int",
    vendedores.descripcion AS "Descripción_text",
    vendedores.clase AS "Clase_varchar",
    vendedores.nivelprecios2 AS "Nivel de Precios_varchar",
    vendedores.tabladeventas2 AS "Tabla de Ventas_varchar",
    vendedores.tabladecobros2 AS "Tabla de Cobros_varchar",
    vendedores.tabladeutilidad2 AS "Tabla de Utilidad_varchar",
    vendedores.diastolerancia2 AS "Días de Tolerancia_varchar"
   FROM vendedores
  WHERE ((vendedores.codsucursal = ( SELECT configuracion.sucursal
           FROM configuracion)) AND (vendedores.estatus = false));


ALTER TABLE public.v_vendedor_i OWNER TO postgres;

--
-- TOC entry 428 (class 1259 OID 98687)
-- Name: ventaespera; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ventaespera (
    codventaespera integer NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codcliente integer NOT NULL,
    codclientesucursal integer NOT NULL,
    coddeposito integer NOT NULL,
    hora time(6) without time zone DEFAULT ('now'::text)::time with time zone NOT NULL
);


ALTER TABLE public.ventaespera OWNER TO postgres;

--
-- TOC entry 429 (class 1259 OID 98692)
-- Name: v_ventaespera; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventaespera AS
 SELECT a.codventaespera AS "Código_int",
    a.fecha AS "Fecha_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    b.descripcion AS "Cliente_varchar"
   FROM (ventaespera a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))));


ALTER TABLE public.v_ventaespera OWNER TO postgres;

--
-- TOC entry 442 (class 1259 OID 115457)
-- Name: v_ventas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventas AS
 SELECT (((a.codventa)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.numerofactura AS "Factura_varchar",
    a.fecha AS "Fecha_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.codvendedor AS "Vendedor_varchar",
    0 AS ctid_int
   FROM (ventas a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))));


ALTER TABLE public.v_ventas OWNER TO postgres;

--
-- TOC entry 430 (class 1259 OID 98702)
-- Name: v_ventas2; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventas2 AS
 SELECT (((a.codventa)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.numerofactura AS "Factura_varchar",
    a.fecha AS "Fecha_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.codvendedor AS "Vendedor_varchar",
    0 AS ctid
   FROM (ventas a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))));


ALTER TABLE public.v_ventas2 OWNER TO postgres;

--
-- TOC entry 431 (class 1259 OID 98707)
-- Name: v_ventas_vendedor; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventas_vendedor AS
 SELECT a.codventa,
    a.codsucursal,
    b.descripcion AS cliente,
    a.numerofactura,
    a.fecha,
    a.coddeposito,
    b.codcliente,
    a.codvendedor
   FROM (ventas a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))))
  WHERE ((a.estatus)::text <> 'E'::text);


ALTER TABLE public.v_ventas_vendedor OWNER TO postgres;

--
-- TOC entry 432 (class 1259 OID 98712)
-- Name: v_ventasfacturas_cxc; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventasfacturas_cxc AS
 SELECT (((a.codcliente)::text || '-'::text) || (a.codsucursal)::text) AS codcliente,
    a.descripcion,
    a.direccion,
    a.tipomovimiento,
    a.documento,
    b.fvencimiento,
    a.fecha,
    b.monto,
    a.codmovimiento,
    '' AS ctid
   FROM (( SELECT a_1.codventa AS codmovimiento,
            'VENTAS'::text AS tipomovimiento,
            a_1.numerofactura AS documento,
            a_1.fecha,
            a_1.codcliente,
            a_1.codsucursal,
            a_1.codclientesucursal,
            ( SELECT clientes.descripcion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS descripcion,
            ( SELECT clientes.direccion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion
           FROM ventas a_1
        UNION
         SELECT a_1.codccfactura AS codmovimiento,
            'FA'::text AS tipomovimiento,
            a_1.nrodocumento AS documento,
            a_1.fecha,
            a_1.codcliente,
            a_1.codsucursal,
            a_1.codclientesucursal,
            ( SELECT clientes.descripcion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS descripcion,
            ( SELECT clientes.direccion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion
           FROM ccfactura a_1) a
     JOIN cuentasporcobrar b ON ((((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text)) AND (a.codsucursal = b.codsucursal))))
  ORDER BY a.descripcion;


ALTER TABLE public.v_ventasfacturas_cxc OWNER TO postgres;

--
-- TOC entry 433 (class 1259 OID 98717)
-- Name: v_ventasposiblesparaanular; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventasposiblesparaanular AS
 SELECT v.codventa,
    v.codsucursal,
    v.numerofactura,
    v.fecha,
    v.codcliente,
    v.codvendedor,
    v.codclientesucursal,
    v.hora,
    v.coddeposito,
    c.rif,
    c.descripcion
   FROM (( SELECT ventas.codventa,
            ventas.codsucursal
           FROM ventas
        EXCEPT
         SELECT devolucionventa.codventa,
            devolucionventa.codsucursalventa AS codsucursal
           FROM devolucionventa) dv
     JOIN (ventas v
     JOIN clientes c ON (((v.codcliente = c.codcliente) AND (v.codclientesucursal = c.codsucursal)))) ON (((dv.codventa = v.codventa) AND (dv.codsucursal = v.codsucursal))));


ALTER TABLE public.v_ventasposiblesparaanular OWNER TO postgres;

--
-- TOC entry 4406 (class 0 OID 0)
-- Dependencies: 433
-- Name: VIEW v_ventasposiblesparaanular; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON VIEW v_ventasposiblesparaanular IS 'No se puede anular si ya se ha devuelto parte de la factura

No se puede anular si fué a crédito, y se ha cancelado alguna parte (FALTA)';


--
-- TOC entry 434 (class 1259 OID 98722)
-- Name: ventaespera_codventaespera_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ventaespera_codventaespera_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ventaespera_codventaespera_seq OWNER TO postgres;

--
-- TOC entry 4407 (class 0 OID 0)
-- Dependencies: 434
-- Name: ventaespera_codventaespera_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ventaespera_codventaespera_seq OWNED BY ventaespera.codventaespera;


--
-- TOC entry 435 (class 1259 OID 98724)
-- Name: ventaesperaproductos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE ventaesperaproductos (
    codventaespera integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL,
    precio real NOT NULL,
    unidad character varying(20) NOT NULL,
    iva real NOT NULL,
    factorconversion real NOT NULL,
    indice smallint NOT NULL
);


ALTER TABLE public.ventaesperaproductos OWNER TO postgres;

--
-- TOC entry 436 (class 1259 OID 98727)
-- Name: z_cambios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE z_cambios (
    codcambio integer NOT NULL,
    tabla character varying(50) NOT NULL,
    codsucursal integer NOT NULL
);


ALTER TABLE public.z_cambios OWNER TO postgres;

--
-- TOC entry 437 (class 1259 OID 98730)
-- Name: z_producto; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE z_producto (
    codproducto integer NOT NULL,
    codigo character varying(8),
    nombre text,
    coddepartamento integer,
    codmarca integer,
    modelo character varying(20),
    referencia character varying(15),
    codbarra character varying(14),
    codiva integer,
    estado character varying(10),
    stockminimo integer,
    stockmaximo integer,
    foto text,
    unidadprincipal character varying(20),
    unidadalterna character varying(20),
    factorconversion integer,
    costoactual real,
    costopromedio real,
    costoanterior real,
    preciominimo real,
    preciomayor real,
    preciodetal real,
    numero integer,
    fecha date,
    "varchar" character varying(20),
    texto text,
    "float" real,
    preciominimo1 real,
    preciomayor1 real,
    preciodetal1 real
);


ALTER TABLE public.z_producto OWNER TO postgres;

--
-- TOC entry 438 (class 1259 OID 98736)
-- Name: z_productosdepositos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE z_productosdepositos (
    z_codproductosdepositos integer NOT NULL,
    coddeposito integer NOT NULL,
    codproducto integer NOT NULL,
    cantidad real NOT NULL,
    z_estatus boolean DEFAULT false NOT NULL
);


ALTER TABLE public.z_productosdepositos OWNER TO postgres;

--
-- TOC entry 439 (class 1259 OID 98740)
-- Name: z_productosdepositos_z_codproductosdepositos_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE z_productosdepositos_z_codproductosdepositos_seq
    START WITH 4612
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.z_productosdepositos_z_codproductosdepositos_seq OWNER TO postgres;

--
-- TOC entry 4408 (class 0 OID 0)
-- Dependencies: 439
-- Name: z_productosdepositos_z_codproductosdepositos_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE z_productosdepositos_z_codproductosdepositos_seq OWNED BY z_productosdepositos.z_codproductosdepositos;


--
-- TOC entry 440 (class 1259 OID 98742)
-- Name: z_unidad_codunidad_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE z_unidad_codunidad_seq
    START WITH 48
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    CYCLE;


ALTER TABLE public.z_unidad_codunidad_seq OWNER TO postgres;

--
-- TOC entry 441 (class 1259 OID 98744)
-- Name: z_unidad; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE z_unidad (
    codproducto integer NOT NULL,
    codigo character varying(8),
    unidadprincipal character varying(20),
    unidadalterna character varying(20),
    factorconversion integer,
    operacion character varying(20),
    cantidad real,
    codsucursal integer NOT NULL,
    fecha date DEFAULT ('now'::text)::date NOT NULL,
    codunidad integer DEFAULT nextval('z_unidad_codunidad_seq'::regclass) NOT NULL,
    estatus character varying(1) DEFAULT 'T'::character varying NOT NULL
);


ALTER TABLE public.z_unidad OWNER TO postgres;

--
-- TOC entry 3825 (class 2604 OID 98750)
-- Name: codbanco; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bancos ALTER COLUMN codbanco SET DEFAULT nextval('bancos_codbanco_seq'::regclass);


--
-- TOC entry 3833 (class 2604 OID 98751)
-- Name: codccnotadebito; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ccnotadebito ALTER COLUMN codccnotadebito SET DEFAULT nextval('ccnotadebito_codccnotadebito_seq'::regclass);


--
-- TOC entry 3872 (class 2604 OID 98752)
-- Name: codcpabono; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cpabono ALTER COLUMN codcpabono SET DEFAULT nextval('cpabono_codcpabono_seq'::regclass);


--
-- TOC entry 3875 (class 2604 OID 98753)
-- Name: codcpfactura; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cpfactura ALTER COLUMN codcpfactura SET DEFAULT nextval('cpfactura_codcpfactura_seq'::regclass);


--
-- TOC entry 3879 (class 2604 OID 98754)
-- Name: codcpnotadebito; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cpnotadebito ALTER COLUMN codcpnotadebito SET DEFAULT nextval('cpnotadebito_codcpnotadebito_seq'::regclass);


--
-- TOC entry 4043 (class 2604 OID 133606)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cuadrediarios ALTER COLUMN id SET DEFAULT nextval('cuadrediarios_id_seq'::regclass);


--
-- TOC entry 3880 (class 2604 OID 98755)
-- Name: codcuenta; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cuentasbancarias ALTER COLUMN codcuenta SET DEFAULT nextval('cuentasbancarias_codcuenta_seq'::regclass);


--
-- TOC entry 3898 (class 2604 OID 98756)
-- Name: coddevolucion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY devolucionventa ALTER COLUMN coddevolucion SET DEFAULT nextval('devolucionventa_coddevolucion_seq'::regclass);


--
-- TOC entry 4067 (class 2604 OID 133724)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dinerodenominaciones ALTER COLUMN id SET DEFAULT nextval('dinerodenominaciones_id_seq'::regclass);


--
-- TOC entry 4048 (class 2604 OID 133607)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documentotipo ALTER COLUMN id SET DEFAULT nextval('documentotipo_id_seq'::regclass);


--
-- TOC entry 3907 (class 2604 OID 98757)
-- Name: codformato; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY formatos ALTER COLUMN codformato SET DEFAULT nextval('formatos_codformato_seq'::regclass);


--
-- TOC entry 3911 (class 2604 OID 98758)
-- Name: codfuncionformulario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY funcionesformularios ALTER COLUMN codfuncionformulario SET DEFAULT nextval('funcionesformularios_codfuncionformulario_seq'::regclass);


--
-- TOC entry 4042 (class 2604 OID 131800)
-- Name: codiva; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY iva ALTER COLUMN codiva SET DEFAULT nextval('iva_codiva_seq'::regclass);


--
-- TOC entry 4051 (class 2604 OID 133608)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY movbancarios ALTER COLUMN id SET DEFAULT nextval('movbancarios_id_seq'::regclass);


--
-- TOC entry 3942 (class 2604 OID 98760)
-- Name: codnumerodocumento; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY numerodocumento ALTER COLUMN codnumerodocumento SET DEFAULT nextval('numerodocumento_codnumerodocumento_seq'::regclass);


--
-- TOC entry 3946 (class 2604 OID 98761)
-- Name: codorigendatos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY origendatos ALTER COLUMN codorigendatos SET DEFAULT nextval('origendatos_codorigendatos_seq'::regclass);


--
-- TOC entry 3947 (class 2604 OID 98762)
-- Name: codvariable; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY origendatosvariables ALTER COLUMN codvariable SET DEFAULT nextval('origendatosvariables_codvariable_seq'::regclass);


--
-- TOC entry 3951 (class 2604 OID 98763)
-- Name: codpedido; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pedido ALTER COLUMN codpedido SET DEFAULT nextval('pedido_codpedido_seq'::regclass);


--
-- TOC entry 3956 (class 2604 OID 98764)
-- Name: codplanificacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY planificaciondepagos ALTER COLUMN codplanificacion SET DEFAULT nextval('planificaciondepagos_codplanificacion_seq'::regclass);


--
-- TOC entry 3957 (class 2604 OID 98765)
-- Name: codplanificacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY planificaciondepagosdetalles ALTER COLUMN codplanificacion SET DEFAULT nextval('planificaciondepagosdetalles_codplanificacion_seq'::regclass);


--
-- TOC entry 3977 (class 2604 OID 98766)
-- Name: codpuntoventa; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY puntodeventa ALTER COLUMN codpuntoventa SET DEFAULT nextval('puntodeventa_codpuntoventa_seq'::regclass);


--
-- TOC entry 3990 (class 2604 OID 98767)
-- Name: codretencion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY retenciones ALTER COLUMN codretencion SET DEFAULT nextval('retenciones_codretencion_seq'::regclass);


--
-- TOC entry 4057 (class 2604 OID 133609)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sucdepositos ALTER COLUMN id SET DEFAULT nextval('sucdepositos_id_seq'::regclass);


--
-- TOC entry 4071 (class 2604 OID 133769)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sucgastos ALTER COLUMN id SET DEFAULT nextval('sucgastos_id_seq'::regclass);


--
-- TOC entry 4007 (class 2604 OID 98768)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipocomision ALTER COLUMN id SET DEFAULT nextval('tipocomision_id_seq'::regclass);


--
-- TOC entry 4008 (class 2604 OID 98769)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipocomisiondef ALTER COLUMN id SET DEFAULT nextval('tipocomisiondef_id_seq'::regclass);


--
-- TOC entry 4010 (class 2604 OID 98770)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipocomisiondef_tablacomision ALTER COLUMN id SET DEFAULT nextval('tipocomisiondef_tablacomision_id_seq'::regclass);


--
-- TOC entry 4012 (class 2604 OID 98771)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipocomisiondef_vendedores ALTER COLUMN id SET DEFAULT nextval('tipocomisiondef_vendedores_id_seq'::regclass);


--
-- TOC entry 4020 (class 2604 OID 98772)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipoexcepcion ALTER COLUMN id SET DEFAULT nextval('tipoexcepcion_id_seq'::regclass);


--
-- TOC entry 4021 (class 2604 OID 98773)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipoexcepciondef ALTER COLUMN id SET DEFAULT nextval('tipoexcepciondef_id_seq'::regclass);


--
-- TOC entry 4023 (class 2604 OID 98774)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipoexcepciondef_tablacomision ALTER COLUMN id SET DEFAULT nextval('tipoexcepciondef_tablacomision_id_seq'::regclass);


--
-- TOC entry 4025 (class 2604 OID 98775)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipoexcepciondef_vendedores ALTER COLUMN id SET DEFAULT nextval('tipoexcepciondef_vendedores_id_seq'::regclass);


--
-- TOC entry 4026 (class 2604 OID 98776)
-- Name: codtipousuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tiposusuario ALTER COLUMN codtipousuario SET DEFAULT nextval('tiposusuario_codtipousuario_seq'::regclass);


--
-- TOC entry 4031 (class 2604 OID 98777)
-- Name: codusuario; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY usuarios ALTER COLUMN codusuario SET DEFAULT nextval('usuarios_codusuario_seq'::regclass);


--
-- TOC entry 4036 (class 2604 OID 98778)
-- Name: codventaespera; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ventaespera ALTER COLUMN codventaespera SET DEFAULT nextval('ventaespera_codventaespera_seq'::regclass);


--
-- TOC entry 4037 (class 2604 OID 98779)
-- Name: z_codproductosdepositos; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY z_productosdepositos ALTER COLUMN z_codproductosdepositos SET DEFAULT nextval('z_productosdepositos_z_codproductosdepositos_seq'::regclass);


--
-- TOC entry 4078 (class 2606 OID 98781)
-- Name: acceso_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY acceso
    ADD CONSTRAINT acceso_pkey PRIMARY KEY (codusuario, hora, accion);


--
-- TOC entry 4080 (class 2606 OID 98783)
-- Name: bancodeposito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bancodeposito
    ADD CONSTRAINT bancodeposito_pkey PRIMARY KEY (coddepbanco);


--
-- TOC entry 4082 (class 2606 OID 98785)
-- Name: bancos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY bancos
    ADD CONSTRAINT bancos_pkey PRIMARY KEY (codbanco);


--
-- TOC entry 4084 (class 2606 OID 98787)
-- Name: clientes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY clientes
    ADD CONSTRAINT clientes_pkey PRIMARY KEY (codcliente, codsucursal);


--
-- TOC entry 4106 (class 2606 OID 98789)
-- Name: codsucursal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sucursal
    ADD CONSTRAINT codsucursal_pkey PRIMARY KEY (codsucursal);


--
-- TOC entry 4086 (class 2606 OID 98851)
-- Name: configuracion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY configuracion
    ADD CONSTRAINT configuracion_pkey PRIMARY KEY (sucursal);


--
-- TOC entry 4088 (class 2606 OID 98791)
-- Name: cpnotacredito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cpnotacredito
    ADD CONSTRAINT cpnotacredito_pkey PRIMARY KEY (codcpnotacredito);


--
-- TOC entry 4090 (class 2606 OID 98793)
-- Name: cpnotadebito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cpnotadebito
    ADD CONSTRAINT cpnotadebito_pkey PRIMARY KEY (codcpnotadebito);


--
-- TOC entry 4128 (class 2606 OID 133611)
-- Name: cuadrediarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cuadrediarios
    ADD CONSTRAINT cuadrediarios_pkey PRIMARY KEY (id);


--
-- TOC entry 4092 (class 2606 OID 98855)
-- Name: depositos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY depositos
    ADD CONSTRAINT depositos_pkey PRIMARY KEY (coddeposito);


--
-- TOC entry 4136 (class 2606 OID 133726)
-- Name: dinerodenominaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dinerodenominaciones
    ADD CONSTRAINT dinerodenominaciones_pkey PRIMARY KEY (id);


--
-- TOC entry 4130 (class 2606 OID 133613)
-- Name: documentotipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY documentotipo
    ADD CONSTRAINT documentotipo_pkey PRIMARY KEY (id);


--
-- TOC entry 4094 (class 2606 OID 98795)
-- Name: estaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY estaciones
    ADD CONSTRAINT estaciones_pkey PRIMARY KEY (codestacion);


--
-- TOC entry 4126 (class 2606 OID 131805)
-- Name: iva_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY iva
    ADD CONSTRAINT iva_pkey PRIMARY KEY (codiva);


--
-- TOC entry 4132 (class 2606 OID 133615)
-- Name: movbancarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY movbancarios
    ADD CONSTRAINT movbancarios_pkey PRIMARY KEY (id);


--
-- TOC entry 4096 (class 2606 OID 133538)
-- Name: pkey_maquinas; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY maquinas
    ADD CONSTRAINT pkey_maquinas PRIMARY KEY (codmaquina, codsucursal);


--
-- TOC entry 4124 (class 2606 OID 115428)
-- Name: pkey_usuarios; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT pkey_usuarios PRIMARY KEY (codusuario);


--
-- TOC entry 4098 (class 2606 OID 98797)
-- Name: proveedores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY proveedores
    ADD CONSTRAINT proveedores_pkey PRIMARY KEY (codproveedor);


--
-- TOC entry 4100 (class 2606 OID 98799)
-- Name: puntodeventa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY puntodeventa
    ADD CONSTRAINT puntodeventa_pkey PRIMARY KEY (codpuntoventa);


--
-- TOC entry 4134 (class 2606 OID 133617)
-- Name: sucdepositos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sucdepositos
    ADD CONSTRAINT sucdepositos_pkey PRIMARY KEY (id);


--
-- TOC entry 4138 (class 2606 OID 133779)
-- Name: sucgastos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sucgastos
    ADD CONSTRAINT sucgastos_pkey PRIMARY KEY (id);


--
-- TOC entry 4108 (class 2606 OID 98801)
-- Name: tipocomision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipocomision
    ADD CONSTRAINT tipocomision_pkey PRIMARY KEY (id);


--
-- TOC entry 4110 (class 2606 OID 98803)
-- Name: tipocomisiondef_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipocomisiondef
    ADD CONSTRAINT tipocomisiondef_pkey PRIMARY KEY (id);


--
-- TOC entry 4112 (class 2606 OID 98805)
-- Name: tipocomisiondef_tablacomision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipocomisiondef_tablacomision
    ADD CONSTRAINT tipocomisiondef_tablacomision_pkey PRIMARY KEY (id);


--
-- TOC entry 4114 (class 2606 OID 98807)
-- Name: tipocomisiondef_vendedores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipocomisiondef_vendedores
    ADD CONSTRAINT tipocomisiondef_vendedores_pkey PRIMARY KEY (id);


--
-- TOC entry 4116 (class 2606 OID 98809)
-- Name: tipoexcepcion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipoexcepcion
    ADD CONSTRAINT tipoexcepcion_pkey PRIMARY KEY (id);


--
-- TOC entry 4118 (class 2606 OID 98811)
-- Name: tipoexcepciondef_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipoexcepciondef
    ADD CONSTRAINT tipoexcepciondef_pkey PRIMARY KEY (id);


--
-- TOC entry 4120 (class 2606 OID 98813)
-- Name: tipoexcepciondef_tablacomision_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipoexcepciondef_tablacomision
    ADD CONSTRAINT tipoexcepciondef_tablacomision_pkey PRIMARY KEY (id);


--
-- TOC entry 4122 (class 2606 OID 98815)
-- Name: tipoexcepciondef_vendedores_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipoexcepciondef_vendedores
    ADD CONSTRAINT tipoexcepciondef_vendedores_pkey PRIMARY KEY (id);


--
-- TOC entry 4102 (class 2606 OID 98817)
-- Name: ventas_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ventas
    ADD CONSTRAINT ventas_pkey PRIMARY KEY (codventa, codsucursal);


--
-- TOC entry 4104 (class 2606 OID 98819)
-- Name: ventaspagos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ventaspagos
    ADD CONSTRAINT ventaspagos_pkey PRIMARY KEY (codventa, codsucursal, tipopago);


--
-- TOC entry 4139 (class 2606 OID 98820)
-- Name: cpnotacredito_codproveedor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cpnotacredito
    ADD CONSTRAINT cpnotacredito_codproveedor_fkey FOREIGN KEY (codproveedor) REFERENCES proveedores(codproveedor) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 4140 (class 2606 OID 98825)
-- Name: puntodeventa_codbanco_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY puntodeventa
    ADD CONSTRAINT puntodeventa_codbanco_fkey FOREIGN KEY (codbanco) REFERENCES bancos(codbanco);


--
-- TOC entry 4141 (class 2606 OID 98830)
-- Name: ventas_codcliente_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ventas
    ADD CONSTRAINT ventas_codcliente_fkey FOREIGN KEY (codcliente, codclientesucursal) REFERENCES clientes(codcliente, codsucursal) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 4142 (class 2606 OID 98835)
-- Name: ventaspago_codventa_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ventaspagos
    ADD CONSTRAINT ventaspago_codventa_fkey FOREIGN KEY (codventa, codsucursal) REFERENCES ventas(codventa, codsucursal) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 4326 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-03-26 16:03:14

--
-- PostgreSQL database dump complete
--

