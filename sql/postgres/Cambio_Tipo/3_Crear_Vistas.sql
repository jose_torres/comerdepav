CREATE VIEW rep_despachos AS
 SELECT dp.nrodocumento AS nro,
    dp.codchofer,
    ch.nombre,
    ch.vehiculoasignado,
    ((((date_part('day'::text, dp.fechaemision) || '-'::text) || date_part('month'::text, dp.fechaemision)) || '-'::text) || date_part('year'::text, dp.fechaemision)) AS fecha,
    dp.cliente,
    dp.monto,
    ((((date_part('day'::text, dp.fechadespacho) || '-'::text) || date_part('month'::text, dp.fechadespacho)) || '-'::text) || date_part('year'::text, dp.fechadespacho)) AS fecha_despacho,
    dp.turno,
    dp.tipo,
    date_part('month'::text, dp.fechadespacho) AS mes_despacho,
    date_part('year'::text, dp.fechadespacho) AS ano_despacho,
    dp.fechaemision,
    dp.fechadespacho AS f_despacho
   FROM (despachos dp
     LEFT JOIN choferes ch ON ((ch.codchofer = dp.codchofer)))
  ORDER BY dp.codchofer, dp.fechaemision;


ALTER TABLE public.rep_despachos OWNER TO postgres;

--
-- TOC entry 325 (class 1259 OID 98225)
-- Name: rep_inv_existencia; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rep_inv_existencia AS
 SELECT (p.codigo)::text AS codproducto,
    p.nombre,
    dep.codigo AS dpto,
    dep.descripcion AS descdpto,
    d.descripcion AS descripciondepto,
    (pd.cantidad)::text AS cantidad
   FROM (((productosdepositos pd
     JOIN producto p ON ((p.codproducto = pd.codproducto)))
     JOIN departamentos dep ON ((dep.coddepartamento = p.coddepartamento)))
     JOIN depositos d ON ((d.coddeposito = pd.coddeposito)));


ALTER TABLE public.rep_inv_existencia OWNER TO postgres;

--
-- TOC entry 326 (class 1259 OID 98230)
-- Name: rep_inventariofisico; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW rep_inventariofisico AS
 SELECT p.codigo,
    p.nombre,
    pd.cantidad,
    p.unidadprincipal,
    p.unidadalterna,
    p.factorconversion,
    d.codigo AS cod_departamento,
    d.descripcion AS departamento,
    pd.coddeposito AS cod_deposito,
    dp.descripcion AS depo_descripcion,
    p.costoactual,
    p.costopromedio
   FROM (((productosdepositos pd
     JOIN producto p ON ((p.codproducto = pd.codproducto)))
     JOIN departamentos d ON ((p.coddepartamento = d.coddepartamento)))
     JOIN depositos dp ON ((dp.coddeposito = pd.coddeposito)));


ALTER TABLE public.rep_inventariofisico OWNER TO postgres;

--
-- TOC entry 327 (class 1259 OID 98235)
-- Name: resumendecomprascreditosindevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendecomprascreditosindevolucion AS
 SELECT c.codcompra,
    ((((c.femision)::text || ' '::text) || (c.hora)::text))::timestamp without time zone AS fecha,
    cp.totalcosto,
    cp.totalcostodescuento,
    cp.subtotaliva,
    cp.subtotalivadescuento,
    cp.total,
    cp.totaldescuento,
    pag.credito,
    c.descuento
   FROM (( SELECT compraspagos.codcompra,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CREDITO'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS credito
           FROM compraspagos
          GROUP BY compraspagos.codcompra) pag
     JOIN (( SELECT cp_1.codcompra,
            COALESCE(sum((cp_1.costo * cp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((cp_1.costodescuento * cp_1.cantidad)), (0)::real) AS totalcostodescuento,
            COALESCE(sum(((cp_1.costo * cp_1.cantidad) * ((1)::real + (cp_1.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((cp_1.costodescuento * cp_1.cantidad) * ((1)::real + (cp_1.iva / (100)::real)))), (0)::real) AS totaldescuento,
            COALESCE(sum(((cp_1.costo * cp_1.cantidad) * (cp_1.iva / (100)::real))), (0)::real) AS subtotaliva,
            COALESCE(sum(((cp_1.costodescuento * cp_1.cantidad) * (cp_1.iva / (100)::real))), (0)::real) AS subtotalivadescuento
           FROM comprasproductos cp_1
          GROUP BY cp_1.codcompra) cp
     JOIN compras c ON ((c.codcompra = cp.codcompra))) ON ((pag.codcompra = c.codcompra)));


ALTER TABLE public.resumendecomprascreditosindevolucion OWNER TO postgres;

--
-- TOC entry 328 (class 1259 OID 98240)
-- Name: resumendedevolucionescompracreditodiario; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendedevolucionescompracreditodiario AS
 SELECT d.coddevolucioncompra,
    d.codsucursal,
    d.codcompra,
    d.fdevolucion,
    d.totalcosto,
    d.total,
    d.subtotaliva,
    d.totalcostodescuento,
    d.totaldescuento,
    d.subtotalivadescuento,
    pag.credito
   FROM (( SELECT devolucioncomprapago.coddevolucioncompra,
            devolucioncomprapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'CREDITO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS credito
           FROM devolucioncomprapago
          GROUP BY devolucioncomprapago.coddevolucioncompra, devolucioncomprapago.codsucursal) pag
     JOIN ( SELECT d_1.coddevolucioncompra,
            d_1.codsucursal,
            d_1.codcompra,
            d_1.fdevolucion,
            COALESCE(sum((cp.costo * dp.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum(((cp.costo * dp.cantidad) * (cp.iva / (100)::real))), (0)::real) AS subtotaliva,
            COALESCE(sum(((cp.costo * dp.cantidad) * ((1)::real + (cp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum((cp.costodescuento * dp.cantidad)), (0)::real) AS totalcostodescuento,
            COALESCE(sum(((cp.costodescuento * dp.cantidad) * (cp.iva / (100)::real))), (0)::real) AS subtotalivadescuento,
            COALESCE(sum(((cp.costodescuento * dp.cantidad) * ((1)::real + (cp.iva / (100)::real)))), (0)::real) AS totaldescuento
           FROM (comprasproductos cp
             JOIN (devolucioncompra d_1
             JOIN devolucioncompraproductos dp ON ((dp.coddevolucioncompra = d_1.coddevolucioncompra))) ON (((d_1.codcompra = cp.codcompra) AND (cp.indice = dp.indice))))
          GROUP BY d_1.coddevolucioncompra, d_1.codsucursal, d_1.codcompra, d_1.fdevolucion) d ON (((pag.coddevolucioncompra = d.coddevolucioncompra) AND (d.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendedevolucionescompracreditodiario OWNER TO postgres;

--
-- TOC entry 330 (class 1259 OID 98248)
-- Name: resumendedevolucionescreditodiario; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendedevolucionescreditodiario AS
 SELECT d.coddevolucion,
    d.codsucursal,
    d.codventa,
    d.codsucursalventa,
    d.fecha,
    d.totalcosto,
    d.subtotal,
    d.total,
    d.subtotaliva,
    pag.credito
   FROM (( SELECT devolucionventapago.coddevolucion,
            devolucionventapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CREDITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS credito
           FROM devolucionventapago
          GROUP BY devolucionventapago.coddevolucion, devolucionventapago.codsucursal) pag
     JOIN ( SELECT d_1.coddevolucion,
            d_1.codsucursal,
            d_1.codventa,
            d_1.codsucursalventa,
            d_1.fecha,
            COALESCE(sum((vp.costopromedio * dp.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp.precio * dp.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp.precio * dp.cantidad) * ((1)::real + (vp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp.precio * dp.cantidad) * (vp.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM (ventasproductos vp
             JOIN (devolucionventa d_1
             JOIN devolucionventaproductos dp ON (((dp.coddevolucion = d_1.coddevolucion) AND (d_1.codsucursal = dp.codsucursal)))) ON ((((d_1.codventa = vp.codventa) AND (d_1.codsucursalventa = vp.codsucursal)) AND (vp.indice = dp.indice))))
          GROUP BY d_1.coddevolucion, d_1.codsucursal, d_1.codventa, d_1.codsucursalventa, d_1.fecha) d ON (((pag.coddevolucion = d.coddevolucion) AND (d.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendedevolucionescreditodiario OWNER TO postgres;

--
-- TOC entry 331 (class 1259 OID 98253)
-- Name: resumendedevolucionesdiario; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendedevolucionesdiario AS
 SELECT d.coddevolucion,
    d.codsucursal,
    d.codventa,
    d.codsucursalventa,
    d.fecha,
    d.totalcosto,
    d.subtotal,
    d.total,
    d.subtotaliva,
    pag.credito,
    (((pag.efectivo + pag.cheque) + pag.transfer) + pag.debito) AS contado,
    pag.efectivo,
    pag.cheque,
    pag.transfer,
    pag.debito
   FROM (( SELECT devolucionventapago.coddevolucion,
            devolucionventapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CREDITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'EFECTIVO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CHEQUE'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TRANSFER'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TDEBITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS debito
           FROM devolucionventapago
          GROUP BY devolucionventapago.coddevolucion, devolucionventapago.codsucursal) pag
     JOIN ( SELECT d_1.coddevolucion,
            d_1.codsucursal,
            d_1.codventa,
            d_1.codsucursalventa,
            d_1.fecha,
            COALESCE(sum((vp.costopromedio * dp.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp.precio * dp.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp.precio * dp.cantidad) * ((1)::real + (vp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp.precio * dp.cantidad) * (vp.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM (ventasproductos vp
             JOIN (devolucionventa d_1
             JOIN devolucionventaproductos dp ON (((dp.coddevolucion = d_1.coddevolucion) AND (d_1.codsucursal = dp.codsucursal)))) ON ((((d_1.codventa = vp.codventa) AND (d_1.codsucursalventa = vp.codsucursal)) AND (vp.indice = dp.indice))))
          GROUP BY d_1.coddevolucion, d_1.codsucursal, d_1.codventa, d_1.codsucursalventa, d_1.fecha) d ON (((pag.coddevolucion = d.coddevolucion) AND (d.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendedevolucionesdiario OWNER TO postgres;


--
-- TOC entry 335 (class 1259 OID 98270)
-- Name: resumendeventascreditosindevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventascreditosindevolucion AS
 SELECT v.codventa,
    v.codsucursal,
    ((((v.fecha)::text || ' '::text) || (v.hora)::text))::timestamp without time zone AS fecha,
    vp.totalcosto,
    vp.subtotal,
    vp.total,
    vp.subtotaliva,
    pag.credito
   FROM (( SELECT ventaspagos.codventa,
            ventaspagos.codsucursal,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS credito
           FROM ventaspagos
          GROUP BY ventaspagos.codventa, ventaspagos.codsucursal) pag
     JOIN (( SELECT vp_1.codventa,
            vp_1.codsucursal,
            COALESCE(sum((vp_1.costopromedio * vp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp_1.precio * vp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * ((1)::real + (vp_1.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * (vp_1.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM ventasproductos vp_1
          GROUP BY vp_1.codventa, vp_1.codsucursal) vp
     JOIN ventas v ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal)))) ON (((pag.codventa = v.codventa) AND (v.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendeventascreditosindevolucion OWNER TO postgres;

--
-- TOC entry 336 (class 1259 OID 98275)
-- Name: resumendeventasdiarias; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventasdiarias AS
 SELECT a.codventa,
    a.codsucursal,
    a.codproducto,
    a.comprado,
    a.devuelto,
    a.cantidad,
    a.costopromedio,
    a.precio,
    a.iva,
    a.fecha,
    (a.cantidad * a.costopromedio) AS totalcosto,
    (a.cantidad * a.precio) AS subtotal,
    ((a.cantidad * a.precio) * ((1)::double precision + (a.iva / (100)::double precision))) AS total,
    ((a.cantidad * a.precio) * (a.iva / (100)::double precision)) AS subtotaliva
   FROM ( SELECT vp.codventa,
            vp.codsucursal,
            vp.codproducto,
            vp.cantidad AS comprado,
            COALESCE(b.cantidad, (0)::real) AS devuelto,
            (vp.cantidad - COALESCE(b.cantidad, (0)::real)) AS cantidad,
            vp.costopromedio,
            vp.precio,
            vp.iva,
            v.fecha
           FROM (ventas v
             JOIN (ventasproductos vp
             LEFT JOIN ( SELECT v_1.codventa,
                    v_1.codsucursal,
                    dp.indice,
                    sum(dp.cantidad) AS cantidad
                   FROM (ventas v_1
                     LEFT JOIN (devolucionventa d
                     JOIN devolucionventaproductos dp ON (((d.coddevolucion = dp.coddevolucion) AND (d.codsucursal = dp.codsucursal)))) ON ((((d.codventa = v_1.codventa) AND (d.codsucursalventa = v_1.codsucursal)) AND (v_1.fecha = (d.fecha)::date))))
                  GROUP BY v_1.codventa, v_1.codsucursal, dp.indice) b ON ((((vp.codventa = b.codventa) AND (vp.codsucursal = b.codsucursal)) AND (vp.indice = b.indice)))) ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal))))) a;


ALTER TABLE public.resumendeventasdiarias OWNER TO postgres;

--
-- TOC entry 337 (class 1259 OID 98280)
-- Name: resumendeventasdiariasporproducto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventasdiariasporproducto AS
 SELECT a.codventa,
    a.codsucursal,
    a.codproducto,
    a.comprado,
    a.devuelto,
    a.cantidad,
    a.costopromedio,
    a.precio,
    a.iva,
    a.fecha,
    (a.cantidad * a.costopromedio) AS totalcosto,
    (a.cantidad * a.precio) AS subtotal,
    ((a.cantidad * a.precio) * ((1)::double precision + (a.iva / (100)::double precision))) AS total,
    ((a.cantidad * a.precio) * (a.iva / (100)::double precision)) AS subtotaliva
   FROM ( SELECT vp.codventa,
            vp.codsucursal,
            vp.codproducto,
            vp.cantidad AS comprado,
            COALESCE(a_1.cantidad, (0)::real) AS devuelto,
            (vp.cantidad - COALESCE(a_1.cantidad, (0)::real)) AS cantidad,
            vp.costopromedio,
            vp.precio,
            vp.iva,
            v.fecha
           FROM (ventas v
             JOIN (ventasproductos vp
             LEFT JOIN ( SELECT v_1.codventa,
                    v_1.codsucursal,
                    dp.indice,
                    sum(dp.cantidad) AS cantidad
                   FROM (ventas v_1
                     LEFT JOIN (devolucionventa d
                     JOIN devolucionventaproductos dp ON (((d.coddevolucion = dp.coddevolucion) AND (d.codsucursal = dp.codsucursal)))) ON ((((d.codventa = v_1.codventa) AND (d.codsucursalventa = v_1.codsucursal)) AND (v_1.fecha = (d.fecha)::date))))
                  GROUP BY v_1.codventa, v_1.codsucursal, dp.indice) a_1 ON ((((vp.codventa = a_1.codventa) AND (vp.codsucursal = a_1.codsucursal)) AND (vp.indice = a_1.indice)))) ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal))))) a;


ALTER TABLE public.resumendeventasdiariasporproducto OWNER TO postgres;

--
-- TOC entry 338 (class 1259 OID 98285)
-- Name: resumendeventasdiariasporventacondevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventasdiariasporventacondevolucion AS
 SELECT a.codventa,
    a.codsucursal,
    a.fecha,
    a.totalcosto,
    a.subtotal,
    a.total,
    a.subtotaliva,
    b.credito,
    b.contado,
        CASE a.total
            WHEN 0 THEN (0)::real
            ELSE ((a.subtotal * b.contado) / a.total)
        END AS basecontado,
        CASE a.total
            WHEN 0 THEN (0)::real
            ELSE ((a.subtotal * b.credito) / a.total)
        END AS basecredito
   FROM (( SELECT a_1.codventa,
            a_1.codsucursal,
            a_1.fecha,
            sum((a_1.cantidad * a_1.costopromedio)) AS totalcosto,
            sum((a_1.cantidad * a_1.precio)) AS subtotal,
            (sum(((a_1.cantidad * a_1.precio) * ((1)::double precision + (a_1.iva / (100)::double precision)))))::real AS total,
            (sum(((a_1.cantidad * a_1.precio) * (a_1.iva / (100)::double precision))))::real AS subtotaliva
           FROM ( SELECT vp.codventa,
                    vp.codsucursal,
                    vp.codproducto,
                    vp.cantidad AS comprado,
                    COALESCE(a_2.cantidad, (0)::real) AS devuelto,
                    (vp.cantidad - COALESCE(a_2.cantidad, (0)::real)) AS cantidad,
                    vp.costopromedio,
                    vp.precio,
                    vp.iva,
                    v.fecha
                   FROM (ventas v
                     JOIN (ventasproductos vp
                     LEFT JOIN ( SELECT v_1.codventa,
                            v_1.codsucursal,
                            dp.indice,
                            sum(dp.cantidad) AS cantidad
                           FROM (ventas v_1
                             LEFT JOIN (devolucionventa d
                             JOIN devolucionventaproductos dp ON (((d.coddevolucion = dp.coddevolucion) AND (d.codsucursal = dp.codsucursal)))) ON ((((d.codventa = v_1.codventa) AND (d.codsucursalventa = v_1.codsucursal)) AND (v_1.fecha = (d.fecha)::date))))
                          GROUP BY v_1.codventa, v_1.codsucursal, dp.indice) a_2 ON ((((vp.codventa = a_2.codventa) AND (vp.codsucursal = a_2.codsucursal)) AND (vp.indice = a_2.indice)))) ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal))))) a_1
          GROUP BY a_1.codventa, a_1.codsucursal, a_1.fecha) a
     JOIN ( SELECT c.codventa,
            c.codsucursal,
            sum(
                CASE
                    WHEN (c.tipopago = 'CREDITO'::text) THEN c.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN (c.tipopago <> 'CREDITO'::text) THEN c.monto
                    ELSE (0)::real
                END) AS contado
           FROM ( SELECT v.codventa,
                    v.codsucursal,
                        CASE
                            WHEN ((vp.tipopago)::text = 'CREDITO'::text) THEN 'CREDITO'::text
                            ELSE 'CONTADO'::text
                        END AS tipopago,
                    sum(vp.monto) AS monto
                   FROM (ventaspagos vp
                     JOIN ventas v ON (((vp.codventa = v.codventa) AND (vp.codsucursal = v.codsucursal))))
                  GROUP BY v.codventa, v.codsucursal,
                        CASE
                            WHEN ((vp.tipopago)::text = 'CREDITO'::text) THEN 'CREDITO'::text
                            ELSE 'CONTADO'::text
                        END
                UNION
                 SELECT v.codventa,
                    v.codsucursal,
                        CASE
                            WHEN ((dp.tipopago)::text = 'CREDITO'::text) THEN 'CREDITO'::text
                            ELSE 'CONTADO'::text
                        END AS tipopago,
                    sum((dp.monto * (- (1)::real))) AS monto
                   FROM (devolucionventapago dp
                     JOIN (devolucionventa d
                     JOIN ventas v ON ((((d.codventa = v.codventa) AND (d.codsucursalventa = v.codsucursal)) AND ((d.fecha)::date = v.fecha)))) ON (((dp.coddevolucion = d.coddevolucion) AND (dp.codsucursal = d.codsucursal))))
                  GROUP BY v.codventa, v.codsucursal,
                        CASE
                            WHEN ((dp.tipopago)::text = 'CREDITO'::text) THEN 'CREDITO'::text
                            ELSE 'CONTADO'::text
                        END) c
          GROUP BY c.codventa, c.codsucursal) b ON (((a.codventa = b.codventa) AND (a.codsucursal = b.codsucursal))));


ALTER TABLE public.resumendeventasdiariasporventacondevolucion OWNER TO postgres;

--
-- TOC entry 443 (class 1259 OID 115581)
-- Name: resumendeventasdiariasporventasindevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW resumendeventasdiariasporventasindevolucion AS
 SELECT v.codventa,
    v.codsucursal,
    ((((v.fecha)::text || ' '::text) || (v.hora)::text))::timestamp without time zone AS fecha,
    vp.totalcosto,
    vp.subtotal,
    vp.total,
    vp.subtotaliva,
    (((((pag.efectivo + pag.tdebito) + pag.cheque) + pag.transfer) + pag.tcredito) + pag.ticket) AS contado,
    pag.credito,
    pag.efectivo,
    pag.tdebito,
    pag.cheque,
    pag.transfer,
    pag.tcredito,
    pag.ticket,
    v.retencion
   FROM (( SELECT ventaspagos.codventa,
            ventaspagos.codsucursal,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'EFECTIVO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'DEBITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS tdebito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CHEQUE'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TRANSFER'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TCREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS tcredito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TICKET'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS ticket
           FROM ventaspagos
          GROUP BY ventaspagos.codventa, ventaspagos.codsucursal) pag
     JOIN (( SELECT vp_1.codventa,
            vp_1.codsucursal,
            COALESCE(sum((vp_1.costopromedio * vp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp_1.precio * vp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * ((1)::real + (vp_1.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * (vp_1.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM ventasproductos vp_1
          GROUP BY vp_1.codventa, vp_1.codsucursal) vp
     JOIN ventas v ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal)))) ON (((pag.codventa = v.codventa) AND (v.codsucursal = pag.codsucursal))));


ALTER TABLE public.resumendeventasdiariasporventasindevolucion OWNER TO postgres;



--
-- TOC entry 378 (class 1259 OID 98448)
-- Name: v_ajuste; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ajuste AS
 SELECT (h_producto.codajuste)::text AS "Código_varchar",
    count(h_producto.codproducto) AS "TotalProductos_int",
    h_producto.fecha AS "FechaCreacion_date",
    (h_producto.codusuario)::text AS "CodUsuario_varchar"
   FROM h_producto
  GROUP BY h_producto.codajuste, h_producto.fecha, h_producto.codusuario;


ALTER TABLE public.v_ajuste OWNER TO postgres;

--
-- TOC entry 449 (class 1259 OID 133359)
-- Name: v_bancos; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_bancos AS
 SELECT bancos.codbanco AS "Código_int",
    bancos.nombre AS "Nombre_varchar",
    bancos.abreviatura AS "Abreviatura_text"
   FROM bancos
  WHERE (bancos.estatus = true);


ALTER TABLE public.v_bancos OWNER TO postgres;

--
-- TOC entry 379 (class 1259 OID 98452)
-- Name: v_cargo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_cargo AS
 SELECT cargos.codcargo AS "Código_int",
    cargos.descripcion AS "Descripción_text",
    cargos.autorizado AS "Autorizado_varchar",
    cargos.responsable AS "Responsable_varchar",
    to_char((cargos.fecha)::timestamp with time zone, 'DD/MM/YYYY'::text) AS "Fecha_date",
    sucursal.descripcion AS "Sucursal_text"
   FROM (cargos
     JOIN sucursal ON ((cargos.codsucursal = sucursal.codsucursal)));


ALTER TABLE public.v_cargo OWNER TO postgres;


--
-- TOC entry 382 (class 1259 OID 98466)
-- Name: v_cliente; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_cliente AS
 SELECT (((clientes.codcliente)::text || '-'::text) || (clientes.codsucursal)::text) AS "Código_varchar",
    clientes.descripcion AS "Descripción_text",
    clientes.rif AS "RIF_varchar",
    clientes.clase AS "Clase_varchar",
    clientes.representante AS "Representante_text",
    clientes.direccion AS direccion_text,
    clientes.telefonos AS "Teléfonos_varchar",
    clientes.email AS "Email_text",
    clientes.numerodefax AS "Nro. Fax_varchar",
    clientes.zona AS "Zona_varchar",
    vendedores.descripcion AS "Vendedor_text",
    clientes.tipodeprecio AS "Tipo de Precio_varchar",
    clientes.limitecredito AS "Límite de Crédito_float",
    clientes.diasdecredito AS "Días de Crédito_int",
    clientes.diastolerancia AS "Días de Tolerancia_int",
    clientes.fechadeinicio AS "Fecha de Inicio_date",
    clientes.observaciones AS "Observaciones_text",
    clientes.tipodecliente AS "Tipo de Cliente_varchar",
    clientes.interesdemora AS "Interés de Mora_varchar",
    clientes.convenioprecio AS "Convenio de Precio_varchar",
    clientes.porcdescuento AS "% Descuento_float"
   FROM (clientes
     LEFT JOIN vendedores ON ((clientes.codvendedor = vendedores.codvendedor)))
  WHERE (clientes.estatus = true);


ALTER TABLE public.v_cliente OWNER TO postgres;

--
-- TOC entry 383 (class 1259 OID 98471)
-- Name: v_cliente_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_cliente_i AS
 SELECT (((clientes.codcliente)::text || '-'::text) || (clientes.codsucursal)::text) AS "Código_varchar",
    clientes.descripcion AS "Descripción_text",
    clientes.rif AS "RIF_varchar",
    clientes.clase AS "Clase_varchar",
    clientes.representante AS "Representante_text",
    clientes.direccion AS direccion_text,
    clientes.telefonos AS "Teléfonos_varchar",
    clientes.email AS "Email_text",
    clientes.numerodefax AS "Nro. Fax_varchar",
    clientes.zona AS "Zona_varchar",
    vendedores.descripcion AS "Vendedor_text",
    clientes.tipodeprecio AS "Tipo de Precio_varchar",
    clientes.limitecredito AS "Límite de Crédito_float",
    clientes.diasdecredito AS "Días de Crédito_int",
    clientes.diastolerancia AS "Días de Tolerancia_int",
    clientes.fechadeinicio AS "Fecha de Inicio_date",
    clientes.observaciones AS "Observaciones_text",
    clientes.tipodecliente AS "Tipo de Cliente_varchar",
    clientes.interesdemora AS "Interés de Mora_varchar",
    clientes.convenioprecio AS "Convenio de Precio_varchar",
    clientes.porcdescuento AS "% Descuento_float"
   FROM (clientes
     JOIN vendedores ON (((clientes.codvendedor = vendedores.codvendedor) AND (clientes.estatus = false))));


ALTER TABLE public.v_cliente_i OWNER TO postgres;

--
-- TOC entry 384 (class 1259 OID 98476)
-- Name: v_compra; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_compra AS
 SELECT compras.codcompra AS "Código_int",
    proveedores.rif AS "RIF_text",
    proveedores.descripcion AS "Proveedor_text",
    depositos.descripcion AS "Depósito_text",
    compras.nrodocumento AS "Documento_varchar",
    compras.nrocontrol AS "Control_varchar",
    compras.femision AS "Emisión_date",
    compras.fregistro AS "Registro_date"
   FROM ((compras
     JOIN proveedores ON ((compras.codproveedor = proveedores.codproveedor)))
     JOIN depositos ON ((depositos.coddeposito = compras.coddeposito)));


ALTER TABLE public.v_compra OWNER TO postgres;

--
-- TOC entry 385 (class 1259 OID 98481)
-- Name: v_compraespera; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_compraespera AS
 SELECT a.codcompraespera AS "Código_int",
    b.descripcion AS "Proveedor_varchar",
    a.nrodocumento AS "Documento_varchar",
    a.femision AS "Fecha_date",
    a.coddeposito AS "Depósito_int"
   FROM (comprasespera a
     JOIN proveedores b ON ((a.codproveedor = b.codproveedor)));


ALTER TABLE public.v_compraespera OWNER TO postgres;

--
-- TOC entry 386 (class 1259 OID 98485)
-- Name: v_comprasfacturas_cxp; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_comprasfacturas_cxp AS
 SELECT a.codproveedor,
    a.descripcion,
    a.direccion,
    a.tipomovimiento,
    a.documento,
    b.fvencimiento,
    a.fecha,
    b.monto,
    a.codmovimiento
   FROM (( SELECT a_1.codcompra AS codmovimiento,
            'COMPRAS'::text AS tipomovimiento,
            a_1.nrodocumento AS documento,
            a_1.femision AS fecha,
            a_1.codproveedor,
            ( SELECT proveedores.descripcion
                   FROM proveedores
                  WHERE (proveedores.codproveedor = a_1.codproveedor)) AS descripcion,
            ( SELECT proveedores.direccion
                   FROM proveedores
                  WHERE (proveedores.codproveedor = a_1.codproveedor)) AS direccion
           FROM compras a_1
        UNION
         SELECT a_1.codcpfactura AS codmovimiento,
            'FA'::text AS tipomovimiento,
            a_1.nrodocumento AS documento,
            a_1.fecha,
            a_1.codproveedor,
            ( SELECT proveedores.descripcion
                   FROM proveedores
                  WHERE (proveedores.codproveedor = a_1.codproveedor)) AS descripcion,
            ( SELECT proveedores.direccion
                   FROM proveedores
                  WHERE (proveedores.codproveedor = a_1.codproveedor)) AS direccion
           FROM cpfactura a_1) a
     JOIN cuentasporpagar b ON (((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text))))
  ORDER BY a.descripcion, b.fvencimiento;


ALTER TABLE public.v_comprasfacturas_cxp OWNER TO postgres;

--
-- TOC entry 387 (class 1259 OID 98490)
-- Name: v_correo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_correo AS
 SELECT upper((correo.nombre)::text) AS "Nombre_varchar",
    correo.tipo AS "Tipo_varchar"
   FROM correo;


ALTER TABLE public.v_correo OWNER TO postgres;

--
-- TOC entry 388 (class 1259 OID 98494)
-- Name: v_departamento; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_departamento AS
 SELECT departamentos.codigo AS "Código_varchar",
    departamentos.descripcion AS "Descripción_text",
    departamentos.clase AS "Clase_varchar",
    departamentos.compuesto AS "Compuesto_varchar",
    departamentos.seriales AS "Seriales_varchar",
    departamentos.comisiones AS "Comisiones_varchar",
    departamentos.estatus AS "Estatus_bool"
   FROM departamentos
  WHERE (departamentos.estatus = true);


ALTER TABLE public.v_departamento OWNER TO postgres;

--
-- TOC entry 389 (class 1259 OID 98498)
-- Name: v_departamento_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_departamento_i AS
 SELECT departamentos.coddepartamento AS "Código_int",
    departamentos.descripcion AS "Descripción_text",
    departamentos.clase AS "Clase_varchar",
    departamentos.compuesto AS "Compuesto_varchar",
    departamentos.seriales AS "Seriales_varchar",
    departamentos.comisiones AS "Comisiones_varchar",
    departamentos.estatus AS "Estatus_bool"
   FROM departamentos
  WHERE (departamentos.estatus = false);


ALTER TABLE public.v_departamento_i OWNER TO postgres;

--
-- TOC entry 390 (class 1259 OID 98502)
-- Name: v_deposito; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_deposito AS
 SELECT depositos.coddeposito AS "Código_varchar",
    depositos.descripcion AS "Descripción_text",
    depositos.responsable AS "Responsable_text",
    sucursal.descripcion AS "Sucursal_text",
    depositos.clase AS "Clase_varchar"
   FROM (depositos
     JOIN sucursal ON (((depositos.codsucursal = sucursal.codsucursal) AND (depositos.estatus = true))));


ALTER TABLE public.v_deposito OWNER TO postgres;

--
-- TOC entry 391 (class 1259 OID 98506)
-- Name: v_deposito_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_deposito_i AS
 SELECT depositos.coddeposito AS "Código_varchar",
    depositos.descripcion AS "Descripción_text",
    depositos.responsable AS "Responsable_text",
    sucursal.descripcion AS "Sucursal_text",
    depositos.clase AS "Clase_varchar"
   FROM (depositos
     JOIN sucursal ON (((depositos.codsucursal = sucursal.codsucursal) AND (depositos.estatus = false))));


ALTER TABLE public.v_deposito_i OWNER TO postgres;

--
-- TOC entry 392 (class 1259 OID 98510)
-- Name: v_descargo; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_descargo AS
 SELECT descargos.coddescargo AS "Código_int",
    descargos.descripcion AS "Descripción_text",
    descargos.autorizado AS "Autorizado_varchar",
    descargos.responsable AS "Responsable_varchar",
    to_char((descargos.fecha)::timestamp with time zone, 'DD/MM/YYYY'::text) AS "Fecha_date",
    sucursal.descripcion AS "Sucursal_text"
   FROM (descargos
     JOIN sucursal ON ((descargos.codsucursal = sucursal.codsucursal)));


ALTER TABLE public.v_descargo OWNER TO postgres;

--
-- TOC entry 393 (class 1259 OID 98514)
-- Name: v_devolucion_z; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_devolucion_z AS
 SELECT devolucion_z.codevolucionz AS "Códigodev_int",
    devolucion_z.codmovimientoz AS "Códigomov_int",
    devolucion_z.factura AS "Factura_int",
    devolucion_z.fecha AS "Fecha_date"
   FROM devolucion_z;


ALTER TABLE public.v_devolucion_z OWNER TO postgres;

--
-- TOC entry 394 (class 1259 OID 98518)
-- Name: v_devolucioncompra; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_devolucioncompra AS
 SELECT d.coddevolucioncompra AS "Código_varchar",
    d.codcompra AS "Compra_varchar",
    p.rif AS "RIF_text",
    p.descripcion AS "Descripcion_varchar",
    c.nrodocumento AS "Documento_varchar",
    d.fdevolucion AS "Fecha_date"
   FROM (devolucioncompra d
     JOIN (compras c
     JOIN proveedores p ON ((c.codproveedor = p.codproveedor))) ON ((d.codcompra = c.codcompra)))
  ORDER BY d.coddevolucioncompra;


ALTER TABLE public.v_devolucioncompra OWNER TO postgres;

--
-- TOC entry 395 (class 1259 OID 98523)
-- Name: v_devolucionventa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_devolucionventa AS
 SELECT (((d.coddevolucion)::text || '-'::text) || (d.codsucursal)::text) AS "Código_varchar",
    d.coddevolucion AS "Numero_varchar",
    v.codvendedor AS "Vendedor_varchar",
    (((v.codcliente)::text || '-'::text) || (v.codsucursal)::text) AS "Código Cliente_varchar",
    v.numerofactura AS "Factura_varchar",
    c.rif AS "Rif_varchar",
    c.descripcion AS "Descripción_varchar",
    d.fecha AS "Fecha_date",
    ( SELECT sum(devolucionventapago.monto) AS sum
           FROM devolucionventapago
          WHERE ((devolucionventapago.codsucursal = d.codsucursal) AND (devolucionventapago.coddevolucion = d.coddevolucion))) AS "Monto_double",
    d.coddeposito AS "Deposito_varchar",
    d.codventa AS "Venta_varchar",
    d.devoluciontotal AS "DevolucionTotal_bool"
   FROM (devolucionventa d
     JOIN (ventas v
     JOIN clientes c ON (((v.codcliente = c.codcliente) AND (v.codclientesucursal = c.codsucursal)))) ON (((d.codventa = v.codventa) AND (d.codsucursalventa = v.codsucursal))))
  ORDER BY d.coddevolucion;


ALTER TABLE public.v_devolucionventa OWNER TO postgres;

--
-- TOC entry 396 (class 1259 OID 98528)
-- Name: v_devolucionventa_2; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_devolucionventa_2 AS
 SELECT (((d.coddevolucion)::text || '-'::text) || (d.codsucursal)::text) AS "Código_varchar",
    d.coddevolucion AS "Numero_varchar",
    v.codvendedor AS "Vendedor_varchar",
    (((v.codcliente)::text || '-'::text) || (v.codsucursal)::text) AS "Código Cliente_varchar",
    v.numerofactura AS "Factura_varchar",
    c.rif AS "Rif_varchar",
    c.descripcion AS "Descripción_varchar",
    d.fecha AS "Fecha_date",
    ( SELECT sum(devolucionventapago.monto) AS sum
           FROM devolucionventapago
          WHERE ((devolucionventapago.codsucursal = d.codsucursal) AND (devolucionventapago.coddevolucion = d.coddevolucion))) AS "Monto_double",
    d.coddeposito AS "Deposito_varchar",
    d.codventa AS "Venta_varchar",
    d.devoluciontotal AS "DevolucionTotal_bool",
    0 AS ctid
   FROM (devolucionventa d
     JOIN (ventas v
     JOIN clientes c ON (((v.codcliente = c.codcliente) AND (v.codclientesucursal = c.codsucursal)))) ON (((d.codventa = v.codventa) AND (d.codsucursalventa = v.codsucursal))))
  ORDER BY d.coddevolucion;


ALTER TABLE public.v_devolucionventa_2 OWNER TO postgres;

--
-- TOC entry 397 (class 1259 OID 98533)
-- Name: v_estaciones; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_estaciones AS
 SELECT a.codestacion AS "Código_int",
    a.nombre AS "Nombre_text",
    a.descripcion AS "Descripción_varchar",
    (((b.codsucursal)::text || '-'::text) || b.descripcion) AS "Sucursal_varchar",
    a.codnumerofactura AS "Numeración_varchar"
   FROM (estaciones a
     JOIN sucursal b ON ((a.codsucursal = b.codsucursal)));


ALTER TABLE public.v_estaciones OWNER TO postgres;

--
-- TOC entry 398 (class 1259 OID 98537)
-- Name: v_estadodecuenta; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_estadodecuenta AS
 SELECT todo.tipomovimiento,
    todo.femision,
    todo.fvencimiento,
    todo.numero,
    todo.concepto,
    todo.debito,
    todo.credito,
    todo.fecha,
    todo.codproveedor
   FROM ( SELECT a.tipomovimiento,
            a.femision,
            b.fvencimiento,
            a.numero,
            b.concepto,
            0 AS debito,
            b.monto AS credito,
            a.fecha,
            a.codproveedor
           FROM (( SELECT a_1.codcompra AS codmovimiento,
                    'COMPRAS'::text AS tipomovimiento,
                    a_1.femision,
                    a_1.nrodocumento AS numero,
                    a_1.femision AS fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM compras a_1
                UNION
                 SELECT a_1.codcpfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.fecha,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpfactura a_1
                UNION
                 SELECT a_1.codcpnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.fecha,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpnotadebito a_1) a
             JOIN cuentasporpagar b ON ((((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text)) AND (a.codsucursal = b.codsucursal))))
        UNION
         SELECT a.tipomovimiento,
            a.femision,
            bh.fvencimiento,
            a.numero,
            bh.concepto,
            0 AS debito,
            bh.monto AS credito,
            a.fecha,
            a.codproveedor
           FROM (( SELECT a_1.codcompra AS codmovimiento,
                    'COMPRAS'::text AS tipomovimiento,
                    a_1.femision,
                    a_1.nrodocumento AS numero,
                    a_1.femision AS fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM compras a_1
                UNION
                 SELECT a_1.codcpfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.fecha,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpfactura a_1
                UNION
                 SELECT a_1.codcpnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.fecha,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpnotadebito a_1) a
             JOIN h_cuentasporpagar bh ON ((((a.codmovimiento = bh.codmovimiento) AND (a.tipomovimiento = (bh.tipomovimiento)::text)) AND (a.codsucursal = bh.codsucursal))))
        UNION
         SELECT cpp.tipomovimientopago AS tipomovimiento,
            pagos.fecha AS femision,
            NULL::date AS fvencimiento,
            pagos.numero,
            pagos.descripcion AS concepto,
            cpp.monto AS debito,
            0 AS credito,
            pagos.fecha,
            a.codproveedor
           FROM (( SELECT cpnotacredito.codcpnotacredito AS codmovimiento,
                    'NC'::text AS tipomovimiento,
                    cpnotacredito.fecha,
                    cpnotacredito.nrodocumento AS numero,
                    cpnotacredito.concepto AS descripcion
                   FROM cpnotacredito
                UNION
                 SELECT cpabono.codcpabono AS codmovimiento,
                    'PAG'::text AS tipomovimiento,
                    cpabono.fecha,
                    cpabono.nrodocumento AS numero,
                    cpabono.concepto AS descripcion
                   FROM cpabono
                UNION
                 SELECT cpabono.codcpabono AS codmovimiento,
                    'ABO'::text AS tipomovimiento,
                    cpabono.fecha,
                    cpabono.nrodocumento AS numero,
                    cpabono.concepto AS descripcion
                   FROM cpabono) pagos
             JOIN (cppagadas cpp
             JOIN (cuentasporpagar cxp
             JOIN ( SELECT a_1.codcompra AS codmovimiento,
                    'COMPRAS'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.femision,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM compras a_1
                UNION
                 SELECT a_1.codcpfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpfactura a_1
                UNION
                 SELECT a_1.codcpnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpnotadebito a_1) a ON ((((a.codmovimiento = cxp.codmovimiento) AND (a.tipomovimiento = (cxp.tipomovimiento)::text)) AND (a.codsucursal = cxp.codsucursal)))) ON (((((cpp.tipomovimientocp)::text = (cxp.tipomovimiento)::text) AND (cpp.codmovimientocp = cxp.codmovimiento)) AND (cxp.codsucursal = cpp.codsucursalcp)))) ON (((cpp.codmovimientopago = pagos.codmovimiento) AND ((cpp.tipomovimientopago)::text = pagos.tipomovimiento))))
        UNION
         SELECT cpp.tipomovimientopago AS tipomovimiento,
            pagos.fecha AS femision,
            NULL::date AS fvencimiento,
            pagos.numero,
            pagos.descripcion AS concepto,
            cpp.monto AS debito,
            0 AS credito,
            pagos.fecha,
            a.codproveedor
           FROM (( SELECT cpnotacredito.codcpnotacredito AS codmovimiento,
                    'NC'::text AS tipomovimiento,
                    cpnotacredito.fecha,
                    cpnotacredito.nrodocumento AS numero,
                    cpnotacredito.concepto AS descripcion
                   FROM cpnotacredito
                UNION
                 SELECT cpabono.codcpabono AS codmovimiento,
                    'PAG'::text AS tipomovimiento,
                    cpabono.fecha,
                    cpabono.nrodocumento AS numero,
                    cpabono.concepto AS descripcion
                   FROM cpabono
                UNION
                 SELECT cpabono.codcpabono AS codmovimiento,
                    'ABO'::text AS tipomovimiento,
                    cpabono.fecha,
                    cpabono.nrodocumento AS numero,
                    cpabono.concepto AS descripcion
                   FROM cpabono) pagos
             JOIN (cppagadas cpp
             JOIN (h_cuentasporpagar hcxp
             JOIN ( SELECT a_1.codcompra AS codmovimiento,
                    'COMPRAS'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.femision,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM compras a_1
                UNION
                 SELECT a_1.codcpfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpfactura a_1
                UNION
                 SELECT a_1.codcpnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codproveedor,
                    a_1.codsucursal
                   FROM cpnotadebito a_1) a ON ((((a.codmovimiento = hcxp.codmovimiento) AND (a.tipomovimiento = (hcxp.tipomovimiento)::text)) AND (a.codsucursal = hcxp.codsucursal)))) ON (((((cpp.tipomovimientocp)::text = (hcxp.tipomovimiento)::text) AND (cpp.codmovimientocp = hcxp.codmovimiento)) AND (hcxp.codsucursal = cpp.codsucursalcp)))) ON (((cpp.codmovimientopago = pagos.codmovimiento) AND ((cpp.tipomovimientopago)::text = pagos.tipomovimiento))))) todo;


ALTER TABLE public.v_estadodecuenta OWNER TO postgres;

--
-- TOC entry 399 (class 1259 OID 98542)
-- Name: v_estadodecuenta_cxc; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_estadodecuenta_cxc AS
 SELECT todo.tipomovimiento,
    todo.fvencimiento,
    todo.numero,
    todo.concepto,
    todo.debito,
    todo.credito,
    todo.fecha,
    todo.codcliente,
    todo.codclientesucursal
   FROM ( SELECT a.tipomovimiento,
            b.fvencimiento,
            a.numero,
            b.concepto,
            0 AS debito,
            b.monto AS credito,
            a.fecha,
            a.codcliente,
            a.codclientesucursal
           FROM (( SELECT a_1.codventa AS codmovimiento,
                    'VENTAS'::text AS tipomovimiento,
                    a_1.numerofactura AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ventas a_1
                UNION
                 SELECT a_1.codccfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccfactura a_1
                UNION
                 SELECT a_1.codccnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccnotadebito a_1) a
             JOIN cuentasporcobrar b ON ((((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text)) AND (a.codsucursal = b.codsucursal))))
        UNION
         SELECT a.tipomovimiento,
            bh.fvencimiento,
            a.numero,
            bh.concepto,
            0 AS debito,
            bh.monto AS credito,
            a.fecha,
            a.codcliente,
            a.codclientesucursal
           FROM (( SELECT a_1.codventa AS codmovimiento,
                    'VENTAS'::text AS tipomovimiento,
                    a_1.numerofactura AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ventas a_1
                UNION
                 SELECT a_1.codccfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccfactura a_1
                UNION
                 SELECT a_1.codccnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccnotadebito a_1) a
             JOIN h_cuentasporcobrar bh ON ((((a.codmovimiento = bh.codmovimiento) AND (a.tipomovimiento = (bh.tipomovimiento)::text)) AND (a.codsucursal = bh.codsucursal))))
        UNION
         SELECT ccp.tipomovimientopago AS tipomovimiento,
            NULL::date AS fvencimiento,
            pagos.numero,
            pagos.descripcion AS concepto,
            ccp.monto AS debito,
            0 AS credito,
            pagos.fecha,
            a.codcliente,
            a.codclientesucursal
           FROM (( SELECT ccnotacredito.codccnotacredito AS codmovimiento,
                    'NC'::text AS tipomovimiento,
                    ccnotacredito.fecha,
                    ccnotacredito.nrodocumento AS numero,
                    ccnotacredito.concepto AS descripcion
                   FROM ccnotacredito
                UNION
                 SELECT ccabono.codccabono AS codmovimiento,
                    'PAG'::text AS tipomovimiento,
                    ccabono.fecha,
                    ccabono.nrodocumento AS numero,
                    ccabono.concepto AS descripcion
                   FROM ccabono
                UNION
                 SELECT ccabono.codccabono AS codmovimiento,
                    'ABO'::text AS tipomovimiento,
                    ccabono.fecha,
                    ccabono.nrodocumento AS numero,
                    ccabono.concepto AS descripcion
                   FROM ccabono) pagos
             JOIN (ccpagadas ccp
             JOIN (cuentasporcobrar cxc
             JOIN ( SELECT a_1.codventa AS codmovimiento,
                    'VENTAS'::text AS tipomovimiento,
                    a_1.numerofactura AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ventas a_1
                UNION
                 SELECT a_1.codccfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccfactura a_1
                UNION
                 SELECT a_1.codccnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccnotadebito a_1) a ON ((((a.codmovimiento = cxc.codmovimiento) AND (a.tipomovimiento = (cxc.tipomovimiento)::text)) AND (a.codsucursal = cxc.codsucursal)))) ON (((((ccp.tipomovimientocc)::text = (cxc.tipomovimiento)::text) AND (ccp.codmovimientocc = cxc.codmovimiento)) AND (cxc.codsucursal = ccp.codsucursalcc)))) ON (((ccp.codmovimientopago = pagos.codmovimiento) AND ((ccp.tipomovimientopago)::text = pagos.tipomovimiento))))
        UNION
         SELECT ccp.tipomovimientopago AS tipomovimiento,
            NULL::date AS fvencimiento,
            pagos.numero,
            pagos.descripcion AS concepto,
            sum(ccp.monto) AS debito,
            0 AS credito,
            pagos.fecha,
            a.codcliente,
            a.codclientesucursal
           FROM (( SELECT ccnotacredito.codccnotacredito AS codmovimiento,
                    'NC'::text AS tipomovimiento,
                    ccnotacredito.fecha,
                    ccnotacredito.nrodocumento AS numero,
                    ccnotacredito.concepto AS descripcion
                   FROM ccnotacredito
                UNION
                 SELECT ccabono.codccabono AS codmovimiento,
                    'PAG'::text AS tipomovimiento,
                    ccabono.fecha,
                    ccabono.nrodocumento AS numero,
                    ccabono.concepto AS descripcion
                   FROM ccabono
                UNION
                 SELECT ccabono.codccabono AS codmovimiento,
                    'ABO'::text AS tipomovimiento,
                    ccabono.fecha,
                    ccabono.nrodocumento AS numero,
                    ccabono.concepto AS descripcion
                   FROM ccabono) pagos
             JOIN (ccpagadas ccp
             JOIN (h_cuentasporcobrar hcxc
             JOIN ( SELECT a_1.codventa AS codmovimiento,
                    'VENTAS'::text AS tipomovimiento,
                    a_1.numerofactura AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ventas a_1
                UNION
                 SELECT a_1.codccfactura AS codmovimiento,
                    'FA'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccfactura a_1
                UNION
                 SELECT a_1.codccnotadebito AS codmovimiento,
                    'ND'::text AS tipomovimiento,
                    a_1.nrodocumento AS numero,
                    a_1.fecha,
                    a_1.codcliente,
                    a_1.codsucursal,
                    a_1.codclientesucursal
                   FROM ccnotadebito a_1) a ON ((((a.codmovimiento = hcxc.codmovimiento) AND (a.tipomovimiento = (hcxc.tipomovimiento)::text)) AND (a.codsucursal = hcxc.codsucursal)))) ON (((((ccp.tipomovimientocc)::text = (hcxc.tipomovimiento)::text) AND (ccp.codmovimientocc = hcxc.codmovimiento)) AND (hcxc.codsucursal = ccp.codsucursalcc)))) ON (((ccp.codmovimientopago = pagos.codmovimiento) AND ((ccp.tipomovimientopago)::text = pagos.tipomovimiento))))
          GROUP BY ccp.tipomovimientopago, pagos.numero, pagos.descripcion, pagos.fecha, a.codcliente, a.codclientesucursal) todo
  ORDER BY todo.numero;


ALTER TABLE public.v_estadodecuenta_cxc OWNER TO postgres;

--
-- TOC entry 400 (class 1259 OID 98547)
-- Name: v_formatoretencion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_formatoretencion AS
 SELECT c.femision AS fechafactura,
    c.nrodocumento AS numerofactura,
    c.nrocontrol AS numerocontrol,
    c.codcompra AS numeronotadebito,
    '' AS numeronotacredito,
    '1' AS tipotransaccion,
    '' AS numerofacturaafectada,
    ( SELECT sum((documentosretenciones.baseiva * ((1)::double precision + (documentosretenciones.poriva / (100)::double precision)))) AS sum
           FROM documentosretenciones
          WHERE ((documentosretenciones.codmovimiento = c.codcompra) AND ((documentosretenciones.tipomovimiento)::text = c.tipomovimiento))) AS totalincluyendoiva,
    COALESCE(( SELECT (documentosretenciones.baseiva * ((1)::double precision + (documentosretenciones.poriva / (100)::double precision)))
           FROM documentosretenciones
          WHERE (((documentosretenciones.codmovimiento = c.codcompra) AND ((documentosretenciones.tipomovimiento)::text = c.tipomovimiento)) AND (documentosretenciones.poriva = (0)::double precision))), (0)::double precision) AS comprassinderechoaiva,
    a.baseiva AS baseimponible,
    a.poriva AS porcentajeiva,
    (a.baseiva * (a.poriva / (100)::double precision)) AS iva,
    a.ivaretenido,
    b.codretencion
   FROM ((documentosretenciones a
     JOIN ( SELECT compras.femision,
            compras.nrodocumento,
            compras.nrocontrol,
            compras.codcompra,
            'COMPRAS'::text AS tipomovimiento
           FROM compras
        UNION
         SELECT cpnotadebito.fecha,
            cpnotadebito.nrodocumento,
            cpnotadebito.nrodocumento,
            cpnotadebito.codcpnotadebito,
            'ND'::text AS tipomovimiento
           FROM cpnotadebito) c ON ((((a.codmovimiento = c.codcompra) AND ((a.tipomovimiento)::text = c.tipomovimiento)) AND (a.poriva <> (0)::double precision))))
     JOIN retenciones b ON ((a.codretencion = b.codretencion)));


ALTER TABLE public.v_formatoretencion OWNER TO postgres;

--
-- TOC entry 401 (class 1259 OID 98552)
-- Name: v_historiadelproducto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historiadelproducto AS
 SELECT cg.descripcion,
    'CARGO'::text AS tipo,
    (cg.codcargo)::text AS numero,
    date_trunc('second'::text, ((((cg.fecha)::text || ' '::text) || (cg.hora)::text))::timestamp without time zone) AS fecha,
    pcg.coddeposito AS deposito,
    pcg.cantidad,
    pcg.unidad,
    (pcg.totalcosto / pcg.cantidad) AS costo,
    pcg.codproducto,
    pcg.factorconversion
   FROM (productoscargos pcg
     JOIN cargos cg ON (((pcg.codcargo = cg.codcargo) AND (pcg.codsucursal = cg.codsucursal))))
UNION
 SELECT dcg.descripcion,
    'DESCARGO'::text AS tipo,
    (dcg.coddescargo)::text AS numero,
    date_trunc('second'::text, ((((dcg.fecha)::text || ' '::text) || (dcg.hora)::text))::timestamp without time zone) AS fecha,
    pdcg.coddeposito AS deposito,
    (pdcg.cantidad * (- (1)::double precision)) AS cantidad,
    pdcg.unidad,
    (pdcg.totalcosto / pdcg.cantidad) AS costo,
    pdcg.codproducto,
    pdcg.factorconversion
   FROM (productosdescargos pdcg
     JOIN descargos dcg ON (((pdcg.coddescargo = dcg.coddescargo) AND (pdcg.codsucursal = dcg.codsucursal))))
UNION
 SELECT ('PROVEEDOR '::text || p.descripcion) AS descripcion,
    'COMPRA'::text AS tipo,
    c.nrodocumento AS numero,
    date_trunc('second'::text, ((((c.fregistro)::text || ' '::text) || (c.hora)::text))::timestamp without time zone) AS fecha,
    c.coddeposito AS deposito,
    cp.cantidad,
    cp.unidad,
    cp.costo,
    cp.codproducto,
    cp.factorconversion
   FROM (proveedores p
     JOIN (compras c
     JOIN comprasproductos cp ON ((c.codcompra = cp.codcompra))) ON ((p.codproveedor = c.codproveedor)))
UNION
 SELECT ('PROVEEDOR '::text || p.descripcion) AS descripcion,
    'DEVOLUCIÓN COMPRA'::text AS tipo,
    (d.coddevolucioncompra)::text AS numero,
    date_trunc('second'::text, ((((d.fdevolucion)::text || ' '::text) || (d.hora)::text))::timestamp without time zone) AS fecha,
    d.coddeposito AS deposito,
    (dp.cantidad * (- (1)::double precision)) AS cantidad,
    dp.unidad,
    dp.costo,
    dp.codproducto,
    dp.factorconversion
   FROM (devolucioncompraproductos dp
     JOIN (devolucioncompra d
     JOIN (compras c
     JOIN proveedores p ON ((c.codproveedor = p.codproveedor))) ON ((d.codcompra = c.codcompra))) ON ((dp.coddevolucioncompra = d.coddevolucioncompra)))
UNION
 SELECT ('CLIENTE '::text || p.descripcion) AS descripcion,
    'VENTA'::text AS tipo,
    c.numerofactura AS numero,
    date_trunc('second'::text, ((((c.fecha)::text || ' '::text) || (c.hora)::text))::timestamp without time zone) AS fecha,
    c.coddeposito AS deposito,
    (cp.cantidad * (- (1)::double precision)) AS cantidad,
    cp.unidad,
    cp.precio AS costo,
    cp.codproducto,
    cp.factorconversion
   FROM (clientes p
     JOIN (ventas c
     JOIN ventasproductos cp ON ((c.codventa = cp.codventa))) ON ((p.codcliente = c.codcliente)))
UNION
 SELECT ('CLIENTE '::text || c.descripcion) AS descripcion,
    'DEVOLUCIÓN VENTA'::text AS tipo,
    (d.coddevolucion)::text AS numero,
    date_trunc('second'::text, d.fecha) AS fecha,
    d.coddeposito AS deposito,
    dp.cantidad,
    vp.unidad,
    vp.precio AS costo,
    vp.codproducto,
    vp.factorconversion
   FROM (clientes c
     JOIN (ventas v
     JOIN (ventasproductos vp
     JOIN (devolucionventa d
     JOIN devolucionventaproductos dp ON (((d.coddevolucion = dp.coddevolucion) AND (d.codsucursal = dp.codsucursal)))) ON ((((vp.codventa = d.codventa) AND (vp.codsucursal = d.codsucursal)) AND (vp.codproducto = dp.codproducto)))) ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal)))) ON (((c.codcliente = v.codcliente) AND (c.codsucursal = v.codclientesucursal))))
UNION
 SELECT ('CLIENTE '::text || p.descripcion) AS descripcion,
    'NOTA ENTREGA'::text AS tipo,
    (c.codnotaentrega)::text AS numero,
    date_trunc('second'::text, ((((c.fecha)::text || ' '::text) || (c.hora)::text))::timestamp without time zone) AS fecha,
    c.coddeposito AS deposito,
    (cp.cantidad * (- (1)::double precision)) AS cantidad,
    cp.unidad,
    cp.precio AS costo,
    cp.codproducto,
    cp.factorconversion
   FROM (clientes p
     JOIN (nota_entrega c
     JOIN nota_entregaproductos cp ON ((c.codnotaentrega = cp.codnotaentrega))) ON ((p.codcliente = c.codcliente)));


ALTER TABLE public.v_historiadelproducto OWNER TO postgres;

--
-- TOC entry 402 (class 1259 OID 98557)
-- Name: v_historialcompras; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historialcompras AS
 SELECT c.codcompra,
    c.fregistro,
    c.nrodocumento,
    c.codproveedor,
    cp.totalcosto,
    cp.total,
    (((pag.caja + pag.cajachica) + pag.cheque) + pag.transaccion) AS contado,
    pag.credito
   FROM (( SELECT compraspagos.codcompra,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CAJA'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS caja,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CAJACHICA'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS cajachica,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CHEQUE'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'TRANSACCION'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS transaccion,
            sum(
                CASE
                    WHEN ((compraspagos.tipopago)::text = 'CREDITO'::text) THEN compraspagos.monto
                    ELSE (0)::real
                END) AS credito
           FROM compraspagos
          GROUP BY compraspagos.codcompra) pag
     JOIN (( SELECT cp_1.codcompra,
            COALESCE(sum((cp_1.costo * cp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum(((cp_1.costo * cp_1.cantidad) * ((1)::real + (cp_1.iva / (100)::real)))), (0)::real) AS total
           FROM comprasproductos cp_1
          GROUP BY cp_1.codcompra) cp
     JOIN compras c ON ((c.codcompra = cp.codcompra))) ON ((pag.codcompra = c.codcompra)));


ALTER TABLE public.v_historialcompras OWNER TO postgres;

--
-- TOC entry 403 (class 1259 OID 98562)
-- Name: v_historialdevolucion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historialdevolucion AS
 SELECT dv.coddevolucion,
    dv.codsucursal,
    dv.fecha,
    dv.codventa,
    dp.codcliente,
    dp.total,
    (((((pag.efectivo + pag.tdebito) + pag.cheque) + pag.transfer) + pag.tcredito) + pag.ticket) AS contado,
    pag.credito
   FROM (( SELECT devolucionventapago.coddevolucion,
            devolucionventapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CREDITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'EFECTIVO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TDEBITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS tdebito,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'CHEQUE'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TRANSFER'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TCREDITO'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS tcredito,
            sum(
                CASE
                    WHEN ((devolucionventapago.tipopago)::text = 'TICKET'::text) THEN devolucionventapago.monto
                    ELSE (0)::real
                END) AS ticket
           FROM devolucionventapago
          GROUP BY devolucionventapago.coddevolucion, devolucionventapago.codsucursal) pag
     JOIN (( SELECT dp_1.coddevolucion,
            dp_1.codsucursal,
            v.codcliente,
            COALESCE(sum((vp.costopromedio * dp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp.precio * dp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp.precio * dp_1.cantidad) * ((1)::real + (vp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp.precio * dp_1.cantidad) * (vp.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM (((devolucionventaproductos dp_1
             JOIN devolucionventa dv_1 ON ((dp_1.coddevolucion = dv_1.coddevolucion)))
             JOIN ventas v ON ((v.codventa = dv_1.codventa)))
             JOIN ventasproductos vp ON (((vp.codventa = dv_1.codventa) AND (dp_1.codproducto = vp.codproducto))))
          GROUP BY dp_1.coddevolucion, dp_1.codsucursal, v.codcliente
          ORDER BY dp_1.coddevolucion) dp
     JOIN devolucionventa dv ON (((dv.coddevolucion = dp.coddevolucion) AND (dv.codsucursal = dp.codsucursal)))) ON (((pag.coddevolucion = dv.coddevolucion) AND (dv.codsucursal = pag.codsucursal))));


ALTER TABLE public.v_historialdevolucion OWNER TO postgres;

--
-- TOC entry 404 (class 1259 OID 98567)
-- Name: v_historialdevolucioncompra; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historialdevolucioncompra AS
 SELECT dc.coddevolucioncompra,
    dc.codsucursal,
    dc.fdevolucion,
    dc.codcompra,
    dp.codproveedor,
    dp.total,
    (((((pag.efectivo + pag.tdebito) + pag.cheque) + pag.transfer) + pag.tcredito) + pag.ticket) AS contado,
    pag.credito
   FROM (( SELECT devolucioncomprapago.coddevolucioncompra,
            devolucioncomprapago.codsucursal,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'CREDITO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'EFECTIVO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'TDEBITO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS tdebito,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'CHEQUE'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'TRANSFER'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'TCREDITO'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS tcredito,
            sum(
                CASE
                    WHEN ((devolucioncomprapago.tipopago)::text = 'TICKET'::text) THEN devolucioncomprapago.monto
                    ELSE (0)::real
                END) AS ticket
           FROM devolucioncomprapago
          GROUP BY devolucioncomprapago.coddevolucioncompra, devolucioncomprapago.codsucursal) pag
     JOIN (( SELECT dp_1.coddevolucioncompra,
            c.codproveedor,
            COALESCE(sum((cp.costo * dp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((cp.costo * dp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((cp.costo * dp_1.cantidad) * ((1)::real + (cp.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((cp.costo * dp_1.cantidad) * (cp.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM (((devolucioncompraproductos dp_1
             JOIN devolucioncompra dc_1 ON ((dp_1.coddevolucioncompra = dc_1.coddevolucioncompra)))
             JOIN compras c ON ((c.codcompra = dc_1.codcompra)))
             JOIN comprasproductos cp ON (((cp.codcompra = dc_1.codcompra) AND (dp_1.codproducto = cp.codproducto))))
          GROUP BY dp_1.coddevolucioncompra, c.codproveedor
          ORDER BY dp_1.coddevolucioncompra) dp
     JOIN devolucioncompra dc ON ((dc.coddevolucioncompra = dp.coddevolucioncompra))) ON (((pag.coddevolucioncompra = dc.coddevolucioncompra) AND (dc.codsucursal = pag.codsucursal))));


ALTER TABLE public.v_historialdevolucioncompra OWNER TO postgres;

--
-- TOC entry 405 (class 1259 OID 98572)
-- Name: v_historialventas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_historialventas AS
 SELECT v.codventa,
    v.codsucursal,
    v.fecha,
    v.numerofactura,
    v.codcliente,
    vp.total,
    (((((pag.efectivo + pag.tdebito) + pag.cheque) + pag.transfer) + pag.tcredito) + pag.ticket) AS contado,
    pag.credito,
    v.estatus
   FROM (( SELECT ventaspagos.codventa,
            ventaspagos.codsucursal,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS credito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'EFECTIVO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS efectivo,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TDEBITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS tdebito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'CHEQUE'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS cheque,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TRANSFER'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS transfer,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TCREDITO'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS tcredito,
            sum(
                CASE
                    WHEN ((ventaspagos.tipopago)::text = 'TICKET'::text) THEN ventaspagos.monto
                    ELSE (0)::real
                END) AS ticket
           FROM ventaspagos
          GROUP BY ventaspagos.codventa, ventaspagos.codsucursal) pag
     JOIN (( SELECT vp_1.codventa,
            vp_1.codsucursal,
            COALESCE(sum((vp_1.costopromedio * vp_1.cantidad)), (0)::real) AS totalcosto,
            COALESCE(sum((vp_1.precio * vp_1.cantidad)), (0)::real) AS subtotal,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * ((1)::real + (vp_1.iva / (100)::real)))), (0)::real) AS total,
            COALESCE(sum(((vp_1.precio * vp_1.cantidad) * (vp_1.iva / (100)::real))), (0)::real) AS subtotaliva
           FROM ventasproductos vp_1
          GROUP BY vp_1.codventa, vp_1.codsucursal) vp
     JOIN ventas v ON (((v.codventa = vp.codventa) AND (v.codsucursal = vp.codsucursal)))) ON (((pag.codventa = v.codventa) AND (v.codsucursal = pag.codsucursal))));


ALTER TABLE public.v_historialventas OWNER TO postgres;

--
-- TOC entry 448 (class 1259 OID 131816)
-- Name: v_iva; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_iva AS
 SELECT iva.codiva AS "Código_int",
    iva.porcentaje AS "Porcentaje_float",
    iva.descripcion AS "Descripción_varchar"
   FROM iva;


ALTER TABLE public.v_iva OWNER TO postgres;

--
-- TOC entry 406 (class 1259 OID 98581)
-- Name: v_maquinas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_maquinas AS
 SELECT maquinas.codmaquina AS "Código_int",
    maquinas.marca AS "Marca_varchar",
    maquinas.modelo AS "Modelo_text",
    maquinas.serial AS "Serial_text",
        CASE
            WHEN (maquinas.estatus = true) THEN 'ACTIVO'::text
            ELSE 'NO ACTIVO'::text
        END AS "Estatus_text"
   FROM maquinas;


ALTER TABLE public.v_maquinas OWNER TO postgres;

--
-- TOC entry 407 (class 1259 OID 98585)
-- Name: v_marca; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_marca AS
 SELECT marca.codmarca AS "Código_int",
    marca.descripcion AS "Descripción_varchar"
   FROM marca;


ALTER TABLE public.v_marca OWNER TO postgres;

--
-- TOC entry 408 (class 1259 OID 98589)
-- Name: v_movimientoz; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_movimientoz AS
 SELECT movimiento_z.codmovimiento AS "Código_int",
    movimiento_z.ultimafactura AS "UltimaFactura_int",
    movimiento_z.memoria AS "Memoria_int",
    movimiento_z.totalventa AS "Totalventa_real"
   FROM movimiento_z
  WHERE (movimiento_z.estatus = true);


ALTER TABLE public.v_movimientoz OWNER TO postgres;

--
-- TOC entry 409 (class 1259 OID 98593)
-- Name: v_notacredito_compras; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_notacredito_compras AS
 SELECT (((a.codnotacredito)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Proveedor_varchar",
    a.facturaafecta AS "Factura Afectada_varchar",
    a.fecha AS "Fecha_date",
    b.codproveedor AS "Código Proveedor_varchar",
    a.montobruto AS "Monto_Double"
   FROM (notacredito_compras a
     JOIN proveedores b ON ((a.codproveedor = b.codproveedor)));


ALTER TABLE public.v_notacredito_compras OWNER TO postgres;

--
-- TOC entry 410 (class 1259 OID 98598)
-- Name: v_notacredito_ventas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_notacredito_ventas AS
 SELECT (((a.codnotacredito)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.facturaafecta AS "Factura Afectada_varchar",
    a.fecha AS "Fecha_date",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.montobruto AS "Monto_Double"
   FROM (notacredito_ventas a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codsucursal = b.codsucursal))));


ALTER TABLE public.v_notacredito_ventas OWNER TO postgres;

--
-- TOC entry 411 (class 1259 OID 98603)
-- Name: v_notaentrega; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_notaentrega AS
 SELECT (((a.codnotaentrega)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.fecha AS "Fecha_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.codvendedor AS "Vendedor_varchar",
    'V' AS ctid_varchar
   FROM (nota_entrega a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))))
  WHERE ((a.estatus)::text <> 'E'::text);


ALTER TABLE public.v_notaentrega OWNER TO postgres;

--
-- TOC entry 412 (class 1259 OID 98608)
-- Name: v_pedido; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_pedido AS
 SELECT (((a.codpedido)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.fecha AS "FechaCreacion_date",
    a.fechavencimiento AS "FechaVencimiento_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar"
   FROM (pedido a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))))
  WHERE ((a.estatus)::text <> 'E'::text);


ALTER TABLE public.v_pedido OWNER TO postgres;

--
-- TOC entry 413 (class 1259 OID 98613)
-- Name: v_presupuesto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_presupuesto AS
 SELECT (((a.codpresupuesto)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.fecha AS "FechaCreacion_date",
    a.fechavencimiento AS "FechaVencimiento_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.empresa AS "Empresa_varchar"
   FROM (presupuesto a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))));


ALTER TABLE public.v_presupuesto OWNER TO postgres;

--
-- TOC entry 446 (class 1259 OID 131806)
-- Name: v_producto; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_producto AS
 SELECT producto.codigo AS "Código_varchar",
    producto.nombre AS "Nombre_varchar",
    departamentos.descripcion AS "Departamento_text",
    marca.descripcion AS "Marca_varchar",
    producto.modelo AS "Modelo_varchar",
    producto.referencia AS "Referencia_varchar",
    (((iva.porcentaje)::text || '% '::text) || (iva.descripcion)::text) AS "Iva_varchar",
    producto.estado AS "Estado_varchar",
    producto.stockminimo AS "Stock Mínimo_varchar",
    producto.stockmaximo AS "Stock Máximo_varchar",
    producto.foto AS "Foto_varchar",
    producto.unidadprincipal AS "Unidad Principal_varchar",
    producto.unidadalterna AS "Unidad Alterna_varchar",
    producto.factorconversion AS "Factor de Conversion_varchar",
    producto.costoactual AS "Costo Actual_varchar",
    producto.costopromedio AS "Costo Promedio_varchar",
    producto.costoanterior AS "Costo Anterior_varchar",
    producto.preciominimo AS "Precio Mínimo_varchar",
    producto.preciomayor AS "Precio Mayor_varchar",
    producto.preciodetal AS "Precio Detal_varchar",
    producto.preciominimo1 AS "Prec. Min. con IVA_varchar",
    producto.preciomayor1 AS "Prec. May. con IVA_varchar",
    producto.preciodetal1 AS "Prec. Det. con IVA_varchar",
    producto.numero AS "Número_varchar",
    producto.fecha AS "Fecha_varchar",
    producto.texto AS "Texto_varchar",
    producto."varchar" AS "Varchar_varchar",
    producto."float" AS float_varchar,
    producto.codbarra AS "Código de Barras_varchar"
   FROM (((producto
     JOIN departamentos ON ((producto.coddepartamento = departamentos.coddepartamento)))
     JOIN iva ON ((producto.codiva = iva.codiva)))
     JOIN marca ON (((producto.codmarca = marca.codmarca) AND (NOT ((producto.estado)::text = 'INACTIVO'::text)))));


ALTER TABLE public.v_producto OWNER TO postgres;

--
-- TOC entry 447 (class 1259 OID 131811)
-- Name: v_producto_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_producto_i AS
 SELECT producto.codproducto AS "Código_int",
    producto.nombre AS "Nombre_int",
    departamentos.descripcion AS "Departamento_text",
    marca.descripcion AS "Marca_varchar",
    producto.modelo AS "Modelo_varchar",
    producto.referencia AS "Referencia_varchar",
    iva.descripcion AS "Iva_varchar",
    producto.estado AS "Estado_varchar",
    producto.stockminimo AS "Stock Mínimo_varchar",
    producto.stockmaximo AS "Stock Máximo_varchar",
    producto.foto AS "Foto_varchar",
    producto.unidadprincipal AS "Unidad Principal_varchar",
    producto.unidadalterna AS "Unidad Alterna_varchar",
    producto.factorconversion AS "Factor de Conversion_varchar",
    producto.costoactual AS "Costo Actual_varchar",
    producto.costopromedio AS "Costo Promedio_varchar",
    producto.costoanterior AS "Costo Anterior_varchar",
    producto.preciominimo AS "Precio Mínimo_varchar",
    producto.preciomayor AS "Precio Mayor_varchar",
    producto.preciodetal AS "Precio Detal_varchar",
    producto.preciominimo1 AS "Prec. Min. con IVA_varchar",
    producto.preciomayor1 AS "Prec. May. con IVA_varchar",
    producto.preciodetal1 AS "Prec. Det. con IVA_varchar",
    producto.numero AS "Número_varchar",
    producto.fecha AS "Fecha_varchar",
    producto.texto AS "Texto_varchar",
    producto."varchar" AS "Varchar_varchar",
    producto."float" AS float_varchar,
    producto.codbarra AS "Código de Barras_varchar"
   FROM (((producto
     JOIN departamentos ON ((producto.coddepartamento = departamentos.coddepartamento)))
     JOIN iva ON ((producto.codiva = iva.codiva)))
     JOIN marca ON (((producto.codmarca = marca.codmarca) AND ((producto.estado)::text = 'INACTIVO'::text))));


ALTER TABLE public.v_producto_i OWNER TO postgres;

--
-- TOC entry 414 (class 1259 OID 98628)
-- Name: v_proveedor; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_proveedor AS
 SELECT proveedores.codproveedor AS "Código_varchar",
    proveedores.descripcion AS "Descripción_text",
    proveedores.rif AS "RIF_varchar",
    proveedores.clase AS "Clase_varchar",
    proveedores.direccion AS "Dirección_text",
    proveedores.direccion2 AS "Dirección 2_text",
    proveedores.telefonos AS "Teléfonos_varchar",
    proveedores.email AS "E-mail_text",
    proveedores.numerodefax AS "Número de fax_varchar",
    proveedores.diasdecredito AS "Días de crédito_int",
    proveedores.limitedecredito AS "Límite de Crédito_float",
    proveedores.formadepago AS "Forma de pago_varchar",
    proveedores.porcderetencion AS "% de Retención_int",
    proveedores.fechadeinicio AS "Fecha de Inicio_date",
    proveedores.tipoproveedor AS "Tipo Proveedor_varchar"
   FROM proveedores
  WHERE (proveedores.estatus = true);


ALTER TABLE public.v_proveedor OWNER TO postgres;

--
-- TOC entry 415 (class 1259 OID 98632)
-- Name: v_proveedor_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_proveedor_i AS
 SELECT proveedores.codproveedor AS "Código_varchar",
    proveedores.descripcion AS "Descripción_text",
    proveedores.rif AS "RIF_varchar",
    proveedores.clase AS "Clase_varchar",
    proveedores.direccion AS "Dirección_text",
    proveedores.direccion2 AS "Dirección 2_text",
    proveedores.telefonos AS "Teléfonos_varchar",
    proveedores.email AS "E-mail_text",
    proveedores.numerodefax AS "Número de fax_varchar",
    proveedores.diasdecredito AS "Días de crédito_int",
    proveedores.limitedecredito AS "Límite de Crédito_float",
    proveedores.formadepago AS "Forma de pago_varchar",
    proveedores.porcderetencion AS "% de Retención_int",
    proveedores.fechadeinicio AS "Fecha de Inicio_date",
    proveedores.tipoproveedor AS "Tipo Proveedor_varchar"
   FROM proveedores
  WHERE (proveedores.estatus = false);


ALTER TABLE public.v_proveedor_i OWNER TO postgres;

--
-- TOC entry 416 (class 1259 OID 98636)
-- Name: v_puntodeventa; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_puntodeventa AS
 SELECT puntodeventa.codpuntoventa AS "Código_int",
    bancos.nombre AS "Nombre_text",
    puntodeventa.serial AS "Serial_varchar",
    puntodeventa.nrodecuenta AS "Nrodecuenta_text",
    puntodeventa.marca AS "Marca_text",
    puntodeventa.modelo AS "Modelo_text"
   FROM puntodeventa,
    bancos
  WHERE (puntodeventa.codbanco = bancos.codbanco);


ALTER TABLE public.v_puntodeventa OWNER TO postgres;

--
-- TOC entry 417 (class 1259 OID 98640)
-- Name: v_rep_cxc_general; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_rep_cxc_general AS
 SELECT (((a.codcliente)::text || '-'::text) || (a.codsucursal)::text) AS codcliente,
    a.descripcion AS nombrecliente,
    a.direccion AS direccion1,
    a.direccion2,
    a.telefonos AS telefono,
    a.tipomovimiento,
    a.fecha,
    b.fvencimiento,
    '0' AS dias,
    a.documento AS numero,
    b.monto
   FROM (( SELECT a_1.codventa AS codmovimiento,
            'VENTAS'::text AS tipomovimiento,
            a_1.numerofactura AS documento,
            a_1.fecha,
            a_1.codcliente,
            a_1.codsucursal,
            a_1.codclientesucursal,
            ( SELECT clientes.descripcion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS descripcion,
            ( SELECT clientes.direccion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion,
            ( SELECT clientes.direccion2
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion2,
            ( SELECT clientes.telefonos
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS telefonos
           FROM ventas a_1
        UNION
         SELECT a_1.codccfactura AS codmovimiento,
            'FA'::text AS tipomovimiento,
            a_1.nrodocumento AS documento,
            a_1.fecha,
            a_1.codcliente,
            a_1.codsucursal,
            a_1.codclientesucursal,
            ( SELECT clientes.descripcion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS descripcion,
            ( SELECT clientes.direccion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion,
            ( SELECT clientes.direccion2
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion2,
            ( SELECT clientes.telefonos
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS telefonos
           FROM ccfactura a_1) a
     JOIN cuentasporcobrar b ON ((((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text)) AND (a.codsucursal = b.codsucursal))))
  ORDER BY a.descripcion;


ALTER TABLE public.v_rep_cxc_general OWNER TO postgres;

--
-- TOC entry 418 (class 1259 OID 98645)
-- Name: v_retencion_v; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_retencion_v AS
 SELECT retenciones."Código_varchar",
    retenciones."Cliente_varchar",
    retenciones."Factura_text",
    retenciones."Monto_float",
    retenciones."Fecha_varchar"
   FROM ( SELECT rv.codretencion AS "Código_varchar",
            c.descripcion AS "Cliente_varchar",
            rv.documento AS "Factura_text",
            rv.montoretenido AS "Monto_float",
            r.fechaemision AS "Fecha_varchar"
           FROM ((retenciones_ventas_detalles rv
             JOIN retenciones_ventas r ON (((r.codretencion = rv.codretencion) AND ((r.estatus)::text <> 'E'::text))))
             JOIN clientes c ON ((c.codcliente = rv.codcliente)))
          ORDER BY r.fechaemision, rv.documento) retenciones
  ORDER BY retenciones."Cliente_varchar";


ALTER TABLE public.v_retencion_v OWNER TO postgres;

--
-- TOC entry 419 (class 1259 OID 98650)
-- Name: v_sucursal; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_sucursal AS
 SELECT sucursal.codsucursal AS "Código_int",
    sucursal.descripcion AS "Descripción_text",
    sucursal.ip AS "IP_varchar",
    sucursal.direccion AS "Dirección_text",
    sucursal.telefonos AS "Teléfonos_varchar"
   FROM sucursal
  WHERE (sucursal.estatus = true);


ALTER TABLE public.v_sucursal OWNER TO postgres;

--
-- TOC entry 420 (class 1259 OID 98654)
-- Name: v_sucursal_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_sucursal_i AS
 SELECT sucursal.codsucursal AS "Código_int",
    sucursal.descripcion AS "Descripción_text",
    sucursal.ip AS "IP_varchar",
    sucursal.direccion AS "Dirección_text",
    sucursal.telefonos AS "Teléfonos_varchar"
   FROM sucursal
  WHERE (sucursal.estatus = false);


ALTER TABLE public.v_sucursal_i OWNER TO postgres;

--
-- TOC entry 421 (class 1259 OID 98658)
-- Name: v_tipocomision; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_tipocomision AS
 SELECT tipocomision.id AS "Código_int",
    tipocomision.nombre AS "Nombre_varchar",
    tipocomision.abreviatura AS "Abreviatura_text",
    tipocomision.estatus AS "Estatus_bool"
   FROM tipocomision;


ALTER TABLE public.v_tipocomision OWNER TO postgres;

--
-- TOC entry 422 (class 1259 OID 98662)
-- Name: v_tipocomisiondef; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_tipocomisiondef AS
 SELECT tipocomisiondef.id AS "Código_int",
    tipocomisiondef.nombre AS "Nombre_varchar",
    tipocomisiondef.estatus AS "Estatus_bool",
    tipocomision.nombre AS "Comision_varchar"
   FROM (tipocomisiondef
     JOIN tipocomision ON ((tipocomision.id = tipocomisiondef.tipocomision_id)));


ALTER TABLE public.v_tipocomisiondef OWNER TO postgres;

--
-- TOC entry 423 (class 1259 OID 98666)
-- Name: v_tipoexcepcion; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_tipoexcepcion AS
 SELECT tipoexcepcion.id AS "Código_int",
    tipoexcepcion.nombre AS "Nombre_varchar",
    tipoexcepcion.abreviatura AS "Abreviatura_text",
    tipoexcepcion.estatus AS "Estatus_bool"
   FROM tipoexcepcion;


ALTER TABLE public.v_tipoexcepcion OWNER TO postgres;

--
-- TOC entry 424 (class 1259 OID 98670)
-- Name: v_tipoexcepciondef; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_tipoexcepciondef AS
 SELECT tipoexcepciondef.id AS "Código_int",
    tipoexcepciondef.nombre AS "Nombre_varchar",
    tipoexcepciondef.estatus AS "Estatus_bool",
    tipoexcepcion.nombre AS "Excepcion_varchar"
   FROM (tipoexcepciondef
     JOIN tipoexcepcion ON ((tipoexcepcion.id = tipoexcepciondef.tipoexcepcion_id)));


ALTER TABLE public.v_tipoexcepciondef OWNER TO postgres;

--
-- TOC entry 425 (class 1259 OID 98674)
-- Name: v_vendedor; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_vendedor AS
 SELECT (((vendedores.codvendedor)::text || '-'::text) || (vendedores.codsucursal)::text) AS "Código_varchar",
    vendedores.descripcion AS "Descripción_text",
    vendedores.clase AS "Clase_varchar",
    vendedores.nivelprecios2 AS "Nivel de Precios_varchar",
    vendedores.tabladeventas2 AS "Tabla de Ventas_varchar",
    vendedores.tabladecobros2 AS "Tabla de Cobros_varchar",
    vendedores.tabladeutilidad2 AS "Tabla de Utilidad_varchar",
    vendedores.diastolerancia2 AS "Días de Tolerancia_varchar"
   FROM vendedores
  WHERE ((vendedores.codsucursal = ( SELECT configuracion.sucursal
           FROM configuracion)) AND (vendedores.estatus = true));


ALTER TABLE public.v_vendedor OWNER TO postgres;

--
-- TOC entry 426 (class 1259 OID 98679)
-- Name: v_vendedor2; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_vendedor2 AS
 SELECT vendedores.codvendedor AS "Código_int",
    vendedores.descripcion AS "Descripción_text",
    vendedores.clase AS "Clase_varchar",
    vendedores.nivelprecios2 AS "Nivel de Precios_varchar",
    vendedores.tabladeventas2 AS "Tabla de Ventas_varchar",
    vendedores.tabladecobros2 AS "Tabla de Cobros_varchar",
    vendedores.tabladeutilidad2 AS "Tabla de Utilidad_varchar",
    vendedores.diastolerancia2 AS "Días de Tolerancia_varchar"
   FROM vendedores
  WHERE ((vendedores.codsucursal = ( SELECT configuracion.sucursal
           FROM configuracion)) AND (vendedores.estatus = true));


ALTER TABLE public.v_vendedor2 OWNER TO postgres;

--
-- TOC entry 427 (class 1259 OID 98683)
-- Name: v_vendedor_i; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_vendedor_i AS
 SELECT vendedores.codvendedor AS "Código_int",
    vendedores.descripcion AS "Descripción_text",
    vendedores.clase AS "Clase_varchar",
    vendedores.nivelprecios2 AS "Nivel de Precios_varchar",
    vendedores.tabladeventas2 AS "Tabla de Ventas_varchar",
    vendedores.tabladecobros2 AS "Tabla de Cobros_varchar",
    vendedores.tabladeutilidad2 AS "Tabla de Utilidad_varchar",
    vendedores.diastolerancia2 AS "Días de Tolerancia_varchar"
   FROM vendedores
  WHERE ((vendedores.codsucursal = ( SELECT configuracion.sucursal
           FROM configuracion)) AND (vendedores.estatus = false));


ALTER TABLE public.v_vendedor_i OWNER TO postgres;

--
-- TOC entry 429 (class 1259 OID 98692)
-- Name: v_ventaespera; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventaespera AS
 SELECT a.codventaespera AS "Código_int",
    a.fecha AS "Fecha_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    b.descripcion AS "Cliente_varchar"
   FROM (ventaespera a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))));


ALTER TABLE public.v_ventaespera OWNER TO postgres;

--
-- TOC entry 442 (class 1259 OID 115457)
-- Name: v_ventas; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventas AS
 SELECT (((a.codventa)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.numerofactura AS "Factura_varchar",
    a.fecha AS "Fecha_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.codvendedor AS "Vendedor_varchar",
    0 AS ctid_int
   FROM (ventas a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))));


ALTER TABLE public.v_ventas OWNER TO postgres;

--
-- TOC entry 430 (class 1259 OID 98702)
-- Name: v_ventas2; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventas2 AS
 SELECT (((a.codventa)::text || '-'::text) || (a.codsucursal)::text) AS "Código_varchar",
    b.descripcion AS "Cliente_varchar",
    a.numerofactura AS "Factura_varchar",
    a.fecha AS "Fecha_date",
    a.coddeposito AS "Depósito_int",
    (((b.codcliente)::text || '-'::text) || (b.codsucursal)::text) AS "Código Cliente_varchar",
    a.codvendedor AS "Vendedor_varchar",
    0 AS ctid
   FROM (ventas a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))));


ALTER TABLE public.v_ventas2 OWNER TO postgres;

--
-- TOC entry 431 (class 1259 OID 98707)
-- Name: v_ventas_vendedor; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventas_vendedor AS
 SELECT a.codventa,
    a.codsucursal,
    b.descripcion AS cliente,
    a.numerofactura,
    a.fecha,
    a.coddeposito,
    b.codcliente,
    a.codvendedor
   FROM (ventas a
     JOIN clientes b ON (((a.codcliente = b.codcliente) AND (a.codclientesucursal = b.codsucursal))))
  WHERE ((a.estatus)::text <> 'E'::text);


ALTER TABLE public.v_ventas_vendedor OWNER TO postgres;

--
-- TOC entry 432 (class 1259 OID 98712)
-- Name: v_ventasfacturas_cxc; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventasfacturas_cxc AS
 SELECT (((a.codcliente)::text || '-'::text) || (a.codsucursal)::text) AS codcliente,
    a.descripcion,
    a.direccion,
    a.tipomovimiento,
    a.documento,
    b.fvencimiento,
    a.fecha,
    b.monto,
    a.codmovimiento,
    '' AS ctid
   FROM (( SELECT a_1.codventa AS codmovimiento,
            'VENTAS'::text AS tipomovimiento,
            a_1.numerofactura AS documento,
            a_1.fecha,
            a_1.codcliente,
            a_1.codsucursal,
            a_1.codclientesucursal,
            ( SELECT clientes.descripcion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS descripcion,
            ( SELECT clientes.direccion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion
           FROM ventas a_1
        UNION
         SELECT a_1.codccfactura AS codmovimiento,
            'FA'::text AS tipomovimiento,
            a_1.nrodocumento AS documento,
            a_1.fecha,
            a_1.codcliente,
            a_1.codsucursal,
            a_1.codclientesucursal,
            ( SELECT clientes.descripcion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS descripcion,
            ( SELECT clientes.direccion
                   FROM clientes
                  WHERE ((clientes.codcliente = a_1.codcliente) AND (clientes.codsucursal = a_1.codsucursal))) AS direccion
           FROM ccfactura a_1) a
     JOIN cuentasporcobrar b ON ((((a.codmovimiento = b.codmovimiento) AND (a.tipomovimiento = (b.tipomovimiento)::text)) AND (a.codsucursal = b.codsucursal))))
  ORDER BY a.descripcion;


ALTER TABLE public.v_ventasfacturas_cxc OWNER TO postgres;

--
-- TOC entry 433 (class 1259 OID 98717)
-- Name: v_ventasposiblesparaanular; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW v_ventasposiblesparaanular AS
 SELECT v.codventa,
    v.codsucursal,
    v.numerofactura,
    v.fecha,
    v.codcliente,
    v.codvendedor,
    v.codclientesucursal,
    v.hora,
    v.coddeposito,
    c.rif,
    c.descripcion
   FROM (( SELECT ventas.codventa,
            ventas.codsucursal
           FROM ventas
        EXCEPT
         SELECT devolucionventa.codventa,
            devolucionventa.codsucursalventa AS codsucursal
           FROM devolucionventa) dv
     JOIN (ventas v
     JOIN clientes c ON (((v.codcliente = c.codcliente) AND (v.codclientesucursal = c.codsucursal)))) ON (((dv.codventa = v.codventa) AND (dv.codsucursal = v.codsucursal))));


ALTER TABLE public.v_ventasposiblesparaanular OWNER TO postgres;

