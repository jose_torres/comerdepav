﻿-- Tabla Ventas --
ALTER TABLE ventas ALTER COLUMN recibido TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN montobruto TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN montoexento TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN baseimp1 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN baseimp2 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN baseimp3 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN ivaimp1 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN ivaimp2 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN ivaimp3 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN porimp1 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN porimp2 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN porimp3 TYPE numeric(150,2);
ALTER TABLE ventas ALTER COLUMN retencion TYPE numeric(150,2);
-- Tabla ventaesperaproductos --
ALTER TABLE ventaesperaproductos ALTER COLUMN cantidad TYPE numeric(150,2);
ALTER TABLE ventaesperaproductos ALTER COLUMN precio TYPE numeric(150,2);
ALTER TABLE ventaesperaproductos ALTER COLUMN iva TYPE numeric(150,2);
ALTER TABLE ventaesperaproductos ALTER COLUMN factorconversion TYPE numeric(150,2);
-- Tabla ventaspagos --
ALTER TABLE ventaspagos ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE ventaspagos ALTER COLUMN montorecibido TYPE numeric(150,2);
-- TABLE ventasproductos --
ALTER TABLE ventasproductos ALTER COLUMN cantidad TYPE numeric(150,2);
ALTER TABLE ventasproductos ALTER COLUMN precio TYPE numeric(150,2);
ALTER TABLE ventasproductos ALTER COLUMN iva TYPE numeric(150,2);
--ALTER TABLE ventasproductos ALTER COLUMN factorconversion TYPE numeric(150,2);
ALTER TABLE ventasproductos ALTER COLUMN costopromedio TYPE numeric(150,2);
ALTER TABLE ventasproductos ALTER COLUMN factorconversion2 TYPE numeric(150,2);
-- Table ccabonopago --
ALTER TABLE ccabonopago ALTER COLUMN monto TYPE numeric(150,2);
-- Table ccfactura --
ALTER TABLE ccfactura ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE ccfactura ALTER COLUMN baseimp1 TYPE numeric(150,2);
ALTER TABLE ccfactura ALTER COLUMN baseimp2 TYPE numeric(150,2);
ALTER TABLE ccfactura ALTER COLUMN baseimp3 TYPE numeric(150,2);
ALTER TABLE ccfactura ALTER COLUMN porimp1 TYPE numeric(150,2);
ALTER TABLE ccfactura ALTER COLUMN porimp2 TYPE numeric(150,2);
ALTER TABLE ccfactura ALTER COLUMN porimp3 TYPE numeric(150,2);
ALTER TABLE ccfactura ALTER COLUMN montoexento TYPE numeric(150,2);
-- Table ccnotacredito --
ALTER TABLE ccnotacredito ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE ccnotacredito ALTER COLUMN saldo TYPE numeric(150,2);
-- Table ccnotadebito --
ALTER TABLE ccnotadebito ALTER COLUMN monto TYPE numeric(150,2);
-- Table ccpagadas --
ALTER TABLE ccpagadas ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE ccpagadas ALTER COLUMN iva TYPE numeric(150,2);
-- Table ccpagadasiva --
ALTER TABLE ccpagadasiva ALTER COLUMN poriva TYPE numeric(150,2);
ALTER TABLE ccpagadasiva ALTER COLUMN baseiva TYPE numeric(150,2);
-- Table cheque --
ALTER TABLE cheque ALTER COLUMN monto TYPE numeric(150,2);
-- Table clientes --
ALTER TABLE clientes ALTER COLUMN limitecredito TYPE numeric(150,2);
ALTER TABLE clientes ALTER COLUMN porcdescuento TYPE numeric(10,2);
ALTER TABLE clientes ALTER COLUMN porcentajepatente TYPE numeric(10,2);
-- Table compras --
ALTER TABLE compras ALTER COLUMN descuento TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN pordescuento TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN montobruto TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN montoexento TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN baseimp1 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN baseimp2 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN baseimp3 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN ivaimp1 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN ivaimp2 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN ivaimp3 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN porimp1 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN porimp2 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN porimp3 TYPE numeric(150,2);
ALTER TABLE compras ALTER COLUMN retencion TYPE numeric(150,2);
-- Table comprasesperaproductos --
ALTER TABLE comprasesperaproductos ALTER COLUMN costo TYPE numeric(150,2);
ALTER TABLE comprasesperaproductos ALTER COLUMN iva TYPE numeric(150,2);
ALTER TABLE comprasesperaproductos ALTER COLUMN cantidad TYPE numeric(150,2);
ALTER TABLE comprasesperaproductos ALTER COLUMN factorconversion TYPE numeric(150,2);
-- Table compraspagos --
ALTER TABLE compraspagos ALTER COLUMN monto TYPE numeric(150,2);
-- Table comprasproductos --
ALTER TABLE comprasproductos ALTER COLUMN costo TYPE numeric(150,2);
ALTER TABLE comprasproductos ALTER COLUMN iva TYPE numeric(150,2);
ALTER TABLE comprasproductos ALTER COLUMN cantidad TYPE numeric(150,2);
ALTER TABLE comprasproductos ALTER COLUMN factorconversion TYPE numeric(150,2);
ALTER TABLE comprasproductos ALTER COLUMN costodescuento TYPE numeric(150,2);
ALTER TABLE comprasproductos ALTER COLUMN costoanterior TYPE numeric(150,2);
-- Table cpabonopago --
ALTER TABLE cpabonopago ALTER COLUMN monto TYPE numeric(150,2);
-- Table cpfactura --
ALTER TABLE cpfactura ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE cpfactura ALTER COLUMN baseimp1 TYPE numeric(150,2);
ALTER TABLE cpfactura ALTER COLUMN baseimp2 TYPE numeric(150,2);
ALTER TABLE cpfactura ALTER COLUMN baseimp3 TYPE numeric(150,2);
ALTER TABLE cpfactura ALTER COLUMN porimp1 TYPE numeric(150,2);
ALTER TABLE cpfactura ALTER COLUMN porimp2 TYPE numeric(150,2);
ALTER TABLE cpfactura ALTER COLUMN porimp3 TYPE numeric(150,2);
ALTER TABLE cpfactura ALTER COLUMN montoexento TYPE numeric(150,2);
-- Table cpnotacredito --
ALTER TABLE cpnotacredito ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE cpnotacredito ALTER COLUMN saldo TYPE numeric(150,2);
-- Table cpnotadebito --
ALTER TABLE cpnotadebito ALTER COLUMN monto TYPE numeric(150,2);
-- Table cppagadas --
ALTER TABLE cppagadas ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE cppagadas ALTER COLUMN iva TYPE numeric(150,2);
-- Table cppagadasiva --
ALTER TABLE cppagadasiva ALTER COLUMN poriva TYPE numeric(150,2);
ALTER TABLE cppagadasiva ALTER COLUMN baseiva TYPE numeric(150,2);
-- Table cuentasporcobrar --
ALTER TABLE cuentasporcobrar ALTER COLUMN monto TYPE numeric(150,2);
-- Table cuentasporpagar --
ALTER TABLE cuentasporpagar ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE cuentasporpagar ALTER COLUMN iva TYPE numeric(150,2);
-- Table devolucioncompra --
--ALTER TABLE devolucioncompra ALTER COLUMN devoluciontotal TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN montobruto TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN montoexento TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN baseimp1 TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN baseimp2 TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN baseimp3 TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN porimp1 TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN porimp2 TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN porimp3 TYPE numeric(150,2);
ALTER TABLE devolucioncompra ALTER COLUMN retencion TYPE numeric(150,2);
-- Table devolucioncomprapago --
ALTER TABLE devolucioncomprapago ALTER COLUMN monto TYPE numeric(150,2);
-- Table devolucioncompraproductos --
ALTER TABLE devolucioncompraproductos ALTER COLUMN cantidad TYPE numeric(150,2);
ALTER TABLE devolucioncompraproductos ALTER COLUMN factorconversion TYPE numeric(150,2);
ALTER TABLE devolucioncompraproductos ALTER COLUMN costo TYPE numeric(150,2);
ALTER TABLE devolucioncompraproductos ALTER COLUMN iva TYPE numeric(150,2);
-- Table devolucionventa --
--ALTER TABLE devolucionventa ALTER COLUMN devoluciontotal TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN montobruto TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN montoexento TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN baseimp1 TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN baseimp2 TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN baseimp3 TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN porimp1 TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN porimp2 TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN porimp3 TYPE numeric(150,2);
ALTER TABLE devolucionventa ALTER COLUMN retencion TYPE numeric(150,2);
-- Table devolucionventacreditopagos --
ALTER TABLE devolucionventacreditopagos ALTER COLUMN monto TYPE numeric(150,2);
-- Table devolucionventapago --
ALTER TABLE devolucionventapago ALTER COLUMN monto TYPE numeric(150,2);
-- Table devolucionventaproductos --
ALTER TABLE devolucionventaproductos ALTER COLUMN cantidad TYPE numeric(150,2);
-- Table documentosretenciones --
ALTER TABLE documentosretenciones ALTER COLUMN montoretenido TYPE numeric(150,2);
ALTER TABLE documentosretenciones ALTER COLUMN poriva TYPE numeric(150,2);
ALTER TABLE documentosretenciones ALTER COLUMN baseiva TYPE numeric(150,2);
ALTER TABLE documentosretenciones ALTER COLUMN ivaretenido TYPE numeric(150,2);
-- Table h_cuentasporcobrar --
ALTER TABLE h_cuentasporcobrar ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE h_cuentasporcobrar ALTER COLUMN iva TYPE numeric(150,2);
-- Table h_cuentasporpagar --
ALTER TABLE h_cuentasporpagar ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE h_cuentasporpagar ALTER COLUMN iva TYPE numeric(150,2);
-- Table h_producto --
ALTER TABLE h_producto ALTER COLUMN costonuevo TYPE numeric(150,2);
ALTER TABLE h_producto ALTER COLUMN costoanterior TYPE numeric(150,2);
ALTER TABLE h_producto ALTER COLUMN preciominimonuevo TYPE numeric(150,2);
ALTER TABLE h_producto ALTER COLUMN preciomayornuevo TYPE numeric(150,2);
ALTER TABLE h_producto ALTER COLUMN preciodetalnuevo TYPE numeric(150,2);
ALTER TABLE h_producto ALTER COLUMN preciominimoanterior TYPE numeric(150,2);
ALTER TABLE h_producto ALTER COLUMN preciomayoranterior TYPE numeric(150,2);
ALTER TABLE h_producto ALTER COLUMN preciodetalanterior TYPE numeric(150,2);
-- Table inventariopedidos --
ALTER TABLE inventariopedidos ALTER COLUMN cantidad TYPE numeric(150,2);
ALTER TABLE inventariopedidos ALTER COLUMN cantidadaprobada TYPE numeric(150,2);
-- Table inventarioproductos --
ALTER TABLE inventarioproductos ALTER COLUMN existenciat TYPE numeric(150,2);
ALTER TABLE inventarioproductos ALTER COLUMN existenciaf TYPE numeric(150,2);
ALTER TABLE inventarioproductos ALTER COLUMN diferencia TYPE numeric(150,2);
ALTER TABLE inventarioproductos ALTER COLUMN costoactual TYPE numeric(150,2);
-- Table iva --
ALTER TABLE iva ALTER COLUMN porcentaje TYPE numeric(150,2);
ALTER TABLE iva ALTER COLUMN montocomparar TYPE numeric(150,2);
-- Table movbancarios --
ALTER TABLE movbancarios ALTER COLUMN monto TYPE numeric(150,2);
ALTER TABLE movbancarios ALTER COLUMN saldo TYPE numeric(150,2);
-- Table movimientoivacc --
ALTER TABLE movimientoivacc ALTER COLUMN poriva TYPE numeric(150,2);
ALTER TABLE movimientoivacc ALTER COLUMN baseiva TYPE numeric(150,2);
-- Table movimientoivacp --
ALTER TABLE movimientoivacp ALTER COLUMN poriva TYPE numeric(150,2);
ALTER TABLE movimientoivacp ALTER COLUMN baseiva TYPE numeric(150,2);
