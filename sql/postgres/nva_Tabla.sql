--
-- PostgreSQL database dump
--

-- Started on 2017-03-16 08:07:01 VET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 421 (class 1259 OID 335456)
-- Dependencies: 2712 2713 2714 2715 6
-- Name: cuadrediarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cuadrediarios (
    id bigint NOT NULL,
    nro integer NOT NULL,
    fecha date DEFAULT '1900-01-01'::date,
    estatus integer DEFAULT 0 NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone
);


ALTER TABLE public.cuadrediarios OWNER TO postgres;

--
-- TOC entry 420 (class 1259 OID 335454)
-- Dependencies: 6 421
-- Name: cuadrediarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cuadrediarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.cuadrediarios_id_seq OWNER TO postgres;

--
-- TOC entry 2746 (class 0 OID 0)
-- Dependencies: 420
-- Name: cuadrediarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cuadrediarios_id_seq OWNED BY cuadrediarios.id;


--
-- TOC entry 2747 (class 0 OID 0)
-- Dependencies: 420
-- Name: cuadrediarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cuadrediarios_id_seq', 1, false);


--
-- TOC entry 423 (class 1259 OID 335679)
-- Dependencies: 2717 2718 2719 6
-- Name: dinerodenominaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE dinerodenominaciones (
    id bigint NOT NULL,
    denominacion text NOT NULL,
    valor double precision,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    fin text DEFAULT 'DINERO'::text NOT NULL
);


ALTER TABLE public.dinerodenominaciones OWNER TO postgres;

--
-- TOC entry 422 (class 1259 OID 335677)
-- Dependencies: 423 6
-- Name: dinerodenominaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE dinerodenominaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.dinerodenominaciones_id_seq OWNER TO postgres;

--
-- TOC entry 2748 (class 0 OID 0)
-- Dependencies: 422
-- Name: dinerodenominaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE dinerodenominaciones_id_seq OWNED BY dinerodenominaciones.id;


--
-- TOC entry 2749 (class 0 OID 0)
-- Dependencies: 422
-- Name: dinerodenominaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('dinerodenominaciones_id_seq', 20, true);


--
-- TOC entry 429 (class 1259 OID 335747)
-- Dependencies: 2730 2731 6
-- Name: documentotipo; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE documentotipo (
    id bigint NOT NULL,
    descorta text NOT NULL,
    descripcion text NOT NULL,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone NOT NULL,
    accion text
);


ALTER TABLE public.documentotipo OWNER TO postgres;

--
-- TOC entry 428 (class 1259 OID 335745)
-- Dependencies: 6 429
-- Name: documentotipo_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE documentotipo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.documentotipo_id_seq OWNER TO postgres;

--
-- TOC entry 2750 (class 0 OID 0)
-- Dependencies: 428
-- Name: documentotipo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE documentotipo_id_seq OWNED BY documentotipo.id;


--
-- TOC entry 2751 (class 0 OID 0)
-- Dependencies: 428
-- Name: documentotipo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('documentotipo_id_seq', 1, false);


--
-- TOC entry 425 (class 1259 OID 335715)
-- Dependencies: 2721 2722 2723 2724 2725 2726 2727 2728 6
-- Name: sucdepositos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sucdepositos (
    id bigint NOT NULL,
    movbancario_id bigint DEFAULT 0 NOT NULL,
    codsucursal bigint DEFAULT 0 NOT NULL,
    monto double precision DEFAULT 0.00 NOT NULL,
    fecha date,
    created timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    modified timestamp without time zone DEFAULT '1900-01-01 00:00:00'::timestamp without time zone,
    caja_id bigint DEFAULT 0 NOT NULL,
    cuadrediario_id bigint DEFAULT 0 NOT NULL,
    valor double precision DEFAULT 0 NOT NULL
);


ALTER TABLE public.sucdepositos OWNER TO postgres;

--
-- TOC entry 424 (class 1259 OID 335713)
-- Dependencies: 425 6
-- Name: sucdepositos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sucdepositos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.sucdepositos_id_seq OWNER TO postgres;

--
-- TOC entry 2752 (class 0 OID 0)
-- Dependencies: 424
-- Name: sucdepositos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sucdepositos_id_seq OWNED BY sucdepositos.id;


--
-- TOC entry 2753 (class 0 OID 0)
-- Dependencies: 424
-- Name: sucdepositos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sucdepositos_id_seq', 1, false);


--
-- TOC entry 2711 (class 2604 OID 335459)
-- Dependencies: 420 421 421
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cuadrediarios ALTER COLUMN id SET DEFAULT nextval('cuadrediarios_id_seq'::regclass);


--
-- TOC entry 2716 (class 2604 OID 335689)
-- Dependencies: 423 422 423
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY dinerodenominaciones ALTER COLUMN id SET DEFAULT nextval('dinerodenominaciones_id_seq'::regclass);


--
-- TOC entry 2729 (class 2604 OID 335750)
-- Dependencies: 429 428 429
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY documentotipo ALTER COLUMN id SET DEFAULT nextval('documentotipo_id_seq'::regclass);


--
-- TOC entry 2720 (class 2604 OID 335718)
-- Dependencies: 425 424 425
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sucdepositos ALTER COLUMN id SET DEFAULT nextval('sucdepositos_id_seq'::regclass);


--
-- TOC entry 2740 (class 0 OID 335456)
-- Dependencies: 421
-- Data for Name: cuadrediarios; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2741 (class 0 OID 335679)
-- Dependencies: 423
-- Data for Name: dinerodenominaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (7, '2', 2, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (8, '5', 5, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (9, '10', 10, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (10, '20', 20, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (11, '50', 50, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (12, '100', 100, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (13, '500', 500, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (14, '1000', 1000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (15, '2000', 2000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (16, '5000', 5000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (17, '10000', 10000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (18, '20000', 20000, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'DINERO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (19, '50', 50, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'MONEDA');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (1, '0,05', 0.050000000000000003, '1900-01-01 00:00:00', '2012-05-03 11:55:07', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (2, '0,10', 0.10000000000000001, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (3, '0,125', 0.125, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (4, '0,2', 0.20000000000000001, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (5, '0,5', 0.5, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (6, '1', 1, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'INACTIVO');
INSERT INTO dinerodenominaciones (id, denominacion, valor, created, modified, fin) VALUES (20, '100', 100, '1900-01-01 00:00:00', '1900-01-01 00:00:00', 'MONEDA');


--
-- TOC entry 2743 (class 0 OID 335747)
-- Dependencies: 429
-- Data for Name: documentotipo; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (1, 'DP', 'DEPOSITO', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (2, 'NC', 'NOTA DE CREDITO', '2011-03-17 08:14:59', '2011-03-17 08:14:59', 'AUMENTA');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (3, 'CH', 'CHEQUE', '2011-01-31 09:53:52', '2011-03-17 08:15:35', 'DISMINUYE');
INSERT INTO documentotipo (id, descorta, descripcion, created, modified, accion) VALUES (4, 'ND', 'NOTA DE DEBITO', '2011-03-21 08:40:11', '2011-03-21 17:03:02', 'DISMINUYE');


--
-- TOC entry 2742 (class 0 OID 335715)
-- Dependencies: 425
-- Data for Name: sucdepositos; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2733 (class 2606 OID 335465)
-- Dependencies: 421 421
-- Name: cuadrediarios_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cuadrediarios
    ADD CONSTRAINT cuadrediarios_pkey PRIMARY KEY (id);


--
-- TOC entry 2735 (class 2606 OID 335691)
-- Dependencies: 423 423
-- Name: dinerodenominaciones_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY dinerodenominaciones
    ADD CONSTRAINT dinerodenominaciones_pkey PRIMARY KEY (id);


--
-- TOC entry 2739 (class 2606 OID 335757)
-- Dependencies: 429 429
-- Name: documentotipo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY documentotipo
    ADD CONSTRAINT documentotipo_pkey PRIMARY KEY (id);


--
-- TOC entry 2737 (class 2606 OID 335728)
-- Dependencies: 425 425
-- Name: sucdepositos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sucdepositos
    ADD CONSTRAINT sucdepositos_pkey PRIMARY KEY (id);


-- Completed on 2017-03-16 08:07:02 VET

--
-- PostgreSQL database dump complete
--

